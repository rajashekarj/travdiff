({
    doInit : function(component, event, helper) {
        //Call apex class method to get the List of Leads to be displayed on the UI
        var action = component.get("c.fetchLeadsIFollow");
        action.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                // adding the response from the server to the attribute that is generating the data table.
            	component.set("v.leadsIFollow",response.getReturnValue());
                console.log(JSON.stringify(response.getReturnValue()));
            }
            else{
                //If an error occured in the apex then this segment executes
                console.log(response.getState());
            }
        });
        $A.enqueueAction(action);
    },
    redirectBackToListView : function(component, event, helper){
        //Using the listViewId, firing the navigateToList event to redirect user back to the leads tab
        var listViewId = component.get("v.listViewId");
        var navEvent = $A.get("e.force:navigateToList");
        //Setting the required params for the event : ListViewId, ListViewName not required and scope - sObject name 
        navEvent.setParams({
            "listViewId": listViewId,
            "listViewName": null,
            "scope": "Lead"
        });
        //Firing the event
        navEvent.fire();
    },
    showSpinner : function(component, event, helper){
        //Show spinner when component loads and  apex call made
        var spinner = component.find("spinner");
        $A.util.addClass("slds-show");
        $A.util.removeClass("slds-hide");
    },
    hideSpinner : function(component, event, helper){
        //Hide spinner when component finished loads
  		var spinner = component.find("spinner");
        $A.util.addClass("slds-hide");
        $A.util.removeClass("slds-show");
    }
})