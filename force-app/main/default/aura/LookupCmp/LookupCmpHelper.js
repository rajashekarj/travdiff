({
    updateSearchTerm : function(component, searchTerm) {
        // Save search term so that it updates input
        component.set("v.searchTerm", searchTerm);
        
        // Get previous clean search term
        const cleanSearchTerm = component.set("v.cleanSearchTerm");
        
        // Compare clean new search term with current one and abort if identical
        const newCleanSearchTerm = searchTerm.trim().replace(/\*/g, '').toLowerCase();
        if (cleanSearchTerm === newCleanSearchTerm) {
            return;
        }
        // Update clean search term for later comparison
        component.set('v.cleanSearchTerm', newCleanSearchTerm);
        
        // Ignore search terms that are too small
        if (newCleanSearchTerm.length < 2 && (component.get("v.sObjectName")!='Product2')) {
            component.set('v.searchResults', []);
            return;
        }

        // Apply search throttling (prevents search if user is still typing)
        let searchTimeout = component.get('v.searchThrottlingTimeout');
        if (searchTimeout) {
            clearTimeout(searchTimeout);
        }
        // Two parameters isBSIView and selectedProductsList added for BSI product view 
        var action = component.get("c.search");
        action.setParams({
            "searchTerm" : searchTerm,
            "sObjectName" : component.get("v.sObjectName"),
            "recId" : component.get("v.recordId"),
            "isBSIView": component.get("v.isBSIView"),
            "selectedProductsList": component.get("v.selectedProductsList")
        });
        action.setCallback(this, function(response){
            component.set("v.searchResults",response.getReturnValue());
            console.log(response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    selectResult : function(component, recordId) {
        // Save selection and fire event for parent list update
        var selectedProducts=component.get("v.selectedProductsList");
        var compEvent=component.getEvent("UpdateSelectedProducts");
        const searchResults = component.get("v.searchResults");
        const selectedResult = searchResults.filter(function(result) { return result.id === recordId; });
        if (selectedResult.length > 0) {
            var selection = component.get("v.selection");
            selection = selectedResult;
            component.set("v.selection", selection);
            const sObjectName= component.get("v.sObjectName");
            if(sObjectName=='Product2'){
                selectedProducts.push(recordId);
                component.set("v.selectedProductsList",selectedProducts);
                compEvent.setParams({"spList":selectedProducts});
                compEvent.fire();
            }
        }
        // Reset search
        component.set('v.searchTerm', '');
        component.set('v.searchResults', []);
    },
    
    getSelectedIds : function(component) {
        const selection = component.get('v.selection');
        return selection.map(function(element) { return element.id; });
    },
    
    removeSelectedItem : function(component, removedItemId) {
        //remove selection and list update event
        var selectedProducts=component.get("v.selectedProductsList");
        var compEvent=component.getEvent("UpdateSelectedProducts");
        var updatedSelectedProducts=selectedProducts.filter(function(item){return item !== removedItemId;});
        component.set("v.selectedProductsList",updatedSelectedProducts);
        compEvent.setParams({"spList":updatedSelectedProducts});
        compEvent.fire();
        const selection = component.get('v.selection');
        const updatedSelection = selection.filter(function(item) { return item.id !== removedItemId; });
        component.set('v.selection', updatedSelection);
    },
    
    clearSelection : function(component, itemId) {
        //event for list update and clear selection
        const sObjectName= component.get("v.sObjectName");
        if(sObjectName=='Product2'){
            const selectionId = component.get("v.selectedRecordId");
            var selectedProducts=component.get("v.selectedProductsList");
            var compEvent=component.getEvent("UpdateSelectedProducts");
            var updatedSelectedProducts=selectedProducts.filter(function(item){return item !== selectionId;});
            console.log("ClearSel"+updatedSelectedProducts);
            component.set("v.selectedProductsList",updatedSelectedProducts);
            compEvent.setParams({"spList":updatedSelectedProducts});
            compEvent.fire();
                this.updateSearchTerm(component,'');
        }
        component.set('v.selection', []);
    },
    
    toggleSearchSpinner : function(component) {
        const spinner = component.find('spinner');
        const searchIcon = component.find('search-icon');
        
        $A.util.toggleClass(spinner, 'slds-hide');
        $A.util.toggleClass(searchIcon, 'slds-hide');
    }
})