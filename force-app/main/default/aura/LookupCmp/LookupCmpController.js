({
    doInit:function(component,event,helper){
      if(component.get('v.sObjectName')=='Product2')
      {
        //helper.updateSearchTerm(component,'');
        var action = component.get("c.search");
        action.setParams({
            "searchTerm" : '',
            "sObjectName" : component.get("v.sObjectName"),
            "recId" : component.get("v.recordId"),
            "isBSIView": component.get("v.isBSIView"),
            "selectedProductsList": component.get("v.selectedProductsList")
        });
        action.setCallback(this, function(response){
            component.set("v.searchResults",response.getReturnValue());
            console.log(response.getReturnValue());
        });
        $A.enqueueAction(action);
      }
    },
    
    searchJS : function(component, event, helper) {
        //const action = event.getParam('arguments').serverAction;
        var action = component.get("c.search");
        helper.toggleSearchSpinner(component);
        
        action.setParams({
            searchTerm : component.get('v.cleanSearchTerm'),
            selectedIds : helper.getSelectedIds(component)
        });
        
        action.setCallback(this, function(response) {
            const state = response.getState();
            if (state === 'SUCCESS') {
                helper.toggleSearchSpinner(component);
                // Process server success response
                const returnValue = response.getReturnValue();
                component.set('v.searchResults', returnValue);
            }
            else if (state === 'ERROR') {
                helper.toggleSearchSpinner(component);
                // Retrieve the error message sent by the server
                const errors = response.getError();
                let message = 'Unknown error'; // Default error message
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    const error = errors[0];
                    if (typeof error.message != 'undefined') {
                        message = error.message;
                    } else if (typeof error.pageErrors != 'undefined' && Array.isArray(error.pageErrors) && error.pageErrors.length > 0) {
                        const pageError = error.pageErrors[0];
                        if (typeof pageError.message != 'undefined') {
                            message = pageError.message;
                        }
                    }
                }
                // Fire error toast if available (LEX only)
                const toastEvent = $A.get('e.force:showToast');
                if (typeof toastEvent !== 'undefined') {
                    toastEvent.setParams({
                        title : 'Server Error',
                        message : message,
                        type : 'error',
                        mode: 'sticky'
                    });
                    toastEvent.fire();
                }
            }
        });
        action.setStorable(); // Enables client-side cache & makes action abortable
        $A.enqueueAction(action);
    },
    
    onInput : function(component, event, helper) {
        const newSearchTerm = event.target.value;
        helper.updateSearchTerm(component, newSearchTerm);
    },
    onResultClick : function(component, event, helper) {
        const recordId = event.currentTarget.id;
        component.set("v.selectedRecordId", recordId);
        var errorMsg;
        component.set("v.errors",errorMsg);
        helper.selectResult(component, recordId);
    },
    
    onComboboxClick : function(component, event, helper) {
        // Hide combobox immediatly
        const blurTimeout = component.get('v.blurTimeout');
        if (blurTimeout) {
            clearTimeout(blurTimeout);
        }
        component.set('v.hasFocus', false);
    },
    
    onFocus : function(component, event, helper) {
        component.set('v.hasFocus', true);
    },
    
    onBlur : function(component, event, helper) {
        // Delay hiding combobox so that we can capture selected result
        const blurTimeout = window.setTimeout(
            $A.getCallback(function() {
                component.set('v.hasFocus', false);
                component.set('v.blurTimeout', null);
            }),
            300
        );
        component.set('v.blurTimeout', blurTimeout);
    },
    
    onRemoveSelectedItem : function(component, event, helper) {
        const itemId = event.getSource().get('v.name');
        helper.removeSelectedItem(component, itemId);
    },
    
    onClearSelection : function(component, event, helper) {
        helper.clearSelection(component);
    }
})