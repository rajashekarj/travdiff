({
	/**
	 * This JS method is used to check the lead status on refresh event and call
	 * the LeadConersion apex class to convert the lead A toast message will be
	 * displayed for success and failure Modification 
	 * 
	 *  Log :
	 * -----------------------------------------------------------------------------------------------------------
	 * Developer    User Story/Defect             Date                                 Description
	 * -----------------------------------------------------------------------------------------------------------
	 * Bhanu Reddy      US59527                 5/20/20119                          Original Version
	 *  
	 */
	doInit : function(component, event, helper) {

		// declarations
		let status = component.get("v.leadobject.Status");
         
		let isRecursive = component.get("v.avoidRecurrsion");
		let message;
		const QUALIFIED = 'Qualified';
		const NO = 'no';
		const SUCCESS = 'SUCCESS';
		const ERROR = "ERROR";
		// check if status is qualified and proceed with lead conversion if not
		// recursive call
		if (status == QUALIFIED && isRecursive == NO) 
		{
			component.set("v.avoidRecurrsion", "yes");
			component.set("v.loadSpinner", true);
			let convertlead = component.get("c.accountRedirect");

			// set params for the apex method
			convertlead.setParams({
				"strLeadId" : component.get("v.recordId")
			 });

			// set callback function
			convertlead.setCallback(this, function(response) {
				let status = response.getState();
				if (status == SUCCESS) {
					// fetch the returned account id
					let accountid = response.getReturnValue();

					// navigate to sobject
					let navEvt = $A.get("e.force:navigateToSObject");
					navEvt.setParams({
						"recordId" : accountid
					});
					// stop spinner
					component.set("v.loadSpinner", false);
					
					navEvt.fire();

				}
				if (status == ERROR) {
					// fetch the errors
					let errors = response.getError();
					message = "An error occured";

					if (errors && Array.isArray(errors) && errors.length > 0) {
						message = errors[0];
						// show error toast message
						let toastEvent = $A.get("e.force:showToast");
						toastEvent.setParams({
							title : "Lead Conversion Failed",
							message : message,
							type : "error",
							mode : "sticky"
						});
						// stop spinner
						component.set("v.loadSpinner", false);
						// fire error toast message
						toastEvent.fire();
					} 
				}
			}); //end of call back function

			$A.enqueueAction(convertlead);

		} //end of qualified status check

	} //end of doInit method
});