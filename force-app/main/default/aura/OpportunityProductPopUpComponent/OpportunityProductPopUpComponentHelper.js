({
    doInit : function(component,event){ 
        var action = component.get("c.getComponentLoadConfig");
        action.setParams({"opptyId": component.get("v.recordId"), "stage" : ''});
        // Opportunity Details callback function
        action.setCallback(this, function(response) {
            var closedReasonList = response.getReturnValue();
            if(closedReasonList != null){
                component.set("v.displayPopup",closedReasonList.loadPopup);
                component.set("v.productConfig", closedReasonList.productConfig);
                component.set("v.OptyStage",closedReasonList.stageName);
                component.set("v.OverallStage",closedReasonList.stageName);
                component.set("v.evaluateddeferralDate", closedReasonList.deferraldate);
                component.set("v.evaluatedProspectEffectiveDate", closedReasonList.prospectEffectiveDate);
                component.set("v.isBoundStage", closedReasonList.isBoundStage);
                component.set("v.defaultCarrier", closedReasonList.defaultCarrier);
                component.set("v.opptyBU", closedReasonList.strOpportunityBU);
                component.set("v.OOACount", closedReasonList.productConfig.length);
                var result = closedReasonList.productStage;
                var stageValues = [];
                if( (component.get("v.OverallStage") == 'Out of Appetite' && closedReasonList.strOpportunityBU =='BI-NA') || 
                   component.get("v.OverallStage") == 'Deferred' || component.get("v.OverallStage") == 'Lost' || 
                  component.get("v.OverallStage") == 'Declined' || component.get("v.OverallStage") == 'Quote Unsuccessful') {
                    component.set("v.showProspectEffectiveDate", true);
                } else {
                    component.set("v.showProspectEffectiveDate", false);
                }
                var result = closedReasonList.productStage;
                var stageValues = [];
                //Set values for Close Reason:SM
                var prodStr = '';
                    for(var i = 0;i < closedReasonList.productConfig.length ; i++){
                        prodStr +=i;	
                    }
                    component.set("v.closeReasonIndexString", prodStr);
              
              
                //1 product present, the 'Product Stage' will be defaulted to 'Written' for Bound stage
                if(closedReasonList.isBoundStage  && (!(closedReasonList.multipleProducts)) ){
                    stageValues.push({label: 'Written',value: true });
                    component.set("v.writtenIndexString",'0');
                    //changes start for 12/4/2019
                    for(var i = 0;i < closedReasonList.productConfig.length ; i++){
                        if( closedReasonList.productConfig[i].productData.TRAV_Stage__c == null){
                            closedReasonList.productConfig[i].productData.TRAV_Stage__c = 'Written';
                        }
                    }
                    //changes ended for 12/4/2019
                }
                //Changes for US69890 starts
                if(component.get("v.OverallStage") == 'Out of Appetite' && (!(closedReasonList.multipleProducts)) ){
                   // stageValues.push({label: 'Out Of Appetite',value: true });
                    component.set("v.outOfAppIndexString",'');
                }
                //Changes for US69890 end
                /*If stage is lost, all product should be lost
                if(component.get("v.OptyStage") == 'Lost'){
                    stageValues.push({label: 'Lost',value: true });
                }*/
                
                //Multiple Products in Bound stage, Product stage will be blank
                if(closedReasonList.isBoundStage && closedReasonList.multipleProducts ){
                    stageValues.push({label: '',value: true });
                    
					/*set initial indexes in productErrorIndexString
                    for(var i = 0;i < closedReasonList.productConfig.length ; i++){
                        var prodStr = component.get("v.productErrorIndexString");
                        component.set("v.productErrorIndexString", prodStr+i);
                    }*/
                }
                /*Below condition is added to have blank value for product stage declined if multiple products are there as part of US63930 by Sayanka*/
                if(component.get("v.OptyStage") == 'Declined' && (closedReasonList.multipleProducts) ){
                   // stageValues.push({label:'',value: true });
                    
                    //set initial indexes in productErrorIndexString
                    /*for(var i = 0;i < closedReasonList.productConfig.length ; i++){
                        var prodStr = component.get("v.productErrorIndexString");
                        component.set("v.productErrorIndexString", prodStr+i);
                    }*/
                } 
                
                if(result != null && result != undefined && result != ''){
                    for (var i = 0; i < result.length; i++) {
                        
                        if(result[i] == component.get("v.OptyStage")) {
                            if(!(component.get("v.OptyStage") == 'Declined' && (closedReasonList.multipleProducts))){
                                stageValues.push({label: result[i],value: true });
                            }else{
                                stageValues.push({label: result[i],value: false });
                            }
                        }
                        else{
                            if(closedReasonList.isBoundStage && !closedReasonList.multipleProducts){
                                if(result[i] != 'Written'){
                                    stageValues.push({label: result[i],value: false });
                                }
                            }else{
                                stageValues.push({label: result[i],value: false });
                               
                            }
                        }
                    }
                }
                component.set("v.stageList",stageValues);
                component.set("v.multipleProducts", closedReasonList.multipleProducts);
            }
            
        });
        $A.enqueueAction(action);
    },
    handleReasonChange: function (component, event, items){
        var actionSubList = component.get("c.getSubReasonList");
        actionSubList.setParams({"opptyProductReasonList": items,
                                 "opptyCloseReason": component.get("v.OptyStage")});
        actionSubList.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                var result = response.getReturnValue();
                var subReasonValues = [];
                for (var i = 0; i < result.length; i++) {
                    subReasonValues.push({
                        label: result[i],
                        value: result[i] });
                }
                component.set("v.subReasonList",subReasonValues);
            }
        });
        $A.enqueueAction(actionSubList);
    },
    checkReasonList : function(component,event) {
        var items = component.get("v.selectedReasonList");
        var action = component.get("c.checkForOtherAsReason");
        action.setParams({"opptyProductReason": items});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.displayOtherBox", response.getReturnValue());   
            }
        });
        $A.enqueueAction(action);		
    },
    addCarrier: function(component,event){
        var selectedCarrier = event.getParam("carrierByEvent");
        var tempCarriers = [];
    },
    submitDetails : function(component,event){
        var isBound = component.get("v.isBoundStage");
        var productConfig = component.get("v.productConfig");
        var carrierList = component.get("v.selectedCarriers");
        //console.log('**Carrier List**'+JSON.parse(JSON.stringify(carrierList)));
        var productList = [];
		for(var i = 0 ; i < productConfig.length ; i++){
            //Add default carrier
            var def = component.get("v.defaultCarrier");
            if(def != null && def != '' && def != undefined && productConfig[i].productData.TRAV_Stage__c == 'Quote Unsuccessful'){
                productConfig[i].productData.TRAV_Product_Winning_New_Carrier__c = JSON.parse(JSON.stringify(def)).Id;
            }
			productList.push(productConfig[i].productData);
        }
        
        var clearCarriers = [];
		/*Added validation for deferral date as part of US63930 and product validation as part of US63929 by Sayanka*/
        
        
        if(isBound){
            var currentStgList = component.get("v.writtenIndexString");
            //first check if blank
            if(currentStgList.length == 0){
                component.set("v.writtenValidated",false);
                var writtenErrField = component.find("writtenproductError");
                $A.util.addClass(writtenErrField, 'slds-show');
                $A.util.removeClass(writtenErrField, 'slds-hidden');
            }else{
                component.set("v.writtenValidated",true);
                $A.util.addClass(writtenErrField, 'slds-hidden');
                $A.util.removeClass(writtenErrField, 'slds-show');
                
            }
        }
        //Product Stage error display
        var productStgList = component.get("v.productErrorIndexString");
        var prodErrField = component.find("productError");
        //first check if blank
        if(productStgList.length === 0){
            $A.util.addClass(prodErrField, 'slds-hidden');
            $A.util.removeClass(prodErrField, 'slds-show');
        }else{
            if((component.get("v.opptyBU") != 'BI-NA' && component.get("v.OverallStage") != 'Deferred' && component.get("v.OverallStage") != 'Out of Appetite')  || 
                component.get("v.opptyBU") == 'BI-NA') {
                $A.util.addClass(prodErrField, 'slds-show');
            	$A.util.removeClass(prodErrField, 'slds-hidden');
            }
            
            
        }
        //Added for US69890 - out of appetite error
        ////Changes for US69890 starts 
        var outOfAppIndexStringlst = component.get("v.outOfAppIndexString");
        var outofAppProductErrorField = component.find("outofAppProductError");
        var OOACount = component.get("v.OOACount");
        
        if(component.get("v.opptyBU") == 'BI-NA' ||
           (component.get("v.OverallStage") != 'Out of Appetite' && component.get("v.OverallStage") != 'Deferred' && component.get("v.opptyBU") != 'BI-NA') ) {
            
            
            if(outOfAppIndexStringlst == 0 && outOfAppIndexStringlst !== ''){
                component.set("v.outOfAppValidated",true);
                $A.util.addClass(outofAppProductErrorField, 'slds-show');
                $A.util.removeClass(outofAppProductErrorField, 'slds-hidden');
            }else{
                component.set("v.outOfAppValidated",false);
                $A.util.addClass(outofAppProductErrorField, 'slds-hidden');
                $A.util.removeClass(outofAppProductErrorField, 'slds-show');
            }
            component.set("v.OOACount",outOfAppIndexStringlst);
        }
        var productCloseDescription = component.find("ProductCloseReasonSelectError");
        var closeReasonValidated = new Boolean(true);
        var isDeclineSelected = new Boolean(true);
        var isLostSelected = new Boolean(true);
        if(component.get("v.opptyBU") == 'BI-NA') {
            //DE9953 product close reason required based on product stage
            var TravError = false;
            var stagesList = ['Quote Unsuccessful','Lost','Written','Declined'];
            for(var i = 0;i < productConfig.length ; i++){
                if( (productConfig[i].productData.TRAV_Closed_Reason__c == '' || productConfig[i].productData.TRAV_Closed_Reason__c == undefined)
                   && stagesList.includes(productConfig[i].productData.TRAV_Stage__c)){
                    TravError = true;
                    break;
                }
            }
            if(TravError) {
                if(component.get("v.outOfAppValidated") == false){
                    closeReasonValidated = false;
                component.set("v.ProdCloseReasonSelectBoolean",true);
                $A.util.addClass(productCloseDescription, 'slds-has-error');
                }
                
            } else {
                closeReasonValidated = true;
                $A.util.removeClass(productCloseDescription, 'slds-has-error'); 
                component.set("v.ProdCloseReasonSelectBoolean",false);
            }
            
            //DE9970 changes start 
            if(component.get("v.OverallStage") == 'Declined' && productConfig.length > 0){
                var declineErrorField = component.find("declineSelectError");
                var declineCount = 0;
                for(var i = 0;i < productConfig.length ; i++){
                    if( productConfig[i].productData.TRAV_Stage__c == 'Declined'){
                        declineCount = declineCount + 1; 
                    }
                }
                if(declineCount == 0) {
                    isDeclineSelected = false;
                    $A.util.addClass(declineErrorField, 'slds-show');
                    $A.util.removeClass(declineErrorField, 'slds-hidden');
                }else {
                    isDeclineSelected = true;
                    $A.util.removeClass(declineErrorField, 'slds-show');
                    $A.util.addClass(declineErrorField, 'slds-hidden');
                }
            }
            //for Lost
            if(component.get("v.OverallStage") == 'Lost' && productConfig.length > 0){
                var lostErrorField = component.find("lostSelectError");
                var LostCount = 0;
                for(var i = 0;i < productConfig.length ; i++){
                    if( productConfig[i].productData.TRAV_Stage__c == 'Lost'){
                        LostCount = LostCount + 1; 
                    }
                }
                if(LostCount == 0) {
                    isLostSelected = false;
                    $A.util.addClass(lostErrorField, 'slds-show');
                    $A.util.removeClass(lostErrorField, 'slds-hidden');
                }else {
                    isLostSelected = true;
                    $A.util.removeClass(lostErrorField, 'slds-show');
                    $A.util.addClass(lostErrorField, 'slds-hidden');
                }
            }
        }
        //DE9970 changes end
		var productValidated = component.get("v.writtenValidated");
        var carrierValid = component.get("v.carrierValidated");
        var prodStageValidated = component.get("v.productErrorIndexString");
        var errorField = component.find("dateError");
        var errorProspectEffective = component.find("dateProspectError");
        var defDateId = component.find("deferralDate");
        var prosEffDateId = component.find("prospectEffectiveDate");
        var validated = new Boolean(true);
        if(defDateId != null ){
            var defDate = defDateId.get("v.value");
            
            if(defDate === null || defDate === ''){
                validated = false;
                $A.util.addClass(errorField, 'slds-visible');
                $A.util.removeClass(errorField, 'slds-hidden');
                component.set("v.isValidated",false);
            }else{
                $A.util.removeClass(errorField, 'slds-visible');
                $A.util.addClass(errorField, 'slds-hidden');
                component.set("v.isValidated",true);
            }
        }
        //Added for US68543
        if(prosEffDateId != null ){
            var prosEffDate = prosEffDateId.get("v.value");
            if(prosEffDate === null || prosEffDate === ''){
                validated = false;
                $A.util.addClass(errorProspectEffective, 'slds-visible');
                $A.util.removeClass(errorProspectEffective, 'slds-hidden');
                component.set("v.isProspectDateValidated",false);
            }else{
                $A.util.removeClass(errorProspectEffective, 'slds-visible');
                $A.util.addClass(errorProspectEffective, 'slds-hidden');
                component.set("v.isProspectDateValidated",true);
            }
        }
        
        //future date check
        var showError = false;
        var futureDtvalidated = new Boolean(true);
        var errorDt = component.find("dateFutureError");
        var defDateId = component.find("deferralDate");
        if(defDateId != null){
            var currDate = new Date();
            var defDate = defDateId.get("v.value");
            if(Date.parse(defDate) <= currDate){
                showError = true;
                component.set("v.isValidated",false);
            }else{
                component.set("v.isValidated",true);
            }
        }
        //Added for US68543
        if(prosEffDateId != null){
            var currDate = new Date();
            var prosEffDate = prosEffDateId.get("v.value");
            //Validating Prospect Effective Date
            if(Date.parse(prosEffDate) < currDate){
                showError = true;
                component.set("v.isProspectDateValidated",false);
            }else{
                component.set("v.isProspectDateValidated",true);
            }
        }
        if(showError) {
            futureDtvalidated = false;
            $A.util.addClass(errorDt, 'slds-visible');
            $A.util.removeClass(errorDt, 'slds-hidden');
        } else {
            futureDtvalidated = true;
            $A.util.removeClass(errorDt, 'slds-visible');
            $A.util.addClass(errorDt, 'slds-hidden');
        }
        //Checking if Out of appetite reason is populated or not
        var ooaReason = component.find("outOfAppetiteReason");
        var OOAReaonPresent = true;
        if(ooaReason != null) {
            var ooaReasonValue = ooaReason.get("v.value");
            var closeDescription = component.find("deferralDescription");
            var closeDescriptionValue = closeDescription.get("v.value");
            if(component.get("v.opptyBU") != 'BI-NA' && (ooaReasonValue == null || ooaReasonValue == '') && component.get("v.OverallStage") == 'Out of Appetite') {
                OOAReaonPresent = false;
                $A.util.addClass(ooaReason, 'slds-has-error');
            } else if(ooaReasonValue == 'Other' && (closeDescriptionValue == '' || closeDescriptionValue == null) ) {
                OOAReaonPresent = false;
                $A.util.addClass(closeDescription, 'slds-has-error');  
                $A.util.removeClass(ooaReason, 'slds-has-error'); 
            } else {
                $A.util.removeClass(ooaReason, 'slds-has-error'); 
                $A.util.removeClass(closeDescription, 'slds-has-error'); 
            }
        }
         
        if(futureDtvalidated &&
           validated && 
           productValidated &&
           component.get("v.outOfAppValidated") == false &&
           (carrierValid === null || carrierValid === '') && (prodStageValidated === null ||component.get("v.OptyStage") =='Deferred'|| component.get("v.OptyStage") =='Out of Appetite' || prodStageValidated === '') && 
            OOAReaonPresent  &&
            closeReasonValidated && 
            isDeclineSelected &&
            isLostSelected
          ){
            component.find("opportunityForm").submit();  
            component.set("v.callProcessRecordUpdate",true);
        //if(futureDtvalidated && validated && productValidated && (carrierValid === null || carrierValid === '') && (prodStageValidated === null || prodStageValidated === '')){
            var action = component.get("c.saveProductList");  
            action.setParams({
                "listOfProducts": productList,
                "stage" : component.get("v.OptyStage")
            });      
            debugger;
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state === 'SUCCESS'){
                    component.find('notiFlib').showToast({
                        "title": "Record updated",
                        "message": "The record has been updated",
                        "variant": "success"
                    });
                }/*else{
                    component.find('notiFlib').showToast({
                        "title": "Record not updated",
                        "message": "The record was not updated",
                        "variant": "error"
                    });
                }*/
            });
            $A.enqueueAction(action);
            component.set("v.displayPopup", false);
        } else {
            component.set("v.callProcessRecordUpdate",false);
        }
    },
    handleClick: function(component, event, where) {
        //getting target element of mouse click
        var tempElement = event.target;
        
        var outsideComponent = true;
        //click indicator
        //1. Drop-Down is clicked
        //2. Option item within dropdown is clicked
        //3. Clicked outside drop-down
        //loop through all parent element
        while(tempElement){
            if(tempElement.id === 'ms-list-item'){
                //2. Handle logic when picklist option is clicked
                //Handling option click in helper function
                if(where === 'component'){
                    this.onOptionClick(component, event.target);
                }
                outsideComponent = false;
                break;
            } else if(tempElement.id === 'ms-dropdown-items'){
                //3. Clicked somewher within dropdown which does not need to be handled
                //Break the loop here
                outsideComponent = false;
                break;
            } else if(tempElement.id === 'ms-picklist-dropdown'){
                //1. Handle logic when dropdown is clicked
                if(where === 'component'){
                    this.onDropDownClick(tempElement);
                }
                outsideComponent = false;
                break;
            }
            //get parent node
            tempElement = tempElement.parentNode;
        }
        if(outsideComponent){
            this.closeAllDropDown();
        }
    },
    validateFutureDate:function(component, event, helper){
        var errorDt = component.find("dateFutureError");
        var defDateId = component.find("deferralDate");
        var prosEffDateId = component.find("prospectEffectiveDate");
        var showError = false;
        if(defDateId != null){
            var currDate = new Date();
            var defDate = defDateId.get("v.value");
            var prosEffDate = prosEffDateId.get("v.value");
            //Validating Prospect Creation Date
            if(Date.parse(defDate) < currDate){
                component.set("v.isValidated",false);
                showError = true;
            }else{
                component.set("v.isValidated",true);
            }
        }
        //Added for US68543
        if(prosEffDateId != null){
            var currDate = new Date();
            var prosEffDate = prosEffDateId.get("v.value");
            //Validating Prospect Effective Date
            if(Date.parse(prosEffDate) < currDate){
                showError = true;
                component.set("v.isProspectDateValidated",false);
            }else{
                component.set("v.isProspectDateValidated",true);
            }
        }
        if(showError) {
            $A.util.addClass(errorDt, 'slds-visible');
            $A.util.removeClass(errorDt, 'slds-hidden');
        } else {
            $A.util.removeClass(errorDt, 'slds-visible');
            $A.util.addClass(errorDt, 'slds-hidden');
        }
    }
    
})