({
    doInit: function(component, event, helper) {
		helper.doInit(component,event);
        
       
    },
    ProcessRecordUpdate: function(component, event, helper) {
        var outofAppProductErrorField = component.find("outofAppProductError");
        if(outofAppProductErrorField != undefined && component.get("v.outOfAppValidated")) {
            $A.util.addClass(outofAppProductErrorField, 'slds-show');
            $A.util.removeClass(outofAppProductErrorField, 'slds-hidden');
        }else if (outofAppProductErrorField != undefined && component.get("v.outOfAppValidated") == false) {
            $A.util.addClass(outofAppProductErrorField, 'slds-hidden');
             $A.util.removeClass(outofAppProductErrorField, 'slds-show');
        } 
        var changeType = event.getParams().changeType;
        var changedFields = event.getParams().changedFields;
        if (changeType === "LOADED"  && component.get("v.callProcessRecordUpdate") == true) { 
          helper.doInit(component,event);

       }
       var runInit = true;
       if (changeType === "CHANGED" && component.get("v.callProcessRecordUpdate") == true) { 
           var prospectCreationdate = changedFields['TRAV_Defferal_Date__c'];
           if(prospectCreationdate != undefined 
              && prospectCreationdate['oldValue'] == null 
              && prospectCreationdate['value'] != null ) {
               runInit = false;
           }
           if(runInit){
               helper.doInit(component,event);
           }

       }
},
    handleReasonChange: function (component, event, helper) {
        //Get the Selected reason values
        var items = component.get("v.items");
        items = event.getParam("values");
        component.set("v.selectedReasonList", items);
        var a = component.get('c.checkReasonList');
        $A.enqueueAction(a);
        helper.handleReasonChange(component,event,items);
    },  
    handleSubReasonChange: function (component, event, helper) {
        //Get the Selected sub-reason values   
        var items = component.get("v.items");
        items = event.getParam("values");
        component.set("v.selectedSubReasonList", items);   
    },
    checkReasonList : function(component, event, helper){
        helper.checkReasonList(component,event);
    }, 
    submitDetails : function(component, event, helper) {
        helper.submitDetails(component,event);
    },    
    closeModal : function(component, event, helper) {
        component.set("v.displayPopup", false);
    },
    closeDropdowns : function(component, event, helper) {
        //var toggleClick = component.get("v.toggleClick");
        //component.set("v.toggleClick", !toggleClick);
        
    },
    addCarrier : function(component, event, helper){
		 helper.addCarrier(component,event);
    },
    handleValidation : function(component, event, helper){
        //Changes for US69890 starts
        var OutOfAppetiteIndex = event.getParam("OutOfAppetiteIndexStr");
        if(OutOfAppetiteIndex != null) {
            component.set("v.outOfAppIndexString",OutOfAppetiteIndex);
        }
        //Changes for US69890 end
        var forBoundValidation = event.getParam("forBoundValidation");
        if(!forBoundValidation){
            var errIndex = event.getParam("errorIndexStr");
            if (errIndex != null){
                errIndex = errIndex.toString();
            }
            var add = event.getParam("add");
            if(add){
                var carrierStr = component.get("v.carrierValidated");
                
                if(carrierStr != null && carrierStr != '' ){
                    if(!(carrierStr.indexOf(errIndex) > -1)){
                        component.set("v.carrierValidated",carrierStr+errIndex);
                    }
                }else{
                    component.set("v.carrierValidated",errIndex);
                }
            }else{
                var carrierStr = component.get("v.carrierValidated");
                if(carrierStr != null && carrierStr != '' ){
                    if(carrierStr.indexOf(errIndex) > -1){
                        carrierStr = carrierStr.replace(errIndex,'');
                        component.set("v.carrierValidated",carrierStr);
                    }
                }
            }
        }else{
            
            var errIndex = event.getParam("writtenIndexStr");
            if (errIndex != null){
                errIndex = errIndex.toString();
            }
            var addition = event.getParam("addition");
            if(addition){
                var stageStr = component.get("v.writtenIndexString");
                
                if(stageStr != null && stageStr != '' ){
                    if(!(stageStr.indexOf(errIndex) > -1)){
                        component.set("v.writtenIndexString",stageStr+errIndex);
                    }
                }else{
                    component.set("v.writtenIndexString",errIndex);
                }
            }else{
                var stageStr = component.get("v.writtenIndexString");
                if(stageStr != null && stageStr != '' ){
                    if(stageStr.indexOf(errIndex) > -1){
                        stageStr = stageStr.replace(errIndex,'');
                        component.set("v.writtenIndexString",stageStr);
                    }
                }
            }
            
            
        }
        //Product Stage validation goes here
            var prodIndex = event.getParam("productStageIndexStr");
            if (prodIndex != null){
                prodIndex = prodIndex.toString();
            }
            var prodAddition = event.getParam("productStageAddition");
            if(prodAddition){
                var stageStr = component.get("v.productErrorIndexString");
                
                if(stageStr != null && stageStr != '' ){
                    if(!(stageStr.indexOf(prodIndex) > -1)){
                        component.set("v.productErrorIndexString",stageStr+prodIndex);
                    }
                }else{
                    component.set("v.productErrorIndexString",prodIndex);
                }
            }else{
                var stageStr = component.get("v.productErrorIndexString");
                if(stageStr != null && stageStr != '' ){
                    if(stageStr.indexOf(prodIndex) > -1){
                        stageStr = stageStr.replace(prodIndex,'');
                        component.set("v.productErrorIndexString",stageStr);
                    }
                }
            }
        
    },
    validateFutureDate:function(component, event, helper){
		
        helper.validateFutureDate(component, event, helper);
    },
    validateCloseReason:function(component, event, helper){
        var stagevalue = event.getParam("SelectedStage");
        component.set("v.closedStageValueStr",stagevalue);
		var errIndex = event.getParam("closeReasonStr");
        if (errIndex != null){
            errIndex = errIndex.toString();
        }
        var addition = event.getParam("closeReasonAdd");
        if(addition){
            console.log('**Entered if event handler method**');
            var closeReasonStr = component.get("v.closeReasonIndexString");
            
            if(closeReasonStr != null && closeReasonStr != '' ){
                if(!(closeReasonStr.indexOf(errIndex) > -1)){
                    component.set("v.closeReasonIndexString",closeReasonStr+errIndex);
                }
            }else{
                component.set("v.closeReasonIndexString",errIndex);
            }
        }else{
            console.log('**Entered else event handler method**');
            var closeReasonStr = component.get("v.closeReasonIndexString");
            if(closeReasonStr != null && closeReasonStr != '' ){
                if(closeReasonStr.indexOf(errIndex) > -1){
                    closeReasonStr = closeReasonStr.replace(errIndex,'');
                    component.set("v.closeReasonIndexString",closeReasonStr);
                }
            }
        }
    }
})