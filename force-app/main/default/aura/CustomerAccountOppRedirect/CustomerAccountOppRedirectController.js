({
	doInit : function(component, event, helper) {
		$A.get("e.force:closeQuickAction").fire();
       var parentId=component.get("v.recordId");
        var action= component.get("c.getRecordTypeId");
        var createRecordEvent = $A.get("e.force:createRecord");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var varrecordTypeId=response.getReturnValue();
            createRecordEvent.setParams({
                "entityApiName": "Opportunity",
                "defaultFieldValues" :{
                    "Name" : "Opportunity Name",
                    "RecordTypeId"  : varrecordTypeId,
                    "AccountId" : parentId,
                    "CloseDate" : $A.localizationService.formatDate(new Date(), "YYYY-MM-DD")
                } 
            });
             createRecordEvent.fire();
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action); 
        
	}
})