({
    doInit : function(component){
        component.set("v.isOpen", true);
        //Get Title Values
        var action = component.get("c.getTitleValues");
        action.setCallback(this,function(response){
            component.set("v.titleValues",response.getReturnValue());
        });
        $A.enqueueAction(action);
        console.log(component.get("v.parentFieldId"));
    },
    saveRecord : function(component, event, helper) {
        var isFormValidate = helper.validateForm(component, event, helper);
        if(isFormValidate){
            var recDetails = component.get("v.contactRec");
            var recordId = component.get("v.recordId");
            var parentFieldId = component.get("v.parentFieldId");
            if(recordId == undefined || recordId == ""){
                recordId = component.get("v.parentFieldId");
            }
            var action = component.get("c.saveContact");
            action.setParams({
                contactRec : recDetails,
                recordId : recordId
            });
            action.setCallback(this, function(response){
                console.log(response.getReturnValue());
                var data = response.getReturnValue();
                var returnVal = response.getReturnValue();
                if(returnVal == undefined || returnVal.strReturn == null || returnVal.strReturn.toUpperCase() =="Error" || returnVal.strReturn.toUpperCase() =="FAILURE"){
                    //Toast for Error
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "Error",
                        "title": "Error!",
                        "message": "The system has encountered an issue and is unable to create your contact at this time. \n Please wait while we fix the issue and contact your system administrator with any questions."
                    });
                    toastEvent.fire();
                    //fire refresh view event
                    $A.get("e.force:refreshView").fire();
                    component.set("v.multipleRecords",false);
                    component.set("v.errorAPI",true);
                    component.set("v.isOpen",false);
                }
                else if(returnVal != undefined && returnVal.strReturn.toUpperCase() == "CREATE"){
                    // This executes when a new contact was created in MDM and now the page should redirect to the point of invocation
                    //Toast for Success
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "Success",
                        "title": "Success!",
                        "message": "Record Created Successfully"
                    });
                    toastEvent.fire();
                    //fire refresh view event
                    $A.get("e.force:refreshView").fire();
                    // close modal
                    component.set("v.isOpen",false);
                }
                    else if(returnVal != undefined && returnVal.strReturn.toUpperCase() == "UPDATE"){
                        // This executes when a match is found in MDM
                        var contactDetails = returnVal.lstWrapperContactDetails;
                        var contactRec = {sObjectType : "Contact"};
                        var contactList = [];
                        component.set("v.multipleRecords",true);
                        component.set("v.showNewForm",false);
                        for(var i= 0; i< contactDetails.length; i++){
                            contactRec.FirstName = contactDetails[i].fstNm;
                            contactRec.LastName = contactDetails[i].lstNm;
                            contactRec.Email = contactDetails[i].busEmailAddr;
                            contactRec.Phone = contactDetails[i].busTelNbr;
                            contactRec.TRAV_External_Id__c = contactDetails[i].agencyIndvId;
                            contactList.push(contactRec);
                        }
                        component.set("v.wrapperList",returnVal.lstWrapperContactDetails);
                        component.set("v.listOfContacts",contactList);
                    }
            });
            $A.enqueueAction(action);   
        }
    },
    selectContact : function(component, event, helper) {
        //call method to create contact
        var contactList = component.get("v.listOfContacts");
        var wrapperList = component.get("v.wrapperList");
        var selectedContact = contactList[component.get("v.selectedIndex")];
        var selectedWrapperItem = wrapperList[component.get("v.selectedIndex")];
        var recordId = component.get("v.recordId");
        var parentFieldId = component.get("v.parentFieldId");
        if(recordId == undefined || recordId == ""){
            recordId = component.get("v.parentFieldId");
        }
        var action = component.get("c.saveExistingContact");
        action.setParams({
            wrapperString : JSON.stringify(selectedWrapperItem),
            contactRec : selectedContact,
            recordId : recordId
        });
        action.setCallback(this, function(response){
            //Toast for Success
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type": "Success",
                "title": "Success!",
                "message": "Record Created Successfully"
            });
            toastEvent.fire();
            //fire refresh view event
            $A.get("e.force:refreshView").fire();
            // close modal    
            component.set("v.isOpen",false);
            component.set("v.showNewForm",false);
            component.set("v.multipleRecords",false); 
        });
        $A.enqueueAction(action);
    },
    closeModal : function(component, event, helper){
        component.set("v.errorAPI",false); //deprecated multipleRecords
        component.set("v.multipleRecords",false);
        component.set("v.showNewForm",false);
        component.set("v.isOpen",false);
        component.set("v.newClicked",false);
    },
    showSpinner : function(component, event, helper){
        var spinner = component.find("loading");
        $A.util.addClass(spinner,"slds-show");
        $A.util.removeClass(spinner,"slds-hide");
    },
    hideSpinner : function(component, event, helper){
        var spinner = component.find("loading");		
        $A.util.addClass(spinner,"slds-hide");
        $A.util.removeClass(spinner,"slds-show");
    },
    setSelected : function(component, event, helper){
        var target = event.currentTarget;
        var selectedIndex = target.getAttribute("data-index");
        if(selectedIndex == component.get("v.selectedIndex")){
            component.set("v.contactSelected",false);
            component.set("v.selectedIndex",-1);   
        }
        else{
            component.set("v.contactSelected",true);
            component.set("v.selectedIndex",selectedIndex);
        }
    }
})