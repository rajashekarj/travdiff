({
    validateForm : function(component, event, helper) {
        var formValidated = true;
        var fName = component.find("fName");
        if(fName.get("v.value")== undefined || fName.get("v.value")==""){
            fName.showHelpMessageIfInvalid();
            formValidated = false;
        }
        var lName = component.find("lName");
        if(lName.get("v.value")== undefined || lName.get("v.value")==""){
            lName.showHelpMessageIfInvalid();
            formValidated = false;
        }
        var email = component.find("email");
        if(email.get("v.value")== undefined || email.get("v.value")==""){
            email.showHelpMessageIfInvalid();
            formValidated = false;
        }
        return formValidated;
    }
})