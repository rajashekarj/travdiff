({
    doInit : function(component, event, helper) {
        var stageName = component.get("v.opportunityStage");
        var  ClosedStages = ['Written','Declined','Quote Unsuccessful','Deferred','Out of Appetite','Not Interested','Lost','Lost to Competition','Not Purchased','Received From Other Agent','Did Not Acquire','Wipeout'];
        if(!ClosedStages.includes(stageName)){
            component.set("v.errorBlock",true);
        }else{
            component.set("v.spinner",true);
            var action = component.get("c.getCloneBSIOpportunity");
            action.setParams({
                "objOpportunityId": component.get("v.opportunityId")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log('response>'+response.getReturnValue() +response.getState());
                //message = response.getReturnValue();
                if (state === "SUCCESS") {
                    component.set("v.spinner",false);
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": response.getReturnValue(),
                        "slideDevName": "detail"
                    });
                    console.log('navEvt'+navEvt);
                    navEvt.fire();
                }
                
            });
            // enqueue the server side action  
            $A.enqueueAction(action);
        }
    },
    Cancel : function(component, event, helper){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.opportunityId"),
            "slideDevName": "detail"
        });
        navEvt.fire();
    }
})