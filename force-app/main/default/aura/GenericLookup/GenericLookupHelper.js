({
    //function to set the selected record
    recordSelected : function(component, event, helper) {
        var selectedIndex = helper.getIndexFromParent(event.target,helper,"data-selectedIndex");  
        if(selectedIndex){
            var searchResults = component.get("v.searchResults");
            var selectedRecord = searchResults[selectedIndex];
            if(selectedRecord.val){
                component.set("v.selectedRecord",selectedRecord);
                component.set("v.lastSearchResults",searchResults);
            } 
            component.set("v.searchResults",null); 
        } 
    }, 
    
    //function to search records based on input
    searchRecords : function(component, event, helper) { 
        console.log('search val ' + event.target.value);
        var searchText = event.target.value; 
        var lastSearchedText = component.get("v.lastSearchedText");
        if (event.keyCode == 27 || !searchText.trim()) { 
            helper.clearSelection(component, event, helper);
        }else if(searchText.trim() != lastSearchedText  ){ 
            
            var objectName = component.get("v.objectName");
            var searchFieldName = component.get("v.searchFieldName");
            var returnFieldName = component.get("v.returnFieldName");
            var fieldToSearch = component.get("v.fieldToSearch");
            var limit = component.get("v.limit");
            
            var action = component.get('c.searchSobjectRecords');
            action.setStorable();
            console.log(objectName + searchFieldName + returnFieldName + limit + fieldToSearch + searchText );
            action.setParams({
                strObjectName : objectName,
                strSearchFieldName : searchFieldName,
                strReturnFieldName : returnFieldName,
                strLimit : limit, 
                strFieldToSearch : fieldToSearch,
                strSearchText : searchText,
                strRecordTypeName : component.get("v.recordTypeName"),
                strParentRecordId : component.get("v.parentRecordId")
            });
            
            action.setCallback(this,function(response){
                this.handleResponse(response,component,helper);
            });
            console.log("enable new" + component.get("v.enableNew"));
            component.set("v.lastSearchedText",searchText.trim());
            $A.enqueueAction(action); 
        }else if(searchText && lastSearchedText && searchText.trim() == lastSearchedText.trim()){ 
            component.set("v.searchResults",component.get("v.lastSearchResults"));
        }         
    },
    //function to set the search results
    handleResponse : function (response,component,helper){
        if (response.getState() === 'SUCCESS') {
            var retObj = JSON.parse(response.getReturnValue());
            if(retObj.length <= 0){
                component.set("v.noRecordFound",true);
                var noResult = JSON.parse('[{"text":"No Results Found"}]');
                component.set("v.searchResults",noResult); 
                component.set("v.lastSearchResults",noResult);
            }else{
                component.set("v.noRecordFound",false);
                component.set("v.searchResults",retObj); 
                component.set("v.lastSearchResults",retObj);
            }  
        }else if (response.getState() === 'ERROR'){
            //toast message for error
            var toast = $A.get("e.force:showToast");
            if(toast){
                toast.setParams({
                    "title":"Error",
                    "type":"Error",
                    "message":"An error occurred."
                });
            }
            toast.fire();
        }
    },
    //function to set the selected record index
    getIndexFromParent : function(target,helper,attributeToFind){
        var selectedIndex = target.getAttribute(attributeToFind);
        while(!selectedIndex){
            target = target.parentNode ;
            selectedIndex = helper.getIndexFromParent(target,helper,attributeToFind);           
        }
        return selectedIndex;
    },
    //function to clear the selected record
    clearSelection: function(component, event, helper){
        component.set("v.selectedRecord",null);
        component.set("v.searchResults",null);
    },
    createNewContact : function(component, event, helper){
        component.set("v.newCustTPAClicked",false);
        if(component.get("v.recordTypeName") == 'TRAV_Agency_Non_Licensed_Contact;TRAV_Agency_Licensed_Contact'){
            component.set("v.newClicked",true);
        }
        else{
            component.set("v.newCustTPAClicked",true);
            
        }
        
    } 
})