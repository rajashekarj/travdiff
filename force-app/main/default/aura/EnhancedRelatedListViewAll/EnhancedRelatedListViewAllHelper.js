({
	loadMore: function(component,helper)
    {
        component.set("v.pageNumber",component.get("v.pageNumber")+1);
        var object=component.get("v.objectAPIName");
        var action= component.get('c.getData');
        action.setParams({ 
            objAPIName: object, 
            sortCriteria: component.get("v.sort"),
            filterCriteria: component.get("v.filter"),
            recordId: component.get("v.recordId"),
            parentLookup: component.get("v.parentField"),
            relatedListName:component.get("v.relatedListName"),
            pageNumber: component.get("v.pageNumber")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var currWrapper=component.get("v.wrapper.lstValues");
                var returnValue=response.getReturnValue();
                var updatedLstValues = [];
                for (var lstValue of returnValue.lstValues) {
                    var updatedListLength = updatedLstValues.push([]);
                    for (var ilstValue of lstValue) {
                        updatedLstValues[updatedListLength - 1].push({
                            lstVal: ilstValue,
                            isHtml: this.isHtmlContent(ilstValue)
                        });
                    }
                    updatedLstValues[updatedListLength - 1].push(lstValue);
                }
                returnValue.lstValues = updatedLstValues;
                console.log(updatedLstValues);
                currWrapper.push.apply(currWrapper,returnValue.lstValues);
                component.set("v.wrapper.lstValues",currWrapper );
                if(returnValue.lstValues.length<50|| returnValue.lstValues.length==0)
                    component.set('v.buttonDisabled',true);
                component.set("v.spinner",false);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },

    /**
     * @description Ben Climan | User Story US74880. Added this method to identify and render HTML content
     * @param       {string} valToTest String to test
     * @returns     {boolean} True if current value is HTML content
     */ 
    isHtmlContent: function(valToTest)
    {
        return /<.+(?=(\/>|.*<))/.test(valToTest);
    }
})