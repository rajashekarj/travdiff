({
      afterRender : function( component, helper ) {
        
        this.superAfterRender();
        var didScroll = false;
        
        
        window.onscroll = function() {
            //component.set("v.spinner",true);
            didScroll = true;
        }; 
        
        
        var scrollCheckIntervalId = setInterval( $A.getCallback( function() {
            //fix for sorting issue (US79225)
            if ( didScroll && component.isValid() ) {
                didScroll = false;
                if ( window['scrollY'] >= document.body['scrollHeight'] - window['outerHeight'] - 100 ) {
                   component.set("v.spinner",true);
                   helper.loadMore(component,helper);
                }
            }
            
        }), 2000 );
        
        component.set( 'v.scrollCheckIntervalId', scrollCheckIntervalId );
        var device = $A.get("$Browser.formFactor");
        if(device==='PHONE')
        {
            
        }
        
    },
    
    unrender : function( component, helper ) {
        
        this.superUnrender();
        
        var scrollCheckIntervalId = component.get( 'v.scrollCheckIntervalId' );
        
        if ( !$A.util.isUndefinedOrNull( scrollCheckIntervalId ) ) {
            window.clearInterval( scrollCheckIntervalId );
        }
        
    }
})