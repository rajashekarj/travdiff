({
    doInit : function(component, event, helper) {
        component.set("v.spinner",true);
        /*var pageReference = component.get("v.pageReference");
        console.log(pageReference);
        component.set("v.recordId", pageReference.state.c__recordId);
        component.set("v.objectAPIName" , pageReference.state.c__objectAPIName);
        component.set("v.relatedListName" , pageReference.state.c__relatedListName);
        component.set("v.sort" , pageReference.state.c__sort);
        component.set("v.filter" , pageReference.state.c__filter);
        component.set("v.title" , pageReference.state.c__title);
        component.set("v.parentField" , pageReference.state.c__parentField);
        component.set("v.sObjectName" , pageReference.state.c__sObjectName);*/
        var object=component.get("v.objectAPIName");
        var columns; 
        var action= component.get('c.getData');
        action.setParams({
            objAPIName: object, 
            sortCriteria: component.get("v.sort"),
            filterCriteria: component.get("v.filter"),
            recordId: component.get("v.recordId"),
            parentLookup: component.get("v.parentField"),
            relatedListName:component.get("v.relatedListName"),
            pageNumber: component.get("v.pageNumber")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var returnValue = response.getReturnValue();
                console.log('Before');
                console.log(returnValue.lstValues.length);
                // Ben Climan | User Story US74880. Call new helper file method to identify and render HTML content
                var updatedLstValues = [];
                for (var lstValue of returnValue.lstValues) {
                    var updatedListLength = updatedLstValues.push([]);
                    for (var ilstValue of lstValue) {
                        updatedLstValues[updatedListLength - 1].push({
                            lstVal: ilstValue,
                            isHtml: helper.isHtmlContent(ilstValue)
                        });
                    }
                    updatedLstValues[updatedListLength - 1].push(lstValue);
                }
                returnValue.lstValues = updatedLstValues;
                console.log('After');
                component.set("v.wrapper", returnValue);
				
                console.log(returnValue.lstValues.length);
                if(returnValue.lstValues.length<50|| returnValue.lstValues.length==0) 
                    component.set('v.buttonDisabled',true);
                    console.log('spinner');
                    component.set("v.spinner",false);
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
        var action1= component.get('c.getUserProfile');
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.userProfile", response.getReturnValue());
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action1);
        var action2 =component.get('c.getParentRecord');
        action2.setParams({
            recordId: component.get("v.recordId"),
            sOName: component.get("v.sObjectName")
        });
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.accountName", response.getReturnValue());
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action2);
    },
    
      sortData:function(component,event,helper)
    {
        
        var val=event.currentTarget.getAttribute("data-row-index");
        //alert(event.currentTarget.getAttribute("data-row-index"));
        var columns= component.get("v.wrapper").lstColumnLabels;
        var fieldName= columns[val];
        var arrow= component.get("v.arrow");
        var tab= component.get("v.tab");
        var data= component.get("v.wrapper").lstValues;
        
        if(tab!==fieldName)
        {
            component.set("v.arrow",'up');
            component.set("v.tab", fieldName);
        }else if(tab===fieldName)
        {
            if(arrow!=='up')
            {
                component.set("v.arrow",'up');
            }else if(arrow==='up')
            {
                component.set("v.arrow",'down');
            }
        }
        var fieldAPIName= component.get("v.wrapper.lstColumnValues")[val];
        var sortCriteria= fieldAPIName + (component.get("v.arrow")==='up'?' ASC':' DESC');
        component.set("v.sort",sortCriteria);
        component.set("v.pageNumber",0);
        component.set("v.outerheight",0);
        component.set("v.spinner",true);
        console.log( component.get("v.outerheight"));
        var object=component.get("v.objectAPIName");
        var action= component.get('c.getData');
        action.setParams({
            objAPIName: object, 
            sortCriteria: component.get("v.sort"),
            filterCriteria: component.get("v.filter"),
            recordId: component.get("v.recordId"),
            parentLookup: component.get("v.parentField"),
            relatedListName:component.get("v.relatedListName"),
            pageNumber: component.get("v.pageNumber")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();          
		//fix for sorting issue (US79225)
		     if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var returnValue=response.getReturnValue();
                var updatedLstValues = [];
                for (var lstValue of returnValue.lstValues) {
                    var updatedListLength = updatedLstValues.push([]);
                    for (var ilstValue of lstValue) {
                        updatedLstValues[updatedListLength - 1].push({
                            lstVal: ilstValue,
                            isHtml: helper.isHtmlContent(ilstValue)
                        });
                    }
                    updatedLstValues[updatedListLength - 1].push(lstValue);
                }
                returnValue.lstValues = updatedLstValues;
                console.log(updatedLstValues);
                component.set("v.wrapper.lstValues",returnValue.lstValues );
                component.set("v.spinner",false);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
       
    backToAccount: function(component,event, helper)
    {
        var sObjectEvent = $A.get("e.force:navigateToSObject");
        sObjectEvent .setParams({
            "recordId": component.get("v.recordId")  ,
            "slideDevName": "detail"
        });
        sObjectEvent.fire(); 
    },
    handleSelect: function(component,event,helper)
    {
        var id= event.getSource().get("v.name");
        var selectedMenuItemValue = event.getParam("value");
        if(selectedMenuItemValue==='Edit'){
            var editRecordEvent = $A.get("e.force:editRecord");
            editRecordEvent.setParams({
                "recordId": id
            });
            editRecordEvent.fire();
        }else if(selectedMenuItemValue==='Delete')
        {
            component.set("v.deleteId",id);
            component.set("v.isOpen",true);
            
        }
    },
    
    goToRecord: function(component,event,helper)
    {
        var val=event.currentTarget.getAttribute("data-row-index");
        console.log(val.substring(val.lastIndexOf(',')+1));
        var sObjectEvent = $A.get("e.force:navigateToSObject");
        sObjectEvent .setParams({
            "recordId": val.substring(val.lastIndexOf(',')+1) ,
            "slideDevName": "detail"
        });
        sObjectEvent.fire(); 
    },
    calculateWidth : function(component, event, helper) {
        var childObj = event.target
        var parObj = childObj.parentNode;
        var count = 1;
        while(parObj.tagName != 'TH') {
            parObj = parObj.parentNode;
            count++;
        }
        var mouseStart=event.clientX;
        component.set("v.mouseStart",mouseStart);
        component.set("v.oldWidth",parObj.offsetWidth);
    },
    setNewWidth : function(component, event, helper) {
        var childObj = event.target
        var parObj = childObj.parentNode;
        var count = 1;
        while(parObj.tagName != 'TH') {
            parObj = parObj.parentNode;
            count++;
        }
        var mouseStart = component.get("v.mouseStart");
        var oldWidth = component.get("v.oldWidth");
        var newWidth = event.clientX- parseFloat(mouseStart)+parseFloat(oldWidth);
        parObj.style.width = newWidth+'px';
    },
    
    closeModel: function(component,event,helper)
    {
        component.set("v.isOpen",false);
    },
    
    deleteRec: function(component,event,helper)
    {
        var id=component.get("v.deleteId");
        var action= component.get("c.deleteRecord");
        action.setParams({
            recordId: id
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.isOpen",false);
                alert('Record Deleted');
                $A.get('e.force:refreshView').fire();
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    refresh: function(component,event,helper)
    {
        $A.get('e.force:refreshView').fire();
    },
    
    LoadMore: function(component,event,helper)
    {
        component.set("v.spinner", true);
        helper.loadMore(component,helper);
    },
    
    backToAccountHome: function(component,event,helper)
    {
     console.log('back home');   
    var homeEvent = $A.get("e.force:navigateToObjectHome");
    homeEvent.setParams({
        "scope": component.get("v.sObjectName")
    });
    homeEvent.fire();

    }
})