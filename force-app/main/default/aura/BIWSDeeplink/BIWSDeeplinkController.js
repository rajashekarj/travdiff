({
    copyAccountText : function(component, event, helper) {
        // get HTML hardcore value using aura:id
        var textForCopy = component.find('accountLinkId').getElement().href;
        // calling common helper class to copy selected text value
        helper.copyTextHelper(component,event,textForCopy);
    },
    copySearchText : function(component, event, helper) {
        // get HTML hardcore value using aura:id
        var textForCopy = component.find('searchLinkId').getElement().innerHTML;
        // calling common helper class to copy selected text value
        helper.copyTextHelper(component,event,textForCopy);
    },
    
    copyInputFieldValue : function(component, event, helper) {
        // get lightning:textarea field value using aura:id
        var textForCopy = component.find('inputF').get("v.value");
        // calling common helper class to copy selected text value
        helper.copyTextHelper(component,event,textForCopy);
    },
	assignData : function(component, event, helper) {
        helper.getInfo(component,event);
    }
    
})