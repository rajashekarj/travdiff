({
  copyTextHelper: function(component, event, text) {
    // Create an hidden input
    var hiddenInput = document.createElement("input");
    // passed text into the input
    hiddenInput.setAttribute("value", text);
    // Append the hiddenInput input to the body
    document.body.appendChild(hiddenInput);
    // select the content
    hiddenInput.select();
    // Execute the copy command
    document.execCommand("copy");
    // Remove the input from the body after copy text
    document.body.removeChild(hiddenInput);
    // store target button label value
    var orignalLabel = event.getSource().get("v.label");
    // change button icon after copy text
    event.getSource().set("v.iconName", "utility:check");
    // change button label with 'copied' after copy text
    event.getSource().set("v.label", "Copied");

    // set timeout to reset icon and label value after 700 milliseconds
    setTimeout(function() {
      event.getSource().set("v.iconName", "utility:copy_to_clipboard");
      event.getSource().set("v.label", orignalLabel);
    }, 700);
  },
  getInfo: function(component, event) {
    var action = component.get("c.getInfo");
    action.setParams({
      id: component.get("v.recordId")
    });
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        component.set("v.info", response.getReturnValue());
        var info = component.get("v.info");
        if(info[0] != 'no value'){
        	component.set("v.sai", info[0]);
        } else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "ATTENTION: There is no SAI for this Opportunity.",
                message: "Please go to BI Workstation through other means.",
                type: "warning",
                duration: 10000
            });
            toastEvent.fire();
            $A.get("e.force:closeQuickAction").fire();
        	$A.get('e.force:refreshView').fire()
            component.set("v.sai", "");
        }
        if(info[1] != 'no value'){
        	component.set("v.ibu", info[1]);
        } else {
            component.set("v.ibu", "51");
        }
        component.set("v.accountLink", info[2] + '/poc?uri=deeplink:link&json={"pageName":"trav.bi.om.accounts.accountProfile.pg.hidden","portlets":[{"name":"trav.bi.om.accountWorkspace.pt","state":"normal"}],"params":[{"name":"releaseNumber","value":"18-2-0"},{"name":"sai","value":"' + component.get("v.sai") + '"},{"name":"ibu","value":"' + component.get("v.ibu") + '"},{"name":"windowState","value":"normal"}]}');
        component.set("v.displayLink", info[2] + '/poc?uri=deeplink...');
      }
    });
    $A.enqueueAction(action);
  },
});