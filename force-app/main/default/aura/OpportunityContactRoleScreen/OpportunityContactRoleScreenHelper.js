({
    //function to handle the Record form load for Opportunity and Account fields
    handleOnLoad : function(component,event) {
        console.log("--" + JSON.stringify(event.getParam("recordUi")));
        //setting account id value
        var response = event.getParam("recordUi");
        if(response.record.fields.Account != undefined){
            if(component.get("v.contactRecordType") == "TRAV_Agency_Non_Licensed_Contact;TRAV_Agency_Licensed_Contact"){
                component.set("v.accountId",response.record.fields.TRAV_Agency_Broker__c.value);
                component.set("v.accountLabel","Agency Location");
                
            }
            else{
                component.set("v.accountId",response.record.fields.Account.value.fields.Id.value);
            }
        }
        else{
            component.set("v.accountId","");
        }
    },
    
    //funtion to retrieve role picklist values
    fetchRoleValues : function(component,event) {
        var action = component.get('c.fetchOpportunityContactRoles');
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var listRoles = [];
                for(var key in result){
                    listRoles.push({key: key, value: result[key]});
                }
                component.set("v.listRoles", listRoles);
            }
        });
        $A.enqueueAction(action);
    },
    
    //function to save Opportunity Contact role record
    addOpportunityContactRole : function(component,event) {
        //setting selected contact record id
        component.set("v.contactId",component.get("v.selectedRecord").val);
        
        var action = component.get('c.createContactRole');
        action.setParams({
            strOpportunityId : component.get("v.opportunityId"),
            strContactId : component.get("v.contactId"),
            strRoleName : component.get("v.selectedRole"),
            boolIsPrimary : component.get("v.isPrimary")
        });
        
        action.setCallback(this,function(response){
            console.log('response ' + JSON.stringify(response.getError()));
            var state = response.getState();
            
            var message = response.getReturnValue();
            if(state == 'SUCCESS' && message === ''){
                console.log('Inserted record');
                //toast message for successful insert
                var toast = $A.get("e.force:showToast");
                if(toast){
                    toast.setParams({
                        "title":"Success",
                        "type":"Success",
                        "message":"Opportunity contact role successfully added."
                    });
                }
                toast.fire();
                component.set("v.isOpen", false);
            }
            else if(state == 'SUCCESS' && message === 'Updates to this Opportunity must be made by a user from the respective Business Unit') {
                var toast = $A.get("e.force:showToast");
                if(toast){
                    toast.setParams({
                        "title":"Error",
                        "type":"Error",
                        "message":"Updates to this Opportunity must be made by a user from the respective Business Unit"
                    });
                }
                toast.fire();
                component.set("v.isOpen", true);
            }
            else if(state == 'ERROR'){
                //toast message for error
                var toast = $A.get("e.force:showToast");
                if(toast){
                    toast.setParams({
                        "title":"Error",
                        "type":"Error",
                        "message":"An error occurred."
                    });
                }
                toast.fire();
            }
            
        });
        $A.enqueueAction(action);
    }
})