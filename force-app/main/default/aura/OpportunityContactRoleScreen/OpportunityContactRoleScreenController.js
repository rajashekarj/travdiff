({
    //function to handle the Record form load for Opportunity and Account fields
    handleOnLoad: function(component, event, helper) {
        helper.handleOnLoad(component, event);
        //retrieving role picklist values
        helper.fetchRoleValues(component, event);
    },
    
    //function to hide modal on close
    closeModal: function(component, event, helper) {
        component.set("v.isOpen", false);
    },
    
    //function to fire on Save Modal
    saveAndCloseModal: function(component, event, helper) {
        console.log("--" + JSON.stringify(component.get("v.selectedRecord")));
        //function to save Opportunity Contact role record
        helper.addOpportunityContactRole(component, event);
    }
})