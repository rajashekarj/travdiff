({
    //function to set the selected record
    recordSelected : function(component, event, helper) {
        helper.recordSelected(component, event, helper);
    },  
    //function to search records based on input
    searchRecords :  function(component, event, helper) {
        helper.searchRecords(component, event, helper);
    },
    //function to remove the selected record
    clearSelection : function(component, event, helper){
        helper.clearSelection(component, event, helper);
    },
    //function to launch create new contact modal
    createNewContact : function(component, event, helper){
        helper.createNewContact(component, event, helper);
    }
    
})