({
    selectCarrier : function(component, event, helper){      
        // get the selected Carrier from list  
        var getSelectCarrier = component.get("v.oCarrier");
        component.set("v.selectedRecord",getSelectCarrier);
        component.set("v.selectedId",getSelectCarrier.Id);
        component.set("v.lookupError", false);
        console.log('**VALUE IN LKP RESULT**'+component.get("v.lookupError"));
        // call the event   
        var compEvent = component.getEvent("oSelectedCarrierEvent");
        // set the Selected Carrier to the event attribute.  
        compEvent.setParams({"carrierByEvent" : getSelectCarrier });  
        // fire the event  
        compEvent.fire();
    },
})