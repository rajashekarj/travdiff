({
	doInit : function(component,event) { 
		var action = component.get("c.getAllDetails");
        action.setParams({
            "recordId" : component.get("v.recordId"),
            "sObjectName" : component.get("v.sObject"),
            "fields" : component.get("v.fields")
        });
        action.setCallback(this, function(response) {
            var popUpWrapperValue = response.getReturnValue();
            component.set("v.displayPopup",popUpWrapperValue.loadPopUpScreen);
            component.set("v.popUpHeader",popUpWrapperValue.headerValue);
            component.set("v.columnConfig",popUpWrapperValue.columnList);
            component.set("v.productConfig",popUpWrapperValue.listOpportunityProducts);
            component.set("v.Pwrapper",popUpWrapperValue);
            console.log(JSON.stringify(popUpWrapperValue.listOpportunityProducts));
    
            var opportunityInputCmp = component.find("opportunityInput");
            if(opportunityInputCmp != null) {
                for(var i=0;i < opportunityInputCmp.length;i++){
                    if( opportunityInputCmp[i].get("v.fieldName") == 'TRAV_Prospect_Effective_Date__c'){
                       opportunityInputCmp[i].set("v.value",popUpWrapperValue.opportunityRecord.TRAV_Prospect_Effective_Date__c); 
                    }
                }
            }
        });
        $A.enqueueAction(action);
	},
    closeModal : function(component, event) {
        var action = component.get("c.revertStageUpdate");
        action.setParams({"recordId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            component.set("v.displayPopup", false);
            $A.get('e.force:refreshView').fire();
        });
        
        $A.enqueueAction(action);
        
    }
})