({
    doInit: function(component, event, helper) {
        helper.doInit(component,event);
    },
    closeModal : function(component, event, helper) {
        helper.closeModal(component,event);
        
    },
    updateRefersh : function(component, event, helper) {
        var changeType = event.getParams().changeType;
        helper.doInit(component,event);
    },
    handleOnError : function(component, event, helper) {
        var errors = event.getParam("detail");
        console.log('Some error occurred'+errors);
        component.set("v.errorOnSubmit","true");
    },
    handleOnSuccess : function(component, event, helper) {
        var datdetails = component.get("v.Pwrapper").dataDetails;
        if(datdetails != null){                
            var action = component.get("c.saveProductDetails");
            action.setParams({
                "lstPopupDataWrapper" : datdetails,
                "idOpptyRec" : component.get("v.recordId")
            });
            action.setCallback(this,function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
				var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Success',
                        message: 'Record Updated Successfully',
                        type: 'success',
                        mode : 'sticky'
                    });
                    toastEvent.fire();
                    component.set("v.displayPopup", false);
                }
                
            });
            $A.enqueueAction(action);
            
        }else{
            var action = component.get("c.updateProspectCreationDate");
            action.setParams({
                "recordId" : component.get("v.recordId")
            });
            action.setCallback(this,function(response){
                component.set("v.displayPopup", false);
                
                
            });
            $A.enqueueAction(action);
        }
        
        
    },
    submitDetails : function(component, event, helper) {
        console.log('fields'+event.getParam('fields'));
        
        var datdetails = component.get("v.Pwrapper").dataDetails;
        var errorMessage = '';
        var hasError = false;
        var samestageError = '';
        var columnconfig = component.get("v.columnConfig");
        var outstandingvalue = '';
        if(columnconfig != null) {
            
            for(var i=0;i < columnconfig.length;i++){
                if(columnconfig[i].required && columnconfig[i].sObjectName == component.get("v.sObject") 
                   && (columnconfig[i].opportunityValue == '' || columnconfig[i].opportunityValue == null)){
                    hasError = true;
                    errorMessage = columnconfig[i].validationErrorMessage;
                    console.log('error1'+JSON.stringify(columnconfig[i]));
                    break;
                }else {
                    hasError = false;
                    errorMessage = '';
                }
                if(columnconfig[i].apiName == 'TRAV_Outstanding_Items_Indicator__c' 
                   && columnconfig[i].opportunityValue == 'Yes'){
                    outstandingvalue = columnconfig[i].sameStageError;
                }
                if(columnconfig[i].apiName == 'TRAV_Requested__c' && outstandingvalue != '' && 
                   (columnconfig[i].opportunityValue == null || columnconfig[i].opportunityValue == '')){
                    hasError = true;
                    errorMessage = outstandingvalue;
                    outstandingvalue = '';
                    console.log('error2');
                    break;
                }else {
                    hasError = false;
                    errorMessage = '';
                }
                
                //validating close description when ooa reason is other
                if(columnconfig[i].apiName == "TRAV_Out_of_Appetite_Reason__c" 
                   && columnconfig[i].opportunityValue == 'Other'){
                    outstandingvalue = columnconfig[i].sameStageError;
                }
                if(columnconfig[i].apiName == "TRAV_Close_Description__c" && outstandingvalue != '' && 
                   (columnconfig[i].opportunityValue == null || columnconfig[i].opportunityValue == '')){
                    hasError = true;
                    errorMessage = outstandingvalue;
                    outstandingvalue = '';
                    console.log('error3');
                    break;
                }else {
                    hasError = false;
                    errorMessage = '';
                }
            }
        }
        //Save OpportunityLineItems if present
        if(datdetails !== undefined){
            if(datdetails.length > 0){
                console.log('Data Details'+JSON.stringify(datdetails));
                var stagesRequireSameStage = component.get("v.Pwrapper").stagesRequireSameStage;
                var oppStage = component.get("v.Pwrapper").opportunityRecord.StageName;
                var stageRequireReason = component.get("v.Pwrapper").stagesRequireReason;
                var prodWinningError = component.get("v.Pwrapper").productWinningError;
                var sameCountCheck = 0; 
                //map for product stages
                var mapProduct = new Map();
                for(var i=0;i < datdetails.length;i++){
                    for(var j=0;j < datdetails[i].length;j++){
                        if(datdetails[i][j].sfField == 'TRAV_Stage__c' ){
                            mapProduct.set(datdetails[i][j].sfOpportunityProductId,datdetails[i][j].inpValue);
                        }
                    }
                }
				//validations 
                for(var i=0;i < datdetails.length;i++){
                    for(var j=0;j < datdetails[i].length;j++){
                        //validating anyfield marked as required in metadata
                        if(datdetails[i][j].required){ 
                            if( (datdetails[i][j].inpValue == null || datdetails[i][j].inpValue == "" || datdetails[i][j].inpValue == " ") && 
                               datdetails[i][j].columnType != 'Multi-Select'){
                                errorMessage = datdetails[i][j].validationMessage;
                                hasError = true;
                                console.log('error4');
                                break;
                            }
                            var prodStage = mapProduct.get(datdetails[i][j].sfOpportunityProductId);
                            if(datdetails[i][j].sfField == 'TRAV_Closed_Reason__c' && stageRequireReason.includes(prodStage)) {
                                let opp = component.get("v.Pwrapper").opportunityRecord;
                                if (opp.TRAV_Marketing_Conditions__c === 'Not Being Marketed'
                                && opp.RecordType.Name === 'Renewal NA'
                                && prodStage === 'Written'
                                && opp.StageName === 'Bound') {
                                    hasError = false;
                                    errorMessage = '';
                                    break;
                                }
                                console.log('**CLOSE REASON LIST**'+JSON.stringify(datdetails[i][j].closeReasonList));
                                if(datdetails[i][j].closeReasonList == null || datdetails[i][j].closeReasonList.length == 0) {
                                    hasError = true; 
                                    errorMessage = datdetails[i][j].validationMessage +' '+prodStage;
                                    break;
                                } else {
                                    var countCheck = 0;
                                    for(var k=0;k < datdetails[i][j].closeReasonList.length;k++){
                                        if(datdetails[i][j].closeReasonList[k].reasonChecked && 
                                           (datdetails[i][j].closeReasonList[k].reasonValue != null && 
                                            datdetails[i][j].closeReasonList[k].reasonValue != "" && 
                                            datdetails[i][j].closeReasonList[k].reasonValue != " ")){
                                            countCheck++; 
                                        }
                                    }
                                    if(countCheck == 0){
                                        hasError = true; 
                                        errorMessage = datdetails[i][j].validationMessage + ' ' + prodStage;
                                        break;
                                    }
                                }
                            }
                        }
                        
                        //validating stage should has the same stage of current opportunity stage
                        console.log('stf2'+datdetails[i][j].sfField+datdetails[i][j].sameStageError+stagesRequireSameStage);
                        if(datdetails[i][j].sfField == 'TRAV_Stage__c' && 
                           datdetails[i][j].sameStageError != null && 
                           stagesRequireSameStage.includes(oppStage)){ 
                            console.log('stf1'+oppStage);
                            
                            if(oppStage == 'Bound') {
                                console.log('stf'+datdetails[i][j].inpValue);
                                if(datdetails[i][j].inpValue == 'Written'){
                                    sameCountCheck++;
                                    
                                } 
                            }else {
                                if(datdetails[i][j].inpValue == oppStage){
                                    sameCountCheck++;
                                    
                                }  
                            }
                            
                            if(sameCountCheck == 0) {
                                // hasError = true; 
                                if(oppStage == 'Bound') {
                                    samestageError = datdetails[i][j].sameStageError.replace("<ReplaceStage>", "Written"); 
                                    samestageError = samestageError.replace("this", oppStage);
                                    console.log('Same stage error'+samestageError);
                                    
                                }else{
                                    samestageError = datdetails[i][j].sameStageError.replace("<ReplaceStage>", oppStage);   
                                    
                                }
                            }
                        }
                        
                        var prodStageForWinning = mapProduct.get(datdetails[i][j].sfOpportunityProductId);
                        if(datdetails[i][j].sfField == "TRAV_Product_Winning_New_Carrier__c" && 
                           prodStageForWinning == 'Quote Unsuccessful' && 
                           datdetails[i][j].selectedLookupRecord == null) {
                            hasError = true;
                            errorMessage = prodWinningError;
                        }
                        
                    }
                } 
                if(!hasError && sameCountCheck == 0 && oppStage != "Deferred" && oppStage != "Quote Unsuccessful" ) {
                    hasError = true;
                    console.log('error7');
                    errorMessage = samestageError;
                }
            }
        }
       /* if(component.get("v.lookupError")){
            hasError = true;
            console.log('error8');
            errorMessage = 'Product Winning New Carrier is required when a product is marked as quote unsuccessful.';
        }
        console.log('has error value'+hasError);*/
        
        if(hasError){
            
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error',
                message:errorMessage,
                type: 'error',
                mode : 'sticky'
            });
            toastEvent.fire();
        } else {
            //Saving part
            component.find("opportunityForm").submit();
            
            console.log('Data Details'+JSON.stringify(datdetails));
            //console.log('Full Details'+JSON.stringify(component.get("v.Pwrapper")));
            
        }
        
    },
    
})