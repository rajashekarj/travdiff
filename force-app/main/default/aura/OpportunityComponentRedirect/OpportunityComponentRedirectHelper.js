({
   getParameterByName: function(component,event, name) {
        name = name.replace(/[\[\]]/g, "\\$&");
        console.log('name is'+name);
        var url = window.location.href;
        console.log('Url is'+url);
        var regex = new RegExp("[?&]" + name + "(=1\.([^&#]*)|&|#|$)");
        console.log('regex is'+regex);
        var results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
})