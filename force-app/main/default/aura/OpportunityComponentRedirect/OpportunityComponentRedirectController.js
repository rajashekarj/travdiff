({
    doInit : function(component, event, helper) {
        var parentId='';
        
        try
        {
            var value = helper.getParameterByName(component , event, 'inContextOfRef');
            var context = JSON.parse(window.atob(value));
            parentId =context.attributes.recordId; 
        }catch(exceptionmessage)
        {
            console.error(exceptionmessage);
        }
        var varrecordTypeId = component.get("v.pageReference").state.recordTypeId;
        var createRecordEvent = $A.get("e.force:createRecord");
        var str = '001';
        
        if(parentId == ""){
            createRecordEvent.setParams({
                "entityApiName": "Opportunity",
                "defaultFieldValues" :{
                    "Name" : "Opportunity Name",
                    "RecordTypeId"  : varrecordTypeId,
                    "CloseDate" : $A.localizationService.formatDate(new Date(), "YYYY-MM-DD")
                } 
            });
        }else if(parentId!=null && parentId.includes(str)){
            createRecordEvent.setParams({
                "entityApiName": "Opportunity",
                "defaultFieldValues" :{
                    "Name" : "Opportunity Name",
                    "RecordTypeId"  : varrecordTypeId,
                    "AccountId" : parentId,
                    "CloseDate" : $A.localizationService.formatDate(new Date(), "YYYY-MM-DD")
                }
            });
        }
        else{
            createRecordEvent.setParams({
                "entityApiName": "Opportunity",
                "defaultFieldValues" :{
                    "Name" : "Opportunity Name",
                    "RecordTypeId"  : varrecordTypeId,
                    "CloseDate" : $A.localizationService.formatDate(new Date(), "YYYY-MM-DD")
                }
            });
        }
        createRecordEvent.fire();
    }
})