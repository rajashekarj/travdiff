({
    doInit : function(component, event, helper) {
	 //get empty options in case of blank stage value
    	console.log('inside the CloseReasonMultiSelectWrapper doINit');
	    var dataInd = component.get("v.indexD");
		var prdStage = component.get("v.stage");
        console.log('value of the stage is'+prdStage);
        var valueOnLoad = [];
        var carrierOnLoad = component.get("v.selectedRecordOnLoad");
		var stage =component.get("v.OptyStage");
		var multiProd = component.get("v.multipleProducts");
        //Product default check

        //assign blank close reason if product stage is null
        if(prdStage == '' || prdStage == undefined){
			component.find("closeRs").set("v.options",null);
        }
        if(prdStage == 'Written' || prdStage == 'Bound' ){
            component.set("v.isParentSS",true);
            var cmpEvent = component.getEvent("cmpEvent"); 
            cmpEvent.setParams({"writtenIndexStr" : dataInd,"addition" : true, "forBoundValidation" : true}); 
            cmpEvent.fire();
            var action = component.get("c.getStageDependentReasons");
            action.setParams({ "productStage" : component.find("productStages").get("v.value") });
            // Opportunity Details callback function
            action.setCallback(this, function(response) {
                
                var reasonList = response.getReturnValue();
                if(reasonList != null && reasonList != undefined){
                    
                    reasonList.sort((a, b) => (a.reasonValue > b.reasonValue) ? 1 : -1);
                    component.set("v.options",reasonList);
                    var closeReasonVariable = component.find("closeRs");
                    if(closeReasonVariable != null) {
                        component.find("closeRs").set("v.options",reasonList);
                    }
                    var closeSubRsVariable = component.find("closeSubRs");
                    if(closeSubRsVariable != null) {
                        component.find("closeSubRs").set("v.options",reasonList);
                    }

                    console.log('inside the reasonlist'+reasonList);
                }
            });
            $A.enqueueAction(action);
        }else{
            component.set("v.isParentSS",false);
        }
        if(prdStage == "Quote Unsuccessful" && component.get("v.selectedItems") == 'Pricing'){
             component.set("v.isChildSS",true);
        }else{
            console.log('is inside the Loop');
             component.set("v.isChildSS",false);
        }
        //changes for defaulting
        var OptyStg = component.get("v.OptyStage");
        console.log('Opportunity Stage is'+OptyStg + 'multi'+multiProd);
		if(OptyStg == 'Written'  && multiProd == false ){   
            component.set("v.stage",'Written');
		} 
        if(OptyStg == 'Declined'){
            component.set("v.stage",'Declined');
        }
        if(OptyStg == 'Lost'){
            component.set("v.stage",'Lost');
		}
        if(OptyStg == 'Quote Unsuccessful'){
            component.set("v.stage",'Quote Unsuccessful');

		}
        //Changes for US69890 starts
        if(OptyStg == 'Deferred'){
            component.set("v.stage",'Deferred');

		}
        if(OptyStg == 'Out of Appetite'){
            component.set("v.stage",'Out of Appetite');

		}
        //Changes for US69890 end
		//Validation for product Stage
        if(prdStage === '' || prdStage == undefined ){
            var cmpEvent = component.getEvent("cmpEvent"); 
            if(cmpEvent != null){
                cmpEvent.setParams({"productStageIndexStr" : dataInd,"productStageAddition" : true}); 
                cmpEvent.fire(); 
            }
        }else{
            var cmpEvent = component.getEvent("cmpEvent"); 
            if(cmpEvent != null){
                cmpEvent.setParams({"productStageIndexStr" : dataInd,"productStageAddition" : false}); 
                cmpEvent.fire(); 
            }
        }

		},
    closeDropdowns : function(component, event, helper) {
        //closed section
    },
    validateStage : function(component, event, helper) {
        component.set("v.selectedItems",'');
        component.set("v.selectedSubItems",'');
        //Reset other description
        component.set("v.showOtherComments",false);
		var selectedList = [];
		var stgId = component.find("productStages");
        console.log('stagePassed>>'+stagePassed);
		var stagePassed = component.get("v.stage");
		 if(stagePassed != '' && stagePassed != null && stagePassed != undefined){
            if(stgId != null && stgId != '' && stgId != undefined){
                var stgVal = stgId.get("v.value");
            }
        }else{
            var stgVal = '';
        }

        var dataInd = component.get("v.indexD");
		var objField = component.find("carrierError");
		if((component.get("v.OptyStage") == "Bound" || component.get("v.OptyStage") == "Written" || component.get("v.OptyStage") == "Declined") && (!component.get("v.multipleProducts"))){
			if(component.get("v.OptyStage") == "Declined"){
				stgVal = "Declined";
            }
            else{
                
            //	stgVal = "Written";    
            }
            
        }
		var action = component.get("c.getStageDependentReasons");
		action.setParams({ "productStage" : component.find("productStages").get("v.value") });
		// Opportunity Details callback function
        action.setCallback(this, function(response) {
             
            var reasonList = response.getReturnValue();
			if(reasonList != null && reasonList != undefined){
                
                reasonList.sort((a, b) => (a.reasonValue > b.reasonValue) ? 1 : -1);
				component.set("v.options",reasonList);
                var closeReasonVariable = component.find("closeRs");
                if(closeReasonVariable != null) {
                    component.find("closeRs").set("v.options",reasonList);
                }
                var closeSubRsVariable = component.find("closeSubRs");
                if(closeSubRsVariable != null) {
                    component.find("closeSubRs").set("v.options",reasonList);
                }
			}
        });
        $A.enqueueAction(action);
		//Single Select Setting
		console.log('stgval'+component.find("productStages").get("v.value"));
        if(component.find("productStages").get("v.value") == 'Written'){
            component.set("v.isParentSS",true);
        }else{
			component.set("v.isParentSS",false);
        }
        if(component.find("productStages").get("v.value") == "Quote Unsuccessful" && component.get("v.selectedItems") == 'Pricing'){
             component.set("v.isChildSS",true);
        }else{
             component.set("v.isChildSS",false);
        }
        /**/
        var prodStgVal = component.find("productStages");
		if(prodStgVal != undefined){
            if((component.find("productStages").get("v.value") == 'Quote Unsuccessful') && component.get("v.carrierCurrVal") == null){
				$A.util.addClass(objField, 'displayBlock');
                $A.util.removeClass(objField, 'displayNone');
                var cmpEvent = component.getEvent("cmpEvent"); 
                cmpEvent.setParams({"errorIndexStr" : dataInd,"add" : true}); 
                cmpEvent.fire(); 
                
            }else{
                $A.util.removeClass(objField, 'displayBlock');
                $A.util.addClass(objField, 'displayNone');
                var cmpEvent = component.getEvent("cmpEvent"); 
                cmpEvent.setParams({"errorIndexStr" : dataInd,"add" : false}); 
                cmpEvent.fire(); 
                
            }
        }
        //validation for written in bound stage
        var isBound = component.get("v.isBoundStage");
        if(isBound){
            if(stgVal == 'Written'){
                var cmpEvent = component.getEvent("cmpEvent"); 
                cmpEvent.setParams({"writtenIndexStr" : dataInd,"addition" : true, "forBoundValidation" : true}); 
            	cmpEvent.fire();
            } else{
                var cmpEvent = component.getEvent("cmpEvent"); 
                cmpEvent.setParams({"writtenIndexStr" : dataInd,"addition" : false, "forBoundValidation" : true}); 
            	cmpEvent.fire();
            } 
        }
        //Changes for US69890 starts
        var OverallStage = component.get("v.OverallStage");
       // component.set("v.OOACount",component.get("v.stageCount")); 
        var OOAStageCount = component.get("v.stageCount");
        if(OverallStage == 'Out of Appetite') {
           if(stgVal != 'Out of Appetite'){
            OOAStageCount = OOAStageCount - 1;
            component.set("v.stageCount",OOAStageCount); 
            var cmpEvent = component.getEvent("cmpEvent"); 
            cmpEvent.setParams({"OutOfAppetiteIndexStr" : OOAStageCount}); 
            cmpEvent.fire();
           }  else {
                OOAStageCount = OOAStageCount + 1;
            	component.set("v.stageCount",OOAStageCount); 
               var cmpEvent = component.getEvent("cmpEvent"); 
               cmpEvent.setParams({"OutOfAppetiteIndexStr" : OOAStageCount}); 
               cmpEvent.fire();
           }
        }
        
        //Changes for US69890 end
		//Validation for product Stage
        if(stgVal === ''){
			var cmpEvent = component.getEvent("cmpEvent"); 
            cmpEvent.setParams({"productStageIndexStr" : dataInd,"productStageAddition" : true}); 
            cmpEvent.fire(); 
        }else{
            var cmpEvent = component.getEvent("cmpEvent"); 
            cmpEvent.setParams({"productStageIndexStr" : dataInd,"productStageAddition" : false}); 
            cmpEvent.fire(); 
        }
		
    },
    parentPress : function(component, event, helper) {
        var dataInd = component.get("v.indexD");
		var objChild = component.find('LKP');
        var objField = component.find("carrierError");
        component.set("v.carrierCurrVal",objChild.get('v.selectedRecord'));
		var currentStage = component.find("productStages").get("v.value");
		if(currentStage === null || currentStage === '' || currentStage === undefined){
			currentStage = component.get("v.OptyStage");
        }
        if((currentStage == 'Quote Unsuccessful') && objChild.get('v.selectedRecord') == null){
            $A.util.addClass(objField, 'displayBlock');
            $A.util.removeClass(objField, 'displayNone');
            var cmpEvent = component.getEvent("cmpEvent"); 
            cmpEvent.setParams({"errorIndexStr" : dataInd,"add" : true}); 
            cmpEvent.fire();
            
        }else{
            $A.util.removeClass(objField, 'displayBlock');
            $A.util.addClass(objField, 'displayNone');
            var cmpEvent = component.getEvent("cmpEvent"); 
            cmpEvent.setParams({"errorIndexStr" : dataInd,"add" : false}); 
            cmpEvent.fire();
            
        }
    },
    handleReasonChange: function (component, event, helper) {
        //Get the Selected reason values
        
    },
    handleValueOnLoad : function (component, event, helper){
        var valueOnLoad = [];
        var carrierOnLoad = component.get("v.selectedRecordOnLoad");
        
    },validateCloseReason: function (component, event, helper){
        //do nothing, this will be bubbled to parent
        
    }
    
})