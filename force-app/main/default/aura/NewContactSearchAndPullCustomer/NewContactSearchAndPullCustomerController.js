({
    doInit : function(component){
        component.set("v.isOpen", true);
        component.set("v.isCreateScreenOpen", true);
        console.log('-----here2');
        console.log(component.get("v.parentFieldId"));
        var recordType = component.get("v.recordTypeName");
        if(recordType == 'Customer_Contact'){
            component.set("v.recordType", 'Customer Contact');
        }
        else if(recordType == 'TRAV_TPA'){
            component.set("v.recordType", 'TPA Contact');
        }
            else{
                component.set("v.recordType", 'Agency Location Non Licensed Contact');
            }
    },
    saveRecord : function(component, event, helper) {
        console.log('----here 3' );
        var isFormValidate = helper.validateForm(component, event, helper);
        if(isFormValidate){
            var recDetails = component.get("v.contactRec");
            var accId = '';
            if(component.get("v.contactRec").AccountId != undefined){
                accId = component.get("v.contactRec").AccountId.val;
            }
            var recordId = component.get("v.recordId");
            var parentFieldId = component.get("v.parentFieldId");
            if(recordId == undefined || recordId == ""){
                recordId = component.get("v.parentFieldId");
            }
            var action = component.get("c.saveContact");
            action.setParams({
                contactRec : recDetails,
                recordId : recordId,
                accId : accId,
                recordType : component.get("v.recordTypeName")
            });
            action.setCallback(this, function(response){
                console.log('---response--'+JSON.stringify(response.getError()));
                var data = response.getReturnValue();
                if(response.getState() == 'SUCCESS'){
                    component.set("v.selectedRecord",data);
                    //Toast for Success
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "Success",
                        "title": "Success!",
                        "message": "Record Created Successfully"
                    });
                    toastEvent.fire();
                    
                    // close modal
                    component.set("v.isOpen",true);
                    component.set("v.isCreateScreenOpen", false);
                }
                else {
                    //toast message for error
                    var toast = $A.get("e.force:showToast");
                    if(toast){
                        toast.setParams({
                            "title":"Error",
                            "type":"Error",
                            "message":"An error occurred. Please contact your System Administrator for help." 
                        });
                    }
                    toast.fire();
                    component.set("v.isOpen",false);
                    component.set("v.isCreateScreenOpen", false);
                }
            });
            $A.enqueueAction(action);   
        }
    },
    
    closeModal : function(component, event, helper){
        component.set("v.errorAPI",false); //deprecated multipleRecords
        component.set("v.multipleRecords",false);
        component.set("v.isCreateScreenOpen", false);
        component.set("v.showNewForm",false);
        component.set("v.isOpen",true);
        
        component.set("v.newClicked",false);
    },
    showSpinner : function(component, event, helper){
        var spinner = component.find("loading");
        $A.util.addClass(spinner,"slds-show");
        $A.util.removeClass(spinner,"slds-hide");
    },
    hideSpinner : function(component, event, helper){
        var spinner = component.find("loading");		
        $A.util.addClass(spinner,"slds-hide");
        $A.util.removeClass(spinner,"slds-show");
    },
    setSelected : function(component, event, helper){
        var target = event.currentTarget;
        var selectedIndex = target.getAttribute("data-index");
        if(selectedIndex == component.get("v.selectedIndex")){
            component.set("v.contactSelected",false);
            component.set("v.selectedIndex",-1);   
        }
        else{
            component.set("v.contactSelected",true);
            component.set("v.selectedIndex",selectedIndex);
        }
    }
})