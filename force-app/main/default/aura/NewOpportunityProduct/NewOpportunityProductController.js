({ 
    
    // function call on component Load
    doInit: function(component, event, helper) {
        // create a Default RowItem [Contact Instance] on first time Component Load
        // by call this helper function 
        helper.loadUIConfig(component); 
        helper.createObjectData(component, event);
        
        var stage = component.get("v.opportunityStage");
        var bu = component.get("v.opportunityBU");
        var  BSIBU = ['BSI-FI','BSI-PNP','BSI-PCL','BSI-PL'];
        if(stage != 'Prospect' && BSIBU.includes(bu)){  
            $A.util.addClass(component.find('productBlock'), 'slds-hide');
        }else{
            $A.util.addClass(component.find('errorBlock'), 'slds-hide');
        }
        
        //Added for US69846
        helper.getExistingProducts(component, event);
    },
    
    // function for save the Records 
    Save: function(component, event, helper) {
        component.set("v.spinner", true);
        //alert('test>>'+helper.validateRequired(component, event));
        // first call the helper function in if block which will return true or false.
        // this helper function check the "Product2Id" will not be blank on each row.
        if (helper.validateRequired(component, event)) {
            // call the apex class method for save the OpportunityLineItems List
            //alert('test-->>');
            var action = component.get("c.saveOpportunityLineItems");
            var message = '';
            var addProductError = $A.get("$Label.c.TRAV_AddProductError");
            console.log(JSON.stringify(component.get("v.objectList")));
            action.setParams({
                "sObjectList": component.get("v.objectList"),
                "isBSIView": component.get("v.isBSIView")
            });
            // set call back 
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log('response>'+response.getReturnValue() +response.getState());
                message = response.getReturnValue();
                if (state === "SUCCESS") {
                    // if response if success then reset/blank the 'contactList' Attribute 
                    // and call the common helper method for create a default Object Data to Contact List 
                    component.set("v.objectList", []);
                    //Redirect to the opportunity detail page.
                    if(message.includes(addProductError) || message.includes("Updates to this Opportunity must be made by a user from the respective Business Unit.")) {
                        component.set("v.spinner",false);
                        component.set("v.isError",true);
                        component.set("v.exception",message);
                        helper.createObjectData(component, event);
                    } else{
                        component.set("v.spinner",false);
                        var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                            "recordId": component.get("v.recordId"),
                            "slideDevName": "Detail"
                        });
                        navEvt.fire();
                    }
                }
                
            });
            // enqueue the server side action  
            $A.enqueueAction(action);
        }
    },
    
    // function for create new object Row in Contact List 
    addNewRow: function(component, event, helper) {
        // call the common "createObjectData" helper method for add new Object Row to List
        helper.createObjectData(component, event);
    },
    
    // function for delete the row 
    removeDeletedRow: function(component, event, helper) {
        // get the selected row Index for delete, from Lightning Event Attribute  
        var index = event.getParam("indexVar");
        // get the all List (contactList attribute) and remove the Object Element Using splice method    
        var AllRowsList = component.get("v.objectList");
        AllRowsList.splice(index, 1);
        // set the contactList after remove selected row element  
        component.set("v.objectList", AllRowsList);
    },
    Cancel : function(component, event, helper){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordId"),
            "slideDevName": "Detail"
        });
        navEvt.fire();
    },
    //function for updating the selected products list on event handling
    UpdateSPList: function(component,event,helper){
        helper.UpdateSelectedProductsList(component,event);
    },
    // changes start for US66729
    ErrorForNotApproved: function(component, event, helper) {
        var assignedOppStage =['Bound','Written'];
        
        var oppStage = component.get("v.opportunityStage");
        var message = '';
        var allObjRows = component.get("v.objectList");
        var programValues=['Unbundled','Open Inventory','Service Only Carrier','Service Only Customer'];
        component.set("v.isAssignedApproved",event.getParam("approve"));
        for (var indexVar = 0; indexVar < allObjRows.length; indexVar++) {
            if(allObjRows[indexVar].TRAV_Assigned_TPA__c == event.getParam("recordIdVar")
               && assignedOppStage.includes(oppStage) 
               && programValues.includes(allObjRows[indexVar].TRAV_Program_Type__c)){
                if(event.getParam("approve") == "false") {
                    component.set("v.tpaAssignedCheckError","The Assigned TPA is not Approved");
                    component.set("v.isError",true); 
                    component.set("v.errorMessage",'');
                    component.set("v.tpaIncumbentErrorMessage",'');
                    component.set("v.tpaWinningErrorMessage",'');
                    component.set("v.tpaAssignedErrorMessage",'');
                } 
                else {
                     component.set("v.tpaAssignedCheckError","");
                	 component.set("v.isError",false);
                }
               
            }
        }
      // changes end for US66729  
    },
    first : function(component, event, helper)
    
    {
        
        var oppProdList = component.get("v.existingProductsList");
        
        var pageSize = component.get("v.pageSize");
        
        var paginationList = [];
        
        for(var i=0; i< pageSize; i++)
            
        {
            
            paginationList.push(oppProdList[i]);
            
        }
        
        component.set("v.paginationList", paginationList);
        
    },
    
    last : function(component, event, helper)
    
    {
        
        var oppProdList = component.get("v.existingProductsList");
        
        var pageSize = component.get("v.pageSize");
        
        var totalSize = component.get("v.totalSize");
        
        var paginationList = [];
        
        for(var i=totalSize-pageSize+1; i< totalSize; i++)
            
        {
            
            paginationList.push(oppProdList[i]);
            
        }
        
        component.set("v.paginationList", paginationList);
        
    },
    
    next : function(component, event, helper)
    
    {
        
        var oppProdList = component.get("v.existingProductsList");
        
        var end = component.get("v.end");
        
        var start = component.get("v.start");
        
        var pageSize = component.get("v.pageSize");
        
        var paginationList = [];
        
        var counter = 0;
        
        for(var i=end+1; i < end+pageSize+1; i++)
            
        {
            
            if(oppProdList.length > end)
                
            {
                
                paginationList.push(oppProdList[i]);
                
                counter ++ ;
                
            }
            
        }
        
        start = start + counter;
        
        end = end + counter;
        
        component.set("v.start",start);
        
        component.set("v.end",end);
        
        component.set("v.paginationList", paginationList);
        
    },
    
    previous : function(component, event, helper)
    
    {
        
        var oppProdList = component.get("v.existingProductsList");
        
        var end = component.get("v.end");
        
        var start = component.get("v.start");
        
        var pageSize = component.get("v.pageSize");
        
        var paginationList = [];
        
        var counter = 0;
        
        for(var i= start-pageSize; i < start ; i++)
            
        {
            
            if(i > -1)
                
            {
                
                paginationList.push(oppProdList[i]);
                
                counter ++;
                
            }
            
            else {
                
                start++;
                
            }
            
        }
        
        start = start - counter;
        
        end = end - counter;
        
        component.set("v.start",start);
        
        component.set("v.end",end);
        
        component.set("v.paginationList", paginationList);
        
    }
})