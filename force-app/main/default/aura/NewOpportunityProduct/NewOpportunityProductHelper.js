({ 
    loadUIConfig : function(component){
        component.set("v.spinner",true);
        var isBSIView=true;
        var bu = component.get("v.opportunityBU");
        var  BSIBU = ['BSI-FI','BSI-PNP','BSI-PCL','BSI-PL',''];
        if( BSIBU.includes(bu))
        	isBSIView=true;
        else isBSIView=false;
            component.set("v.isBSIView",isBSIView);
            if(isBSIView){
                var action2 = component.get("c.selectedProducts");
                action2.setParams({"recordId": component.get("v.recordId")});
                action2.setCallback(this, function(response){
                    component.set("v.spinner",false);
                    var data = response.getReturnValue();
                    component.set("v.selectedProductsList",data);
                    component.set("v.selectedFlag",true);
                });
                $A.enqueueAction(action2);
            }
            else{
                component.set("v.spinner",false);
                component.set("v.selectedFlag",true);
                // Get Picklist Option for Program Type and Pricing Type.
                var action = component.get("c.getPicklistOptions");
                action.setCallback(this, function(response){
                    console.log("picklist Options "+JSON.stringify(response.getReturnValue()));
                    var data = response.getReturnValue();
                    var pricingType = data.TRAV_Pricing_Type__c;
                    component.set("v.pricingType",pricingType);
                    var programType = data.TRAV_Program_Type__c;
                    component.set("v.programType",programType);
                });
                $A.enqueueAction(action);
            }
            // Added the below remote action call for US74862.
            var cb = component.get("c.getCurrentUserProfile");
            cb.setCallback(this, function(response) {
                var getUserProfileResponse = response.getReturnValue();
                if (getUserProfileResponse) component.set("v.currentUserProfile", getUserProfileResponse);
            });
            $A.enqueueAction(cb);
    },
    //Added for US69846
    getExistingProducts : function(component, event) {
        var pageSize = component.get("v.pageSize");
        var action2 = component.get("c.existingProducts");
        action2.setParams({"recordId": component.get("v.recordId")});
        action2.setCallback(this, function(response){
            var data = response.getReturnValue();
            component.set("v.existingProductsList",data);
            component.set("v.totalSize", component.get("v.existingProductsList").length);
            component.set("v.start",0);
            component.set("v.end",pageSize-1);            
            var paginationList = [];            
            for(var i=0; i< pageSize; i++)
                
            {
                
                paginationList.push(response.getReturnValue()[i]);
                
            }
            
            component.set("v.paginationList", paginationList);
        });
        $A.enqueueAction(action2);
        
    },
    createObjectData: function(component, event) {
        // get the sObjectList from component and add(push) New Object to List
        var RowItemList = component.get("v.objectList");
        var recordId = component.get("v.recordId");
        RowItemList.push({
            'sobjectType': 'OpportunityLineItem',
            'Product2Id': '',
            'TRAV_Pricing_Type__c': '',
            'TRAV_Program_Type__c': '',
            'TRAV_Incumbent_Insurance_Carrier_Name__c': '',
            'TRAV_Incumbent_TPA__c': '',
            'TRAV_Assigned_TPA__c': '',
            'TRAV_Winning_TPA__c': '',
            'TRAV_Maximum_Coverage_Limit_Amount__c':'',
            'TRAV_Retention_Amount__c':'',
            'TRAV_Premium__c':'',
            'OpportunityId': recordId,
            'Quantity': 1,
            'TotalPrice' : 0
        });
        component.set("v.objectList", RowItemList);
        //'TRAV_Assigned_TPA__c' : ''
        // set the updated list to attribute (contactList) again    
        
    },

    /**
     * @description Helper function to apply a given set of validation rules to an OpportunityLineItem entry.
     * @param {object} obj Object to apply the given set of validation rules to.
     * @param {Map<string, ...object>} fieldsToValidate Set of validation rules to compare the entry to, in the form of string, string rest params, or 
     * provided validator function, and error message to display.
     * @returns {string} Error message if error, or undefined otherwise.
     * Example usage:
     * let obj = {Pricing_Type: 'Type', Program_Type: ''};
     * let kvArr = [['Pricing_Type', {validator: '', errorMessage: 'Pricing Type cannot be null'}],
     * ['Program_Type', {validator: '', errorMessage: 'Program Type not null'}]];
     * let validationRules = new Map(kvArr);
     * console.log(applyValidationRules(obj, validationRules));
     */
    applyValidationRules: function(obj, fieldsToValidate) {
        let isValid, errorMsg, fieldValidator;
        for (let [field, val] of Object.entries(obj)) {
            let fieldRule = fieldsToValidate.get(field);
            if (!(fieldRule || fieldRule.validator || fieldRule.errorMessage)) continue;
            fieldValidator = fieldRule.validator;
            errorMsg = fieldRule.errorMessage; 
            let vType = typeof fieldValidator === "object" && Array.isArray(fieldValidator) ? 'array' : typeof fieldValidator;
            if (vType == 'string') {
                isValid = fieldValidator === '' ? val !== fieldValidator : val === fieldValidator;
                if (!isValid) break;
            } else if (vType === 'array') {
                fieldValidator = fieldValidator.filter(currentValidator => typeof currentValidator == 'string');
                isValid = fieldValidator.every(cv => cv === '' ? val !== cv : val === cv);
                if (!isValid) break;
            } else {
                if (vType === 'function' && fieldValidator.length === 1) { // Validator function must take one argument and return boolean expression
                    isValid = fieldValidator(val);
                    if (!isValid) break;
                }
            }
        }
        return errorMsg;
    },

    // helper function for check if first Name is not null/blank on save  
    validateRequired: function(component, event) {
        var isValid = true;
        var programValues=['Unbundled','Open Inventory','Service Only Carrier','Service Only Customer'];
        var winningOppStage =['Quote Unsuccessful','Lost'];
        var assignedOppStage =['Bound','Written'];
        var oppStage = component.get("v.opportunityStage");
        var strClosedAndBoundStages = $A.get("$Label.c.TRAV_ClosedAndBoundStages");
        var closedAndBoundStages = strClosedAndBoundStages.split(";");
        var allObjRows = component.get("v.objectList");
        
        for (var indexVar = 0; indexVar < allObjRows.length; indexVar++) {
            debugger;
            //changes fro US69459
            if(closedAndBoundStages.includes(oppStage) && component.get("v.isBSIView") == false) {
                var addProductError = $A.get("$Label.c.TRAV_AddProductError");
                component.set("v.spinner",false);
                isValid = false;
                component.set("v.AddProductException",addProductError);
                component.set("v.isError",true); 
                component.set("v.tpaAssignedCheckError",'');
                component.set("v.errorMessage",'');
                component.set("v.tpaIncumbentErrorMessage",'');
                component.set("v.tpaWinningErrorMessage",'');
                component.set("v.tpaAssignedErrorMessage",'');
            } else { 
                
                // changes start for US66729
                if(component.get("v.isAssignedApproved") == "false" && allObjRows[indexVar].TRAV_Assigned_TPA__c != '') {
                    component.set("v.spinner",false);
                    isValid = false;
                    component.set("v.tpaAssignedCheckError","The Assigned TPA is not Approved");
                    component.set("v.isError",true); 
                    component.set("v.errorMessage",'');
                    component.set("v.tpaIncumbentErrorMessage",'');
                    component.set("v.tpaWinningErrorMessage",'');
                    component.set("v.tpaAssignedErrorMessage",'');
                }
                // changes end for US66729
                if (allObjRows[indexVar].Product2Id == '') {
                    component.set("v.spinner",false);
                    isValid = false;
                    component.set("v.isError",true);
                    component.set("v.errorMessage",'*Please populate all the required fields.');
                    component.set("v.tpaIncumbentErrorMessage",'');
                    component.set("v.tpaWinningErrorMessage",'');
                    component.set("v.tpaAssignedErrorMessage",'');
                }
                //Commented as this was causing error because messgae variable is not defined in this method scope
                /*if(message.includes("Updates to this Opportunity must be made by a user from the respective Business Unit.")) {
                    component.set("v.isError",true);
                    component.set("v.exception",message);
                }*/
                if(allObjRows[indexVar].TRAV_Incumbent_Insurance_Carrier_Name__c == '' && component.get("v.isBSIView")== false) {
                    component.set("v.spinner",false);
                    isValid = false;
                    component.set("v.isError",true); 
                    component.set("v.errorMessage",'*Please populate all the required fields.');
                    component.set("v.tpaIncumbentErrorMessage",'');
                    component.set("v.tpaWinningErrorMessage",'');
                    component.set("v.tpaAssignedErrorMessage",'');
                    
                }
                if(programValues.includes(allObjRows[indexVar].TRAV_Program_Type__c) && allObjRows[indexVar].TRAV_Incumbent_TPA__c == '') {
                    component.set("v.spinner",false);
                    isValid = false;
                    component.set("v.isError",true);
                    component.set("v.tpaIncumbentErrorMessage",'*Incumbent TPA is required when Program Type = Unbundled, Open Inventory, Service Only Carrier, or Service Only Customer.');
                    if (allObjRows[indexVar].Product2Id != '' && allObjRows[indexVar].TRAV_Incumbent_Insurance_Carrier_Name__c != '') {
                        component.set("v.errorMessage",'');
                    }
                    if(allObjRows[indexVar].TRAV_Winning_TPA__c != '') {
                        component.set("v.tpaWinningErrorMessage",'');
                    }
                    if(allObjRows[indexVar].TRAV_Assigned_TPA__c != '') {
                        component.set("v.tpaAssignedErrorMessage",'');
                    }
                }
                if(programValues.includes(allObjRows[indexVar].TRAV_Program_Type__c) && allObjRows[indexVar].TRAV_Winning_TPA__c == ''
                   && winningOppStage.includes(oppStage)) {
                    component.set("v.spinner",false);
                    isValid = false;
                    component.set("v.isError",true);
                    component.set("v.tpaWinningErrorMessage",'*Winning TPA is required on Opportunity Products with a program type = Unbundled, Open Inventory, Service Only Carrier, or Service Only Customer.');
                    if (allObjRows[indexVar].Product2Id != '' && allObjRows[indexVar].TRAV_Incumbent_Insurance_Carrier_Name__c == '') {
                        component.set("v.errorMessage",'');
                    }
                    if(allObjRows[indexVar].TRAV_Incumbent_TPA__c != '') {
                        component.set("v.tpaIncumbentErrorMessage",'');
                    }
                    if(allObjRows[indexVar].TRAV_Assigned_TPA__c != '') {
                        component.set("v.tpaAssignedErrorMessage",'');
                    }
                }
                if(programValues.includes(allObjRows[indexVar].TRAV_Program_Type__c) && allObjRows[indexVar].TRAV_Assigned_TPA__c == ''
                   && assignedOppStage.includes(oppStage)) {
                    component.set("v.spinner",false);
                    isValid = false;
                    component.set("v.isError",true);
                    component.set("v.tpaAssignedErrorMessage",'*Assigned TPA is required on Opportunity Products with a program type = Unbundled, Open Inventory, Service Only Carrier, or Service Only Customer.');
                    if (allObjRows[indexVar].Product2Id != '' && allObjRows[indexVar].TRAV_Incumbent_Insurance_Carrier_Name__c == '') {
                        component.set("v.errorMessage",'');
                    }
                    if(allObjRows[indexVar].TRAV_Incumbent_TPA__c != '') {
                        component.set("v.tpaIncumbentErrorMessage",'');
                    }
                    if(allObjRows[indexVar].TRAV_Winning_TPA__c != '') {
                        component.set("v.tpaWinningErrorMessage",'');
                    }
                }
                // Added the following if block for US74862/US76617.
                if (component.get("v.currentUserProfile") == 'BI - National Accounts') {
                    let biNaErrorMsg;
                    if (!allObjRows[indexVar].TRAV_Program_Type__c && allObjRows[indexVar].TRAV_Pricing_Type__c) {
                        biNaErrorMsg = "A valid Program Type must be selected.";
                    }
                    const reqProgramTypes = ['Bundled', 'Unbundled'];
                    if (!allObjRows[indexVar].TRAV_Pricing_Type__c &&
                        reqProgramTypes.toString().toLowerCase().includes(allObjRows[indexVar].TRAV_Program_Type__c.toString().toLowerCase())) {
                        biNaErrorMsg = 'Pricing Type is required when Program Type = Bundled, Unbundled.'
                    }
                    const reqFields = ['TRAV_Incumbent_Insurance_Carrier_Name__c',
                    'TRAV_Program_Type__c',
                    'TRAV_Pricing_Type__c',
                    'Product2Id'];
                    let invalidCount = 0;
                    for (let [field, val] of Object.entries(allObjRows[indexVar])) {
                        if (reqFields.includes(field) && !val) {
                            invalidCount++;
                            isValid = false;
                            if (invalidCount > 1) {
                                break;
                            }
                        }
                    }
                    if (!isValid) {
                        component.set("v.spinner", false);
                        component.set("v.isError", true);
                        if(allObjRows[indexVar].TRAV_Incumbent_TPA__c != '') component.set("v.tpaIncumbentErrorMessage",'');
                        if(allObjRows[indexVar].TRAV_Winning_TPA__c != '') component.set("v.tpaWinningErrorMessage",'');
                        if (biNaErrorMsg && invalidCount < 2) {
                            component.set("v.biNaReqError", biNaErrorMsg);
                            component.set("v.errorMessage",'');  
                        } else {
                            component.set("v.biNaReqError", '');
                            component.set("v.errorMessage", '*Please populate all the required fields.');
                        } 
                    }
                }
            }
        }
        
        return isValid;
    },
    // sets the attribute to the updated values sent in the event
    UpdateSelectedProductsList: function(component,event){
        var updatedSelectedProducts=event.getParam("spList");
        component.set("v.selectedProductsList",updatedSelectedProducts);
        event.stopPropagation();
    },
        /**
     * @description Ben Climan | User Story US74862: Added this method to show toast events as needed.
     * @param       {string} type The toast type. Can be one of: ['error', 'warning', 'success', 'info']
     * @param       {string} title String to be displayed as the toast title.
     * @param       {string} msg String to be displayed as the toast message.
     */ 
    showToast: function(type, title, message) {
        const validToastTypes = ['error', 'warning', 'success', 'info'];
        if (validToastTypes.includes(type)) { 
            $A
            .get("e.force:showToast")
            .setParams({
                type: type,
                title: title,
                message: message 
            })
            .fire();
        }
    }
})