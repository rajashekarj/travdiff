({
    doInit : function(component, event, helper) {
        helper.doInit(component, event, helper);
    },
    
  
    handleOnSubmit : function(component,event,helper)
    {
        helper.handleOnSubmit(component,event,helper);
    },
    
     closeModal: function(component, event, helper) {
        component.set("v.isOpen", false);
    }
})