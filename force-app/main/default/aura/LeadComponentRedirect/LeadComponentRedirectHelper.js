({
    doInit : function(component, event, helper) {
        var action= component.get("c.fetchPicklistValues");
        action.setCallback(this, function(response) {
                var state = response.getState();
                console.log('Picklist response>'+response.getReturnValue() +response.getState());
            if(state==="SUCCESS")
            {
                component.set("v.statusList",response.getReturnValue().listStatusValues);
                component.set("v.sourceList",response.getReturnValue().listSourceValues);
                component.set("v.industryList",response.getReturnValue().listIndustryValues);
                component.set("v.buInterestedList",response.getReturnValue().listBUInterestedValues);
                
            }
            else
            {
                console.log("picklist Error");
            }
        });
        $A.enqueueAction(action);
        
         var stateList = $A.get("$Label.c.TRAV_State_List");
        var stateArray = stateList.split(',');
        component.set("v.listStates", stateArray);
    },
    handleOnSubmit : function(component, event, helper) {
        event.preventDefault();
        //var eventFields= event.getParam("fields");
        //console.log(JSON.stringify(eventFields));
        if(component.get("v.state")==null||component.get("v.city")==null||
           component.get("v.street")==null||component.get("v.postalCode")==null||
           component.get("v.company")==null||component.get("v.state")==""||component.get("v.city")==""||
           component.get("v.street")==""||component.get("v.postalCode")==""||
           component.get("v.company")=="")
        {
            component.set("v.error", true);
        }
        else{
            component.set("v.error", false);
            component.set("v.spinner",true);
            var action1=component.get("c.getGUID");
            action1.setCallback(this, function(response) {
                var state = response.getState();
                console.log('response>'+response.getReturnValue() +response.getState());
                var accountDetail = {
                        "actionCode":"I",
                       "sourceSystem" : "SFSCCB",
                    	"phoneNumber" : component.get("v.phone"),
                        "accountNumber":response.getReturnValue().substring(0,8),
                        "accountName":component.get("v.company"),
                        "NbrOfEE":component.get("v.numberOfEmployees"),
                        "Revenue": component.get("v.annualRevenue"),
                        "postalAddress":{
                            "stateCode":component.get("v.state"),
                            "cityName":component.get("v.city"),
                            "addressLine1":component.get("v.street"),
                            "postalCode":component.get("v.postalCode")
                        },
                    "accountUrl":component.get("v.website")
                    /*"webAddresses":[{
                        "businessUrl":eventFields["Website"]
                    }]*/
                };
                
                var createJSON={
                    "version":"1.0",
                    "accountDetails": accountDetail
                    
                };
                console.log(
                    "agencyLocation"+component.get("v.agencyLocationId"));
                var spsfJSON={
                    
                    "leadSource":component.get("v.leadSource"),
                    "industry":component.get("v.industry"),
                    "leadStatus":component.get("v.status"),
                    "numberOfEmployees":component.get("v.numberOfEmployees"),
                    "annualRevenue":component.get("v.annualRevenue"),
                    "bUInterested":component.get("v.buInterested")===undefined? null:component.get("v.buInterested").toString().replace(",",";"),
                    "totalAssets":component.get("v.totalAssets"),
                    "effectiveDate":component.get("v.effectiveDate"),
                    "expirationDate":component.get("v.expirationDate"),
                    "guid" : response.getReturnValue().substring(0,8),
                    "agencyLocation":(component.get("v.agencyLocationId")===undefined? null:component.get("v.agencyLocationId").val)
                };
                console.log(spsfJSON);
                var createRequestWrapper={
                    "createRequestWrapper":createJSON,
                    "getSFFieldsResponseWrapper":spsfJSON
                };
                console.log(createRequestWrapper);
                var action = component.get("c.executeLeadSearchPullOperations");
                action.setParams({
                    "operation": "Create_CBE_SF",
                    "wrapperLeadSPReq": createRequestWrapper
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    console.log('response----->'+response.getReturnValue() +response.getState());
                    if(state=== 'SUCCESS'){
                        component.set("v.spinner",false);
                        console.log('Errorif any'+response.getReturnValue().errorIFAny);
                    if (response.getReturnValue().errorIFAny != "" && response.getReturnValue().errorIFAny != null || response.getReturnValue().errorIFAny != undefined) {
                        console.log(response.getReturnValue());
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "type" : "error",
                            "message": "This customer can’t be found / created at this time. Please reach out to your system administrator for next steps."
                        });
                        toastEvent.fire();
                        component.set("v.isOpen", false);
                    }else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "type" : "success",
                            "message": "Customer successfully created in Salesforce."
                        });
                        toastEvent.fire();
                        var idRecordSelected = response.getReturnValue().idNewRecord;
                        console.log("record sel " + idRecordSelected);
                        var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                            "recordId": idRecordSelected,
                            "slideDevName": "Detail"
                        });
                        navEvt.fire();
                    }
                    }else if(state==='ERROR')
                    {
                        component.set("v.spinner",false);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "type" : "error",
                            "message": "This customer can’t be found / created at this time. Please reach out to your system administrator for next steps."
                        });
                        toastEvent.fire();
                        component.set("v.isOpen", false);
                    }
                });
                $A.enqueueAction(action);
            });
            $A.enqueueAction(action1);
        }
    },
    
})