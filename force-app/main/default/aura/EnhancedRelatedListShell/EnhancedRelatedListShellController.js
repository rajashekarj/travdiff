({
    LVRedirect: function(component, event, helper){
       
        var myURL = '/apex/c__AccountRelatedListViewAll?'+'recordId='+ component.get("v.recordId")+
            '&objectAPIName='+ component.get("v.objectAPIName")+
            '&relatedListName='+ component.get("v.relatedListName")+
            '&sort='+ component.get("v.sort")+
            '&filter='+ component.get("v.filter")+
            '&title='+ component.get("v.title")+
            '&parentField='+ component.get("v.parentField")+
            '&sObjectName='+  component.get("v.sObjectName");
        
        component.find("navService").navigate({    
            "type": "standard__webPage",
            "attributes": {
                "url": myURL
            }
        }); 
    }
})