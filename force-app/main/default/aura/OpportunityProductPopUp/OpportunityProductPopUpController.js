({
    doInit: function(component, event, helper) {
        helper.doInit(component,event);
    },
    handleReasonChange: function (component, event, helper) {
        //Get the Selected reason values
        var items = component.get("v.items");
        items = event.getParam("values");
        var objChild = component.find('prod-close-reason');
        var key = event.getSource.getValue('v.myString');
    	alert(key);
   /*     var doc = document.getElementById('prod-reason');
        var auraId = doc.getAttribute('aura:id');
        console.log(auraId); */
        var a = component.get('c.checkReasonList');
        $A.enqueueAction(a);
    //    helper.handleReasonChange(component,event,items);
    },  
    handleSubReasonChange: function (component, event, helper) {
        //Get the Selected sub-reason values   
        var items = component.get("v.items");
        items = event.getParam("values");
        component.set("v.selectedSubReasonList", items);   
    },
    checkReasonList : function(component, event, helper){
        helper.checkReasonList(component,event);
    }, 
    submitDetails: function(component, event, helper) {
        helper.submitDetails(component,event);
    },    
    closeModal: function(component, event, helper) {
        component.set("v.displayPopup", false);
    }
})