({
    doInit : function(component,event){
        var action = component.get("c.getOpportunityStage");
        action.setParams({"opptyId": component.get("v.recordId")});
        // Opportunity Details callback function
        action.setCallback(this, function(response) {
            component.set("v.OptyStage",response.getReturnValue());
            if(component.get("v.OptyStage") == "Written" || component.get("v.OptyStage") == "Declined" || component.get("v.OptyStage") == "Quote Unsuccessful"){
                component.set("v.displayPopup", true);
                
                  var actionList = component.get("c.getClosureList");
        actionList.setParams({"opptyCloseReason": component.get("v.OptyStage")});
        actionList.setCallback(this, function(response) {
            var state = response.getState();
                if(state === 'SUCCESS'){
                var result = response.getReturnValue();
                var plValues = [];
                for (var i = 0; i < result.length; i++) {
                    plValues.push({
                        label: result[i],
                        value: result[i] });
                }
                component.set("v.reasonList",plValues); 
            }
        });
        $A.enqueueAction(actionList);  
            }
            
        });
        $A.enqueueAction(action);
         
        var actionProd = component.get("c.getOpportunityProducts");
        actionProd.setParams({"opptyId": component.get("v.recordId")});
        // Opportunity Products callback function
        actionProd.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.optyProducts",response.getReturnValue());
                console.log(JSON.stringify(response.getReturnValue()));
            }
        });
        $A.enqueueAction(actionProd);
    },
    handleReasonChange: function (component, event, items){
        var actionSubList = component.get("c.getSubReasonList");
        actionSubList.setParams({"opptyProductReasonList": items,
                                 "opptyCloseReason": component.get("v.opportunityCloseReason")});
        actionSubList.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                var result = response.getReturnValue();
                var subReasonValues = [];
                for (var i = 0; i < result.length; i++) {
                    subReasonValues.push({
                        label: result[i],
                        value: result[i] });
                }
                component.set("v.subReasonList",subReasonValues);
            }
        });
        $A.enqueueAction(actionSubList);
    },
    checkReasonList : function(component,event) {
        var items = component.get("v.selectedReasonList");
        var action = component.get("c.checkForOtherAsReason");
        action.setParams({"opptyProductReason": items});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.displayOtherBox", response.getReturnValue());   
            }
        });
        $A.enqueueAction(action);		
    },
    submitDetails : function(component,event){
        component.find("opportunityForm").submit();
        var reasonList = component.get("v.selectedReasonList");
        console.log('ReasonList'+reasonList);
        var subReasonList = component.get("v.selectedSubReasonList");
        console.log('subReasonList'+subReasonList);
        var opptyProd1 =  component.get("v.optyProducts");
        console.log(JSON.stringify(component.get("v.optyProducts")));
        var action = component.get("c.saveOpportunityProducts");  
        action.setParams({
            "opptyProd": component.get("v.optyProducts"),
            "opptyProdReasonList": reasonList,
            "opptyProdSubReasonList" : subReasonList
        });      
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.find('notiFlib').showToast({
                    "title": "Record updated",
                    "message": "The record has been updated",
                    "variant": "success"
                });
            }
        });
        $A.enqueueAction(action);
        component.set("v.displayPopup", false);
    }
})