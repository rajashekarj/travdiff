({
    doInit: function(component, event, helper) {
        var datdetails = component.get("v.wrapper.dataDetails");
        if(datdetails != null && datdetails != undefined){
            for(var i=0;i < datdetails.length;i++){
                for(var j=0;j < datdetails[i].length;j++){
                    if(datdetails[i][j].sfField == 'TRAV_Closed_Sub_Reason__c'){
                        component.set("v.childoptions",datdetails[i][j].closeReasonList);
                    }
                    if(datdetails[i][j].sfField == 'TRAV_Closed_Reason__c'){
                        component.set("v.parentoptions",datdetails[i][j].closeReasonList);
                    }
                    if(datdetails[i][j].sfField == 'TRAV_Stage__c'){
                        //component.set("v.parentoptions",datdetails[i][j].closeReasonList);
                    }
                }
            } 
        }
    },
    handleSingleSelectChange: function(component, event, helper) {
        var selectedProductStage = event.getSource().get("v.value");
        var datdetails = component.get("v.wrapper").dataDetails;
        var action = component.get("c.getStageDependentReasons");
        var recordId = event.getSource().get("v.name");
        action.setParams({ "productStage" : selectedProductStage });
        // Opportunity Details callback function
        action.setCallback(this, function(response) {
            var responseVar = response.getReturnValue();
            var isMultiSelect = true;
            if(responseVar.true != null) {
                var reasonList = responseVar.true;
            } else {
                var reasonList = responseVar.false;
                isMultiSelect = false;
            }
            var childEmptyOptions = [];
            if(reasonList != null && reasonList != undefined){
                reasonList.sort((a, b) => (a.reasonValue > b.reasonValue) ? 1 : -1);
                component.set("v.parentoptions",reasonList);
                component.set("v.childoptions",childEmptyOptions);
                console.log('DATA DETAILS'+JSON.stringify(datdetails));
                for(var i=0;i < datdetails.length;i++){
                    for(var j=0;j < datdetails[i].length;j++){
                        if(recordId == datdetails[i][j].sfOpportunityProductId){
                            if(datdetails[i][j].sfField == 'TRAV_Closed_Reason__c'){
                                datdetails[i][j].closeReasonList = reasonList;
                            }
                            if(datdetails[i][j].sfField == 'TRAV_Closed_Sub_Reason__c'){
                                datdetails[i][j].closeReasonList = childEmptyOptions;
                            }
                            if(datdetails[i][j].sfField == 'TRAV_Product_Winning_New_Carrier__c'){
                                console.log('Curr carrier value'+datdetails[i][j].selectedLookupRecord);
                            }
                        }
                        
                    }
                }
                component.get("v.wrapper").dataDetails = datdetails;
                
                var closeParentVariable = component.find("parentPicklistCmp");
                if(closeParentVariable != null) {
                    if(closeParentVariable.length == undefined) {
                        closeParentVariable.set("v.options",reasonList);
                        closeParentVariable.set("v.selectedOptions",'');
                        closeParentVariable.set("v.IsMultiSelect",isMultiSelect);
                    }else {
                        for(var i=0;i<closeParentVariable.length;i++) {
                            if(closeParentVariable[i].get("v.recordID") == recordId){
                                closeParentVariable[i].set("v.options",reasonList);
                                closeParentVariable[i].set("v.selectedOptions",'');
                                closeParentVariable[i].set("v.IsMultiSelect",isMultiSelect);
                            }
                        }
                    }
                    
                }
                var closeChildVariable = component.find("childPicklistCmp");
                if(closeChildVariable != null) {
                    if(closeChildVariable.length == undefined) {
                        closeChildVariable.set("v.options",childEmptyOptions); 
                    }else {
                        for(var i=0;i<closeChildVariable.length;i++) {
                            if(closeChildVariable[i].get("v.recordID") == recordId){
                                closeChildVariable[i].set("v.options",childEmptyOptions);
                            }
                        } 
                    }                    
                    
                }
                
                
                //lokkup
                var lookupComponentVar = component.find("lookupCmpId");
                var objField = component.find("carrierError");
                if(lookupComponentVar != null) {
                    for(var i=0;i<lookupComponentVar.length;i++) {
                        if(selectedProductStage == 'Quote Unsuccessful' && 
                           lookupComponentVar[i].get("v.currentRecordId") == recordId ){
                            if(lookupComponentVar[i].get("v.selectedRecord") != null &&
                               lookupComponentVar[i].get("v.selectedRecord").Id != null) {
                                $A.util.addClass(objField, 'displayNone');
                                $A.util.removeClass(objField, 'displayBlock');
                                component.set("v.lookupError", false);
                            }else {
                                $A.util.addClass(objField, 'displayBlock');
                                $A.util.removeClass(objField, 'displayNone');
                                component.set("v.lookupError", true); 
                            }
                            
                        } else if (selectedProductStage != 'Quote Unsuccessful') {
                            $A.util.addClass(objField, 'displayNone');
                            $A.util.removeClass(objField, 'displayBlock');
                            component.set("v.lookupError", false);
                        }
                    }
                }
                
            }
        });
        $A.enqueueAction(action);
        //Quote Unsuccessful changes
        /*var objField = component.find("carrierError");
        console.log('carrier error'+objField);
        console.log('Product stg'+selectedProductStage+'lkp'+component.get("v.selectedLookupRecord"));
        if((selectedProductStage == 'Quote Unsuccessful') && component.get("v.selectedLookupRecord") == null){
		    $A.util.addClass(objField, 'displayBlock');
            $A.util.removeClass(objField, 'displayNone');
            component.set("v.lookupError", true);
        }else{
            console.log('Remove error');
            $A.util.addClass(objField, 'displayNone');
            $A.util.removeClass(objField, 'displayBlock');
            component.set("v.lookupError", false);
        }*/
    },
    loadSubItems : function(component, event, helper) {
        var selectedItems = event.getParam("selectedItemsVar");
        var isParentPicklist = event.getParam("isParentPicklist");
        var recordId = event.getParam("RecordId");
        var prodStage;
        if(isParentPicklist){
            var datdetails = component.get("v.wrapper").dataDetails;
            if(datdetails != null && datdetails != undefined){
                for(var i=0;i < datdetails.length;i++){
                    for(var j=0;j < datdetails[i].length;j++){
                        if(datdetails[i][j].sfField == 'TRAV_Stage__c' && 
                           recordId == datdetails[i][j].sfOpportunityProductId){
                            prodStage = datdetails[i][j].inpValue;
                        }
                    }
                } 
            }
            if(selectedItems != null && selectedItems != '' && selectedItems != undefined){
                var action = component.get("c.getSubListItems");
                action.setParams({
                    selectedReasons : selectedItems,
                    opptyCloseReason : prodStage
                });
                action.setCallback(this, function(response){
                    var responseVar = response.getReturnValue();
                    var isMultiSelect = true;
                    if(responseVar.true != null) {
                        var res = responseVar.true;
                    } else {
                        var res = responseVar.false;
                        isMultiSelect = false;
                    }
                    res.sort((a, b) => (a.reasonValue > b.reasonValue) ? 1 : -1)
                    component.set("v.childoptions",res);
                    // component.set("v.IsChildMultiSelect",isMultiSelect);
                    /* for(var i=0;i < datdetails.length;i++){
                        for(var j=0;j < datdetails[i].length;j++){
                            if(datdetails[i][j].sfField == 'TRAV_Closed_Sub_Reason__c' && 
                               recordId == datdetails[i][j].sfOpportunityProductId
                              ){
                                datdetails[i][j].closeReasonList = res;
                            }
                        }
                      
                    }
                     component.get("v.wrapper").dataDetails = datdetails;*/
                    
                    var closeChildVariable = component.find("childPicklistCmp");
                    if(closeChildVariable != null) {
                        if(closeChildVariable.length == undefined) {
                            closeChildVariable.set("v.options",res);
                            closeChildVariable.set("v.IsMultiSelect",isMultiSelect); 
                        }else {
                            for(var i=0;i<closeChildVariable.length;i++) {
                                if(closeChildVariable[i].get("v.recordID") == recordId){
                                    closeChildVariable[i].set("v.options",res);
                                    closeChildVariable[i].set("v.IsMultiSelect",isMultiSelect);
                                }
                            } 
                        }
                        
                    }
                });
                $A.enqueueAction(action);
            }
        }
    },
    itemsChange:function(component, event, helper) {
        console.log('Value changed');
    }
    
})