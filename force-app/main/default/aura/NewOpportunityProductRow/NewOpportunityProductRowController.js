({
    AddNewRow : function(component, event, helper){
        // fire the AddNewRowEvt Lightning Event 
        component.getEvent("AddRowEvt").fire();
        //<!--Added comment for testing deployment issue-->
        //Run a validation to check if row has correct data
        helper.validateData(component);
    },
    
    removeRow : function(component, event, helper){
        //event to update selected products list on row removal
        const selectionId = component.get("v.objectInstance.Product2Id");
        var selectedProducts=component.get("v.selectedProductsList");
        var compEvent=component.getEvent("UpdateSelectedProducts");
        var updatedSelectedProducts=selectedProducts.filter(function(item){return item !== selectionId;});
        component.set("v.selectedProductsList",updatedSelectedProducts);
        compEvent.setParams({"spList":updatedSelectedProducts});
        compEvent.fire();
        // fire the DeleteRowEvt Lightning Event and pass the deleted Row Index to Event parameter/attribute
        component.getEvent("DeleteRowEvt").setParams({"indexVar" : component.get("v.rowIndex") }).fire();
    }, 
    
    
    
})