({
	validateData : function(component) {
		//Check if lookups and Sales price populated
		var objInstance = component.get("v.objectInstance");
        //Is Product is Blank?
        if(objInstance.Product2Id == ""){
            var productError = "Please select a product!";
        }
        if(objInstance.Product2Id == ""){
            var productError = "Please select a product.";
            component.set("v.productError",productError);
        }
        if(objInstance.TRAV_Incumbent_Insurance_Carrier_Name__c == ""){
            var carrierError = "Please select a carrier.";
            component.set("v.carrierError",carrierError);
        }
        if(objInstance.TRAV_Assigned_TPA__c == ""){
            var assignedTPAError = "Please select a Assigned TPA.";
            component.set("v.assignedTPAError",assignedTPAError);
        }
        if(objInstance.TRAV_Winning_TPA__c == ""){
            var winningTPAError = "Please select a winning TPA.";
            component.set("v.winningTPAError",winningTPAError);
        }
	},
    
    fetchNAProducts:function(component, event){
        var action= component.get("c.getNAProducts");
        action.setParams({
            "recordId": component.get("v.recordId")
        });
        console.log(action);
        action.setCallback(this, function(response){
            component.set("v.NAProductsList",response.getReturnValue());
            console.log(response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    comboChangeValue: function(component,event){
        var recordId = event.getParam("value");
        var selectedProductsList= component.get("v.selectedProductsList");
        var selectedItem= component.get("v.selectedItem");
        if(selectedItem===null||selectedItem===undefined)
        {
            selectedItem=event.getParam("value");
            component.set("v.selectedItem",selectedItem);
        }
        else
        {
            selectedProductsList.splice(selectedProductsList.lastIndexOf( selectedItem ), 1);
        }
        selectedProductsList.push(recordId);
        component.set("v.objectInstance.Product2Id",recordId);
        component.set("v.selectedProductsList",selectedProductsList);
    }
})