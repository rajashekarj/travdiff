({
    
    init: function(component) {
        
        //note, we get options and set options_
        
        //options_ is the private version and we use this from now on.
        
        //this is to allow us to sort the options array before rendering
        
        if (component.get("v.initialized")){
            
            return;
            
        } 
        
        component.set("v.initialized",true);
        
        
        
        var options = component.get("v.options");
        //console.log('options in multi select'+JSON.stringify(options));
        options.sort(function compare(a, b) {
            
            if (a.value == 'All') {
                
                return -1;
                
            } else if (a.value < b.value) {
                
                return -1;
                
            }
            
            if (a.value > b.value) {
                
                return 1;
                
            }
            
            return 0;
            
        });
        
        
        component.set("v.options_", options);
        var labels = this.getSelectedLabels(component);
        
        this.setInfoText(component, labels);
        
    },  
    
    
    setInfoText: function(component, values) {    
        if (values.length == 0) {
            component.set("v.infoText", "Select an Option");
        }
        if (values.length == 1) {
            component.set("v.infoText", values[0]);
        }
        else if (values.length > 1) {
            component.set("v.infoText", values.length + " Options Selected");
        }
        
    },
    
    getSelectedValues: function(component){
        var options = component.get("v.options_");
        var values = [];
        options.forEach(function(element) {
            if (element.selected) {
                values.push(element.value);
            }
        });
        return values;
    },
    
    getSelectedLabels: function(component){
        var options = component.get("v.options_");
        var labels = [];
        options.forEach(function(element) {
            if (element.selected) {
                labels.push(element.label);
            }
        });
        return labels;
        
    },
    
    despatchSelectChangeEvent: function(component,values){
        //var compEvent = component.getEvent("selectChange");
        //compEvent.setParams({ "values": values });
        //compEvent.fire();
        
    },
    handleClick1: function(component, event, where) {
         //getting target element of mouse click
        var tempElement = event.target;
		var outsideComponent = true;
		while(tempElement){
           if(tempElement.id === 'ms-list-item'){
                //2. Handle logic when picklist option is clicked
				 outsideComponent = false;
                break;
            } else if(tempElement.id === 'ms-dropdown-items'){
                
                //3. Clicked somewher within dropdown which does not need to be handled
                //Break the loop here
                outsideComponent = false;
                break;
            } else if(tempElement.id === 'ms-picklist-dropdown'){
                //1. Handle logic when dropdown is clicked
                if(where === 'component'){
                	this.onDropDownClick(tempElement);
                }
                outsideComponent = false;
                break;
            }
            //get parent node
            tempElement = tempElement.parentNode;
        }
        if(outsideComponent){
             
             component.set("v.clicked", false);
        }
    }
    
})