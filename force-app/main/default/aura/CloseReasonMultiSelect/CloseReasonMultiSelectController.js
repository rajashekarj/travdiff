({
    doInit : function(component, event, helper) {
        if(component.get("v.isParent")){
            var selecteItems = component.get("v.selectedItems");
			var selectedList = [];
            if(selecteItems != undefined){
                if(selecteItems.includes(";")){
                    selectedList = selecteItems.split(";");  
                    selectedList.sort();
                }
                else{
                    selectedList.push(selecteItems);
                }
            }
 
		    var options = component.get("v.options");
            console.log('Options'+options);
            if(options.length != 0){
                for(var i = 0; i< options.length; i++){
                    if(selectedList != undefined && selectedList != ""){
                        if(selectedList.includes(options[i].reasonValue)){
                            options[i].reasonChecked = true;
                        }
                    }
                }
                options.sort();
            }
            if(selectedList == undefined){
                component.set("v.dropdownText","Select an Option");
            }
            else if(selectedList.length == 1){
                component.set("v.dropdownText",selectedList[0]);
            }
                else{
					component.set("v.dropdownText",selectedList.length+" Options Selected");
                }
            options.sort((a, b) => (a.reasonValue > b.reasonValue) ? 1 : -1)
           
            
            component.set("v.options",options); 
           }
    },
    toggleDropdown : function(component, event, helper) {
        //onmouseleave.fire();
        var clicked = component.get("v.clicked");
       
        component.set("v.clicked", !clicked);
        
    },
    toggleValue : function(component, event, helper,where) {
		var target = event.target;
        console.log('**target value**'+target);
        var listOfReasons = component.get("v.options");
        var selectedValue ="";
        if(target != null && target != undefined) {
           var elem = target.getAttribute("data-selected-index"); 
        
        
        for(var i = 0; i< listOfReasons.length; i++){
            if(i == elem){
                listOfReasons[i].reasonChecked = !listOfReasons[i].reasonChecked;
            }
            if(listOfReasons[i].reasonChecked){
                selectedValue += listOfReasons[i].reasonValue+";";
            }
            else{
                selectedValue = selectedValue.replace(listOfReasons[i].reasonValue+";","");
            }
        }
            }
        if(selectedValue != undefined){
            selectedValue = selectedValue.substring(0, selectedValue.length-1);    
        }
        var selectedList = [];
        if(selectedValue != null && selectedValue.includes(";")){
            selectedList = selectedValue.split(";");
        }
        else{
            selectedList.push(selectedValue);
        }
        if(selectedList.length > 1){
            component.set("v.dropdownText",selectedList.length+" Options Selected");
        }
        else if(selectedList.length == 1 && selectedList[0].length > 0){
            component.set("v.dropdownText",selectedList[0].replace(";",""));
        }
            else {
                component.set("v.dropdownText","Select an Option");
            }
        if(component.get("v.isParent")){
            if(selectedValue.includes("Other")){
                component.set("v.showOtherComments", true);
            }
            else{
                component.set("v.showOtherComments", false);
            }
			component.set("v.selectedItems",selectedValue);
            listOfReasons.sort((a, b) => (a.reasonValue > b.reasonValue) ? 1 : -1);
            component.set("v.options", listOfReasons);    
        }
        else{
			component.set("v.selectedSubItems",selectedValue);
            listOfReasons.sort((a, b) => (a.reasonValue > b.reasonValue) ? 1 : -1);
            component.set("v.options", listOfReasons);
        }
        //Store it to Event and handle at component level
        var rowIndex = component.get("v.rowIndex");
        var optionsSelected = component.get("v.selectedItems");
            console.log('**Options Selected**'+optionsSelected);
            if(optionsSelected != '' && optionsSelected != null){
               //Fire event for addition 
				var compEvent = component.getEvent("ProductCloseReasonChangeEvent");
                compEvent.setParams({"closeReasonStr" : rowIndex,"closeReasonAdd" :false,"SelectedStage":component.get("v.prodStage")+';'+rowIndex}); 
                compEvent.fire();
            }else{
               //Fire event for subtraction
                var compEvent = component.getEvent("ProductCloseReasonChangeEvent");
                compEvent.setParams({"closeReasonStr" : rowIndex,"closeReasonAdd" : true,"SelectedStage":component.get("v.prodStage")+';'+rowIndex}); 
                compEvent.fire();
            }
        
    },
    loadSubItems : function(component, event, helper) {
        var selectedItems = component.get("v.selectedItems");
        var stg = component.get("v.prodStage");
            if(selectedItems != null && selectedItems != '' && selectedItems != undefined ){
                if(selectedItems == 'Pricing' && stg == 'Quote Unsuccessful'){
                    
					component.set("v.isChildSS",true);
				}else{
                 
				 component.set("v.isChildSS",false);
                 
            }
            }else{
                 
				 component.set("v.isChildSS",false);
                 
            }

			if(!component.get("v.isParent")){
			var closedReason = component.get("v.prodStage") ;
            if(selectedItems != null && selectedItems != '' && selectedItems != undefined){
                    var action = component.get("c.getSubListItems");
                    action.setParams({
                        selectedReasons : selectedItems,
                        opptyCloseReason : closedReason
                    });
                    action.setCallback(this, function(response){
                        var res = response.getReturnValue();
                        res.sort((a, b) => (a.reasonValue > b.reasonValue) ? 1 : -1)
                        component.set("v.dropdownText","Select an Option");
                        component.set("v.options",res);
                    });
                    $A.enqueueAction(action);
                }
        }
    },
    onRender : function(component, event, helper) {
        //Document listner to detect click outside multi select component
        //Removed this and using mouse events
        document.addEventListener("click", function(event){
            //helper.handleClick1(component, event, 'document');
        });
    },
    selectValue : function(component, event, helper) { 
        var stg = component.get("v.OptyStage");
        if(stg == 'Quote Unsuccessful'){
            component.set("v.isQuoteUnsuccClicked",true);
        }
        component.set("v.isChildSS",true);
        var reason =   component.find("reasonId");
        if(reason != null && reason != undefined){
            var selectedValue = component.find("reasonId").get("v.value");
            if(selectedValue != null && selectedValue != undefined ){
                // Sep 2, 2019 : Updated : To resolve close reason and sub reason population
                if(stg == 'Quote Unsuccessful'){
                    component.set("v.selectedSubItems",selectedValue);
                }else {
                    component.set("v.selectedItems",selectedValue);	
                }
                //Changes END
                
                
            }
        }
        var rowIndex = component.get("v.rowIndex");
        var optionsSelected = component.get("v.selectedItems");
            console.log('**Options Selected**'+optionsSelected);
            if(optionsSelected != '' && optionsSelected != null){
               //Fire event for addition 
				var compEvent = component.getEvent("ProductCloseReasonChangeEvent");
                compEvent.setParams({"closeReasonStr" : rowIndex,"closeReasonAdd" :false,"SelectedStage":component.get("v.prodStage")+';'+rowIndex}); 
                compEvent.fire();
            }else{
               //Fire event for subtraction
                var compEvent = component.getEvent("ProductCloseReasonChangeEvent");
                compEvent.setParams({"closeReasonStr" : rowIndex,"closeReasonAdd" : true,"SelectedStage":component.get("v.prodStage")+';'+rowIndex}); 
                compEvent.fire();
            }
        
    },
    //Added below mouse events for preventing value override
    
    handleClick: function(component, event, helper) {
        component.set("v.clicked", true);
    },
    
    handleMouseLeave: function(component, event, helper) {
        component.set("v.dropdownOver",false);
        component.set("v.clicked", false);
    },
    
    handleMouseEnter: function(component, event, helper) {
        component.set("v.dropdownOver",true);
        component.set("v.clicked", true);
    },
    
    handleMouseOutButton: function(component, event, helper) {
        window.setTimeout(
            $A.getCallback(function() {
                if (component.isValid()) {
                    //if dropdown over, user has hovered over the dropdown, so don't close.
                    if (component.get("v.dropdownOver")) {
                        return;
                    }
                    component.set("v.clicked", false);
                }
            }), 200
        );
        
    },
    resetText : function(component, event, helper) {
        console.log("invoked");
        component.set("v.dropdownText","Select an Option");
    },
})