({
	doInit : function(component,event) {
		var action = component.get("c.getLoadPopUpDetails");
        action.setParams({"recordId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var underReviewWrapperValue = response.getReturnValue();
            if(underReviewWrapperValue != null && underReviewWrapperValue != undefined) {
                component.set("v.displayPopup",underReviewWrapperValue.loadPopup);
            } 
        });
        $A.enqueueAction(action);
	}, 
    closeModal : function(component, event) {
        component.set("v.isMaterialRequestedNull",false);
        component.set("v.isOutstandingItemNull",false);
        var action = component.get("c.revertStageUpdate");
        action.setParams({"recordId": component.get("v.recordId")});
        $A.enqueueAction(action);
        component.set("v.displayPopup", false);
        $A.get('e.force:refreshView').fire();
    }
})