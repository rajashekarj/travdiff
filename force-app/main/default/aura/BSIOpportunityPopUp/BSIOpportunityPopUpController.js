({
    doInit: function(component, event, helper) {
        helper.doInit(component,event);
    }, 
    closeModal : function(component, event, helper) {
        var varMaterialRequestedError = component.find("isMaterialRequestedNullError");
        $A.util.removeClass(varMaterialRequestedError, 'slds-show');
        $A.util.addClass(varMaterialRequestedError, 'slds-hidden');
        var varOutstandingItemError = component.find("isOutstandingItemNullError");
        $A.util.removeClass(varOutstandingItemError, 'slds-show');
        $A.util.addClass(varOutstandingItemError, 'slds-hidden');
        helper.closeModal(component,event);
    },
    updateRefersh : function(component, event, helper) {
        var changeType = event.getParams().changeType;
        helper.doInit(component,event);
    },
    submitDetails : function(component, event, helper) {
        //helper.submitDetails(component,event);
        var outstandingItemsIndicator = component.find("outstandingItems");
        var outstandingItemsIndicatorValue = outstandingItemsIndicator.get("v.value");
        console.log(outstandingItemsIndicatorValue);
        if(outstandingItemsIndicatorValue == 'Yes'){
            var materialRequested = component.find("mateialsRequested");
            var materialRequestedValue = materialRequested.get("v.value");
            console.log(materialRequestedValue);
            if(materialRequestedValue == null || materialRequestedValue == ''){
                component.set("v.isMaterialRequestedNull",true);
                component.set("v.isOutstandingItemNull",false);
            }else{
                component.set("v.isMaterialRequestedNull",false);
                component.set("v.isOutstandingItemNull",false);
                var action = component.get("c.saveDetails");
                action.setParams({
                    "recordId" : component.get("v.recordId"),
                    "strOutstandingItemIndicator" : outstandingItemsIndicatorValue,
                    "strMaterialsRequested" : materialRequestedValue,
                    "strUnderReviewNotes" : component.find("underReviewNotes").get("v.value")
                });
                $A.enqueueAction(action);
                component.set("v.displayPopup", false);
                $A.get('e.force:refreshView').fire();
            }
        }else if(outstandingItemsIndicatorValue == null || outstandingItemsIndicatorValue == ''){
            component.set("v.isMaterialRequestedNull",false);
            component.set("v.isOutstandingItemNull",true);
        }else{
            var action = component.get("c.saveDetails");
            action.setParams({
                "recordId" : component.get("v.recordId"),
                "strOutstandingItemIndicator" : outstandingItemsIndicatorValue,
                "strMaterialsRequested" : materialRequestedValue,
                "strUnderReviewNotes" : component.find("underReviewNotes").get("v.value")
            });
            $A.enqueueAction(action);
            component.set("v.displayPopup", false);
            $A.get('e.force:refreshView').fire();
        }
    },
})