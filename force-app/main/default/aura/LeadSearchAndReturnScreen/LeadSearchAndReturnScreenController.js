({
    //Method called on initialization
    doInit : function(component, event, helper) {
        helper.doInit(component, event, helper);
    },
    //Method called on Search 
    searchRecord : function(component, event, helper) {
        helper.searchRecord(component, event, helper);
    },
    //Method called on Cancel
    cancelSearch : function(component, event, helper) {
        helper.cancelSearch(component, event, helper);
    },
    //Method called on Go to record in Search results
    goToRecord : function(component, event, helper) {
        helper.goToRecord(component, event, helper);
    },
    //Method called on Create Lead in Search Results
    createRecord : function(component, event, helper) {
        helper.createRecord(component, event, helper);
    },
    //Method called on default Create New Lead
    createNewLead : function(component, event, helper) {
        helper.createNewLead(component, event, helper);
    }
})