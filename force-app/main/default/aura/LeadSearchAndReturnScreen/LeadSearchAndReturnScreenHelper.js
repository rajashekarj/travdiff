({
    //Method called on initialization
    doInit : function(component, event, helper) {
        //Set list of states on initialization
        var stateList = $A.get("$Label.c.TRAV_State_List");
        var stateArray = stateList.split(',');
        console.log("state array " + stateArray);
        component.set("v.listStates", stateArray);
        
    },
    
    //Method called on Search
    searchRecord : function(component, event, helper) {
        component.set("v.spinner", true);
        //Create request wrapper
        var searchReqWrapper = {"companyName": component.get("v.strCompany"),
                                "state": component.get("v.strAddrState"),
                                "street": component.get("v.strAddrStreet"),
                                "city": component.get("v.strAddrCity")};
        var searchJSON ={
            "operation": "search",
            "searchRequestWrapper": searchReqWrapper
        }; 
        console.log("---search JSON " + JSON.stringify(searchJSON));
        var action = component.get("c.executeLeadSearchPullOperations");
        action.setParams({
            "operation": "search",
            "wrapperLeadSPReq": searchJSON
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var searchResults = response.getReturnValue();
            console.log('response> '+ JSON.stringify(response.getReturnValue()) +response.getState());
            if(response.getReturnValue().listSearchResponseWrapper != null && state ==="SUCCESS" ){
                component.set("v.isSearched", true);
                //Search results are not null
                if (response.getReturnValue().errorIFAny == null) {
                    component.set("v.isResultEmpty", false);
                    component.set("v.listSearchResults", searchResults.listSearchResponseWrapper);
                    component.set("v.spinner", false);
                }
                //No search results are returned
                else if(response.getReturnValue().errorIFAny == "BLANK"){
                    component.set("v.isResultEmpty", true);
                    component.set("v.spinner", false);
                }
            }
            //Error during search call
            else{
                var genericError = $A.get("$Label.c.TRAV_Generic_Error_LSP");
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type" : "error",
                    "message": genericError
                });
                toastEvent.fire();
                component.set("v.spinner", false);
            }
        });
        $A.enqueueAction(action);
    },
    
    //Method called on Cancel
    cancelSearch : function(component, event, helper) {
        //Go back to lead list view
        var navEvt = $A.get("e.force:navigateToURL");
        navEvt.setParams({
            "url" : "/006/o"
        });
        navEvt.fire();
    },
    
    //Method called on Go to record in Search results
    goToRecord : function(component, event, helper) {
        //navigate to the record detail page
        var idRecordSelected = event.getSource().get("v.value");
        console.log("record sel " + idRecordSelected);
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": idRecordSelected,
            "slideDevName": "Detail"
        });
        navEvt.fire();
    },
    
    //Method called on Create Lead in Search Results
    createRecord : function(component, event, helper) {
        component.set("v.spinner", true);
        var cbeIDselected = event.getSource().get("v.value");
        var createJSON ={
            "cbeID": cbeIDselected
        };
        var action = component.get("c.executeLeadSearchPullOperations");
        action.setParams({
            "operation": "create_sf",
            "wrapperLeadSPReq": createJSON
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var createResponse = response.getReturnValue();
            console.log('response> '+ JSON.stringify(response.getReturnValue()) +response.getState());
            //Record successfully created and id of the created record is returned
            if (createResponse.idNewRecord != "" && createResponse.idNewRecord != null && createResponse.idNewRecord != undefined) {
                var createSuccessMessage = $A.get("$Label.c.TRAV_Create_Success_Message_LSP");
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type" : "success",
                    "message": createSuccessMessage
                });
                toastEvent.fire();
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId" : createResponse.idNewRecord,
                    "slideDevName": "Detail"
                });
                navEvt.fire();
                component.set("v.spinner", false);
            }
            //Error in create call
            else{
                var genericError = $A.get("$Label.c.TRAV_Generic_Error_LSP");
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type" : "error",
                    "message": genericError
                });
                toastEvent.fire();
                component.set("v.spinner", false);
            }
        });
        $A.enqueueAction(action);
        
    },
    //Method called on default Create New Lead
    createNewLead : function(component, event, helper) {
        component.set("v.spinner", true);
        component.set("v.newLeadOpen", true);
        component.set("v.spinner", false);
    }
})