({
    selectOptionHelper : function(component,label,isCheck) {
        var selectedOption=''; 
        var selectedItems=[];
        var allOptions = component.get('v.options');
        var count=0;
        for(var i=0;i<allOptions.length;i++){ 
            if(component.get("v.IsMultiSelect")){
                if(allOptions[i].reasonValue == label) {
                    if(isCheck=='true'){ 
                        allOptions[i].reasonChecked = false; 
                    }else{ 
                        allOptions[i].reasonChecked = true; 
                    } 
                }
                if(allOptions[i].reasonChecked){ 
                    selectedOption=allOptions[i].reasonValue ;
                    count++; 
                    selectedItems.push(selectedOption); 
                } 
            }else{
                if(allOptions[i].reasonValue == label){
                    allOptions[i].reasonChecked = true;
                    selectedOption = label;
                    selectedItems.push(selectedOption);
                }else {
                    allOptions[i].reasonChecked = false;  
                }
                
            }
            
        } 
        
        if(count>1){
            selectedOption = count+' items selected';
        }
        component.set("v.selectedOptions",selectedOption);
        component.set("v.selectedItems",selectedItems);
        component.set('v.options',allOptions);
        component.get('v.DataDetails').closeReasonList = allOptions;
        var datdetails = component.get("v.DataDetails");
        if(component.get('v.isParent')) {
            var cmpEvent = component.getEvent("picklistEvent");
            cmpEvent.setParams({
                "selectedItemsVar" : component.get('v.selectedItems'),
                "isParentPicklist" : component.get('v.isParent'),
                "RecordId" : datdetails.sfOpportunityProductId
            });
            cmpEvent.fire();
        }
        
        /*if(component.get('v.isParent')) {
             component.set('v.parentoptions',allOptions);
        } else {
             component.set('v.childoptions',allOptions);
        }*/
    }
})