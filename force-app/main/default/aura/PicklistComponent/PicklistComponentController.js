({
    doInit:function(component,event,helper){
            
        var OppStage = component.get("v.OpportunityStage");
        if( (OppStage == "Written" || OppStage == "Bound") && 
           component.get("v.isParent") == true){
            component.set("v.IsMultiSelect",false);
        }      
    },
    openDropdown:function(component,event,helper){
        $A.util.addClass(component.find('dropdown'),'slds-is-open');
        $A.util.removeClass(component.find('dropdown'),'slds-is-close');
    },
    closeDropDown:function(component,event,helper){
        $A.util.addClass(component.find('dropdown'),'slds-is-close');
        $A.util.removeClass(component.find('dropdown'),'slds-is-open');
    },
    selectOption:function(component,event,helper){        
        if(event.currentTarget.id != null) {
            var label = event.currentTarget.id.split("#BP#")[0];
            var isCheck = event.currentTarget.id.split("#BP#")[1]; 
            
        } else {
            var label = event.getSource().get("v.value");
            var isCheck = true;  
        }
        component.set("v.CloseReasonSelected",label);
        helper.selectOptionHelper(component,label,isCheck);
    }   
    
})