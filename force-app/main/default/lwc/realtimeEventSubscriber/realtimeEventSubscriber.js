import { LightningElement, api, track } from 'lwc';
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled} from 'lightning/empApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class RealtimeEventSubscriber extends LightningElement {
    @track platformEventType = '/event/BatchApexErrorEvent';
    //@api debugMode;
    @track subscription;
    @track tableEvents = [];
    @track renderEventsTable = false;

    showErrorNotification(err)
    {
        const errEvt = new ShowToastEvent({
            title: 'Error occurred during component load',
            message: err.message,
            variant: 'error'
        });
        this.dispatchEvent(errEvt);
    }

    showUnsubNotification()
    {
        const unsubEvt = new ShowToastEvent({
            title: 'Unsubscribed from event',
            message: `Unsubscribed from channel: ${this.subscription}`,
            variant: 'info'
        });
        this.dispatchEvent(unsubEvt);
    }

    showEmpDisabledNotification()
    {
        const empUnavailableEvt = new ShowToastEvent({
            title: 'The EMP API is unavailable in this context',
            message: 'The EMP API is unavailable in this context',
            variant: 'warning'
        });
        this.dispatchEvent(empUnavailableEvt);
    }

    handleUnsubscribe() {
        unsubscribe(this.subscription, (unsubResponse) => {
            this.subscription = null;
            this.tableEvents = [];
            this.showUnsubNotification(unsubResponse.subscription);
        });
    }

    handleSubscribe() {
        subscribe(this.platformEventType, -1, (realTimeEvent) => {
            if (!this.renderEventsTable)
            {
                this.renderEventsTable = true;
            }
            let rte = JSON.parse(JSON.stringify(realTimeEvent));
            console.log(rte);
            let rteObj = {
                ReplayId: rte.data.event.replayId,
                AsyncApexJobId: rte.data.event.asyncApexJobId,
                ExceptionType: rte.data.event.exceptionType,
                JobScope: rte.data.event.jobScope,
                Message: rte.data.event.message,
                Phase: rte.data.event.phase,
                RequestId: rte.data.event.requestId,
                StackTrace: rte.data.event.stackTrace
            };
            this.tableEvents.push(rteObj);
            // Dispatch an event containing a payload with the up-to-date list of platform events
            //this.dispatchEvent(new CustomEvent('platformeventreceived', {detail: rteObj}));
        }).then((eventSubscription) => {
            this.subscription = eventSubscription;
            console.log(JSON.parse(JSON.stringify(eventSubscription)));
        });
    }

    connectedCallback() {
        if (!isEmpEnabled())
        {
            this.showEmpDisabledNotification();
        }
        else
        {
            //setDebugFlag(Boolean(this.debugMode));
            setDebugFlag(true);
            this.handleSubscribe();
            onError((eventSubscriberErr) => {
                this.showErrorNotification(eventSubscriberErr);
            });
        }
    }        
}