import { LightningElement, wire } from 'lwc';
import getAccountingMonthDetails from '@salesforce/apex/DaysLeftInCurrentMonthController.getAccountingMonthDetails';
import daysLeftTitle from '@salesforce/label/c.TRAV_Days_Left_in_Current_Month_Title';

export default class DaysLeftInCurrentMonth extends LightningElement {
    data;
    error;

    @wire(getAccountingMonthDetails) days({data,error}){
        if(data){
            this.data = data;
            window.console.log('data -->' + JSON.stringify(data));
        }
        else if (error){
            this.error = error;
        }
    }
    
    label = {
        daysLeftTitle,
    };
  
}