import { LightningElement, api, track, wire } from 'lwc';

import getFields from '@salesforce/apex/OpportunityLineItemFieldAccessor.getTRAV_FieldsetByParameter';

export default class OpportunityLayoutLwc extends LightningElement {
    recordId;
    @api recordId; 
    @api objectApiName;
    @api sectionLabel;
    @api visibilitySetting;    
    @api fieldsetNameProp;
    @api objectNameProp;
    @track wiredFieldsTracked;
    @track error;
    hardcodedfields= [];
  
    @wire(getFields, {objectName: '$objectNameProp', fieldsetName: '$fieldsetNameProp',IdOpportuntiyLineItem: '$recordId'})
    wiredFields({error,data}) {
      if(data) {
        for(var d of data) {
          this.hardcodedfields.push(d);
        }
        this.wiredFieldsTracked = this.hardcodedfields;
      }
      else if (error) {
        console.log(error);
        this.error = error;
      }
    }

  handleOpportunityLineItemSelected(opportunityLineItemId) {
    this.recordId = opportunityLineItemId;
}

}