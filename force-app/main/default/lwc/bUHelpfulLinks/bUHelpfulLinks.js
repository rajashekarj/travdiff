import { LightningElement, wire, track} from 'lwc';

import getBULinks from '@salesforce/apex/BUHelpfulLinkController.getBULinks';
import TRAV_My_BU_Helpful_Links from '@salesforce/label/c.TRAV_My_BU_Helpful_Links';

export default class BUHelpfulLink extends LightningElement {
    @track recordList1;
    @track recordList2;
    @track error;

    @wire(getBULinks) buLinks({ error, data }) {
        var list1= [];
        var list2= [];
        var i;
        var j;
        if (data) {           
            
            for(i=0;i<Math.round(data.length/2);i++){
                list1.push(data[i]);
            }
            for(j=Math.round(data.length/2);j<data.length;j++){
                list2.push(data[j]);
            }
            this.recordList1 = list1;
            this.recordList2 = list2;
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.recordList1 = [];
            this.recordList2 = [];
        }
    }

    label = {
        TRAV_My_BU_Helpful_Links,
    };
}