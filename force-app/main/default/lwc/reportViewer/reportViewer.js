import { LightningElement, api, track, wire } from "lwc";
import { getRecord } from "lightning/uiRecordApi";
import reportUrl from '@salesforce/label/c.TRAV_Report_URL_Qlikview';
var rFld = 'Opportunity.TRAV_BU_Account_Number__c';

export default class ReportViewer extends LightningElement {
     @api recordId;
     @track accountId;
     @track acctId;
     @track rUrl;
     @api rptUrl;
     @api rptApp;
     @api rptSht;
     @api rptLst;
     @api rptObj;
     @api rptKey;
     @api rptHgt;

     //@track rFld = this.rptObj + '.' + this.rptKey;
     //rFld = "['Opportunity.TRAV_BU_Account_Number__c']";
          
     @wire(getRecord, {
          recordId: "$recordId",
          fields: [rFld]
     })
     wiredRecord({ error, data }) {
          if (error) {
               this.acctId = "error";
          } else if (data) {
               this.accountId = data;
               this.acctId = this.accountId.fields.TRAV_BU_Account_Number__c.value;
          }
     }

     //"http://bond-qlikview-dev.dvllb.travp.net/QvAJAXZfc/opendoc.htm?document=bond_dev_browsable%5Cmanagement%20liability%20account%20analysis%20salesforce.qvw&lang=en-US&host=QVS%40DEV_BOND_Cluster&select=LB05,{!Account.AccountNumber}"
     //http://bond-qlikview-dev.dvllb.travp.net/QvAJAXZfc/opendoc.htm?document=bond_dev_browsable%5C
     //management%20liability%20account%20analysis.qvw
     //&lang=en-US&host=QVS%40DEV_BOND_Cluster
     //&sheet=shtSalesforceNew
     //&select=lbAccountNumber,5837231
     //rptSrc = this.rptUrl + this.rptApp + '&lang=en-US&host=QVS%40DEV_BOND_Cluster&sheet=' + this.rptSht +'&select=' + this.rptLst + ',' + this.acctId;
     get rptSrc() {
          if (this.acctId === "error") {
               this.rUrl =
                    reportUrl +
                    this.rptApp +
                    this.rptSht;
          } else {
               this.rUrl =
                    reportUrl +
                    this.rptApp +
                    "&sheet=" +
                    this.rptSht +
                    "&select=" +
                    this.rptLst +
                    "," +
                    this.acctId;
          }
          return this.rUrl;
     }
}