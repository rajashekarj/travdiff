trigger UserFieldValidation on User (before insert,before update) 
{
	if(Trigger.isBefore)
    {
       for (User a:Trigger.new)
        {
        	a.EmailPreferencesAutoBcc = true;
            a.SenderEmail = a.Email;
            a.SenderName = a.FirstName + ' ' + a.LastName;
        }
    }
}