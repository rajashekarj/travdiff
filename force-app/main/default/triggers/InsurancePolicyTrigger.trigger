/**
* Trigger for InsurancePolicy object. This will be fired whenever a DML is performed on InsurancePolicy record.
*
Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
*Sayanka Mohanty                TBD         08/29/2019        Original Version
* -----------------------------------------------------------------------------------------------------------
*/
trigger InsurancePolicyTrigger on InsurancePolicy (Before Insert, Before Update, After Update, After Insert, 
                             Before Delete, After Delete, After Undelete) {
     //Check to skip trigger org wide and individually.
    //if(UtilityClass.triggerCheck('PolicyTrigger')) {
      PolicyTriggerHandler policyTriggerHandlerObj = new PolicyTriggerHandler();
       TriggerDispatcher.Run(policyTriggerHandlerObj);
    //} 
}