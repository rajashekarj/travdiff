trigger OpportunityContactRoleTrigger on OpportunityContactRole (Before Insert, Before Update, After Update, After Insert, Before Delete, After Delete, After Undelete) {
//Check to skip trigger org wide and individually.
    if(UtilityClass.triggerCheck('OpportunityContactRoleTrigger')) {
       OpportunityContactRoleTriggerHandler OpptyContactTriggerHandlerObj = new OpportunityContactRoleTriggerHandler();
       TriggerDispatcher.Run(OpptyContactTriggerHandlerObj);
    }
}