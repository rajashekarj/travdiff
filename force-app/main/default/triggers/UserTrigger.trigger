/**
 * Trigger for the User object.
 *
 * Modification Log:
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Ben Climan             US79918              04/08/2020       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
trigger UserTrigger on User (before insert, before update, after insert, after update) {
    if (UtilityClass.triggerCheck('UserTrigger')) {
        // Force Salesforce to only run SailPoint utilities if Permission Set assigned.
        List<PermissionSetAssignment> hasSailPointPermissions = 
        [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name = 'SailPoint_Service_Account' AND AssigneeId = :UserInfo.getUserId()];
        if (hasSailPointPermissions.size() > 0) {
            TriggerDispatcher.run(new UserTriggerHandler());
        }   
    }
}