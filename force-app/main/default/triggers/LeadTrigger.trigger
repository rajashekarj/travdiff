/**
* Trigger for Lead object. This will be fired whenever a DML is performed on Lead record.
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
*Shashank Agarwal       US59527              05/23/2019       Original Version
* -----------------------------------------------------------------------------------------------------------
*/

trigger LeadTrigger on Lead (Before Insert, Before Update, After Update, After Insert, 
                             Before Delete, After Delete, After Undelete)
{
     //Check to skip trigger org wide and individually.
    if(UtilityClass.triggerCheck('LeadTrigger'))
    {
        LeadTriggerHandler LeadTriggerHandlerObj = new LeadTriggerHandler();
        TriggerDispatcher.Run(LeadTriggerHandlerObj);
    }
}