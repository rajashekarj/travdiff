/**
 * TRAV_Agency_Delete_Tracking__c Trigger
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------
 * Pratik Jogwar            US79245            03/20/2020       Added code to afterInsert
 * -----------------------------------------------------------------------------------------------------------
 */
trigger TRAV_AgencyDeleteTrigger on TRAV_Agency_Delete_Tracking__c (after insert) {

    TRAV_AgencyDelTriggerHandler handler = new TRAV_AgencyDelTriggerHandler();
    if (Trigger.IsAfter){
        if(Trigger.IsInsert){
            handler.afterInsert(Trigger.new);
        }
    }
}