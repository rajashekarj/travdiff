/**
*    @author Sayanka Mohanty
*    @date   06/21/2019 
*    @description : This is the wrapper structure for creating Notifications or other object records

Modification Log:
------------------------------------------------------------------------------------
Developer                       Date                Description
------------------------------------------------------------------------------------
Sayanka Mohanty              02/07/2019          Original Version*
*/
public class NotificationWrapper {
    public  String type{get;set;}
    public  String Subject{get;set;}
    public  Id Owner{get;set;}
    //public  Datetime ActivityDateTime{get;set;}
    //public  Date ActivityDate{get;set;}
    public  Id objectId{get;set;}
    //public  Id WhatId{get;set;} 
    //public  Id WhoId{get;set;} 
    //public  String eventType{get;set;} 
    //public String chatterMessage{get;set;}
    //public String role{get;set;}
    public String roleProfile{get;set;}
    public Boolean containsHyperlink{get;set;}
    public List<Id> userIdList{get;set;}
    public Map<String,String> fieldValueMap{get;set;}
    public Map<String,String> mergeFieldAPIValMap{get;set;}
}