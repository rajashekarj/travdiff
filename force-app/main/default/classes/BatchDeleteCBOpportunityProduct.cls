/**
* Batch Class for removing purged or inactive data received from CB from Salesforce. This class will be 
* responsible for deleting those records.
*
* Modification Log    :
* ------------------------------------------------------------------------------------------------------
* Developer             User Story/Defect       Date            Description
* Erec Lawrie           US79515/US80830         04/13/2020      Original Version
* ------------------------------------------------------------------------------------------------------
*/
global class BatchDeleteCBOpportunityProduct implements Database.Batchable<sObject>, Database.Stateful {
    global final String CLASSNAME ='BatchDeleteCBOpportunityProduct';
    global Integer deletedRecordCount = 0;

    Renewal_Batch__c objBatch = Renewal_Batch__c.getOrgDefaults();
    DateTime lastRun = objBatch.TRAV_Last_Successful_CBRecDelete_Batch__c;
    
    /**
    * This method collect the batches of records or objects to be passed to execute
    * @param Database.BatchableContext
    * @return Database.QueryLocator record
    */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if (lastRun == null)
        {
            lastRun = DateTime.newInstance(2020, 1, 1, 23, 59, 59);
        }
        String METHODNAME = 'start';

        try{
            String strQuery = 'SELECT Id FROM OpportunityLineItem WHERE TRAV_Direct_Source_Sys_Code__c = \'CB MDM\' AND TRAV_Submission_Coverage_Status__c = \'Purged\' AND LastModifiedDate >= '+ lastRun.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
            return Database.getQueryLocator(strQuery);
        }catch(Exception objException){
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objException, null);
            return null;
        }
    }
    
    /**
    * This method process each batch of records for the implementation logic
    * @param Database.BatchableContext
    * @param List<OpportunityLineItem> 
    * @return void 
    */ 
    global void execute(Database.BatchableContext BC, List<OpportunityLineItem> scope) {
        String METHODNAME = 'execute';

        try{
            deletedRecordCount += scope.size();

            if(!scope.isEmpty()){
                Database.DeleteResult[] lstResult = Database.delete(scope, false);
                ExceptionUtility.logDMLError(CLASSNAME, METHODNAME, lstResult);
            }
            
            if(Test.isRunningTest()){
                throw new QueryException();
            }
        }catch(Exception objException){
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objException, null);
        }
    }
    
    /**
    * This method execute any post-processing operations
    * @param Database.BatchableContext record 
    * @return void 
    */ 
    global void finish(Database.BatchableContext BC) {
        System.debug('Finished ' + CLASSNAME + ': ' + deletedRecordCount + ' records deleted. Batch complete.');

        Renewal_Batch__c objBatch = Renewal_Batch__c.getOrgDefaults();
        objBatch.TRAV_Last_Successful_CBRecDelete_Batch__c = DateTime.now();
        upsert objBatch;
    }

/** ** THIS BATCH SHOULD NOT BE SCHEDULED *************    

	* This method execute this batch class when scheduled
    * @param SchedulableContext record 
    * @return void 
    global void execute(SchedulableContext schCon) {
        BatchDeleteCBOpportunityProduct batchObj  = new BatchDeleteCBOpportunityProduct(); 
        Database.executebatch(batchObj);
    }
**/
}