/**
 * Dispatcher method used to call methods based on Trigger Context
 *
 * @author Chris Aldridge, http://chrisaldridge.com/triggers/lightweight-apex-trigger-framework/
 * MIT License
 * 
 * Copyright (c) 2016 Chris Aldridge, Silver Softworks
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Pranil Thubrikar       US59671              05/09/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */

public class TriggerDispatcher{
    /*
        NOTE: Call this static method from Trigger and pass instance of TriggerHandler which implements ITriggerInterface
    */
    public static void run(ITriggerHandler handler){
        //Before Trigger Logic
        if (Trigger.IsBefore){
            if(Trigger.IsInsert){
                handler.beforeInsert(trigger.new);
            }
            if(Trigger.IsUpdate){
                handler.beforeUpdate(trigger.newMap, trigger.oldMap);
            }
            if(Trigger.IsDelete){
                handler.beforeDelete(trigger.oldMap);
            }
        }
        //After Trigger Logic
        if (Trigger.IsAfter){
            if(Trigger.IsInsert){
                handler.afterInsert(Trigger.newMap);
            }
            if(Trigger.IsUpdate){
                handler.afterUpdate(Trigger.newMap, Trigger.oldMap);
            }
            if(Trigger.IsDelete){
                handler.afterDelete(Trigger.oldMap);
            }
            if(Trigger.IsUndelete){
                handler.afterUndelete(Trigger.newMap);
            }
        }
    }
}