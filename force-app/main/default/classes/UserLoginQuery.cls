public class UserLoginQuery {
    @InvocableMethod
    public static List<List<UserLogin>> GetTodayUpdates()
    {
        List<UserLogin> LoginUpdates = [select userid,id,isfrozen,lastmodifieddate,LASTMODIFIEDBYID,ispasswordlocked from userlogin where lastmodifieddate = Today];
        List<List<UserLogin>> outerlist = new List<List<UserLogin>>();
        outerlist.add(LoginUpdates);
        return outerlist;
    }

}