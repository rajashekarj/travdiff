/**
* This class is the wrapper class for Lead Search and Pull Functionality
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect          		Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty        US73994/US73995/US73996          24/01/2020      Original Version
* -----------------------------------------------------------------------------------------------------------
*/

public class LeadSPCreateRequestWrapper{
public class PostalAddress {
        @auraEnabled
        public String stateCode{get;set;}
        @auraEnabled
        public String cityName{get;set;}
        @auraEnabled
        public String addressLine1{get;set;}
        @auraEnabled
        public String postalCode{get;set;}
    }
    @auraEnabled
    public String version{get;set;}
    @auraEnabled
    public AccountDetails accountDetails{get;set;}
    
    public class WebAddresses {
        @auraEnabled
        public String businessUrl{get;set;}
    }
    
    public class AccountDetails {
        @auraEnabled
        public String actionCode{get;set;}
        @auraEnabled
        public String phoneNumber{get;set;}
		@auraEnabled
        public String sourceSystem{get;set;}
        @auraEnabled
        public String accountNumber{get;set;}
        @auraEnabled
        public String accountName{get;set;}
        @auraEnabled
        public String NbrOfEE{get;set;}
        @auraEnabled
        public String Revenue{get;set;}
        @auraEnabled
        public PostalAddress postalAddress{get;set;}
        @auraEnabled
        public String accountUrl{get;set;}
		/*@auraEnabled
        public List<WebAddresses> webAddresses{get;set;}*/
    }
}
