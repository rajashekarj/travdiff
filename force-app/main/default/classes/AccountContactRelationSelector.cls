/**
 * This class is used as a Selector Layer for Contact object. 
 * All queries for Contact object will be performed in this class.
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Bharat Madaan            US63760          07/24/2019       Original Version
*/
public class AccountContactRelationSelector {
/**
* This is a method is used to get list of Contacts having a Account lookup
* @param : null
* @return List<AccountContactRelation> List of AccountContactRelation.
*/
    public static Map<String,AccountContactRelation> getACRWithAccountandOwner(Set<Id> accounId, Set<Id> ownerId)
    {
        Map<String,AccountContactRelation> mapStringTOACR = new Map<String,AccountContactRelation>();
        List<AccountContactRelation> listACR = [select Id, 
                               AccountId,
                               ContactId                 
                               from AccountContactRelation 
                               where AccountId IN : accounId
                               And Contact.TRAV_User__c IN : ownerId];
        if(!listACR.isEmpty() ){
            for(AccountContactRelation objACR : listACR){
                mapStringTOACR.put(''+ objACR.AccountId+ objACR.ContactId, objACR);
            }
        }
        return mapStringTOACR;
    }
}