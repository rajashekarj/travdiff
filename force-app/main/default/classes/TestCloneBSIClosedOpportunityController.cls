@isTest
public class TestCloneBSIClosedOpportunityController {
    static testmethod void testGetCloneBSIOpportunity() {
        
        //create the custom setting for general data setting
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Written,Declined,Quote Unsuccessful';
        insert objGeneralDataSetting;
        
        //create a pricebook
        Pricebook2 biPricebook = TestDataFactory.createTestPricebook('BSI - Financial Institution');
        List<Pricebook2> lstPricebook = new List<Pricebook2>{biPricebook};
            insert lstPricebook;

        //create account record
        Account objProspectAccount = TestDataFactory.createProspect();
        List<Account> lstAccount = new List<Account>{objProspectAccount};
            insert lstAccount;

        //Create List of opportunities
        List<Opportunity> lstOppty = TestDataFactory.createOpportunities(1,objProspectAccount.Id,null);
        lstOppty[0].Pricebook2ID = lstPricebook[0].Id;
        lstOppty[0].TRAV_Business_Unit__c = 'BSI-FI';
        insert lstOppty;

        //create 2 carriers (1 for use as incumbent, 1 for use as Competing)
        List<TRAV_Carrier__c> lstCarriers = TestDataFactory.createCarriers(new List<String> {'Competing Carrier', 'Incumbent Carrier', 'Unknown'});
        lstCarriers[2].TRAV_External_ID__c = Constants.UnknownCarrierExternalId;
        insert lstCarriers;

        List<TRAV_Carrier__c> cCarriers =  new List<TRAV_Carrier__c>();
        List<TRAV_Carrier__c> iCarriers =  new List<TRAV_Carrier__c>();
        for(Integer i=1; i<6; i++) { cCarriers.add(lstCarriers[0]); }
        for(Integer i=1; i<2; i++) { iCarriers.add(lstCarriers[1]); }
        List<TRAV_Opportunity_Carrier__c> lstCompetingOppCarriers = TestDataFactory.createOpportunityCarriers(lstOppty, cCarriers, Constants.COMPETINGCARRIER);
        List<TRAV_Opportunity_Carrier__c> lstIncumbentOppCarriers = TestDataFactory.createOpportunityCarriers(lstOppty, iCarriers, Constants.INCUMBENTCARRIER);
        List<TRAV_Opportunity_Carrier__c> lstOpportunityCarriers = new List<TRAV_Opportunity_Carrier__c>();
        lstOpportunityCarriers.addAll(lstCompetingOppCarriers);
        lstOpportunityCarriers.addAll(lstIncumbentOppCarriers);
        insert lstOpportunityCarriers;

        //Get teh standard pricebook Id to create standard pricebookentry for the lsit of products
        Id pricebookId = Test.getStandardPricebookId();

        //create 5 products
        List<Product2> lstProducts = TestDataFactory.createProducts(5);
        insert lstProducts;

        //Standard PricebookEntry
        List<PricebookEntry> pricebookEntryList = TestDataFactory.createPricebookEntries(lstProducts, pricebookId);
        insert pricebookEntryList;

        List<PricebookEntry> lstBIPricebookEntries = TestDataFactory.createPricebookEntries(lstProducts, lstPricebook[0].Id);
        insert lstBIPricebookEntries;


        List<OpportunityLineItem> lstOpportunityLineItems = new List<OpportunityLineItem>();
        for(Opportunity opp : lstOppty) {
            for(Product2 product : lstProducts) {
                OpportunityLineItem lineItem = new OpportunityLineItem(
                    OpportunityId = opp.Id,
                    Product2Id = product.Id,
                    TRAV_Incumbent_Insurance_Carrier_Name__c=lstCarriers[1].Id,
                    TRAV_Coverage_Premium__c = 100,
                    PricebookEntryId = lstBIPricebookEntries[0].Id
                );
                lstOpportunityLineItems.add(lineItem);
            }
        }

        insert lstOpportunityLineItems;
        
        User objUsr = TestDataFactory.createTestUser('BI - National Accounts','TestNA2','LastNameNA2');
        insert objUsr;
        
        OpportunityTeamMember objOpportunityTeam = new OpportunityTeamMember();
        objOpportunityTeam.OpportunityId = lstOppty[0].Id;
        objOpportunityTeam.UserId = objUsr.Id;
        Test.startTest();
        insert objOpportunityTeam;
        
        Contact objCon = TestDataFactory.createAgencyNonLicensed(lstAccount[0].id);
        objCon.TRAV_External_Id__c = 'TRV00005400056';
        insert objCon;
        
        OpportunityContactRole objOpRoleRec = new OpportunityContactRole(ContactId = objCon.Id, opportunityId = lstOppty[0].Id);
        database.insert(objOpRoleRec);
        
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.snote',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        
        ContentNote objContentNote = new ContentNote();
        objContentNote.Title = 'test Note 1';
        String body = 'Test Body';
        objContentNote.Content = Blob.valueOf(body.escapeHTML4());
        insert objContentNote;
        
        ContentVersion objContentVersion = [Select FileType,ContentDocumentId From ContentVersion Where Id = :contentVersionInsert.Id LIMIT 1];
        
        
        ContentDocumentLink objContentDocumentLink = new ContentDocumentLink();
        objContentDocumentLink.ContentDocumentId = objContentVersion.ContentDocumentId;
        objContentDocumentLink.LinkedEntityId = lstOppty[0].Id;
        objContentDocumentLink.ShareType = 'V';
        insert objContentDocumentLink;
        
        CloneBSIClosedOpportunityController.getCloneBSIOpportunity(lstOppty[0].Id);

       
        
    }

}