public with sharing class OpportunityLineItemFieldAccessor {
    @AuraEnabled(cacheable=true)
    public static list<String> getTRAV_BiOppProdFieldset() {
        
        List<string> fields = new List<string>();
        
        for(Schema.FieldSetMember f : SObjectType.OpportunityLineItem.fieldSets.TRAV_BiOppProdFieldset.getFields()) {
            fields.add(f.getFieldPath());
        }
        
        return fields;
    }
    
    @AuraEnabled(cacheable=true)
    public static list<String> getTRAV_FieldsetByParameter(String objectName, String fieldsetName,string IdOpportuntiyLineItem) {
        OpportunityLineItem objOpptyLineItem = new OpportunityLineItem();
        List<string> fields = new List<string>();
        if(string.isnotBlank(IdOpportuntiyLineItem)){
            objOpptyLineItem =OpportunityLineItemSelector.getOpportunityLienItem(IdOpportuntiyLineItem);
        }
        
        Map<String,Schema.SObjectType> gdmap = Schema.GetGlobalDescribe();
        Schema.SObjectType sotObj = gdmap.get(objectName);
        Schema.DescribeSObjectResult sotObjResult = sotObj.GetDescribe();
        Schema.FieldSet fsObj = sotObjResult.FieldSets.getMap().get(fieldsetName);
        
        for(Schema.FieldSetMember f : fsObj.getFields()) {
            if(String.isnotBlank(objOpptyLineItem.Product2.name) && !Constants.lstProductstoExclude.contains(objOpptyLineItem.Product2.Name) && f.getFieldPath() == 'TRAV_Insured_Asset_Under_Management__c'){
                continue;
            }else if(String.isnotBlank(objOpptyLineItem.Product2.name) && !Constants.lstProductstoInclude.contains(objOpptyLineItem.Product2.Name) && f.getFieldPath() == 'TRAV_Products_Excluded__c'){
                continue;
            }else{
                fields.add(f.getFieldPath());
            }
        }
        
        return fields;
    }
}