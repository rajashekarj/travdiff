/**
 * Test class for Notification Framework

*
 * @Author : Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Sayanka Mohanty        		              07/02/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
@isTest
public class TestOpportunityNotifications {
    @testSetup static void createTestData(){
        //create the custom setting for opportunity trigger
			TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
            objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
            objCustomsetting.TRAV_Trigger_Name__c = 'OpportunityTrigger';
            insert objCustomsetting;
            
            //create the custom setting for general data setting
            General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
            objGeneralDataSetting.NumberOfMonths__c = 365;
            objGeneralDataSetting.ClosedStages__c = 'Written,Declined,Quote Unsuccessful';
            insert objGeneralDataSetting;
        
        //Add Account
        Account a = TestDataFactory.createProspect();
        insert a;
        //Add opportunity
        List<Opportunity> finalOpptyList = TestDataFactory.createOpportunities(2, a.Id, 'BI-NA');
            finalOpptyList[0].StageName = 'Submission';
        finalOpptyList[1].StageName = 'Prospect';
        insert finalOpptyList;
        
        system.assert(finalOpptyList[0].StageName == 'Submission');
       
        //add Opportunity Team Member
        List<OpportunityTeamMember> finalOpptyTeamList = new List<OpportunityTeamMember>();
        //Select a user of BI-support profile
        User u = [SELECT Id, Name FROM User WHERE Profile.name = 'BI - Support' and isActive = true LIMIT 1];
        User u2 = [SELECT Id, Name FROM User WHERE Profile.name = 'BI - National Accounts' and isActive = true LIMIT 1];
        OpportunityTeamMember ot = TestDataFactory.createOpptyTeammember(u.Id, finalOpptyList[0].id, 'Cure Underwriting Officer');
        OpportunityTeamMember ot2 = TestDataFactory.createOpptyTeammember(u2.Id, finalOpptyList[1].id, 'Account Manager');
        finalOpptyTeamList.add(ot);
        finalOpptyTeamList.add(ot2);
        if(!finalOpptyTeamList.isEmpty()){
            insert finalOpptyTeamList;
        }
		system.assert(finalOpptyTeamList[0].TeamMemberRole == 'Cure Underwriting Officer');        
    }
    static testmethod void testNotificationOnOppty1(){
        //Unit test for US62421, Stage change to Underwrite and Price
        Test.startTest();
        // createTestData();
        Opportunity opp = [SELECT Id,StageName FROM Opportunity WHERE StageName = 'Submission' LIMIT 1];
        system.debug('**opp**'+opp);
        opp.StageName = 'Underwrite & Price';
        update opp;
        Test.stopTest();
    }
    static testmethod void testNotificationOnOppty2(){
        //Unit test for US62421, Stage change to Bound
        Test.startTest();
        insert new TRAV_Bypass_Settings__c(TRAV_Skip_Validation_Rules__c = true, SetupOwnerId = userInfo.getUserId());
        // createTestData();
        Opportunity opp = [SELECT Id,StageName FROM Opportunity WHERE StageName = 'Submission' LIMIT 1];
        system.debug('**opp**'+opp);
        opp.StageName = 'Bound';
        update opp;
        Test.stopTest();
    }
    static testmethod void testNotificationOnOppty3(){
        //Unit test for US62421, Stage change to Bound
        Test.startTest();
        insert new TRAV_Bypass_Settings__c(TRAV_Skip_Validation_Rules__c = true, SetupOwnerId = userInfo.getUserId());
        // createTestData();
        Opportunity opp = [SELECT Id,StageName FROM Opportunity WHERE StageName = 'Prospect' LIMIT 1];
        system.debug('**opp**'+opp);
        opp.StageName = 'Submission';
        update opp;
        Test.stopTest();
    }
}