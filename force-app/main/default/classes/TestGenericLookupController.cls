/**
* Test class for GenericLookupController Class
*
* @Author : Deloitte Consulting
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Tejal Pradhan            US68408            10/07/2019       Original Version
* Erec Lawrie              US78212            03/02/2020       Updated test to cover for searching both licensed and non-licensed contacts
* -----------------------------------------------------------------------------------------------------------
*/

@isTest
private class TestGenericLookupController {
    
    static testmethod void testDIprof() {
        TRAV_Exception_Log__c objException = new TRAV_Exception_Log__c();
        Profile p = [SELECT Id FROM Profile WHERE Name='Devops Administrator']; 
      User u = new User(Alias = '123te', Email='standarduser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='123435te@testte.com');

      System.runAs(u) {
         
        objException.Class_Name__c = '';
        objException.Method_Name__c = '';
        objException.Error_Message__c = '';
        if(objException != null){
            ExceptionUtility.logIntegrationerror(null, null, null);
        }
      }
    }

    /**
    * This method will test scenarios when a contact record is searched
    * @return void
    */
    static testmethod void testSearchSobjectRecords() {
        
        //create account record
        Account objAgencyAcc = TestDataFactory.createProspect(); 
        insert objAgencyAcc;
        
        
        //create account record
        Account objTPAAcc = TestDataFactory.createTPAAccount(); 
        insert objTPAAcc;
        //create opportunity record
        Opportunity objOpp = TestDataFactory.createOppty('Prospect','Test Opp',objAgencyAcc.Id); 
        insert objOpp;
        
        //create Contact record
        List<Contact> lstContact = TestDataFactory.createTravelersContact(4);
        lstContact[0].accountId = objAgencyAcc.Id; 
        lstContact[0].RecordTypeId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(Constants.CustomerRecordType).getRecordTypeId();
        lstContact[1].accountId = objAgencyAcc.Id; 
        lstContact[1].Email = 'nl_testEmail@test.com'; 
        lstContact[1].RecordTypeId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(Constants.agencyNonLicensedRecordType).getRecordTypeId();
        lstContact[2].accountId = objAgencyAcc.Id; 
        lstContact[2].RecordTypeId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(Constants.TPARcordType).getRecordTypeId();
        lstContact[3].accountId = objAgencyAcc.Id; 
        lstContact[3].Email = 'l_testEmail2@test.com'; 
        lstContact[3].RecordTypeId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(Constants.agencyLicensedRecordType).getRecordTypeId();
        insert lstContact;
        
        Test.startTest();
        String strResultsCustomer = GenericLookupController.searchSobjectRecords('Contact','Id','Name',5,'LastName','Name',Constants.CustomerRecordType,String.valueOf(objAgencyAcc.Id) );
        String strResultsTPA = GenericLookupController.searchSobjectRecords('Contact','Id','Name',5,'LastName','Name',Constants.TPARcordType,String.valueOf(objAgencyAcc.Id) );
        String strResultsAcc = GenericLookupController.searchSobjectRecords('Account','Id','Name',5,'Name','Name','TPA','' );   

        String strResultsNonLicensedAgency = GenericLookupController.searchSobjectRecords('Contact','Id','Name',5,'LastName','Name',Constants.agencyNonLicensedRecordType,String.valueOf(objAgencyAcc.Id) );
        String strResultsLicensedAgency = GenericLookupController.searchSobjectRecords('Contact','Id','Name',5,'LastName','Name',Constants.agencyLicensedRecordType,String.valueOf(objAgencyAcc.Id) );

        String validRecordTypes = Constants.agencyNonLicensedRecordType + ';' + Constants.agencyLicensedRecordType;
        String strResultsNonLicensedAndLicensedAgency = GenericLookupController.searchSobjectRecords('Contact','Id','Name',5,'LastName','Name',validRecordTypes,String.valueOf(objAgencyAcc.Id) );
        Test.stopTest();

        System.assertNotEquals(strResultsCustomer, null);
        System.assertNotEquals(strResultsNonLicensedAgency, null);
        System.assertNotEquals(strResultsLicensedAgency, null);
        System.assertNotEquals(strResultsNonLicensedAndLicensedAgency, null);
        System.assertNotEquals(strResultsTPA, null);
    }
}