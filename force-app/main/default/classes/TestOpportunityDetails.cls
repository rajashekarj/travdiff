/**
* This class is the Test Class for OpportunityDetails class
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------         
* Tejal Pradhan           NA                                08/02/2019       Created this class with methods - testGetOpportunityStage,testGetOpportunityProducts,
*                                                                                                                                                                                                                                                             testSaveOpportunityProducts,testGetClosureList,testGetSubReasonList,testCheckForOtherAsReason                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      and checkForPricingAsReason methods
* -----------------------------------------------------------------------------------------------------------             
*/

@isTest

public class TestOpportunityDetails{
    
    /* This is a test method for the OpportunityDetails.getOpportunityStage method
Test Scenario - Check if the stage of the Oppty retrieved is as per the data created
*/   
    @isTest
    Static void testGetOpportunityStage(){
        //Create Test Data
        Account acctRecData = TestDataFactory.createProspect();
        insert acctRecData;
        List<Opportunity> opptyListData = TestDataFactory.createOpportunities(1,acctRecData.Id,null);
        insert opptyListData;
        
        Test.startTest();
        String opptyStage = OpportunityDetails.getOpportunityStage(opptyListData[0].Id);
        Test.stoptest();
        
        /*************Assert************/
        System.assertEquals(opptyStage,'Prospect');
    }
    
    /* This is a test method for the OpportunityDetails.getClosureList method 
Test Scenario - Check if the different closure lists created are as per the different close reasons
*/   
    @isTest
    Static void testGetClosureList(){
        Test.startTest();
        List<String> closureListForWritten = OpportunityDetails.getClosureList('Written');
        List<String> closureListForDeclined = OpportunityDetails.getClosureList('Declined');
        List<String> closureListForQuoteUnsuccessful = OpportunityDetails.getClosureList('Quote Unsuccessful');
        Test.stoptest();
        
        /*************Assert************/
        List<String> genericList = Constants.GenericReasonList;
        List<String> wonList = Constants.WonReasonList;
        List<String> declinedList = Constants.DeclinedReasonList;
        List<String> quoteList = Constants.QuoteUnSucReasonList;
        List<String> prodCloseListForWritten = new List<String>();
        prodCloseListForWritten.addAll(genericList);
        prodCloseListForWritten.addAll(wonList);
        
        List<String> prodCloseListForDeclined = new List<String>();
        prodCloseListForDeclined.addAll(genericList);
        prodCloseListForDeclined.addAll(declinedList);
        
        List<String> prodCloseListForQuoteUnsuccessful = new List<String>();
        prodCloseListForQuoteUnsuccessful.addAll(genericList);
        prodCloseListForQuoteUnsuccessful.addAll(quoteList);
        
        System.assertEquals(closureListForWritten,prodCloseListForWritten);
        System.assertEquals(closureListForDeclined,prodCloseListForDeclined);
        System.assertEquals(closureListForQuoteUnsuccessful,prodCloseListForQuoteUnsuccessful);
    }
    
    /* This is a test method for the OpportunityDetails.getSubReasonList method 
Test Scenario - Check if the sub reasons list created is as per the one expected for Quote Unsuccessful close reason
*/     
    @isTest
    Static void testGetSubReasonList(){
        //Create Test Data
        List<String> opptyProductReasonList = new List<String>{'Coverage','Pricing'};
            
            Test.startTest();
        List<String> subReasonList = OpportunityDetails.getSubReasonList(opptyProductReasonList,'Quote Unsuccessful');
        Test.stoptest();
        
        /*************Assert************/
        List<String> coverageList = Constants.CoverageSubReasonList;
        List<String> priceList = Constants.PriceSubReasonList;
        List<String> prodSubReasonList = new List<String>();
        prodSubReasonList.addAll(coverageList);
        prodSubReasonList.addAll(priceList);
        
        System.assertEquals(subReasonList,prodSubReasonList);
    }
    
    /* This is a test method for the OpportunityDetails.getOpportunityProducts method 
Test Scenario - Check if the Oppty Line Items are retrieved for the given Oppty data 
*/      
    @isTest
    Static void testGetOpportunityProducts(){
        //Create Test Data
        Account acctRecData = TestDataFactory.createProspect();
        insert acctRecData;
        List<Opportunity> opptyListData = TestDataFactory.createOpportunities(1,acctRecData.Id,null);
        insert opptyListData;
        List<OpportunityLineItem> opptyLineItemData = TestDataFactory.createOpportunityLineItem(opptyListData[0].Id, 2);
        insert opptyLineItemData;
        
        Test.startTest();
        List<OpportunityLineItem> opptyLineItems = OpportunityDetails.getOpportunityProducts(opptyListData[0].Id);
        Test.stoptest();
        
        /*************Assert************/
        System.assertNotEquals(opptyLineItems,null);
    }
    
    /* This is a test method for the OpportunityDetails.saveOpportunityProducts method 
Test Scenario - Check if the Closed Reasons and Closed SUb Reasons are getting concatenated as expected
*/      
    @isTest
    Static void testSaveOpportunityProducts(){
        //Create Test Data
        Account acctRecData = TestDataFactory.createProspect();
        insert acctRecData;
        List<Opportunity> opptyListData = TestDataFactory.createOpportunities(1,acctRecData.Id,null);
        insert opptyListData;
        List<OpportunityLineItem> opptyLineItemData = TestDataFactory.createOpportunityLineItem(opptyListData[0].Id, 2);
        insert opptyLineItemData;
        List<String> closedReasonsList = new List<String>{'Claim Service','Collateral','Coverage'};
            List<String> closedSubReasonsList = new List<String>{'Discrimination','Per Location','Pollution'};
                
                Test.startTest();
        List<OpportunityLineItem> opptyLineItems = OpportunityDetails.saveOpportunityProducts(opptyLineItemData,closedReasonsList,closedSubReasonsList);
        Test.stoptest();
        
        /*************Assert************/
        System.assertEquals(opptyLineItems[0].TRAV_Closed_Reason__c,'Claim Service;Collateral;Coverage');
        System.assertEquals(opptyLineItems[0].TRAV_Closed_Sub_Reason__c,'Discrimination;Per Location;Pollution');
    }   
    
    /* This is a test method for the OpportunityDetails.checkForOtherAsReason method 
Test Scenario - Check if Other is mentioned as the oppty product reason 
*/      
    @isTest
    Static void testCheckForOtherAsReason(){
        //Create Test Data
        List<String> opptyProductReasonList = new List<String>{'Coverage','Other'};
            Test.startTest();
        Boolean isOther = OpportunityDetails.checkForOtherAsReason(opptyProductReasonList);
        Test.stoptest();
        
        /*************Assert************/
        System.assertEquals(isOther,true);
    }
    
}