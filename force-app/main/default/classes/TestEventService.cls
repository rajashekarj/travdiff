/**
 * Test class for EventService Class
 *
 * @Author : Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------
 * Isha Shukla            US62215              06/25/2019       Original Version
 * Shashank Agarwal       US63739              08/02/2019       Added Method testInsertEventRelationForStewardship
 * Shashank Agarwal       Test Coverage        08/05/2019       Added Method testEmptyMethodsForTriggerHandler
-----------------------------------------------------------------------------------------------------------
 */
@isTest
private class TestEventService {
    /**
    * This method will test scenarios when event is inserted, stewardship record id is stored on TRAV_Stewardship_Id__c field
    * @param no parameters
    * @return void
    */
    static testmethod void testEventInsertionCase() {
        //create the custom setting for Opportunity trigger
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'EventTrigger';
        insert objCustomsetting;
         //create the custom setting for general data setting
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Deferred;Written;Declined;Quote Unsuccessful';
        insert objGeneralDataSetting;

         //create account record
        Account prospectAccount = TestDataFactory.createProspect();
        List<Account> lstAccount = new List<Account>{prospectAccount};
        insert lstAccount;

        //Creating opportunity records
        List<Opportunity> lstOpportunity = TestDataFactory.createOpportunities(1, prospectAccount.Id,'BI-NA');
        insert lstOpportunity;

        //Creating event
        List<Event> lstEvent = TestDataFactory.createEvent(5, UserInfo.getUserId(), lstOpportunity[0].Id);
        Test.startTest();
        insert lstEvent;
        Test.stopTest();
        System.assertEquals([SELECT
                                  TRAV_Stewardship_Id__c
                             FROM
                                  Event
                             WHERE
                                  WhatId =: lstOpportunity[0].Id LIMIT 1].TRAV_Stewardship_Id__c != null,true);
    }
    /**
    * This method will test scenarios when opportunity team member is Inserted or updated for stewradship Events.
    * @param no parameters
    * @return void
    */
    static testmethod void testInsertEventRelationForStewardship() {
        List<User> lstUser = new List<User>();
        User objUser1 = TestDataFactory.createTestUser('BI - National Accounts','testUser1','testLastName1');
        User objUser2 = TestDataFactory.createTestUser('BI - National Accounts','testUser2','testLastName2');
        lstUser.add(objUser1);
        lstUser.add(objUser2);
        if(!lstUser.isEmpty()){
            insert lstUser;
        }
        User objUser3 = TestDataFactory.createTestUser('System Administrator','testUser3','testLastName3');
        
        system.runAs(objUser3){
            //create the custom setting for OpportunityTeam trigger
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'OpportunityTeamMemberTrigger';
        insert objCustomsetting;

        //create the custom setting for general data setting
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Deferred;Written;Declined;Quote Unsuccessful';
        insert objGeneralDataSetting;

        //create agency account record
        Account objAccount = TestDataFactory.createProspect();
        insert objAccount;

        Opportunity objOpportunity = TestDataFactory.createOppty('Written', 'Test oppty A',objAccount.Id);
        objOpportunity.OwnerId = objUser1.id;
        objOpportunity.TRAV_Policy_Effective_Date__c  = (system.today()).addDays(-30);
        objOpportunity.TRAV_Policy_Expiration_Date__c = System.today().addDays(365);
         Date renewalDate = System.today().addDays(100);
        objOpportunity.TRAV_Renewal_Creation__c = renewalDate;
        insert objOpportunity;


        System.debug('Opportunity '+objOpportunity);
        List<OpportunityTeamMember> lstOpportunityTeamFinal = new List<OpportunityTeamMember>();
        //OpportunityTeamMember objOpportunityTeamMember1 = TestDataFactory.createOpptyTeammember(objUser1.Id, objOpportunity.id, 'RMIS Information Account Executive');
        OpportunityTeamMember objOpportunityTeamMember2 = TestDataFactory.createOpptyTeammember(objUser2.Id, objOpportunity.id, 'Account Manager');
        //lstOpportunityTeamFinal.add(objOpportunityTeamMember1);
        lstOpportunityTeamFinal.add(objOpportunityTeamMember2);
        if(!lstOpportunityTeamFinal.isEmpty()){
            insert lstOpportunityTeamFinal;
        }


        List<Event> lstEvent = TestDataFactory.createEvent(1, objUser1.Id,  objOpportunity.id);

        Test.startTest();
        insert lstEvent;

       /*  System.assertEquals([SELECT
                                  RelationId
                             FROM
                                  EventRelation
                             WHERE
                                  RelationId = :objUser2.Id
                             ].size() != 0 ,true);*/
            Test.stopTest();
        }

    }

    /**
    * This method is added to cover the coverage for TriggerHandler Class.
    * @param no parameters
    * @return void
    */
    static testmethod void testEmptyMethodsForTriggerHandler() {
        User objUser1 = TestDataFactory.createTestUser('BI - National Accounts','testUser1','testLastName1');
        insert objUser1;
        User objUser2 = TestDataFactory.createTestUser('System Administrator','testUser2','testLastName2');
        
        system.runAs(objUser2){
        //create the custom setting for OpportunityTeam trigger
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'OpportunityTeamMemberTrigger';
        insert objCustomsetting;

        //create the custom setting for general data setting
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Deferred;Written;Declined;Quote Unsuccessful';
        insert objGeneralDataSetting;

        

        //create agency account record
        Account objAccount = TestDataFactory.createProspect();
        insert objAccount;

        Opportunity objOpportunity = TestDataFactory.createOppty('Submission', 'Test oppty A',objAccount.Id);
        objOpportunity.OwnerId = objUser1.id;
        insert objOpportunity;

        List<Event> lstEvent = TestDataFactory.createEvent(1, objUser1.Id,  objOpportunity.id);
        insert lstEvent;

        Set<Id> setEventId = new Set<Id>{lstEvent[0].Id};
        EventSelector.getEventAddingErrorOnNonEditableNotes(setEventId);

        Test.startTest();
        delete lstEvent;
        undelete lstEvent;
        Test.stopTest();
}
    }

    /**
    * This method will test scenarios when event is related to agency contact or not
    * @param no parameters
    * @return void
    */
    static testmethod void testEventFlagCase() {
        //create the custom setting for Opportunity trigger
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'EventTrigger';
        insert objCustomsetting;
        //create the custom setting for general data setting
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Deferred;Written;Declined;Quote Unsuccessful';
        insert objGeneralDataSetting;

         //create account record
        Account prospectAccount = TestDataFactory.createProspect();
        List<Account> lstAccount = new List<Account>{prospectAccount};
        insert lstAccount;


        //creating agency contact
        Id objRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('TRAV_Agency_Licensed_Contact').getRecordTypeId();
       // Id objTRAVRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Travelers Contact').getRecordTypeId();
        List<Contact> lstContact = TestDataFactory.createTravelersContact(2);
        lstContact[0].RecordTypeId = objRecordTypeId;
        lstContact[1].RecordTypeId = objRecordTypeId;
        lstContact[0].AccountId = lstAccount[0].Id;
        lstContact[1].AccountId = lstAccount[0].Id;
        insert lstContact;

        //Creating event
        List<Event> lstEvent = new List<Event>();
        Event objEvent = new Event();
            objEvent.OwnerId = UserInfo.getUserId(); //user id
            objEvent.WhoId = lstContact[0].Id; //record id
            objEvent.DurationInMinutes = 60;
            objEvent.ActivityDateTime = System.now();
        Event objEvent2 = new Event();
            objEvent2.OwnerId = UserInfo.getUserId(); //user id
            objEvent2.WhoId = lstContact[1].Id; //record id
            objEvent2.DurationInMinutes = 60;
            objEvent2.ActivityDateTime = System.now();
        lstEvent.add(objEvent);
        lstEvent.add(objEvent2);
        Test.startTest();

        insert lstEvent;
		objEvent2.DurationInMinutes = 55;
        update objEvent2;
        Test.stopTest();
        System.assertEquals([SELECT
                                  TRAV_Related_Agency_Contact__c
                             FROM
                                  Event
                             WHERE
                                  WhoId =: lstContact[0].Id LIMIT 1].TRAV_Related_Agency_Contact__c ,true);
        delete objEvent2;
    }
}