/**
 * This class is used as a Selector Layer for Opportunity object.
 * All queries for Opportunity object will be performed in this class.
 *
 *
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------
 * Isha Shukla            US60806         	 05/31/2019       Original Version
 * Isha Shukla            US62980            06/28/2019 	  Added SEMICOLON variable
 * Bharat Madaan		  US63301			 07/16/2019		  Added method getDeferredOpportunities
 * Bharat Madaan		  US63760			 07/16/2019		  Added method getBINAOpportunities
 * Shruti Gupta			  US63931			 07/26/2019		  Added method getOppLineItem
 * Isha Shukla			  US64798			 08/08/2019		  Updated method getOpportunities to include TRAV_Agency_Broker__c field
 * Shashank Agarwal       US64825            08/08/2019       Added method getOpportunitiesForTPAError
 * Bharat Madaan          US65898            09/05/2019       Added method getOpportunitiesWithAccountAndUser
 * Shashank Agarwal       US73728            01/24/2020       Added method getOpportunitiesOwner
 * Ben Climan             US76573            02/13/2020       Updated getOpportuntiyProductNames method query to incl. TRAV_Direct_Source_Sys_Code__c field (to identify CB records)
 -----------------------------------------------------------------------------------------------------------
 */
public class OpportunitySelector {

    public enum ExcludeOpportunityBy {NONE, CB_MDM}

    /**
* This is a method is used to get list opportunities from ID
* @param string Opportunities id set
* @return List<Opportunity> List of Opportunities based on the SOQL query provided as an input parameter.
*/
    public static List<Opportunity> getOpportunities(Set<Id> setOfOpptyId) {
        String strClosedStages = General_Data_Setting__c.getOrgDefaults().ClosedStages__c;
        List<String> lstClosedStages = strClosedStages.split(Constants.SEMICOLON);
        final string METHODNAME = 'getOpportunities';
        final string CLASSNAME = 'OpportunitySelector';
        Integer intNumberOfMonths = (Integer)General_Data_Setting__c.getOrgDefaults().NumberOfMonths__c;
        String strQuery = 'SELECT Id, TRAV_Product_Names__c, AccountId,TRAV_Agency_Broker__c FROM Opportunity WHERE Id IN :setOfOpptyId';
        strQuery +=  ' AND ( StageName NOT IN :lstClosedStages OR ';
        strQuery += '( isClosed = true AND CloseDate >= LAST_N_DAYS:'+intNumberOfMonths+') )';
        List<Opportunity> lstOpportunity = new List<Opportunity>();
        try {
            lstOpportunity = Database.query(strQuery);
			if(Test.isRunningTest()){
                throw new QueryException();
            }
        }
        catch(QueryException objExp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);

        } // End Catch

        return lstOpportunity;
    } //end of method getLeads
    /**
* This is a method is used to get list opportunities from ID
* @param string Opportunities id set
* @return List<Opportunity> List of Opportunities based on the SOQL query provided as an input parameter.
*/
    public static List<Opportunity> getRenewalOpportunities(Set<Id> setOpptyId) {
        final string METHODNAME = 'getRenewalOpportunities';
        final string CLASSNAME = 'OpportunitySelector';
        String strQuery = 'Select Id, TRAV_Renewal_Opportunity__c,TRAV_Deferred_From_Opportunity__c From Opportunity Where Id IN: setOpptyId OR TRAV_Deferred_From_Opportunity__c IN: setOpptyId';
        List<Opportunity> lstOpportunity = new List<Opportunity>();
        try {
            lstOpportunity = Database.query(strQuery);
			if(Test.isRunningTest()){
                throw new QueryException();
            }
        }
        catch(QueryException objExp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);

        } // End Catch
        return lstOpportunity;
    } //end of method getLeads

    /**
* This is a method is used to get list opportunities from ID
* @param string Opportunities id set
* @return List<Opportunity> List of Opportunities based on the SOQL query provided as an input parameter.
*/
   public static String getOpportunitiesForRenewal(String objName,DateTime dtFrom,DateTime dtTo,DateTime dtFromProspectDate) {
    //String strAllFields = UtilityClass.getAllFieldsSOQL('Opportunity');
    String strQueryOpp = 'Select ID ' +
     ' From Opportunity' +
     ' WHERE ( TRAV_Policy_Expiration_Date__c != NULL'+
     ' AND TRAV_Renewal_Creation__c > :dtFrom'+
     ' AND TRAV_Renewal_Creation__c <= :dtTo'+
     ' AND TRAV_Non_Renewal_Advisement_Indicator__c = false'+
     ' AND StageName ='+'\''+Constants.CLOSEDWON+'\''+
     ' AND TRAV_Business_Unit__c ='+'\''+Constants.BI_NA+'\') OR (TRAV_Defferal_Date__c > :dtFromProspectDate'+
     ' AND TRAV_Defferal_Date__c <= :dtTo AND TRAV_Defferal_Date__c != NULL'+
     ' AND (StageName = \''+Constants.CLOSEDDEFERRED+'\' OR StageName = \''+Constants.LOSTTOCOMPETITION+'\' OR StageName = \''+Constants.DIDNOTACQUIRE+'\' OR '+
     ' ((StageName = \''+Constants.OUTOFAPPETITE+'\' OR StageName = \''+Constants.STAGELOST+'\' OR'+
     ' StageName = \''+Constants.DECLINED+'\' OR StageName = \''+Constants.QUOTEUNSUCCESSFUL+'\') AND TRAV_Business_Unit__c = \'BI-NA\') ) ) ';
 system.debug('strQueryOpp-->>'+strQueryOpp);
 return strQueryOpp;
}
/**
* This is a method is used to get list opportunities from ID
* @param string Opportunities id set
* @return List<Opportunity> List of Opportunities based on the SOQL query provided as an input parameter.
*/
   public static String getOpptyForBatchCreateOpportunityOnExpitationDate(Set<Id> settempOpportunityId) {
       String strQueryOpp = 'Select '+Constants.OPPLINEITEMQUERY +' , '+Constants.CONTENTDOCUMENTLINKQUERY+','+Constants.OPPTEAMMEMQUERY +','+Constants.OpportunityFieldForRenewalBatchQuery+' from Opportunity where id in :settempOpportunityId';
       return strQueryOpp;
   }


    /**
* This is a method is used to get list of deferred opportunities from Id
* @param string Opportunities id set
* @return List<Opportunity> List of Opportunities based on the SOQL query provided as an input parameter.
*/
    public static List<Opportunity> getDeferredOpportunities(Set<Id> opptyId) {
        final string METHODNAME = 'getDeferredOpportunities';
        final string CLASSNAME = 'OpportunitySelector';
        String strQuery = 'Select Id, TRAV_Deferred_From_Opportunity__c,TRAV_Policy_Effective_Date__c From Opportunity Where TRAV_Deferred_From_Opportunity__c =: opptyId';
        List<Opportunity> listOpportunity = new List<Opportunity>();
        try {
            listOpportunity = Database.query(strQuery);
			if(Test.isRunningTest()){
                throw new QueryException();
            }
        }
        catch(QueryException objExp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);

        } // End Catch
        return listOpportunity;
    } //end of method TRAV_Deferred_From_Opportunity__c

    /**
* This is a method is used to get list opportunities from ID
* @param string Opportunities id set
* @return List<Opportunity> List of Opportunities based on the SOQL query provided as an input parameter.
*/
    public static String getBINAOpportunities(String objName) {
        String strAllFields = UtilityClass.getAllFieldsSOQL(objName);
        String strQueryOpp = 'Select AccountId,TRAV_Agency_Broker__c,OwnerId,Owner.UserName,Owner.FirstName,Owner.LastName,Owner.Profile.Name' +
            ' From Opportunity' +
            ' WHERE TRAV_Business_Unit__c ='+'\''+Constants.BI_NA+'\'  AND TRAV_Agency_Broker__c != NULL' +
            ' AND StageName ='+'\''+Constants.SUBMISSION+ '\'';
        return strQueryOpp;
    }
/**
* This is a method is used to get list opportunities from ID
* @param string Opportunities id set
* @return List<Opportunity> List of Opportunities based on the SOQL query provided as an input parameter.
*/
    public static List<Opportunity> getOpportunitiesWithAccountAndUser(Set<Id> setAccountId,Set<Id> setOwnerId) {
        List<Opportunity> lstOpportunity = new List<Opportunity>();
        lstOpportunity= [Select id,
                                OwnerId,
                                TRAV_Agency_Broker__c
                                from Opportunity
                                Where OwnerId IN :setOwnerId
                                AND TRAV_Agency_Broker__c IN :setAccountId];

        return lstOpportunity;
    }

    /**
* This is a method is used to get listof renewal opportunities from set of ID
* @param Set Opportunities id set
* @return List<Opportunity> List of Opportunities based on the SOQL query provided as an input parameter.
*/
    public static List<Opportunity> getlstRenewalOppty(Set<ID> setOpptyId) {

        final string METHODNAME = 'getRenewalOpportunities';
        final string CLASSNAME = 'OpportunitySelector';
        List<Opportunity> lstOpportunity = new List<Opportunity>();
        try {
            String strQuery = 'Select Id, Name, TRAV_Renewal_Opportunity__c FROM Opportunity Where TRAV_Renewal_Opportunity__c IN :'+String.valueOF(setOpptyId);
            lstOpportunity = Database.query(strQuery);
			if(Test.isRunningTest()){
                throw new QueryException();
            }
        }
        catch(QueryException objExp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);

        } // End Catch
        return lstOpportunity;
    } //end of method getLeads

      /**
* This is a method is used to get Map of Opportunity for potential premium from set of ID
* @param Set Opportunities id set
* @return Map<Id,Opportunity> Map of Opportunities based on the SOQL query provided as an input parameter.
*/

    public static Map<Id,Opportunity> getOpportunitiesForPotentialPremium(Set<Id> setOpportunityIds){

        final string METHODNAME = 'getOpportunitiesForPotentialPremium';
        final string CLASSNAME = 'OpportunitySelector';
        Map<Id,Opportunity> mapOpportunity = new Map<Id,Opportunity>();

        try{
            mapOpportunity = new Map<Id,Opportunity>([
                                SELECT
                                    Id,
                                    TRAV_Potential_Premium__c,
									TRAV_Business_Unit__c,
                					(Select Id
                                     from OpportunityLineItems
                                     where OpportunityId IN : setOpportunityIds
                                     And TRAV_Coverage_Premium__c != null)
                                FROM
                                    Opportunity
                                WHERE
                                    Id IN : setOpportunityIds
                        ]);
						if(Test.isRunningTest()){
                throw new QueryException();
            }
        } //End - try
        catch(QueryException objExp){
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        } //End - catch

      return mapOpportunity;

    } //End of the Method


/**
* This is a method is used to get list opportunities from ID
* @param string Opportunities id set
* @return List<Opportunity> List of Opportunities based on the SOQL query provided as an input parameter.
*/
    public static List<Opportunity> getOpportuntiyProductNames(Set<Id> setOpptyId) {
        final string METHODNAME = 'getOpportuntiyProductNames';
        final string CLASSNAME = 'OpportunitySelector';
        String strQuery = 'Select Id, TRAV_Product_Names__c, TRAV_Direct_Source_Sys_Code__c From Opportunity Where Id IN :setOpptyId';
        List<Opportunity> lstOpportunity = new List<Opportunity>();
        try {
            lstOpportunity = Database.query(strQuery);
			if(Test.isRunningTest()){
                throw new QueryException();
            }
        }
        catch(QueryException objExp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);

        } // End Catch
        return lstOpportunity;
    } //end of method getOpportuntiyProductNames
    /**
* This is a method is used to get list opportunities from ID
* @param string Opportunities id set
* @return List<Opportunity> List of Opportunities based on the SOQL query provided as an input parameter.
*/
    public static List<Opportunity> getOpportunitiesWithRelatedLineItemProducts(Set<Id> setOpptyId, ExcludeOpportunityBy filterBy) {
        final string METHODNAME = 'getOpportunitiesWithRelatedLineItemProducts';
        final string CLASSNAME = 'OpportunitySelector';

        string exclusionCriteria = '';
        switch on filterBy{
            when CB_MDM{
                exclusionCriteria = ' AND TRAV_Direct_Source_Sys_Code__c != \'CB MDM\'';
            }
            when NONE{
                exclusionCriteria = '';
            }
        }

        String strQuery = 'SELECT Id, TRAV_Product_Names__c, TRAV_Product_Names_Abr__c, (SELECT Product2.Name, Product2.TRAV_Product_Abbreviation__c FROM OpportunityLineItems) FROM Opportunity WHERE Id IN: setOpptyId' + exclusionCriteria;
        List<Opportunity> lstOpportunity = new List<Opportunity>();
        try {
            lstOpportunity = Database.query(strQuery);
			if(Test.isRunningTest()){
                throw new QueryException();
            }
        }
        catch(QueryException objExp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);

        } // End Catch
        return lstOpportunity;
    } //end of method getOpportunitiesWithRelatedLineItemProducts

	 /**
* This is a method is used to get Map opportunities from ID
* @param string Opportunities id set
* @return Map<Id,Opportunity> MAp of Opportunities based on the SOQL query provided as an input parameter.
*/

    public static  Map<Id,Opportunity> getOpportunitiesForTPAError(Set<Id> setOpportunityIds){

        final string METHODNAME = 'getOpportunitiesForTPAError';
        final string CLASSNAME = 'OpportunitySelector';
        Map<Id,Opportunity> mapOpportunity = new Map<Id,Opportunity>();

        try{
           mapOpportunity =new Map<Id,Opportunity>( [
                SELECT
                Id,
                (Select Id, TRAV_Assigned_TPA__c, TRAV_Assigned_TPA__r.TRAV_Approved__c from OpportunityLineItems where TRAV_Program_Type__c IN :Constants.setLineItemProgramTypeForAssignedTPA AND TRAV_Assigned_TPA__c = Null AND TRAV_Assigned_TPA__r.TRAV_Approved__c = false)
                FROM
                Opportunity
                WHERE
                Id IN : setOpportunityIds
            ]);
			if(Test.isRunningTest()){
                throw new QueryException();
            }
        } //End - try
        catch(QueryException objExp){
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        } //End - catch

        return mapOpportunity;

    } //End of the Method

    /**
* This is a method is used to get list opportunities
* @param string object name
* @return string query
*/
  /*  public static String getDeferredStageOpportunities(String objName) {
        String strDeferred = Constants.CLOSEDDEFERRED;
        String strAllFields = UtilityClass.getAllFieldsSOQL('Opportunity');
        String strQueryOpp = 'SELECT ' + strAllFields + ' FROM Opportunity' +
          ' WHERE TRAV_Defferal_Date__c >=: dtFrom'+
          ' AND TRAV_Defferal_Date__c <=: dtTo AND StageName = \''+strDeferred+'\'';
        system.debug('strQueryOpp-->>'+strQueryOpp);
        return strQueryOpp;
    } */
    /**
* This is a method is used to get Map opportunities from ID
* @param string Opportunities id set
* @return Map<Id,Opportunity> MAp of Opportunities based on the SOQL query provided as an input parameter.
*/

    public static  Map<Id,Opportunity> getOpportunitiesForWinningTPAError(Set<Id> setOpportunityIds){

        final string METHODNAME = 'getOpportunitiesForWinningTPAError';
        final string CLASSNAME = 'OpportunitySelector';
        Map<Id,Opportunity> mapOpportunity = new Map<Id,Opportunity>();

        try{
           mapOpportunity =new Map<Id,Opportunity>( [
                SELECT
                Id,
                (Select Id, TRAV_Winning_TPA__c from OpportunityLineItems where TRAV_Program_Type__c IN :Constants.setLineItemProgramTypeForAssignedTPA AND (TRAV_Winning_TPA__c = Null))
                FROM
                Opportunity
                WHERE
                Id IN : setOpportunityIds
            ]);
			if(Test.isRunningTest()){
                throw new QueryException();
            }
        } //End - try
        catch(QueryException objExp){
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        } //End - catch

        return mapOpportunity;

    } //End of the Method

    /**
    * This is a method is used to get Map opportunities from ID
    * @param string Opportunities id set
    * @return Map<Id,Opportunity> MAp of Opportunities based on the SOQL query provided as an input parameter.
    */
    public static  Map<Id,Opportunity> getOpportunitiesOwner(Set<Id> setOpportunityIds){
        final string METHODNAME = 'getOpportunitiesOwner';
        final string CLASSNAME = 'OpportunitySelector';
        Map<Id,Opportunity> mapOpportunity = new Map<Id,Opportunity>();

        try{
           mapOpportunity =new Map<Id,Opportunity>( [
                SELECT
                Id,
                OwnerId,
                Owner.TimeZoneSidKey
                FROM
                Opportunity
                WHERE
                Id IN : setOpportunityIds
            ]);
            if(Test.isRunningTest()){
                throw new QueryException();
            }
        } //End - try
        catch(QueryException objExp){
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        } //End - catch

        return mapOpportunity;
    } //End of the Method

}