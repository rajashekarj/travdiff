////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Class: BIWSController
//
// Modification Log:
// -----------------------------------------------------------------------------------------------------------
// Developer              User Story/Defect    Date             Description
// ----------------------------------------------------------------------------------------------------------- 
// Evan Wilcher           US77069              02/24/2020       Added conditional to check if opportunity is from CB
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public with sharing class BIWSController {
    
    @AuraEnabled
     public static String[] getInfo(Id id) {
        
         Opportunity opportunity = [
                SELECT TRAV_SAI__c, Account.Id, TRAV_Direct_Source_Sys_Code__c, TRAV_CB_BU_Description__c
             FROM Opportunity
            WHERE Id = :id
         ];

         Map<String, Object> fieldToValue = opportunity.getPopulatedFieldsAsMap();
		 String strIbu = '';
         
         // US77069 update by Evan Wilcher
         if (opportunity.TRAV_Direct_Source_Sys_Code__c != 'CB MDM'){
             if(fieldToValue.containsKey('AccountId')){
                strIbu = [
                Select TRAV_Issuing_Business_Unit_Code__c
                FROM Account
                Where Id = :opportunity.AccountId
                LIMIT 1
                 ].TRAV_Issuing_Business_Unit_Code__c;
             }
         }else{
             if(opportunity.TRAV_CB_BU_Description__c != NULL)
                 strIbu = opportunity.TRAV_CB_BU_Description__c;
         }
         // US77069 update End
         
        CustomUrl__mdt customUrl = [
            Select Environment_Url__c, Environment__c
            From CustomUrl__mdt
            Where MasterLabel = 'BIWS Url'
        ];
        String[] result = new List<String>();
         
         if(opportunity.TRAV_SAI__c == null || opportunity.TRAV_SAI__c == ''){
             result.add('no value');
         } else {
         	result.add(opportunity.TRAV_SAI__c);
         }
         system.debug(strIbu);
         if(strIbu == '' || strIbu == null){
             result.add('no value');
         } else {
         	result.add(strIbu);
         }
         result.add(customUrl.Environment_Url__c);
         result.add(customUrl.Environment__c);
        return result;
    }
    

}