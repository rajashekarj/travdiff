/***
 * This class is used as a Selector Layer for Account object. 
 * All queries for Account object will be performed in this class.
 *
 * @Author Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shashank Agarwal       US66509              09/18/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */ 
public with sharing class BAMSelector { 
     /**
     * This is a method is used to get list BAM Records 
     * @param Set<String> -SetUniqueIdentifiers
     * @returnList<TRAV_BAM_Agency_Mapping__c> List of BAM Records based on the SOQL query provided as an input parameter.
     */
    
    public static List<TRAV_BAM_Agency_Mapping__c> getBAMtoTagForProducers(Set<String> setBAMUniqueIdentifier){
    
        List<TRAV_BAM_Agency_Mapping__c> lstBAMRecords = new List<TRAV_BAM_Agency_Mapping__c>();
        final string CLASSNAME = 'BAMSelector';
        final string METHODNAME = 'getBAMtoTagForProducers';
        
        try{
            lstBAMRecords = [
                                SELECT 
                                    TRAV_Unique_Identifier__c ,
                                    Id
                                FROM 
                                    TRAV_BAM_Agency_Mapping__c
                                WHERE 
                                    TRAV_Unique_Identifier__c IN : setBAMUniqueIdentifier	
                        ];
			if(Test.isRunningTest()){
                throw new QueryException();
            }
        } //End - try
        catch(QueryException objExp){
            //exception handling code
             ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        } //End - catch

      return lstBAMRecords;
     
    } //End of the Method
	
	

} //End of the class