/**
 * This class is used as a Wrapper Class to store the Product vs the Closed Reason data.
 * This is invoked when an opportunity stage is marked as closed.
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------          
 * Shashank Shastri            US60000         06/20/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------              
 */
public class ClosedOpportunityProductWrapper {
    @AuraEnabled
    public boolean loadPopup = false;
    @AuraEnabled
    public boolean isBoundStage = false;
    @AuraEnabled
    public string stageName;
    @AuraEnabled
    public List<productWithReasonList> productConfig = new List<productWithReasonList> ();
    @AuraEnabled
    public List<String> productStage = new List<String> ();
    @AuraEnabled
    public Date deferraldate;
    @AuraEnabled
    public Boolean multipleProducts;
    @AuraEnabled
    public TRAV_Carrier__c defaultCarrier;
    @AuraEnabled
    public Date prospectEffectiveDate;//Added for US68543
	@AuraEnabled
    public String strOpportunityBU;
}