/**
*Utility Class for Account Trigger Handler. This class will have logic for all the implementation
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -------------------- ---------------------------------------------------------------------------------------              
* Bhanu Reddy              US62952            04/30/2019       Orginal Version
* Bhanu Reddy              US62952            05/07/2019       Added queable method
* Mohit Mendiratta                            09/03/2019       Quick fix to avoid overwriting the Producer Codes in Agency B&M as part of 
incremental data loads through IICS process
* -----------------------------------------------------------------------------------------------------------
*/ 
public  class AccountService implements Queueable { 
    list<Account> lstNewAccounts = new list<Account> (); 
    map<id,Account> mapOldAccounts = new map<id,Account>(); 
    map<id, Account> mapIdtoAccount = new map<id, Account>();
    
    /**
* Parameterised Constructor
* @param lstnewOpportunity - List of new values in the for the Account records.
* @param mapOldOpportunity - Map of old values in the for the Account records
* @return void - returns void .
*/    
    public AccountService(List<Account> listNewAccounts,map<id,Account> mapOfOldAccount){
        lstNewAccounts = listNewAccounts;
        mapOldAccounts = mapOfOldAccount;
    } //end of prametraized constructor
    
    /**
* This method is used to populate/delete Agency Producer code object when ever the 
* new agency is inserted or CL Prime Producer code
* @param lstnewOpportunity - List of new values in the for the Account records.
* @param mapOldOpportunity - Map of old values in the for the Account records
* @return void - returns void .
*/ 
    public static void processAgencyProducerCode(list<Account> lstNewAccounts,map<id,Account> mapOldAccounts){        
        // Agency RecordType Ids
        Set<Id> setBMRecordtypeids = new Set<Id>();
        Set<Id> setNonBMRecordtypeids = new Set<Id>();
        
        // Map of account-producer relation records to insert and delete
        Map<String, AcctProdRelWrapper> mapAcctProdRelWrapperToInsert = new Map<String, AcctProdRelWrapper>();
        
        // Set of all producer codes
        Set<String> setProducerCodes = new Set<String>();
        
        //Fetch agency record type ids 
        setBMRecordtypeids.add(
            Schema.SObjectType.Account.getRecordTypeInfosByName().get(CONSTANTS.BRICKANDMOTAR).getRecordTypeId());
        setNonBMRecordtypeids.add(
            Schema.SObjectType.Account.getRecordTypeInfosByName().get(CONSTANTS.AGENCYHEADQUATER).getRecordTypeId());        
        setNonBMRecordtypeids.add(
            Schema.SObjectType.Account.getRecordTypeInfosByName().get(CONSTANTS.AGENCYORGANIZATION).getRecordTypeId());         
        
        try {            
            // Query all account-producer-relation records from DB based on account ids
            Set<String> setAccountIds = new Set<String>();
            for(Account agency : lstNewAccounts) {
                setAccountIds.add(agency.id);
            }
            
            Map<String, TRAV_Account_Producer_Relation__c> mapAcctProdRel = 
                AccountProducerRelationshipSelector.getAccountProducerRelationsbyAccountId(setAccountIds);
            
            //Iterate over the new Account records
            for(Account newAccount : lstNewAccounts) {
                Set<String> setCurrRecProducerCodes = new Set<String>();
                // Handle non-brick and mortar records
                if(setNonBMRecordtypeids.contains(newAccount.recordtypeid) &&
                   newAccount.TRAV_CL_Master_Producer_Code__c != null) {
                       setCurrRecProducerCodes.addAll(newAccount.TRAV_CL_Master_Producer_Code__c.split(CONSTANTS.COMMA));
                   } 
                
                // Handle brick and mortar records
                if(setBMRecordtypeids.contains(newAccount.recordtypeid) &&
                   newAccount.TRAV_CL_Prime_Producer_Code__c != null) {
                       setCurrRecProducerCodes.addAll(newAccount.TRAV_CL_Prime_Producer_Code__c.split(CONSTANTS.COMMA));
                   } 
                
                // Add 0NA063 producer code to the list of producer codes
                // setCurrRecProducerCodes.add(CONSTANTS.ProducerCodeUnknown);
                
                // Create set of all producer codes
                setProducerCodes.addAll(setCurrRecProducerCodes);
                
                // Iterate over the current agency record's producer codes
                for(String producerCode : setCurrRecProducerCodes) {
                    String externalId = newAccount.TRAV_Account_External_Id__c + '_' + producerCode;
                    if(mapAcctProdRel.containsKey(externalId)) {
                        // This record already exists in account-producer relation. No action is required, 
                        // hence remove this record from the map. Whatever will finally be left in this map,
                        // will be considered as extra values and will be deleted.
                        mapAcctProdRel.remove(externalId);
                    }
                    else {
                        // The account-producer relation record does not exist. Hence, it needs to be added.
                        AcctProdRelWrapper acctProdRel = new AcctProdRelWrapper(newAccount, producerCode, externalId);
                        mapAcctProdRelWrapperToInsert.put(externalId, acctProdRel);
                    }
                }
            } //end of for loop-iteration over account records 
            
            if(!mapAcctProdRel.isEmpty() && !General_Data_Setting__c.getOrgDefaults().Ignore_Producer_Code_Deletion__c) {
                delete mapAcctProdRel.values();
            }
            if(!mapAcctProdRelWrapperToInsert.isEmpty()) {
                insertAcctProdRelRecords(mapAcctProdRelWrapperToInsert, setProducerCodes);
            }              
            if(Test.isRunningTest()){
                throw new QueryException();
            }
        } //end of try
        catch(Exception objException){
            ExceptionUtility.logApexException('AccountService','processAgencyProducerCode',objException,null);            
        } //end of catch block
    } //end of method processAgencyProducerCode         
    
    /**
* This method is used to insert agency-producer-relation records
* @param mapAcctProdRelWrapperToInsert - Map of account-producer-relation external-id and
* AcctProducerRelWrapper objects.
* @param mapAcctProdRelWrapperToInsert - Set of all producer codes. The method queries all 
* producer records based on the producer codes.
* @return void - returns void .
*/ 
    public static void insertAcctProdRelRecords(Map<String, AcctProdRelWrapper> mapAcctProdRelWrapperToInsert,
                                                Set<String> setProducerCodes) {
                                                    List<TRAV_Account_Producer_Relation__c> lstAccntProdRelToInsert = new list<TRAV_Account_Producer_Relation__c>();
                                                    
                                                    // Query all producers
                                                    Map<String, TRAV_Producer__c> mapProducers = ProducerSelector.getProducerMapByExternalId(setProducerCodes);
                                                    
                                                    for(AcctProdRelWrapper acctProdRelWrap : mapAcctProdRelWrapperToInsert.values()) {
                                                        TRAV_Producer__c producer = mapProducers.get(acctProdRelWrap.producerCode);
                                                        if(null == producer) {
                                                            // Producer record not found in DB. Skip and continue
                                                            continue;
                                                        }
                                                        
                                                        // Create new account-producer-relation record
                                                        TRAV_Account_Producer_Relation__c acctProdRelRec = new TRAV_Account_Producer_Relation__c();
                                                        acctProdRelRec.TRAV_Producer__c = producer.id;
                                                        acctProdRelRec.TRAV_Account__c = acctProdRelWrap.agency.id;
                                                        acctProdRelRec.TRAV_External_ID__c = acctProdRelWrap.acctProducerExternalId;
                                                        acctProdRelRec.name = producer.TRAV_Producer_Code__c + ' - ' + producer.Name;
                                                        
                                                        lstAccntProdRelToInsert.add(acctProdRelRec);
                                                    }
                                                    
                                                    // Insert records
                                                    if(!lstAccntProdRelToInsert.isEmpty()) {
                                                        insert lstAccntProdRelToInsert;
                                                    }
                                                }
    
    /**
* This method is used to make the excetuion Queueable
* @param QueueableContext contact
* @return void - returns void .
*/ 
    public void execute(QueueableContext context) { 
        processAgencyProducerCode(lstNewAccounts,mapOldAccounts);
        if(mapOldAccounts !=null){
            if(CheckAddressUpdates(lstNewAccounts,mapOldAccounts,mapIdtoAccount)){
                 processContactsToUpdate(mapIdtoAccount);
             }
        }        
    } //end method execute
    
    /**
* Wrapper class to contain the relation between an agency record and individual producer-code
*/
    public class AcctProdRelWrapper {
        public Account agency;
        public String producerCode;
        public String acctProducerExternalId;        
        
        /**
* Parametrized constructor
* @param agency - Agency record from Account object
* @param producerCode - producerCode corresponding to the agency record
* @param acctProducerExternalId - External id to be used for the account-producer-relation record
*/
        public AcctProdRelWrapper(Account agency, String producerCode, String acctProducerExternalId) {
            this.agency = agency;
            this.producerCode = producerCode;
            this.acctProducerExternalId = acctProducerExternalId;
        }
    }
    /**
* This method is used to update the contacts when there is change in Adress of Agency
* @param List of new records - 
* @param map Old records
* @param Map account to process
*/
    public static boolean CheckAddressUpdates(list<Account> listNewAccounts, Map<id,Account> mapOldAccounts,map<id,Account> mapIdtoAccount ){
        //boolean to check of there is change in addresss
        boolean boolIsAccountAdressModified = false;
        //set to capture the accounts whose address is modified
        system.debug('new Account'+listNewAccounts);
         system.debug('nOld Account'+mapOldAccounts);
        //check account address modifications
        for(Account objAccount:listNewAccounts){
            if(objAccount.Billingstreet != mapOldAccounts.get(objAccount.id).Billingstreet   ||
               objAccount.BillingState != mapOldAccounts.get(objAccount.id).BillingState     ||
               objAccount.BillingCountry != mapOldAccounts.get(objAccount.id).BillingCountry ||
               objAccount.BillingPostalcode != mapOldAccounts.get(objAccount.id).BillingPostalcode 
              ){
                  boolIsAccountAdressModified = true;
                  mapIdtoAccount.put(objAccount.id,objAccount);
                  system.debug('Inside the Excepected loop'+mapIdtoAccount); 
              }
        }
        return boolIsAccountAdressModified;
    } //end of method CheckAddressUpdates
    
    public static void processContactsToUpdate(map<id,Account> mapIdtoAccountObject){
        try{
        List<contact> lstContactstoUpdate = new list<contact>();
        contact objContactToUpdate ;
        for(Contact objContact : ContactSelector.fetchAgencyContacts(mapIdtoAccountObject.keySet())){
            if(mapIdtoAccountObject.containsKey(objContact.AccountId)){
                objContactToUpdate = new Contact();
                objContactToUpdate.id = objContact.id;
                   objContactToUpdate.OtherStreet = ContactService.checknull(mapIdtoAccountObject.get(objCOntact.AccountId).BillingStreet);
                    objContactToUpdate.OtherCity = ContactService.checknull(mapIdtoAccountObject.get(objCOntact.AccountId).BillingCity);
                    objContactToUpdate.OtherState = ContactService.checknull(mapIdtoAccountObject.get(objCOntact.AccountId).BillingState);
                    objContactToUpdate.OtherPostalCode = ContactService.checknull(mapIdtoAccountObject.get(objCOntact.AccountId).BillingPostalCode);
                    objContactToUpdate.OtherCountry = ContactService.checknull(mapIdtoAccountObject.get(objCOntact.AccountId).BillingCountry);
                    lstContactstoUpdate.add(objContactToUpdate);      
            } //end if account check
        } //end for Iteration of for itreation of contacts
        if(!lstContactstoUpdate.isEmpty()){
            Database.SaveResult[] lstSaveResult = Database.update(lstContactstoUpdate,false);
        ExceptionUtility.logDMLError('AccountService', 'processContactsToUpdate', lstSaveResult); 
        } //end if contact update
        if(Test.isRunningTest()){
                throw new QueryException();
            }
        } //end try
        catch(Exception ObjException){
            ExceptionUtility.logApexException('AccountService', 'processContactsToUpdate', ObjException, null);
        } //end of catch
    } //end of method processContactUpdate
} //end of class