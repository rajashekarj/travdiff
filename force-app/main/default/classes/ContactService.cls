/**
* Utility Class for Contact Trigger Handler. This class will have logic for all the implementations.
* 
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect   Date        Description
* -----------------------------------------------------------------------------------------------------------              
* Shashank Agarwal       US65899             08/19/2019   Added method updateContactAccount
* Sayanka Mohanty        US69347/US69863	 10/17/209	  Added method updateContactINMDM
*/
public class ContactService {
    /**
* This is a method will add User and Account to all the records with record type Traveler's Contact
* @param lstNewContact - List of new values in the for the Contact records.
* @return void - returns void .
*/ 
    
    public static void updateContactAccount(List<Contact> lstNewContact, Boolean objTriggerIsExecuting) {
        final string METHODNAME = 'updateContactAccount';
        final string CLASSNAME = 'ContactService';
        try{
            Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.TRAVELERSCONTACTRECORDTYPE).getRecordTypeId();
            
            Set<String> setContactEmail = new Set<String>();
            for(Contact objContact : lstNewContact){
                if(objContact.Email != Null && objContact.RecordTypeId == recordTypeId ){
                    setContactEmail.add(objContact.Email);
                }
            }
            
            
            System.enqueueJob( new QueueableUpdateTravelersContact(lstNewContact,setContactEmail,objTriggerIsExecuting) );
           

        }catch(Exception objExp){
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        } //End of catch block
    } //End of Method
    /**
* This is a method will update contact record type based on licensed indicator
* @param lstNewContact - List of new values in the for the Contact records.
* @return void - returns void .
*/ 
    public static void updateContactRecordtype(List<Contact> lstNewContact,Map<Id, Contact> mapOldContact) {
        final string METHODNAME = 'updateContactRecordtype';
        final string CLASSNAME = 'ContactService';
        try{
            Id recordTypeIdforLicensed = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.LICENSEDCONTACTRECORDTYPE).getRecordTypeId();
            Id recordTypeIdforNonLicensed = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.NONLICENSEDCONTACTRECTYPE).getRecordTypeId();
            
            for(Contact objContact : lstNewContact){
                if((mapOldContact == NULL && objContact.TRAV_Licensed_Individual_Indicator__c != null) || 
                   (mapOldContact != NULL &&
                    mapOldContact.get(objContact.Id).TRAV_Licensed_Individual_Indicator__c != objContact.TRAV_Licensed_Individual_Indicator__c 
                    && objContact.TRAV_Licensed_Individual_Indicator__c != null)) {
                    if(objContact.TRAV_Licensed_Individual_Indicator__c == true && objContact.RecordTypeId == recordTypeIdforLicensed){
                        objContact.RecordTypeId = recordTypeIdforLicensed;
                    }
                }
            }
        }catch(Exception objExp){
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        } //End of catch block
    } //End of Method
    /**

* This is a method will add User and Account to all the records with record type Traveler's Contact
* @param lstNewContact - List of new values in the for the Contact records.
* @return void - returns void .
*/
    public static void updateContactINMDM(List<Contact> lstNewContact, Map<Id, Contact> mapOldContact) {
        final String METHODNAME = 'updateContactINMDM';
        final string CLASSNAME = 'ContactService';
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('TRAV_Agency_Non_Licensed_Contact').getRecordTypeId();
    MAP<ID, String> mapAccountExternalID = new Map<ID, String>();
        Set<ID> setAccountIDs = new Set<ID>();
        // Create a set from List
        for(Contact objContact : lstNewContact){
            setAccountIDs.add(objContact.AccountID);
        }
        for(Account objAccount : [SELECT ID, TRAV_Account_External_ID__c FROM Account WHERE ID IN: setAccountIDs]){
            mapAccountExternalID.put(objAccount.ID, objAccount.TRAV_Account_External_Id__c);
        }
        if(!lstNewContact.isEmpty()){
            try{
                for(Contact objContact : lstNewContact){
                    if(objContact.RecordTypeId == recordTypeId){
                    String strRequestBody = '';
                    MDMUpdateIndividualWrapper requestWrapper = new MDMUpdateIndividualWrapper(objContact.Id, objContact, mapAccountExternalID);
                    //requestWrapper.agncyIndvId = objContact.TRAV_External_Id__c;
                    strRequestBody = JSON.serialize(requestWrapper);
                    system.debug('**Request Body**'+strRequestBody);
                    ContactSearchPullUtility.updateContactInMDM(objContact.TRAV_External_Id__c, strRequestBody);
                    }
                }
            }catch(Exception objExp){
                ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
            }
        }  
    }
    /**
* This is a method Copy agency brick and mortar address to Agency Lincesed and Non Licensed contact

* @param lstNewContact - List of new values in the for the Contact records.
* @return void - returns void .
*/ 
    public static void copyBillingAdress(list<contact> lstNewContacts,map<id,contact> mapOldContacts){
        try
        {
        //fetch the agency Licensed and Non Licensed recordtypes 
        Id recordTypeIdforLicensed = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.LICENSEDCONTACTRECORDTYPE).getRecordTypeId();
        Id recordTypeIdforNonLicensed = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.NONLICENSEDCONTACTRECTYPE).getRecordTypeId();
        map<id,Account> mapAccountIdToObject = new map<id,Account>();
        set<id> setAccountids = new set<id>();
        //Iterate over the Loop of contacts to pick Agency Licensed and Non Licensed Contacts
        for(Contact objContact :lstNewContacts){
            Contact objOldContact ;
            if(mapOldContacts !=null)
              objOldContact = mapOldContacts.get(objCOntact.id);
            
            if((objContact.RecordTypeId ==recordTypeIdforLicensed ||objContact.RecordTypeId == recordTypeIdforNonLicensed) &&
               ((mapOldContacts ==null) ||
                ((string.isnotBlank(objOldContact.AccountId)) && objContact.AccountId != objOldContact.AccountId)               ||     
                ((string.isnotBlank(objOldContact.OtherStreet) && objContact.OtherStreet != objOldContact.OtherStreet)          ||
                 (string.isnotBlank(objOldContact.OtherCity) && objContact.OtherCity != objOldContact.OtherCity )               ||
                 (string.isnotBlank(objOldContact.OtherState) && objContact.OtherState != objOldContact.OtherState)   		     ||
                 (string.isnotBlank(objOldContact.OtherPostalCode) && objContact.OtherPostalCode != objOldContact.OtherPostalCode)  ||
                 (string.isnotBlank(objOldContact.OtherCountry) && objCOntact.OtherCountry != objOldContact.OtherCountry))
               )){
                   setAccountids.add(objContact.AccountId);
               } //end if agency RecordType check
        } //end of for iteration over new contacts
        if(!setAccountids.isEmpty()){
            for(Account  objAccount : AccountSelector.getAccountBillidAddress(setAccountids)){
                mapAccountIdToObject.put(objAccount.id,objAccount);
            } //end for iteration of accounts
        } //end if -account's empty check
        if(!mapAccountIdToObject.isEmpty()){
            //iterate over the contacts and assing billing address
            for(contact objCOntact :lstNewContacts ){
                if(mapAccountIdToObject.containsKey(objCOntact.AccountId)){
                    objCOntact.OtherStreet = checknull(mapAccountIdToObject.get(objCOntact.AccountId).BillingStreet);
                    objCOntact.OtherCity = checknull(mapAccountIdToObject.get(objCOntact.AccountId).BillingCity);
                    objCOntact.OtherState = checknull(mapAccountIdToObject.get(objCOntact.AccountId).BillingState);
                    objCOntact.OtherPostalCode = checknull(mapAccountIdToObject.get(objCOntact.AccountId).BillingPostalCode);
                    objCOntact.OtherCountry = checknull(mapAccountIdToObject.get(objCOntact.AccountId).BillingCountry);
                   } // end of if map comtains checkbox
                
            }// end for iteration of contacts
        }
        } // try
        catch (Exception objException){
            ExceptionUtility.logApexException('ContactService', 'copyBillingAdress', objException, null);
        } //end try
    } //end of method copyBillingAdress
    
    public static string checknull(string strValuetoCheck){
        if(string.isnotBlank(strValuetoCheck)){
            return strValuetoCheck;
        }
        else{
            return  '';
        }
    } //end method checknull 
    
    /**
* This is a method that will check if the contacts phone field is blank and if it is, the mobile field will be copied over to the phone field.
* @return void - returns void .
*/
    public static void copyMobileToPhone(list<contact> lstNewContacts,map<id,contact> mapOldContacts){
        try{
            for(Contact objContact :lstNewContacts){
                //Check if phone number is empty and mobile has a value
                if(objContact.Phone == NULL && objContact.MobilePhone != NULL){
                    //Copy Mobile number to phone number
                    objContact.Phone = objContact.MobilePhone;
                }
            }
        } //Try
        catch (Exception objException){
            ExceptionUtility.logApexException('ContactService', 'copyMobileToPhone', objException, null);
        }
    } //end copyMobileToPhone
    
} // End of Class