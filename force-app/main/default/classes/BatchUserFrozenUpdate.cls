global class BatchUserFrozenUpdate implements Database.Batchable<SObject>, Schedulable{

  global BatchUserFrozenUpdate() {
    
  }
        
    global Database.QueryLocator start(Database.BatchableContext BC){
      Renewal_Batch__c objBamBatch = Renewal_Batch__c.getOrgDefaults();
        Datetime last_run= objBamBatch.TRAV_Last_Successful_Frozen_Batch_Run__c;
        if (last_run == null)
        {
            last_run = last_run = DateTime.newInstance(2018, 2, 29, 18, 2, 3);
        }
        
        String Query = 'SELECT UserId, IsFrozen,lastmodifieddate,lastmodifiedbyId FROM UserLogin where lastmodifieddate > '+ last_run.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\''); 
        return Database.getQueryLocator(Query);
     }
      /**
        * This method execute this batch class when scheduled
        * @param SchedulableContext record 
        * @return void 
        */
    global void execute(SchedulableContext schCon) {
        
      BatchUserFrozenUpdate batchObj  = new BatchUserFrozenUpdate (); 
      Database.executebatch(batchObj);
      }

     global void execute(Database.BatchableContext BC, List<UserLogin> scope)
     {
       
       List<id> userid = new List<id>();
       Map<Id, UserLogin> userLoginToUsers = new Map<Id, UserLogin>();

       for(UserLogin s : scope)
       {
       		userid.add(s.UserId);
           userLoginToUsers.put(s.UserId,s);
       }
       List<User> frozenusers = [select id,TRAV_Isfrozen__c from user where id in :userLoginToUsers.keySet()];
		List <User> frozenupdateList = new List<User>();         
       for (User freezeuser:frozenusers)
       {
         UserLogin usertofreeze = userLoginToUsers.get(freezeuser.id);
         freezeuser.TRAV_Isfrozen__c = userToFreeze.IsFrozen;
           frozenupdateList.add(freezeuser);
       }
         update frozenupdateList;
      }
  
     global void finish(Database.BatchableContext BC){
      DateTime dtTo = DateTime.now();
      Renewal_Batch__c objBamBatch = Renewal_Batch__c.getOrgDefaults();
      objBamBatch.TRAV_Last_Successful_Frozen_Batch_Run__c = dtTo;
      upsert objBamBatch;
     }

}