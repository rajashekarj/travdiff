/**
* Test class for EntitySubscriptionSelector

*
* @Author : Deloitte Consulting
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty       					 08/02/2019       Original Version
* -----------------------------------------------------------------------------------------------------------
*/
@isTest
public class TestEntitySubscriptionSelector {
    @testSetup static void createTestData(){
        //Create 100 leads
        List<Lead> listOfLeadsOwned = new List<Lead>();
        Lead leadRec;
        for(integer i = 0; i < 100 ; i++){
            leadRec = new Lead();
            leadRec = TestDataFactory.createLead();
            leadRec.LastName += i;
            leadRec.TRAV_Certified_Business_Id__c += i;
			leadRec.Street = 'Test Street';
            leadRec.City = 'Test City';
            leadRec.PostalCode = 'Test Postal Code';
            leadRec.Country = 'Test Country';
            listOfLeadsOwned.add(leadRec);
        }
        //Inserting a list of 100 leads
        if(!listOfLeadsOwned.isEmpty()){
            database.insert(listOfLeadsOwned);
        }
        system.assert(listOfLeadsOwned.size() == 100);
        //Inserting EntitySubscription Records
        List<EntitySubscription> listToFollow = new List<EntitySubscription>();
        for(Lead leadToFollow : listOfLeadsOwned){
            //CreateEntitySubscription Record
            EntitySubscription followRec = new EntitySubscription(ParentId = leadToFollow.Id, SubscriberId = userinfo.getUserId());
            listToFollow.add(followRec);
        }
        if(!listToFollow.isEmpty()){
            database.insert(listToFollow);
        }
        system.assert(!listToFollow.isEmpty());
    }
   public static testMethod void testEntitySubscriptionUserId(){
        Test.startTest();
        Lead l = [SELECT Id FROM Lead LIMIT 1];
        EntitySubscriptionSelector.getSubscriptionList(userInfo.getUserId());
        Map<Id,String> testMap = new Map<Id,String>();
        testMap.put(l.Id, '');
        //EntitySubscriptionSelector.getUserListForObj(testMap);
        Test.stopTest();
        
    }
}