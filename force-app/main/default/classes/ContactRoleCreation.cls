/**
* Apex Controller Class for OpportunityContactRoleScreen Lightning Component. This class will have logic to 
*insert opportunity contact role records and retrieve role picklist values on load.
* 
*Modification Log    :
* ------------------------------------------------------------------------------------------------------
* Developer              	User Story/Defect    Date             Description
* ------------------------------------------------------------------------------------------------------
* Tejal Pradhan				US68408              09/27/2019       Original Version
*/
public without sharing class ContactRoleCreation {
    
    /**
* This method inserts Opportunity Contact Role records 
* @param strOpportunityId - Id of the opportunity record
* @param strContactId - Id of the Contact record
* @param strRoleName - Name of the Role 
* @param boolIsPrimary - To indicate if it is primary
* @return void - returns void .
*/
    @AuraEnabled
    public static String createContactRole(String strOpportunityId, String strContactId, String strRoleName, Boolean boolIsPrimary){
        
        String userId = UserInfo.getUserId();
        String message = '';
        User u = [SELECT id, TRAV_BU__c from User where ID =: userId].get(0);
        Opportunity opp = [SELECT id, TRAV_Business_Unit__c from Opportunity where Id =: strOpportunityId].get(0);
        if(u.TRAV_BU__c != opp.TRAV_Business_Unit__c) {
            message = 'Updates to this Opportunity must be made by a user from the respective Business Unit';
        }
        else{
            message = '';
            OpportunityContactRole objOppContRole = new OpportunityContactRole();
            objOppContRole.OpportunityId = Id.valueOf(strOpportunityId);
            objOppContRole.ContactId = Id.valueOf(strContactId);
            objOppContRole.Role = strRoleName;
            objOppContRole.IsPrimary = boolIsPrimary;
            Database.SaveResult resultOpp = Database.insert(objOppContRole);
        }
        return message;
    }
    
    @AuraEnabled
    public static String buValidation(String strOpportunityId) {
        String userId = UserInfo.getUserId();
        String message = '';
        User u = [SELECT id, TRAV_BU__c from User where ID =: userId].get(0);
        Opportunity opp = [SELECT id, TRAV_Business_Unit__c from Opportunity where Id =: strOpportunityId].get(0);
        if(u.TRAV_BU__c != opp.TRAV_Business_Unit__c) {
            message = 'Updates to this Opportunity must be made by a user from the respective Business Unit';
        }
        else
            message = '';
        
        return message;
    }
    
    /**
* This method retrieves Role picklist values
* @param - None
* @return void - returns Map of picklist value options
*/
    @AuraEnabled
    public static Map<String,String> fetchOpportunityContactRoles(){
        Map<String,String> mapOptions = new Map<String,String>();
        String profileId = UserInfo.getProfileId();
        String strProfile = [SELECT Name from Profile where Id = :profileId].Name;
        Schema.DescribeFieldResult fieldResult = OpportunityContactRole.Role.getDescribe();
        List<Schema.PicklistEntry> listValues = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry picklistVal : listValues){
            if(Constants.BIPROFILES.Contains(strProfile) && 
               Constants.BICONTACTROLEPICKLIST.CONTAINS(picklistVal.getValue())) {
                mapOptions.put(picklistVal.getValue(),picklistVal.getLabel());
            }
            else if(Constants.BSIPROFILES.Contains(strProfile) &&
                   Constants.BSICONTACTROLEPICKLIST.CONTAINS(picklistVal.getValue())) {
                mapOptions.put(picklistVal.getValue(),picklistVal.getLabel());
            }
        }
        return mapOptions;
    }
}