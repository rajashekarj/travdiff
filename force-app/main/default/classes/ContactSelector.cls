/**
 * This class is used as a Selector Layer for Contact object. 
 * All queries for Contact object will be performed in this class.
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Bharat Madaan            US63760        	 07/24/2019       Original Version
 * Bharat Madaan            US65898       	 09/09/2019       Updated method getContactsWithAccount
*/
public class ContactSelector {
/**
* This is a method is used to get list of Contacts having a Account lookup
* @param : Set<id>
* @return List<Contact> List of contact.
*/
   public static Map<Id,Id> getContactsWithAccount(Set<Id> setOwnerId)
    {
        Map<Id,Id> mapUserIdToContId = new Map<Id,Id>();
        List<Contact> listContact = [select Id, 
                               FirstName, 
                               LastName, 
                               AccountId,
                               TRAV_User__c
                               from Contact 
                               where TRAV_User__c  IN :setOwnerId];
        if( !listContact.isEmpty()){
            for(Contact objContact : listContact){
            mapUserIdToContId.put(objContact.TRAV_User__c,objContact.Id);
            }
        }
        return mapUserIdToContId;         
    } // End of method getContactsWithAccount 
   
    public static list<Contact> fetchAgencyContacts(set<id> setAgencyids){ 
        return [select id,accountid
                from 
                Contact 
                	where accountid in:setAgencyids 
                 and 
                    recordtype.developername in(:Constants.agencyLicensedRecordType,:Constants.agencyNonLicensedRecordType)
                      ];
    } //end Method fetchAgencyContacts 
    
    public static list<Contact> fetchAgencyContactsById(Set<id> setContactId){ 
        Id objRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('TRAV_Agency_Licensed_Contact').getRecordTypeId();
        Id objTRAVRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('TRAV_Agency_Non_Licensed_Contact').getRecordTypeId();
        List<Contact> listContact = new List<Contact>();
        listContact = [SELECT 
                         Id, Accountid, Name
                       FROM 
                         Contact 
                       WHERE 
                           ID IN :setContactId 
                       AND 
                            (RecordtypeId = :objRecordTypeId
                       OR 
                       RecordtypeId = :objTRAVRecordTypeId)
                      ];
        system.debug('listContact>>'+listContact);
         system.debug('setContactId>>'+setContactId);
         return listContact;
    } //end Method fetchAgencyContactsById 
    /*
    public static list<Contact> getContactRecordType(Set<id> setContactId){ 
        List<Contact> listContact = new List<Contact>();
        listContact = [SELECT 
                         Id, RecordType.Name
                       FROM 
                         Contact 
                       WHERE 
                           ID IN :setContactId
                      ];
        system.debug('listContact>>'+listContact);
        return listContact;
    }*/ //end Method getContactRecordType 
}