/**
 * Test class for BUHelpfulLinkController Class
 *
 * @Author : Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Isha Shukla            US63361              07/16/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
@isTest
private class TestBUHelpfulLinkController {
	/**
    * This method will test controller functionality of retrieving bu links
    * @param no parameters
    * @return void
    */
    static testmethod void testController() {
        User objUser = TestDataFactory.createTestUser('BI - National Accounts', 'Test BI user', 'Test lLast name');
        objUser.TRAV_BU__c = 'BI-NA';
        insert objUser;
         List<TRAV_BU_Helpful_Links__mdt> objHelpfulLinks;
        Test.startTest();
        System.runAs(objUser){
          objHelpfulLinks  = BUHelpfulLinkController.getBULinks();
        }
        Test.stopTest();
        system.assertEquals(objHelpfulLinks != null, true);
    }
}