@istest
private class TestBatchCopyContactBillingAddress {
    static testmethod void testexecute(){
     TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'ContactTrigger';
        insert objCustomsetting;
        TestDataFactory.createGeneralTriggersetting();
        Account ObjAgency= TestDataFactory.createAgency();
        ObjAgency.BillingStreet = '12 A Banking Street';
        ObjAgency.BillingCity = 'NewYork';
        ObjAgency.BillingCountry = 'USA';
        ObjAgency.BillingPostalCode ='515291';
        insert ObjAgency;
         Contact AgencyNonLicendContact = TestDataFactory.createAgencyNonLicensed(ObjAgency.id);
        insert AgencyNonLicendContact;
    test.starttest();
    BatchCopyContactBillingAddress bc = new BatchCopyContactBillingAddress();
    database.executeBatch(bc);
    test.stoptest();
        Contact objContoCheck =[select id,OtherStreet,OtherCity,OtherCountry,OtherPostalCode 
                                	from contact where id =:AgencyNonLicendContact.id];
        system.assertEquals(objContoCheck.OtherStreet,'12 A Banking Street');
        system.assertEquals(objContoCheck.OtherCity, 'NewYork');
        system.assertEquals(objContoCheck.OtherCountry, 'USA');
        system.assertEquals(objContoCheck.OtherPostalCode, '515291');
       
    }
}