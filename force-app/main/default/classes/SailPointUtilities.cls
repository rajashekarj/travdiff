/**
 * Implements the business logic for User-related changes that are triggered via the SailPoint integration.
 * The SailPoint connector does most of the initial user provisioning for Salesforce as well as user updates,
 * so this class only works with a smaller subset of User object fields. It references a Custom Metadata Type (MDT)
 * called "SailPoint Derivation Rule" to retrieve the business logic used to automatically set this subset of fields. 
 *
 * Modification Log:
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Ben Climan             US79918              04/08/2020       Original Version
 * Ben Climan             US79918              04/10/2020       Moved MDT deployment automation piece to separate class "MakeMDTScript"
 * Ben Climan             US79918              04/14/2020       Updated logic to address the case where a single User evaluates true 
                                                                for multiple derivation rules for the same field
 * Ben Climan             US79918              04/15/2020       Added createExceptionUtility to log async to avoid MIXED_DML_OPERATION errors
 * Ben Climan             US79918              04/17/2020       For UserRoleId and on Update operation ONLY, 
 *                                                              set User.IsActive = false if no UserRoleId rule matched for User
 * Ben Climan             US79918              04/24/2020       Updated defaulting logic for TimezoneSidKey:
                                                                it is a special case where derivation only takes place on INSERT operations, 
                                                                so this field should not default on UPDATE operations
 * -----------------------------------------------------------------------------------------------------------
 */
public without sharing class SailPointUtilities {
    // Below are so that we don't do unecessary SOQL for Profiles and UserRoles
    static Map<String, UserRole> referencedRoles = new Map<String, UserRole>();
    static Map<String, Profile> referencedProfiles = new Map<String, Profile>();

    // Map of field API names to default values per US79918 business requirements document
    static final Map<String, Object> DefaultDerivationFieldValues = new Map<String, Object>{
        'TimezoneSidKey' => 'America/New_York',
        'ProfileId' => 'BSI - Support',
        'TRAV_BSI_Role__c' => null,
        'TRAV_Region__c' => 'Home Office',
        'TRAV_BU__c' => 'BSI-PL'
    };

    // Map of Profile Names to Business Units per US79918 business requirements document
    static final Map<String, String> ProfileToBusinessUnit = new Map<String, String>{
        'BI - National Accounts' => 'BI-NA',
        'BI - Support' => 'BI-NA',
        'BSI - Executive' => 'BSI-PL', // Exception is if User.Title == 'VP, Financial Institutions'
        'BSI - Financial Institutions' => 'BSI-FI',
        'BSI - Public Company Liability' => 'BSI-PCL',
        'BSI - Professional Liability' => 'BSI-PL',
        'BSI - Private Non Profit' => 'BSI-PNP',
        'BSI - Support' => 'BSI-PL' // Exception is if User.Title == 'IOLDP Participant'
    };    

    /**
     * A custom Exception type to be thrown by the SailPointUtilities class.
     */
    private class SailPointDerivationException extends Exception { }

    /**
     * This class is used to group together the User record that has been evaluated,
     * along with the result of matching the User record against the set of conditions
     * for a single field to derive. This is needed in case there are multiple
     * rules that evaluate to true for a single field and for a single User.
     * In such a case, a comparison is done to determine which rule is "most true,"
     * simply meaning the rule that had the most matches for the User.
     */
    private class UserEvaluationResult {
        User evaluatedUser;
        String field; // field or assignTo
        String value;
        Integer matchCount;

        UserEvaluationResult(User u, String f, String v, Integer mc) {
            this.evaluatedUser = u;
            this.field = f;
            this.value = v;
            this.matchCount = mc;
        }

        Boolean compare(UserEvaluationResult r) {
            return r.matchCount >= this.matchCount;
        }
    }

    /**
     * Used for logging Apex exceptions async, to avoid MIXED_DML_ERROR throws. 
     */
    public class QueueableExceptionLog implements Queueable {
        String method;
        Exception e;
        String className;

        public QueueableExceptionLog(String className, String method, Exception e) {
            this.method = method;
            this.e = e;
            this.className = className;
        }

        public void execute(QueueableContext qc) {
            TRAV_Exception_Log__c log = new TRAV_Exception_Log__c();
            log.Class_Name__c = this.className;
            log.Method_Name__c = this.method;
            log.Log_Type__c = 'Application';
            log.Line__c = String.valueOf(this.e.getLineNumber());
            log.Message__c = this.e.getMessage();
            log.Error_Message__c = this.e.getStackTraceString();
            insert log;
        }
    }

    /**
     * Contains the list of field API names that CAN BE SET in an override scenario.
     * Field names not contained here WILL BE IGNORED for User updates.
     */
    public enum OverrideFields {
        LastName,
        MiddleName,
        Email,
        Alias,
        CommunityNickname,
        LocaleSidKey,
        LanguageLocaleKey,
        EmailEncodingKey,
        CompanyName,
        Phone,
        Fax,
        MobilePhone,
        FederationIdentifier,
        Username,
        IsActive 
    }

    /**
     * Query to retrieve all Derivation Rule records.
     * If test context, will return test record in case no MDTs exist yet in the current Org.
     * @return {List<SailPoint_Derivation_Rule__mdt>}
     */
    public static List<SailPoint_Derivation_Rule__mdt> getSailPointDerivationRules() {
        List<SailPoint_Derivation_Rule__mdt> derivationRules = [SELECT DeveloperName, 
        MasterLabel,
        Set_If_Override__c,
        Set_On_Trigger__c,
        Field_To_Set__c,
        Value_To_Set__c,
        Assign_User_To__c, 
        User_Department__c, 
        User_Division__c, 
        User_Office__c, 
        User_State__c, 
        User_Title__c FROM SailPoint_Derivation_Rule__mdt WHERE Is_Active__c = true];

        return derivationRules;
    }

    /**
     * Used internally to compare evaluation results and determine which rule should be used as the final one for setting the field.
     * @param {UserEvalationResult} compareTo
     * @param {List<UserEvaluationResult>} evalResults
     * @return {List<UserEvaluationResult>} The updated List of UserEvaluationResults to be used for setting the field 
     */
    private static List<UserEvaluationResult> compareAndSet(UserEvaluationResult compareTo, List<UserEvaluationResult> evalResults) {
        for (Integer i = 0; i < evalResults.size(); i++) {
            UserEvaluationResult result = evalResults.get(i);
            if (result.evaluatedUser.Username == compareTo.evaluatedUser.Username
            && result.field == compareTo.field) {
                if (result.compare(compareTo)) {
                    evalResults.remove(i);
                    evalResults.add(compareTo);
                    return evalResults; 
                }
                return evalResults;
            }
        }
        evalResults.add(compareTo);
        return evalResults;
    }

    /**
     * Helper method for evaluating a single custom MDT field against the corresponding user field.
     * @param {String} mdtVal
     * @param {String} userVal
     * @return {Boolean} True if mdtVal contains userVal
     */
    public static Boolean evaluateRuleHelper(String mdtVal, String userVal) {
        if (String.isBlank(userVal)) {
            return false;
        } else {
            List<String> cleansedVals = new List<String>(); 
            for (String sv : mdtVal.split(';')) {
                // Remove blank spaces between delimiters
                cleansedVals.add(sv.replaceAll('"', '').trim());
            }
            //return cleansedVals.contains(userVal);
            userVal = userVal.replaceAll('"', ''); // Remove any double quotes that may come from the HR extract
            for (String cleansedVal : cleansedVals) {
                if (userVal.contains(cleansedVal)) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * Evaluates a single SailPoint_Derivation_Rule__mdt to determine whether the derivation criteria is met.
     * @param {User} u The User record whose values will be evaluated against the derivation rule 
     * @param {SailPoint_Derivation_Rule__mdt} rule The rule to evaluate against
     * @return {Integer} The number of matched conditions
     */
    public static Integer evaluateRule(User u, SailPoint_Derivation_Rule__mdt rule) {
        String department = rule.User_Department__c;
        String division = rule.User_Division__c;
        String office = rule.User_Office__c;
        String state = rule.User_State__c;
        String title = rule.User_Title__c;

        List<Boolean> evalResults = new List<Boolean>();
        
        if (String.isNotBlank(department)) {
            evalResults.add(evaluateRuleHelper(department, u.Department));
        }
        if (String.isNotBlank(division)) {
            evalResults.add(evaluateRuleHelper(division, u.Division));
        }
        if (String.isNotBlank(office)) {
            evalResults.add(evaluateRuleHelper(office, u.TRAV_Office__c));
        }
        if (String.isNotBlank(state)) {
            evalResults.add(evaluateRuleHelper(state, u.State));
        }
        if (String.isNotBlank(title)) {
            evalResults.add(evaluateRuleHelper(title, u.Title));
        }

        // Return the count of matches; 0 indicates that the User did not match the rule
        // Loop over a value copy of evalResults.size() since we are mutating the List by removing items, 
        // which will cause the loop to not iterate the expected number of times
        Integer evalResultsCount = 0;
        for (Integer i = 0; i < evalResults.size(); i++) {
            if (evalResults.get(i)) evalResultsCount++; 
        }

        if (evalResults.contains(false)) evalResultsCount = 0;

        // Only exception to rules using an AND-like evaluation currently is the rule for assigning Role = 'BSI Leadership',
        // which uses an OR condition
        if (rule.DeveloperName == 'SPRoleBSILeadership') {
            Integer bsiLeadershipCount = 0;
            if (evaluateRuleHelper(division, u.Division)) {
                bsiLeadershipCount++;
                Boolean titleMatched = evaluateRuleHelper(title, u.Title);
                Boolean departmentMatched = evaluateRuleHelper(department, u.Department);
                if (!(titleMatched || departmentMatched)) {
                    bsiLeadershipCount = 0;
                } else {
                    if (titleMatched) bsiLeadershipCount++;
                    if (departmentMatched) bsiLeadershipCount++;
                }
            } else {
                bsiLeadershipCount = 0;
            }
            evalResultsCount = bsiLeadershipCount;
        }

        return evalResultsCount;
    }

    /**
     * Since SailPoint_Derivation_Rule__mdt Field_To_Set__c and Value_To_Set__c are just text fields,
     * this method is used to dynamically set the value of the field according to its actual database type.
     * @param {String} The intended field to dynamically set 
     * @param {String} The intended value to set the field to
     * @return {Object} The typed value to use for the field
     */
    public static Object setFieldValue(String fieldToSet, String valToSet) {
        Map<String, Schema.SObjectField> userFieldMap = Schema.SObjectType.User.fields.getMap();
        Schema.DescribeFieldResult dfr = userFieldMap.get(fieldToSet).getDescribe();
        if (!(dfr.isAccessible() || dfr.isCreateable())) {
            throw new SailPointDerivationException('The given fieldToSet cannot be set manually.');
        } 
        Schema.SoapType fieldType = dfr.getSoapType(); // Get the DB type as a String
        Object val;
        switch on fieldType {
            when BOOLEAN {
                valToSet = valToSet.toLowerCase();
                if (!(valToSet == 'true' || valToSet == 'false')) {
                    throw new SailPointDerivationException('The given fieldToSet is a BOOLEAN; expected true or false, but got: ' + valToSet);
                }
                val = valToSet == 'true' ? true : false;
            }
            when DATE {
                val = Date.parse(valToSet);
            }
            when DATETIME {
                val = Datetime.parse(valToSet);
            }
            when DOUBLE {
                val = Double.valueOf(valToSet);
            }
            when INTEGER {
                val = Integer.valueOf(valToSet);
            }
            // ID is a separate case from String even though they are essentially the same in Apex.
            // This way a user can enter the name of a reference rather than the ID in the custom MDT, for ease of use.
            when ID {
                String fieldName = userFieldMap.get(fieldToSet).getDescribe().getName();
                if (fieldName == 'UserRoleId') {
                    if (!SailPointUtilities.referencedRoles.containsKey(valToSet)) {
                        SailPointUtilities.referencedRoles.put(valToSet, SailPointUtilities.getRole(valToSet));
                    }
                    val = SailPointUtilities.referencedRoles.get(valToSet).Id;
                }
                if (fieldName == 'ProfileId') {
                    if (!SailPointUtilities.referencedProfiles.containsKey(valToSet)) {
                        SailPointUtilities.referencedProfiles.put(valToSet, SailPointUtilities.getProfile(valToSet));
                    }
                    val = SailPointUtilities.referencedProfiles.get(valToSet).Id;
                }
            }
            when STRING {
                val = valToSet;
            }
            when else {
                // System cannot set other fieldTypes
                System.debug(fieldType);
                SailPointDerivationException sde = new SailPointDerivationException('Unable to set field of type ' + fieldType);
                System.enqueueJob(new QueueableExceptionLog('SailPointUtilities', 'setFieldValue', sde));
                throw sde;
            }
        }
        return val;
    }

    /**
     * Derives all fields with entries in SailPoint_Derivation_Rule__mdt.
     * @param {List<User>} The users provided by the trigger context 
     * @param {List<SailPoint_Derivation_Rule__mdt>} The list of applicable field rules to reference
     */
    public static void deriveFields(List<User> users, List<SailPoint_Derivation_Rule__mdt> rules) {
        List<UserEvaluationResult> evaluationResults = new List<UserEvaluationResult>();
        Boolean isInsertFlag = Trigger.isBefore && Trigger.isInsert; // Used to determine whether or not to apply the TimezoneSidKey default
        Boolean isUpdateFlag = Trigger.isBefore && Trigger.isUpdate; // For making determination on whether or not to consider User deactivation if no UserRoleId rules match
        Map<Id, Boolean> userIdToRoleResults = new Map<Id, Boolean>(); // For making determination on whether or not to consider User deactivation if no UserRoleId rules match

        for (User u : users) {
            userIdToRoleResults.put(u.Id, false);
            for (SailPoint_Derivation_Rule__mdt rule : rules) {
                if (u.TRAV_Override_Automation__c == false 
                || (u.TRAV_Override_Automation__c == true && rule.Set_If_Override__c == true)) {
                    Integer matchCount = evaluateRule(u, rule);
                    // If 0, User did not meet rule criteria
                    if (matchCount > 0) {
                        String fieldToSet = rule.Field_To_Set__c;
                        String valToSet = rule.Value_To_Set__c;
                        if (fieldToSet == 'UserRoleId' && !userIdToRoleResults.get(u.Id)) userIdToRoleResults.put(u.Id, true); 
                        // Rather than immediately setting the User field, compare results to determine which rule will be used
                        UserEvaluationResult evaluationResult = new UserEvaluationResult(u, fieldToSet, valToSet, matchCount);
                        evaluationResults = compareAndSet(evaluationResult, evaluationResults); 
                    }
                }
            }
            // If all rules evaluated for user, and no UserRoleId rule matched, set the User to inactive
            if (isUpdateFlag && !userIdToRoleResults.get(u.Id)) u.IsActive = false;
        }

        // Map of Username => User record
        Map<String, User> updatedUsers = new Map<String, User>();
        // Map of Username => copy of User record, to determine which fields were actually derived in this transaction
        Map<String, User> userUpdatesInTransaction = new Map<String, User>();

        // Set the field values for each User
        for (UserEvaluationResult er : evaluationResults) {
            // We don't know if fieldToSet and valToSet are valid, so try/catch.
            // This also handles the case where a derivation field has been mistakenly set to a field
            // that is not modifiable via code.
            try {
                er.evaluatedUser.put(er.field, setFieldValue(er.field, er.value));
                if (er.field == 'ProfileId') { // Business Unit is assigned according to Profile
                    switch on er.value {
                        when 'BSI - Executive' {
                            er.evaluatedUser.put('TRAV_BU__c', er.evaluatedUser.Title == 'VP, Financial Institutions' ? 'BSI-FI' : ProfileToBusinessUnit.get(er.value));
                        }
                        when 'BSI - Support' {
                            er.evaluatedUser.put('TRAV_BU__c', er.evaluatedUser.Title == 'IOLDP Participant' ? 'BSI-FI' : ProfileToBusinessUnit.get(er.value));
                        }
                        when else {
                            er.evaluatedUser.put('TRAV_BU__c', ProfileToBusinessUnit.get(er.value));
                        }
                    }
                }
                String username = er.evaluatedUser.Username; 
                updatedUsers.put(username, er.evaluatedUser);

                // Need to create a separate copy of the updated User record that ONLY contains the fields that have actually been derived in this transaction,
                // as the User record from the Trigger context variable has EVERY field populated even if not updated in this transaction,
                // plus we can't tamper with the context variable User since this is a BEFORE trigger and we aren't doing DML
                User userCopy = userUpdatesInTransaction.get(username);
                userCopy = userCopy == null ? new User(Username=username) : userCopy;
                userCopy.put(er.field, er.evaluatedUser.get(er.field));
                if (er.field == 'ProfileId') userCopy.put('TRAV_BU__c', er.evaluatedUser.get('TRAV_BU__c')); 
                userUpdatesInTransaction.put(username, userCopy);
            } catch (Exception e) {
                System.debug(e);
                if (!Test.isRunningTest()) {
                    System.enqueueJob(new QueueableExceptionLog('SailPointUtilities', 'deriveFields', e));
                }
            }
        }

        // Set default values if no rules matched
        for (String username : userUpdatesInTransaction.keySet()) {
            User userUpdatedInTransaction = userUpdatesInTransaction.get(username); // The copy of the User to check which fields were set in this transaction
            User ctxUser = updatedUsers.get(username); // The context User to set field values for
            

            // TimezoneSidKey is a special case where derivation only takes place on INSERT operations,
            // so this field should not default on UPDATE operations
            if (!userUpdatedInTransaction.isSet('TimezoneSidKey') && isInsertFlag) {
                ctxUser.put('TimezoneSidKey', DefaultDerivationFieldValues.get('TimezoneSidKey'));
            }
            if (!userUpdatedInTransaction.isSet('ProfileId')) {
                String defaultProfileName = (String)DefaultDerivationFieldValues.get('ProfileId');
                Profile defaultProfile = getProfile(defaultProfileName);
                ctxUser.put('ProfileId', defaultProfile.Id);
            }
            if (!userUpdatedInTransaction.isSet('TRAV_BSI_Role__c')) {
                ctxUser.put('TRAV_BSI_Role__c', DefaultDerivationFieldValues.get('TRAV_BSI_Role__c'));
            }
            if (!userUpdatedInTransaction.isSet('TRAV_Region__c')) {
                ctxUser.put('TRAV_Region__c', DefaultDerivationFieldValues.get('TRAV_Region__c'));
            }
            if (!userUpdatedInTransaction.isSet('TRAV_BU__c')) {
                ctxUser.put('TRAV_BU__c', DefaultDerivationFieldValues.get('TRAV_BU__c'));
            }  
        }
    }

    /**
     * Evaluates the SailPoint_Derivation_Rule__mdt records and assigns Users to Permission Sets and Public Groups.
     * Called by the AFTER INSERT or AFTER UPDATE methods.
     * @param {List<User>} The users provided by the trigger context 
     * @param {List<SailPoint_Derivation_Rule__mdt>} The list of applicable field rules to reference
     */
    public static void assignUsers(List<User> users, List<SailPoint_Derivation_Rule__mdt> rules) {
        List<UserEvaluationResult> permSetEvalResults = new List<UserEvaluationResult>();
        List<UserEvaluationResult> pubGroupEvalResults = new List<UserEvaluationResult>();
        Map<String, List<User>> assignToPermissionSet = new Map<String, List<User>>();
        Map<String, List<User>> assignToPublicGroup = new Map<String, List<User>>();

        for (User u : users) {
            for (SailPoint_Derivation_Rule__mdt rule : rules) {
                if (rule.Assign_User_To__c != null) {
                    if (u.TRAV_Override_Automation__c == false
                    || (u.TRAV_Override_Automation__c == true && rule.Set_If_Override__c == true)) {
                        Integer matchCount = evaluateRule(u, rule);
                        // If 0, User did not meet rule criteria
                        if (matchCount > 0) { 
                            String assignTo = rule.Assign_User_To__c.toLowerCase();
                            String valToSet = rule.Value_To_Set__c;
                            UserEvaluationResult evalResult = new UserEvaluationResult(u, valToSet, valToSet, matchCount);

                            if (assignTo == 'permission set') {
                                // Permission Set and Public Group assignments must be compared separately 
                                permSetEvalResults = compareAndSet(evalResult, permSetEvalResults);
                                if (!assignToPermissionSet.containsKey(valToSet)) {
                                    assignToPermissionSet.put(valToSet, new List<User>());    
                                }
                            }

                            if (assignTo == 'public group') {
                                pubGroupEvalResults = compareAndSet(evalResult, pubGroupEvalResults);
                                if (!assignToPublicGroup.containsKey(valToSet)) {
                                    assignToPublicGroup.put(valToSet, new List<User>());
                                }
                            }
                        }
                    }
                }
            }
        }

        for (UserEvaluationResult permSetResult : permSetEvalResults) {
            List<User> usersToPermissionSet = assignToPermissionSet.get(permSetResult.value);
            usersToPermissionSet.add(permSetResult.evaluatedUser);
            assignToPermissionSet.put(permSetResult.value, usersToPermissionSet);
        }

        for (UserEvaluationResult pubGroupResult : pubGroupEvalResults) {
            List<User> usersToPubGroup = assignToPublicGroup.get(pubGroupResult.value);
            usersToPubGroup.add(pubGroupResult.evaluatedUser);
            assignToPublicGroup.put(pubGroupResult.value, usersToPubGroup);
        }

        if (assignToPermissionSet.size() > 0) {
            for (String permSetName : assignToPermissionSet.keySet()) {
                UserService.assignPermissionSet(permSetName, assignToPermissionSet.get(permSetName));
            }
        }

        if (assignToPublicGroup.size() > 0) {
            for (String groupName : assignToPublicGroup.keySet()) {
                UserService.assignPublicGroup(groupName, assignToPublicGroup.get(groupName));
            }
        }
    }

    /**
     * Used to ensure that all pertinent User fields are populated.
     * @param {List<User>} The users to get the fields for
     * @return {List<User>} The users with the fields populated
     */
    public static List<User> getUsersWithSPFields(List<User> users) {
        return
        [SELECT
        Id,
        IsActive,
        FirstName,
        LastName,
        Username,
        ProfileId,
        TimeZoneSidKey,
        UserRoleId,
        TRAV_BSI_Role__c,
        TRAV_BU__c,
        TRAV_Manager__c,
        TRAV_Region__c,
        TRAV_Office__c,
        Title,
        State,
        Department,
        Division,
        TRAV_Override_Automation__c FROM User WHERE Id IN :users];
    }

    /**
     * Gets the Role with the Name.
     * @param {String} roleName
     * @return {UserRole}
     */
    public static UserRole getRole(String roleName) {
        List<UserRole> roles = [SELECT Id, Name FROM UserRole WHERE Name = :roleName];
        if (roles.size() > 0) return roles.get(0);
        return null;
    }

    /**
     * Gets the Profile with the Name.
     * @param{String} profileName
     * @return {Profile}
     */
    public static Profile getProfile(String profileName) {
        List<Profile> profiles = [SELECT Id, Name FROM Profile WHERE Name = :profileName];
        if (profiles.size() > 0) return profiles.get(0);
        return null;
    }

    /**
     * Gets the Permission Set with the Name.
     * @param{String} permissionSetName
     * @return{PermissionSet}
     */
    public static PermissionSet getPermissionSet(String permissionSetName) {
        List<PermissionSet> permissionSets = [SELECT Id FROM PermissionSet WHERE Label = :permissionSetName];
        if (permissionSets.size() > 0) return permissionSets.get(0);
        return null;
    }

    /**
     * Gets the Public Group with the Name.
     * @param{String} publicGroupName
     * @return{Group}
     */
    public static Group getPublicGroup(String publicGroupName) {
        List<Group> publicGroups = [SELECT Id, Name FROM Group WHERE Type = 'Regular' AND Name = :publicGroupName];
        if (publicGroups.size() > 0) return publicGroups.get(0);
        return null;
    }

    /**
     * Used to determine whether or not fields set via SailPoint should be reverted in an override scenario.
     * @param {Map<Id, SObject>} newItems Used to check field values against the OverrideFields
     * @return {Map<Id, SObject} oldItems Used in case field values need to be reverted
     */
    public static void checkOverrideFields(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        // TRAV_External_Id__c has to be hardcoded, underscores not valid for variable names        
        List<String> overrideFieldVals = new List<String>{'TRAV_External_Id__c'};
        
        for (OverrideFields f : OverrideFields.values()) {
            overrideFieldVals.add(f.name());
        }
        
        for (Id userId : newItems.keySet()) {
            User u = (User)newItems.get(userId);
            Map<String, Object> fields = u.getPopulatedFieldsAsMap();
            Boolean overrideA = (Boolean)fields.get('TRAV_Override_Automation__c');
            if (overrideA == null || !overrideA) continue; // If override not set on User, we can set any of the updated fields
            Map<String, Schema.SObjectField> userFieldMap = Schema.SObjectType.User.fields.getMap();
            for (String fieldName : fields.keySet()) {
                Schema.DescribeFieldResult dfr = userFieldMap.get(fieldName).getDescribe();
                Object newVal = fields.get(fieldName);
                Object oldVal = oldItems.get(userId).getPopulatedFieldsAsMap().get(fieldName);
                if (dfr.isUpdateable() && !overrideFieldVals.contains(fieldName) && newVal != oldVal) {
                    // Then revert to old value
                    u.put(fieldName, oldVal);
                }
            }
        }
    }
}