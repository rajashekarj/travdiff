/**
* Test class for BatchAnalyticalSnapshotPopulate Class
*
* @Author : Deloitte Consulting
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------
* Siddharth Menon           US77581          03/06/2020       Original Version
* -----------------------------------------------------------------------------------------------------------
*/
@isTest
public class TestBatchAnalyticalSnapshotPopulate {
    @isTest
    public static void checkBatch(){
         Account prospectAccount = TestDataFactory.createProspect(); 
        List<Account> lstAccount = new List<Account>{prospectAccount};
            insert lstAccount;
        Id objRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('TRAV_Agency_Licensed_Contact').getRecordTypeId();
        List<Contact> lstContact = TestDataFactory.createTravelersContact(2);
        lstContact[0].RecordTypeId = objRecordTypeId;
        lstContact[1].RecordTypeId = objRecordTypeId;
        lstContact[0].AccountId = lstAccount[0].Id;
        lstContact[1].AccountId = lstAccount[0].Id;
        insert lstContact;
        Event objEvent = new Event();
        objEvent.OwnerId = UserInfo.getUserId(); //user id
        objEvent.WhoId = lstContact[0].Id; //record id
        objEvent.WhatId= lstAccount[0].Id;
        objEvent.DurationInMinutes = 60;
        objEvent.ActivityDateTime = System.now();
        insert objEvent;
        User objUser= TestDataFactory.createTestAdminUser('fName', 'Test');
        insert objUser;
        EventRelation objRelation= new EventRelation();
        objRelation.EventId=objEvent.Id;
        objRelation.RelationId=objUser.id;
        insert objRelation;
        Test.startTest();
        Database.executeBatch(new BatchAnalyticalSnapshotPopulate());
        Test.stopTest();
        system.assertEquals([Select id from TRAV_Analytical_Snapshot__c].size(),2);
        
    }

}