/**
 * This class is used as a Selector Layer for OpportunityCarrier object. 
 * All queries for OpportunityCarrier object will be performed in this class.
 *
 *
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect   Date            Description
 * -----------------------------------------------------------------------------------------------------------              
 * Erec Lawrie            US62419             07/02/2019      Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
public class OpportunityCarrierSelector {
    /**
     * This is a method is used to get list of opportunity Carriers from ID
     * @param string Opportunities id set
     * @return List<Opportunity> List of Opportunities based on the SOQL query provided as an input parameter.
     */
    public static CarrierListsObject getCarrierListsFromOppCarriers(Set<Id> oppIds) {
        CarrierListsObject mapOppToCarrierLists = new CarrierListsObject();
        Id oppId = null;
        Id carrierId = null;
        String carrierType = '';
        List<id> lstCarrierIds = null;
 
        system.debug('Inside opp selector');
        if (!oppIds.IsEmpty()) {
            lstCarrierIds = new List<id>();
            Integer indexer = 0;

            //capture a list of carriers grouped by typefrom the opportunity carriers that are 
            for(TRAV_Opportunity_Carrier__c c : [SELECT TRAV_Carrier_Type__c, TRAV_Opportunity__r.Id, TRAV_Carrier__r.Id
                                                FROM TRAV_Opportunity_Carrier__c
                                                WHERE TRAV_Opportunity__r.ID = :oppIds
                                                ORDER BY TRAV_Opportunity__r.ID, TRAV_Carrier_Type__c
                                                ]) {
                oppId = c.TRAV_Opportunity__r.Id;
                carrierId = c.TRAV_Carrier__r.Id;
                carrierType = c.TRAV_Carrier_Type__c;

                indexer++;
                system.debug('*'+indexer+'* '+ carrierType);
                system.debug('*'+indexer+'* '+ c);
                switch on carrierType {
                    when 'Competing' {
                        if((lstCarrierIds = mapOppToCarrierLists.oppCompetingCarriers.get(oppId)) == null) {
                            mapOppToCarrierLists.oppCompetingCarriers.put(oppId, lstCarrierIds = new List<id>());
                            system.debug('*'+indexer+'* '+'Competing - new list: ' +mapOppToCarrierLists.oppCompetingCarriers.get(oppId));
                        }              
                        lstCarrierIds.add(carrierId);
                        system.debug('*'+indexer+'* '+'Competing - after add: ' +mapOppToCarrierLists.oppCompetingCarriers.get(oppId));
                    }
                    when 'Incumbent' {
                        if((lstCarrierIds = mapOppToCarrierLists.oppIncumbentCarriers.get(oppId)) == null) {
                            mapOppToCarrierLists.oppIncumbentCarriers.put(oppId, lstCarrierIds = new List<id>());
                            system.debug('*'+indexer+'* '+'incumbent - new list: ' +mapOppToCarrierLists.oppIncumbentCarriers.get(oppId));
                        }
                        lstCarrierIds.add(carrierId);
                        system.debug('*'+indexer+'* '+'incumbent - after add: ' +mapOppToCarrierLists.oppIncumbentCarriers.get(oppId));
                    }
                    when else {system.debug('unknown carrier type');}
                }
                oppId = null;
                carrierId = null;
            }

            indexer = 0;

            //finally update the list of carriers according to the business rules
            List<TRAV_Carrier__c> unknownCarrier = null;
            lstCarrierIds = null;
            for(Id opportunityId : oppIds) {
                lstCarrierIds = mapOppToCarrierLists.oppCompetingCarriers.get(opportunityId);
                indexer++;

                //set Competing to 0 if it has more than 5
                if(lstCarrierIds != null && lstCarrierIds.size() > 5) {
                    system.debug('*'+indexer+'* '+'Competing - size > 5: ' +lstCarrierIds.size());
                    mapOppToCarrierLists.oppCompetingCarriers.put(opportunityId, null);
                }

                lstCarrierIds = mapOppToCarrierLists.oppIncumbentCarriers.get(opportunityId);

                //set incumbent to 'UNKNONWN' if it does not have exactly 1 
                if(lstCarrierIds == null || (lstCarrierIds != null && lstCarrierIds.size() != 1)) {
                    system.debug('*'+indexer+'* '+'incumbent - size != 1');
                    if (unknownCarrier == null) {
                        unknownCarrier = [SELECT Id FROM TRAV_Carrier__c WHERE TRAV_External_Id__c = :Constants.UnknownCarrierExternalId LIMIT 1];
                    }
                    if(unknownCarrier.size() > 0){ 
                    	mapOppToCarrierLists.oppIncumbentCarriers.put(opportunityId, new List<Id>{unknownCarrier[0].Id});
                	}
                    system.debug('*'+indexer+'* '+'incumbent - after add unknown: ' +mapOppToCarrierLists.oppIncumbentCarriers.get(opportunityId));
                }

                lstCarrierIds = null;
            }
        } else {
	        system.debug('opportunity id list is empty.');
        }

        return mapOppToCarrierLists;
    }
}