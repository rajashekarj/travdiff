/**
* @author Sayanka Mohanty
* @date   12/18/2019
* @description  
      
Modification Log:
----------------------------------------------------------------------------------------
Developer                   Date                Description
----------------------------------------------------------------------------------------
Sayanka Mohanty           12/18/2019          Original Version
*/
public class PopupDataWrapper {
    
    @AuraEnabled
    public String columnName{get;set;}
    @AuraEnabled
    public String defaultValue{get;set;}
    @AuraEnabled
	public List<String> values{get;set;}
    @AuraEnabled
    public String originalValue{get;set;}
    @AuraEnabled
    public String inpValue{get;set;}
    @AuraEnabled
    public String section{get;set;}
    @AuraEnabled
    public Boolean required{get;set;}
    @AuraEnabled
    public String validationMessage{get;set;}
    @AuraEnabled
    public String columnType{get;set;}
    @AuraEnabled
    public Boolean isReadOnly{get;set;}
    @AuraEnabled
    public String sfField{get;set;}
    @AuraEnabled
    public Id sObjId{get;set;}
    @AuraEnabled
    public Id sfOpportunityProductId{get;set;}
    @AuraEnabled
    public String fieldValue{get;set;}
    @AuraEnabled
    public String sameStageError{get;set;}
    @AuraEnabled
    public List<CloseReasonWrapper> closeReasonList{get;set;}
    public class CloseReasonWrapper{
        
        @AuraEnabled
        public String reasonValue{get;set;}
        @AuraEnabled
        public Boolean reasonChecked{get;set;}
    }
    
    //Functionality Specific attributes
    @AuraEnabled
    public TRAV_Carrier__c defaultCarrier{get;set;}
    @AuraEnabled
    public TRAV_Carrier__c selectedLookupRecord{get;set;}
}