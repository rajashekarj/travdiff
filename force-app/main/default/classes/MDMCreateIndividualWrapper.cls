/**
* This class is the utility class for Contact Search and Pull Functionality
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect          Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty        US69347/US69863          11/10/2019      Original Version
* -----------------------------------------------------------------------------------------------------------
*/
public class MDMCreateIndividualWrapper {

    public class BMLoc {
        public String bmLocId{get;set;}
    }

    public class roleCdArr {
        public String roleCd{get;set;}
    }
    public class RuArr {
        public String ruCd;
    }
    
    public class SpclCdArr {
        public String spclCd;
    }
    public String srcSysCd = '';
    public String srcId = '';
    public String forceUpdt = '';
    public String fullNm = '';
    public String fstNm = '';
    public String lstNm = '';
    public String updatedBy = '';
    public String busTelNbr = '';
    public String licInd = 'N';
    public String busEmailAddr = '';
    public String birthDt;
    public String ptyNm = '';
    public List<BMLoc> BMLoc{get;set;}
    public List<roleCdArr> roleCdArr{get;set;}
    public List<RuArr> ruArr{get;set;}
	public List<SpclCdArr> spclCdArr{get;set;}

    
    /*public static MDMCreateIndividualWrapper parse(String json) {
        return (MDMCreateIndividualWrapper) System.JSON.deserialize(json, MDMCreateIndividualWrapper.class);
    }*/
    
    public MDMCreateIndividualWrapper(String strObjId, Contact contactRec){
        BMLoc bmLocRec = new BMLoc();
        if(!test.isRunningTest()){
            bmLocRec.bmLocId = (NewContactSearchAndPullMDM.bnmId).startsWith('BM_') ? (NewContactSearchAndPullMDM.bnmId).removeStart('BM_') : NewContactSearchAndPullMDM.bnmId;
        }else{
            bmLocRec.bmLocId = '1059253';
        }
        this.BMLoc = new List<BMLoc>();
        this.BMLoc.add(bmLocRec);
		roleCdArr roleCDRec = new roleCdArr();
        roleCDRec.roleCd = 'CSR';
        this.roleCdArr = new List<roleCdArr>();
        this.roleCdArr.add(roleCDRec);
        
        this.fullNm = contactRec.LastName+', '+contactRec.FirstName;
        this.ptyNm = contactRec.LastName+', '+contactRec.FirstName;
        this.fstNm = contactRec.FirstName;
        this.lstNm = contactRec.LastName;
        this.busTelNbr = contactRec.Phone;
        this.busEmailAddr = contactRec.Email;
        this.birthDt = contactRec.Birthdate != null ? String.valueOf(contactRec.Birthdate).remove('-') : '';
        this.srcSysCd = 'SFSC';
        this.srcId = contactRec.TRAV_GU_Id__c;
        this.updatedBy = userinfo.getFirstName();
        this.forceUpdt = 'N';
    }
}