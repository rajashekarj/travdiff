/**
* Test class for NewContactSearchAndPullCustomerTPA Class
*
* @Author : Deloitte Consulting
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Tejal Pradhan            US66730            11/11/2019       Original Version
* -----------------------------------------------------------------------------------------------------------
*/
@isTest
public class TestNewContactSearchAndPullCustomerTPA {
	static testmethod void testDIprof() {
        TRAV_Exception_Log__c objException = new TRAV_Exception_Log__c();
        Profile p = [SELECT Id FROM Profile WHERE Name='Devops Administrator']; 
      User u = new User(Alias = '123te', Email='standarduser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='123435te@testte.com');

      System.runAs(u) {
         
        objException.Class_Name__c = '';
        objException.Method_Name__c = '';
        objException.Error_Message__c = '';
        if(objException != null){
            ExceptionUtility.logIntegrationerror(null, null, null);
            //Database.insert(objException);
        }
//System.debug('---' + objException); 
      }
    }
    /**
* This method will test scenarios when a contact record is searched
* @return void
*/
    static testmethod void testSearchSobjectRecords() {
        
        //create account record
        Account objAgencyAcc = TestDataFactory.createProspect(); 
        insert objAgencyAcc;
        
        
        //create account record
        Account objTPAAcc = TestDataFactory.createTPAAccount(); 
        insert objTPAAcc;
        //create opportunity record
        Opportunity objOpp = TestDataFactory.createOppty('Prospect','Test Opp',objAgencyAcc.Id); 
        insert objOpp;
        
        //create Contact record
        List<Contact> lstContact = TestDataFactory.createTravelersContact(3);
        lstContact[0].accountId = objAgencyAcc.Id; 
        lstContact[0].RecordTypeId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(Constants.CustomerRecordType).getRecordTypeId();
        lstContact[1].accountId = objAgencyAcc.Id; 
        lstContact[1].Email = 'testEmail@test.com'; 
        lstContact[1].RecordTypeId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(Constants.agencyNonLicensedRecordType).getRecordTypeId();
        lstContact[2].accountId = objAgencyAcc.Id; 
        lstContact[2].RecordTypeId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(Constants.TPARcordType).getRecordTypeId();
        
        NewContactSearchAndPullCustomerTPA.SelectedRecordWrapper selectedWrapperResult = new NewContactSearchAndPullCustomerTPA.SelectedRecordWrapper();
        Test.startTest();
        selectedWrapperResult = NewContactSearchAndPullCustomerTPA.saveContact(lstContact[0], objOpp.Id, '', 'Customer_Contact'); 
        Test.stopTest();
        //System.assertNotEquals(strResultsCustomer, null);
        
    }
    public static testmethod void testListTitle(){
        NewContactSearchAndPullMDM.getTitleValues();
    }
}