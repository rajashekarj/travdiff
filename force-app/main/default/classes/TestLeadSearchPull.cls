/**
* Test class for Lead Search and Pull Classes
*
* @Author : Deloitte Consulting
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer                     User Story/Defect                      Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty           US73994/US73995/US73996                  2/3/2019       Original Version
-----------------------------------------------------------------------------------------------------------
*/
@isTest
public class TestLeadSearchPull {
//Setup Test Data
 @testSetup static void createTestData(){
     //Create NAICS Data
     List<TRAV_NAICS__c> listNAICS = new List<TRAV_NAICS__c>();
     TRAV_NAICS__c objNaics = new TRAV_NAICS__c();
     objNaics.TRAV_NAICS_Code__c = '12345';
     objNaics.Name = 'Groot';
     listNAICS.add(objNaics);
     if(!listNAICS.isEmpty()){
         insert listNAICS;
     }
     //Create SIC Data
     List<TRAV_SIC__c> listSIC = new List<TRAV_SIC__c>();
     TRAV_SIC__c objSIC = new TRAV_SIC__c();
     objSIC.TRAV_SIC_Code__c = '6789';
     objSIC.Name = 'Rocket';
     listSIC.add(objSIC);
     if(!listSIC.isEmpty()){
         insert listSIC;
     }
 }
//Test SEARCH Scenario
static testmethod void testExecuteSearchPullOperations(){
    Test.startTest();
    //HAPPY SCENARIO
    //Create a fake response for Search API
    MockHttpResponseGenerator fakeSearchResp = new MockHttpResponseGenerator( 200,'Complete',Constants.LEAD_SEARCH_RESPONSE,null);
    MockHttpResponseGenerator fakeSearchRespErr = new MockHttpResponseGenerator( 202,'Complete','{"ERROR":SIP-23038: Internal error. See the app server log files for error information}',null);
    Map<String, HttpCalloutMock> endpointTestResp = new Map<String,HttpCalloutMock>();
    endpointTestResp.put('callout:SfscGateway/searchAccount?fq=Name=Coca%20cola%20and%20State=TX&depth=1&suppressLinks=true',fakeSearchResp);                                                                            
    endpointTestResp.put('callout:SfscGateway/searchAccount?fq=Name=Coca%20cola%20and%20State=TX%20and%20City=Plano&depth=1&suppressLinks=true',fakeSearchRespErr);                                                                            
    HttpCalloutMock multiCalloutMock =
        new MultiRequestMock(endpointTestResp);
    Test.setMock(HttpCalloutMock.class, multiCalloutMock);
    //Create Search Wrapper and call Lead Utility main class
    LeadSPSearchRequestWrapper wrapSearchRequest = new LeadSPSearchRequestWrapper();
    wrapSearchRequest.companyName = 'Coca cola';
    wrapSearchRequest.state = 'TX';
    wrapSearchRequest.city = '';
    wrapSearchRequest.street = '';
    LeadSPRequestWrapper wrapRequest = new LeadSPRequestWrapper();
    wrapRequest.searchRequestWrapper = wrapSearchRequest;
    LeadSPResponseWrapper wrapResponse = LeadSearchPullUtility.executeLeadSearchPullOperations(Constants.LEAD_SEARCH_OP, wrapRequest);
    system.debug('**Response in wrapper**'+wrapResponse);
    system.assert(wrapResponse != null);
    //ALL PARAMS and return ERROR Response
    LeadSPSearchRequestWrapper wrapSearchRequestAll = new LeadSPSearchRequestWrapper();
    wrapSearchRequestAll.companyName = 'Coca cola';
    wrapSearchRequestAll.state = 'TX';
    wrapSearchRequestAll.city = 'Plano';
    //wrapSearchRequestAll.street = '2805';
    LeadSPRequestWrapper wrapRequestAll = new LeadSPRequestWrapper();
    wrapRequestAll.searchRequestWrapper = wrapSearchRequestAll;
    LeadSPResponseWrapper wrapResponseAll = LeadSearchPullUtility.executeLeadSearchPullOperations(Constants.LEAD_SEARCH_OP, wrapRequestAll);
    Test.stopTest();
}
//Test CREATE & GET Scenario
    static testmethod void testExecuteCreateOperations(){
        Test.startTest();
        //Form Request Wrapper
        LeadSPCreateRequestWrapper wrapperResult = (LeadSPCreateRequestWrapper)JSON.deserialize(Constants.CREATE_REQUEST_JSON,LeadSPCreateRequestWrapper.class);
        LeadSPRequestWrapper wrapRequest = new LeadSPRequestWrapper();
        LeadSPSFFieldsWrapper wrapSFFields = new LeadSPSFFieldsWrapper();
        wrapSFFields.bUInterested = 'Professional Liability';
        wrapSFFields.effectiveDate = date.today();
        wrapSFFields.expirationDate = date.today();
        wrapSFFields.industry = 'SampleInd';
        wrapSFFields.leadSource = 'Advertisement';
        wrapSFFields.leadStatus = 'active';
        wrapSFFields.totalAssets = '908999';
        wrapRequest.getSFFieldsResponseWrapper = wrapSFFields;
        wrapRequest.createRequestWrapper = wrapperResult;
        wrapRequest.cbeID = 'TRV00002800002';
        //Create a fake response for Search API
        MockHttpResponseGenerator fakeCreateResp = new MockHttpResponseGenerator( 200,'Complete',Constants.LEAD_CREATE_RESPONSE,null);
        MockHttpResponseGenerator fakeGETResp = new MockHttpResponseGenerator( 200,'Complete',Constants.LEAD_GET_RESPONSE,null);
        MockHttpResponseGenerator fakeCreateRespErr = new MockHttpResponseGenerator( 202,'Complete','{"Error":"Java payload creation failureTypeError: Cannot read property trim of undefined"}',null);
        Map<String, HttpCalloutMock> endpointTestResp = new Map<String,HttpCalloutMock>();
        endpointTestResp.put('callout:SfscGateway/postAccount',fakeCreateResp);
        endpointTestResp.put('callout:SfscGateway/getAccount?cbeID=TRV00002800002',fakeGETResp);
        HttpCalloutMock multiCalloutMock =
            new MultiRequestMock(endpointTestResp);
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);
        LeadSPResponseWrapper wrapResponseAll = LeadSearchPullUtility.executeLeadSearchPullOperations(CONSTANTS.LEAD_CREATE_SF_CB_OP,wrapRequest);
        Test.stopTest();
    }
//TEST OTHER SCENARIOS
    static testmethod void testExecuteOtherOperations(){
        Test.startTest();
        LeadSPRequestWrapper wrapRequest = new LeadSPRequestWrapper();
        wrapRequest.cbeID = 'TRV00002900002';
        MockHttpResponseGenerator fakeGETResp = new MockHttpResponseGenerator( 200,'Complete',Constants.LEAD_GET_RESPONSE2,null);
        Map<String, HttpCalloutMock> endpointTestResp = new Map<String,HttpCalloutMock>();
        endpointTestResp.put('callout:SfscGateway/getAccount?cbeID=TRV00002900002',fakeGETResp);
        HttpCalloutMock multiCalloutMock =
            new MultiRequestMock(endpointTestResp);
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);
         //SCEANRIO WHERE LEAD IS PRESENT IN SYSTEM
        LeadSPResponseWrapper wrapResponse = LeadSearchPullUtility.executeLeadSearchPullOperations(CONSTANTS.LEAD_CREATE_SF_OP,wrapRequest);
        
        String strCode = LeadSearchPullUtility.getGUID();
        system.assert(strCode != null);
        //PICKLIST TEST COVERAGE
        LeadSearchPullUtility.PicklistWrapper wrapPick = LeadSearchPullUtility.fetchPicklistValues();
        Test.stopTest();
    }
}