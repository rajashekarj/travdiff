/**
* Batch Class for Updating managers in opportunity teams if owner's managers are updated
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Isha Shukla		      US63800            12/07/2019        Added batch class 
*/ 
global class BatchUpdateManagerInOpportunityTeam implements Database.Batchable<sObject>, Schedulable,Database.Stateful {
    String strManagerRole = Label.TRAV_Manager_Team_Role;
    final string METHODNAME = 'execute';
    final string CLASSNAME = 'BatchUpdateManagerInOpportunityTeam';
    List<User> lstUsersToUpdate = new List<User>();//List of Users to update and set checkbox false
    /**
* This method collect the batches of records or objects to be passed to execute
* @param Database.BatchableContext
* @return Database.QueryLocator record
*/
    global Database.QueryLocator start(Database.BatchableContext batchCon) {
        
        //Retrieving all those Opportunity Teams whose Opportunities with TRAV_Manager_Team_Role role
        String strQuery = 'SELECT Id, UserId, User.ManagerId, User.Manager.ManagerId, OpportunityId, Opportunity.OwnerId, User.TRAV_Is_Manager_Changed__c,Opportunity.Owner.TRAV_Is_Manager_Changed__c,TRAV_Manager__c ';
        strQuery += 'FROM OpportunityTeamMember WHERE TRAV_Active__c = true ';
        strQuery += 'AND (Opportunity.Owner.TRAV_Is_Manager_Changed__c=true OR (User.TRAV_Is_Manager_Changed__c = true))  ';
        return Database.getQueryLocator(strQuery);
    }
    /**
* This method process each batch of records for the implementation logic
* @param Database.BatchableContext record
* @param List of OpportunityTeamMember 
* @return void 
*/
    global void execute(Database.BatchableContext batchCon, List<OpportunityTeamMember> records){
        
        Map<Id,Id> mapOpportunityToOwner = new Map<Id,Id>();//Map of Opportunity to it's owners
        Map<Id,Id> mapOpportunityToUser = new Map<Id,Id>();
        Map<Id,Set<OpportunityTeamMember>> mapOpportunityToManagers = new Map<Id,Set<OpportunityTeamMember>>();// Map of Opportunity to it's Owner's previous managers associated as team members
        Set<Id> setOwnerIds = new Set<Id>(); //Set of Opportunity owner ids used to pass as parameter to get the latest manager hierarchy
        Map<Id,Set<Id>> mapUserToManagers = new Map<Id,Set<Id>>();//Map of Owners to their set of latest manager hierarchy
        Map<String,OpportunityTeamWrapper> mapWrapper = new Map<String,OpportunityTeamWrapper>();//Map of OpportunityTeamWrapper to further process on team member operations
        List<OpportunityTeamMember> lstOpportunityTeamToInsert = new List<OpportunityTeamMember>();
        List<OpportunityTeamMember> lstOpportunityTeamToDelete = new List<OpportunityTeamMember>();
        Set<User> setUsersToUpdate = new Set<User>();//Set of users to avoid duplicates
        
        List<OpportunityTeamMember> lstOppTeamMembers = new List<OpportunityTeamMember>();
        Set<Id> setUserIDServicePartner = new Set<Id>();
        List<OpportunityTeamMember> oppTeamMemForChange = new List<OpportunityTeamMember>();
        List<OpportunityTeamMember> lstOppTeamMem = new List<OpportunityTeamMember>();
        Set<String> setOppIdUserId = new Set<String>();
        List<OpportunityTeamMember> finalLstTeamMember = new List<OpportunityTeamMember>();
        
        try{
            User objUser;
            for(OpportunityTeamMember objOpportunityMem : records) {
                //Populating map opportunity to owners
                if(objOpportunityMem.Opportunity.Owner.TRAV_Is_Manager_Changed__c){
                    mapOpportunityToOwner.put(objOpportunityMem.OpportunityId, objOpportunityMem.Opportunity.OwnerId);
                    if(objOpportunityMem.Opportunity.OwnerId ==objOpportunityMem.UserId){
                        oppTeamMemForChange.add(objOpportunityMem);
                    }
                }
                else if(objOpportunityMem.User.TRAV_Is_Manager_Changed__c) {
                    mapOpportunityToOwner.put(objOpportunityMem.OpportunityId,objOpportunityMem.UserId);
                    oppTeamMemForChange.add(objOpportunityMem);
                }
                
                if(objOpportunityMem.Opportunity.Owner.TRAV_Is_Manager_Changed__c == true) {
                    objUser = new User(
                        Id = objOpportunityMem.Opportunity.OwnerId,
                        TRAV_Is_Manager_Changed__c = false
                    );
                    setUsersToUpdate.add(objUser);
                }
                if(objOpportunityMem.User.TRAV_Is_Manager_Changed__c == true) {
                    objUser = new User(
                        //Id = objOpportunityMem.Opportunity.OwnerId,
                        Id = objOpportunityMem.UserId,
                        TRAV_Is_Manager_Changed__c = false
                    );
                    setUsersToUpdate.add(objUser);
                }
            }
            lstUsersToUpdate.addAll(setUsersToUpdate);
            lstOppTeamMem = OpportunityTeamMemberSelector.getExistingTeamMem(mapOpportunityToOwner.keySet());
            for(OpportunityTeamMember oppTeam : lstOppTeamMem) {
                setOppIdUserId.add(oppTeam.OpportunityId + '_' +oppTeam.UserId);
            }
            
            //Populating set to pass it as a parameter for further querying the manager hierarchy
            setOwnerIds.addAll(mapOpportunityToOwner.values());
            if(mapOpportunityToOwner.keySet().size() > 0 ){
                lstOppTeamMembers = OpportunityTeamMemberSelector.getAllExistingByOpportunityId(mapOpportunityToOwner.keySet());
            }
            if(lstOppTeamMembers != null) {
                for(OpportunityTeamMember oppTeamMem : lstOppTeamMembers) {
                    if(oppTeamMem.TeamMemberRole != 'Account Executive') {
                        setUserIDServicePartner.add(oppTeamMem.UserID);
                    } 
                }
                if(lstOppTeamMembers.size() > 0) {
                    //Getting existing team members
                    // List<OpportunityTeamMember> lstExistingTeams = OpportunityTeamMemberSelector.getOpportunityTeamMembersForBatch(mapOpportunityToOwner.keySet(),strManagerRole);
                    // Querying reporting manger having their opportunities related owner or team member updated managers
                    Map<Id,Id> userIdToDelete = new Map<Id,Id>();
                    for(OpportunityTeamMember objOpportunityTeam : lstOppTeamMembers) {
                        system.debug('objOpportunityTeam.TeamMemberRole --> '+objOpportunityTeam.TeamMemberRole);
                        system.debug('objOpportunityTeam.TRAV_Created_From_System__c --> '+objOpportunityTeam.TRAV_Created_From_System__c);
                        if(objOpportunityTeam.TeamMemberRole == strManagerRole || 
                           (objOpportunityTeam.TeamMemberRole == Constants.MANAGINGDIRECTOR && objOpportunityTeam.TRAV_Created_From_System__c == true)
                          ){
                              system.debug('objOpportunityTeam1 --> '+objOpportunityTeam);
                              for(OpportunityTeamMember oppTeam : oppTeamMemForChange) {
                                  system.debug('oppTeam.TRAV_Manager__c --> '+oppTeam.TRAV_Manager__c);
                                  if(objOpportunityTeam!=null && oppTeam.TRAV_Manager__c!=null) {
                                      //Populating map of opportunity to it's owner's previous managers associated as a reporting manager team                                                   
                                      if(!mapOpportunityToManagers.containsKey(objOpportunityTeam.OpportunityId)){
                                          String[] arr = oppTeam.TRAV_Manager__c.split(',');
                                          for(String str : arr) {
                                              if(str != '' && String.valueOf(objOpportunityTeam.UserId).equals(str)){
                                                  userIdToDelete.put(str,objOpportunityTeam.OpportunityId);
                                                  mapOpportunityToManagers.put(objOpportunityTeam.OpportunityId, new Set<OpportunityTeamMember>{objOpportunityTeam});
                                              }                               
                                              
                                          }
                                     }else{ 
                                          
                                          String[] arr = oppTeam.TRAV_Manager__c.split(',');
                                          for(String str : arr) {
                                              if(str != '' && String.valueOf(objOpportunityTeam.UserId).equals(str)){
                                                  userIdToDelete.put(str,objOpportunityTeam.OpportunityId);
                                                  mapOpportunityToManagers.get(objOpportunityTeam.OpportunityId).add(objOpportunityTeam);
                                              }                               
                                              
                                          }
                                      }
                                  }
                              }
                          }
                    }
                    
                    //Getting opportunity owner's Latest manager hierarchy
                    if(setOwnerIds.size() > 0) {
                        mapUserToManagers = UserSelector.getManagersForServicePartners(setOwnerIds);
                    }
                        OpportunityTeamWrapper objWrapper;
                        OpportunityTeamMember objOpportunityTeamToInsert;
                    //Iterating over previous managers listed as teams and populating wrapper for further operations
                    if(mapOpportunityToManagers.values().size() > 0) {
                        
                        //Populating map of wrapper
                        for(Set<OpportunityTeamMember> objSetOfOpportunityMemRecord : mapOpportunityToManagers.values()) {
                            for(OpportunityTeamMember objOpptyMem : objSetOfOpportunityMemRecord) {
                                objWrapper = new OpportunityTeamWrapper(objOpptyMem);
                                mapWrapper.put(objOpptyMem.OpportunityId+'_'+objOpptyMem.UserId,objWrapper);
                                
                            }                    
                         }
                        } //End of if checking mapOpportunityToManagers values are present or not
                        List<OpportunityTeamMember> lstTeamForUpdateManager = new List<OpportunityTeamMember>();
                        OpportunityTeamMember objTeamForUpdate;
                        for(OpportunityTeamMember objTeam : oppTeamMemForChange) {
                            // String strUserId = mapOpportunityToOwner.get(objTeam);
                            if(mapUserToManagers.containsKey(objTeam.UserId)) {
                                String strManagers = '';
                                for(Id objuserIds : mapUserToManagers.get(objTeam.UserId)) {
                                    //If latest manager hierarchy found in existing team then we don't need any action
                                    if(mapWrapper.containsKey(objTeam.OpportunityId+'_'+objuserIds)) {
                                        mapWrapper.get(objTeam.OpportunityId+'_'+objuserIds).strAction = Constants.NOACTION;                       
                                    } else {
                                        //If latest manager hierarchy not found in team then we need to create 
                                        objOpportunityTeamToInsert = new OpportunityTeamMember();
                                        objOpportunityTeamToInsert.OpportunityId = objTeam.OpportunityId;
                                        objOpportunityTeamToInsert.UserId = objuserIds;
                                        objOpportunityTeamToInsert.OpportunityAccessLevel = Constants.EDIT;
                                        objOpportunityTeamToInsert.TRAV_Active__c = true;
                                        objOpportunityTeamToInsert.TRAV_Created_From_System__c = true;
                                        //Changes for US73394 starts
                                        if(UserSelector.mapManagerToRole != null && UserSelector.mapManagerToRole.containsKey(objuserIds) ){
                                            if(UserSelector.mapManagerToRole.get(objuserIds) == Constants.NAMD || 
                                               UserSelector.mapManagerToRole.get(objuserIds) == Constants.BSIMD){
                                                   objOpportunityTeamToInsert.TeamMemberRole = Constants.MANAGINGDIRECTOR; 
                                               } else if(UserSelector.mapManagerToRole.get(objuserIds) == Constants.NAMAE || 
                                                         UserSelector.mapManagerToRole.get(objuserIds) == Constants.BSIMAE) {
                                                             objOpportunityTeamToInsert.TeamMemberRole = Constants.MANAGINGACCOUNTEXECUTIVE;
                                                         }else {
                                                             objOpportunityTeamToInsert.TeamMemberRole = strManagerRole;   
                                                         }//Changes for US73394 ends
                                        }
                                        system.debug('objOpportunityTeamToInsert 1--> '+objOpportunityTeamToInsert);
                                        objWrapper = new OpportunityTeamWrapper(objOpportunityTeamToInsert);
                                        objWrapper.strAction = Constants.STRINSERT;
                                        mapWrapper.put(objTeam.OpportunityId+'_'+objuserIds,objWrapper);
                                    }//end of else creating opportunityteammember
                                    strManagers = strManagers + objuserIds + ',';
                                }//End of for loop iterating over mapUserToManagers
                                objTeamForUpdate = new OpportunityTeamMember();
                                objTeamForUpdate.Id = objTeam.Id;
                                objTeamForUpdate.TRAV_Manager__c = strManagers; 
                                objWrapper = new OpportunityTeamWrapper(objTeamForUpdate);
                                objWrapper.strAction = Constants.STRUPDATE;
                                mapWrapper.put(objTeam.OpportunityId+'_'+objTeam.UserId,objWrapper);
                            }//End of if checking if mapUserToManagers containskey or not
                        }//End of for loop iterating over mapOpportunityToManagers
                    
                    
                }//End of if checking mapOpportunityToOwner values size
            }
            for(String objWrap : mapWrapper.keySet()) {
                if(mapWrapper.get(objWrap).strAction == Constants.STRINSERT || 
                   mapWrapper.get(objWrap).strAction == Constants.STRUPDATE) {
                       lstOpportunityTeamToInsert.add(mapWrapper.get(objWrap).objOpportunityTeamRecord);
                   }else if(mapWrapper.get(objWrap).strAction == Constants.STRDELETE) {
                       lstOpportunityTeamToDelete.add(mapWrapper.get(objWrap).objOpportunityTeamRecord);
                   }
            }
            if(!lstOpportunityTeamToDelete.isEmpty()) {
                Database.DeleteResult[] lstDeleteResult = Database.delete(lstOpportunityTeamToDelete, false);
                ExceptionUtility.logDeletionError(CLASSNAME,METHODNAME,lstDeleteResult);
            }
            for(OpportunityTeamMember oppTeam : lstOpportunityTeamToInsert) {
                if(!setOppIdUserId.contains(oppTeam.OpportunityId + '_' +oppTeam.UserId)) {
                    finalLstTeamMember.add(oppTeam);
                }
            }
            if( !finalLstTeamMember.isEmpty() ) {
                Database.UpsertResult[] lstSaveResult = Database.upsert(finalLstTeamMember, false);
                ExceptionUtility.logDMLError(CLASSNAME, METHODNAME, lstSaveResult);             
            }
            if( !lstUsersToUpdate.isEmpty() ) {
               // Database.SaveResult[] lstSaveResult = Database.update(lstUsersToUpdate, false);
               // ExceptionUtility.logDMLError(CLASSNAME, METHODNAME, lstSaveResult);             
            }
            if(Test.isRunningTest()){
                throw new QueryException();
            }
        }//end of try
        catch(exception objExp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        }//End of catch
        
        
        
    }//end of method execute
    
    /**
* This method execute any post-processing operations test
* @param Database.BatchableContext record 
* @return void 
*/
    global void finish(Database.BatchableContext batchCon){
        system.debug('lstUsersToUpdate>finish'+lstUsersToUpdate);
        Set<User> setOfUsers = new Set<User>();
        if( !lstUsersToUpdate.isEmpty() ) {
            setOfUsers.addAll(lstUsersToUpdate);
            lstUsersToUpdate.clear();
            lstUsersToUpdate.addAll(setOfUsers);
            Database.SaveResult[] lstSaveResult = Database.update(lstUsersToUpdate, false);
            ExceptionUtility.logDMLError(CLASSNAME, METHODNAME, lstSaveResult);             
         }
    } 
    /**
* This method execute this batch class when scheduled
* @param SchedulableContext record 
* @return void 
*/
    global void execute(SchedulableContext schCon) {
        BatchUpdateManagerInOpportunityTeam batchObj  = new BatchUpdateManagerInOpportunityTeam(); 
        Database.executebatch(batchObj);
    }
    
    /**
* Wrapper class for storing and manipulating the Opportunity Team Member Records
**/
    public class OpportunityTeamWrapper{
        public OpportunityTeamMember objOpportunityTeamRecord;
        public String strAction;
        public OpportunityTeamWrapper(OpportunityTeamMember opportunityTeam) {
            objOpportunityTeamRecord = opportunityTeam;
            strAction = Constants.STRDELETE;
        }
    }
    
}