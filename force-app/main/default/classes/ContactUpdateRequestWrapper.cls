/**
* This class is the utility class for Contact Search and Pull Functionality
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect   		Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty        US69347/US69863          11/10/2019      Original Version
* -----------------------------------------------------------------------------------------------------------
*/
public class ContactUpdateRequestWrapper {
    
    public class BMLoc {
        @AuraEnabled
        public String bmLocId;
    }
    @AuraEnabled
    public String agncyIndvId;
    @AuraEnabled
    public List<BMLoc> BMLoc;
    @AuraEnabled
    public List<RoleCdArr> roleCdArr;
    @AuraEnabled
    public List<RuArr> ruArr;
    @AuraEnabled
    public List<SpclCdArr> spclCdArr;
    @AuraEnabled
    public String fullNm;
    @AuraEnabled
    public String fstNm;
    @AuraEnabled
    public String lstNm;
    @AuraEnabled
    public String middleNm;
    @AuraEnabled
    public String prefixNm;
    @AuraEnabled
    public String suffixNm;
    @AuraEnabled
    public String busEmailAddr;
    @AuraEnabled
    public String busTelNbr;
    @AuraEnabled
    public String licInd;
    @AuraEnabled
    public String birthDt;
    @AuraEnabled
    public String taxIdNbr;
    @AuraEnabled
    public String persEstbDt;
    @AuraEnabled
    public String natlPrdrNbr;
    @AuraEnabled
    public String agcyIndvSts;
    @AuraEnabled
    public String srcSysCd;
    @AuraEnabled
    public String updatedBy;
    
    public class RuArr {
        @AuraEnabled
        public String ruCd;
    }
    
    public class SpclCdArr {
        @AuraEnabled
        public String spclCd;
    }
    
    public class RoleCdArr {
        @AuraEnabled
        public String roleCd;
    }
    
}