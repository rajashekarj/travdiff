/**
* This class is used as a Selector Layer for FeedItem  . 
* All queries for FeedItem will be performed in this class.
*
*
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Siddharth Menon		   US68464		   10/07/2019       Original Version 		
* -----------------------------------------------------------------------------------------------------------
*/ 
public class FeedItemSelector {
	private final static string CLASSNAME = 'FeedItemSelector';
    
        /**
* This is a method is used to get list of Chatter Feed Item records a set of Opportunities
* @param : Set of Opportunity Ids
* @return List<FeedItem> List of feeditems.
*/
    public static List<FeedItem> getOpportunityChatterFeed(Set<Id> setOpportunityIds) {
        final string METHODNAME = 'getOpportunityChatterFeed';
        List<FeedItem> lstOpportunityChatterFeed = new List<FeedItem>();
        try {
            lstOpportunityChatterFeed = new List<FeedItem>([
                SELECT id 
                FROM FeedItem 
                WHERE ParentId in :setOpportunityIds]);
			If(Test.isRunningTest())
            {
               throw new QueryException('For testing purpose');
            }
        } //End of try
        catch(QueryException exp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, exp, null);
            
        } // End Catch
        
        return lstOpportunityChatterFeed;
    } //end of method getOpportunityChatterFeed
}