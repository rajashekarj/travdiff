/**
* Test class for ContactRoleCreation Class
*
* @Author : Deloitte Consulting
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Tejal Pradhan            US68408            10/07/2019       Original Version
* -----------------------------------------------------------------------------------------------------------
*/

@isTest
private class TestContactRoleCreation {
    /**
* This method will test scenarios when a contact record is searched
* @return void
*/
    static testmethod void testCreateContactRole1() {
        User objUsr = TestDataFactory.createTestUser('BI - National Accounts','TestNA','LastNameNA');
        objUsr.TRAV_BU__c = 'BSI-FI';
        insert objUsr;
        system.runAs(objUsr) {
            Account objProspectAccount = TestDataFactory.createProspect(); 
            List<Account> lstAccount = new List<Account>{objProspectAccount};
            database.insert(lstAccount);
            
            List<Opportunity> lstOpportunities = TestDataFactory.createOpportunities(10, objProspectAccount.Id, 'BI-NA');
            lstOpportunities[0].ownerId = objUsr.ID;
            Database.insert(lstOpportunities); 
            
            //create Contact record
            List<Contact> lstContact = TestDataFactory.createTravelersContact(3);
            lstContact[0].accountId = lstAccount[0].Id; 
            lstContact[0].RecordTypeId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Customer_Contact').getRecordTypeId();
            insert lstContact;
            Map<String,String> mapPicklistValues;
            Test.startTest();
            try{
                ContactRoleCreation.createContactRole(String.valueOf(lstOpportunities[0].Id),String.valueOf(lstContact[0].Id),'',false);
            }
            catch(Exception e){}
            Test.stopTest();
        }
        //System.assertNotEquals(mapPicklistValues, null);
    }
    /**
* This method will test scenarios when a contact record is searched
* @return void
*/
    static testmethod void testCreateContactRole2() {
        User objUsr = TestDataFactory.createTestUser('BI - National Accounts','TestNA','LastNameNA');
        objUsr.TRAV_BU__c = 'BSI-FI';
        insert objUsr;
        system.runAs(objUsr) {
            Account objProspectAccount = TestDataFactory.createProspect(); 
            List<Account> lstAccount = new List<Account>{objProspectAccount};
            insert lstAccount;
            
            List<Opportunity> lstOpportunities = TestDataFactory.createOpportunities(10, objProspectAccount.Id, 'BI-NA');
            Database.insert(lstOpportunities);
            
            //create Contact record
            List<Contact> lstContact = TestDataFactory.createTravelersContact(3);
            lstContact[0].accountId = lstAccount[0].Id; 
            lstContact[0].RecordTypeId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Customer_Contact').getRecordTypeId();
            insert lstContact;
            Map<String,String> mapPicklistValues;
            Test.startTest();
            try{
                ContactRoleCreation.buValidation(lstOpportunities[0].Id);
            }
            catch(Exception e){}
            Test.stopTest();
        }
        //System.assertNotEquals(mapPicklistValues, null);
    }
    /**
* This method will test scenarios when a contact record is searched
* @return void
*/
    static testmethod void testCreateContactRole3() {
        User objUsr = TestDataFactory.createTestUser('BI - National Accounts','TestNA','LastNameNA');
        objUsr.TRAV_BU__c = 'BI-NA';
        insert objUsr;
        system.runAs(objUsr) {
            Account objProspectAccount = TestDataFactory.createProspect(); 
            List<Account> lstAccount = new List<Account>{objProspectAccount};
            database.insert(lstAccount);
            
            List<Opportunity> lstOpportunities = TestDataFactory.createOpportunities(10, objProspectAccount.Id, 'BI-NA');
            lstOpportunities[0].ownerId = objUsr.ID;
            Database.insert(lstOpportunities); 
            
            //create Contact record
            List<Contact> lstContact = TestDataFactory.createTravelersContact(3);
            lstContact[0].accountId = lstAccount[0].Id; 
            lstContact[0].RecordTypeId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Customer_Contact').getRecordTypeId();
            insert lstContact;
            Map<String,String> mapPicklistValues;
            Test.startTest();
            try{
                ContactRoleCreation.createContactRole(String.valueOf(lstOpportunities[0].Id),String.valueOf(lstContact[0].Id),'',false);
                ContactRoleCreation.buValidation(lstOpportunities[0].Id);
            }
            catch(Exception e){}
            Test.stopTest();
        }
        //System.assertNotEquals(mapPicklistValues, null);
    }
    /**
* This method will test scenarios when a contact record is searched
* @return void
*/
    static testmethod void testCreateContactRole4() {
        User objUsr = TestDataFactory.createTestUser('BI - National Accounts','TestNA','LastNameNA');
        objUsr.TRAV_BU__c = 'BSI-FI';
        insert objUsr;
        system.runAs(objUsr) {
            Account objProspectAccount = TestDataFactory.createProspect(); 
            List<Account> lstAccount = new List<Account>{objProspectAccount};
            insert lstAccount;
            
            List<Opportunity> lstOpportunities = TestDataFactory.createOpportunities(10, objProspectAccount.Id, 'BI-NA');
            database.insert(lstOpportunities);
            
            //create Contact record
            List<Contact> lstContact = TestDataFactory.createTravelersContact(3);
            lstContact[0].accountId = lstAccount[0].Id; 
            lstContact[0].RecordTypeId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Customer_Contact').getRecordTypeId();
            insert lstContact;
            Map<String,String> mapPicklistValues;
            Test.startTest();
            try{
                mapPicklistValues = ContactRoleCreation.fetchOpportunityContactRoles();
            }
            catch(Exception e){}
            Test.stopTest();
            System.assertNotEquals(mapPicklistValues, null);
        }
        
    }
    /**
* This method will test scenarios when a contact record is searched
* @return void
*/
    static testmethod void testCreateContactRol5() {
        User objUsr = TestDataFactory.createTestUser('BSI - Financial Institutions','TestNA','LastNameNA');
        objUsr.TRAV_BU__c = 'BSI-PNP';
        insert objUsr;
        system.runAs(objUsr) {
            Account objProspectAccount = TestDataFactory.createProspect(); 
            List<Account> lstAccount = new List<Account>{objProspectAccount};
            insert lstAccount;
            
            List<Opportunity> lstOpportunities = TestDataFactory.createOpportunities(10, objProspectAccount.Id, 'BSI-FI');
            database.insert(lstOpportunities);
            
            //create Contact record
            List<Contact> lstContact = TestDataFactory.createTravelersContact(3);
            lstContact[0].accountId = lstAccount[0].Id; 
            lstContact[0].RecordTypeId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Customer_Contact').getRecordTypeId();
            insert lstContact;
            Map<String,String> mapPicklistValues;
            Test.startTest();
            try{
                mapPicklistValues = ContactRoleCreation.fetchOpportunityContactRoles();
            }
            catch(Exception e){}
            Test.stopTest();
            System.assertNotEquals(mapPicklistValues, null);
        }
        
    }
}