/**
* Utility Class for Lead Trigger Handler. This class will have logic.
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Shashank Agarwal       US59527              05/23/2019       Adding standardLeadStatusSync
* 
* Bhanu Reddy			 US59527              05/23/2019       Added converLeads Method
*
*Shruti Gupta			 US59528			  06/13/2019	   Added chatter post on Account
*
*Shruti Gupta			 US62738			  05/07/2019		Added logic for populating BU 
*																Account name on Converted Opportunity 
*																from Company name on Converted Lead
*Shruti Gupta			 US62950			  07/19/2019		Added method setLastName
*Siddharth M			 US73196			  10/02/2020		Added method setAccountFieldsAfterConvert
*/
public without sharing class LeadService {
    
    private static final String CLASSNAME = 'LeadService';
    
    /**
* This is a method included to sync the STANDARD LEAD STATUS and CUSTOM LEAD STATUS on field
* insert and field update.
* @param newListItems - List of new values in the for thr Lead records.
* @param newMapItems - Map of new values in the for thr Lead records.
* @param oldMapItems - Map of old values in the for thr Lead records
* @return void - No return.
*/ 
    public static void standardLeadStatusSync(List<Lead> newList, Map<Id, Lead> oldMap){
        
        //Operation on Before Insert
        if(newList != null && oldMap == null){
            for(Lead newLead : newList){
                newLead.Trav_Lead_Status_For_Path__c = newLead.Status;
            }//end of for loop
        }//end of before insert if statement
        
        //Operation on Before Update
        if(newList != null  && oldMap != null){
            for(Lead newLead : newList){
                
                //check if there is a change in custom lead status and assign it back 
                //to standard lead status
                if(newLead.Trav_Lead_Status_For_Path__c 
                   != oldMap.get(newLead.Id).Trav_Lead_Status_For_Path__c
                   && 
                   newLead.status == oldMap.get(newLead.Id).status) {
                       newLead.Status = newLead.Trav_Lead_Status_For_Path__c;
                   }//end of if condition
                
                //check if there is a change in standard Lead status and assign it back to 
                //custom Lead status Field
                if(newLead.Trav_Lead_Status_For_Path__c == 
                   oldMap.get(newLead.Id).Trav_Lead_Status_For_Path__c
                   && 
                   newLead.status != oldMap.get(newLead.Id).status){
                       newLead.Trav_Lead_Status_For_Path__c = newLead.status;
                   }//end of if condition
            }//end of for loop
        }//end of Before Update if condition
    }//end of standardLeadStatusSync method
    
 /**
* This is method is used to convert leads whose status are qualified and delete the 
* contacts created as part of converted lead
* *@param lstLeads The list leads that need to be converted
* @return null
*/ 
    public static void convertLeads( Map<Id, Lead> newMap, Map<Id, Lead> oldMap) {
        
        //variable declarations
        list<Contact> lstContactToDelete = new List<Contact>();
        Contact objContact ;
        list<Database.LeadConvert> leadConverts = new list<Database.LeadConvert>();
        List<Lead> lstConvertedLead = new List<Lead>();
        List<Opportunity> lstConvertedOpp = new List<Opportunity>();
        Set<Id> setOfId = new Set<Id>();
        Map<Id, string> mapConvertedOppId = new Map<Id, String>();
        set <string> setCertifiedIds = new set<string>();
        map<string,id> mapCertifiedBusinesssId = new map<string,id>();
        //check if there is change in status of the standard or custom Field
        list<Lead> lstLeads = new list<Lead>();
        final string METHODNAME = 'convertLeads';
        //Iterate over the inserted leads to check the lead conversion
        //List for Bulk Chatter Feeds
        List<ConnectApi.BatchInput> listBatchInputs = new List<ConnectApi.BatchInput>();
        //Chatter Feed API for chatter post
        ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
        //List of converted opportunities for update
        List<Opportunity> lstOpportunity = new List<Opportunity>();
        
        try {
            for(Lead newLead : newMap.values()) {
                if((newLead.Status != oldMap.get(newLead.id).status
                    && newLead.status == SYSTEM.LABEL.TRAV_Qualified)
                   ||
                   (newLead.Trav_Lead_Status_For_Path__c
                    != oldMap.get(newLead.id).Trav_Lead_Status_For_Path__c
                    && newLead.Trav_Lead_Status_For_Path__c == SYSTEM.LABEL.TRAV_Qualified)
                  ) {
                      lstLeads.add(newLead);
                      //check if certifed Business id is blank and add to teh set
                      if(string.isnotBlank(newLead.TRAV_Certified_Business_Id__c)) {
                          setCertifiedIds.add(newLead.TRAV_Certified_Business_Id__c);
                      } //end of if block for certifiefd id additiona
                  } //end of if block for status change
            }  //end of for blaock for iteration over inserted Leads
            
            //check account with same certified business id as Lead
            if(!setCertifiedIds.isEmpty()) {
                for(Account objAccnt :AccountSelector.getCertifiedBusinessAccnts(setCertifiedIds)) {
                    mapCertifiedBusinesssId.put(objAccnt.TRAV_Certified_Business_Id__c,objAccnt.id);
                } //end For accounts with certfied business ids
            } //end of if to check certified business ids are Empty
            
            //Iterate over qualfiied leads to add them for lead conversions
            for(Lead objLead : lstLeads) { 
                Constants.LEADCONVERTED = True;
                Database.LeadConvert objLeadConvert = new database.LeadConvert();
                objLeadConvert.setLeadId(objLead.Id);
                objLeadConvert.convertedStatus = SYSTEM.LABEL.TRAV_Qualified;
                objLeadConvert.setOwnerId(UserInfo.getUserId());
                //check if account exists with same certified busniess id exists
                if(mapCertifiedBusinesssId.containsKey(objLead.TRAV_Certified_Business_Id__c)){
                    objLeadConvert.setAccountId(mapCertifiedBusinesssId.get(objLead.TRAV_Certified_Business_Id__c));
                    
                } //end if set acccount if
                
                leadConverts.add(objLeadConvert);
            }  //end lead for loop for lstLeads to convert
            
            //convert the lead if the size the lead conversion is less than 
            // 100 to avoid the salesforce governor limit
            if(!leadConverts.isEmpty()){
                for(Integer i = 0; i <= leadConverts.size()/100 ; i++){
                    list<Database.LeadConvert> tempList = new list<Database.LeadConvert>();
                    Integer startIndex = i*100;
                    Integer endIndex = ((startIndex+100) < leadConverts.size()) ? startIndex+100: leadConverts.size();
                    for(Integer j=startIndex;j<endIndex;j++){
                        tempList.add(leadConverts[j]);
                    }
                    
                    if(!tempList.isEmpty()) {
                        Database.LeadConvertResult[] lcrList = Database.convertLead(tempList, false);
                        ExceptionUtility.logDMLError(CLASSNAME, METHODNAME, lcrList);
                        
                        //Iterate over Lead conversion list to ge the contact ids that need to be deleted
                        for(Database.LeadConvertResult lcr : lcrList) {
                            String strErrorMessage=ExceptionUtility.getErrorString(lcr.getErrors());
                            system.debug(' errors occured'+strErrorMessage);
                            setOfId.add(lcr.getLeadID());
                            
                            //contact deletion code
                            if( lcr.getContactId() != null){
                                objContact = new Contact();
                                objContact.id = lcr.getContactId();
                                lstContactToDelete.add(objContact);
                            }
                        }  //end for- LeadConversion result
                    }
                    
                }  //end  for check for lead size is 100
                //check if contact is Empty and delete contact
                if(!lstContactToDelete.isEmpty()){
                    delete lstContactToDelete;
                } //end if contact deletion
            } //end if Leadconversion list is not Empty
            
        } //end of try
        catch(exception objExp){
            ExceptionUtility.logApexException(CLASSNAME,METHODNAME,objExp,null);
        } //end of catch
        
    } //end method converLeads
    
    /**
* This is a method is used in lightning component to redirect user to account after redirection
*@param strLeadID -Id of the converted lead
* @return  string -Account id of the converted Lead
*/ 
    @AuraEnabled
    public static string accountRedirect(string strLeadId){
        string strAccountId ;
        //Start US59528
        string strAccountName;
        string strOwnerId;
        final string METHODNAME = 'accountRedirect';
        //Timestamp for Account Chatter Message on Lead Conversion
        string strDateFormat = 'MMM dd,YYYY@ HH:mm a';
        string strTimeStamp = Datetime.now().format(strDateFormat);
        //List to store empty Chatter feeds on Account
        List<AccountFeed> lstBodyNull = new List<AccountFeed>();
        //Set of converted Accounts ID
        Set<Id> setAccountID = new Set<Id>();
          
        //Chatter Feed API for chatter post
        ConnectApi.FeedItemInput objInput = new ConnectApi.FeedItemInput();
        for(Lead objLead : LeadSelector.getLeads(strLeadId)){
            strAccountId = objLead.convertedAccountId;
            setAccountID.add(strAccountId);
            strOwnerId = UserInfo.getUserId();
            //Getting converted Account Name
            strAccountName = objLead.convertedAccount.Name;
            if(strTimeStamp!=NULL && strAccountName!=NULL && strOwnerId!=NULL) {
                //Creating chatter message for chatter feed
                String strTextMessage = Label.TRAV_Lead_Chatter_Msg + '\r\n' + '\r\n' + 
                    					Label.TRAV_Lead_Chatter_Msg1 + 
                    					' '+strAccountName + Label.TRAV_Lead_Chatter_Msg2 + ' '+
                    					'{'+strOwnerId+'}'+ Label.TRAV_Lead_Chatter_Msg3;
                //Using ChatterUtility for generating Chatter Feed from input message
            	objInput = ChatterUtility.generateFeedInputWithMentions(null,strAccountId,strTextMessage);
            }
        }
        //AccountFeed object for deleting automatic chatter post 
        //on Lead conversion
        List<AccountFeed> lstAccFeed = AccountFeedSelector.getAccountFeed(setAccountID);
        //Loop to check for null body check of Account Feed
        for(AccountFeed objAccountFeed : lstAccFeed) {
            if(objAccountFeed.Body == NULL) {
                lstBodyNull.add(objAccountFeed);
            }
        }//end of AccountFeed List
        if(!lstBodyNull.isEmpty()){
            List<Database.DeleteResult> lstdeleteResult;
            try {
                lstdeleteResult = Database.Delete(lstBodyNull,false);
                ExceptionUtility.logDeletionError(CLASSNAME, METHODNAME, lstdeleteResult);
            }catch(Exception objExp) {
                ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
            } // End Catch
        } //End if
        if(objInput!=NULL) {
            try {
                //posting Chatter feed on Account with input message
                ConnectApi.ChatterFeeds.postFeedElement(NULL , objInput); 
            }catch(Exception objExp) {
                ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
            }
        }
        //End US59528
        return strAccountId;
    }  //end accountRedirect
/**
* This is method is used to Set the expiration date of the lead to  1 year ahead of the effective date 
* *@param lstLeads The list leads that need to be converted
* @return null
*/
    public static void setExpirationDate(List<Lead> lstnewLeads,Map<id,Lead> mapOldLeads){
        
        //iterate over the inserted or updated Leads and set the Expiration date
        for(Lead objLead : lstnewLeads){
            if((mapOldLeads==null && objLead.TRAV_Effective_Date__c!=null)|| 
                (objLead.TRAV_Effective_Date__c!=null && 
                                 objLead.TRAV_Effective_Date__c != mapOldLeads.get(objLead.id).TRAV_Effective_Date__c)){
                objLead.TRAV_Expiration_Date__c = objLead.TRAV_Effective_Date__c.addYears(1);
            } //end id
        } //end for iteration over the leads
    } //end of the method setExpirationDate
/**
* This is method is used to Set the last name of the lead as company name
* *@param lstLeads The list leads that are created or updated
* @return null
*/
    public static void setLastName(List<Lead> lstnewLeads,Map<id,Lead> mapOldLeads){
        //Get profile of logged in user
        String profileName = LeadSelector.getProfileName();
        //iterate over the inserted or updated Leads and set the Last Name
        for(Lead objLead : lstnewLeads){
            //null check for profile name
            if(profileName != NULL) {
                if(((mapOldLeads==null && objLead.LastName!=null)|| 
                    (objLead.LastName!=null && objLead.LastName != mapOldLeads.get(objLead.id).LastName))
                   ){
                    objLead.LastName = objLead.TRAV_CompanyName__c;
                } //end id
            }//end if
        } //end for iteration over the leads
    } //end of the method setLastName
    
    /*
* This is method is used to populate converted Account Fields that cannot be mapped through UI
* @param lstNewLead The list leads that are being updated
* @return null
*/
   public static void setAccountFieldsAfterConvert(List<Lead> lstNewLead)
   {
       List<SObject> listConvertedAccounts= new List<SObject>();
       for(Lead objLead: lstNewLead)
           {
               Account objAccount= new Account();
               Opportunity objOpp= new Opportunity();
               if(objLead.convertedAccountId!=null){
               objAccount.Id=objLead.ConvertedAccountId;
               if(objLead.TRAV_CB_No_of_Employees__c!=null){
               objAccount.NumberOfEmployees= Integer.valueOf(objLead.TRAV_CB_No_of_Employees__c);
                 }
               objAccount.TickerSymbol=objLead.TRAV_Stock_Ticker_Symbol__c;
               objAccount.TRAV_SNL_No_of_Employees__c=objLead.NumberOfEmployees;
               objAccount.AnnualRevenue=objLead.AnnualRevenue;
                   
				objOpp.Id= objLead.ConvertedOpportunityId;
                objOpp.TRAV_Headquarters_Address__c=objLead.Street+', '+objLead.City+', '+objLead.State+' '+objLead.PostalCode+', '+objLead.Country;
              listConvertedAccounts.add(objOpp);     
              listConvertedAccounts.add(objAccount);
                   }
           }
       if(!listConvertedAccounts.isEmpty())
       {
           update listConvertedAccounts;
       }
   }
} //end class