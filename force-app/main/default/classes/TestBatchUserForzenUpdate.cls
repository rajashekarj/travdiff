@isTest
public class TestBatchUserForzenUpdate {
    @IsTest
    static void TestExcuteBatch(){
        
        /*
        * 1 - Create new user and update to frozen
        * 2 - Set three users as frozen and update user data
        * 3 - Run the batch 
        */
        User objUsrOwner = TestDataFactory.createTestUser('BI - National Accounts','TestNA2','LastNameNA2');
        insert objUsrOwner;
        UserLogin user_login = [select id,IsFrozen from userlogin where userid =: objusrowner.Id];
        user_login.IsFrozen = true;
        update user_login;
              
        Test.startTest();
              
       	BatchUserFrozenUpdate bc = new BatchUserFrozenUpdate();
       	ID jobID = Database.executeBatch(bc,5000);
        Test.stopTest();
        
    } {

    }
}