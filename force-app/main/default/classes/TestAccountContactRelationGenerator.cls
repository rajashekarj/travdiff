@isTest
public class TestAccountContactRelationGenerator {
    
    @isTest
    public static void testgenerateACR()
    {
        Account objProspect= TestDataFactory.createProspect();
        insert objProspect;
        Account agency= TestDataFactory.createAgency();
        insert agency;
        system.debug(objProspect);
        
        Contact newContact= TestDataFactory.createTravelersContact(1)[0];
        newContact.AccountId=objProspect.Id;
        insert newContact;
        system.debug(newContact);
         
        AccountContactRelationWrapper objACRWrapper1= new AccountContactRelationWrapper();
        objACRWrapper1.strUniqueId='A01';
        objACRWrapper1.accountId=agency.Id;
        objACRWrapper1.contactId=newContact.Id;
        objACRWrapper1.isInsert= true;
        objACRWrapper1.isDelete=false;
        
        List<AccountContactRelationWrapper> listInsertACR= new List<AccountContactRelationWrapper>{objACRWrapper1};
            AccountContactRelationGenerator.generateACR(listInsertACR);
        system.debug('generated');
        Id objACRId= [SELECT id FROM AccountContactRelation WHERE AccountId=:agency.Id AND ContactId=:newContact.Id].Id;
        system.debug(objACRId);
        
        AccountContactRelationWrapper objACRWrapper2= new AccountContactRelationWrapper();
        objACRWrapper1.strUniqueId='A02';
        objACRWrapper2.acrId= objACRId;
        objACRWrapper2.accountId=agency.Id;
        objACRWrapper2.contactId=newContact.Id;
        objACRWrapper2.isDelete= true;
        objACRWrapper2.isInsert=false;
        List<AccountContactRelationWrapper> listDeleteACR= new List<AccountContactRelationWrapper>{objACRWrapper2};
            AccountContactRelationGenerator.generateACR(listDeleteACR);
        
        List<AccountContactRelation> listCheckACR=[SELECT id FROM AccountContactRelation WHERE AccountId=:agency.Id AND
                                                   ContactId=:newContact.Id];
        system.assertEquals(listCheckACR.size(),0);
    }
    
    @isTest
    public static void testSelectACR()
    {
        User objUser= TestDataFactory.createTestUser('BI - National Accounts', 'test', 'test1');
        insert objUser;
        Account objProspect= TestDataFactory.createProspect();
        Account agency= TestDataFactory.createAgency();
        Contact newContact= TestDataFactory.createTravelersContact(1)[0];
        System.runAs(objUser){
            insert objProspect;
            insert agency;
            
            newContact.AccountId=objProspect.Id;
            newContact.TRAV_User__c=objUser.Id;
            insert newContact;
            
        }
        Set<Id> accId=new Set<Id>{objProspect.Id,agency.Id};
            Set<Id> ownId= new Set<Id>{objUser.Id};
                Map<String,AccountContactRelation> mapACR= AccountContactRelationSelector.getACRWithAccountandOwner(accId, ownId);
        system.debug(mapACR);
        system.assertEquals(mapACR.size(),1);
    }
}