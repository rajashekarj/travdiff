/**
* Batch Class for Populating the Analytical Snapshot Object with its attendees.
* Modification Log    :
* ------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* Siddharth Menon		 US					  03/04/2020       Original Version  		
* ------------------------------------------------------------------------------------------------------
*/ 


global class BatchAnalyticalSnapshotPopulate implements Database.Batchable<sObject>, Schedulable{
    String strQuery;
    String CLASSNAME ='BatchAnalyticalSnapshotPopulate';
    Renewal_Batch__c objRenewalBatch = Renewal_Batch__c.getOrgDefaults();
    DateTime dtFrom = objRenewalBatch.TRAV_Last_Successful_Attendee_Batch_Run__c;
    DateTime dtTo = System.now();
    
    /**
* This method collect the batches of records or objects to be passed to execute
* @param Database.BatchableContext
* @return Database.QueryLocator record
*/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String METHODNAME = 'Start';
        try{
            
            strQuery = 'Select Id,Subject, Type, EndDateTime, OwnerId,TRAV_Related_Agency_Contact__c, ActivityDate, WhatId, WhoId, Description, StartDateTime, Location,(Select Id, RelationId FROM EventRelations) FROM Event where LastModifiedDate > :dtFrom and isChild=false';
            
            return Database.getQueryLocator(strQuery);
        }catch(Exception objException){
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objException, null);
            return null;
        }
    } //End  Method
    
    /**
* This method process each batch of records for the implementation logic
* @param Database.BatchableContext record
* @param List of Opportunity 
* @return void 
*/ 
    global void execute(Database.BatchableContext BC, List<Event> scope) {
        String METHODNAME = 'execute';
        
        try{
            if(!scope.isEmpty()){
                system.debug(scope);
                List<TRAV_Analytical_Snapshot__c> lstAnalyticalSnapshots= new List<TRAV_Analytical_Snapshot__c>();
                for(Event objEvent: scope)
                {
                    TRAV_Analytical_Snapshot__c objOwnerRecord= new TRAV_Analytical_Snapshot__c();
                    objOwnerRecord.TRAV_Subject__c= objEvent.Subject;
                    objOwnerRecord.TRAV_Type__c = objEvent.Type;
                    objOwnerRecord.TRAV_Start__c= objEvent.StartDateTime;
                    objOwnerRecord.TRAV_End__c= objEvent.EndDateTime;
                    objOwnerRecord.TRAV_Location__c= objEvent.Location;
                    objOwnerRecord.TRAV_Date__c= objEvent.ActivityDate;
                    objOwnerRecord.TRAV_IsOwner__c=true;
                    objOwnerRecord.TRAV_Related_Agency_Contact__c=objEvent.TRAV_Related_Agency_Contact__c;
                    objOwnerRecord.OwnerId=objEvent.OwnerId;
                    objOwnerRecord.TRAV_SObjectName__c='Event';
                    objOwnerRecord.TRAV_Full_Comments__c=objEvent.Description;
                    objOwnerRecord.TRAV_External_Id__c=objEvent.Id;
                    if(objEvent.WhoId!=null && objEvent.WhoId.getSObjectType().getDescribe().getName()=='Contact'){
                        objOwnerRecord.TRAV_Contact_Name__c=objEvent.WhoId;
                    }
                    if(objEvent.WhatId!=null && objEvent.WhatId.getSObjectType().getDescribe().getName()=='Account'){
                        objOwnerRecord.TRAV_Account_Name__c=objEvent.WhatId;
                    }
                    lstAnalyticalSnapshots.add(objOwnerRecord);
                    system.debug('lstAnalyticalSnapshots'+lstAnalyticalSnapshots+' objOwnerRecord '+objOwnerRecord);
                    for(EventRelation objAttendee: objEvent.EventRelations){
                        if(objAttendee.RelationId!= null && objAttendee.RelationId!=objEvent.OwnerId && objAttendee.RelationId.getSObjectType().getDescribe().getName()=='User'){
                            TRAV_Analytical_Snapshot__c objOwnerRecord1= new TRAV_Analytical_Snapshot__c();
                            objOwnerRecord1.TRAV_Subject__c= objEvent.Subject;
                            objOwnerRecord1.TRAV_Type__c = objEvent.Type;
                            objOwnerRecord1.TRAV_Start__c= objEvent.StartDateTime;
                            objOwnerRecord1.TRAV_End__c= objEvent.EndDateTime;
                            objOwnerRecord1.TRAV_Location__c= objEvent.Location;
                            objOwnerRecord1.TRAV_Date__c= objEvent.ActivityDate;
                            objOwnerRecord1.TRAV_IsOwner__c=false;
                            objOwnerRecord1.TRAV_Related_Agency_Contact__c=objEvent.TRAV_Related_Agency_Contact__c;
                            objOwnerRecord1.OwnerId=objAttendee.RelationId;
                            objOwnerRecord1.TRAV_SObjectName__c='Event';
                            objOwnerRecord1.TRAV_Full_Comments__c=objEvent.Description;
                            objOwnerRecord1.TRAV_External_Id__c=objAttendee.Id;
                            if(objEvent.WhoId!=null && objEvent.WhoId.getSObjectType().getDescribe().getName()=='Contact'){
                                objOwnerRecord1.TRAV_Contact_Name__c=objEvent.WhoId;
                            }
                            if(objEvent.WhatId!=null && objEvent.WhatId.getSObjectType().getDescribe().getName()=='Account'){
                                objOwnerRecord1.TRAV_Account_Name__c=objEvent.WhatId;
                            }
                            lstAnalyticalSnapshots.add(objOwnerRecord1);
                        }
                    }
                    
                    upsert lstAnalyticalSnapshots;
                    
                    
                    
                }
                
                //ExceptionUtility.logDMLError(Constants.OPPORTUNITYSERVICE, METHODNAME, lstSaveResultForDelete);
                if(Test.isRunningTest()) {
                    CalloutException e = new CalloutException();
                    e.setMessage('This is a constructed exception for testing and code coverage');
                    throw e;
                }
            }
        }catch(Exception objException){
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objException, null);
        }
    } // End  Method
    
    /**
* This method execute any post-processing operations test
* @param Database.BatchableContext record 
* @return void 
*/ 
    global void finish(Database.BatchableContext BC) {
        if(Renewal_Batch__c.getOrgDefaults().id != null){
            Renewal_Batch__c objRenewalBatch = Renewal_Batch__c.getOrgDefaults();
            objRenewalBatch.TRAV_Last_Successful_Attendee_Batch_Run__c = System.now();
            update objRenewalBatch;
        }
    }
    
    /**
* This method execute this batch class when scheduled
* @param SchedulableContext record 
* @return void 
*/
    global void execute(SchedulableContext schCon) {
        BatchAnalyticalSnapshotPopulate batchObj  = new BatchAnalyticalSnapshotPopulate(); 
        Database.executebatch(batchObj);
    }//End Method  
}