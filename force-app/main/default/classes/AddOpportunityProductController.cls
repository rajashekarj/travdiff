/**
 * This class is used as a The controller for the NewOpportunityLineItem component
 *
 * @Author Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shashank S             US59528             07/27/2019       Original Version
 * Siddharth M            US66568             09/04/2019       Added function selectedProducts 
 *                                                              Modified saveOpportunityLineItems
 * Isha Shukla			  US69846			  10/18/2019		Added method existingProducts
 * -----------------------------------------------------------------------------------------------------------
 */
public with sharing class AddOpportunityProductController{
   /**
   * This is a method which is used to Save the opportunity line items
   * @param List<sObject>
   * @return void
   */
   @AuraEnabled
    public static String saveOpportunityLineItems(List<OpportunityLineItem> sObjectList, Boolean isBSIView){
        String errorMessage = '';
        try {
        List<Database.SaveResult> lstSaveResult;
        List<sObject> bsiInsertList= new List<sObject>();
        if(isBSIView){
            Id unknownCarrier=[select id from TRAV_Carrier__c where name='UNKNOWN' limit 1].id;
            for(OpportunityLineItem bsiObject:sObjectList){
                bsiObject.TRAV_Incumbent_Insurance_Carrier_Name__c=unknownCarrier;
                bsiInsertList.add(bsiObject);
            }
            lstSaveResult = database.insert(bsiInsertList,false);
            errorMessage = ExceptionUtility.logDMLError('AddOpportunityProductController', 'saveOpportunityLineItems', lstSaveResult);
        }
        else{
            lstSaveResult = database.insert(sObjectList,false);
            errorMessage = ExceptionUtility.logDMLError('AddOpportunityProductController', 'saveOpportunityLineItems', lstSaveResult);
            }
        } catch (DmlException de) {
            // DmlException thrown if a validation rule configured via Setup occurs
            // Get context user - UserInfo built-in doesn't include profile name, only ID
            String currentUserProfileName = getCurrentUserProfile();
            if (de.getDmlMessage(0) != null) {
                throw new AuraHandledException(de.getDmlMessage(0)); // Throw any validation rule error message via DmlException.getDmlMessage()
            }
        } finally {
            return errorMessage;
        }      
        return errorMessage;
    }
     /**
    * This is a method which is used to get Picklist Options for Programtype adn Pricing type
    * @param no parameters used
    * @return Map<String, List<String>> List of options and key is the field API NAme
    */
    @AuraEnabled
    public static Map<String, List<String>> getPicklistOptions(){
    //Var to be returned to the server. This will hold the picklist options
        Map<String, List<String>> picklistOptions = new Map<String, List<String>>();
        List<String> listOptions = new List<String>();
        listOptions.add('');
        //Describe the OpportunityLineitem to get the field details and extract the options
        Schema.DescribeFieldResult fieldResult = OpportunityLineItem.TRAV_Pricing_Type__c.getDescribe();
                              List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                              for( Schema.PicklistEntry pickListVal : ple){
                                             listOptions.add(pickListVal.getLabel());
                              }
        picklistOptions.put('TRAV_Pricing_Type__c', listOptions);
        listOptions = new List<String>();
        listOptions.add('');
        //Describe the OpportunityLineitem to get the field details and extract the options
        fieldResult = OpportunityLineItem.TRAV_Program_Type__c.getDescribe();
                              ple = fieldResult.getPicklistValues();
                              for( Schema.PicklistEntry pickListVal : ple){
                                             listOptions.add(pickListVal.getLabel());
                              }
        picklistOptions.put('TRAV_Program_Type__c', listOptions);
        return picklistOptions;
    }
    /**
* This is a method which is used to return list of already selected products
* @param recordId of current Opportunity record
* @return Set<string> selectedProductsSet which contains ids of all previously added products.
*/
     @AuraEnabled
    public static List<Id> selectedProducts(Id recordId){
        Set<Id> selectedProductsSet= new Set<Id>();
            for(OpportunityLineItem objprod:[select id,Product2Id from OpportunityLineItem where OpportunityId=:recordId])
            {
                selectedProductsSet.add(objprod.Product2Id);
            }
        return new List<Id>(selectedProductsSet);
    }
    
    /**
    * US69846 - This is a method which is used to return list of already existing products
    * @param recordId of current Opportunity record
    * @return List<OpportunityLineItem> existing products which related to opportunity
    */
    @AuraEnabled
    public static List<OpportunityLineItem> existingProducts(Id recordId){
        List<OpportunityLineItem> lstExistingProducts = new List<OpportunityLineItem>();
        lstExistingProducts = OpportunityLineItemSelector.getOpportunityLineItems(new Set<Id>{recordId});
        system.debug('lstExistingProducts>'+lstExistingProducts);
        return lstExistingProducts;        
    }
    /**
     * US74862 - Retrieve the context user's profile. If the user's profile is 'BI - National Accounts,' the component will ensure that the Program Type
     * field is required.
     * @returns {String} The context user's profile name
     */
    @AuraEnabled
    public static String getCurrentUserProfile() {
        return [SELECT Profile.Name FROM User WHERE Id = :UserInfo.getUserId()].Profile.Name;
    }
}