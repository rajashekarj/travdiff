/**
 * The Lookup controller returns a List<LookupSearchResult> when sending search result back to Lightning
 *
 * @Author Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shashank S             US59528             07/27/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
public class LookupSearchResult {
    @AuraEnabled
    public Id id;
    @AuraEnabled
    public String sObjectType;
    @AuraEnabled
    public String icon;
    @AuraEnabled
    public String title;
    @AuraEnabled
    public String subtitle;
    @AuraEnabled
    public String approved;
    
    public LookupSearchResult(Id id, String sObjectType, String icon, String title, String subtitle,String approved) {
        this.id = id;
        this.sObjectType = sObjectType;
        this.icon = icon;
        this.title = title;
        this.subtitle = subtitle;
        this.approved = approved;
    }
}