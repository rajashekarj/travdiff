/**
 * Handler Class for Stewardship Trigger. This class won't have any logic and
 * will call other Stewardship Service/Utilities class.
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shashank Agarwal       US73728              01/27/2020       Added code in beforeUpdate
 * -----------------------------------------------------------------------------------------------------------
 */
public class StewardshipTriggerHandler implements ITriggerHandler{
    //Handler Method for Before Insert.
    public void beforeInsert(List<sObject> newItems) {
       
    }
    
    //Handler Method for Before Update.
    public void beforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
        List<TRAV_Stewardship__c> newList = newItems.values();
        //US73728
        StewardshipService.updateOwnerToStewardshipLead(newList, (Map<Id, TRAV_Stewardship__c>)oldItems);
    }
    //Handler Method for Before Delete.    
    public void beforeDelete(Map<Id, sObject> oldItems) {
        
    }
    
    //Handler method for After Insert.
    public void afterInsert(Map<Id, sObject> newItems) {

    }

    //Handler method for After Update.    
    public void afterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {  
        
    }
    //Handler method for After Delete    
    public void afterDelete(Map<Id, sObject> oldItems) {
    
    }
    
    //Handler method for After Undelete.
    public void afterUndelete(Map<Id, sObject> oldItems) {
    
    } 

}