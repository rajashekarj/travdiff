/**
* Queueable Class for Opportunity. This class will have logic for cloning of opportunity record if Record 
* Type is Renewal on opportunity 
* Modification Log    :
* ------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* ------------------------------------------------------------------------------------------------------
* Bharat Madaan            US65898           08/30/2019       Original Version
*/ 
public class QueueableAddTravelersContact implements Queueable {
 String CLASSNAME ='QueueableAddTravelersContact';
    List<Opportunity> lstNewOpportunity= new List<Opportunity>();
    List<Opportunity> lstExistingOpportunity = new List<Opportunity>();
    Map<String,AccountContactRelation> mapStringToACR = new Map<String,AccountContactRelation>();
    Map<String,AccountContactRelation> mapOldStringToACR = new Map<String,AccountContactRelation>();
    Map<Id,Id> mapUserIdToContactId= new Map<Id,Id>();
    Map<Id,Id> mapOldUserIdToContactId= new Map<Id,Id>();
    Boolean objTriggerIsExecuting;
    //constructor setting parametre values
    public QueueableAddTravelersContact(List<Opportunity> lstNewOpportunity,List<Opportunity> lstExistingOpportunity,Map<String,AccountContactRelation> mapStringToACR,
                                        Map<String,AccountContactRelation> mapOldStringToACR,Map<Id,Id> mapUserIdToContactId,Map<Id,Id> mapOldUserIdToContactId){
     
        this.lstNewOpportunity= lstNewOpportunity;
        this.lstExistingOpportunity = lstExistingOpportunity;
        this.mapStringToACR= mapStringToACR;
        this.mapOldStringToACR= mapOldStringToACR;
        this.mapUserIdToContactId = mapUserIdToContactId;
        this.mapOldUserIdToContactId = mapOldUserIdToContactId;
    }
 public void execute(QueueableContext context){
        final String METHODNAME = 'execute';
        System.debug('lstExistingOpportunity === ' + lstExistingOpportunity);
        System.debug('mapOldStringToACR === ' + mapOldStringToACR);
        System.debug('mapOldUserIdToContactId === ' + mapOldUserIdToContactId);
        System.debug('mapUserIdToContactId === ' + mapUserIdToContactId);
        System.debug('mapStringToACR === ' + mapStringToACR);
        //List of Account contact Relation Wrapper
        List<AccountContactRelationWrapper> lstACRWrap = new List<AccountContactRelationWrapper>();
        Integer countOppty = 0;
        try{
        for(Opportunity objOpportunity : lstNewOpportunity){
         AccountContactRelationWrapper objAccConRelWrap = new AccountContactRelationWrapper();
          if( mapUserIdToContactId.containsKey(objOpportunity.OwnerId) && 
              (mapStringToACR.isEmpty() || !mapStringToACR.containsKey(''+ objOpportunity.TRAV_Agency_Broker__c + mapUserIdToContactId.get(objOpportunity.OwnerId)) ) ){
            objAccConRelWrap.AccountId = objOpportunity.TRAV_Agency_Broker__c;
            objAccConRelWrap.ContactId = mapUserIdToContactId.get(objOpportunity.OwnerId);
            objAccConRelWrap.isInsert = true;
            objAccConRelWrap.isDelete = false;
            lstACRWrap.add(objAccConRelWrap);   
            }
        }
        if( !mapOldStringToACR.isEmpty()){
        for(AccountContactRelation objACR : mapOldStringToACR.values() ){
        Map<String,AccountContactRelationWrapper> mapStringToAccountCntRelWrap = new Map<String,AccountContactRelationWrapper>();
        AccountContactRelationWrapper objAccContRelWrap = new AccountContactRelationWrapper();
        if( !lstExistingOpportunity.isEmpty() ){
        for(Opportunity objExistOpportunity : lstExistingOpportunity){
            if(objACR.AccountId == objExistOpportunity.TRAV_Agency_Broker__c && objACR.ContactId == mapOldUserIdToContactId.get(objExistOpportunity.OwnerId) ){
                if(!mapStringToAccountCntRelWrap.isEmpty() && mapStringToAccountCntRelWrap.containsKey(''+ objACR.AccountId + objACR.ContactId) ){
                    objAccContRelWrap = mapStringToAccountCntRelWrap.get(''+ objACR.AccountId + objACR.ContactId);
                    objAccContRelWrap.isDelete = false;
                    lstACRWrap.add(objAccContRelWrap);
                }
            }
            else{
               objAccContRelWrap.strUniqueId = ''+ objACR.AccountId + objACR.ContactId;
               objAccContRelWrap.acrId = objACR.Id;
               objAccContRelWrap.isDelete = true;
               objAccContRelWrap.isInsert = false;
               mapStringToAccountCntRelWrap.put(objAccContRelWrap.strUniqueId,objAccContRelWrap);
               lstACRWrap.add(objAccContRelWrap);
            }
            }
        }else{
            objAccContRelWrap.acrId = objACR.Id;
            objAccContRelWrap.strUniqueId = ''+ objACR.AccountId + objACR.ContactId;
            objAccContRelWrap.isDelete = true;
            objAccContRelWrap.isInsert = false;
            lstACRWrap.add(objAccContRelWrap);
            }
        }// End of for loop on mapOldStringToACR
        } // End of If on mapOldStringToACR
            if( !lstACRWrap.isEmpty() ) {
            AccountContactRelationGenerator.generateACR(lstACRWrap);
        //    ExceptionUtility.logDMLError(CLASSNAME, METHODNAME, listSaveResult);
            }
       }
       catch(Exception objExp){
          ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        } //End of catch block
    }// End of method 'execute'
}