/**
* This class is used as a Selector Layer for Opportunity object. All queries for Opportunity object will be 
* performed in this class.
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------          
* Bharat Madaan            US60000          06/12/2019       Created this class with getOpportunityStage,
* 															 getOpportunityProducts, saveOpportunityProducts,
*															 getClosureList, getSubReasonList,checkForOtherAsReason
*															 and checkForPricingAsReason methods
* -----------------------------------------------------------------------------------------------------------              
*/
public with sharing class OpportunityDetails {
    /**
* This method is used to fetch the opportunity stage.
* @param opptyId The opportunity Id passed to the method to get a particular opportunity.
* @return String: Opportunity stage is returned for the Id provided as an input parameter.
*/
    @AuraEnabled
    public static String getOpportunityStage(Id opptyId) {
        Opportunity oppty = [SELECT Id,StageName FROM Opportunity WHERE Id=:opptyId];
        String optyStage = oppty.StageName;
        return optyStage;
    }
    
    /**
* This method is used to fetch the opportunity products.
* @param opptyId The opportunity Id passed to the method to get a particular opportunity.
* @return LisStringt<OpportunityLineItem>: List of OpportunityLineItem is returned for the Id provided 
* as an input parameter.
*/
    @AuraEnabled
    public static List<OpportunityLineItem> getOpportunityProducts(Id opptyId){
        List<OpportunityLineItem> opptyLineItem = [
            SELECT Id,
            Name,
            OpportunityId,
            Quantity,
            UnitPrice,
            TRAV_Closed_Reason__c,
            TRAV_Closed_Sub_Reason__c
            FROM OpportunityLineItem 
            WHERE OpportunityId=:opptyId
        ];
        return opptyLineItem;
    }
    
    /**
* This method is used to save the opportunity products.
* @return LisStringt<OpportunityLineItem>: List of OpportunityLineItem is returned for the Id provided 
* as an input parameter.
*/
    @AuraEnabled
    public static List<OpportunityLineItem> saveOpportunityProducts(List<OpportunityLineItem> opptyProd, List<String> opptyProdReasonList,List<String> opptyProdSubReasonList){
        System.debug('>>>>0'+opptyProd);
        System.debug('>>>>1'+opptyProdReasonList);
        System.debug('>>>>2'+opptyProdSubReasonList);
        List<OpportunityLineItem> products = new List<OpportunityLineItem>();
        String reasons = '';
        String subReasons = '';
        
        if(opptyProdReasonList!=null && !opptyProdReasonList.isEmpty()){
            Boolean reasonStart = true;
            for(String str : opptyProdReasonList){
                if(reasonStart){
                    reasons = str;
                    reasonStart = false;
                }else{
                    reasons = reasons + ';' + str;
                }
            } 
            System.debug('>>>>4'+reasons);
        }  
        if(opptyProdSubReasonList!=null && !opptyProdSubReasonList.isEmpty()){
            Boolean subreasonStart = true;
            for(String str : opptyProdSubReasonList){
                if(subreasonStart){
                    subReasons = str;
                    subreasonStart = false;
                }else{
                    subReasons = subReasons + ';' + str;
                }
            }
            System.debug('>>>>4'+subReasons);
        } 
        For(OpportunityLineItem oppLine : opptyProd){
            oppLine.TRAV_Closed_Reason__c = reasons;   
            oppLine.TRAV_Closed_Sub_Reason__c = subReasons;
        }     
        upsert opptyProd;
        return opptyProd;
    }
    
    /**
* This method is used to save the picklist values.
* @return LisStringt<OpportunityLineItem>: List of OpportunityLineItem is returned for the Id provided 
* as an input parameter.
*/
    @AuraEnabled
    public static List<String> getClosureList(String opptyCloseReason){
        List<String> genericList = Constants.GenericReasonList;
        List<String> wonList = Constants.WonReasonList;
        List<String> declinedList = Constants.DeclinedReasonList;
        List<String> quoteList = Constants.QuoteUnSucReasonList;
        system.debug('value to check'+opptyCloseReason);
        List<String> prodCloseList = new List<String>();
        if(opptyCloseReason =='Written'){
            prodCloseList.addAll(genericList);
            prodCloseList.addAll(wonList);
        }
        else if(opptyCloseReason =='Declined'){
            prodCloseList.addAll(genericList);
            prodCloseList.addAll(declinedList);
        }
        else if(opptyCloseReason =='Quote Unsuccessful'){
            prodCloseList.addAll(genericList);
            prodCloseList.addAll(quoteList);
        }
        system.debug('return list=='+prodCloseList);
        return prodCloseList;
    }
    
    /**
* This method is used to get the Sub-Reason values.
* @parameter List<String>, String : List of close reasons and Opportunity close reason is given as input
* @return List <String>: List of String is returned for the field TRAV_Close_Sub_Reason__c
* 
*/
    @AuraEnabled
    public static List<String> getSubReasonList(List<String> opptyProductReasonList, String opptyCloseReason){
        List<String> coverageList = Constants.CoverageSubReasonList;
        List<String> priceList = Constants.PriceSubReasonList;
        List<String> prodSubReasonList = new List<String>();
        for(String str : opptyProductReasonList){
            if(opptyCloseReason =='Quote Unsuccessful' && str == 'Coverage'){
                prodSubReasonList.addAll(coverageList);
            }
            if(opptyCloseReason =='Quote Unsuccessful' && str =='Pricing'){
                prodSubReasonList.addAll(priceList);
            }
        }
        return prodSubReasonList;
    }
    
    /**
* This method is used to chck if Reasons is Other.
* @parameter List String<OpportunityLineItem>: List of String is given as input paramter 
* @return Boolean variable
*/
    @AuraEnabled
    public static Boolean checkForOtherAsReason(List<String> opptyProductReason){
        Boolean isOther = false;
        for(String str : opptyProductReason){
            if(str == 'Other'){
                isOther = true;
                Break;
            }
        }
        return isOther;
    }
}