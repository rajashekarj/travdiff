/**
* Utility class that makes it easier to do operations with the classes in the ConnectApi.
*
* Modification Log    :
* -------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -------------------------------------------------------------------------------------------------------
* Ankit Bhatnagar        US59902              06/10/2019       Added Condition to check Prospect   Oppty
* 
*/

global class ChatterUtility {

    /**
    * This method generate Feeed Input in order to post on chatter
    * @param  
    * strCommunityId:       put it as null if not available
    * strSubjectId:     meniton the id of ther record on which notification has to be posted
    * textWithMention:  The complete chatter post. In order to mention someone just place the ID in
    *                   this format -eg {0058A00000xxxx}
    * @return ConnectApi.FeedItemInput
    */
    public static ConnectApi.FeedItemInput generateFeedInputWithMentions(String strCommunityId, String strSubjectId,
                                                                         String strTextWithMentions) {
        System.debug('**************feedinputs>>>>'+strCommunityId);
        return generateFeedInput(strCommunityId, strSubjectId, strTextWithMentions);
                                                                             
    }
    
    /**
    * This method generate Feeed Input in order to post on chatter
    * @param  
    * strCommunityId:       put it as null if not available
    * strSubjectId:     meniton the id of ther record on which notification has to be posted
    * textWithMention:  The complete chatter post. In order to mention someone just place the ID in
    *                   this format -eg {0058A00000xxxx}
    * @return ConnectApi.FeedItemInput
    */
    private static ConnectApi.FeedItemInput generateFeedInput(String strCommunityId, String strSubjectId,
                                                              String strFormattedText) {
        ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
        messageInput.messageSegments = getMessageSegmentInputs(strFormattedText);
        ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
        input.body = messageInput;
        if(strSubjectId != null && strSubjectId !=''){
            input.SubjectId = strSubjectId;
        }
        return input;
    }
    
    /**
    * This method generates the List of MessageSegmentInput
    * @param  
    * strInputText: Chatter message to be posted
    * @return List<ConnectApi.MessageSegmentInput>
    */
    public static List<ConnectApi.MessageSegmentInput> getMessageSegmentInputs(String strInputText) {
        List<ConnectApi.MessageSegmentInput> messageSegmentInputs = new List<ConnectApi.MessageSegmentInput>();
        Integer strPos = 0;
        Pattern globalPattern = Pattern.compile(Constants.GLOBALPATTERN);
        Matcher globalMatcher = globalPattern.matcher(strInputText);
        while (globalMatcher.find()) {
            String strTextSegment = strInputText.substring(strPos, globalMatcher.start());
            String strMatchingText = globalMatcher.group();
            if (strMatchingText.startsWith('{')) {
                addTextSegment(messageSegmentInputs, strTextSegment); 
                // Strip off the { and }.
                String strInnerMatchedText = strMatchingText.substring(1, strMatchingText.length() - 1);
                // This is a mention id.
                ConnectApi.MentionSegmentInput mentionSegmentInput = makeMentionSegmentInput(strInnerMatchedText);
                messageSegmentInputs.add(mentionSegmentInput);
                strPos = globalMatcher.end();
            }
            else {
               
            }
        }
        if (strPos < strInputText.length()) {
            String strTrailingText = strInputText.substring(strPos, strInputText.length());
            addTextSegment(messageSegmentInputs, strTrailingText);
        }

        return messageSegmentInputs;
    }
    
    /**
    * @description This method will be adding the TextSegment to the input
    * @param  
    * messageSegmentInputs: Message Segment Input
    * strText: Chatter Message
    * @return void
    */
    private static void addTextSegment(List<ConnectApi.MessageSegmentInput> messageSegmentInputs, String strText) {
        if (strText != null && strText.length() > 0) {
            ConnectApi.TextSegmentInput textSegmentInput = makeTextSegmentInput(strText);
            messageSegmentInputs.add(textSegmentInput);
        }
    }
    
    /**
    * @description This method will be adding the text to text Segment input
    * @param  
    * strText: Custom Chatter Post
    * 
    * @return ConnectApi.TextSegmentInput
    */
    private static ConnectApi.TextSegmentInput makeTextSegmentInput(String strText) {
        ConnectApi.TextSegmentInput strTextSegment = new ConnectApi.TextSegmentInput();
        strTextSegment.text = strText;
        return strTextSegment;
    }
    
    /**
    * @description: Method will set the value to MenitonSegmentInput
    * @param  
    * strMentionId: Meniton will be the id of the user which we want to tag
    * @return ConnectApi.MentionSegmentInput
    */
    private static ConnectApi.MentionSegmentInput makeMentionSegmentInput(String strMentionId) {
        ConnectApi.MentionSegmentInput mentionSegment = new ConnectApi.MentionSegmentInput();
        mentionSegment.id = strMentionId;
        return mentionSegment;
    }
    /**
    * @description: Method will set the value to postToChatter
    * @param  
    * strMentionId: Meniton will be the id of the user which we want to tag
    * @return ConnectApi.MentionSegmentInput
    */
    public static void postToChatter(List<NotificationWrapper> notList) {
        //List of feed items to insert
        List<FeedItem> lstFeedItem = new List<FeedItem>();
        //Feed Item obj to be inserted in the list
        FeedItem objFeedItem;
        for(NotificationWrapper notWrap : notList) {
            objFeedItem = new FeedItem();
            objFeedItem.ParentId = notWrap.objectId;
            objFeedItem.body = notWrap.Subject;
            objFeedItem.IsRichText = true;
            lstFeedItem.add(objFeedItem);
        }
        Database.insert(lstFeedItem);
    }
}