/**
* Test class for Agency Delete Trigger Class
*
* @Author : Deloitte Consulting
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------
*  Pratik Jogwar            US79245          03/20/2020    Original Version
* -----------------------------------------------------------------------------------------------------------
*/   

@isTest
private class TestAgencyDelTriggerHandler {
    
    static testmethod void testAgencydelAftrInsert() {
        
        Account agency = TestDataFactory.createProspect();
        Account agency2 = TestDataFactory.createAgency();
        List<Account> lstAccount = new List<Account>{agency,agency2};
        insert lstAccount;
        
        TRAV_Producer__c objProducer = TestDataFactory.createProducers();
        insert objProducer;
        
        TRAV_Account_Producer_Relation__c objAccountProducerRelationReturn= TestDataFactory.createAccountProducerRelation(objProducer.Id, lstAccount[0].Id);
        insert objAccountProducerRelationReturn;
        
        TRAV_Agency_Delete_Tracking__c objAgDel = new TRAV_Agency_Delete_Tracking__c();
        objAgDel.TRAV_New_Agency_ID__c = lstAccount[0].Id;
        objAgDel.TRAV_Old_Agency_ID__c = lstAccount[1].Id;
        objAgDel.TRAV_Producer_Code__c = objProducer.Id;
        
        insert objAgDel;
        
        objAccountProducerRelationReturn.TRAV_Prior_Agency__c = lstAccount[1].Id;
        update objAccountProducerRelationReturn;
        
    }
}