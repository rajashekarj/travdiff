/**
* This class is used as Controller for Lightning Component CustomLeadsListView. This returns the list of Leads 
* Owned by the logged in User and the list of Leads that the logged in User has followed.
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------          
* Shashank Shastri       US60972              06/10/2019       Original Version
* -----------------------------------------------------------------------------------------------------------              
*/
global with sharing class CustomLeadsListViewController {
    /**
* This method is invoked on the init of the lightning component.
* The method is caching the data so subsequent loads are faster.
* @return List<Lead> List of Leads the where the logged in user is the owner addition to this list
* the list of leads the user is following
*/
    @AuraEnabled(cacheable = true)
    global static List<Lead> fetchLeadsIFollow(){
        /*Using the Map to get the Set<Id> for the list of leads owned by the user.
*This set<Id> to be used to prevent duplicate entries in the table
*/
        Map<Id, Lead> leadsOwnedMap = new Map<Id, Lead>();
        //List Returned to the Lightning component. Contains Leads owned and the leads followed by the logged in user.
        List<Lead> leadsIFollow = new List<Lead>();
        //This is a set of Leads owned by the user. To prevent duplication will check if user is following a lead record he owns
        Set<Id> setOfLeadIdsOwned = new Set<Id>();
        //This set will be populated from entitysubscription object to get the Lead record Ids that the user is following 
        Set<Id> setOfLeadIdsFollowed = new Set<Id>();
        //Get Leads that the User is the owner of 
        leadsOwnedMap = LeadSelector.getLeadsIOwn(UserInfo.getUserId()); 
        setOfLeadIdsOwned = leadsOwnedMap.keySet();
        leadsIFollow = leadsOwnedMap.values();
        //Query on the entitysubscription to get the list of Records followed by the Logged in User
        for(EntitySubscription objES : EntitySubscriptionSelector.getSubscriptionList(UserInfo.getUserId())){
            //If the sObject type is Lead then add the Ids to a set.
            if(objES.ParentId.getSObjectType().getDescribe().getName().equalsIgnoreCase('Lead') && !setOfLeadIdsOwned.contains(objES.ParentId)){
                setOfLeadIdsFollowed.add(objES.ParentId);
            }
        }
        //Get List of records the user follows and Add it to the list of records owned
        if(!setOfLeadIdsFollowed.isEmpty()){
            leadsIFollow.addAll(LeadSelector.getLeadsBySetOfIds(setOfLeadIdsFollowed));
            // Sorting the final list
            leadsIFollow.sort();
        }
        //Return the final sorted list
        return leadsIFollow;
    }
}