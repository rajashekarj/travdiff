/**
* This class is used as a apex controller for DaysLeftInCurrentMonth lightning web component. 
* 
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Tejal Pradhan          US63928              03/17/2020        Apex controller for LWC DaysLeftInCurrentMonth
*-----------------------------------------------------------------------------------------------------------
*/
public without sharing class DaysLeftInCurrentMonthController {
    /**
* This is a method is used to get the current accounting cycle information
* @param null
* @return AccountingMonthDetailWrapper
*/
    @AuraEnabled(cacheable=true)
    public static AccountingMonthDetailWrapper getAccountingMonthDetails() {       
        final string METHODNAME = 'getAccountingMonthDetails';
        final string CLASSNAME = 'DaysLeftInCurrentMonthController';
        List<AccountingMonthDetailWrapper> wrapperAccMonth = new List<AccountingMonthDetailWrapper>();
        try{
            
            Date startDate = System.today();
            List<TRAV_Days_Left_in_Current_Month__mdt> listDaysLeftMetadata = MetadataSelector.getCurrentAccountCycleInformation();
            for(TRAV_Days_Left_in_Current_Month__mdt currentRecord : listDaysLeftMetadata){
                wrapperAccMonth.add(new AccountingMonthDetailWrapper(currentRecord.MasterLabel,startDate.daysBetween(currentRecord.TRAV_Cycle_End_Date__c),currentRecord.TRAV_Cycle_End_Date__c  ));
            }
            
        }
        catch(Exception objException){
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objException, null);
        }
        if(wrapperAccMonth.size() >= 1){
            return wrapperAccMonth[0] ;
        }
        else{
            return null;
        }
    }
    
}