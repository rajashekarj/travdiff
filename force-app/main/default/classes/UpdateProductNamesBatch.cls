global without sharing class UpdateProductNamesBatch implements Database.Batchable<SObject>, Database.RaisesPlatformEvents {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('UpdateProductNamesBatch initiated, job ID: ' + bc.getJobId());

        List<Id> recordTypeIdList = new List<Id>();
        recordTypeIdList.Add(SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(Constants.NBBSIRECTYPE).getRecordTypeId());
        recordTypeIdList.Add(SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(Constants.RENEWALBSIRECTYPE).getRecordTypeId());
        recordTypeIdList.Add(SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(Constants.POLICYCHANGEBSIRECTYPE).getRecordTypeId());

        String queryString = 'SELECT ID, TRAV_Product_Names_Abr__c FROM Opportunity WHERE RecordTypeID in :recordTypeIdList AND Number_Of_Line_Items__c > 0';
        System.debug(queryString);

        return Database.getQueryLocator(queryString);
    }

    global void execute(Database.BatchableContext bc, List<Opportunity> oppsInScope) {
        final string METHODNAME = 'execute';
        final string CLASSNAME = 'UpdateProductNamesBatch';
        
        List<Opportunity> oppsToUpdate = new List<Opportunity>();
        List<String> opportunityLineitemProductAbbrList = new List<String>();

        try {
            for(Opportunity opp : oppsInScope) {
                for(OpportunityLineItem lineItem :[SELECT OpportunityID, Product2.TRAV_Product_Abbreviation__c FROM OpportunityLineItem WHERE OpportunityID = :opp.ID]){
                    opportunityLineitemProductAbbrList.Add(lineItem.Product2.TRAV_Product_Abbreviation__c);
                }

                opp.TRAV_Product_Names_Abr__c = String.Join(opportunityLineitemProductAbbrList,';');
                oppsToUpdate.add(opp); 

                opportunityLineitemProductAbbrList.Clear();
            }

            List<Database.SaveResult> updateResults = Database.update(oppsToUpdate, false);
           // ExceptionUtility.logDMLError(CLASSNAME, METHODNAME, updateResults);

            System.debug(LoggingLevel.FINEST, updateResults);
        } catch(exception objExp) {
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        }
    }

    global void finish(Database.BatchableContext bc) {
        final string METHODNAME = 'execute';
        final string CLASSNAME = 'UpdateProductNamesBatch';
        
        try {
            AsyncApexJob job = [SELECT Id, ApexClass.Name, FORMAT(CreatedDate), FORMAT(CompletedDate), CreatedBy.Name, JobItemsProcessed, NumberOfErrors, TotalJobItems FROM AsyncApexJob WHERE Id = :bc.getJobId()];
            List<Object> strFormats = new List<Object>{job.ApexClass.Name, bc.getJobId()};
            String msg = String.format('{0} batch job chunk completed with job ID: {1}', strFormats);
            System.debug(msg);

            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            Map<String, Object> jobMap = job.getPopulatedFieldsAsMap();
            String headers = '';
            for (String fieldName : jobMap.keySet()) {
                headers += '<th>' + fieldName + '</th>';
            }

            List<Object> emailBodyData = new List<Object>(jobMap.values());
            String body =  '';
            for (Object fieldVal : emailBodyData) {
                body += '<td>' + fieldVal + '</td>';
            }

            String tableTempl = '<table><tr>{0}</tr><tr>{1}</tr></table>';
            String table = String.format(tableTempl, new List<Object>{headers, body});
            email.setToAddresses(new List<String>{'ben09002@gmail.com', 'elawrie@travelers.com', 'ishukla@travelers.com'}); 
            email.setSubject(msg); 
            email.setHtmlBody(table); 

            Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email});
        } catch(exception objExp) {
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        }
    }
}