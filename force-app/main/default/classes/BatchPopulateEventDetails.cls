/**
 * BatchPopulateEventDetails Class for populating details on event existing data 
 *
 * @Author : Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * -----------------------------------------------------------------------------------------------------------
 */
global class BatchPopulateEventDetails implements Database.Batchable<sObject> {
    /**
	* This method collect the batches of records or objects to be passed to execute
	* @param Database.BatchableContext
	* @return Database.QueryLocator record
	*/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String strQuery = 'SELECT Id, WhoId,TRAV_Related_Agency_Contact__c,TRAV_Primary_Contact_Name__c ';
        strQuery=strQuery+ 'FROM Event WHERE WhoId != null AND TRAV_Primary_Contact_Name__c = null';
        return Database.getQueryLocator(strQuery);
    } //end of start Method
    /**
	* This method process each batch of records for the implementation logic
	* @param Database.BatchableContext record
	* @param List of OpportunityTeamMember 
	* @return void 
	*/
    global void execute(Database.BatchableContext batchCon, List<Event> lstevent){
        Map<Id,Id> mapContactIds = new Map<Id,Id>();
        for(Event objevent : lstevent) {
            //check if whoid is contact 
            String str = objevent.WhoId;
            if(str.startsWith('003')) {
                mapContactIds.put(objEvent.Id,objEvent.WhoId);
            }
        }
        if( mapContactIds.values().size() > 0 ) {
            List<Event> lstToUpdate = new List<Event>();
            List<Contact> lstContact = new List<Contact>();
            Set<Id> setContIds = new Set<Id>();
            setContIds.addAll(mapContactIds.values());
            lstContact = ContactSelector.fetchAgencyContactsById(setContIds);
            Map<Id,String> mapAgencyContacts = new  Map<Id,String>();
            for(Contact objContact : lstContact) {
                mapAgencyContacts.put(objContact.Id,objContact.Name);
            }
            if(mapAgencyContacts.keySet().size() > 0) {
                //Populating values 
                for(Event objEvent : lstevent){ 
                    if(mapContactIds.containsKey(objEvent.Id) ){
                        if( mapAgencyContacts.containsKey(mapContactIds.get(objEvent.Id)) ){
                            objEvent.TRAV_Related_Agency_Contact__c = true;
                            objEvent.TRAV_Primary_Contact_Name__c = mapAgencyContacts.get(mapContactIds.get(objEvent.Id));  
                            lstToUpdate.add(objEvent);
                        }
                    } 
                }
            }
            //Updating existing data
            if(lstToUpdate.size() > 0) {
                Database.update(lstToUpdate,false);
            }
        }
        
    }
    /**
* This method execute any post-processing operations test
* @param Database.BatchableContext record 
* @return void 
*/
    global void finish(Database.BatchableContext batchCon){
        
    } 
}