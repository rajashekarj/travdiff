public class ContactSPCreateWrapper {
public class BMLoc {
		public String bmLocId;
	}

	public String srcSysCd;
	public String srcId;
	public String forceUpdt;
	public String fullNm;
	public String ptyNm;
	public String fstNm;
	public String lstNm;
	public String updatedBy;
	public String busTelNbr;
	public String licInd;
	public String busEmailAddr;
	public List<BMLoc> BMLoc;
	public List<RoleCdArr> roleCdArr;

	public class RoleCdArr {
		public String roleCd;
	}

}