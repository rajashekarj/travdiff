/**
 * Test class for BatchNotificationGeneration Class
 *
 * @Author : Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    					Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Sayanka Mohanty       US62717/US62205/US62214              06/24/2019       Original Version
 * Shashank Shastri 	 US62717/US62205/US62214			  07/17/2019       Modified
 * -----------------------------------------------------------------------------------------------------------
 */
@isTest 
public class TestBatchNotificationGeneration {
    @testSetup static void createTestData(){
		//Create Users
        List<User> lstUser = new List<user>();
        
        User objUser1 = TestDataFactory.createTestUser('BI - National Accounts','testUser1','testLastName1');
        lstUser.add(objUser1);
        User objUser2 = TestDataFactory.createTestUser('BI - Support','testUser2','testLastName2');
        User objUser3 = TestDataFactory.createTestUser('System Administrator','testUser3','testLastName3');
        lstUser.add(objUser2);
        if(!lstUser.isEmpty()){
        	insert lstUser;
        }
        system.assert(lstUser.size() == 2);
        User objUser4 = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs(objUser4){
		//Add Account
        Account objAccount = TestDataFactory.createProspect();
        insert objAccount;
        /*Add Contact
        List<Contact> cnList = TestDataFactory.createTravelersContact(1);
        cnList[0].AccountId = objAccount.Id;
        insert cnList;*/

        //Add opportunity
        List<Opportunity> lstOpportunityFinal = new List<Opportunity>();
        Opportunity objOpportunity = TestDataFactory.createOppty('Written', 'Test oppty A',objAccount.Id);
        Opportunity objOpportunity2 = TestDataFactory.createOppty('Written', 'Test oppty B',objAccount.Id);
        Opportunity objOpportunity3 = TestDataFactory.createOppty('Propose', 'Test oppty C',objAccount.Id);
        objOpportunity.OwnerId = objUser1.id;
        //objOpportunity.TRAV_Policy_Effective_Date__c  = (system.today()).addDays(-40);
	objOpportunity.TRAV_Popup_Stage__c='Written';
        objOpportunity.TRAV_Policy_Effective_Date__c  = system.today();
        objOpportunity.TRAV_Multi_Year_Indicator__c = true;
        objOpportunity.TRAV_Deal_Years__c = 3;
        //objOpportunity.TRAV_Agent_Broker_Cont__c = cnList[0].Id;
        lstOpportunityFinal.add(objOpportunity);
        objOpportunity2.OwnerId = objUser1.id;
        objOpportunity2.TRAV_Policy_Effective_Date__c  = (system.today()).addDays(-30);
	objOpportunity2.TRAV_Popup_Stage__c='Written';
        objOpportunity2.TRAV_Multi_Year_Indicator__c = true;
        objOpportunity2.TRAV_Deal_Years__c = 3;
        objOpportunity3.TRAV_Policy_Effective_Date__c  = system.today();
        objOpportunity3.TRAV_Multi_Year_Indicator__c = true;
        objOpportunity3.TRAV_Deal_Years__c = 3;
        objOpportunity3.TRAV_StageChangeTimeStamp__c = (system.today()).addDays(-7);
	objOpportunity3.TRAV_Popup_Stage__c='Propose';
        objOpportunity3.Type = 'Renewal';
        lstOpportunityFinal.add(objOpportunity3);
        lstOpportunityFinal.add(objOpportunity2);
        system.debug('**opportunity List**'+lstOpportunityFinal);
        if(!lstOpportunityFinal.isEmpty()){
            insert lstOpportunityFinal;
        }
        system.assert(lstOpportunityFinal[0].StageName == 'Written');
        //system.assert(lstOpportunityFinal[0].TRAV_Multi_Year_Deal_Indicator__c == '2/3');
        //Add Opportunity Team Member
        List<OpportunityTeamMember> lstOpportunityTeamFinal = new List<OpportunityTeamMember>();
        //Select a user of BI-support profile
        User objUser = [SELECT Id, Name FROM User WHERE Profile.name = 'BI - Support' and isActive = true LIMIT 1];
        OpportunityTeamMember objOpportunityTeamMember = TestDataFactory.createOpptyTeammember(objUser.Id, objOpportunity.id, 'RMIS Information Account Executive');
        lstOpportunityTeamFinal.add(objOpportunityTeamMember);
        if(!lstOpportunityTeamFinal.isEmpty()){
            insert lstOpportunityTeamFinal;
        }   
        system.assert(lstOpportunityTeamFinal[0].TeamMemberRole == 'RMIS Information Account Executive');
        }
    }
    
    static testMethod void testMethod1() {
        User objUser = [select Id from User where Id = :UserInfo.getUserId()];
        System.runAs(objUser){
        //Test for US62205 scenario:Stage = written, Owner = BI - National Accounts, Effective Date = today-40
        Test.startTest();
		//Run Batch
        BatchNotificationGeneration obj = new BatchNotificationGeneration(6);
        DataBase.executeBatch(obj); 
		Test.stopTest();
        }
    }
    static testMethod void testMethod2() {
        //Test for US62214 scenario:Stage = written, Opportunity Team Member = RMIS rep, Effective Date = today-40
        Test.startTest();
		//Run Batch
        BatchNotificationGeneration obj = new BatchNotificationGeneration(5);
        DataBase.executeBatch(obj); 
        Test.stopTest();
    }
    static testMethod void testMethod3() {
        //Test for US63514  scenario:Stage = Submission,Account Name!= null, TRAV_StageChangeTimeStamp__c = today-30
        Test.startTest();
		//Run Batch
        Opportunity o = [SELECT Id, StageName, Account.Name, TRAV_StageChangeTimeStamp__c FROM Opportunity LIMIT 1  ];
        if(o != null){
            o.StageName = 'Submission';
            o.TRAV_StageChangeTimeStamp__c = (system.today()).addDays(-30);
        }
        update o;
        BatchNotificationGeneration obj = new BatchNotificationGeneration(39);
        DataBase.executeBatch(obj); 
        Test.stopTest();
    }
    static testMethod void testMethod4() {
        //Test for US62205B scenario:Stage = written, Owner = BI - National Accounts, Effective Date = today-40
        Test.startTest();
		//Run Batch
        BatchNotificationGeneration obj = new BatchNotificationGeneration(18);
        DataBase.executeBatch(obj); 
		Test.stopTest();
    }
    static testMethod void testMethod5() {
        //Test for US62205B Chatter scenario
        Test.startTest();
		//Run Batch
        BatchNotificationGeneration obj = new BatchNotificationGeneration(9);
        DataBase.executeBatch(obj); 
		Test.stopTest();
    }
    static testMethod void testMethod6() {
        //Explicitly cover other scenarios
        Test.startTest();
        List<NotificationWrapper> nwList = new List<NotificationWrapper>();
         List<NotificationWrapper> nwList1 = new List<NotificationWrapper>();
		NotificationWrapper nw = new NotificationWrapper();
        nw.Subject = 'Test Subject';
        nw.containsHyperlink = false;
        Map<String,String> testMap = new Map<String,String>();
        testMap.put('Subject','Test Subject');
        nw.mergeFieldAPIValMap = testMap;
        NotificationWrapper nw1 = new NotificationWrapper();
        nw1.Subject = 'Test Subject';
        Map<String,String> testMap1 = new Map<String,String>();
        testMap1.put('Subject','Test Subject');
        nw1.mergeFieldAPIValMap = testMap;
        //Additional conditions
        Id opptyId = [SELECT Id From Opportunity LIMIT 1].Id;
        nw.objectId = opptyId;
        nw.fieldValueMap = null;
        List<Id> emptyId = new List<Id>();
        nw.userIdList = emptyId;
		nwList1.add(nw1);
        nwList.add(nw);
        NotificationGenerator.generateTasks(nwList1);
        NotificationGenerator.generateEvents(nwList);
        NotificationGenerator.generateChatterPosts(nwList);
        Opportunity op = [Select TRAV_Agent_Broker_Cont__r.Account.RecordType.Name FROM Opportunity Limit 1];
        NotificationGenerator.getfieldApiValues(op, 'TRAV_Agent_Broker_Cont__r.Account.RecordType.Name');
		Test.stopTest();
    }
    private static testMethod void testScheduler(){
        SchedulerNotificationGeneration batchSchObj = new SchedulerNotificationGeneration();
		String sch = '0 0 23 * * ?'; 
        system.schedule('ScheduleNotifications', sch, batchSchObj);
    }
}
