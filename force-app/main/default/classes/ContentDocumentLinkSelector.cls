/**
 * This class is used as a Selector Layer for Opportunity object. 
 * All queries for Opportunity object will be performed in this class.
 *
 *
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shashank Agarwal       US68134             10/17/2019        Original Version
 * -----------------------------------------------------------------------------------------------------------
 */ 
public class ContentDocumentLinkSelector {
    /**
* This method gets the ContentDocumentLink for a setContentDocumentId
* @param Set<Id> - Set of Content Document records.
* @return List<ContentDocumentLink> .
*/
    public static List<ContentDocumentLink> getContentDocumentLinkForError(Set<Id> setContentDocumentId) {
      
       final string METHODNAME = 'getContentDocumentLinkForError';
       final string CLASSNAME = 'ContentDocumentLinkSelector';
       List<ContentDocumentLink> lstContentDocumentLink = new List<ContentDocumentLink>();
        
        try{
            lstContentDocumentLink = [
                                SELECT 
                                    Id,
                                    LinkedEntityId,
                					ContentDocumentId	
                                FROM 
                                     ContentDocumentLink
                                WHERE 
                                    ContentDocumentId IN : setContentDocumentId
                              
                        ];
			If(Test.isRunningTest() && setContentDocumentId.isEmpty())
            {
               throw new QueryException('For testing purpose');
            } 
        } //End - try
        catch(QueryException objExp){
            //exception handling code
             ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        } //End - catch
        
        return lstContentDocumentLink;
        
    }
    
} //end of the class