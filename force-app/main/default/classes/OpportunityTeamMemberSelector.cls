/**
* This class is used as a Selector Layer for OpportunityTeamMember object. 
* All queries for OpportunityTeamMember object will be performed in this class.
*
* 
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Isha Shukla            US60806         	 06/03/2019       Original Version
* Isha Shukla			 US62980			 06/24/2019		  Added fields in query for getOpportunityTeamMembers and getOpportunityTeamMembersByOpptyIds
* Isha Shukla            US62980            06/28/2019 	      Added SEMICOLON variable
* Shashank Agarwal       US62549            07/01/2019        Added getOpportunityTeamMembersForTask method
* Isha Shukla			 US63800			 07/16/2019		  Added getOpportunityTeamManagers method
* Shashank Agarwal		 US63739             07/30/2019       Added method getOpportunityTeamMembersForStweardshipEvents
* Isha Shukla			 US64798			 08/08/2019		  Updated method getOpportunities to include TRAV_Agency_Broker__c field
* Isha Shukla			 US63800			 08/22/2019		  Added method getOpportunityTeamMembersForBatch
* Shashank Agarwal       US65788			 09/04/2019		  Added method getOpportunityTeamMembersForUpdate
* Siddharth Menon		 US68464			 10/07/2019		  Added method getOpportunityTeamMembersFromMetadata
* -----------------------------------------------------------------------------------------------------------
*/
public class OpportunityTeamMemberSelector {
    private final static string CLASSNAME = 'OpportunityTeamMemberSelector';
    private static List<String> lstRole = Label.TRAV_Roles.split(Constants.SEMICOLON);
    /**
* This is a method is used to get list opportunity team members 
* @param  users and account id set
* @return List<OpportunityTeamMember> List of OpportunityTeamMembers based on the SOQL query provided as an input parameter.
*/
    public static List<OpportunityTeamMember> getOpportunityTeamMembers(Set<Id> setOfAccountId, Set<Id> setOfUserId) {
        final string METHODNAME = 'getOpportunityTeamMembers';
        List<OpportunityTeamMember> lstOpportunityTeam = new List<OpportunityTeamMember>();
        try {
            lstOpportunityTeam = new List<OpportunityTeamMember>([
                SELECT 
                Id,
                Opportunity.AccountId,
                Opportunity.TRAV_Agency_Broker__c,
                UserId,
                TeamMemberRole,
                TRAV_Active__c
                FROM 
                OpportunityTeamMember
                WHERE 
                UserId IN : setOfUserId
                AND 
                ( Opportunity.AccountId IN : setOfAccountId OR Opportunity.TRAV_Agency_Broker__c IN :setOfAccountId )
                AND
                TRAV_Active__c = true
                AND 
                TeamMemberRole IN :lstRole
                AND 
                (Opportunity.CloseDate > LAST_N_DAYS:365 OR Opportunity.CloseDate = null)
            ]);
            if(Test.isRunningTest()){
                throw new QueryException();
            }
        } //End of try
        catch(QueryException exp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, exp, null);
            
        } // End Catch
        
        return lstOpportunityTeam;
    } //end of method getOpportunityTeamMembers
    
    /**
     * This is a method is used to get list opportunity team members 
     * @param  opportunity id set
     * @return List<OpportunityTeamMember> List of OpportunityTeamMembers based on the SOQL query provided as an input parameter.
     */
	 public static List<OpportunityTeamMember> getOpportunityTeamMembersByOpptyIds(Set<Id> setOfOpportunityId) {
       final string METHODNAME = 'getOpportunityTeamMembersByOpptyIds';
       List<OpportunityTeamMember> lstOpportunityTeam = new List<OpportunityTeamMember>();
       try {
	   		lstOpportunityTeam = new List<OpportunityTeamMember>([
                                     SELECT 
                                           Id,
                                           OpportunityId,
                						   UserId,
                						   TRAV_Active__c,
                						   TeamMemberRole
                                     FROM 
                                           OpportunityTeamMember
                                     WHERE 
                                           OpportunityId IN : setOfOpportunityId
                				     AND
                						   TRAV_Active__c = true
                				     AND 
                						   TeamMemberRole IN :lstRole
                              ]);
           if(Test.isRunningTest()){
                throw new QueryException();
            }
       } 
       catch(QueryException exp) {
       		//exception handling code
      		ExceptionUtility.logApexException(CLASSNAME, METHODNAME, exp, null);
                
       } // End Catch
	  	
	   return lstOpportunityTeam;
	 } //end of method getLeads
     /**
     * This is a method is used to get list opportunity team members 
     * @param  opportunity id set
     * @return List<OpportunityTeamMember> List of OpportunityTeamMembers based on the SOQL query provided as an input parameter.
     */
	 public static Map<Id,List<OpportunityTeamMember>> getOpportunityTeamMembersForTask(Set<Id> setOfOpportunityId) {
       final string METHODNAME = 'getOpportunityTeamMembersForTask';
       List<OpportunityTeamMember> lstOpportunityTeam = new List<OpportunityTeamMember>();
       try {
	   		lstOpportunityTeam = new List<OpportunityTeamMember>([
                                     SELECT 
                                           Id,
                                           OpportunityId,
                						   UserId,
                						   TRAV_Active__c,
                						   TeamMemberRole
                                     FROM 
                                           OpportunityTeamMember
                                     WHERE 
                                           OpportunityId IN : setOfOpportunityId
                				     AND
                						   TRAV_Active__c = true
                				     AND 
                						   TeamMemberRole = :Constants.COLLATERALANALYST
                					 AND
                						   Opportunity.StageName = :Constants.SUBMISSION
                              ]);
           if(Test.isRunningTest()){
                throw new QueryException();
            }
       } 
       catch(QueryException exp) {
       		//exception handling code
      		ExceptionUtility.logApexException(CLASSNAME, METHODNAME, exp, null);
                
       } // End Catch
         System.debug('List of Oppty Team Member>>'+lstOpportunityTeam);
         Map<Id,List<OpportunityTeamMember>> mapOppIdToTeamMember = new Map<Id,List<OpportunityTeamMember>>();
         for(Id objOppId : setOfOpportunityId){
             List<OpportunityTeamMember> lstTeamMember = new List<OpportunityTeamMember>();
             for(OpportunityTeamMember objOpptyTeamMember : lstOpportunityTeam ){
                 if(objOpptyTeamMember.OpportunityId == objOppId){
                     lstTeamMember.add(objOpptyTeamMember);
                 }
             }
             mapOppIdToTeamMember.put(objOppId, lstTeamMember);
         }
           	
	   return mapOppIdToTeamMember;
	 } //end of method getOpportunityTeamMembersForTask
    
    /**
     * This is a method is used to get list opportunity team members 
     * @param  opportunity id set
     * @return List<OpportunityTeamMember> List of OpportunityTeamMembers based on the SOQL query provided as an input parameter.
     */
	 public static Map<Id,List<OpportunityTeamMember>> getOpportunityTeamMembersFromOpty(Set<Id> setOfOpportunityId) {
       final string METHODNAME = 'getOpportunityTeamMembersFromOpty';
       List<OpportunityTeamMember> lstOpportunityTeam = new List<OpportunityTeamMember>();
       try {
	   		lstOpportunityTeam = new List<OpportunityTeamMember>([
                                     SELECT 
                                           OpportunityId,
                						   UserId,
                						   TRAV_Active__c,
                						   TeamMemberRole,TRAV_Created_From_System__c
                                     FROM 
                                           OpportunityTeamMember
                                     WHERE 
                                           OpportunityId IN : setOfOpportunityId
                				     AND
                						   TRAV_Active__c = true
                					 AND 
                						TRAV_Created_From_System__c = false
                              ]);
           if(Test.isRunningTest()){
                throw new QueryException();
            }
       } 
       catch(QueryException exp) {
       		//exception handling code
      		ExceptionUtility.logApexException(CLASSNAME, METHODNAME, exp, null);
                
       } // End Catch
         System.debug('List of Oppty Team Member>>'+lstOpportunityTeam);
         Map<Id,List<OpportunityTeamMember>> mapOppIdToTeamMember = new Map<Id,List<OpportunityTeamMember>>();
         for(Id objOppId : setOfOpportunityId){
             List<OpportunityTeamMember> lstTeamMember = new List<OpportunityTeamMember>();
             for(OpportunityTeamMember objOpptyTeamMember : lstOpportunityTeam ){
                 if(objOpptyTeamMember.OpportunityId == objOppId){
                     lstTeamMember.add(objOpptyTeamMember);
                 }
             }
             mapOppIdToTeamMember.put(objOppId, lstTeamMember);
         }
           	
	   return mapOppIdToTeamMember;
	 } //end of method getOpportunityTeamMembersFromOpty
    
     /**
     * This is a method is used to get list opportunity team memberswith Manager team members
     * @param  users and account id set
     * @return List<OpportunityTeamMember> List of OpportunityTeamMembers based on the SOQL query provided as an input parameter.
     */
	 public static List<OpportunityTeamMember> getOpportunityTeamManagers(Set<Id> setOpportunityIds, Set<Id> setOfOldUserId) {
       final string METHODNAME = 'getOpportunityTeamManagers';
       List<OpportunityTeamMember> lstOpportunityTeam = new List<OpportunityTeamMember>();
       try {
	   		lstOpportunityTeam = new List<OpportunityTeamMember>([
                                    	SELECT 
                							Id, UserId, OpportunityId 
                                        FROM 
                							OpportunityTeamMember 
                                        WHERE 
                							UserId = :setOfOldUserId 
                                        AND 
                							OpportunityId = :setOpportunityIds 
                                        AND 
                							TeamMemberRole = :Label.TRAV_Manager_Team_Role
                						AND 
                							TRAV_Active__c = true
                             		 ]);
           if(Test.isRunningTest()){
                throw new QueryException();
            }
       } //End of try
       catch(QueryException exp) {
       		//exception handling code
      		ExceptionUtility.logApexException(CLASSNAME, METHODNAME, exp, null);
                
       } // End Catch
	  	
	   return lstOpportunityTeam;
	 } //end of method getOpportunityTeamMembers
	 
	 /**
* This is a method is used to get list opportunity team members for Stwwardship Event Attendees Update
* @param  users and Opportunity id set
* @return  List<OpportunityTeamMember> List of OpportunityTeamMembers based on the SOQL query provided as an input parameter.
*/
    public static List<OpportunityTeamMember> getOpportunityTeamMembersForStweardshipEvents(Set<Id> setOpportunityId) {
        final string METHODNAME = 'getOpportunityTeamMembersForStweardshipEvents';
        List<OpportunityTeamMember> lstOpportunityTeam;
        try {
            lstOpportunityTeam = new List<OpportunityTeamMember>([
                SELECT 
                Id,
                OpportunityId,
                UserId,
                TeamMemberRole
                FROM 
                OpportunityTeamMember
                WHERE 
                OpportunityId IN : setOpportunityId
                AND
                TRAV_Active__c = true
                AND 
                TeamMemberRole IN :Constants.setOpportunityTeamMemberforStewardshipMeeting
                
            ]);
            if(Test.isRunningTest()){
                throw new QueryException();
            }
        } 
        catch(QueryException exp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, exp, null);
            
        } // End Catch
        
        
        return lstOpportunityTeam;
    } //end of method getOpportunityTeamMembersForStweardshipEvents
	
	/**
* This is a method is used to get list opportunity team members for for update according to line item approver
* @param  users and Opportunity id set
* @return  Map<Id,List<OpportunityTeamMember>> Map of OpportunityTeamMembers based on the SOQL query provided as an input parameter.
*/
    
    public static List<OpportunityTeamMember> getOpportunityTeamMembersForUpdate(Set<Id> setOpportunityId) {
        final string METHODNAME = 'getOpportunityTeamMembersForUpdate';
        List<OpportunityTeamMember> lstOpportunityTeam;
        try {
            lstOpportunityTeam = new List<OpportunityTeamMember>([
                SELECT 
                Id,
                OpportunityId,
                UserId,
                TeamMemberRole
                FROM 
                OpportunityTeamMember
                WHERE 
                OpportunityId IN : setOpportunityId
                AND
                TRAV_Active__c = true
                AND 
                TeamMemberRole IN :Constants.setOpportunityTeamMemberforUpdate
                
            ]);
            if(Test.isRunningTest()){
                throw new QueryException();
            }
        } 
        catch(QueryException exp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, exp, null);
            
        } // End Catch
        
        
        return lstOpportunityTeam;
    } //end of method 

     /**
* This is a method is used to get list opportunity team memberswith Manager team members
* @param  role tring and Opportunity id set
* @return List<OpportunityTeamMember> List of OpportunityTeamMembers based on the SOQL query provided as an input parameter.
*/
    /*public static List<OpportunityTeamMember> getOpportunityTeamMembersForBatch(Set<Id> setOpportunityIds, String memberRole) {
        final string METHODNAME = 'getOpportunityTeamMembersForBatch';
        List<OpportunityTeamMember> lstOpportunityTeam = new List<OpportunityTeamMember>();
        try {
            lstOpportunityTeam = new List<OpportunityTeamMember>([
                SELECT 
                Id, UserId, OpportunityId,User.ManagerId,User.Manager.ManagerId,TRAV_Manager__c  
                FROM 
                OpportunityTeamMember 
                WHERE 
                OpportunityId = :setOpportunityIds 
                AND
                (TeamMemberRole =:memberRole OR ( TeamMemberRole = 'Managing Director' AND TRAV_Created_From_System__c  = true) )
                AND 
                TRAV_Active__c = true
            ]);
            if(Test.isRunningTest()){
                throw new QueryException();
            }
        } //End of try
        catch(QueryException exp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, exp, null);
            
        } // End Catch
        
        return lstOpportunityTeam;
    } //end of method getOpportunityTeamMembersForBatch
*/
    /**
* This is a method is used to get list opportunity team members that were created through Metadata
* @param  MetadataUsername list and Opportunity id set
* @return List<OpportunityTeamMember> List of OpportunityTeamMembers based on the SOQL query provided as an input parameter.
*/
    public static List<OpportunityTeamMember> getOpportunityTeamMembersFromMetadata(Set<Id> listOpportunityIds,List<String> listMetadataUsernames) {
        final string METHODNAME = 'getOpportunityTeamMembersFromMetadata';
        List<OpportunityTeamMember> lstOpportunityTeam = new List<OpportunityTeamMember>();
        try {
            lstOpportunityTeam = new List<OpportunityTeamMember>([
                SELECT 
                Id,UserId 
                FROM 
                OpportunityTeamMember 
                WHERE 
                OpportunityId IN :listOpportunityIds 
                AND 
                User.UserName IN : listMetadataUsernames]);
            if(Test.isRunningTest()){
                throw new QueryException();
            }
        } //End of try
        catch(QueryException exp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, exp, null);
            
        } // End Catch
        
        return lstOpportunityTeam;
    } //end of method getOpportunityTeamMembersFromMetadata
    
    /**
* This is a method is used to get list opportunity team members
* @param  Set of users
* @return List<OpportunityTeamMember> List of OpportunityTeamMembers
*/
    /*public static List<OpportunityTeamMember> oppTeamMemberRole(Set<Id> userId) {
        List<OpportunityTeamMember> lstOppTeamMember = new List<OpportunityTeamMember>();
        if(userId != null) {
            lstOppTeamMember = [Select Id, TeamMemberRole, UserId FROM 
                                                       		OpportunityTeamMember 
                                                            WHERE UserId IN : userId and TeamMemberRole not in ('Account Executive','Underwriter','Approver','Reporting Manager')];
            system.debug('lstOppTeamMember>>'+lstOppTeamMember);
        }
        
        return lstOppTeamMember;
    }*/
    /**
* This is a method is used to get list opportunity team members
* @param  Set of users
* @return List<OpportunityTeamMember> List of OpportunityTeamMembers
*/
    public static List<OpportunityTeamMember> getManagersList(Set<Id> userId,Set<Id> oppId) {
        List<OpportunityTeamMember> lstOppTeamMember = new List<OpportunityTeamMember>();
        if(userId != null && oppId != null) {
            lstOppTeamMember = [Select Id,OpportunityId, TRAV_Manager__c, UserId FROM 
                                                       		OpportunityTeamMember 
                                                            WHERE UserId IN : userId AND OpportunityId IN : oppId];
            system.debug('lstOppTeamMember>>'+lstOppTeamMember);
        }
        
        return lstOppTeamMember;
    }
    /**
* This is a method is used to get list opportunity team members
* @param  Set of users
* @return List<OpportunityTeamMember> List of OpportunityTeamMembers
*/
    /*public static List<OpportunityTeamMember> getManagersListForDeletion(Set<Id> userId) {
        List<OpportunityTeamMember> lstOppTeamMember = new List<OpportunityTeamMember>();
        if(userId != null) {
            lstOppTeamMember = [Select Id,OpportunityId, TRAV_Manager__c, UserId FROM 
                                                       		OpportunityTeamMember 
                                                            WHERE UserId IN : userId];
        }
        
        return lstOppTeamMember;
    }*/
    /**
* This is a method is used to get list opportunity team members
* @param  Set of users
* @return List<OpportunityTeamMember> List of OpportunityTeamMembers
*/
    public static List<OpportunityTeamMember> getExistingTeamMem(Set<Id> opportunityId){
        List<OpportunityTeamMember> lstOppTeamMember = new List<OpportunityTeamMember>();
        if(opportunityId!=null){
            lstOppTeamMember = [Select Id, Name, TRAV_Manager__c, TeamMemberRole, TRAV_Business_Unit__c, UserId,OpportunityId From OpportunityTeamMember 
                             	where OpportunityId IN :opportunityId AND (TeamMemberRole != 'Reporting Manager' AND TeamMemberRole != 'Managing Director'AND TRAV_Created_From_system__c = false)];
        }
        return lstOppTeamMember;
    }
    
    /**
* This is a method is used to get list opportunity team members
* @param  Set of Opportunity
* @return List<OpportunityTeamMember> List of OpportunityTeamMembers
*/ 
    public static List<OpportunityTeamMember> getAllExistingByOpportunityId(Set<Id> opportunityIDs) {
        List<OpportunityTeamMember> lstOppTeamMember = new List<OpportunityTeamMember>();
        if(opportunityIDs != null) {
            lstOppTeamMember = [Select 
                                Id,OpportunityId, TRAV_Manager__c, UserId,TeamMemberRole,TRAV_Business_Unit__c, 
                                Opportunity.OwnerId, TRAV_Created_From_System__c
                                FROM 
                                OpportunityTeamMember 
                                WHERE OpportunityId IN : opportunityIDs AND 
                                TRAV_Active__c = true];
        }
        system.debug('lstOppTeamMember>inteamselector>'+lstOppTeamMember);
        return lstOppTeamMember;
    }
}