/**
 * Handler for TRAV_Agency_Delete_Tracking__c Trigger
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------
 * Pratik Jogwar            US79245            03/20/2020       Added code to afterInsert
 * -----------------------------------------------------------------------------------------------------------
 */
public class TRAV_AgencyDelTriggerHandler {
    public void afterInsert (List<TRAV_Agency_Delete_Tracking__c> newItems){
        Map<String,String> mapProdVsNewAgency = new Map<String,String>();
        Set<Id> setProducerCode = new Set<Id>();
        Set<id> setAgency = new Set<Id>();
        try{
            if (newItems != null && newItems.size() > 0){
            for (TRAV_Agency_Delete_Tracking__c objAgency : newItems){
                setProducerCode.add(objAgency.TRAV_Producer_Code__c);
                setAgency.add(objAgency.TRAV_New_Agency_ID__c);
                mapProdVsNewAgency.put(objAgency.TRAV_Producer_Code__c+objAgency.TRAV_New_Agency_ID__c, objAgency.TRAV_Old_Agency_ID__c);
            }
            
            
            List<TRAV_Account_Producer_Relation__c> lstAccRel = [Select 
                                                                 		id, 
                                                                 		TRAV_Producer__c, 
                                                                 		TRAV_Account__c , 
                                                                 		TRAV_Prior_Agency__c 
                                                                 from 
                                                                 		TRAV_Account_Producer_Relation__c 
                                                                 where  TRAV_Producer__c IN: setProducerCode
                                                                 AND 	TRAV_Account__c IN: setAgency];
            
            for (TRAV_Account_Producer_Relation__c objAccRel : lstAccRel ){
                if (objAccRel != null){
                    String key = String.valueOf(objAccRel.TRAV_Producer__c)+String.valueOf(objAccRel.TRAV_Account__c);
                    if (mapProdVsNewAgency != null && mapProdVsNewAgency.containsKey(key)){
                        objAccRel.TRAV_Prior_Agency__c = mapProdVsNewAgency.get(key);
                    }
                }
            }
            if (lstAccRel.size() > 0){
                
                Database.update(lstAccRel);
            }
        }
        }catch (Exception ex){
            ExceptionUtility.logApexException('TRAV_AgencyDelTriggerHandler', 'afterInsert', ex, null);
        }
    }
}