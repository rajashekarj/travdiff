/**
 * Test class for OpportunityProductPopUpUtility

*
 * @Author : Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Sayanka Mohanty       US63929/US63930     08/02/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
@isTest
public class TestOpportunityProductPopUpUtility {
     @testSetup static void createTestData(){
        //Add Account
        Account a = TestDataFactory.createProspect();
        insert a;
        system.assert(a.Name == 'Prospect Account');
        //Add a carrier
        Trav_Carrier__c carrier = new  Trav_Carrier__c(Name = 'Carrier A');
        insert carrier;
        system.assert(carrier.Name == 'Carrier A');
         
        //Add opportunity
        List<Opportunity> finalOpptyList = new List<Opportunity>();
        Opportunity o1 = TestDataFactory.createOppty('Submission', 'Test oppty A',a.Id);
        o1.TRAV_Winning_New_Carrier__c = carrier.Id;
        o1.TRAV_Policy_Effective_Date__c = Date.today().addYears(1);
        Opportunity o2 = TestDataFactory.createOppty('Declined', 'Test oppty B',a.Id);
        o2.TRAV_Winning_New_Carrier__c = carrier.Id;
        o2.TRAV_Policy_Effective_Date__c = Date.today().addYears(1);
        Opportunity o3 = TestDataFactory.createOppty('Written', 'Test oppty C',a.Id);
        o3.TRAV_Winning_New_Carrier__c = carrier.Id;
        o3.TRAV_Policy_Effective_Date__c = Date.today().addYears(1);
        Opportunity o4 = TestDataFactory.createOppty('Deferred', 'Test oppty D',a.Id);
        o4.TRAV_Winning_New_Carrier__c = carrier.Id;
        o4.TRAV_Defferal_Date__c = null;
        o4.TRAV_Policy_Effective_Date__c = Date.today().addYears(1);
        Opportunity o5 = TestDataFactory.createOppty('Lost', 'Test oppty E',a.Id);
        o5.TRAV_Winning_New_Carrier__c = carrier.Id;
        o5.TRAV_Policy_Effective_Date__c = Date.today().addYears(1);
        Opportunity o6 = TestDataFactory.createOppty('Quote Unsuccessful', 'Test oppty F',a.Id);
        o6.TRAV_Winning_New_Carrier__c = carrier.Id;
        o6.TRAV_Policy_Effective_Date__c = Date.today().addYears(1);
        Opportunity o7 = TestDataFactory.createOppty('Line Not Selected', 'Test oppty G',a.Id);
        o7.TRAV_Winning_New_Carrier__c = carrier.Id;
        o7.TRAV_Policy_Effective_Date__c = Date.today().addYears(1);
        Opportunity o8 = TestDataFactory.createOppty('Out of Appetite', 'Test oppty H',a.Id);
        o8.TRAV_Winning_New_Carrier__c = carrier.Id;
        o8.TRAV_Policy_Effective_Date__c = Date.today().addYears(1);
        o8.TRAV_Close_Description__c = '';
        o8.Type = 'Renewal';
        Opportunity o9 = TestDataFactory.createOppty('Out of Appetite', 'Test oppty I',a.Id);
        o9.TRAV_Winning_New_Carrier__c = carrier.Id;
        o9.TRAV_Policy_Effective_Date__c = Date.today().addYears(1);
        o9.Type = 'New Business';
        o9.TRAV_Close_Description__c = '';
        
         finalOpptyList.add(o1);
         finalOpptyList.add(o2);
         finalOpptyList.add(o3);
         finalOpptyList.add(o4);
         finalOpptyList.add(o5);
         finalOpptyList.add(o6);
         finalOpptyList.add(o7);
         finalOpptyList.add(o8);
         finalOpptyList.add(o9);
         
        system.debug('**opportunity List**'+finalOpptyList);
        if(!finalOpptyList.isEmpty()){
            insert finalOpptyList;
        }
        system.assert(finalOpptyList[0].StageName == 'Submission');
        system.assert(finalOpptyList[1].StageName == 'Declined');
        //Skip Validations
        
        insert new TRAV_Bypass_Settings__c(TRAV_Skip_Validation_Rules__c = true, SetupOwnerId = userInfo.getUserId());
         
        //Add Opportunity Line Item
        List<OpportunityLineItem> finalLineItemList = new List<OpportunityLineItem>();
        List<OpportunityLineItem> lineItemList1 = TestDataFactory.createOpportunityLineItem(finalOpptyList[1].Id, 5);
        List<OpportunityLineItem> lineItemList2 = TestDataFactory.createOpportunityLineItem(finalOpptyList[0].Id, 5); 
        finalLineItemList.addAll(lineItemList1);
        finalLineItemList.addAll(lineItemList2); 
         if(!finalLineItemList.isEmpty()){
             insert finalLineItemList;
         }
         system.assert(!finalLineItemList.isEmpty());
         system.debug('**FINAL ITEM LIST**'+finalLineItemList + ' ' + '**SIZE**'+finalLineItemList.size() );
     }
    static testmethod void testStageChangeOnOppty1(){
        //Unit test for US62930, Stage = Declined
        ClosedOpportunityProductWrapper closedWrap = new ClosedOpportunityProductWrapper();
        
        Test.startTest();
        Opportunity opp = [SELECT Id,StageName FROM Opportunity WHERE StageName = 'Declined' LIMIT 1];
        system.debug('opp>>>>'+opp);
        closedWrap = OpportunityProductPopUpUtility.getComponentLoadConfig(opp.Id, opp.StageName);
        system.debug('closed Wrap>>>'+closedWrap);
        Test.stopTest();
    }
    static testmethod void testStageChangeOnOppty2(){
        //Unit test for US62929, Stage = Bound
        Test.startTest();
        Opportunity opp = [SELECT Id,StageName FROM Opportunity WHERE StageName = 'Submission' LIMIT 1];
        OpportunityProductPopUpUtility.getComponentLoadConfig(opp.Id, opp.StageName);
        Test.stopTest();
    }
    static testmethod void testStageChangeOnOppty3(){
        //Unit test for US62930, Stage = Lost
        Test.startTest();
        Opportunity opp = [SELECT Id,StageName FROM Opportunity WHERE StageName = 'Lost' LIMIT 1];
        OpportunityProductPopUpUtility.getComponentLoadConfig(opp.Id, opp.StageName);
        Test.stopTest();
    }
    static testmethod void testStageChangeOnOppty4(){
        //Unit test for US62930, Stage = Quote Unsuccessful
        Test.startTest();
        Opportunity opp = [SELECT Id,StageName FROM Opportunity WHERE StageName = 'Quote Unsuccessful' LIMIT 1];
        OpportunityProductPopUpUtility.getComponentLoadConfig(opp.Id, opp.StageName);
        Test.stopTest();
    }
    static testmethod void testStageChangeOnOppty5(){
        //Unit test for US62930, Stage = Written
        Test.startTest();
        Opportunity opp = [SELECT Id,StageName FROM Opportunity WHERE StageName = 'Written' LIMIT 1];
        OpportunityProductPopUpUtility.getComponentLoadConfig(opp.Id, opp.StageName);
        Test.stopTest();
    }
    static testmethod void testStageChangeOnOppty6(){
        //Unit test for US62930, Stage = Deferred
        Test.startTest();
        Opportunity opp = [SELECT Id,StageName FROM Opportunity WHERE StageName = 'Deferred' LIMIT 1];
        OpportunityProductPopUpUtility.getComponentLoadConfig(opp.Id, opp.StageName);
        Test.stopTest();
    }
    static testmethod void testStageChangeOnOppty7(){
        //Unit test for US62930, Stage = Line Not Selected
        Test.startTest();
        Opportunity opp = [SELECT Id,StageName FROM Opportunity WHERE StageName = 'Line Not Selected' LIMIT 1];
        OpportunityProductPopUpUtility.getComponentLoadConfig(opp.Id, opp.StageName);
        Test.stopTest();
    }
    static testmethod void testStageChangeOnOppty8(){
        //Unit test for US62930, Stage = Line Not Selected
        Test.startTest();
        Opportunity opp = [SELECT Id,StageName FROM Opportunity WHERE StageName = 'Out of Appetite' LIMIT 1];
        OpportunityProductPopUpUtility.getComponentLoadConfig(opp.Id, opp.StageName);
        Test.stopTest();
    }
    static testmethod void testStageChangeOnOppty9(){
        //Unit test for US62930, Stage = Line Not Selected
        Test.startTest();
        List<Opportunity> opp = [SELECT Id,StageName FROM Opportunity WHERE StageName = 'Out of Appetite' LIMIT 2];
        OpportunityProductPopUpUtility.getComponentLoadConfig(opp[1].Id, opp[1].StageName);
        Test.stopTest();
    }
    static testmethod void testGenericScenarios(){
        Test.startTest();
        OpportunityProductPopUpUtility.getStageDependentReasons('Deferred');
        OpportunityProductPopUpUtility.getStageDependentReasons('Quote Unsuccessful');
        OpportunityProductPopUpUtility.getStageDependentReasons('Written');
        OpportunityProductPopUpUtility.getStageDependentReasons('Declined');
        OpportunityProductPopUpUtility.getStageDependentReasons('Line Not Selected');
        OpportunityProductPopUpUtility.getStageDependentReasons('Bound');
		OpportunityProductPopUpUtility.getStageDependentReasons('Lost');
        OpportunityProductPopUpUtility.getSubListItems('Coverage', 'Quote Unsuccessful');
        OpportunityProductPopUpUtility.getSubListItems('Pricing', 'Quote Unsuccessful');
        OpportunityProductPopUpUtility.getSubListItems('Pricing;Coverage', 'Quote Unsuccessful');
        List<OpportunityLineItem> opptyLList = [SELECT Id FROM OpportunityLineItem LIMIT 2 ];
        OpportunityProductPopUpUtility.saveProductList(opptyLList,'Deferred');
        Test.stopTest();
    }
}