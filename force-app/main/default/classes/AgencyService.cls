/**
* Utility Class for used to Rollup Agency details
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer     User Story/Defect   Date           Description
* -----------------------------------------------------------------------------------------------------------
* Bhanu Reddy       US78071        03/10/2020      Initial Version
*/
public  class AgencyService {
 /**
* This is a method rollup Opportunity realted to the Agency
* @param lstOpportunity - List of new values in the for the Opportunity records.
* 
* */
  public static void rollupOpportuntiyTOAgency(list<Opportunity> lstOpportunity,set<id> setAgencyids){
      //variable declarations
        try{
      Map<id,Account> MapAccountToupdate = new Map<id,Account>();
      Account objAgency;
      set<id> setAgencyid = new set<id>();
      Map<string,Integer> MapAccoutToCount = new Map<string,Integer>();
      Map<string,Integer> MapAccountToRewalCount = new Map<string,Integer>();
      if(setAgencyids == null){
      for(Opportunity  objOpportunity:lstOpportunity){
          //check if the Opportunity has agency Location
          if(objOpportunity.TRAV_Agency_Broker__c !=null){
             setAgencyid.add(objOpportunity.TRAV_Agency_Broker__c);
          }// end if agency check
      } //end for  -iteration over new inserted or deleted items
    } //end for batch check
    else{
        setAgencyid.addall(setAgencyids);
    }
     Integer countOfNewBusiness =0;
      if(!setAgencyid.isEmpty()){
           for(AggregateResult objAggregateRe:[select count(id) countOpp,TRAV_Agency_Broker__c agency,Recordtype.name recordtypename from 
                                                 Opportunity where TRAV_Agency_Broker__c in:setAgencyid and
                                                  stagename in
                                                    (:Constants.PROSPECT,:Constants.SUBMISSION,:Constants.UNDERREVIEW,:Constants.PROPOSEANDSELL,:Constants.BOUND)
                                                 GROUP BY ROLLUP(TRAV_Agency_Broker__c,Recordtype.name)]){

                             string strAgency = string.valueof(string.valueof(objAggregateRe.get('agency')));
                                countOfNewBusiness = Integer.valueof(objAggregateRe.get('countOpp'));
                              string strRecordTypename =   string.valueof(objAggregateRe.get('recordtypename'));
                               if(strRecordTypename == Constants.OPPTYRECORDTYPENEWBUSINESSEXTERNAL
                                            ||strRecordTypename==Constants.OPPTYRECORDTYPENEWBUSINESS
                                        ||strRecordTypename==Constants.OPPTYRECORDTYPENEWBUSINESSNA){
                                 
                                        
                                 if(MapAccoutToCount.containsKey(strAgency)){
                                    Integer Count = MapAccoutToCount.get(strAgency)+countOfNewBusiness;
                                    MapAccoutToCount.put(strAgency,Count);
                                 } //end if Map Check
                                 else{
                                    MapAccoutToCount.put(strAgency,countOfNewBusiness);
                                 }
                               } //end if record type check
                               else  if(strRecordTypename == Constants.OPPTYRENEWALRECORDTYPE
                               ||strRecordTypename==Constants.OPPTYRENEWALRECORDTYPENA
                           ||strRecordTypename==Constants.OPPTYRENEWALRECORDTYPEEXTERNAL){
                                if(MapAccountToRewalCount.containsKey(strAgency)){
                                    Integer Count = MapAccountToRewalCount.get(strAgency)+countOfNewBusiness;
                                    MapAccountToRewalCount.put(strAgency,Count);
                                 } //end if Map Check
                                 else{
                                    MapAccountToRewalCount.put(strAgency,countOfNewBusiness);
                                 }
                               } //end of else loop
                            } //end for loop
                      if(!MapAccoutToCount.isEmpty()){
                           for(string idAccount: MapAccoutToCount.keyset()){
                                objAgency = new Account();
                                objAgency.id = idAccount;
                                objAgency.TRAV_New_Business_Open_Opportunity__c = MapAccoutToCount.get(idAccount);
                                MapAccountToupdate.put(objAgency.id,objAgency);
                           } //end for loop iteration over map
                      }  //end if map empty check
                      if(!MapAccountToRewalCount.isEmpty()){
                          for(string idAccount : MapAccountToRewalCount.keyset()){
                              if(MapAccountToupdate.containsKey(idAccount)){
                                objAgency = MapAccountToupdate.get(idAccount);
                              }
                              else{
                                 objAgency = new Account();
                                 objAgency.id = idAccount;
                              }
                              objAgency.TRAV_Renewal_Open_Opportunity__c = MapAccountToRewalCount.get(idAccount);
                              MapAccountToupdate.put(objAgency.id,objAgency);
                          }
                          system.System.debug('Mpa to check'+MapAccountToupdate);
                      } //end if renewal count check
                      if( !MapAccountToupdate.isEmpty()){
                            update MapAccountToupdate.values();
                      }   //end if lsitEmpty check                       
      } //end if set agency not empty
    } //end try
    catch( Exception objExcep){
        ExceptionUtility.logApexException('AgencyService', 'rollupOpportuntiyTOAgency', objExcep, null);
    }
  }  //end method rollupOpportuntiyTOAgency
} //end class AgencyService
