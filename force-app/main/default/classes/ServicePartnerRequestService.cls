/**
* Utility Class for Opportunity Trigger Handler. This class will have logic for all the implementations.
* 
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer		      User Story/Defect	  Date		    Description
* Shashank Agarwal    US62549			  07/01/2019    Added Method taskCreationOnInsertion
* Shashank Agarwal    US63868             07/28/2019    Modified Mehod taskCreationOnInsertion
* -----------------------------------------------------------------------------------------------------------              
* 
*/
public class ServicePartnerRequestService {
    /**
* This method will create a task for OpportunityTeamMember on Role 'Colletral Analyst' When a record is inserted. 
* @param lstNewServicePartnerRequest - List of new values in the for the records.
* @return void - returns void .
*/
    public static void taskCreationOnInsertion(List<TRAV_Service_Partner_Request__c> lstNewServicePartnerRequest){
        
        final string METHODNAME = 'taskCreationOnInsertion';
        // Set of  account ids related to opportinities.
        Set<Id> setOpportunityIds = new Set<Id>();
        // Map for getting account names related to opportunites. 
        Map<Id,List<OpportunityTeamMember>> mapOpportunity = new Map<Id,List<OpportunityTeamMember>>();
        //List of tasks to be inserted
        List<Task> lstTask = new List<Task>();
        
        // Populating the Set setAccountIds
        for(TRAV_Service_Partner_Request__c objSPRequest : lstNewServicePartnerRequest){
            setOpportunityIds.add(objSPRequest.Opportunity__c);
        }
        
        try{
            
            //Null check on set of opportunity Id
            if(!setOpportunityIds.isEmpty()){
                //Populating Map
                mapOpportunity = OpportunityTeamMemberSelector.getOpportunityTeamMembersForTask(setOpportunityIds);
            } // End of If condition
            
            for(TRAV_Service_Partner_Request__c objSPRequest : lstNewServicePartnerRequest){
                List<OpportunityTeamMember> lstTeamMember = mapOpportunity.get(objSPRequest.Opportunity__c);
                if(lstTeamMember != Null){
                    for(OpportunityTeamMember objTeamMember : lstTeamMember){
                        Task objtsk = new Task();
                        objtsk.OwnerId = objTeamMember.UserId;
                        objtsk.Subject = Constants.DigitizationTaskSubject;
                        objtsk.ActivityDate = objSPRequest.TRAV_Need_by_Date__c;
                        objtsk.Service_Partner_Request__c = objSPRequest.Id;
                        objtsk.WhatId = objSPRequest.Id;
                        lstTask.add(objtsk);
                    } //End of for loop on OpportunityTeamMember
                } //End of Null Check on lstTeamMember
            } //End of for loop for TRAV_Service_Partner_Request__c
            if( !lstTask.isEmpty() ) {
                Database.SaveResult[] lstSaveResult = Database.insert(lstTask, false);
                ExceptionUtility.logDMLError(Constants.SERVICEPARTNERREQUESTSERVICE, METHODNAME, lstSaveResult);             
            } //End of if
			if(Test.isRunningTest()){
                throw new QueryException();
            }
        }catch(Exception objExp){
             ExceptionUtility.logApexException(Constants.SERVICEPARTNERREQUESTSERVICE, METHODNAME, objExp, null);
        } //End of Catch
    } //End of Method
    
} //End of class