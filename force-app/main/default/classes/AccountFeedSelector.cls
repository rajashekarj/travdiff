/**
 * This class is used as a Selector Layer for AccountFeed object. 
 * All queries for AccountFeed object will be performed in this class.
 *
 * @Author Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shruti Gupta		       US59528             07/08/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
public with sharing class AccountFeedSelector { 
    
    /**
    * This is a method which is used to get list of chatter feed on Account
    * @param no parameters used
    * @return List<Lead> List of AccountFeed on Account object
    */
    public static List<AccountFeed> getAccountFeed(Set<Id> setAccID)
    {
        List<AccountFeed> lstAccountFeed = new List<AccountFeed>([
            SELECT Id,
            Body FROM AccountFeed
            WHERE ParentId IN: setAccID
        ]);
        return lstAccountFeed;
    } //end of method getLeadsIOwn
} //End of the class