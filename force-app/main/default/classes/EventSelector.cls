/**
* Utility Class for OpportunityTeamMember Trigger Handler. This class will have logic for all the implementations.
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------
* Shashank Agarwal		 US63739             07/30/2019       Added method getEventForStewardshipEventRelation
* Shashank Agarwal		 US68134             10/17/2019       Added method getEventAddingErrorOnNonEditableNotes
*/
public class EventSelector {
/**
     * This is a method is used to get list Event
     * @param Set<Id> -Set Opportunity Ids
     * @return List<Event> List of accounts based on the SOQL query provided as an input parameter.
     */

    public static List<Event> getEventForStewardshipEventRelation(Set<Id> setOpportunityIds){

        List<Event> lstEvent = new List<Event>();

        try{
            lstEvent = [
                                SELECT
                                    Id,
                					WhatId,
                                    Type,Subject
                                FROM
                                    Event
                                WHERE
                                    WhatId IN : setOpportunityIds
                				AND
                					Type IN :Constants.EventTypeStewardship
                        ];
        } //End - try
        catch(QueryException exp){
            //exception handling code

        } //End - catch

      return lstEvent;

    } //End of the Method

    /**
     * This is a method is used to get list Event
     * @param Set<Id> -Set Event Ids
     * @return List<Event> List of accounts based on the SOQL query provided as an input parameter.
     */

    public static List<Event> getEventAddingErrorOnNonEditableNotes(Set<Id> setEventIds){

        List<Event> lstEvent = new List<Event>();

        try{
            lstEvent = [
                                SELECT
                                    Id,
                                    Type
                                FROM
                                    Event
                                WHERE
                                    Id IN : setEventIds
                				AND
                					TRAV_Source_Sys_Code__c = :Constants.EventTypeLyncEvent
                        ];
        } //End - try
        catch(QueryException exp){
            //exception handling code

        } //End - catch

      return lstEvent;

    } //End of the Method
}