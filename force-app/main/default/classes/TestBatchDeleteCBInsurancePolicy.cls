/**
* Test class for TestBatchDeleteCBInsurancePolicy Class
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer             User Story/Defect       Date            Description
* Erec Lawrie           US79515/US80830         04/16/2020      Original Version
* -------------------------------------------------------------------------------------------------------------
*/
@isTest
private class TestBatchDeleteCBInsurancePolicy {

    @isTest
    static void testCase(){
        TestDataFactory.createRenewalBatchsetting();

        //Adding bypass custom setting to bypass validation rule for testing this batch
        TRAV_Bypass_Settings__c objCustomSettingBypass = new TRAV_Bypass_Settings__c();
        objCustomSettingBypass.TRAV_Skip_Validation_Rules__c = true;
        insert objCustomSettingBypass;

        //create account record
        Account objProspectAccount = TestDataFactory.createProspect();
        List<Account> lstAccount = new List<Account>{objProspectAccount};
        insert lstAccount;

        List<Product2> lstProducts = TestDataFactory.createProducts(5);
        insert lstProducts;

        //associate products to pricebook
        List<PricebookEntry> lstStandardPricebookEntries = TestDataFactory.createPricebookEntries(lstProducts, Test.getStandardPricebookId());
        insert lstStandardPricebookEntries;

        //create pricebook
        Pricebook2 priceBook = TestDataFactory.createTestPricebook('BI - National Accounts');
        insert priceBook;

        List<PricebookEntry> lstBIPricebookEntries = TestDataFactory.createPricebookEntries(lstProducts, priceBook.Id);
        insert lstBIPricebookEntries;

        //Creating Opportunity Records
        List<Opportunity> lstOpptyForRenewal = TestDataFactory.createOpportunities(2, objProspectAccount.Id, 'BI-NA');
        lstOpptyForRenewal[0].Name = 'TEST CB KEEP';
        lstOpptyForRenewal[0].TRAV_Direct_Source_Sys_Code__c = 'CB MDM';
        lstOpptyForRenewal[0].TRAV_Submission_Status__c = 'ACTIV';
        lstOpptyForRenewal[0].PriceBook2Id = priceBook.Id;

        lstOpptyForRenewal[1].Name = 'TEST CB DELETE';
        lstOpptyForRenewal[1].TRAV_Direct_Source_Sys_Code__c = 'CB MDM';
        lstOpptyForRenewal[1].TRAV_Submission_Status__c = 'INACT';
        lstOpptyForRenewal[1].PriceBook2Id = priceBook.Id;
        insert lstOpptyForRenewal;


        List<OpportunityLineItem> lstOpportunityLineItems = new List<OpportunityLineItem>();
        OpportunityLineItem objOppLineItem = new OpportunityLineItem();
        objOppLineItem.OpportunityId = lstOpptyForRenewal[0].Id;
        objOppLineItem.Product2Id = lstProducts[0].Id;
        objOppLineItem.TRAV_Direct_Source_Sys_Code__c = 'CB MDM';
        objOppLineItem.TRAV_Submission_Coverage_Status__c = 'Purged';
        lstOpportunityLineItems.add(objOppLineItem);

        OpportunityLineItem objOppLineItem1 = new OpportunityLineItem();
        objOppLineItem1.OpportunityId = lstOpptyForRenewal[0].Id;
        objOppLineItem1.Product2Id = lstProducts[1].Id;
        objOppLineItem1.TRAV_Direct_Source_Sys_Code__c = 'CB MDM';
        objOppLineItem1.TRAV_Submission_Coverage_Status__c = 'Written';
        lstOpportunityLineItems.add(objOppLineItem1);

        OpportunityLineItem objOppLineItem2 = new OpportunityLineItem();
        objOppLineItem2.OpportunityId = lstOpptyForRenewal[1].Id;
        objOppLineItem2.Product2Id = lstProducts[0].Id;
        objOppLineItem2.TRAV_Direct_Source_Sys_Code__c = 'CB MDM';
        objOppLineItem2.TRAV_Submission_Coverage_Status__c = 'Written';
        lstOpportunityLineItems.add(objOppLineItem2);

        List<Database.saveResult> opportunityLineItemSaveResult = database.insert(lstOpportunityLineItems,false);

        List<InsurancePolicy> lstInsurancePolicy = new List<InsurancePolicy>();
        lstInsurancePolicy.add(
            new InsurancePolicy (
                Name = 'Insurnace Policy 1',
                NameInsuredId = lstAccount[0].Id,
                TRAV_Direct_Source_Sys_Code__c = 'CB MDM',
                Status = 'Purged'
            )
        );
            
        lstInsurancePolicy.add(
            new InsurancePolicy (
                Name = 'Insurnace Policy 2',
                NameInsuredId = lstAccount[0].Id,
                TRAV_Direct_Source_Sys_Code__c = 'CB MDM',
                Status = 'Bound'
            )
        );
            
        insert lstInsurancePolicy;



        Test.startTest();
        BatchDeleteCBInsurancePolicy obj = new BatchDeleteCBInsurancePolicy();
        Id batchId = DataBase.executeBatch(obj);

        String schTime = '0 0 12 * * ?';
        system.schedule('TestBatchDeleteCBInsurancePolicy', schTime, obj);
        Test.stopTest();

        //System.assertEquals([SELECT StageName FROM Opportunity WHERE TRAV_Renewal_Opportunity__c = :lstOpptyForRenewal[0].Id].StageName,'Prospect');
    }
}