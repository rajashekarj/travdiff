/**
* This class is the utility class for Contact Search and Pull Functionality
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect          Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty        US69347/US69863          11/10/2019      Original Version
* -----------------------------------------------------------------------------------------------------------
*/
public without sharing class ContactSearchPullUtility {
public static boolean errorProcessing = false;
public static Contact backupContactFromUI = null ;
    /**
* This method will execute all Contact Search Pull Operations sequentially
* @param String strObjId, Contact contactRec
* @return ContactSearchAndPullReturnWrapper
*/
    public static ContactSearchAndPullReturnWrapper executeContactSearchPullOperations(String strObjId, Contact objContactRec){
        backupContactFromUI = objContactRec;
        ContactSearchAndPullReturnWrapper wrapperContactReturnToLC = new ContactSearchAndPullReturnWrapper();
        final string METHODNAME = 'executeContactSearchPullOperations';
        String strFinalResult = '';
        String strContactOperation = '';
        HttpResponse responseGetContactResult;
        ContactSearchAndPullRetrieveWrapper wrapperRes;
        ContactSearchAndPullResponseWrapper wrapperCS;
        ContactSPUpdateResponseWrapper wrapperUpdateCS;
        ContactSPCreateWrapper wrapperRes1;
        Boolean booleanIsUpdate = false;
        try{
            //1.Create JSON Request 
            String strJsonRequestBody = createContactRequestPayload(strObjId, objContactRec);
            system.debug('**JSON REQUEST BODY**'+strJSONRequestBody);
            //2.Pass JSON request and call create/update API
            if(String.isNotBlank(strJsonRequestBody)){
                HttpResponse responseResult = callContactCreateUpdateAPI(strJsonRequestBody); 
                if(responseResult != null){
                    system.debug('**RESPONSE FROM CREATE API**'+responseResult.getBody());
                    //3.Evaluate response status & invoke corresponding methods
                    system.debug('**RESPONSE STATUS CODE**'+responseResult.getStatusCode());
                    if(responseResult.getStatusCode() == 200 || responseResult.getStatusCode() == 202 ){
                        //3a.Scenario  1:Contact created in MDM and needs to be created in SF
                        //Parse response body and get MDM ID[If MDM ID Comes in a list, handle it in catch]
                        try{
                            //This wrapper stores String MDMID
                            wrapperCS = parseResponseResult(responseResult);
                            
                        }catch(Exception objEXP){
                            //This wrapper stores LIST MDMID
                            wrapperUpdateCS = parseUpdateResponseResult(responseResult);  
                            booleanIsUpdate = true;
                        }
                        system.debug('**WRAPPER RESULT**'+wrapperCS);
                        system.debug('**WRAPPER UPDATE RESULT**'+wrapperUpdateCS);
                        //call getContact API and pass MDM ID
                        if(wrapperCS != null || (wrapperUpdateCS != null && booleanIsUpdate)){
                            if(wrapperCS.Status == '200'){
                            //Call method to create Contact in SF and associate to Opty
                                wrapperRes1 = (ContactSPCreateWrapper)JSON.deserialize(strJsonRequestBody,ContactSPCreateWrapper.class);
                                //Pass MDM and other details for Contact Creation in SF    
                                if(!booleanIsUpdate && wrapperCS.Status == '200'){
                                        createContactInSFOnCreate(wrapperRes1,strObjId,wrapperCS.MdmId);
                                    }else{
                                        if(wrapperCS.Status == '200'){
                                        createContactInSFOnCreate(wrapperRes1,strObjId,wrapperUpdateCS.MdmId[0]);
                                        }
                                    }
                                if(errorProcessing){
                                     wrapperContactReturnToLC.strReturn = 'failure';
                                }else{
                                    //Add update scenario for updating SRC ID asynchronous--this is in handle scenario
                                    wrapperContactReturnToLC.strReturn = 'create';
                                }
                                
                            }else if(wrapperCS.Status == '202'){
                                //3b.Scenario  2,3,4:Contact present in MDM and needs to be created/associated in SF 
                                //Create wrapper and send it to LC
                                //We have to send MDM ID and Contact details to lightning component
                                system.debug('**WrapperUpdateCS**'+wrapperUpdateCS);
                                system.debug('**WrapperCS**'+wrapperCS);
                                if(wrapperUpdateCS != null){
                                if(wrapperUpdateCS.MdmId.size() > 0){
                                    for(String strMDMID : wrapperUpdateCS.MdmId){
                                        ContactSearchAndPullRetrieveWrapper wrapperContactRetrieve = new  ContactSearchAndPullRetrieveWrapper();
                                        while(wrapperContactReturnToLC.lstWrapperContactDetails.size() < Constants.MAX_CONTACT){
                                            HttpResponse responseRetrieved = callGetContactAPI(strMDMID);
                                            wrapperContactRetrieve = (ContactSearchAndPullRetrieveWrapper)JSON.deserialize(responseRetrieved.getBody(),ContactSearchAndPullRetrieveWrapper.class);
                                            wrapperContactReturnToLC.lstWrapperContactDetails.add(wrapperContactRetrieve);
                                        }
                                    }
                                    
                                }
                                }else if(wrapperCS != null){
                                    ContactSearchAndPullRetrieveWrapper wrapperContactRetrieve = new  ContactSearchAndPullRetrieveWrapper();
                                    HttpResponse responseRetrieved = callGetContactAPI(wrappercs.MdmId);
                                    wrapperContactRetrieve = (ContactSearchAndPullRetrieveWrapper)JSON.deserialize(responseRetrieved.getBody(),ContactSearchAndPullRetrieveWrapper.class);
                                    wrapperContactReturnToLC.lstWrapperContactDetails.add(wrapperContactRetrieve);
                                }
                                if(!errorProcessing){
                                    wrapperContactReturnToLC.strReturn = 'update';
                                }else{
                                    wrapperContactReturnToLC.strReturn = 'failure';
                                }
                            }
                        }
                    }
                        else if(responseResult.getStatusCode() == 201 || responseResult.getStatusCode() == 400){
                            //3c.Scenario  5,6:Server error or callout failure
                            //Log Exception in Exception Logs 
                            try{
                                wrapperCS = parseResponseResult(responseResult);
                            }catch(Exception objEXP){
                                wrapperUpdateCS = parseUpdateResponseResult(responseResult);  
                                booleanIsUpdate = true;
                            }
                            String strMessage = booleanIsUpdate ? wrapperUpdateCS.Message : wrapperCS.Message;
                            //Log error in Exception Object
                            ExceptionUtility.logIntegrationerror('ContactSearchPullUtility', METHODNAME,wrapperCS.Message);
                            wrapperContactReturnToLC.strReturn = 'failure';
                        }
                    else{
                        ExceptionUtility.logIntegrationerror('ContactSearchPullUtility', METHODNAME,'UNKNOWN STATUS CODE');
                        wrapperContactReturnToLC.strReturn = 'failure'; 
                    }
                    }
                }
            system.debug('**WRAPPER RETURNED TO LC**'+wrapperContactReturnToLC);
            }catch(exception objExp) {
                ExceptionUtility.logApexException('ContactSearchPullUtility', METHODNAME, objExp, null);
                system.debug('**Exception at line**'+objExp.getLineNumber()+'**'+objExp.getMessage());
            }
        return wrapperContactReturnToLC;
    }
    /**
* This method will create Contact Request Body and call subsequent methods
* @param String strObjId, Contact contactRec
* @return String
*/
    public static String createContactRequestPayload(String strObjId, Contact contactRec){
        //Accept Parameter and create the JSON string
        
        String strRequestBody = '';
        try{
        MDMCreateIndividualWrapper requestWrapper = new MDMCreateIndividualWrapper(strObjId, contactRec);
        system.debug('**MDM Create Individual wrapper**'+requestWrapper);
        strRequestBody = JSON.serialize(requestWrapper);
        system.debug('**REQUEST CREATED**'+strRequestBody);
        }catch(Exception objExp){
            system.debug('**Exception2 at line**'+objExp.getLineNumber()+'**'+objExp.getMessage());
        }
        return strRequestBody;
    }
    /**
* This method will call the Create/Update API and return reponse[create/update/error]
* @param String strRequestBody
* @return HTTPResponse responseResult
*/ 
    public static HttpResponse callContactCreateUpdateAPI(String strRequestBody){
        HTTPResponse responseResult = RequestResponseHandler.constructEndPointAndDoCallout(Constants.CONTACT_CREATE_API, null, strRequestBody, false);
        return responseResult;
    }
    /**
* This method will call the getContactAPI 
* @param String strMDMID
* @return HTTPResponse responseResult
*/ 
    public static HttpResponse callGetContactAPI(String strMDMID){
        //Pass MDM ID in a Map
        Map<String,String> mapParameters = new Map<String,String>{'agncyIndId' => strMDMID };
            HTTPResponse responseResult = RequestResponseHandler.constructEndPointAndDoCallout(Constants.CONTACT_GET_API, mapParameters, '', false);
            system.debug('**RESPONSE FROM GET API**'+responseResult.getBody());
        return responseResult;
    }
    
    /**
* This method will parse response payload
* @param HttpResponse response
* @return ContactSearchAndPullResponseWrapper wrapperResult
*/ 
    public static ContactSearchAndPullResponseWrapper parseResponseResult(HttpResponse response){
        ContactSearchAndPullResponseWrapper wrapperResult = (ContactSearchAndPullResponseWrapper)JSON.deserialize(response.getBody(),ContactSearchAndPullResponseWrapper.class);
        return wrapperResult;
    }
    /**
* This method will parse response payload
* @param HttpResponse response
* @return ContactSearchAndPullResponseWrapper wrapperResult
*/ 
    public static ContactSPUpdateResponseWrapper parseUpdateResponseResult(HttpResponse response){
        ContactSPUpdateResponseWrapper wrapperResult = (ContactSPUpdateResponseWrapper)JSON.deserialize(response.getBody(),ContactSPUpdateResponseWrapper.class);
        return wrapperResult;
    }
    /**
* This method will parse,create and associate contacts in SF
* @param HttpResponse response, String strContactOperation,String strObjId
* @return String
*/ 
    public static String handleContactCreationAndAssociation(ContactSearchAndPullRetrieveWrapper wrapperRes, String strContactOperation,String strObjId, String strMDMID){
        Id idContactId = null;
        system.debug('**STRING CONTACT OPERATION**'+strContactOperation);
        try{
            if(strContactOperation.equalsIgnoreCase('update')){
            //Check if Contact is present in SF else create 
            Boolean contactExistsinSF = searchContactInSF(strMDMID); 
            if(contactExistsinSF){
                //do getApi call
                HttpResponse responseGetContactResult = callGetContactAPI(strMDMID);
                //getcontact details
                if(responseGetContactResult.getStatusCode() != 204 && responseGetContactResult.getStatusCode() != 400){
                    wrapperRes = (ContactSearchAndPullRetrieveWrapper)JSON.deserialize(responseGetContactResult.getBody(),ContactSearchAndPullRetrieveWrapper.class);
                }
                updateContactInSF(strMDMID,strObjId, wrapperRes); 
            }else{
                //do getApi call
                HttpResponse responseGetContactResult = callGetContactAPI(strMDMID);
                //createcontact
                if(responseGetContactResult.getStatusCode() != 204 && responseGetContactResult.getStatusCode() != 400){
                    wrapperRes = (ContactSearchAndPullRetrieveWrapper)JSON.deserialize(responseGetContactResult.getBody(),ContactSearchAndPullRetrieveWrapper.class);
                }
                createContactInSF( wrapperRes, strMDMID, strObjId);
                
            }
        }
        }catch(Exception objExp){
            system.debug('**Exception at**'+objExp.getLineNumber() +'**'+objExp.getMessage());
        }
        return '';
    }
    /**
* This method will search for Contact having same MDM ID in SF
* @param String strMDMId
* @return Boolean
*/ 
    public static Boolean searchContactInSF(String strMDMId){
        try{
            Contact objContact = [SELECT Id FROM Contact WHERE TRAV_External_Id__c = :strMDMId LIMIT 1];
            system.debug('**OBJ CONTACT**'+ objContact);
            if(objContact != null){
                return true;
            }else{
                return false;
            }
        }catch(Exception objExp){
            return false;
        }
    }      
    
    /**
* This method will update contacts in SF
* @param String strMDMId, String strObjId
* @return void
*/ 
    public static void updateContactInSF(String strMDMId, String strObjId,ContactSearchAndPullRetrieveWrapper wrapperRes){
    try{
        List<Contact> lstContacts = new List<Contact>();
        List<Opportunity> lstOpptys = new List<Opportunity>();
        Contact objContact = [SELECT Id FROM Contact WHERE TRAV_External_Id__c = :strMDMId LIMIT 1];
        if(strObjId.startsWith('001')){
        if(objContact != null){
            try{
                //objContact.TRAV_Agency_Broker__c = Id.ValueOf(strObjId);
                //objContact.AccountId = Id.ValueOf(strObjId);
                    createAccountContactRelationship( wrapperRes, objContact.Id);
                }catch(Exception objEx){
                    errorProcessing = true;
                    ExceptionUtility.logIntegrationerror('ContactSearchPullUtility', 'updateContactInSF', 'Agency Lookup error');
                    
                }
                lstContacts.add(objContact);
            }
        }
        else if(strObjId.startsWith('006')){
            if(objContact != null){
                Opportunity objoppty = [SELECT Id,TRAV_Agent_Broker_Cont__c FROM opportunity WHERE Id = :strObjId LIMIT 1 ];
                objoppty.TRAV_Agent_Broker_Cont__c = objContact.Id;
                lstOpptys.add(objoppty);
            }
        }
        if(!lstContacts.isEmpty()){
            database.update(lstContacts);
            //Create Account Contact Relationship
            createAccountContactRelationship(wrapperRes, lstContacts[0].Id);
            //Create AccountContactRole
            if(strObjId.startsWith('001')){
                if(!(checkContactAccountExists(objContact.Id, strObjId))){   
                    createAccountContactRole(objContact.Id, strObjId);
                }
               
            }
            
        }
        if(!lstOpptys.isEmpty()){
            database.update(lstOpptys);
            //Create Account Contact Relationship
            system.debug('**WRAPPER RESULT**'+json.serialize(wrapperRes));
            createAccountContactRelationship(wrapperRes, objContact.id);
            if(strObjId.startsWith('006')){
                if(!(checkContactOpptyExists(objContact.Id, strObjId))){
                    createOpportunityContactRole(objContact.Id, strObjId);
                }
            }
        }
        }catch(Exception objExp){
            system.debug('**TROUBLESOME EXCEPTION**'+objExp.getLineNumber()+'**WITH MSG**'+objExp.getMessage());
        } 
    }
    /**
* This method will create Contact
* @param void
* @return ContactSearchAndPullRetrieveWrapper wrapperRes, String strMDMID,String strObjId
*/ 
    public static Id createContactInSF(ContactSearchAndPullRetrieveWrapper wrapperRes, String strMDMID,String strObjId){
        Id contactId = null;
        //Retrieve recordtypeid by developer name
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('TRAV_Agency_Non_Licensed_Contact').getRecordTypeId();
        Contact objCon = new Contact();
        //Check if object is Account / Opportunity
        if(strObjId.startsWith('001')){
            //Account
            try{
                //objCon.TRAV_Agency_Broker__c = Id.ValueOf(strObjId);
                objCon.AccountId = Id.ValueOf(strObjId);
                if(Test.isRunningTest()){
                    throw new DMLException('Coverage');
                }
            }catch(Exception objEx){
                ExceptionUtility.logIntegrationerror('ContactSearchPullUtility', 'createContactInSF', 'Agency Lookup error');
                errorProcessing = true;
            }
        }
        else if(strObjId.startsWith('006')){
            //Opportunity
            //objCon.TRAV_Agency_Broker__c = [SELECT Id,TRAV_Agency_Broker__c FROM Opportunity where id = :strObjId LIMIT 1].TRAV_Agency_Broker__c;
            objCon.AccountId =  [SELECT Id,TRAV_Agency_Broker__c FROM Opportunity where id = :strObjId LIMIT 1].TRAV_Agency_Broker__c;
        }
        objCon.RecordTypeId = recordTypeId;
        objCon.FirstName = wrapperRes.fstNm;
        objCon.LastName = wrapperRes.lstNm;
        objCon.Email =  wrapperRes.busEmailAddr;
        objCon.Phone = wrapperRes.busTelNbr;
        objCon.TRAV_SRC_ID__c = String.isBlank(objCon.TRAV_SRC_ID__c) ? GuidUtility.NewGuid() :objCon.TRAV_SRC_ID__c;
        objCon.TRAV_GU_ID__c = String.isBlank(objCon.TRAV_SRC_ID__c) ? GuidUtility.NewGuid() :objCon.TRAV_SRC_ID__c;
        objCon.TRAV_Source_Sys_Code__c = 'SFSC';
        objCon.TRAV_External_Id__c = strMDMID;
        if(backupContactFromUI != null){
            objCon.TRAV_Agent_Preferences__c = backupContactFromUI.TRAV_Agent_Preferences__c;
            objCon.MobilePhone= backupContactFromUI.MobilePhone;
            objCon.Fax = backupContactFromUI.Fax;
            objCon.TRAV_Job_Title__c = backupContactFromUI.TRAV_Job_Title__c;
            objCon.Birthdate = backupContactFromUI.Birthdate;
            objCon.TRAV_Broker__c = backupContactFromUI.TRAV_Broker__c;
        }
        
        //Fill other information
        database.insert(objCon);
        system.debug('**Contact created**'+objCon.Id);
        contactId = objCon.id;
        //Create Account Contact Relationship
        createAccountContactRelationship(wrapperRes, contactId);
         if(strObjId.startsWith('006')){
                createOpportunityContactRole(objCon.id,strObjId);
            }
        else if(strObjId.startsWith('001')){
                createAccountContactRole(objCon.id,strObjId);
            }
        return contactId;
    }  
    /**
* This method will update contacts in MDM
* @param String strMDMID,String strRequestBody
* @return void
*/ 
    @future (callout = true)
    public static void updateContactInMDM(String strMDMID,String strRequestBody ){
        final string METHODNAME = 'updateContactInMDM';
        //Create the request payload, this should contain MDM Id for update API to be called
        try{
        //Send request and analyse response
        Map<String,String> mapParameters = new Map<String,String>{'agncyIndId' => strMDMID };
            HTTPResponse responseResult = RequestResponseHandler.constructEndPointAndDoCallout(Constants.CONTACT_UPDATE_API, mapParameters, strRequestBody,false);
        //Analyse Response
        system.debug('**Response from Update API**'+responseResult.getBody());
        if(responseResult.getStatusCode() == 400 || responseResult.getStatusCode() == 201 ){
            //Log Exception in Salesforce
            ContactSearchAndPullResponseWrapper wrapperCS = parseResponseResult(responseResult);
            ExceptionUtility.logIntegrationerror('ContactSearchPullUtility', METHODNAME,wrapperCS.Message);
        }
        }catch(Exception objExp){
            ExceptionUtility.logApexException('ContactSearchPullUtility', METHODNAME, objExp, null);
            
        }
    }
    /**
* This method will create Contact on Create API Call
* @param void
* @return ContactSearchAndPullRetrieveWrapper wrapperRes, String strMDMID,String strObjId
*/ 
    public static void createContactInSFOnCreate(ContactSPCreateWrapper wrapperRes,String strObjId,String strMDMID){
        try{
        system.debug('**CONTACT WRAPPER**'+json.serialize(wrapperRes));
        system.debug('**MDM ID AND OBJ ID**'+strMDMID+'**'+strObjId);
        Id contactId = null;
        //Retrieve recordtypeid by developer name
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('TRAV_Agency_Non_Licensed_Contact').getRecordTypeId();
        Contact objCon = new Contact();
        //Check if object is Account / Opportunity
        if(strObjId.startsWith('001')){
            //Account
            
                //objCon.TRAV_Agency_Broker__c = Id.ValueOf(strObjId);
                objCon.AccountId = Id.ValueOf(strObjId);
        }
        else if(strObjId.startsWith('006')){
            //Opportunity
            //objCon.TRAV_Agency_Broker__c = [SELECT Id,TRAV_Agency_Broker__c FROM Opportunity where id = :strObjId LIMIT 1].TRAV_Agency_Broker__c;
            objCon.AccountId =  [SELECT Id,TRAV_Agency_Broker__c FROM Opportunity where id = :strObjId LIMIT 1].TRAV_Agency_Broker__c;
        }
        objCon.RecordTypeId = recordTypeId;
        objCon.FirstName = wrapperRes.fstNm;
        objCon.LastName = wrapperRes.lstNm;
        objCon.Email =  wrapperRes.busEmailAddr;
        objCon.TRAV_SRC_ID__c = wrapperRes.srcId;
        objCon.TRAV_GU_Id__c = wrapperRes.srcId;
        objCon.Phone = wrapperRes.busTelNbr;
        objCon.TRAV_Source_Sys_Code__c = 'SFSC';
        objCon.TRAV_External_Id__c = strMDMID;
        objCon.TRAV_Agent_Preferences__c = backupContactFromUI.TRAV_Agent_Preferences__c;
        objCon.MobilePhone= backupContactFromUI.MobilePhone;
        objCon.Fax = backupContactFromUI.Fax;
        objCon.TRAV_Job_Title__c = backupContactFromUI.TRAV_Job_Title__c;
        objCon.Birthdate = backupContactFromUI.Birthdate;
        objCon.TRAV_Broker__c = backupContactFromUI.TRAV_Broker__c;
        //Fill other information
        
            try{
                database.insert(objCon);
                /*if(Test.isRunningTest()){
                    throw new DMLException('Coverage');
                }*/
            }catch(Exception objEx){
                errorProcessing = true;
                system.debug('**Exception at line**'+objEx.getLineNumber()+objEx.getMessage());
                ExceptionUtility.logIntegrationerror('ContactSearchPullUtility', 'createContactInSFOnCreate', 'Agency Lookup error');
            }
            
        
        contactId = objCon.id;
        system.debug('**Contact created**'+json.serialize(objCon));
            if(strObjId.startsWith('006')){
                createOpportunityContactRole(objCon.id,strObjId);
            }
            else if(strObjId.startsWith('001')){
                system.debug('**Contact and Account Id**'+objCon.id+strObjId);
                createAccountContactRole(objCon.id,strObjId);
            }
        } catch(Exception objEx){
            system.debug('*Ex*'+objEx.getLineNumber()+'*'+objEx.getMessage());
            errorProcessing = true;
            
        }
    }
    /**
* This method will create Contact OPPORTUNITY ROLE
* @param void
* @return Id idContact, Id idOppty
*/ 
    public static void createOpportunityContactRole(Id idContact, Id idOppty){
        system.debug('**Ids retrieved**'+idContact+'**oppty id**'+idOppty);
        List<OpportunityContactRole> listobjOCRoleRec = new List<OpportunityContactRole>();
        OpportunityContactRole objOCRoleRec = new OpportunityContactRole(ContactId = idContact,OpportunityId  = idOppty );
        listobjOCRoleRec.add(objOCRoleRec);
        database.upsert(listobjOCRoleRec);
        system.debug('**OC created**');
    }
    /**
* This method will create Account Contact ROLE
* @param void
* @return Id idContact, Id idAcc
*/ 
    public static void createAccountContactRole(Id idContact, Id idAcc){
        List<AccountContactRole> listobjACRoleRec = new List<AccountContactRole>();
        AccountContactRole objACRoleRec = new AccountContactRole(ContactId = idContact,AccountId  = idAcc );
        listobjACRoleRec.add(objACRoleRec);
        database.upsert(listobjACRoleRec);
        system.debug('**AC created**'+listobjACRoleRec[0].Id);
    }

    /**
* This method will check if Opportunity Contact Role exists
* @param void
* @return Id idContact, Id idOppty
*/ 
    public static Boolean checkContactOpptyExists(Id idContact, Id idOppty){
        final string METHODNAME = 'checkContactOpptyExists';
        Boolean chkContactOpptyRoleExists = false;
        try{
            OpportunityContactRole objOCRoleRec = [SELECT ContactId, OpportunityId, Id FROM OpportunityContactRole WHERE ContactId = :idContact AND OpportunityId = :idOppty LIMIT 1];
            if(objOCRoleRec != null){
                chkContactOpptyRoleExists = true;
            }else{
                chkContactOpptyRoleExists = false;
            }
            if(Test.isRunningTest()){
            throw new DMLException('Coverage');
        }
        }catch(Exception objExp){
            ExceptionUtility.logApexException('ContactSearchPullUtility', METHODNAME, objExp, null);
        }
        return chkContactOpptyRoleExists;
    }
/**
* This method will check if Opportunity Contact Role exists
* @param void
* @return Id idContact, Id idOppty
*/ 
public static Boolean checkContactAccountExists(Id idContact, Id idAcc){
    Boolean chkContactAccRoleExists = false;
    final string METHODNAME = 'checkContactAccountExists';
    try{
        AccountContactRole objACRoleRec = [SELECT ContactId, AccountId, Id FROM AccountContactRole WHERE ContactId = :idContact AND AccountId = :idAcc LIMIT 1];
        if(objACRoleRec != null){
            chkContactAccRoleExists = true;
        }else{
            chkContactAccRoleExists = false;
        }
        if(Test.isRunningTest()){
            throw new DMLException('Coverage');
        }
    }catch(Exception objExp){
        ExceptionUtility.logApexException('ContactSearchPullUtility', METHODNAME, objExp, null);
    }
    return chkContactAccRoleExists;
}
/**
* This method will associate a Cntact to an Account and also create Account Contact Relationships
* @param void
* @return ContactSearchAndPullRetrieveWrapper wrapperContactRetrieve, Id idContact
*/ 
public static void createAccountContactRelationship(ContactSearchAndPullRetrieveWrapper wrapperContactRetrieve, Id idContact){
    final string METHODNAME = 'createAccountContactRelationship';
    try{
    List<String> listBmLocString = getBMLocationList(wrapperContactRetrieve);
    List<AccountContactRelation> listAccContact = new List<AccountContactRelation>();
    if(!listBmLocString.isEmpty()){
        //Associate Account to Contact
        Id idAcc = [SELECT Id FROM Account where TRAV_Account_External_Id__c = :listBmLocString[0] LIMIT 1].Id;
        if(idAcc != null){
            Contact objCon = [SELECT Id FROM Contact WHERE Id = :idContact LIMIT 1];
            objCon.AccountId = idAcc;
            update objCon;
        }
        if(listBmLocString.size()>1){
            listBmLocString.remove(0);
            for(Account objAcc : [SELECT Id FROM Account where TRAV_Account_External_Id__c IN :listBmLocString]){
                AccountContactRelation objAccCont = new AccountContactRelation(AccountId = objAcc.id, ContactId = idContact);
                listAccContact.add(objAccCont);
                
            }
            if(!listAccContact.isEmpty()){
                database.upsert(listAccContact);
            }
        }
    }
        if(Test.isRunningTest()){
            throw new DMLException('Coverage');
        }
    }catch(Exception objExp){
        ExceptionUtility.logApexException('ContactSearchPullUtility', METHODNAME, objExp, null);
    }  
}
/**
* This method will return list of BM Location Ids from wrapper parsed through GET API
* @param List<String>
* @return ContactSearchAndPullRetrieveWrapper wrapperContactRetriev
*/ 
    public static List<String> getBMLocationList(ContactSearchAndPullRetrieveWrapper wrapperContactRetrieve){
        final string METHODNAME = 'getBMLocationList';
        List<String> listBmLocString = new List<String>();
        Integer i = 0;
        try{
            system.debug('**Value**'+wrapperContactRetrieve.BMLoc);
            system.debug('**size**'+wrapperContactRetrieve.BMLoc.size());
            if(wrapperContactRetrieve.BMLoc.size() > 0){
                for(i=0;i<(wrapperContactRetrieve.BMLoc).size();i++){
                    system.debug('**Value**'+wrapperContactRetrieve.BMLoc[i].bmLocId);
                    listBmLocString.add('BM_'+wrapperContactRetrieve.BMLoc[i].bmLocId);
                }
            }
            if(Test.isRunningTest())  {
                throw new DMLException('Coverage');
            }
        }catch(Exception objExp){
            ExceptionUtility.logApexException('ContactSearchPullUtility', METHODNAME, objExp, null);
        }
        return listBmLocString;
    }
}