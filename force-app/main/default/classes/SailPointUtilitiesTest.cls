/**
 * Unit tests for SailPointUtilities class. 
 *
 * Modification Log:
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Ben Climan             US79918              04/08/2020       Original Version
 * Ben Climan             US81170              04/23/2020       General clean-up, moved duplicate test user creation into helper method makeUser         
 * -----------------------------------------------------------------------------------------------------------
 */
@isTest
private without sharing class SailPointUtilitiesTest {
    static final String USER_PROFILE = 'Standard User';
    static final String PUBLIC_GROUP = 'Test Public Group';
    static final String PERMISSION_SET = 'Test Permission Set';
    static integer USER_COUNTER = 0;

    static User makeUser(String userState,
    String userOffice,
    String userTitle,
    String userDepartment,
    String userDivision,
    String userRole,
    String userProfile,
    Boolean userOverrideAutomation) {
        User u = new User();
        String firstName, lastName, email, nickname, alias, username;
        firstName = 'Test';
        lastName = 'User';
        USER_COUNTER++;
        Integer rand = Math.round(Math.random() * 10000);

        username = email = firstName + USER_COUNTER + '.' + rand + '@travelers.com.test';
        alias = username.length() > 8 ? username.substring(0, 8) : username;
        nickname = 'User' + rand;

        u.FirstName = firstName;
        u.LastName = lastName;
        u.Email = email;
        u.Username = username;
        u.Alias = alias;
        u.CommunityNickname = nickname;
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.TimeZoneSidKey = 'America/New_York';
        u.State = userState;
        u.Title = userTitle;
        u.Department = userDepartment;
        u.Division = userDivision;
        u.TRAV_Office__c = userOffice;
        u.TRAV_Override_Automation__c = userOverrideAutomation;
        
        if (userRole != null) u.UserRoleId = [SELECT Id FROM UserRole WHERE Name = :userRole].get(0).Id;

        String profileName = userProfile == null ? USER_PROFILE : userProfile;   
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = :profileName].get(0).Id;

        return u;
    }

    @testSetup static void makeTestData(){
        // Make a Public Group
        Group pg = new Group(Name=PUBLIC_GROUP, DeveloperName='Test_Public_Group', Type='Regular');
        Database.SaveResult pgResult = Database.insert(pg);

        // Make a Permission Set
        PermissionSet ps = new PermissionSet(Label=PERMISSION_SET, Name='Test_Permission_Set');
        Database.SaveResult psResult = Database.insert(ps);

        // Make a User
        insert makeUser('HI', null, '2VP', 'BI&A', 'BOND_SPECIALTY_INS', null, null, false);    
    }

    @isTest static void testGetUsersWithSPFields() {
        List<User> users = [SELECT Id FROM User];
        List<User> usersWithFields = SailPointUtilities.getUsersWithSPFields(users);

        System.assert(usersWithFields.get(0).getPopulatedFieldsAsMap().size() > 1, 'Query did not retrieve expected fields');
    }

    @isTest static void testGetRole() {
        UserRole role = SailPointUtilities.getRole('BSI MD');
        System.assert(role != null, 'Query did not retrieve any Roles'); 
    }

    @isTest static void testGetProfile() {
        Profile p = SailPointUtilities.getProfile(USER_PROFILE);
        System.assert(p != null, 'Query did not retrieve any Profiles');
    }

    @isTest static void testGetPermissionSet() {
        PermissionSet pset = SailPointUtilities.getPermissionSet('Financial Services Cloud Standard');
        System.assert(pset != null, 'Query did not retrieve any Permission Sets');
    }

    @isTest static void testGetPublicGroup() {
        Group g = SailPointUtilities.getPublicGroup('Test Public Group');
        System.assert(g != null, 'Query did not retrieve any Public Groups');
    }

    @isTest static void testGetSailPointDerivationRules() {
        List<SailPoint_Derivation_Rule__mdt> rules = SailPointUtilities.getSailPointDerivationRules();
        System.assert(rules.size() > 0, 'Query did not retrieve any SailPoint_Derivation_Rule__mdt records');
    }

    /**
     * POSITIVE test case for evaluating a derivation rule. 
     */
    @isTest static void testEvaluateRulePos() {
        List<SailPoint_Derivation_Rule__mdt> rules = new List<SailPoint_Derivation_Rule__mdt>();
        SailPoint_Derivation_Rule__mdt fieldDerivationRule = new SailPoint_Derivation_Rule__mdt();
        fieldDerivationRule.MasterLabel = 'FieldDerivationRule';
        fieldDerivationRule.DeveloperName = 'FieldDerivationRule';
        fieldDerivationRule.Field_To_Set__c = 'ProfileId';
        fieldDerivationRule.Is_Active__c = true;
        fieldDerivationRule.Set_On_Trigger__c = 'Insert or Update';
        fieldDerivationRule.User_State__c = 'HI';
        fieldDerivationRule.Value_To_Set__c = 'System Administrator';
        rules.add(fieldDerivationRule);

        SailPoint_Derivation_Rule__mdt assignmentDerivationRule = new SailPoint_Derivation_Rule__mdt();
        assignmentDerivationRule.MasterLabel = 'AssignmentDerivationRule';
        assignmentDerivationRule.DeveloperName = 'AssignmentDerivationRule';
        assignmentDerivationRule.Is_Active__c = true;
        assignmentDerivationRule.Set_On_Trigger__c = 'Insert or Update';
        assignmentDerivationRule.User_State__c = 'HI';
        assignmentDerivationRule.User_Division__c = 'BOND_SPECIALTY_INS';
        assignmentDerivationRule.Assign_User_To__c = 'Public Group';
        assignmentDerivationRule.Value_To_Set__c = PUBLIC_GROUP;
        rules.add(assignmentDerivationRule);

        List<User> users = [SELECT Id FROM User WHERE Username LIKE '%travelers.com.test'];
        User userWithFields = SailPointUtilities.getUsersWithSPFields(users).get(0);

        Integer trueFieldDerivation = SailPointUtilities.evaluateRule(userWithFields, fieldDerivationRule);
        Integer trueAssignmentDerivation = SailPointUtilities.evaluateRule(userWithFields, assignmentDerivationRule);

        System.assert(trueFieldDerivation > 0, 'Field derivation rule evaluated to false, expected true');
        System.assert(trueAssignmentDerivation > 0, 'Assignment derivation rule evaluated to false, expected true');
    }

    /**
     * NEGATIVE test case for evaluating a derivation rule.
     */
     @isTest static void testEvaluateRuleNeg() {
        SailPoint_Derivation_Rule__mdt rule = new SailPoint_Derivation_Rule__mdt();
        rule.MasterLabel = 'TestSetField';
        rule.DeveloperName = 'TestFieldToSet';
        rule.Field_To_Set__c = 'ProfileId';
        rule.Is_Active__c = true;
        rule.Set_On_Trigger__c = 'Insert or Update';
        rule.User_State__c = 'CT';
        rule.Value_To_Set__c = USER_PROFILE;
        List<User> users = [SELECT Id FROM User WHERE Username LIKE '%travelers.com.test'];
        User userWithFields = SailPointUtilities.getUsersWithSPFields(users).get(0);
        Integer falseFieldDerivation = SailPointUtilities.evaluateRule(userWithFields, rule);

        System.assert(falseFieldDerivation == 0, 'Field derivation rule evaluated to false, expected true');
     }

    /**
     * Test case for validating SailPointUtilities.setFieldValue() for a String type User record field.
     */
     @isTest static void testSetFieldValue() {
        List<User> users = [SELECT Id FROM User WHERE Username LIKE '%travelers.com.test'];
        User userWithFields = SailPointUtilities.getUsersWithSPFields(users).get(0);
        Object o = SailPointUtilities.setFieldValue('TimezoneSidKey', 'America/Adak');

        System.assert(o instanceof String, 'Type of field value not set to String, expected String');
     }

    /**
    * Test case for validating SailPointUtilities.setFieldValue() for all non-String types for User record fields.
    * Currently there are no Date or DateTime fields that can be set on the User object so this test only covers
    * Currency (DOUBLE), Number (DOUBLE), and Checkbox (BOOLEAN).
    */
    @isTest static void testSetFieldValue2() {
        List<User> users = [SELECT Id FROM User WHERE Username LIKE '%travelers.com.test'];
        User userWithFields = SailPointUtilities.getUsersWithSPFields(users).get(0);
        Object d = SailPointUtilities.setFieldValue('TRAV_Q1_Budget__c', '500.50');
        Object i = SailPointUtilities.setFieldValue('TRAV_Agency_Visit_Goal__c', '25');
        Object b = SailPointUtilities.setFieldValue('TRAV_Manager__c', 'true');

        System.assert(d instanceof Double, 'Type of field value not set to Double, expected Double');
        System.assert(i instanceof Double, 'Type of field value not set to Double, expected Double');
        System.assert(b instanceof Boolean, 'Type of field value not set to Boolean, expected Boolean');
    }

    /**
     * Test case for validating SailPointUtilities.setFieldValue() for an Id (reference) type User record field.
     */
    @isTest static void testSetFieldValueRef() {
        List<User> users = [SELECT Id FROM User WHERE Username LIKE '%travelers.com.test'];
        User userWithFields = SailPointUtilities.getUsersWithSPFields(users).get(0);
        Id roleId = (Id)SailPointUtilities.setFieldValue('UserRoleId', 'BSI AE');
        Id profileId = (Id)SailPointUtilities.setFieldValue('ProfileId', USER_PROFILE);
        UserRole r = [SELECT Id FROM UserRole WHERE Name = 'BSI AE'].get(0);
        Profile p = [SELECT Id FROM Profile WHERE Name = :USER_PROFILE].get(0);

        System.assertEquals(r.Id, roleId, 'Expected UserRoleId to be set to ' + r.Id + ' but got ' + roleId);
        System.assertEquals(p.Id, profileId, 'Expected ProfileId to be set to ' + p.Id + ' but got ' + profileId);
     }

    /**
     * Test case to verify that User.UserRoleId is set as expected. 
     */
    @isTest static void testDeriveFieldsRole() {
        String qry = 'SELECT Id, LastName, IsActive, UserRoleId FROM User WHERE Username LIKE ' + '\'%travelers.com.test\'';
        List<User> users = (List<User>)Database.query(qry);
        List<User> usersWithFields = SailPointUtilities.getUsersWithSPFields(users);
        User originalUser = usersWithFields[0];
        User sailpointUser  = [SELECT Id FROM User WHERE Username LIKE 'sailpoint_integration%'].get(0);
        makeCustomSetting();

        System.runAs(sailpointUser){
            Test.startTest();
            update originalUser;
            Test.stopTest();
         }
        
        List<User> updatedUsers = (List<User>)Database.query(qry);
        User updatedUser = updatedUsers[0];

        UserRole role = [SELECT Name FROM UserRole WHERE Id = :updatedUser.UserRoleId];

        System.assert(originalUser.IsActive, 'Original User.IsActive should be true, but got false');
        System.assertEquals(role.Id, updatedUser.UserRoleId, 'Expected updatedUser.UserRoleId to be ' + role.Name + ' but got ' + updatedUser.UserRoleId);
     }

    /**
     * Test case to verify that if no rules match for a User for setting UserRoleId,
     * during a BEFORE UPDATE operation, the User is deactivated. 
     */
    @isTest static void testUserDeactivation() {
        String qry = 'SELECT Id, LastName, IsActive, UserRoleId FROM User WHERE Username LIKE ' + '\'%travelers.com.test\'';
        List<User> users = (List<User>)Database.query(qry);
        List<User> usersWithFields = SailPointUtilities.getUsersWithSPFields(users);
        User originalUser = usersWithFields[0];
        User sailpointUser  = [SELECT Id FROM User WHERE Username LIKE 'sailpoint_integration%'].get(0);
        makeCustomSetting();
        originalUser.Title = 'Salesforce Developer';
        originalUser.Division = 'TrailBlazers team';
        originalUser.Department = null;

        System.runAs(sailpointUser){
            Test.startTest();
            update originalUser;
            Test.stopTest();
         }
        
        List<User> updatedUsers = (List<User>)Database.query(qry);
        User updatedUser = updatedUsers[0];

        System.assert(originalUser.IsActive, 'Original User.IsActive should be true, but got false');
        System.assert(updatedUser.UserRoleId == null, 'Expected Updated User.UserRoleId to be blank, but got ' + updatedUser.UserRoleId);
        System.assert(!updatedUser.IsActive, 'Updated User.IsActive should be false, but got true');
     }

    /**
    * Test method to make a custom setting to ensure that the UserTrigger runs.
    */
    @isTest static void makeCustomSetting() {
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'UserTrigger';
        insert objCustomsetting;
    }
    

    /**
     * Test case that covers E2E execution of the trigger, handler, and utilities for an INSERT (before AND after).
     */
     @isTest static void testInsertE2E() {
         User sailpointUser  = [SELECT Id FROM User WHERE Username LIKE 'sailpoint_integration%'].get(0);
         makeCustomSetting();
         System.runAs(sailpointUser){
            Test.startTest();
            insert makeUser('HI', null, '2VP', 'BI&A', 'BOND_SPECIALTY_INS', null, null, false);
            Test.stopTest();
         }
         User updatedUser = [SELECT Name, TimezoneSidKey, FORMAT(LastModifiedDate) FROM User WHERE Username LIKE '%travelers.com.test' ORDER BY LastModifiedDate DESC].get(0);
         String tzk = updatedUser.TimezoneSidKey;
         List<GroupMember> members = 
         [SELECT UserOrGroupId, Group.Name FROM GroupMember WHERE Group.Type = 'Regular' AND Group.Name = :PUBLIC_GROUP];

         System.assertEquals('America/Adak', tzk, 'Expected User.TimezoneSidKey to be America/Adak but got ' + tzk);
         System.assert(members.size() > 0, 'Expected User to be assigned to Group Test Public Group but got ' + members);
     }

    /**
     * Test case that covers E2E execution of the trigger, handler, and utilities for an UPDATE (before AND after).
     */
     @isTest static void testUpdateE2E() {
        User sailpointUser  = [SELECT Id FROM User WHERE Username LIKE 'sailpoint_integration%'].get(0);
        User userToUpdate = [SELECT Id FROM User WHERE Username LIKE '%travelers.com.test'].get(0);
        makeCustomSetting();
        
        System.runAs(sailpointUser){
            Test.startTest();
            update userToUpdate;
            Test.stopTest();
        }
        User u = [SELECT UserRoleId FROM User WHERE Id = :userToUpdate.Id];
        Id roleId = [SELECT Id FROM UserRole WHERE Name = 'BSI Leadership'].get(0).Id;
        Id permSetId = SailPointUtilities.getPermissionSet(PERMISSION_SET).Id;
        List<PermissionSetAssignment> permSetAssignments = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId = :permSetId AND AssigneeId = :u.Id];
        Set<Id> permSetAssignmentIds = new Set<Id>();
        for (PermissionSetAssignment p : permSetAssignments) permSetAssignmentIds.add(p.AssigneeId); 
        
        System.assertEquals(roleId, u.UserRoleId);
        System.assert(permSetAssignmentIds.contains(u.Id), 'Expected User with ID ' + u.Id + ' to be assigned to Permission Set');
    }

    /**
     * Test case to cover handling when User record Override Automation has been set.
     */
    @isTest static void testOverrideTrue() {
        makeCustomSetting();
        User sailpointUser  = [SELECT Id FROM User WHERE Username LIKE 'sailpoint_integration%'].get(0);
        String qry = 'SELECT Id, LastName FROM User WHERE Username LIKE ' + '\'%travelers.com.test\'';
        User userToUpdate = (User)Database.query(qry).get(0);
        userToUpdate.LastName = 'Jones';

        System.runAs(sailpointUser){
            Test.startTest();
            update userToUpdate;
            Test.stopTest();
        }

        User updatedUser = (User)Database.query(qry).get(0);
        
        System.assertEquals('Jones', updatedUser.LastName);
    }

    /**
     * Test case to cover handling when User record Override Automation has been set 
     * AND SailPoint user attempts to set a field that should not be overridden.
     */
    @isTest static void testOverrideFalse() {
        makeCustomSetting();
        User sailpointUser  = [SELECT Id FROM User WHERE Username LIKE 'sailpoint_integration%'].get(0);
        String qry = 'SELECT Id, FirstName FROM User WHERE Username LIKE ' + '\'%travelers.com.test\'';
        User userToUpdate = (User)Database.query(qry).get(0);
        userToUpdate.TRAV_Override_Automation__c = true;
        userToUpdate.FirstName = 'Shouldnotbeset';

        System.runAs(sailpointUser){
            Test.startTest();
            update userToUpdate;
            Test.stopTest();
        }

        User updatedUser = (User)Database.query(qry).get(0);
        
        System.assertEquals('Test', updatedUser.FirstName);
    }

    /**
     * Test case covering a bulk load of 10 Users.
     */
    @isTest static void testBulkInsert() {
        makeCustomSetting();
        User sailpointUser  = [SELECT Id FROM User WHERE Username LIKE 'sailpoint_integration%'].get(0);

        List<User> users = new List<User>(); 
        Id pid = [SELECT Id FROM Profile WHERE Name = :USER_PROFILE].get(0).Id;

        for (Integer i = 0; i < 10; i++) {
            users.add(makeUser('HI', null, '2VP', 'BI&A', 'BOND_SPECIALTY_INS', null, null, false));
        }

        System.runAs(sailpointUser){
            Test.startTest();
            insert users;
            System.debug('Heap limits info: Transaction heap size: ' + Limits.getHeapSize() + ' / max heap size: ' + Limits.getLimitHeapSize());
            System.debug('CPU limits info: Transaction CPU time: ' + Limits.getCpuTime() + ' / max CPU time: ' + Limits.getLimitCpuTime());
            Test.stopTest();
        }
        List<User> testUsers = [SELECT Id FROM User WHERE Username LIKE '%travelers.com.test'];
        System.debug(testUsers);
        List<AggregateResult> insertedUserQry = [SELECT COUNT(Id) FROM User WHERE Username LIKE '%travelers.com.test'];
        Integer userCount = (Integer)insertedUserQry.get(0).get('expr0'); 

        System.assert(userCount >= 10, 'Expected 10 Users to be inserted, but got ' + userCount);
    }

    /**
     * Test case to validate business unit assignment based on profile value.
     */
     @isTest static void testBusinessUnitAssignment() {
        User sailpointUser  = [SELECT Id FROM User WHERE Username LIKE 'sailpoint_integration%'].get(0);
        User u = makeUser('HI', null, '2VP', 'BI&A', 'BUSINESS_INTL_INS', null, null, false);
         makeCustomSetting();
         System.runAs(sailpointUser){
            Test.startTest();
            insert u;
            Test.stopTest();
         }
         User updatedUser = [SELECT Name, TRAV_BU__c, Profile.Name, FORMAT(LastModifiedDate) FROM User WHERE Username LIKE '%travelers.com.test' ORDER BY LastModifiedDate DESC].get(0);
         List<User> testUsers = [SELECT Name, TRAV_BU__c, Profile.Name, FORMAT(LastModifiedDate) FROM User WHERE Username LIKE '%travelers.com.test' ORDER BY LastModifiedDate DESC];
         System.debug(testUsers);
         for (User testUser : testUsers) System.debug(testUser.Profile.Name);
         System.debug(updatedUser);
         String bu = updatedUser.TRAV_BU__c;
         System.assertEquals('BI-NA', bu, 'Expected User.TRAV_BU__c to be BI-NA but got ' + bu);
     }

     @isTest static void testInsert2() {
        User spUser  = [SELECT Id FROM User WHERE Username LIKE 'sailpoint_integration%'].get(0);
        Integer rand = Math.round(Math.random() * 10000);
        User u = makeUser('KY', null, '"Managing Dir, Bond & SI"', null, 'BOND_SPECIALTY_INS', null, null, false);
        makeCustomSetting();
        System.runAs(spUser){
            Test.startTest();
            insert u;
            Test.stopTest();
         }
         List<User> usersWithFields = SailPointUtilities.getUsersWithSPFields(new List<User>{u});
         User userWithFields = usersWithFields[0];
         Map<String, Object> uFields = userWithFields.getPopulatedFieldsAsMap();
         for (String f : uFields.keySet()) {
             System.debug(f + ' ' + uFields.get(f));
         }
        Profile p = [SELECT Name FROM Profile WHERE Id = :userWithFields.ProfileId];
        UserRole r = [SELECT Name FROM UserRole WHERE Id = :userWithFields.UserRoleId];

         System.assertEquals('BSI - Support', p.Name); // User shouldn't match any rules, so default profile (BSI - Support) should be used
         System.assertEquals('BSI MD', r.Name); // User should match rule because Title includes "Managing Dir"
     }

     @isTest static void testInsert3() {
        User spUser  = [SELECT Id FROM User WHERE Username LIKE 'sailpoint_integration%'].get(0);

        User u = makeUser('LA', null, 'Learning Business Liaison', 'Identity & Access Mngmnt', 'BOND_SPECIALTY_INS', null, null, false);

        makeCustomSetting();
        System.runAs(spUser){
            Test.startTest();
            insert u;
            Test.stopTest();
         }
         List<User> usersWithFields = SailPointUtilities.getUsersWithSPFields(new List<User>{u});
         User userWithFields = usersWithFields[0];

        Profile p = [SELECT Name FROM Profile WHERE Id = :userWithFields.ProfileId];
        UserRole r = [SELECT Name FROM UserRole WHERE Id = :userWithFields.UserRoleId];

         System.assertEquals('BSI - Support', p.Name); // User shouldn't match any rules, so default profile (BSI - Support) should be used
         System.assertEquals('BSI Service Partner', r.Name); // User should match rule because Title includes "Managing Dir"
     }


    /**
     * Test case to confirm that User.TimezoneSidKey is not defaulted in an UPDATE operation.
     */
     @isTest static void testTimezoneNoDefault() {
        User spUser  = [SELECT Id FROM User WHERE Username LIKE 'sailpoint_integration%'].get(0);
        User u = makeUser('CT', null, 'Learning Business Liaison', 'Identity & Access Mngmnt', 'BOND_SPECIALTY_INS', null, null, false);
        u.TimezoneSidKey = 'America/Chicago';
        insert u;
        makeCustomSetting();
        System.runAs(spUser){
            Test.startTest();
            u.State = 'HI';
            update u;
            Test.stopTest();
         }
         List<User> usersWithFields = SailPointUtilities.getUsersWithSPFields(new List<User>{u});
         User userWithFields = usersWithFields[0];

         System.assertEquals('America/Chicago', u.TimezoneSidKey);
     }

    /**
     * Verify that a single MDT record can be used to perform multiple Public Group or Permission Set assignments.
     */
    //@isTest static void testAssignMultiple() { }

}
