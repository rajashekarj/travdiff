/**
* Apex Controller Class for GenericLookup Lightning Component. This class will have logic to search records.
* Modification Log    :
* ------------------------------------------------------------------------------------------------------
* Developer              	User Story/Defect    Date             Description
* ------------------------------------------------------------------------------------------------------
* Tejal Pradhan				US68408              09/30/2019       Original Version
*/
public class GenericLookupController {
    
    /**
* This method returns the records searched for a particular sobject 
* @param strObjectName - name of the object to search
* @param strSearchFieldName - name of the field to search in query
* @param strReturnFieldName - name of the field to return 
* @param strLimit - set query limit
* @param strFieldToSearch - api name of the field to search 
* @param strSearchText - search text string
* @param strRecordTypeName - record type name of the object - if applicable
* @param strParentRecordId - parent record id - if applicable
* @return String - returns wrapper as a string .
*/
    @AuraEnabled (cacheable=true)
    public static String searchSobjectRecords(String strObjectName, String strSearchFieldName, String strReturnFieldName,Integer strLimit,String strFieldToSearch,String strSearchText,String strRecordTypeName,String strParentRecordId ){
        
        strSearchText='\'%' + String.escapeSingleQuotes(strSearchText.trim()) + '%\'';
        String query;
        //Form query for Contact object
        if(strObjectName == 'Contact'){
            query = searchContactRecords(strSearchFieldName,strReturnFieldName,strLimit,strFieldToSearch,strSearchText,strRecordTypeName,strParentRecordId);
        }
        else if (strObjectName == 'Account'){
            query = searchAccountRecords(strSearchFieldName,strReturnFieldName,strLimit,strFieldToSearch,strSearchText,strRecordTypeName,strParentRecordId);
        }
        
        System.debug('--query ' +query);
        
        List<sObject> sobjList = Database.query(query);
        List<ResultWrapper> listResults = new List<ResultWrapper>();
        
        if(!sobjList.isEmpty()){
            for(SObject objRec : sobjList){
                ResultWrapper objResult = new ResultWrapper();
                objResult.objName = strObjectName;
                objResult.text = String.valueOf(objRec.get(strSearchFieldName)) ;
                objResult.val = String.valueOf(objRec.get(strReturnFieldName))  ;
                listResults.add(objResult);
            } 
        }
        
        
        return JSON.serialize(listResults) ;
    }
    /**
* This method returns the query formed for COntact object 
* @param strObjectName - name of the object to search
* @param strSearchFieldName - name of the field to search in query
* @param strReturnFieldName - name of the field to return 
* @param strLimit - set query limit
* @param strFieldToSearch - api name of the field to search 
* @param strSearchText - search text string
* @param strRecordTypeName - record type name of the object - if applicable
* @param strParentRecordId - parent record id - if applicable
* @return String - returns the query formed .
*/
    @AuraEnabled (cacheable=true)
    public static String searchContactRecords(String strSearchFieldName, String strReturnFieldName,Integer strLimit,String strFieldToSearch,String strSearchText,String strRecordTypeName,String strParentRecordId ){
        String query;
        Id recordTypeId;
        
        query = 'SELECT '+strSearchFieldName+' ,'+strReturnFieldName+
            ' FROM Contact ' +
            ' WHERE '+strFieldToSearch+' LIKE '+strSearchText;
        
        if(!String.isEmpty(strRecordTypeName)){
            String[] recordTypeNameList = strRecordTypeName.split('\\;');
            List<Id> recordTypeIdList = new List<Id>();
     
            for(String strRecName : recordTypeNameList){
                if(strRecName == Constants.agencyLicensedRecordType){
                    recordTypeId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(Constants.agencyLicensedRecordType).getRecordTypeId();
                    recordTypeIdList.Add(recordTypeId);
                }
                
                if(strRecName == Constants.agencyNonLicensedRecordType){
                    recordTypeId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(Constants.agencyNonLicensedRecordType).getRecordTypeId();
                    recordTypeIdList.Add(recordTypeId);
                } else if(strRecName == Constants.CustomerRecordType){
                    recordTypeId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(Constants.CustomerRecordType).getRecordTypeId();
                    recordTypeIdList.Add(recordTypeId);
                } else if(strRecName == Constants.TPARcordType){
                    recordTypeId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(Constants.TPARcordType).getRecordTypeId();
                    recordTypeIdList.Add(recordTypeId);
                }        
            }
        
            Integer numberOfRecordTypes = recordTypeIdList.size();
            if(numberOfRecordTypes > 1){
                query += ' AND recordTypeId IN (';
                
                for (Integer i = 0; i < numberOfRecordTypes; i++) {
                    query += '\'' + recordTypeIdList[i] +'\'';

                    if (i < (numberOfRecordTypes-1)) {
                        query += ',';
                    }
                }
                
                query += ')';
            } else {
                query += ' AND recordTypeId = \'' + recordTypeId + '\''; 
            }
        }
        
        if(!String.isEmpty(strParentRecordId) && strRecordTypeName != Constants.TPARcordType){
            query += ' AND ' +'ID IN (SELECT ContactId FROM AccountContactRelation WHERE AccountId = '+'\'' +strParentRecordId+'\')';
        }
        
        query += ' LIMIT '+strLimit;
        System.debug('query ' + query);
        return query;
        
    }
    /**
* This method returns the query formed for COntact object 
* @param strObjectName - name of the object to search
* @param strSearchFieldName - name of the field to search in query
* @param strReturnFieldName - name of the field to return 
* @param strLimit - set query limit
* @param strFieldToSearch - api name of the field to search 
* @param strSearchText - search text string
* @param strRecordTypeName - record type name of the object - if applicable
* @param strParentRecordId - parent record id - if applicable
* @return String - returns the query formed .
*/
    @AuraEnabled (cacheable=true)
    public static String searchAccountRecords(String strSearchFieldName, String strReturnFieldName,Integer strLimit,String strFieldToSearch,String strSearchText,String strRecordTypeName,String strParentRecordId ){
        System.debug('----search---');
        String query;
        Id tpaLocationRecordTypeId;
        Id tpaOrganizationRecordTypeId;
        Id agencyLocationRecordTypeId;
        query = 'SELECT '+strSearchFieldName+' ,'+strReturnFieldName+
            ' FROM Account ' +
            ' WHERE '+strFieldToSearch+' LIKE '+strSearchText;
        
        if(strRecordTypeName == 'TPA'){
            tpaLocationRecordTypeId = SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Constants.TPALOCATIONACCOUNT).getRecordTypeId();
            tpaOrganizationRecordTypeId = SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Constants.TPAORGANIZATIONACCOUNT).getRecordTypeId();
        }
        
        if(strRecordTypeName == 'Agency'){
            agencyLocationRecordTypeId = SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Constants.DEVNAMEBRICKANDMOTAR).getRecordTypeId();
        }
        
        if(!String.isEmpty(strRecordTypeName)){
            if(strRecordTypeName == 'TPA'){
                query += ' AND ' +'(recordTypeId = '+'\'' + tpaLocationRecordTypeId +'\'' + ' OR ' +'recordTypeId = '+'\'' + tpaOrganizationRecordTypeId +'\')';
            }
            else if(strRecordTypeName == 'Agency'){
                query += ' AND ' +'recordTypeId = '+'\'' + agencyLocationRecordTypeId +'\'';
                
            }
        }
        
        
        query += ' LIMIT '+strLimit;
        System.debug('query ' + query);
        return query;
        
    }
    /**
* Wrapper class declaration
*/    
    public class ResultWrapper{
        public String objName {get;set;}
        public String text{get;set;}
        public String val{get;set;}
    }
}