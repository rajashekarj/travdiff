/***
 * This class is used as a Selector Layer for Account object. 
 * All queries for Account object will be performed in this class.
 *
 * @Author Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Pranil Thubrikar       US59673              05/13/2019       Original Version
 * Shashank Agarwal       US59536              06/10/2019		Added method getOpportunityNamingConventionAccnts
 * Shashank Agarwal       US65899              08/19/2019       Added method getInternalAccount
 * Shashank Agarwal       US71415			   11/06/2019       Added method getSandPFields	
 * -----------------------------------------------------------------------------------------------------------
 */ 
public with sharing class AccountSelector { 
    
    /**
     * This is a method is used to get list account based on certified business id
     * @param set<string> -set certifedbusniess ids
     * @return List<Account> List of accounts based on the SOQL query provided as an input parameter.
     */
    public static List<Account> getCertifiedBusinessAccnts(set<string> strCerfiedBusinessIDs){
    
        List<Account> lstAccount = new List<Account>();
        
        try{
            lstAccount = new List<Account>([
                                SELECT 
                                    Id,
                                    TRAV_Certified_Business_Id__c
                                FROM 
                                    Account
                                WHERE 
                                    TRAV_Certified_Business_Id__c in :strCerfiedBusinessIDs	
                        ]);
			if(Test.isRunningTest()){
                throw new QueryException();
            }
        } //End - try
        catch(QueryException exp){
            //exception handling code
            
        } //End - catch

      return lstAccount;
     
    } //End of the Method
    
     /**
     * This is a method is used to get list account 
     * @param Set<String> -Set Account Ids
     * @return Map<Id,Account> Map of accounts based on the SOQL query provided as an input parameter.
     */
    
    public static Map<Id,Account> getOpportunityNamingConventionAccnts(Set<Id> setAccountIds){
    
        Map<Id,Account> mapAccount = new Map<Id,Account>();
        
        try{
            mapAccount = new Map<Id,Account>([
                                SELECT 
                                    Id,
                                    Name
                                FROM 
                                    Account
                                WHERE 
                                    Id IN : setAccountIds	
                        ]);
        } //End - try
        catch(QueryException exp){
            //exception handling code
            
        } //End - catch

      return mapAccount;
     
    } //End of the Method
	
	 /**
* This is a method is used to get Account With Name Internal Account
* @param no params
* @return Account record based on the SOQL query provided as an input parameter.
*/
    public static Account getInternalAccount() {
        final string METHODNAME = 'getInternalAccount';
        final string CLASSNAME = 'AccountSelector';
        Account objInternalAccount = new Account();
        try {
            objInternalAccount = [
                                SELECT 
                                    Id
                                FROM 
                                    Account
                                WHERE 
                                    Name = :Constants.InternalAccountName
                                LIMIT 1
                        ];
        } 
        catch(QueryException objExp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
            
        } // End Catch
        return objInternalAccount;
    } //end of method 
    /**
* This is a method is used to get Accounts biling address
* @param set account ids
* @return Account record based on the SOQL query provided as an input parameter.
*/ 
    public static list<Account> getAccountBillidAddress(set<id> Accountids){ 
        return [  select 
                	id,
                	BillingStreet,
                    BillingCity,
                    BillingState,
                    BillingPostalCode,
                    BillingCountry 
                  from 
                    Account 
                   where 
                     id in:Accountids
               ];
    } //end of the methods getAccountBillidAddress
	
	 /**
     * This is a method is used to get list account 
     * @param Set<String> -Set Account Ids
     * @return Map<Id,Account> Map of accounts based on the SOQL query provided as an input parameter.
     */
    
    public static Map<Id,Account> getSandPFields(Set<Id> setAccountIds){
    
        Map<Id,Account> mapAccount = new Map<Id,Account>();
        
        try{
            mapAccount = new Map<Id,Account>([
                                SELECT 
                                    Id,
                                    Name,
                					TRAV_AM_Disciplinary_History__c,
                					TRAV_AM_Discretionary_Accounts__c,
                					TRAV_AM_Discretionary_AUM__c,
                					TRAV_AM_Non_Discretionary_Accounts__c,
                					TRAV_AM_Non_Discretionary_AUM__c,
                					FinServ__AUM__c,
                					TRAV_Banks_TRV_Risk_Score__c,
                					TRAV_Banks_Total_Assets__c,
                					TRAV_Banks_Net_Income__c,
                					TRAV_Banks_Const_and_Land_Dev_Loans_RBC__c,
                					TRAV_Banks_ROA_LTM__c,
                					TRAV_Banks_Reg_Commercial_Real_Estate__c,
                					TRAV_Banks_Texas_Ratio__c,
                					TRAV_Banks_Non_Performing_Loan_Ratio__c,
                					TRAV_Banks_Leverage_Ratio__c,
                					TRAV_Banks_Total_Loans__c,
                					TRAV_Banks_Total_Deposits__c,
                					TRAV_SNL_Address__c,
                					TRAV_SNL_Company_Name__c,
                					TRAV_SNL_ID__c,
                					TRAV_SNL_Latest_Updated_Date__c,
                					TRAV_SNL_No_of_Employees__c,
                					TRAV_SNL_Institution_Key_Identifier__c,
                					TRAV_SNL_Parent_Name__c,
                					TRAV_SNL_Website__c,
                					TRAV_SNL_Years_in_Operation__c,
                					TRAV_Institution_Type__c,
                					TRAV_IC_ACL_RBC_Ratio__c,
                					TRAV_IC_AM_Best_Rating__c,
                					TRAV_IC_AM_Best_Rating_Date__c,
                					TRAV_IC_Capital_Surplus__c,
                					TRAV_IC_Combined_Ratio__c,
                					TRAV_IC_DWP__c,
                					TRAV_IC_Net_Income__c,
                					TRAV_IC_Net_Underwriting_Profit_Loss__c,
                					TRAV_IC_Total_Assets__c
                                FROM 
                                    Account
                                WHERE 
                                    Id IN : setAccountIds	
                        ]);
        } //End - try
        catch(QueryException exp){
            //exception handling code
            ExceptionUtility.logApexException('AccountSelector', 'getSandPFields', exp, null);
        } //End - catch

      return mapAccount;
     
    } //End of the Method

} //End of the class