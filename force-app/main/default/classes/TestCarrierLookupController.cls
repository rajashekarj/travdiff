/**
 * Test class for carrierLookUp Class
 *
 * @Author : Derek Manierre
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Derek Manierre         US62418              07/02/2019       Test class for carrierLookUpController class
 * -----------------------------------------------------------------------------------------------------------
 */
 
@isTest
private class TestCarrierLookupController {

    static testmethod void testFetchCarrier() {
        
        //Create and insert Carriers
        List<String> carrierNames = new List<String> {'ABC Insurance', 'XYZ Insurance'};
        
        List<TRAV_Carrier__c> carriers = TestDataFactory.createCarriers(carrierNames);
        insert carriers;
        
        List<TRAV_Carrier__c> returnedCarriers = carrierLookUpController.fetchCarrier('ABC Insurance');
        System.assertEquals(returnedCarriers.size(), 1);
        System.assertEquals(returnedCarriers[0].Name, 'ABC Insurance');
        
    }
    
}