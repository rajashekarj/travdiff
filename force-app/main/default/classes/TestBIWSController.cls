/**
 * Test class for BIWSController Class
 *
 * @Author : Derek Manierre
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Derek Manierre                              07/22/2019       Test class for BIWSController class
 * -----------------------------------------------------------------------------------------------------------
 */

@isTest
private class TestBIWSController {
    static testmethod void testGetInfo(){
        
        //Create and insert Account with and withouth IBU
        List<Account> lstAccounts = new List<Account>();
        
        Account testAccountWithIbu = TestDataFactory.createProspect();
        testAccountWithIbu.TRAV_Issuing_Business_Unit_Code__c = '40';
        
        Account testAccountNoIbu = TestDataFactory.createProspect();
        testAccountNoIbu.TRAV_Issuing_Business_Unit_Code__c ='';
        
        lstAccounts.add(testAccountWithIbu);
        lstAccounts.add(testAccountNoIbu);
        insert lstAccounts;
        
        //Create and insert Opportunity with and without SAI
        
        List<Opportunity> lstOpportunities = new List<Opportunity>();
        
        Opportunity testOpportunityWithSai = new Opportunity();
        testOpportunityWithSai.Name = 'Abc Opp';
        testOpportunityWithSai.AccountId = testAccountWithIbu.Id;
        testOpportunityWithSai.StageName = 'Prospect';
        testOpportunityWithSai.CloseDate = System.today() + 30;
        testOpportunityWithSai.TRAV_SAI__c = '123456';
        
        Opportunity testOpportunityNoSai = new Opportunity();
        testOpportunityNoSai.Name = 'Abc Opp';
        testOpportunityNoSai.AccountId = testAccountNoIbu.Id;
        testOpportunityNoSai.StageName = 'Prospect';
        testOpportunityNoSai.CloseDate = System.today() + 30;
        testOpportunityNoSai.TRAV_SAI__c = '';
        
        lstOpportunities.add(testOpportunityWithSai);
        lstOpportunities.add(testOpportunityNoSai);
        insert lstOpportunities;
        
        //Get custom metadata
        CustomUrl__mdt customUrl = [
            Select Environment_Url__c, Environment__c
            From CustomUrl__mdt
            Where MasterLabel = 'BIWS Url'
        ];
        
        List<String> noSaiNoIbuResult = BIWSController.getInfo(testOpportunityNoSai.Id);
        List<String> saiAndIbuResult = BIWSController.getInfo(testOpportunityWithSai.Id);
        
        List<String> noSaiNoIbuExpected = new List<String> {'no value', 'no value', customUrl.Environment_Url__c, customUrl.Environment__c};
        List<String> saiAndIbuExpected = new List<String> {testOpportunityWithSai.TRAV_SAI__c, testAccountWithIbu.TRAV_Issuing_Business_Unit_Code__c, customUrl.Environment_Url__c, customUrl.Environment__c};
            
        System.assertEquals(noSaiNoIbuExpected, noSaiNoIbuResult);
        System.assertEquals(saiAndIbuExpected, saiAndIbuResult);
    }
}