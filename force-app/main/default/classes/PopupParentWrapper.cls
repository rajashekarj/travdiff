/**
* @author Sayanka Mohanty
* @date   12/18/2019
* @description  

Modification Log:
----------------------------------------------------------------------------------------
Developer                   Date                Description
----------------------------------------------------------------------------------------
Sayanka Mohanty           12/18/2019          Original Version
*/
public class PopupParentWrapper {
    @AuraEnabled
    public List<PopupColumnWrapper> columnList;//List of Columns needed
    @AuraEnabled
    public List<List<PopupDataWrapper>> dataDetails{get;set;}
    //Multi-select related wrapper attributes below
    @AuraEnabled
    public Boolean loadPopUpScreen ;//Boolean to check if pop up screen will be displayed or not - added by IS
    @AuraEnabled
    public String headerValue;//Header title of the pop up screen - added by IS
    @AuraEnabled
    public Opportunity opportunityRecord;//Details of the opportunity record - added by IS
    @AuraEnabled
    public List<OpportunityLineItem> listOpportunityProducts; //list of deatils of opportunity products - added by IS 
    @AuraEnabled
    public List<PopupProductWithoutReasonListWrapper> listPopupIterable;
    @AuraEnabled
    public String stagesRequireReason;
    @AuraEnabled
    public String stagesRequireSameStage;
    @AuraEnabled
    public String productWinningError;
    
}