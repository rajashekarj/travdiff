/**
* Batch Class for TRAV_Opportunity_Product_Delete__c. This class will have logic for deleting of TRAV_Opportunity_Product_Delete__c record 
* Modification Log    :
* ------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* Shashank Agarwal		 US					  11/04/2019       Original Version  		
* ------------------------------------------------------------------------------------------------------
*/ 

global class BatchEmptyDeletedProducts implements Database.Batchable<sObject>, Schedulable{
    String strQuery;
    String CLASSNAME ='BatchEmptyDeletedProducts';
    /**
* This method collect the batches of records or objects to be passed to execute
* @param Database.BatchableContext
* @return Database.QueryLocator record
*/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String METHODNAME = 'Start';
        try{
            Integer noOfDays = Integer.ValueOf(Label.DaysToArchiveDeletedProducts);
            DateTime strDateTimeForQuery = DateTime.now().addDays(-noOfDays);
            strQuery = 'Select Id From TRAV_Opportunity_Product_Delete__c WHERE TRAV_Deleted_On__c < :strDateTimeForQuery AND TRAV_Object_API_Name__c ='+'\''+'OpportunityLineItem'+'\'';
            
            return Database.getQueryLocator(strQuery);
        }catch(Exception objException){
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objException, null);
            return null;
        }
    } //End  Method
    
    /**
* This method process each batch of records for the implementation logic
* @param Database.BatchableContext record
* @param List of Opportunity 
* @return void 
*/ 
    global void execute(Database.BatchableContext BC, List<TRAV_Opportunity_Product_Delete__c> scope) {
        String METHODNAME = 'execute';
        
        try{
            if(!scope.isEmpty()){
                Database.DeleteResult[] lstSaveResultForDelete = Database.delete(scope, false);
                
                ExceptionUtility.logDMLError(Constants.OPPORTUNITYSERVICE, METHODNAME, lstSaveResultForDelete);
                if(Test.isRunningTest()) {
                    CalloutException e = new CalloutException();
                    e.setMessage('This is a constructed exception for testing and code coverage');
                    throw e;
                }
            }
        }catch(Exception objException){
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objException, null);
        }
    } // End  Method
    
    /**
* This method execute any post-processing operations test
* @param Database.BatchableContext record 
* @return void 
*/ 
    global void finish(Database.BatchableContext BC) {
        String METHODNAME = 'finish';
        Integer rows = Limits.getLimitQueryRows() - Limits.getQueryRows();
        List<TRAV_Agency_Delete_Tracking__c> lstAgencyTrack = [Select id from TRAV_Agency_Delete_Tracking__c LIMIT :rows];
        try {
            if (lstAgencyTrack.size() > 0)
            delete lstAgencyTrack;
        } catch (Exception objException){
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objException, null);
        }
    }
    
    /**
* This method execute this batch class when scheduled
* @param SchedulableContext record 
* @return void 
*/
    global void execute(SchedulableContext schCon) {
        BatchEmptyDeletedProducts batchObj  = new BatchEmptyDeletedProducts(); 
        Database.executebatch(batchObj);
    }//End Method  
}