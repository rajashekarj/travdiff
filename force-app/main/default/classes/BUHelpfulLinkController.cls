/**
* This class is used as a apex controller for BUHelpfulLink component. 
* 
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Isha Shukla            US63361              07/11/2019        Apex controller for BUHelpfulLink
* Erec Lawrie			 US68407			  10/21/2019		Update controller code to support helpful links by role or profile	
*-----------------------------------------------------------------------------------------------------------
*/
public without sharing class BUHelpfulLinkController {
    /**
    * This is a method is used to get list of metadat records having link 
    * @param null
    * @return List<TRAV_BU_Helpful_Links__mdt> List of metadata
    */
    @AuraEnabled(cacheable=true)
    public static List<TRAV_BU_Helpful_Links__mdt> getBULinks() {       
        //getting current user BU
        User currentUser = [SELECT TRAV_BU__c, Profile.Name, UserRole.Name 
                            FROM User 
                            WHERE Id = :UserInfo.getUserId()
                            LIMIT 1];
        system.debug('User: BU [' + currentUser.TRAV_BU__c + '] Role [' + currentUser.UserRole.Name + '] Profile [' + currentUser.Profile.Name + ']');

        String strUserBU = '%' + Label.TRAV_Default +'%';
        if(currentUser.TRAV_BU__c != null) {
            strUserBU = '%' + currentUser.TRAV_BU__c +'%';
        }
       
        List<TRAV_BU_Helpful_Links__mdt> objUserRoleOrProfileFinalLinksList = new List<TRAV_BU_Helpful_Links__mdt>();
        List<TRAV_BU_Helpful_Links__mdt> objNonRoleOrProfileFinalLinksList = new List<TRAV_BU_Helpful_Links__mdt>();        

        for(TRAV_BU_Helpful_Links__mdt link :[SELECT TRAV_Link__c, MasterLabel, Profile_Name__c, Role__c, TRAV_Sort_Order__c
                                              FROM TRAV_BU_Helpful_Links__mdt 
                                              WHERE TRAV_BU__c LIKE :strUserBU 
                                              ORDER BY TRAV_Sort_Order__c]) {
            if((link.Role__c == currentUser.UserRole.Name) || (link.Profile_Name__c == currentUser.Profile.Name)) {
                objUserRoleOrProfileFinalLinksList.add(link);
            } else {
	            if(link.Role__c == '*' && link.Profile_Name__c == '*') {
                    objNonRoleOrProfileFinalLinksList.add(link);
                }
            }
        }
                
        if (objUserRoleOrProfileFinalLinksList.size() > 0) {
            system.debug('Returning Role/Profile matches');
            return objUserRoleOrProfileFinalLinksList;
        } else {
            system.debug('Returning non-role/non-profile matches');
            return objNonRoleOrProfileFinalLinksList;
        }
    }
}