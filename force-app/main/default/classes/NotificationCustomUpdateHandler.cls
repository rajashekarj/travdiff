/**
*    @author Sayanka Mohanty
*    @date   06/21/2019 
*    @description : This is the Utility class for handling all updates to be done

Modification Log:
------------------------------------------------------------------------------------
Developer              User Story         Date                Description
------------------------------------------------------------------------------------
Sayanka Mohanty                           02/07/2019          Original Version*
Shashank Agarwal       US63739            07/30/2019          Added code for Stewardship due date
Tejal Pradhan	       US74802            03/31/2020          Added code for 'Provide credit rating' task date
*/
public class NotificationCustomUpdateHandler {
    public static Map<String,String> calculateFieldUpdates(String fieldsToBeUpdated,Map<String,String> mergeFieldAPIMap){
        Map<String,String> finalFieldValueMap = new Map<String,String>();
        List<String> fieldValuePairList = new List<String>();
        List<String> singleFieldValuePairList = new List<String>();
        String evaluatedVal = '';
        //Logic for calculations
        if(String.isNotBlank(fieldsToBeUpdated)){
            //First split all Field value pairs(if multiple are present)
            if(fieldsToBeUpdated.contains(';')){
                fieldValuePairList = fieldsToBeUpdated.split(';');
                for(String splittedFieldVal:fieldValuePairList){
                    List<String> sngFieldValuePairList = new List<String>();
                    String evalVal = '';
                    sngFieldValuePairList = splittedFieldVal.split(':'); 
					String fieldAPIName = sngFieldValuePairList[0].replaceAll('(\\s)','');
                    evalVal = evaluateFormula(sngFieldValuePairList[1],mergeFieldAPIMap);
                    
                    finalFieldValueMap.put(fieldAPIName.trim(),evalVal);
                    
                }
            }else{
                //Process for single field value
                singleFieldValuePairList = fieldsToBeUpdated.split(':');
                evaluatedVal = evaluateFormula(singleFieldValuePairList[1], mergeFieldAPIMap);
                finalFieldValueMap.put(singleFieldValuePairList[0],evaluatedVal);
                
            }
            
        }
        system.debug('**Final Field Map**'+finalFieldValueMap);
        system.debug('**Merge Field API Map**'+mergeFieldAPIMap);
        return finalFieldValueMap;
    }
    public static String evaluateFormula(String fieldVal, Map<String,String> mergeFieldAPIMap){
        //Enter all combinations here
        String returnVal = '';
        try{
            if(fieldVal.startsWithIgnoreCase('TODAY')){
                //First check if Today date is required
                if(!fieldVal.contains('+')) {
                    returnVal = String.valueOf(system.today());
                }else{
                    Date dt = system.today();
                    returnVal = String.valueOf(dt.addDays(Integer.valueOf(fieldVal.substringAfter('+'))));
                }
            }/*else if(fieldVal.startsWithIgnoreCase('TTODAY')){
                //First check if Today date is required
                if(!fieldVal.contains('+')) {
                    returnVal = String.valueOf(Datetime.valueOf(system.today()));
                }else{
                    
                    returnVal = String.valueOf(Datetime.valueOf(system.today()).addDays(Integer.valueOf(fieldVal.substringAfter('+'))));
                }
            }*/else if(fieldVal.startsWithIgnoreCase('OPPTYEFF')){
                //First check if Today date is required
                if(!fieldVal.contains('+')) {
                    if(mergeFieldAPIMap.containsKey('TRAV_Policy_Effective_Date__c')){
                        returnVal = String.valueOf((Date.valueOf(mergeFieldAPIMap.get('TRAV_Policy_Effective_Date__c'))));
                    }else if(mergeFieldAPIMap.containsKey('trav_policy_effective_date__c')){
                        returnVal = String.valueOf((Date.valueOf(mergeFieldAPIMap.get('trav_policy_effective_date__c'))));
                    }else if(mergeFieldAPIMap.containsKey('opportunity.trav_policy_effective_date__c')){
                        returnVal = String.valueOf((Date.valueOf(mergeFieldAPIMap.get('opportunity.trav_policy_effective_date__c'))));
                    }
                }else{
                    if(mergeFieldAPIMap.containsKey('TRAV_Policy_Effective_Date__c')){
                        returnVal = String.valueOf((Date.valueOf(mergeFieldAPIMap.get('TRAV_Policy_Effective_Date__c'))).addDays(Integer.valueOf(fieldVal.substringAfter('+'))));
                    }else if(mergeFieldAPIMap.containsKey('trav_policy_effective_date__c')){
                        returnVal = String.valueOf((Date.valueOf(mergeFieldAPIMap.get('trav_policy_effective_date__c'))).addDays(Integer.valueOf(fieldVal.substringAfter('+'))));
                    }
                }
            }else if(fieldVal.startsWithIgnoreCase('TTOPPTYEFF')){
                //First check if Today date is required
                if(!fieldVal.contains('+')) {
                    //Logic not required
                }else{
                    if(mergeFieldAPIMap.containsKey('TRAV_Business_Date_180__c')){
                        returnVal = String.valueOf((Date.valueOf(mergeFieldAPIMap.get('TRAV_Business_Date_180__c'))).addDays(-10));
                    }else if(mergeFieldAPIMap.containsKey('trav_business_date_180__c')){
                        returnVal = String.valueOf((Date.valueOf(mergeFieldAPIMap.get('trav_business_date_180__c'))).addDays(-10));
                    }
                }
            }/*Commenting below as currently not required
              * else if(fieldVal.startsWithIgnoreCase('TOPPTYEFFTIME')){
                //First check if Today date is required
                if(!fieldVal.contains('+')) {
                    //Logic not required
                }else{
                    if(mergeFieldAPIMap.containsKey('TRAV_Policy_Effective_Date__c')){
                        returnVal = String.valueOf(datetime.newInstance((Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Policy_Effective_Date__c'))).year(), (Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Policy_Effective_Date__c'))).month(), (Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Policy_Effective_Date__c'))).day() + Integer.valueOf(fieldVal.substringAfter('+')), 8, 0, 0));
                    
                    }else if(mergeFieldAPIMap.containsKey('trav_policy_effective_date__c')){
                        returnVal = String.valueOf(datetime.newInstance((Datetime.valueOf(mergeFieldAPIMap.get('trav_policy_effective_date__c'))).year(), (Datetime.valueOf(mergeFieldAPIMap.get('trav_policy_effective_date__c'))).month(), (Datetime.valueOf(mergeFieldAPIMap.get('trav_policy_effective_date__c'))).day() + Integer.valueOf(fieldVal.substringAfter('+')), 8, 0, 0));
                    }
                }
                system.debug('**Effective Time**'+returnVal);
                //US63739
            }*/else if(fieldVal.startsWithIgnoreCase('BUSINESSDATE180')){
                if(mergeFieldAPIMap.containsKey('TRAV_Business_Date_180__c')){
                     //returnVal = String.valueOf(Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Business_Date_180__c')));
                     returnVal = String.valueOf(datetime.newInstance((Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Business_Date_180__c'))).year(), (Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Business_Date_180__c'))).month(), (Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Business_Date_180__c'))).day(), 8, 0, 0));
                    
                }else if(mergeFieldAPIMap.containsKey('trav_business_date_180__c')){
                     //returnVal = String.valueOf(Datetime.valueOf(mergeFieldAPIMap.get('trav_business_date_180__c')));
                    returnVal = String.valueOf(datetime.newInstance((Datetime.valueOf(mergeFieldAPIMap.get('trav_business_date_180__c'))).year(), (Datetime.valueOf(mergeFieldAPIMap.get('trav_business_date_180__c'))).month(), (Datetime.valueOf(mergeFieldAPIMap.get('trav_business_date_180__c'))).day(), 8, 0, 0));
                }
                //US63739
            }else if(fieldVal.startsWithIgnoreCase('AFTEREFFECTIVEBUSINESSDATE30')){
                // US73728
                if(mergeFieldAPIMap.containsKey('TRAV_Business_Days_30_After_Effective_Dt__c')){
                     //returnVal = String.valueOf(Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Business_Date_180__c')));
                     returnVal = String.valueOf(datetime.newInstance((Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Business_Days_30_After_Effective_Dt__c'))).year(), (Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Business_Days_30_After_Effective_Dt__c'))).month(), (Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Business_Days_30_After_Effective_Dt__c'))).day(), 8, 0, 0));
                    
                }else if(mergeFieldAPIMap.containsKey('trav_business_days_30_after_effective_dt__c')){
                     //returnVal = String.valueOf(Datetime.valueOf(mergeFieldAPIMap.get('trav_business_date_180__c')));
                    returnVal = String.valueOf(datetime.newInstance((Datetime.valueOf(mergeFieldAPIMap.get('trav_business_days_30_after_effective_dt__c'))).year(), (Datetime.valueOf(mergeFieldAPIMap.get('trav_business_days_30_after_effective_dt__c'))).month(), (Datetime.valueOf(mergeFieldAPIMap.get('trav_business_days_30_after_effective_dt__c'))).day(), 8, 0, 0));
                }
                //US63739
            }else if(fieldVal.startsWithIgnoreCase('30DAYSPRIORINTERNALPLANNING')){
                if(mergeFieldAPIMap.containsKey('TRAV_Business_Date_180__c')){
                     //returnVal = String.valueOf(Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Business_Date_180__c')));
                     returnVal = String.valueOf((Date.valueOf(mergeFieldAPIMap.get('TRAV_Business_Date_180__c'))).addDays(-30));
                    
                }else if(mergeFieldAPIMap.containsKey('trav_business_date_180__c')){
                     //returnVal = String.valueOf(Datetime.valueOf(mergeFieldAPIMap.get('trav_business_date_180__c')));
                    returnVal = String.valueOf((Date.valueOf(mergeFieldAPIMap.get('trav_business_date_180__c'))).addDays(-30));
                }
            }else if(fieldVal.startsWithIgnoreCase('45DAYSPRIORINTERNALPLANNING')){
                if(mergeFieldAPIMap.containsKey('TRAV_Business_Date_180__c')){
                     //returnVal = String.valueOf(Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Business_Date_180__c')));
                     returnVal = String.valueOf((Date.valueOf(mergeFieldAPIMap.get('TRAV_Business_Date_180__c'))).addDays(-45));
                    
                }else if(mergeFieldAPIMap.containsKey('trav_business_date_180__c')){
                     //returnVal = String.valueOf(Datetime.valueOf(mergeFieldAPIMap.get('trav_business_date_180__c')));
                    returnVal = String.valueOf((Date.valueOf(mergeFieldAPIMap.get('trav_business_date_180__c'))).addDays(-45));
                }
            }else if(fieldVal.startsWithIgnoreCase('10DAYSAFTERDELIVERYMEETING')){
                if(mergeFieldAPIMap.containsKey('TRAV_Business_Days_30_After_Effective_Dt__c')){
                     //returnVal = String.valueOf(Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Business_Date_180__c')));
                     returnVal = String.valueOf((Date.valueOf(mergeFieldAPIMap.get('TRAV_Business_Days_30_After_Effective_Dt__c'))).addDays(10));
                    
                }else if(mergeFieldAPIMap.containsKey('trav_business_days_30_after_effective_dt__c')){
                     //returnVal = String.valueOf(Datetime.valueOf(mergeFieldAPIMap.get('trav_business_date_180__c')));
                    returnVal = String.valueOf((Date.valueOf(mergeFieldAPIMap.get('trav_business_days_30_after_effective_dt__c'))).addDays(10));
                }
            }else if(fieldVal.startsWithIgnoreCase('BUSINESSDATE45')){
               if(mergeFieldAPIMap.containsKey('TRAV_Business_Date_45__c')){
                     //returnVal = String.valueOf(Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Business_Date_45__c')));
                      returnVal = String.valueOf(datetime.newInstance((Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Business_Date_45__c'))).year(), (Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Business_Date_45__c'))).month(), (Datetime.valueOf(mergeFieldAPIMap.get('TRAV_Business_Date_45__c'))).day(), 8, 0, 0));
                
                }else if(mergeFieldAPIMap.containsKey('trav_business_date_45__c')){
                     returnVal = String.valueOf(datetime.newInstance((Datetime.valueOf(mergeFieldAPIMap.get('trav_business_date_45__c'))).year(), (Datetime.valueOf(mergeFieldAPIMap.get('trav_business_date_45__c'))).month(), (Datetime.valueOf(mergeFieldAPIMap.get('trav_business_date_45__c'))).day(), 8, 0, 0));
                } 
            }
            //Added for US74802 - 'Provide credit rating' task due date as Today+10 days
            else if(fieldVal.startsWithIgnoreCase('10DAYSAFTERTASKCREATED')){
                returnVal = String.valueOf((System.today()).addDays(10));
                
            }else if(fieldVal.equalsIgnoreCase('StewardshipRC')){
                Id recordTypeId =
                    Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName()
                    .get('Stewardship').getRecordTypeId();
                returnVal = String.valueOf(recordTypeId);
            }else if(fieldVal.startsWithIgnoreCase('AUTHORITYDATE')){
                	 if(mergeFieldAPIMap.containsKey('trav_authority_need_by_date__c')){
                         returnVal = String.valueOf((Date.valueOf(mergeFieldAPIMap.get('trav_authority_need_by_date__c'))));
                             }
            }
            else{
                //pass value as is
                returnVal = fieldVal;
            }
        }catch(Exception ex){
            system.debug('**Exception caught**'+ex.getLineNumber() + ' ' + ex.getMessage());
        }
        system.debug('Return'+returnVal);
        return returnVal;
    }
    
}