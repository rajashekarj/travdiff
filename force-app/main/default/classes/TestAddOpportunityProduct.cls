/**
 * This class is used as a The Test Class for AddOpportunityProduct
 *
 * @Author Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shashank S             US59528             07/27/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
@isTest
public class TestAddOpportunityProduct{
    /**
     * Setup Data Method to insert records needed for Testing
     * @param none
     * @return none 
     */
    private static void setupData(){
       //setup Pricebook, Product and Price 
        List<Product2> prodList = TestDataFactory.createProducts(10);
        insert prodList;
        //Get teh standard pricebook Id to create standard pricebookentry for the lsit of products
        Id pricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> pricebookEntryList = new List<PricebookEntry>();
        for(Product2 prod : prodList){
            PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
            pricebookEntryList.add(customPrice);
        }
        //Create Custom pricebook and associate the products with it
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        for(Product2 prod : prodList){
            PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
            pricebookEntryList.add(customPrice);
        }
        insert pricebookEntryList;
        //Insert Account to associate it with the Opportunity
        Account acc = TestDataFactory.createProspect();
        //Insert TPA Account for Lookup
        Account acc1 = TestDataFactory.createTPAAccount();
        List<Account> listAccToAdd= new List<Account>();
        listAccToAdd.add(acc);
        listAccToAdd.add(acc1);
        insert listAccToAdd;
        
        //Insert an Opportunity and associate the custom pricebook to it
        Opportunity oppRec = TestDataFactory.createOppty('Submission','Test Opportunity', acc.Id);
        oppRec.Pricebook2Id = customPB.Id;
        insert oppRec;
    }
    //Test method to search products
    private static testmethod void testSearchProducts(){
        //Setup Data
        setupData();
        //Get Opportunity to search products
        Opportunity oppRec = [SELECT Id, Name FROM Opportunity LIMIT 1];
        
        List<LookupSearchResult> listSearchResults = LookupController.search('Fa','Product2',(string)oppRec.Id,null,false);
        //Will return all records as name starts with FA
        system.assertEquals(10,listSearchResults.size());
        listSearchResults = new List<LookupSearchResult>();
        listSearchResults = LookupController.search('General','Product2',(string)oppRec.Id,null,false);
        //Will return no records as name does not contain General
        system.assertEquals(0,listSearchResults.size());
        listSearchResults = new List<LookupSearchResult>();
        listSearchResults = LookupController.search('TPA','Account',(string)oppRec.Id,null,false);
        system.assertEquals(1,listSearchResults.size());
    }
    //Test method to search products
    private static testmethod void testSaveProducts(){
        //Setup Data
        setupData();
        //Insert Carrier
        TRAV_Carrier__c carrier = new TRAV_Carrier__c(Name = 'Carrier');
        TRAV_Carrier__c carrier1 = new TRAV_Carrier__c(Name = 'UNKNOWN');
        insert carrier;
        insert carrier1;
        //Get Opportunity to search products
        Opportunity oppRec = [SELECT Id, Name FROM Opportunity LIMIT 1];
        List<OpportunityLineItem> oppProductList = new List<OpportunityLineItem>();
        for(Product2 oppProduct : [Select Id, Name FROM Product2 LIMIT 10]){
            OpportunityLineItem obj = new OpportunityLineItem(OpportunityId = oppRec.Id, 
                                        Product2Id = oppProduct.Id,
                                        TotalPrice = 0,
                                        Quantity = 1,
                                        TRAV_Incumbent_Insurance_Carrier_Name__c = carrier.Id);
            oppProductList.add(obj);
        }
        OpportunityLineItem obj = new OpportunityLineItem(OpportunityId = oppRec.Id,                                         
                                        TotalPrice = 0,
                                        Quantity = 1,
                                        TRAV_Incumbent_Insurance_Carrier_Name__c = carrier.Id);
        oppProductList.add(obj);
        AddOpportunityProductController.saveOpportunityLineItems(oppProductList,false);
        AddOpportunityProductController.saveOpportunityLineItems(oppProductList,true);
        oppProductList = new List<OpportunityLineItem>();
        oppProductList = [SELECT Id, Name FROM OpportunityLineItem];
        List<Id> selectedProducts=AddOpportunityProductController.selectedProducts(oppRec.Id);
        List<OpportunityLineItem> existingProducts=AddOpportunityProductController.existingProducts(oppRec.Id);
        system.assert(selectedProducts.size()==oppProductList.size());
        system.assert(existingProducts.size()==oppProductList.size());
        //system.assertEquals(10,oppProductList.size());
        AddOpportunityProductController.getPicklistOptions();

    }
    
    //Test comment
}