/**
* Test class for Content Version Service Class
*
* @Author : Deloitte Consulting
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Shashank Agarwal       US60806              07/12/2019       Original Version
* -----------------------------------------------------------------------------------------------------------
*/  
@isTest
private class TestContentVersionService {
    
    /**
* This method will test scenarios when opportunity is updated
* @param no parameters
* @return void
*/
    static testmethod void testAddErrorLyncOnNoteModified() {
        
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'ContentVersionTrigger';
        insert objCustomsetting;
        
        User objUsr = TestDataFactory.createTestUser('BI - Support','TestBISupport','LastTestBISupport');
        insert objUsr;
        
        //create account record
        Account objAgencyAccount = TestDataFactory.createAgency(); 
        List<Account> lstAccount = new List<Account>{objAgencyAccount};
            insert lstAccount;
        
        //Create Event on Account.
        Id strRecordTypeId = Schema.getGlobalDescribe().get('Event').getDescribe().getRecordTypeInfosByName().get('General').getRecordTypeId();
        Event objEvent = new Event(
            Type = 'External',
            WhatId = lstAccount[0].Id, //Add RecordId of the record where you want to link event
            EndDateTime =DateTime.now().addDays(4),
            ActivityDateTime = DateTime.now().addDays(3), 
            Subject = 'Test Event',
            RecordTypeId = strRecordTypeId
            
        );
        insert objEvent;
        Map<Id,ContentVersion> mapConVersion = new Map<Id,ContentVersion>();
        
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.snote',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        mapConVersion.put(contentVersionInsert.ID, contentVersionInsert);
        //ContentVersion conVer = [Select FileType,Id, Title from ContentVersion where FileType = 'SNOTE'].get(0);
        //system.debug('conVer>>'+conVer);
        ContentNote objContentNote = new ContentNote();
        objContentNote.Title = 'test Note 1';
        String body = 'Test Body';
        objContentNote.Content = Blob.valueOf(body.escapeHTML4());
        insert objContentNote;
        
        ContentVersion objContentVersion = [Select FileType,ContentDocumentId From ContentVersion Where Id = :contentVersionInsert.Id LIMIT 1];
        
        System.debug(objEvent.Id.getsobjecttype());
        
        ContentDocumentLink objContentDocumentLink = new ContentDocumentLink();
        objContentDocumentLink.ContentDocumentId = objContentVersion.ContentDocumentId;
        objContentDocumentLink.LinkedEntityId = objEvent.Id;
        objContentDocumentLink.ShareType = 'V';
        insert objContentDocumentLink;
        System.debug(objContentDocumentLink.LinkedEntityId.getsobjecttype());
        
        Set<Id> setContentDocumentId = new Set<Id>{ objContentDocumentLink.ContentDocumentId};        
        ContentDocumentLinkSelector.getContentDocumentLinkForError(setContentDocumentId);
        
        System.runAs(objUsr){
            String updateBody = 'Update Test Body';
            objContentNote.Content = Blob.valueOf(body.escapeHTML4());
            try{
                insert objContentNote;
                Test.startTest();
                ContentVersionTriggerHandler con = new ContentVersionTriggerHandler();
                con.beforeDelete(mapConVersion);
                con.afterDelete(mapConVersion);
                con.afterUndelete(mapConVersion);
                Test.stopTest();
            }catch(Exception objException){
                System.debug(objException);
            }
        }
    }
    /**
* This method will test scenarios when opportunity is updated
* @param no parameters
* @return void
*/
    static testmethod void testAddErrorLyncOnNoteModified1() {
        
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'ContentVersionTrigger';
        insert objCustomsetting;
        
        User objUsr = TestDataFactory.createTestUser('BI - Support','TestBISupport','LastTestBISupport');
        insert objUsr;
        
        //create account record
        Account objAgencyAccount = TestDataFactory.createAgency(); 
        List<Account> lstAccount = new List<Account>{objAgencyAccount};
            insert lstAccount;
        
        //Create Event on Account.
        Id strRecordTypeId = Schema.getGlobalDescribe().get('Event').getDescribe().getRecordTypeInfosByName().get('General').getRecordTypeId();
        Event objEvent = new Event(
            Type = 'Event',
            WhatId = lstAccount[0].Id, //Add RecordId of the record where you want to link event
            EndDateTime =DateTime.now().addDays(4),
            ActivityDateTime = DateTime.now().addDays(3), 
            Subject = 'Test Event',
            RecordTypeId = strRecordTypeId
            
        );
        insert objEvent;
        Map<Id,ContentVersion> mapConVersion = new Map<Id,ContentVersion>();
        
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.snote',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        mapConVersion.put(contentVersionInsert.ID, contentVersionInsert);
        //ContentVersion conVer = [Select FileType,Id, Title from ContentVersion where FileType = 'SNOTE'].get(0);
        //system.debug('conVer>>'+conVer);
        ContentNote objContentNote = new ContentNote();
        objContentNote.Title = 'test Note 1';
        String body = 'Test Body';
        objContentNote.Content = Blob.valueOf(body.escapeHTML4());
        insert objContentNote;
        
        ContentVersion objContentVersion = [Select FileType,ContentDocumentId From ContentVersion Where Id = :contentVersionInsert.Id LIMIT 1];
        
        System.debug('testtt>>>>'+objEvent.Id.getsobjecttype());
        ContentDocumentLink objContentDocumentLink = new ContentDocumentLink();
        objContentDocumentLink.ContentDocumentId = objContentVersion.ContentDocumentId;
        objContentDocumentLink.LinkedEntityId = objEvent.Id;
        objContentDocumentLink.ShareType = 'V';
        if (Test.isRunningTest()) {
        	insert objContentDocumentLink;
        }
        System.debug(objContentDocumentLink.LinkedEntityId.getsobjecttype());
        
        Set<Id> setContentDocumentId = new Set<Id>{ objContentDocumentLink.ContentDocumentId};        
        List<ContentDocumentLink> lstContentDocLink = ContentDocumentLinkSelector.getContentDocumentLinkForError(setContentDocumentId);
        system.debug('lstContentDocLink>>'+lstContentDocLink);
        
        System.runAs(objUsr){
            String updateBody = 'Update Test Body';
            objContentNote.Content = Blob.valueOf(body.escapeHTML4());
            try{
                insert objContentNote;
                Test.startTest();
                ContentVersionTriggerHandler con = new ContentVersionTriggerHandler();
                con.beforeDelete(mapConVersion);
                con.afterDelete(mapConVersion);
                con.afterUndelete(mapConVersion);
                Test.stopTest();
            }catch(Exception objException){
                System.debug(objException);
            }
        }
    }
	
	//To cover ContentDocumentLinkSelector and ContentVersionService Catch block
	private static testmethod void coverContentDocumentLinkSelectorException(){
        Set<Id> ids = new Set<Id>();
       ContentDocumentLinkSelector.getContentDocumentLinkForError(ids); 
	   ContentVersionService.addErrorLyncOnNoteModified(new List<ContentVersion>(),new Map<Id,ContentVersion>());
    }
	
	/**
* This method will test scenarios when ContentVersion is updated
* @param no parameters
* @return void
*/
    static testmethod void testAddErrorLyncOnNoteModified2() {
        
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'ContentVersionTrigger';
        insert objCustomsetting;
        
        User objUsr = TestDataFactory.createTestUser('BI - Support','TestBISupport','LastTestBISupport');
        insert objUsr;
        
        //create account record
        Account objAgencyAccount = TestDataFactory.createAgency(); 
        List<Account> lstAccount = new List<Account>{objAgencyAccount};
            insert lstAccount;
        
        //Create Event on Account.
        Id strRecordTypeId = Schema.getGlobalDescribe().get('Event').getDescribe().getRecordTypeInfosByName().get('General').getRecordTypeId();
        Event objEvent = new Event(
            Type = 'External',
            WhatId = lstAccount[0].Id, //Add RecordId of the record where you want to link event
            EndDateTime =DateTime.now().addDays(4),
            ActivityDateTime = DateTime.now().addDays(3), 
            Subject = 'Test Event',
            RecordTypeId = strRecordTypeId
            
        );
        insert objEvent;
        Map<Id,ContentVersion> mapConVersion = new Map<Id,ContentVersion>();
        
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.snote',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
            
        );
        insert contentVersionInsert;
        mapConVersion.put(contentVersionInsert.ID, contentVersionInsert);
        //ContentVersion conVer = [Select FileType,Id, Title from ContentVersion where id =: contentVersionInsert.Id].get(0);
        //system.debug('FileType>>1'+conVer);
        ContentNote objContentNote = new ContentNote();
        objContentNote.Title = 'test Note 1';
        String body = 'Test Body';
        objContentNote.Content = Blob.valueOf(body.escapeHTML4());
        insert objContentNote;
        
        ContentVersion objContentVersion = [Select FileType,ContentDocumentId From ContentVersion Where Id = :contentVersionInsert.Id LIMIT 1];
        
        System.debug(objEvent.Id.getsobjecttype());
        
        ContentDocumentLink objContentDocumentLink = new ContentDocumentLink();
        objContentDocumentLink.ContentDocumentId = objContentVersion.ContentDocumentId;
        objContentDocumentLink.LinkedEntityId = objEvent.Id;
        objContentDocumentLink.ShareType = 'V';
        insert objContentDocumentLink;
        System.debug(objContentDocumentLink.LinkedEntityId.getsobjecttype());
        
        Set<Id> setContentDocumentId = new Set<Id>{ objContentDocumentLink.ContentDocumentId};        
        ContentDocumentLinkSelector.getContentDocumentLinkForError(setContentDocumentId);
        System.runAs(objUsr){
            String updateBody = 'Update Test Body';
            objContentNote.Content = Blob.valueOf(body.escapeHTML4());
            try{
                insert objContentNote;
                Test.startTest();
                contentVersionInsert.Title='Testing';
                update contentVersionInsert;
                Test.stopTest();
            }catch(Exception objException){
                System.debug(objException);
            }
        }
    }
}