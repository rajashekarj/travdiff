public class CarrierListsObject {
    public Map<Id, List<Id>> oppCompetingCarriers;
    public Map<Id, List<Id>> oppIncumbentCarriers;

    public CarrierListsObject(){
        oppCompetingCarriers = new Map<Id, List<Id>>();
        oppIncumbentCarriers = new Map<Id, List<Id>>();
    }
}