/**
* This class is used as a test class for DaysLeftInCurrentMonthController
* 
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Tejal Pradhan          US63928              03/18/2020       Test Class for DaysLeftInCurrentMonthController
*-----------------------------------------------------------------------------------------------------------
*/
@IsTest
public class TestDaysLeftInCurrentMonthController {
    
    static testmethod void testAccountingMonthInfo() {
        Test.startTest();
        AccountingMonthDetailWrapper response = DaysLeftInCurrentMonthController.getAccountingMonthDetails();
        Test.stopTest();
        System.assertNotEquals(null, response);
    }
}