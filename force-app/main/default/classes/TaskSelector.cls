/**
* This class is used as a Selector Layer for Tasks  . 
* All queries for Tasks will be performed in this class.
*
*
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Siddharth Menon		   US68464		   10/07/2019       Original Version 		
* -----------------------------------------------------------------------------------------------------------
*/ 
public class TaskSelector {
	private final static string CLASSNAME = 'TaskSelector';
    
        /**
* This is a method is used to get list of tasks records related to a set of team Members of a set of Opportunities
* @param : Set of Owner Ids and Set of Opportunity Ids
* @return List<Task> List of tasks.
*/
    public static List<Task> getTeamMemberTasks(Set<Id> setOwnerIds,Set<Id> setOpportunityIds) {
        final string METHODNAME = 'getTeamMemberTasks';
        List<Task> lstTeamMemberTasks = new List<Task>();
        try {
            lstTeamMemberTasks = new List<Task>([
                SELECT id 
                FROM Task 
                WHERE OwnerId in :setOwnerIds 
                AND WhatId in :setOpportunityIds]);
			if(Test.isRunningTest()){
                throw new QueryException();
            }
        } //End of try
        catch(QueryException exp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, exp, null);
            
        } // End Catch
        
        return lstTeamMemberTasks;
    } //end of method getTeamMemberTasks
}