/**
 * This class is used as a The controller for the NewOpportunityLineItem component
 *
 * @Author Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shashank S             US59528             07/27/2019       Original Version
 * Siddharth M            US66568             09/04/2019       Updated search function to return only products
 *                                                             not selected before for BSI Users
 * Siddharth M			  US69833			  10/25/2019		removed method getNAProducts and wrapper class 
 * 																ComboboxList
 * -----------------------------------------------------------------------------------------------------------
 */
public without sharing class LookupController {
    //LIMIT the records returned to 10.
    private final static Integer LIMIT_RECORDS = 10;
   /**
   * This is a method which is used to Save the opportunity line items
   * @param String searchTerm : Search string, String sObjectName : the sObject API name to filter records on
   * @return List<LookupSearchResults> wrapper of the returned records.
   */
    @AuraEnabled(Cacheable=true)
    public static List<LookupSearchResult> search(String searchTerm, String sObjectName, String recId,List<Id> selectedProductsList,Boolean isBSIView) {
    system.debug(recId+'recrecrec');
        // Prepare query paramters, adding wildcard to perform like query
        searchTerm = '%'+searchTerm+'%';
        String soqlQuery;
        String recordType = '%TPA%';
        if(sObjectName.equals('Account')) {
            soqlQuery = 'SELECT Id, Name,TRAV_Approved__c FROM '+sObjectName+' WHERE Name LIKE \''+searchTerm+'\' AND RecordType.Name LIKE \''+recordType+'\' LIMIT '+LIMIT_RECORDS;
        } else {
         	soqlQuery = 'SELECT Id, Name FROM '+sObjectName+' WHERE Name LIKE \''+searchTerm+'\' LIMIT '+LIMIT_RECORDS;   
        }
        System.debug('Test soql Query'+soqlQuery);
        if(sObjectName.equals('Product2')){
            //Get Pricebook for the opportunity
            Opportunity oppRec = [SELECT Id, Pricebook2Id FROM Opportunity where Id =:recId];
            system.debug(oppRec);
            List<Pricebook2> pb2 = [SELECT Id, Name FROM Pricebook2 WHERE Id =: oppRec.Pricebook2Id LIMIT 1];
            system.debug(pb2);
            if(!pb2.isEmpty()){
                if(isBSIView && searchTerm=='%%')
                {soqlQuery = 'SELECT Id, Name, Pricebook2Id, Product2Id FROM PricebookEntry WHERE Pricebook2Id =\''+pb2[0].Id+'\' AND Product2Id NOT IN:selectedProductsList ORDER BY Name ';
            }else if(isBSIView && searchTerm!='%%')
                {soqlQuery = 'SELECT Id, Name, Pricebook2Id, Product2Id FROM PricebookEntry WHERE Pricebook2Id =\''+pb2[0].Id+'\' AND Name LIKE \''+searchTerm+'\' AND Product2Id NOT IN:selectedProductsList ORDER BY Name LIMIT '+LIMIT_RECORDS;
            }else{
                //form SOQL for product
                soqlQuery = 'SELECT Id, Name, Pricebook2Id, Product2Id FROM PricebookEntry WHERE Pricebook2Id =\''+pb2[0].Id+'\' AND Name LIKE \''+searchTerm+'\' ORDER BY Name LIMIT '+LIMIT_RECORDS;
            }
			}
        }
        // Prepare results
        system.debug(soqlQuery);
        List<LookupSearchResult> results = new List<LookupSearchResult>();
        LIst<sObject> listOfRecords = database.query(soqlQuery);
        //Set a generic icon to display against the lookup results
        String objIcon = 'standard:channel_program_members';
        if(sObjectName.equals('Product2')){
            objIcon = 'standard:product';    
        }
        system.debug(listOfRecords);
        // Iterate over the returned records and prepare the wrapper.
        for (sObject rec : listOfRecords) {
            if(sObjectName.equals('Product2')){
                results.add(new LookupSearchResult((String)rec.get('Product2Id'), sObjectName, objIcon, (String)rec.get('Name'), '',''));   
            }
            else if(sObjectName.equals('Account')){
                String approved = String.valueOf(rec.get('TRAV_Approved__c'));
                results.add(new LookupSearchResult((String)rec.get('Id'), sObjectName, objIcon, (String)rec.get('Name'), '',approved));
            }
            else{
               results.add(new LookupSearchResult((String)rec.get('Id'), sObjectName, objIcon, (String)rec.get('Name'), '','')); 
                system.debug('results-->>'+results);
            }
        }
        return results;
    } 
    
}