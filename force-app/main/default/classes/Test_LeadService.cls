/**
* This is a test clas for LeadService

* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect      Date             Description
* -----------------------------------------------------------------------------------------------------------
* Bhanu Reddy               US59527             5/20/20119        Original Version
*
*/

@isTest
private class Test_LeadService {

    /**
* This method is used for test converage for convertLead method which created new account
* opportunity
*/
    static testmethod void test_convertLeads()
    {

        //create the custom setting for accoutn trigger

        TRAV_Trigger_Setting__c customsetting = TestDataFactory.createTriggersetting();
        insert customsetting;

        //create the custom setting for general data setting
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Written,Declined,Quote Unsuccessful';
        insert objGeneralDataSetting;

        //create agency account record
        Account agency = TestDataFactory.createAgency();
        insert agency; 



        //create an lead with certified businees id
        Lead objLead = TestDataFactory.createLead();
        //leadtoInsert.status = SYSTEM.LABEL.TRAV_Qualified;
        objLead.TRAV_Agency_Broker__c = agency.id;
        insert objLead;
        //check for standard Lead status modificaiton and lead COnversion
        test.startTest();
        objLead.Status = SYSTEM.LABEL.TRAV_QUALIFIED;
        update objLead;
        system.debug('lead covnersted status is'+objLead.IsConverted);
        list<Account> lstaccnt= [select id from account ];
        list<Opportunity> lstoppnt = [select id from Opportunity];
        list<Contact> lstContact = [select id from Contact];
        ApexTrigger AccountTrigger = [Select Id, Status from ApexTrigger where name='AccountTrigger' and NameSpacePrefix=null];
        if(AccountTrigger.Status == 'Active'){
            system.assertEquals(lstaccnt.size(), 2);
            system.assertEquals(lstoppnt.size(), 1);
            system.assertEquals(lstContact.size(), 0);
        }
        
        //check Lead Conversion on modification of custom lead status field
        // and an account that contains certifed businees id
        Lead objLeadcus = TestDataFactory.createLead();
        objLeadcus.TRAV_Agency_Broker__c = agency.id;
        objLeadcus.LastName = 'LastName';
        objLeadcus.TRAV_Certified_Business_Id__c='A1011';
        insert objLeadcus;
        Account objPropect = TestDataFactory.createProspect();
        objPropect.TRAV_Certified_Business_Id__c = 'A1011' ;
        insert objPropect;

        //update the custom status value of the lead for Lead Conversion
        objLeadcus.Trav_Lead_Status_For_Path__c = SYSTEM.LABEL.TRAV_QUALIFIED;
        update objLeadcus;

        //test account redirect methods
         string accountid=LeadService.accountRedirect(objLeadcus.id);
       // system.assertEquals(accountid, objPropect.id) ;
        test.stopTest();
    } //end Test_leadConvertMethod

/**

* This method is used for test converage for convertLead method which created new account
* opportunity
*/
   private static testmethod void coverInvalidTriggerContext(){
       LeadTriggerHandler obj = new LeadTriggerHandler();
       obj.beforeDelete(New Map<Id, sObject>());
       obj.afterDelete(New Map<Id, sObject>());
       obj.afterUndelete(New Map<Id, sObject>());
       AccountFeedSelector.getAccountFeed(new Set<Id>());
   } 
}