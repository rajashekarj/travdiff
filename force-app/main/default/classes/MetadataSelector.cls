/**
* This class is used as a Selector Layer for All Custom Metadata.
* All queries for Custom Metadata will be performed in this class.
*
*
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect     Date             Description
* -----------------------------------------------------------------------------------------------------------
* Tejal Pradhan          US63928        		03/18/2020       Original Version
-----------------------------------------------------------------------------------------------------------
*/
public class MetadataSelector {
    /**
* This is a method is used to get list of get the current accounting cycle information
* @param null
* @return List<TRAV_Days_Left_in_Current_Month__mdt> List of TRAV_Days_Left_in_Current_Month__mdt 
*/
    public static List<TRAV_Days_Left_in_Current_Month__mdt> getCurrentAccountCycleInformation() {
        final string METHODNAME = 'getCurrentAccountCycleInformation';
        final string CLASSNAME = 'MetadataSelector';
        String strQuery = 'SELECT Id, MasterLabel, TRAV_Cycle_Start_Date__c, TRAV_Cycle_End_Date__c '
            + ' FROM TRAV_Days_Left_in_Current_Month__mdt ' 
            + ' WHERE (TRAV_Cycle_Start_Date__c <= TODAY AND TRAV_Cycle_End_Date__c >= TODAY) '
            + ' LIMIT 1';
        List<TRAV_Days_Left_in_Current_Month__mdt> listDaysLeftMetadata = new List<TRAV_Days_Left_in_Current_Month__mdt>();
        try {
            listDaysLeftMetadata = Database.query(strQuery);
            if(Test.isRunningTest()){
                throw new QueryException();
            }
        }
        catch(QueryException objExp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
            
        } // End Catch
        return listDaysLeftMetadata;
    } //end of method getCurrentAccountCycleInformation
}