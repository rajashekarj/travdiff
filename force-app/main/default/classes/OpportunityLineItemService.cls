/**
* Utility Class for OpportunityLineItem Trigger Handler. This class will have logic for all the implementations.
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer     User Story/Defect   Date        Description
* -----------------------------------------------------------------------------------------------------------
* Erec Lawrie       US62419         06/28/2019  Added method to set product defaults
* Shashank Agarwal  US63871         07/27/2019  Added Method populatePotentialPremium
* Bharat Madaan     US63871         07/31/2019  Added Method addProductNamestoOpportunity,removeProductNameFromOpty
* Shashank Agarwal  US65682         09/03/2019  Added Method updateTeamMemberOnLineItemUpdate,TeamMemberWrapper,addErrorWhenOpportunityStageSubmission
* Shashank Agarwal                  11/04/2019  Added Method stampOpportunityLineItemIdOnDelete
* Erec Lawrie       US77080         02/26/2020  Updated add and remove productnames methods to use product abbreviation if available
* Erec Lawrie       US77080         03/05/2020  Updated add/remove ProductName: updates to populate product abbreviations
* Ben Climan        US76573         02/13/2020  Updated addProductNamestoOpportunity method to to ignore certified business opportunity records
* Evan Wilcher	    US78123	    03/03/2020  Updated selected methods to omit Certified Business Opportunities from OpportunityLineItemService methods.
*/
public class OpportunityLineItemService {

    /**
    * This method sets the default incumbent and competing carriers on the Opportunity
    * @param newList - List of new values in the for the OpportunityLineItem records.
    * @param oldMap - Map of old values in the for the OpportunityLineItem records
    * @return void - returns void .
    */
    public static void addProductNamestoOpportunity(List<OpportunityLineItem> lstNewOpportunityLineItem){
        final string METHODNAME = 'addProductNamestoOpportunity';
        Map<Id,Opportunity> setOpportunityForUpdate = new Map<Id,Opportunity>();

        Set<ID> oppIds = new Set<Id>();
        List<String> opportunityLineitemProductNameList = new List<String>();
        List<String> opportunityLineitemProductAbbrList = new List<String>();

        try{
            for(OpportunityLineItem item :lstNewOpportunityLineItem){
                oppIds.Add(item.OpportunityId);
            }

            List<Opportunity> lstOpportunity = OpportunitySelector.getOpportunitiesWithRelatedLineItemProducts(oppIds, OpportunitySelector.ExcludeOpportunityBy.CB_MDM);
            for(Opportunity objOpty : lstOpportunity){
                for(OpportunityLineItem lineItem :objOpty.OpportunityLineItems){
                    opportunityLineitemProductNameList.Add(lineItem.Product2.Name);
                    opportunityLineitemProductAbbrList.Add(lineItem.Product2.TRAV_Product_Abbreviation__c);
                }

                if (opportunityLineitemProductNameList.size() > 0) {
                    objOpty.TRAV_Product_Names__c = String.Join(opportunityLineitemProductNameList,';');
                } else {
                    objOpty.TRAV_Product_Names__c = '';
                }

                if (opportunityLineitemProductAbbrList.size() > 0) {
                    objOpty.TRAV_Product_Names_Abr__c = String.Join(opportunityLineitemProductAbbrList,';');
                } else {
                    objOpty.TRAV_Product_Names_Abr__c = '';
                }

                opportunityLineitemProductNameList.Clear();
                opportunityLineitemProductAbbrList.Clear();
                    
                setOpportunityForUpdate.put(objOpty.Id,objOpty);
            }

            if( !setOpportunityForUpdate.isEmpty() ) {
                //Updating Opportunities
                List<Opportunity> listOpportunityForUpdate = new List<Opportunity>();
                listOpportunityForUpdate.addAll(setOpportunityForUpdate.values()) ;
                Database.SaveResult[] lstSaveResultForUpdate = Database.update(listOpportunityForUpdate, false);
                ExceptionUtility.logDMLError(Constants.OPPORTUNITYLINEITEMSERVICE, METHODNAME, lstSaveResultForUpdate);
            }//End if
        }//End of try
        catch(Exception objExp) {
            system.debug('exception ' +objExp);
            ExceptionUtility.logApexException(Constants.OPPORTUNITYLINEITEMSERVICE, METHODNAME, objExp, null);
        }//End of catch
    } // End of method

    /**
    * This method Populates Potential Premium on Opportunity
    * @param newList - List of new values in the for the OpportunityLineItem records.
    * @return void - returns void .
    */
    public static void populatePotentialPremium(List<OpportunityLineItem> lstNewOpportunityLineItem, Map<Id, OpportunityLineItem> mapOldOpportunityLineItem){
        String METHODNAME = 'populatePotentialPremium';
        System.debug('In populatePotentialPremium');
        Set<Id> setOpportunityId = new Set<Id>();
        try{
            if(lstNewOpportunityLineItem != null){
                for(OpportunityLineItem objOpportunityLineItem : lstNewOpportunityLineItem){
                    // Insert Case

                    if(mapOldOpportunityLineItem == Null && objOpportunityLineItem.TRAV_Coverage_Premium__c != Null){
                        setOpportunityId.add(objOpportunityLineItem.OpportunityId);
                        // Update Case
                    }else if(mapOldOpportunityLineItem != Null && objOpportunityLineItem.TRAV_Coverage_Premium__c != mapOldOpportunityLineItem.get(objOpportunityLineItem.Id).TRAV_Coverage_Premium__c){
                        System.debug('In Update Check');
                        setOpportunityId.add(objOpportunityLineItem.OpportunityId);
                    }
                }
                // Delete Case
            }else{
                for(OpportunityLineItem objOpportunityLineItem : mapOldOpportunityLineItem.values()){
                    if(objOpportunityLineItem.TRAV_Coverage_Premium__c != Null){
                        setOpportunityId.add(objOpportunityLineItem.OpportunityId);
                    }
                }
            }
            
            System.debug('In populatePotentialPremium 2');
            Constants.ErrorCheckForPotentialPremium = false;
			
            //Updated for US78123 by Evan Wilcher 03/04/2020
    		//Updated Method call to call getOpportunitiesForPotentialPremium
    		//override method.
            Map<Id,AggregateResult> mapOpportunity = OpportunityLineItemSelector.getOpportunitiesForPotentialPremium(setOpportunityId, OpportunityLineItemSelector.ExcludeOpportunityBy.CB_MDM);
            List<Opportunity> lstOpportunityForUpdate = new List<Opportunity>();
            System.debug('In populatePotentialPremium 3'+ mapOpportunity);
            for(Id objId : setOpportunityId){
                Opportunity objOpportunity = new Opportunity();
                objOpportunity.Id = objId;
                if(mapOpportunity.containsKey(objId)){
                    if(mapOpportunity.get(objId).get('expr0') != Null){
                        objOpportunity.TRAV_Submission_Premium_Currency__c = (Decimal)mapOpportunity.get(objId).get('expr0') ;
                    }else{
                        objOpportunity.TRAV_Submission_Premium_Currency__c = 0;
                    }
                }else{
                    objOpportunity.TRAV_Submission_Premium_Currency__c = 0;
                }
                lstOpportunityForUpdate.add(objOpportunity);

            }
            System.debug('Hi'+lstOpportunityForUpdate);
            System.debug('In populatePotentialPremium 3');
            //  Null Check for lstOpportunityForUpdate
            if( !lstOpportunityForUpdate.isEmpty() ) {
                //Updating Opportunities
                Database.SaveResult[] lstSaveResultForUpdate = Database.update(lstOpportunityForUpdate, false);
                ExceptionUtility.logDMLError(Constants.OPPORTUNITYLINEITEMSERVICE, METHODNAME, lstSaveResultForUpdate);
            }//End if

            System.debug('In populatePotentialPremium 4');
        }catch(exception objExp) {
            ExceptionUtility.logApexException(Constants.OPPORTUNITYLINEITEMSERVICE, METHODNAME, objExp, null);
        } //End catch
    }



    /**
    * This method deletes the Product Name from Opportunity
    * @param newList - List of new values in the for the OpportunityLineItem records.
    * @return void - returns void .
    */
    public static void removeProductNameFromOpty(Map<Id, OpportunityLineItem> mapOldOpportunityLineItem){
        final String METHODNAME = 'removeProductNameFromOpty';
        String strProductNames;
        
        List<Opportunity> lstOpportunityForUpdate = new List<Opportunity>();
        Set<ID> oppIds = new Set<Id>();

        try{
            for(OpportunityLineItem item : mapOldOpportunityLineItem.values()){
                oppIds.Add(item.OpportunityId);
            }

            Map<Id,Set<String>> mapOptyLineItem = new Map<Id,Set<String>>();
            Map<Id,Set<String>> mapOptyLineItemAbbr = new Map<Id,Set<String>>();
            Map<Id,String> mapOptyDirectSource = new Map<Id,String>();
            List<OpportunityLineItem> listLineItem = OpportunityLineItemSelector.getOpportunityLineItemsForProductNameUpdate(oppIds);
            if(listLineItem.size() > 0){
                for(Id objId : oppIds){
                    Set<String> setOpportunityProd = new Set<String>();
                    Set<String> setOpportunityProdAbbr = new Set<String>();
                    
                    for(OpportunityLineItem objLineItem : listLineItem){
                        if(objLineItem.OpportunityId == objId) {
                            //save the direct source system code from the opportunity here so we can 
                            //check it before updating the opportunity records
                            mapOptyDirectSource.put(objId, objLineItem.Opportunity.TRAV_Direct_Source_Sys_Code__c);

                            setOpportunityProdAbbr.add(objLineItem.Product2.TRAV_Product_Abbreviation__c);
                            setOpportunityProd.add(objLineItem.Product2.Name);
                        }
                    }
                    mapOptyLineItem.put(objId, setOpportunityProd);
                    mapOptyLineItemAbbr.put(objId, setOpportunityProdAbbr);
                }
            }

            for(OpportunityLineItem item : mapOldOpportunityLineItem.values()){
                // need this check here to only update the opportunity if it not from CB, 
                // since this is iterating over the old line items collection we don't know if we should touch it until now
                if (mapOptyDirectSource.get(item.OpportunityId) != 'CB MDM') {
                    Set<String> setOpportunityProd = mapOptyLineItem.get(item.OpportunityId);
                    Set<String> setOpportunityProdAbbr = mapOptyLineItemAbbr.get(item.OpportunityId);

                    Opportunity objOpportunity = new Opportunity();
                    objOpportunity.Id = item.OpportunityId;
                    objOpportunity.TRAV_Product_Names__c = setOpportunityProd != NULL ? String.join(new List<String>(setOpportunityProd), ';') : '';
                    objOpportunity.TRAV_Product_Names_Abr__c = setOpportunityProdAbbr != NULL ? String.join(new List<String>(setOpportunityProdAbbr), ';') : '';
                    lstOpportunityForUpdate.add(objOpportunity);
                }
            }

            if( !lstOpportunityForUpdate.isEmpty() ) {
                //Updating Opportunities
                Database.SaveResult[] lstSaveResultForUpdate = Database.update(lstOpportunityForUpdate, false);
                ExceptionUtility.logDMLError(Constants.OPPORTUNITYLINEITEMSERVICE, METHODNAME, lstSaveResultForUpdate);
            }//End if

        }//End of try
        catch(Exception objExp) {
            system.debug('exception ' +objExp);
            ExceptionUtility.logApexException(Constants.OPPORTUNITYLINEITEMSERVICE, METHODNAME, objExp, null);
        }//End of catch
    }
    /**
* This method insert, update or delete OpportunityTeamMember as per LineItem of Opportunity
* @param newList - List of new values in the for the OpportunityLineItem records.
* @param oldMap- List of old values in the for the OpportunityLineItem records.
* @return void - returns void .
*/
    public static void updateTeamMemberOnLineItemUpdate(List<OpportunityLineItem> lstNewOpportunityLineItem, Map<Id, OpportunityLineItem> mapOldOpportunityLineItem){

        final String METHODNAME = 'updateTeamMemberOnLineItemUpdate';
        Set<Id> setOpportunityId = new Set<Id>();
        List<OpportunityLineItem> lstLineItem = new List<OpportunityLineItem>();
        List<OpportunityLineItem> lstAllLineItem = new List<OpportunityLineItem>();
        List<OpportunityTeamMember> lstAllTeamMember = new List<OpportunityTeamMember>();
        Set<String> setUniqueIdentifier = new Set<String>();
        Set<String> setUniqueIdentifierTeamMember = new Set<String>();
        try{

            if(lstNewOpportunityLineItem != Null && mapOldOpportunityLineItem == Null){
                for(OpportunityLineItem objOpportunityLineItem : lstNewOpportunityLineItem){
                    if(objOpportunityLineItem.TRAV_Coverage_Approver_Name__c != Null){
                        setOpportunityId.add(objOpportunityLineItem.OpportunityId);
                        lstLineItem.add(objOpportunityLineItem);
                    }
                }
            }else if(lstNewOpportunityLineItem != Null && mapOldOpportunityLineItem != Null){
                for(OpportunityLineItem objOpportunityLineItem : lstNewOpportunityLineItem){
                    if(objOpportunityLineItem.TRAV_Coverage_Approver_Name__c != mapOldOpportunityLineItem.get(objOpportunityLineItem.Id).TRAV_Coverage_Approver_Name__c
                       || objOpportunityLineItem.TRAV_URC_Indicator__c != mapOldOpportunityLineItem.get(objOpportunityLineItem.Id).TRAV_URC_Indicator__c){
                        setOpportunityId.add(objOpportunityLineItem.OpportunityId);
                        lstLineItem.add(objOpportunityLineItem);
                    }
                }
            } else{
                for(OpportunityLineItem objOpportunityLineItem : mapOldOpportunityLineItem.values()){
                    if(objOpportunityLineItem.TRAV_Coverage_Approver_Name__c != Null){
                        setOpportunityId.add(objOpportunityLineItem.OpportunityId);
                        lstLineItem.add(objOpportunityLineItem);
                    }
                }
            }

            if(!setOpportunityId.isEmpty()){
                lstAllLineItem = OpportunityLineItemSelector.getOpportunitiesLineItemForTeamMemberUpdate(setOpportunityId,lstLineItem);
                lstAllTeamMember = OpportunityTeamMemberSelector.getOpportunityTeamMembersForUpdate(setOpportunityId);
            }


            Map<String,OpportunityTeamMember> mapKeyToTeamMember = new Map<String,OpportunityTeamMember>();
            Map<String,TeamMemberWrapper> mapTeamMemberWrapper = new Map<String,TeamMemberWrapper>();

            for(OpportunityLineItem objOpprtunityLineItem :lstAllLineItem){
                setUniqueIdentifier.add(string.valueof(objOpprtunityLineItem.OpportunityId)+
                         string.valueof(objOpprtunityLineItem.TRAV_Coverage_Approver_Name__c)+
                           string.valueof(objOpprtunityLineItem.TRAV_URC_Indicator__c));
            } //end of iteration opportunityLine Item

            for(OpportunityTeamMember objOpprtunityTeamMember :lstAllTeamMember){
                String strUniqueKey =string.valueof(objOpprtunityTeamMember.OpportunityId)+
                         string.valueof(objOpprtunityTeamMember.UserId)+
                           string.valueof(objOpprtunityTeamMember.TeamMemberRole);
                mapKeyToTeamMember.put(strUniqueKey,objOpprtunityTeamMember);
            } //end of iteration opportunityLine Item
            System.debug('MAP'+mapKeyToTeamMember);

            //For Insert
            if(lstNewOpportunityLineItem != Null && mapOldOpportunityLineItem == Null){
            for(OpportunityLineItem objLineItem :lstLineItem){

                String strUniqueIndetifier = string.valueof(objLineItem.OpportunityId)+
                                 string.valueof(objLineItem.TRAV_Coverage_Approver_Name__c)+string.valueof(objLineItem.TRAV_URC_Indicator__c);

                if(!setUniqueIdentifier.contains(strUniqueIndetifier)){
                    TeamMemberWrapper objTeamMemberWrapper = new TeamMemberWrapper();
                    objTeamMemberWrapper.opportunityId = objLineItem.OpportunityId;
                    objTeamMemberWrapper.TeamRole = objLineItem.TRAV_URC_Indicator__c ? 'URC Approver' : 'Approver';
                    objTeamMemberWrapper.MemberId = objLineItem.TRAV_Coverage_Approver_Name__c;
                    objTeamMemberWrapper.isInsert = true;
                    mapTeamMemberWrapper.put(strUniqueIndetifier,objTeamMemberWrapper);
                } //end if unique check
            }

            }// End Insert Case

            // For Update
            if(lstNewOpportunityLineItem != Null && mapOldOpportunityLineItem != Null){
                System.Debug('In Update');
                for(OpportunityLineItem objLineItem :lstLineItem){
                    String strUniqueIndetifier = string.valueof(objLineItem.OpportunityId)+string.valueof(objLineItem.TRAV_Coverage_Approver_Name__c)+string.valueof(objLineItem.TRAV_URC_Indicator__c);
                    String strRole = mapOldOpportunityLineItem.get(objLineItem.Id).TRAV_URC_Indicator__c ? 'URC Approver' : 'Approver';
                    String strUniqueIndetifierTeamMember = string.valueof(objLineItem.OpportunityId) +string.valueof(mapOldOpportunityLineItem.get(objLineItem.Id).TRAV_Coverage_Approver_Name__c)+strRole;
                    System.Debug('Unique Key: '+strUniqueIndetifierTeamMember + setUniqueIdentifier.contains(strUniqueIndetifierTeamMember));
                    if(!setUniqueIdentifier.contains(strUniqueIndetifierTeamMember)){

                        //Case 1: If New Coverage Approver is Not Null and Old Coverage Approver is  Null
                        if(objLineItem.TRAV_Coverage_Approver_Name__c != Null && mapOldOpportunityLineItem.get(objLineItem.Id).TRAV_Coverage_Approver_Name__c == Null){
                            TeamMemberWrapper objTeamMemberWrapper = new TeamMemberWrapper();
                            objTeamMemberWrapper.opportunityId = objLineItem.OpportunityId;
                            objTeamMemberWrapper.TeamRole = objLineItem.TRAV_URC_Indicator__c ? 'URC Approver' : 'Approver';
                            objTeamMemberWrapper.MemberId = objLineItem.TRAV_Coverage_Approver_Name__c;
                            objTeamMemberWrapper.isInsert = true;

                            mapTeamMemberWrapper.put(strUniqueIndetifier,objTeamMemberWrapper);
                        } // End Case 1.

                        //Case 2: If New Coverage Approver is Not Null and Old Coverage Approver is Not Null
                        if(objLineItem.TRAV_Coverage_Approver_Name__c != Null && mapOldOpportunityLineItem.get(objLineItem.Id).TRAV_Coverage_Approver_Name__c != Null){
                           System.Debug('In Case2');
                           if(mapKeyToTeamMember.containsKey(strUniqueIndetifierTeamMember)){
                                System.Debug('Passed Unique Check');
                                OpportunityTeamMember objOpportunityTeamMember = new OpportunityTeamMember();
                                objOpportunityTeamMember = mapKeyToTeamMember.get(strUniqueIndetifierTeamMember);
                                if(objOpportunityTeamMember.UserId == mapOldOpportunityLineItem.get(objLineItem.Id).TRAV_Coverage_Approver_Name__c && objOpportunityTeamMember.TeamMemberRole == (mapOldOpportunityLineItem.get(objLineItem.Id).TRAV_URC_Indicator__c ? 'URC Approver' : 'Approver')){
                                            TeamMemberWrapper objTeamMemberWrapper = new TeamMemberWrapper();
                                            objTeamMemberWrapper.opportunityId = objOpportunityTeamMember.OpportunityId;
                                            objTeamMemberWrapper.opportunityTeamMemberId = objOpportunityTeamMember.Id;
                                            objTeamMemberWrapper.TeamRole = objLineItem.TRAV_URC_Indicator__c ? 'URC Approver' : 'Approver';
                                            objTeamMemberWrapper.MemberId = objLineItem.TRAV_Coverage_Approver_Name__c;
                                            objTeamMemberWrapper.isInsert = true;
                                            if(objLineItem.TRAV_Coverage_Approver_Name__c != mapOldOpportunityLineItem.get(objLineItem.Id).TRAV_Coverage_Approver_Name__c)
                                                objTeamMemberWrapper.isDelete = true;

                                            mapTeamMemberWrapper.put(strUniqueIndetifier,objTeamMemberWrapper);
                                    }
                           }



                        } // End Case 2

                        //Case 3: If New Coverage Approver is Null and Old Coverage Approver is Not Null
                        if(objLineItem.TRAV_Coverage_Approver_Name__c == Null && mapOldOpportunityLineItem.get(objLineItem.Id).TRAV_Coverage_Approver_Name__c != Null){

                            if(mapKeyToTeamMember.containsKey(strUniqueIndetifierTeamMember)){
                                OpportunityTeamMember objOpportunityTeamMember = new OpportunityTeamMember();
                                objOpportunityTeamMember = mapKeyToTeamMember.get(strUniqueIndetifierTeamMember);
                                if(objOpportunityTeamMember.UserId == mapOldOpportunityLineItem.get(objLineItem.Id).TRAV_Coverage_Approver_Name__c && objOpportunityTeamMember.TeamMemberRole == (mapOldOpportunityLineItem.get(objLineItem.Id).TRAV_URC_Indicator__c ? 'URC Approver' : 'Approver')){
                                            TeamMemberWrapper objTeamMemberWrapper = new TeamMemberWrapper();
                                            objTeamMemberWrapper.opportunityTeamMemberId = objOpportunityTeamMember.Id;
                                            objTeamMemberWrapper.isDelete = true;
                                            mapTeamMemberWrapper.put(strUniqueIndetifier,objTeamMemberWrapper);
                                }
                            }



                        } // End Case 3
                    } //End if unique check Check
                } //End for loop
            } //End if update check.

            if(lstNewOpportunityLineItem == Null && mapOldOpportunityLineItem != Null){
                 for(OpportunityLineItem objLineItem :lstLineItem){
                    String strUniqueIndetifier = string.valueof(objLineItem.OpportunityId)+string.valueof(objLineItem.TRAV_Coverage_Approver_Name__c)+string.valueof(objLineItem.TRAV_URC_Indicator__c);
                    String strRole = objLineItem.TRAV_URC_Indicator__c ? 'URC Approver' : 'Approver';
                    String strUniqueIndetifierTeamMember = string.valueof(objLineItem.OpportunityId) +string.valueof(objLineItem.TRAV_Coverage_Approver_Name__c)+strRole;
                    if(!setUniqueIdentifier.contains(strUniqueIndetifierTeamMember)){
                        if(mapKeyToTeamMember.containsKey(strUniqueIndetifierTeamMember)){
                                OpportunityTeamMember objOpportunityTeamMember = new OpportunityTeamMember();
                                objOpportunityTeamMember = mapKeyToTeamMember.get(strUniqueIndetifierTeamMember);
                                if(objOpportunityTeamMember.UserId == mapOldOpportunityLineItem.get(objLineItem.Id).TRAV_Coverage_Approver_Name__c && objOpportunityTeamMember.TeamMemberRole == (mapOldOpportunityLineItem.get(objLineItem.Id).TRAV_URC_Indicator__c ? 'URC Approver' : 'Approver')){
                                            TeamMemberWrapper objTeamMemberWrapper = new TeamMemberWrapper();
                                            objTeamMemberWrapper.opportunityTeamMemberId = objOpportunityTeamMember.Id;
                                            objTeamMemberWrapper.isDelete = true;
                                            mapTeamMemberWrapper.put(strUniqueIndetifier,objTeamMemberWrapper);
                                    }
                            }

                    }
                 } //End for loop
            }//End of delete check



            List<OpportunityTeamMember> lstTeamMemberToUpsert = new List<OpportunityTeamMember>();
            List<OpportunityTeamMember> lstTeamMemberToDelete = new List<OpportunityTeamMember>();

            if(!mapTeamMemberWrapper.values().isEmpty()){
                for(TeamMemberWrapper objTeamMemberWrapper : mapTeamMemberWrapper.values()){
                    OpportunityTeamMember objTeamMemberToInsert = new OpportunityTeamMember();
                    if(objTeamMemberWrapper.isInsert == true ){
                        objTeamMemberToInsert.TeamMemberRole = objTeamMemberWrapper.TeamRole;
                        objTeamMemberToInsert.OpportunityId = objTeamMemberWrapper.opportunityId;
                        objTeamMemberToInsert.UserId = objTeamMemberWrapper.MemberId;
                        lstTeamMemberToUpsert.add(objTeamMemberToInsert );
                    }
                    OpportunityTeamMember objTeamMemberToDelete = new OpportunityTeamMember();
                    if(objTeamMemberWrapper.isDelete == true){
                        objTeamMemberToDelete.Id = objTeamMemberWrapper.opportunityTeamMemberId;
                        lstTeamMemberToDelete.add(objTeamMemberToDelete );
                    }
                }
            }


            if(!lstTeamMemberToUpsert.isEmpty()){
                Database.SaveResult[] lstSaveResultForUpsert = Database.insert(lstTeamMemberToUpsert, false);
                ExceptionUtility.logDMLError(Constants.OPPORTUNITYSERVICE, METHODNAME, lstSaveResultForUpsert);
            }

            if(!lstTeamMemberToDelete.isEmpty()){
                Database.DeleteResult[] lstSaveResultForDelete = Database.delete(lstTeamMemberToDelete, false);
                ExceptionUtility.logDMLError(Constants.OPPORTUNITYSERVICE, METHODNAME, lstSaveResultForDelete);
            }

        }catch(Exception objExp) {

            ExceptionUtility.logApexException(Constants.OPPORTUNITYLINEITEMSERVICE, METHODNAME, objExp, null);
        }//End of catch
    }//End of Method

    public class TeamMemberWrapper {
        public Id opportunityId {get; set;}
        public Id opportunityTeamMemberId {get; set;}
        public String TeamRole {get; set;}
        public Id MemberId {get; set;}
        public Boolean isInsert {get; set;}
        public Boolean isDelete{get; set;}
        public TeamMemberWrapper (){
            isInsert = false;
            isDelete = false;
        }
    }

     /**
* This method add error on LineItem when Stage is Not Prospect and BU is BSI Relared
* @param newList - List of new values in the for the OpportunityLineItem records.
* @return void - returns void .
*/

    public static void addErrorWhenOpportunityStageSubmission(List<OpportunityLineItem> lstNewOpportunityLineItem){
        String METHODNAME = 'addErrorWhenOpportunityStageSubmission';

        try{
            Set<Id> setUserId = new Set<Id>();
            setUserId.add(UserInfo.getUserId());
            String userProfile = UserSelector.getUserProfileName(setUserId)[0].Profile.Name;
            if(Constants.BIBSIPROFILES.contains(userProfile)){
                for(OpportunityLineItem objLineItem : lstNewOpportunityLineItem ){
                    if(objLineItem.TRAV_Opportunity_Stage__c != Constants.PROSPECT && Constants.setOpportunityBSIBU.contains(objLineItem.TRAV_Business_Unit__c)){
                        objLineItem.addError(Constants.ErrorForProductsOnSubmissionStage);
                    }
                }
            }
        }catch(exception objExp) {
            ExceptionUtility.logApexException(Constants.OPPORTUNITYLINEITEMSERVICE, METHODNAME, objExp, null);
        } //End catch
    }

	 /**
* This method Stamp delete LineItem's Id to TRAV_Opportunity_Product_Delete__c
* @param mapOldItems - List of old values in the for the OpportunityLineItem records.
* @return void - returns void .
*/

    public static void stampOpportunityLineItemIdOnDelete(Map<Id,OpportunityLineItem> mapOldItems){
        String METHODNAME = 'stampOpportunityLineItemIdOnDelete';

        try{
            List<TRAV_Opportunity_Product_Delete__c> lstProductDelete = new List<TRAV_Opportunity_Product_Delete__c>();
            for(OpportunityLineItem objLineItem : mapOldItems.values()){
                TRAV_Opportunity_Product_Delete__c objProductDelete = new TRAV_Opportunity_Product_Delete__c();
                objProductDelete.TRAV_Opportunity_Product_ID__c = objLineItem.Id;
                objProductDelete.TRAV_Object_API_Name__c = 'OpportunityLineItem';
                objProductDelete.TRAV_Object_Label__c = 'Opportunity Product';
                objProductDelete.TRAV_Deleted_On__c = DateTime.now();
                lstProductDelete.add(objProductDelete);
            }

            if(!lstProductDelete.isEmpty()){
                Database.SaveResult[] lstSaveResultForInsert = Database.insert(lstProductDelete, false);
                ExceptionUtility.logDMLError(Constants.OPPORTUNITYLINEITEMSERVICE, METHODNAME, lstSaveResultForInsert);
            }
        }catch(exception objExp) {
            ExceptionUtility.logApexException(Constants.OPPORTUNITYLINEITEMSERVICE, METHODNAME, objExp, null);
        } //End catch
    }
}
