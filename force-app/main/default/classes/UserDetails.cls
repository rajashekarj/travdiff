public without sharing class UserDetails {
    public class TravUserInfo{
        @AuraEnabled
        public String strRecordTypeId  {get;set;}
    }
    @AuraEnabled
    public static TravUserInfo getUserInfo(String adminRecordTypeID){ 
        //String urlInstance=String.valueof(ApexPages.currentPage().getURL());
        //System.debug('myURL'+urlInstance);
        TravUserInfo info = new TravUserInfo();
        String profileId = UserInfo.getProfileId();
        String strProfile = [SELECT Name from Profile where Id = :profileId].Name;
        if(strProfile == 'BI - National Accounts' || strProfile == 'BI - Support' ){
            info.strRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Constants.OPPTYRECORDTYPENEWBUSINESSNA).getRecordTypeId();
        }else if(strProfile == 'System Administrator'){
            info.strRecordTypeId = adminRecordTypeID;
        }else{
            info.strRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Constants.OPPTYRECORDTYPENEWBUSINESS).getRecordTypeId();
        }
        System.debug(info);
        return info;
        
    } 
    @AuraEnabled
    public static Id getRecordTypeId(){
        List<Schema.RecordTypeInfo> infos = Schema.SObjectType.Opportunity.RecordTypeInfos;
        Id defaultRecordTypeId;
        
        //check each one
        for (Schema.RecordTypeInfo info : infos) {
            if (info.DefaultRecordTypeMapping) {
                defaultRecordTypeId = info.RecordTypeId;
            }
        }
        //here is the default Opportunity RecordType Id for the current user
        return defaultRecordTypeId;
    }
}