/**
* This class is used as a Selector Layer for User object. 
* All queries for User object will be performed in this class.
*
*
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer             User Story/Defect    Date               Description
* -----------------------------------------------------------------------------------------------------------              
* Bharat Madaan         US63871             07/31/2019          Original Version
* Erec Lawrie           US77080             02/26/2020          added TRAV_Product_Abbreviation__c
* -----------------------------------------------------------------------------------------------------------
*/
public class Product2Selector {
	/**
* This is a method is used to get list of Products
* @param : Set<product2> ids
* @return List<Product2> List of Products.
*/
    public static list<Product2> getProductsFromId(Set<Id> Ids)
    {
        List<Product2> listProducts = [select Id, 
                               Name,
                               TRAV_Product_Abbreviation__c
                               From Product2
                               where Id IN : Ids];
        return listProducts;         
    }
}