/**
* This class is used as a Selector Layer for Service Partner Matrix metadata . 
* All queries for Service Partner Matrix metadata will be performed in this class.
*
*
* 
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* BHarat Madaan            US62552         06/25/2019       Original Version
* Siddharth Menon		   US68464		   10/07/2019		Added method getSubmissionTeamMemberUserNames
* -----------------------------------------------------------------------------------------------------------
*/
public class ServicePartnerMetadataSelector {
    private final static string CLASSNAME = 'ServicePartnerMetadataSelector';
    /**
* This is a method is used to get list of metadata records
* @param : null
* @return List<TRAV_Service_Partner_Process__mdt>: List of metadata.
*/
    public static list<TRAV_Service_Partner_Process__mdt> getServicePartnerMatrix(){
        List<TRAV_Service_Partner_Process__mdt> listServicePartnerMatrix = [SELECT Id,
                                                                            TRAV_UserName__c,
                                                                            TRAV_Opprtunity_Team_Region__c,
                                                                            TRAV_Opportunity_Team_Role__c,
                                                                            TRAV_Opportunity_BU__c,
                                                                            TRAV_StageName__c,
                                                                            TRAV_Travelers_Office__c
                                                                            from TRAV_Service_Partner_Process__mdt
                                                                            where 
                                                                            TRAV_IS_Manager__c = false 
                                                                            and TRAV_StageName__c != null];
        return listServicePartnerMatrix;
    }
    
    /**
* This is a method is used to get list of metadata records for Submission Phase
* @param : null
* @return List<TRAV_Service_Partner_Process__mdt>: List of metadata.
*/
    public static List<TRAV_Service_Partner_Process__mdt> getSubmissionTeamMemberUserNames() {
        final string METHODNAME = 'getSubmissionTeamMemberUserNames';
        final string submission= Constants.SUBMISSION;
        List<TRAV_Service_Partner_Process__mdt> lstSubmissionMembers = new List<TRAV_Service_Partner_Process__mdt>();
        try {
            lstSubmissionMembers = new List<TRAV_Service_Partner_Process__mdt>([
                SELECT 
                TRAV_UserName__c 
                FROM 
                TRAV_Service_Partner_Process__mdt 
                WHERE 
                TRAV_StageName__c=:submission]);
            if(Test.isRunningTest()) {
                QueryException e = new QueryException();
                e.setMessage('This is a constructed exception for testing and code coverage');
                throw e;
            }
        } //End of try
        catch(QueryException exp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, exp, null);
            
        } // End Catch
        
        return lstSubmissionMembers;
    } //end of method getSubmissionTeamMemberUserNames
}