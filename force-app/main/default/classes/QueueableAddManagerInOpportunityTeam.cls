/**
* Qeueable Class for adding opportunity owner's manager hierarchy as team members
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Isha Shukla		      US63800             06/03/2019       Added this class
*/  
public class QueueableAddManagerInOpportunityTeam implements Queueable {
    Map<Id,Id> mapOpportunityToOwner = new Map<Id,Id>();
    Map<Id,Id> mapOpportunityToOldOwner = new Map<Id,Id>();
    Map<Id,Set<Id>> mapOpportunityToOldTeams;
    Set<Id> deletedUsers;
    String strManagerRole = Label.TRAV_Manager_Team_Role;
    //constructor setting parametre values
    public QueueableAddManagerInOpportunityTeam(Map<Id,Id> mapOpportunityToOwner,Map<Id,Set<Id>> mapOpportunityToOldTeams, Set<Id> deletedUsers,Map<Id,Id> mapOpportunityToOldOwner){
        this.mapOpportunityToOwner = mapOpportunityToOwner;
        this.mapOpportunityToOldTeams = mapOpportunityToOldTeams;
        this.deletedUsers = deletedUsers;
        this.mapOpportunityToOldOwner = mapOpportunityToOldOwner;
    }
    public void execute(QueueableContext context) { 
        final string METHODNAME = 'execute';
        final string CLASSNAME = 'QueueableAddManagerInOpportunityTeam';
        List<OpportunityTeamMember> finalLstTeamMember = new List<OpportunityTeamMember>();
        Map<Id,Set<Id>> mapUserToManagers = new Map<Id,Set<Id>>();//User to Managers map
        List<OpportunityTeamMember> lstOppTeamMembers = new List<OpportunityTeamMember>();
        List<String> lstTeamMemRole = new List<String>();
        
        Set<Id> setUserId = new Set<Id>();
        Set<Id> setUserIDServicePartner = new Set<Id>();
        List<User> lstUsers = new List<User>();
        List<OpportunityTeamMember> lstOpportunityTeamMember;
        Set<String> setOppIdUserId = new Set<String>();
        List<OpportunityTeamMember> lstOppTeamMem = new List<OpportunityTeamMember>();
        Map<Id,List<OpportunityTeamMember>> mapOppIdTeamMemRole = new Map<Id,List<OpportunityTeamMember>>();
        try {
            if(mapOpportunityToOwner.values()!= null && mapOpportunityToOwner.values().size() > 0 )
            	setUserId.addAll(mapOpportunityToOwner.values());
            //lstOppTeamMem = OpportunityTeamMemberSelector.getExistingTeamMem(mapOpportunityToOwner.keySet());
            if(mapOpportunityToOwner.keySet()!= null && mapOpportunityToOwner.keySet().size() > 0)
            	lstOppTeamMem = OpportunityTeamMemberSelector.getExistingTeamMem(mapOpportunityToOwner.keySet());
            //lstOppTeamMembers = OpportunityTeamMemberSelector.oppTeamMemberRole(setUserId);
            if(lstOppTeamMem!=null) {
                for(OpportunityTeamMember oppTeam : lstOppTeamMem) {
                    setOppIdUserId.add(oppTeam.OpportunityId + '_' +oppTeam.UserId);
                     if(oppTeam.TeamMemberRole != Constants.ACCOUNTEXECUTIVE && setUserId.contains(oppTeam.UserId)) {
                        setUserIDServicePartner.add(oppTeam.UserID);
                    }
                }
            }
            /*if(lstOppTeamMem != null) {
                for(OpportunityTeamMember oppTeamMem : lstOppTeamMem) {
                    if(oppTeamMem.TeamMemberRole != Constants.ACCOUNTEXECUTIVE && setUserId.contains(oppTeamMem.UserId)) {
                        system.debug('oppTeamMem>>'+oppTeamMem);
                        setUserIDServicePartner.add(oppTeamMem.UserID);
                    } 
            	}
            }*/
            //Getting opportunity owner's manager hierarchy
            
            if(mapOpportunityToOldTeams == null && mapOpportunityToOldOwner != null){
                Set<Id> oldUsers = new Set<Id>();
                oldUsers.addAll(mapOpportunityToOldOwner.values());
                mapUserToManagers = UserSelector.getManagers(oldUsers);
            }else {
                 mapUserToManagers = UserSelector.getManagersForServicePartners(setUserId);
                /*if(setUserIDServicePartner == null) {
                    mapUserToManagers = UserSelector.getManagers(setUserId);
                }
                else if(setUserIDServicePartner!=null) {
                    mapUserToManagers = UserSelector.getManagersForServicePartners(setUserId);
                }*/
            }
            //Creating opportunity team members
            lstOpportunityTeamMember = OpportunityService.createDeleteOpportunityTeamForManagers(setUserId, mapOpportunityToOwner,mapUserToManagers,null,null);
            for(OpportunityTeamMember oppTeam : lstOpportunityTeamMember) {
                if(!setOppIdUserId.contains(oppTeam.OpportunityId + '_' +oppTeam.UserId)) {
                    finalLstTeamMember.add(oppTeam);
                }
                
            }
            if( !finalLstTeamMember.isEmpty() ) {
                system.debug('finalLstTeamMember>'+finalLstTeamMember);
                Database.SaveResult[] lstSaveResult = Database.insert(finalLstTeamMember, false);
                ExceptionUtility.logDMLError(CLASSNAME, METHODNAME, lstSaveResult);             
            }//End of if
            List<OpportunityTeamMember> lstOpportunityTeamToDelete = new List<OpportunityTeamMember>();
            if( (mapOpportunityToOldTeams != null && mapOpportunityToOldTeams.keySet().size() > 0) || 
               ( mapOpportunityToOldOwner != null && mapOpportunityToOldOwner.keySet().size() > 0) ) {
                //Set<Id> setOldOpportunitys = new Set<Id>();
                //setOldOpportunitys.addAll(mapOpportunityToOldTeams.keySet());
                if(deletedUsers == null && mapOpportunityToOldOwner != null ){
                     deletedUsers = new Set<Id>(); 
                    deletedUsers.addAll(mapOpportunityToOldOwner.values());
                    
                }
                if(mapOpportunityToOldTeams == null && mapUserToManagers != null){
                    mapOpportunityToOldTeams = new Map<Id,Set<Id>>();
                    for(Id OpptyId : mapOpportunityToOldOwner.keySet()){
                        mapOpportunityToOldTeams.put(OpptyId,new Set<Id>{mapOpportunityToOldOwner.get(OpptyId)});
                        if(mapUserToManagers.containsKey(mapOpportunityToOldOwner.get(OpptyId))){
                           Set<Id> managersSet = mapUserToManagers.get(mapOpportunityToOldOwner.get(OpptyId));
                           mapOpportunityToOldTeams.get(OpptyId).addAll(managersSet); 
                        }
                    }
                } 
                List<OpportunityTeamMember> lstOpportunityTeam = new List<OpportunityTeamMember>();
                //Getting all the manager opportunity team members
                //lstOpportunityTeam = OpportunityTeamMemberSelector.getOpportunityTeamManagers(mapOpportunityToOldOwner.keySet(),setOldOwners);
                lstOpportunityTeam = OpportunityTeamMemberSelector.getAllExistingByOpportunityId(mapOpportunityToOldTeams.keySet());
                //Getting list of Opportunity team member needs to be deleted
                lstOpportunityTeamToDelete = OpportunityService.createDeleteOpportunityTeamForManagers(deletedUsers, mapOpportunityToOwner,mapUserToManagers,lstOpportunityTeam,mapOpportunityToOldTeams);
                
                //Deletion of temas
                if(!lstOpportunityTeamToDelete.isEmpty()) {
                    Database.DeleteResult[] lstDeleteResult = Database.delete(lstOpportunityTeamToDelete, false);
                    ExceptionUtility.logDeletionError(CLASSNAME,METHODNAME,lstDeleteResult);
                }
            }            
        }//End of try
        catch(exception objExp) {
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        }//End of catch
        
    }//End of method execute
}