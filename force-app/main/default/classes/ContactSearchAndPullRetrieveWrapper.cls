/**
* This class is the wrapper class for getContact API Response
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect   		Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty        US69347/US69863          11/10/2019      Original Version
* -----------------------------------------------------------------------------------------------------------
*/
public class ContactSearchAndPullRetrieveWrapper {
public class SrcInfo {
    @AuraEnabled
    public Src src;
}

public class BMLoc {
    @AuraEnabled
    public String bmLocId;
    @AuraEnabled
    public String agcyNm;
    @AuraEnabled
    public String addrLn1;
    @AuraEnabled
    public String city;
    @AuraEnabled
    public String st;
}
@AuraEnabled
public String Status;
@AuraEnabled
public String agencyIndvId;
@AuraEnabled
public List<BMLoc> BMLoc;
@AuraEnabled
public List<RoleCdArr> roleCdArr;
@AuraEnabled
public List<RuArr> ruArr;
@AuraEnabled
public List<RuArr> spclCdArr;
@AuraEnabled
public String fullNm;
@AuraEnabled
public String fstNm;
@AuraEnabled
public String lstNm;
@AuraEnabled
public String busEmailAddr;
@AuraEnabled
public String busTelNbr;
@AuraEnabled
public String licInd;
@AuraEnabled
public Object licCd;
@AuraEnabled
public Object natlPrdrNbr;
@AuraEnabled
public String ivldEmailInd;
@AuraEnabled
public String sysAcssInd;
@AuraEnabled
public String agcyIndvSts;
@AuraEnabled
public String matchSystem;
@AuraEnabled
public String lastUpdatedUser;
@AuraEnabled
public List<SrcInfo> srcInfo;

public class Src {
    @AuraEnabled
    public String srcSysCd;
    @AuraEnabled
    public String srcId;
}

public class RuArr {
}

public class RoleCdArr {
    @AuraEnabled
    public String roleCd;
    @AuraEnabled
    public String roleShrtDesc;
    @AuraEnabled
    public String roleLngDesc;
    @AuraEnabled
    public String roleClssCd;
    
}



}