/**
 * This class is used as a Selector Layer for Carrier object. 
 * All queries for Carrier object will be performed in this class.
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shruti Gupta            US68423        	 09/24/2019       Original Version
*/
public class CarrierSelector {
/**
* This is a method is used to get Carrier with name as carrier
* @param : null
* @return Carrier record.
*/
    public static TRAV_Carrier__c getCarrier()
    {
        TRAV_Carrier__c objCarrier = [SELECT Id, 
                               Name
                               FROM TRAV_Carrier__c 
                               WHERE Name = 'TRAVELERS'
                               LIMIT 1];
        return objCarrier;         
    } // End of method getCarrier
}