/**
 * Handler Class for Content Version Trigger. This class won't have any logic and
 * will call other Note Service/Utilities class.
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shashank Agarwal       US62549              07/01/2019       Added code in afterInsert
 * -----------------------------------------------------------------------------------------------------------
 */
public class ContentVersionTriggerHandler implements ITriggerHandler{
//Handler Method for Before Insert.
    public void beforeInsert(List<sObject> newItems) {
       
    }
    
    //Handler Method for Before Update.
    public void beforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
        List<ContentVersion> lstNote = newItems.values();
        ContentVersionService.addErrorLyncOnNoteModified(lstNote,(Map<Id,ContentVersion>)oldItems);
    }
    //Handler Method for Before Delete.    
    public void beforeDelete(Map<Id, sObject> oldItems) {
        
    }
    
    //Handler method for After Insert.
    public void afterInsert(Map<Id, sObject> newItems) {
    }

    //Handler method for After Update.    
    public void afterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {  
        
    }
    //Handler method for After Delete    
    public void afterDelete(Map<Id, sObject> oldItems) {
    
    }
    
    //Handler method for After Undelete.
    public void afterUndelete(Map<Id, sObject> oldItems) {
    
    } 

}