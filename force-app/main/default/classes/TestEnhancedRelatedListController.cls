@isTest
public class TestEnhancedRelatedListController {
    
    
    @isTest
    public static void testGetData()
    {   
        
        Account objProspect= TestDataFactory.createProspect();
        insert objProspect;
        
        
        Id recordTypeIdforNonLicensed = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Agency Location Licensed Contact').getRecordTypeId();
        Id recordTypeIdforCustomer = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        List<Contact> lstCon = new List<Contact>();
        for(integer i=0; i <5; i++){
            
            Contact objContact = new Contact();
            objContact.LastName = 'LastName' + i ;
            objContact.AccountId = objProspect.Id;
            objContact.RecordTypeId = recordTypeIdforNonLicensed;
            lstCon.add(objContact);
            
               Contact objContact1 = new Contact();
            objContact1.LastName = 'LastNameCust' + i ;
            objContact1.AccountId = objProspect.Id;
            objContact1.RecordTypeId = recordTypeIdforCustomer;
            lstCon.add(objContact1); 
        }
        
        insert lstCon; 
        EnhancedRelatedListController.TableDataWrapper objWrapper= EnhancedRelatedListController.getData('Contact', 'LastName', 'RecordType.DeveloperName = \'Customer_Contact\'', objProspect.Id, 'AccountId', 'CustomerContacts',0);
        EnhancedRelatedListController.TableDataWrapper objWrapper1= EnhancedRelatedListController.getData('AccountContactRelation', 'Contact.Name', 'Contact.RecordType.DeveloperName = \'TRAV_Agency_Licensed_Contact\' OR Contact.RecordType.DeveloperName = \'TRAV_Agency_Non_Licensed_Contact\'', objProspect.Id, 'AccountId', 'BrokerCustomerAccount',0);
        system.debug(objWrapper);
        system.debug(objWrapper1);
        System.assertEquals(objWrapper.lstValues.size(),5);
        System.assertEquals(objWrapper1.lstValues.size(),5);
        
        EnhancedRelatedListController.deleteRecord(lstCon[9].Id);
        EnhancedRelatedListController.getParentRecord(objProspect.Id, 'Account');
        EnhancedRelatedListController.getUserProfile();
        
        
    }        
}