/** 
 * This class is used as a Selector Layer for Contact object. 
 * All queries for Contact object will be performed in this class.
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * ----------------------------------------------------------------------------------------------------------              
 * Bharat Madaan            US65898        07/24/2019       Original Version
*/ 
public class AccountContactRelationGenerator { 
 
/**
* This is a method is used to get list of Contacts having a Account lookup
* @param : null 
* @return List<AccountContactRelation> List of AccountContactRelation.
*/
 public static void generateACR (List<AccountContactRelationWrapper> lstACRWrap){
     final String CLASSNAME = 'AccountContactRelationGenerator';
     final String METHODNAME = 'generateACR';
     List<AccountContactRelation> lstInsertACR = new List<AccountContactRelation>();
     List<AccountContactRelation> lstDeleteACR = new List<AccountContactRelation>();
     try{
         if( !lstACRWrap.isEmpty()){
     for(AccountContactRelationWrapper acrWrap : lstACRWrap){
         if(acrWrap.isInsert){
         AccountContactRelation objACR = new AccountContactRelation();
         System.debug('Account ===' + acrWrap.accountId + 'Contact ===' + acrWrap.contactId);
         objACR.AccountId  = acrWrap.accountId;
         objACR.ContactId = acrWrap.contactId;
         lstInsertACR.add(objACR);
         }
         if(acrWrap.isDelete){
         AccountContactRelation objACR = new AccountContactRelation();
         System.debug('Account for delete===' + acrWrap.accountId + 'Contact for delete===' + acrWrap.contactId);
         objACR.Id = acrWrap.acrId;
         lstDeleteACR.add(objACR);  
         }
     }
     if(!lstInsertACR.isEmpty()) {
     Database.insert(lstInsertACR);
     }
     if(!lstDeleteACR.isEmpty()) {
     Database.delete(lstDeleteACR);
     }
     }
          if(Test.isRunningTest()) {
                    CalloutException e = new CalloutException();
                    e.setMessage('This is a constructed exception for testing and code coverage');
                    throw e;
                }
     }
     catch(Exception objExp){
         ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
     }
 }
    
}
