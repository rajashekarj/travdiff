/**
 * Test class for Policy Trigger Utility

*
 * @Author : Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect                             Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Sayanka Mohanty        US666430/US66750/US66752                    09/09/2019       Original Version
 * Tejal Pradhan          US67699                                     09/09/2019       Method added- testUpdateAccountRecordType
 * -----------------------------------------------------------------------------------------------------------
 */
@isTest
public class Test_PolicyTriggerUtility {
    @testSetup static void createTestData(){
        //Create Account
        Account objAcc = TestDataFactory.createProspect();
        insert objAcc;
        system.assert(objAcc.Id != null);
        
        //Create Opportunity
        List<Opportunity> lstOpptyFinal = new List<Opportunity>();
        Opportunity objO1 = TestDataFactory.createOppty('Deferred', 'Test oppty A',objAcc.Id);
        lstOpptyFinal.add(objO1);
        if(!lstOpptyFinal.isEmpty()){
            insert lstOpptyFinal;
        }
        system.assert(lstOpptyFinal[0].StageName == 'Deferred');
        //Create Policy
        List<InsurancePolicy> lstPolicy = new List<InsurancePolicy>();
        //Below Policiy is for Non - Renewal
        lstPolicy = TestDataFactory.createPolicies(objO1.Id, 'NR1', objAcc.Id, 1);
        system.assert(lstPolicy.size() >0);
        if(!lstPolicy.isEmpty()){
            insert lstPolicy;
        }
        //Create a user
        User u = TestDataFactory.createTestUser('BI - Support', 'James Lewis', 'Lewis');
        if(u != null){
            insert u;
        }
    }
    static testmethod void testNonRenewalPolicy(){
        //Unit test for US66750, Transaction Type =  NR; StageName IN Closed ; Close reason = Non-renewal; Account != null
        Test.startTest();
         InsurancePolicy p = [SELECT Id, TRAV_Transaction_Type_Code__c,
                            TRAV_Opportunity__r.StageName, 
                            TRAV_Opportunity__r.TRAV_Close_Reason__c FROM InsurancePolicy LIMIT 1 ];
        if(p != null){
            p.TRAV_Transaction_Type_Code__c = 'NR';
            p.TRAV_Opportunity__r.TRAV_Close_Reason__c = 'Non-renewal';
            update p;
        }
        system.assert(p.TRAV_Transaction_Type_Code__c == 'NR');
        Test.stopTest();
    }
    static testmethod void testUWPolicyChange(){
        //Unit test for US66752, Change Policy Owner
        Test.startTest();
        InsurancePolicy p = [SELECT Id, OwnerId,TRAV_Previous_Owner__c FROM InsurancePolicy LIMIT 1 ];
        if(p != null){
            user u = [SELECT Id From User where LastName = 'Lewis' AND isActive = true LIMIT 1];
            p.OwnerId = u.Id;
            update p;
            system.assert(p.OwnerId == u.Id);
        }
        
        Test.stopTest();
    }
    static testmethod void testPolicyOnInsert(){
        //Unit test for Policy Insert
        Test.startTest();
        Opportunity objOpp = [SELECT Id FROM Opportunity LIMIT 1];
        Account objAcc = [SELECT Id FROM Account LIMIT 1];
        List<InsurancePolicy> lstPolicy = TestDataFactory.createPolicies(objOpp.Id, 'NR', objAcc.Id, 1);
        system.assert(lstPolicy.size() > 0);
        if(!lstPolicy.isEmpty()){
            insert lstPolicy;
        }
        //Covering handler scenarios
        List<InsurancePolicy> lstPolicy1 = TestDataFactory.createPolicies(objOpp.Id, 'NR1', objAcc.Id, 1);
        system.assert(lstPolicy.size() > 0);
        if(!lstPolicy1.isEmpty()){
            insert lstPolicy1;
        }
            delete lstPolicy1;
            undelete lstPolicy1;
        Test.stopTest();
    }
    
    static testmethod void testUpdateAccountRecordType(){
        //Unit test for account record type 
        Test.startTest();
        Account objAcc = TestDataFactory.createProspect();
        insert objAcc;
        Opportunity objOpp = TestDataFactory.createOppty('Submission','testObjOpp',objAcc.Id);
        insert objOpp;
        List<InsurancePolicy> lstPolicy = TestDataFactory.createPolicies(objOpp.Id, 'NR', objAcc.Id, 1);
        insert lstPolicy;
        Account objUpdatedAcc = [SELECT Id, RecordType.DeveloperName FROM Account WHERE Id = :lstPolicy[0].NameInsuredId];
        //System.assertEquals('TRAV_Client',objUpdatedAcc.RecordType.DeveloperName);
        Test.stopTest();
    }
}