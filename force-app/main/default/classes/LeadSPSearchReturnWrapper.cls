/**
* This class will return list of results after hitting Search API
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect                  Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty        US73994/US73995/US73996          24/01/2020      Original Version
* -----------------------------------------------------------------------------------------------------------
*/
public class LeadSPSearchReturnWrapper {
    @auraEnabled
    public String strbusinessName{get;set;}
    @auraEnabled
    public String strDba{get;set;}
    @auraEnabled
    public String strCbeId{get;set;}
    @auraEnabled
    public String strAddress{get;set;}
    @auraEnabled
    public String strBusinessURL{get;set;}
    @auraEnabled
    public String strDunsNumber{get;set;}
    @auraEnabled
    public String strOrgSICCd{get;set;}
    @auraEnabled
    public String strOrgNAICSCd{get;set;}
    @auraEnabled
    public String strOrgSICDesc{get;set;}
    @auraEnabled
    public String strOrgNAICSDesc{get;set;}
    @auraEnabled
    public String strYearEstb{get;set;}
    @auraEnabled
    public String strRevenue{get;set;}
    @auraEnabled
    public Id idSF{get;set;}
    @auraEnabled
    public String leadAccountType{get;set;}
    
}