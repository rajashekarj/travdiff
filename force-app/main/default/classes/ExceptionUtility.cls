/**
 * This is a queueable class which is used to capture runtime exceptions. Exceptions are then stored  
 * in a custom object for reporting/backtracking the error.
 *
 * @Author Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shashank Shastri       US59672              05/15/2019       Original Version
 * Isha Shukla		      US60806			   06/03/2019		Added method logDeletionError
 * Shashank Agarwal		  US59903			   06/19/2019		Added method logDMLError for Upsert
 * -----------------------------------------------------------------------------------------------------------
 */
public class ExceptionUtility implements Queueable{   

    //stores the class name where the exception was thrown
    public String strClassName;
    //stores the method name where the exception was thrown
    public String strMethodName;
    // instance of the exception thrown  
    public Exception exp;
    //response if a callout was made
    public HttpResponse response; 
    // Database.UpsertResult if a bulk DML operation was performed.
    public List<Database.SaveResult> lstSaveResult; 

    /**
    * Constructor : to instantiate the class and pass params from the parent class where the exception is thrown
    * @param strClassName Apex Class name where the exception was thrown
    * @param strMethodName Method name which thorws the exception
    * @param exp Instance of the exception thrown
    * @param response Callout Response if exception thrown is due to an integration failure
    * @param lstSaveResult List of Saveresult when bulk DML was performed
    */
    public ExceptionUtility(String strClassName, String strMethodName, Exception exp, 
                            HttpResponse response, List<Database.SaveResult> lstSaveResult){

        //set the classname from param to the class variable
        this.strClassName = strClassName; 
        //set the strMethodName from param to the class variable
        this.strMethodName = strMethodName; 
        //set the exception from param to the class variable
        this.exp = exp; 
        //set the responsefrom param to the class variable
        this.response = response; 
        //set the DML List from param to the class variable
        this.lstSaveResult = lstSaveResult; 
    }
    
    /**
    * Queueable Interface execute method override
    * @param context Queueable Context variable
    */
    public void execute(QueueableContext context) {
        
        //Check to Bypass/Skip The Exceptions log orwide or for Individuals Users/Profiles.
        if(!TRAV_Bypass_Settings__c.getOrgDefaults().TRAV_Skip_Exception_Log__c){
            if(!TRAV_Bypass_Settings__c.getInstance(UserInfo.getProfileId()).TRAV_Skip_Exception_Log__c &&
               !TRAV_Bypass_Settings__c.getInstance(UserInfo.getUserId()).TRAV_Skip_Exception_Log__c){
                //If lstSaveResult is empty then invoke the logApexException method. 
                //Else invoke the logDMLException method.
                if(lstSaveResult == null){
                    logApexException(this.strClassName, this.strMethodName, this.exp, this.response);
                } //End - if
                else{
                    //capturing DML Errors
                    logDMLError(this.strClassName, this.strMethodName, this.lstSaveResult);    
                } //End - else
            } //End - if
        } // End - if
    } // End execute
    
    /**
    * This method is invoked when an Apex class throws an exception.
    * @param strClassName Apex Class name where the exception was thrown
    * @param strMethodName Method name which thorws the exception
    * @param exp Instance of the exception thrown
    * @param response Callout Response if exception thrown is due to an integration failure
    */
    public static void logApexException(String strClassName, String strMethodName, Exception exp, 
                                        HttpResponse response){
        
        //Exception Log object
        TRAV_Exception_Log__c log = new TRAV_Exception_Log__c();
        
        log.Class_Name__c = strClassName;
        log.Method_Name__c = strMethodName;
        log.Log_Type__c = 'Application';
        if(exp != null){
            log.Line__c = string.valueOf(exp.getLineNumber());
            if(exp.getMessage().length()>255){
                log.Message__c = exp.getMessage().substring(0,255);
                 log.Error_Message__c = exp.getStacktraceString()+'---'+exp.getMessage();
            }
            else{
                log.Message__c = exp.getMessage();
                log.Error_Message__c = exp.getStacktraceString();
            }
           
        } //End - if

        //If it is a callout error then store the error status code and the response body in the log.
        if(response != null){
            log.Error_Message__c = response.getBody();
            log.HTTP_Status_code__c = string.valueOf(response.getStatusCode());
            log.Log_Type__c = 'Integration';
        } //End - if (callout error code segment)

        //Inserting Log record
        Insert log;
    } //End logApexException
    
    /**
    * This Method is invoked when a buld DML operation is processed and the error records are to be captured.
    * @param strClassName Apex Class name where the exception was thrown
    * @param strMethodName Method name which thorws the exception
    * @param lstSaveResult List of SaveResult records for which DML was performed
    */
    public static String logDMLError(String strClassName, String strMethodName, List<Database.SaveResult> lstSaveResult){

        //List to store log records to insert.
        List<TRAV_Exception_Log__c> logList = new List<TRAV_Exception_Log__c>();
        //Exception Log object
        TRAV_Exception_Log__c log;

        //Iterating over the list of DML rows.
        for(Database.SaveResult upsertResult : lstSaveResult){
            //Use isSuccess() method of Database.UpsertResult class to verify if DML is successful or not
            if (!upsertResult.isSuccess()){//DML Failed on the record
                //Create a new Exception Log record.
                log = new TRAV_Exception_Log__c();
                log.Error_Message__c = String.valueOf(getErrorString(upsertResult.getErrors()));
                log.Class_Name__c = strClassName;
                log.Method_Name__c = strMethodName;
                if(upsertResult.getId() != null){
                    if((String.valueOf(upsertResult.getId().getSobjectType()) + ' ' + upsertResult.getId()).length()>255)
                    {
                        log.Message__c = (String.valueOf(upsertResult.getId().getSobjectType()) + ' ' + upsertResult.getId()).substring(0,255);
                        log.Error_Message__c+='---'+String.valueOf(upsertResult.getId().getSobjectType()) + ' ' + upsertResult.getId();
                     }
                    else
                    log.Message__c = String.valueOf(upsertResult.getId().getSobjectType()) + ' ' + upsertResult.getId();
                } //End - if
                logList.add(log);                    
            } //End of If. All the failed DML records processed.
        } //End - for
        
        if(!logList.isEmpty()){
            Database.insert(logList);
        } //End - if
        if(log != null) {
           return log.Error_Message__c; 
        }else {
            return '';
        }
    } //End logDMLError
    
     /**
    * This Method is invoked when a buld DML operation(UpsertCase) is processed and the error records are to be captured.
    * @param strClassName Apex Class name where the exception was thrown
    * @param strMethodName Method name which thorws the exception
    * @param lstSaveResult List of SaveResult records for which DML was performed
    */
    public static void logDMLError(String strClassName, String strMethodName, List<Database.UpsertResult> lstUpsertResult){

        //List to store log records to insert.
        List<TRAV_Exception_Log__c> logList = new List<TRAV_Exception_Log__c>();
        //Exception Log object
        TRAV_Exception_Log__c log;

        //Iterating over the list of DML rows.
        for(Database.UpsertResult upsertResult : lstUpsertResult){
            //Use isSuccess() method of Database.UpsertResult class to verify if DML is successful or not
            if (!upsertResult.isSuccess()){//DML Failed on the record
                //Create a new Exception Log record.
                log = new TRAV_Exception_Log__c();
                log.Error_Message__c = String.valueOf(getErrorString(upsertResult.getErrors()));
                log.Class_Name__c = strClassName;
                log.Method_Name__c = strMethodName;
                if(upsertResult.getId() != null){
                     if((String.valueOf(upsertResult.getId().getSobjectType()) + ' ' + upsertResult.getId()).length()>255)
                    {
                      log.Message__c=(String.valueOf(upsertResult.getId().getSobjectType()) + ' ' + upsertResult.getId()).substring(0,255);  
                    	 log.Error_Message__c+='---'+String.valueOf(upsertResult.getId().getSobjectType()) + ' ' + upsertResult.getId();
                    }
                    else
                    log.Message__c = String.valueOf(upsertResult.getId().getSobjectType()) + ' ' + upsertResult.getId();
                } //End - if
                logList.add(log);                    
            } //End of If. All the failed DML records processed.
        } //End - for
        
        if(!logList.isEmpty()){
            Database.insert(logList);
        } //End - if
    } //End logDMLError
    
     /**
    * This Method is invoked when a buld DML operation(delete) is processed and the error records are to be captured.
    * @param strClassName Apex Class name where the exception was thrown
    * @param strMethodName Method name which thorws the exception
    * @param lstSaveResult List of SaveResult records for which DML was performed
    */
    public static void logDMLError(String strClassName, String strMethodName, List<Database.DeleteResult> lstDeleteResult){

        //List to store log records to insert.
        List<TRAV_Exception_Log__c> logList = new List<TRAV_Exception_Log__c>();
        //Exception Log object
        TRAV_Exception_Log__c log;

        //Iterating over the list of DML rows.
        for(Database.DeleteResult objDeleteResult : lstDeleteResult){
            //Use isSuccess() method of Database.UpsertResult class to verify if DML is successful or not
            if (!objDeleteResult.isSuccess()){//DML Failed on the record
                //Create a new Exception Log record.
                log = new TRAV_Exception_Log__c();
                log.Error_Message__c = String.valueOf(getErrorString(objDeleteResult.getErrors()));
                log.Class_Name__c = strClassName;
                log.Method_Name__c = strMethodName;
                if(objDeleteResult.getId() != null){
                    if((String.valueOf(objDeleteResult.getId().getSobjectType()) + ' ' + objDeleteResult.getId()).length()>255)
                    {
                       log.Message__c=(String.valueOf(objDeleteResult.getId().getSobjectType()) + ' ' + objDeleteResult.getId()).substring(0,255);
                        log.Error_Message__c+='---'+String.valueOf(objDeleteResult.getId().getSobjectType()) + ' ' + objDeleteResult.getId();
                    }
                    else
                    log.Message__c = String.valueOf(objDeleteResult.getId().getSobjectType()) + ' ' + objDeleteResult.getId();
                } //End - if
                logList.add(log);                    
            } //End of If. All the failed DML records processed.
        } //End - for
        
        if(!logList.isEmpty()){
            Database.insert(logList);
        } //End - if
    } //End logDMLError
    
    /**
    * This Method is invoked when a buld DML operation is processed and the error records are to be captured.
    * @param strClassName Apex Class name where the exception was thrown
    * @param strMethodName Method name which thorws the exception
    * @param lstSaveResult List of SaveResult records for which DML was performed
    */
    public static void logDMLError(String strClassName, String strMethodName, List<Database.LeadConvertResult> lstLeadConvert){

        //List to store log records to insert.
        List<TRAV_Exception_Log__c> logList = new List<TRAV_Exception_Log__c>();
        //Exception Log object
        TRAV_Exception_Log__c log;

        //Iterating over the list of DML rows.
        for(Database.LeadConvertResult leadConvertResult : lstLeadConvert){
            //Use isSuccess() method of Database.UpsertResult class to verify if DML is successful or not
            if (!leadConvertResult.isSuccess()){//DML Failed on the record
                //Create a new Exception Log record.
                log = new TRAV_Exception_Log__c();
                log.Error_Message__c = String.valueOf(getErrorString(leadConvertResult.getErrors()));
                log.Class_Name__c = strClassName;
                log.Method_Name__c = strMethodName;
                if(leadConvertResult.getLeadId() != null){
                    if((String.valueOf(leadConvertResult.getLeadId().getSobjectType()) + ' ' + leadConvertResult.getLeadId()).length()>255)
                    {
                        log.Message__c=(String.valueOf(leadConvertResult.getLeadId().getSobjectType()) + ' ' + leadConvertResult.getLeadId()).substring(0,255);
                    	log.Error_Message__c+='---'+String.valueOf(leadConvertResult.getLeadId().getSobjectType()) + ' ' + leadConvertResult.getLeadId();
                    }
                    else
                    log.Message__c = String.valueOf(leadConvertResult.getLeadId().getSobjectType()) + ' ' + leadConvertResult.getLeadId();
                } //End - if
                logList.add(log);                    
            } //End of If. All the failed DML records processed.
        } //End - for
        
        if(!logList.isEmpty()){
            Database.insert(logList);
        } //End - if
    } //End logDMLError
    
    
    
    /**
    * This Method is invoked when a buld DML operation is processed and the error records are to be captured.
    * @param lstDmlErrors List of Database.Error records
    */
    public static String getErrorString(List<Database.Error> lstDmlErrors){
        
        //String to store a consolidated string with all errors in case of dml exception.
        String strError = ''; 

        for(Database.Error errorRec : lstDmlErrors){
            strError += errorRec.getMessage()+'. \n'; //Adding a new line after each error
        } //End - for

        return strError;
    } //End getErrorString
    
    /**
    * This Method is invoked when a bulk Delete DML operation is processed and the error records are to be captured.
    * @param strClassName Apex Class name where the exception was thrown
    * @param strMethodName Method name which thorws the exception
    * @param lstDeleteResult List of DeleteResult records for which DML was performed
    */
    public static void logDeletionError(String strClassName, String strMethodName, List<Database.DeleteResult> lstDeleteResult){

        //List to store log records to insert.
        List<TRAV_Exception_Log__c> logList = new List<TRAV_Exception_Log__c>();
        //Exception Log object
        TRAV_Exception_Log__c log;

        //Iterating over the list of DML rows.
        for(Database.DeleteResult delResult : lstDeleteResult){
            //Use isSuccess() method of Database.DeleteResult class to verify if DML is successful or not
            if (!delResult.isSuccess()){//DML Failed on the record
                //Create a new Exception Log record.
                log = new TRAV_Exception_Log__c();
                log.Error_Message__c = String.valueOf(getErrorString(delResult.getErrors()));
                log.Class_Name__c = strClassName;
                log.Method_Name__c = strMethodName;
                if(delResult.getId() != null){
                    if((String.valueOf(delResult.getId().getSobjectType()) + ' ' + delResult.getId()).length()>255)
                    {
                        log.Message__c=(String.valueOf(delResult.getId().getSobjectType()) + ' ' + delResult.getId()).substring(0,255);
                        log.Error_Message__c+='---'+String.valueOf(delResult.getId().getSobjectType()) + ' ' + delResult.getId();
                    }
                    else
                    log.Message__c = String.valueOf(delResult.getId().getSobjectType()) + ' ' + delResult.getId();
                } //End - if
                logList.add(log);                    
            } //End of If. All the failed DML records processed.
        } //End - for
        
        if(!logList.isEmpty()){
            Database.insert(logList);
        } //End - if
    } //End logDMLError
    /**
* This method is invoked when error occurs in External system
* @param String strClassName, String strMethodName,String strErrorFromExternalSystem
* @return void
*/
    public static void logIntegrationerror(String strClassName, String strMethodName,String strErrorFromExternalSystem){
        TRAV_Exception_Log__c objException = new TRAV_Exception_Log__c();
        objException.Class_Name__c = strClassName;
        objException.Method_Name__c = strMethodName;
        objException.Error_Message__c = strErrorFromExternalSystem;
        if(objException != null){
            Database.insert(objException);
        }
        
    }
}