@isTest
public class testUserFieldValidation {
    Static ID testUserID;
    @testsetup
    static void setupTestData()
    {
        Profile pf= [Select Id from profile where Name='System Administrator']; 
        User testuser = new User();
        testuser.Email = 'test@travelers.com';
        testuser.FirstName = 'Test';
        testuser.LastName = 'User';
        testuser.EmailPreferencesAutoBcc = true;
        testuser.SenderEmail = 'test@travelers.com';
        testuser.ProfileId = pf.Id;
        
        String dateString=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;
        Integer RandomId=Integer.valueOf(Math.rint(Math.random()*1000000)); 
        String uniqueName=UserInfo.getOrganizationId()+dateString+RandomId; 
        testuser.Username = uniqueName + '@test' + UserInfo.getOrganizationId() + '.org';
        testuser.EmailEncodingKey = 'ISO-8859-1';
        testuser.Alias = uniqueName.substring(18, 23);
        testuser.TimeZoneSidKey = 'America/Los_Angeles'; 
        testuser.LocaleSidKey = 'en_US';
        testuser.LanguageLocaleKey = 'en_US';
        insert testuser;
        testUserID = testuser.Id;
    }
    @istest
    private static void testEmailPrefrences()
    {
        setupTestData();
        Test.startTest();
        User EmailChangeUser = [select ID, EmailPreferencesAutoBcc,SenderEmail,SenderName from user where id =:testUserID ];
        EmailChangeUser.SenderEmail = 'test1@travelers.com';
        EmailChangeUser.EmailPreferencesAutoBcc = false;
        EmailChangeUser.SenderName = 'test1@travelers.com';
        update EmailChangeUser;
        Test.stopTest();
        
        User EmailChangeUserAfterUpdate = [select ID, EmailPreferencesAutoBcc,SenderEmail,SenderName,firstName,lastname from user where id =:testUserID ];
        System.debug('updated email'+EmailChangeUserAfterUpdate.SenderEmail);
        System.assertEquals(EmailChangeUserAfterUpdate.EmailPreferencesAutoBcc,true);
        System.assertEquals(EmailChangeUserAfterUpdate.SenderEmail,null);
        System.assertEquals(EmailChangeUserAfterUpdate.SenderNAme,EmailChangeUserAfterUpdate.FirstName + ' ' + EmailChangeUserAfterUpdate.LastName);
    }

}