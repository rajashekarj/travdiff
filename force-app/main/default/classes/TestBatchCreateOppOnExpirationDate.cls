/**
* Test class for BatchCreateOpportunityOnExpirationDate Class
*
* @Author : Deloitte Consulting
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Shashank Agarwal       US59991              06/27/2019       Original Version
* -------------------------------------------------------------------------------------------------------------
*/
@isTest
private class TestBatchCreateOppOnExpirationDate {

    static testmethod void testDeferredOpportunityCloneCase(){
        List<Opportunity> lstOpptyForRenewal;
        TestDataFactory.createRenewalBatchsetting();
        List<OpportunityLineItem> lstOpportunityLineItems = new List<OpportunityLineItem>();
        //Adding bypass custom setting to bypass validation rule for testing this batch
        TRAV_Bypass_Settings__c objCustomSettingBypass = new TRAV_Bypass_Settings__c();
        objCustomSettingBypass.TRAV_Skip_Validation_Rules__c = true;
        insert objCustomSettingBypass;
        User objUsr2;
        System.runAs(new User(Id = Userinfo.getUserId())){
            objUsr2 = TestDataFactory.createTestUser('BI - National Accounts','TestNA','LastNameNA');
            insert objUsr2;
        }
        Date todayDate = Date.today().addDays(150);
        System.runAs(objUsr2){
            //create account record
            Account objProspectAccount = TestDataFactory.createProspect();
            List<Account> lstAccount = new List<Account>{objProspectAccount};
                insert lstAccount;
            TRAV_Carrier__c objCarrier = new TRAV_Carrier__c();
            objCarrier.Name = 'Travelers';
            insert objCarrier;
            Account agency2 = TestDataFactory.createAgency();
            insert agency2;

            Account agency = TestDataFactory.createAgency();
            insert agency;

            //Creating Opportunity Records
            lstOpptyForRenewal = TestDataFactory.createOpportunities(3,objProspectAccount.Id, 'BI-NA');
            lstOpptyForRenewal[0].TRAV_Defferal_Date__c = System.today();
            lstOpptyForRenewal[0].StageName = 'Deferred';
            lstOpptyForRenewal[0].TRAV_Renewal_Creation__c = System.today();
            lstOpptyForRenewal[0].TRAV_Prospect_Effective_Date__c = System.today();
            lstOpptyForRenewal[0].TRAV_Agency_Broker__c = agency2.Id;
            lstOpptyForRenewal[0].TRAV_Policy_Expiration_Date__c = System.today();
            lstOpptyForRenewal[0].TRAV_Non_Renewal_Advisement_Indicator__c = false;
            lstOpptyForRenewal[1].TRAV_Renewal_Creation__c = Date.today().addDays(-1);
            lstOpptyForRenewal[1].TRAV_Agency_Broker__c = agency2.Id;
            lstOpptyForRenewal[1].TRAV_Policy_Expiration_Date__c = System.today();
            lstOpptyForRenewal[1].TRAV_Prospect_Effective_Date__c = System.today();
            lstOpptyForRenewal[1].TRAV_Multi_Year_Deal_Indicator__c = 'test/test2';
            lstOpptyForRenewal[1].TRAV_Deal_Years__c = 8;
            lstOpptyForRenewal[1].TRAV_Non_Renewal_Advisement_Indicator__c = false;
            lstOpptyForRenewal[1].StageName = 'Written';
            lstOpptyForRenewal[2].TRAV_Defferal_Date__c = System.today();
            lstOpptyForRenewal[2].StageName = 'Deferred';
            lstOpptyForRenewal[2].TRAV_Renewal_Creation__c = System.today();
            lstOpptyForRenewal[2].TRAV_Agency_Broker__c = agency2.Id;
            lstOpptyForRenewal[2].TRAV_Policy_Expiration_Date__c = System.today();
            lstOpptyForRenewal[2].TRAV_Prospect_Effective_Date__c = System.today();
            lstOpptyForRenewal[2].TRAV_Business_Unit__c = 'BSI-FI';
            insert lstOpptyForRenewal;
            
            OpportunityTeamMember objTeamMember1 = TestDataFactory.createOpptyTeammember(objUsr2.Id,lstOpptyForRenewal[1].Id, 'Actuary');
			insert objTeamMember1;
            //Inserting EntitySubscription Records
            List<EntitySubscription> listToFollow = new List<EntitySubscription>();
            //CreateEntitySubscription Record
            EntitySubscription followRec = new EntitySubscription(ParentId = lstOpptyForRenewal[0].Id, SubscriberId = userinfo.getUserId());
            listToFollow.add(followRec);

            EntitySubscription followRec1 = new EntitySubscription(ParentId = lstOpptyForRenewal[1].Id, SubscriberId = userinfo.getUserId());
            listToFollow.add(followRec1);

            if(!listToFollow.isEmpty()){
                database.insert(listToFollow);
            }

            /*lstOpptyForRenewal[0].StageName = 'Deferred';
lstOpptyForRenewal[1].StageName = 'Written';
update lstOpptyForRenewal;*/

            system.debug('lstOpptyForRenewal>>>'+lstOpptyForRenewal);
            //creating opportunity team member records
            List<OpportunityTeamMember> lstOpportunityTeam = TestDataFactory.createOpportunityTeamMembers(2,objUsr2.Id,lstOpptyForRenewal[0].Id);
            lstOpportunityTeam[1].OpportunityId = lstOpptyForRenewal[1].Id;
            insert lstOpportunityTeam;

            List<Product2> lstProducts = TestDataFactory.createProducts(5);
            insert lstProducts;

            //associate products to pricebook
            List<PricebookEntry> lstStandardPricebookEntries = TestDataFactory.createPricebookEntries(lstProducts, Test.getStandardPricebookId());
            insert lstStandardPricebookEntries;

            //create pricebook
            Pricebook2 priceBook = TestDataFactory.createTestPricebook('BI - National Accounts');
            insert priceBook;

            List<PricebookEntry> lstBIPricebookEntries = TestDataFactory.createPricebookEntries(lstProducts, priceBook.Id);
            insert lstBIPricebookEntries;

            lstOpptyForRenewal[0].PriceBook2Id = priceBook.Id;
            lstOpptyForRenewal[1].PriceBook2Id = priceBook.Id;
            update lstOpptyForRenewal;

            Trav_Carrier__c carr = new Trav_Carrier__c(Name = 'Carrier 1A');
            insert carr;

            OpportunityLineItem objOppLineItem = new OpportunityLineItem();
            objOppLineItem.OpportunityId = lstOpptyForRenewal[0].Id;
            objOppLineItem.Product2Id = lstProducts[0].Id;
            objOppLineItem.TRAV_Stage__c = 'Written';
            objOppLineItem.TRAV_Program_Type__c = 'Unbundled';
            objOppLineItem.TRAV_Incumbent_Insurance_Carrier_Name__c = carr.id;
            lstOpportunityLineItems.add(objOppLineItem);

            OpportunityLineItem objOppLineItem1 = new OpportunityLineItem();
            objOppLineItem1.OpportunityId = lstOpptyForRenewal[0].Id;
            objOppLineItem1.Product2Id = lstProducts[0].Id;
            objOppLineItem1.TRAV_Stage__c = 'Quote Unsuccessful';
            objOppLineItem1.TRAV_Program_Type__c = 'Bundled';
            objOppLineItem1.TRAV_Incumbent_Insurance_Carrier_Name__c = objCarrier.id;
            lstOpportunityLineItems.add(objOppLineItem1);
            List<Database.saveResult> opportunityLineItemSaveResult = database.insert(lstOpportunityLineItems,false);

        }
        Test.startTest();

        BatchCreateOpportunityOnExpirationDate obj = new BatchCreateOpportunityOnExpirationDate();
        Id batchId = DataBase.executeBatch(obj);
        String schTime = '0 0 12 * * ?';
        system.schedule('BatchCreateOpportunityOnExpirationDate1', schTime, obj);
        Test.stopTest();

        //System.assertEquals([SELECT StageName FROM Opportunity WHERE TRAV_Renewal_Opportunity__c = :lstOpptyForRenewal[0].Id].StageName,'Prospect');
    }
    static testmethod void testRenewalOpportunityCloneCase(){
        List<Opportunity> lstOpptyForRenewal;
        TestDataFactory.createRenewalBatchsetting();
        List<OpportunityLineItem> lstOpportunityLineItems = new List<OpportunityLineItem>();
        //Adding bypass custom setting to bypass validation rule for testing this batch
        TRAV_Bypass_Settings__c objCustomSettingBypass = new TRAV_Bypass_Settings__c();
        objCustomSettingBypass.TRAV_Skip_Validation_Rules__c = true;
        insert objCustomSettingBypass;

        User objUsr2 ;
        System.runAs(new User(Id = Userinfo.getUserId())){
            objUsr2  = TestDataFactory.createTestUser('BI - National Accounts','TestNA','LastNameNA');
            insert objUsr2;
        }
        Date todayDate = Date.today().addDays(150);
        System.runAs(objUsr2){
            //create account record
            Account objProspectAccount = TestDataFactory.createProspect();
            List<Account> lstAccount = new List<Account>{objProspectAccount};
                insert lstAccount;
            TRAV_Carrier__c objCarrier = new TRAV_Carrier__c();
            objCarrier.Name = 'Travelers';
            insert objCarrier;
            Account agency2 = TestDataFactory.createAgency();
            insert agency2;

            Account agency = TestDataFactory.createAgency();
            insert agency;

            //Creating Opportunity Records
            lstOpptyForRenewal = TestDataFactory.createOpportunities(2,objProspectAccount.Id, 'BI-NA');
            //lstOpptyForRenewal[0].TRAV_Defferal_Date__c = System.today();
            lstOpptyForRenewal[0].StageName = 'Written';
            lstOpptyForRenewal[0].OwnerID = objUsr2.Id;
            //lstOpptyForRenewal[0].Owner.Profile.Name = 'BI - National Accounts';
            lstOpptyForRenewal[0].TRAV_Renewal_Creation__c = Date.today().addDays(-1);
            lstOpptyForRenewal[0].TRAV_Prospect_Effective_Date__c = System.today();
            lstOpptyForRenewal[0].TRAV_Agency_Broker__c = agency2.Id;
            lstOpptyForRenewal[0].TRAV_Policy_Expiration_Date__c = System.today();
            lstOpptyForRenewal[0].TRAV_Non_Renewal_Advisement_Indicator__c = false;
            lstOpptyForRenewal[0].TRAV_Multi_Year_Deal_Indicator__c = 'test/test2';
            lstOpptyForRenewal[0].TRAV_Deal_Years__c = 8;
            insert lstOpptyForRenewal;
			OpportunityTeamMember objTeamMember1 = TestDataFactory.createOpptyTeammember(objUsr2.Id,lstOpptyForRenewal[0].Id, 'Actuary');
			insert objTeamMember1;
            

            Opportunity opp = [Select id, name, Owner.Profile.Name from Opportunity where Id =: lstOpptyForRenewal[0].Id];
            system.debug('Profile Name'+ opp.Owner.Profile.Name);
            //Inserting EntitySubscription Records
            List<EntitySubscription> listToFollow = new List<EntitySubscription>();
            //CreateEntitySubscription Record
            EntitySubscription followRec = new EntitySubscription(ParentId = lstOpptyForRenewal[0].Id, SubscriberId = userinfo.getUserId());
            listToFollow.add(followRec);


            if(!listToFollow.isEmpty()){
                database.insert(listToFollow);
            }

            /*lstOpptyForRenewal[0].StageName = 'Deferred';
lstOpptyForRenewal[1].StageName = 'Written';
update lstOpptyForRenewal;*/

            system.debug('lstOpptyForRenewal>>>'+lstOpptyForRenewal);
            //creating opportunity team member records
            List<OpportunityTeamMember> lstOpportunityTeam = TestDataFactory.createOpportunityTeamMembers(2,objUsr2.Id,lstOpptyForRenewal[0].Id);
            insert lstOpportunityTeam;

            List<Product2> lstProducts = TestDataFactory.createProducts(5);
            insert lstProducts;

            //associate products to pricebook
            List<PricebookEntry> lstStandardPricebookEntries = TestDataFactory.createPricebookEntries(lstProducts, Test.getStandardPricebookId());
            insert lstStandardPricebookEntries;

            //create pricebook
            Pricebook2 priceBook = TestDataFactory.createTestPricebook('BI - National Accounts');
            insert priceBook;

            List<PricebookEntry> lstBIPricebookEntries = TestDataFactory.createPricebookEntries(lstProducts, priceBook.Id);
            insert lstBIPricebookEntries;

            lstOpptyForRenewal[0].PriceBook2Id = priceBook.Id;
            update lstOpptyForRenewal;

            Trav_Carrier__c carr = new Trav_Carrier__c(Name = 'Carrier 1A');
            insert carr;
            
            

            OpportunityLineItem objOppLineItem = new OpportunityLineItem();
            objOppLineItem.OpportunityId = lstOpptyForRenewal[0].Id;
            objOppLineItem.Product2Id = lstProducts[0].Id;
            objOppLineItem.TRAV_Stage__c = 'Written';
            objOppLineItem.TRAV_Program_Type__c = 'Unbundled';
            objOppLineItem.TRAV_Incumbent_Insurance_Carrier_Name__c = carr.id;
            lstOpportunityLineItems.add(objOppLineItem);

            /*OpportunityLineItem objOppLineItem1 = new OpportunityLineItem();
objOppLineItem1.OpportunityId = lstOpptyForRenewal[0].Id;
objOppLineItem1.Product2Id = lstProducts[0].Id;
objOppLineItem1.TRAV_Stage__c = 'Quote Unsuccessful';
objOppLineItem1.TRAV_Program_Type__c = 'Bundled';
objOppLineItem1.TRAV_Incumbent_Insurance_Carrier_Name__c = objCarrier.id;
lstOpportunityLineItems.add(objOppLineItem1);*/
            List<Database.saveResult> opportunityLineItemSaveResult = database.insert(lstOpportunityLineItems,false);
            system.debug('lstOpportunityLineItems>>'+lstOpportunityLineItems);
            system.debug('opportunityLineItemSaveResult>>'+opportunityLineItemSaveResult);
            Test.startTest();

            BatchCreateOpportunityOnExpirationDate obj = new BatchCreateOpportunityOnExpirationDate();
            Id batchId = DataBase.executeBatch(obj);
            //Database.QueryLocator ql = obj.start(null);
            //obj.execute(null,lstOpptyForRenewal);
            //obj.Finish(null);
            Test.stopTest();
        }


        //System.assertEquals([SELECT StageName FROM Opportunity WHERE TRAV_Renewal_Opportunity__c = :lstOpptyForRenewal[0].Id].StageName,'Prospect');
    }



    static testmethod void testDeferredOpportunityCloneCase1(){
        List<Opportunity> lstOpptyForRenewal;
        TestDataFactory.createRenewalBatchsetting();
        List<OpportunityLineItem> lstOpportunityLineItems = new List<OpportunityLineItem>();
        //Adding bypass custom setting to bypass validation rule for testing this batch
        TRAV_Bypass_Settings__c objCustomSettingBypass = new TRAV_Bypass_Settings__c();
        objCustomSettingBypass.TRAV_Skip_Validation_Rules__c = true;
        insert objCustomSettingBypass;

        User objUsr2 ;
        System.runAs(new User(Id = Userinfo.getUserId())){
            objUsr2= TestDataFactory.createTestUser('BI - National Accounts','TestNA','LastNameNA');
            insert objUsr2;
        }
        Date todayDate = Date.today().addDays(150);
        System.runAs(objUsr2){
            //create account record
            Account objProspectAccount = TestDataFactory.createProspect();
            List<Account> lstAccount = new List<Account>{objProspectAccount};
                insert lstAccount;
            TRAV_Carrier__c objCarrier = new TRAV_Carrier__c();
            objCarrier.Name = 'Travelers';
            insert objCarrier;
            Account agency2 = TestDataFactory.createAgency();
            insert agency2;

            Account agency = TestDataFactory.createAgency();
            insert agency;

            //Creating Opportunity Records
            lstOpptyForRenewal = TestDataFactory.createOpportunities(2,objProspectAccount.Id, 'BI-NA');
            //lstOpptyForRenewal[0].TRAV_Defferal_Date__c = System.today();
            lstOpptyForRenewal[1].StageName = 'Written';
            lstOpptyForRenewal[0].TRAV_Renewal_Creation__c = System.today();
            lstOpptyForRenewal[0].TRAV_Agency_Broker__c = agency2.Id;
            lstOpptyForRenewal[0].TRAV_Prospect_Effective_Date__c = System.today();
            lstOpptyForRenewal[0].TRAV_Policy_Expiration_Date__c = System.today();
            lstOpptyForRenewal[0].TRAV_Non_Renewal_Advisement_Indicator__c = false;
            lstOpptyForRenewal[1].TRAV_Renewal_Creation__c = Date.today().addDays(-1);
            lstOpptyForRenewal[1].TRAV_Agency_Broker__c = agency2.Id;
            lstOpptyForRenewal[1].TRAV_Policy_Expiration_Date__c = System.today();
            lstOpptyForRenewal[1].TRAV_Prospect_Effective_Date__c = System.today();
            lstOpptyForRenewal[1].TRAV_Non_Renewal_Advisement_Indicator__c = false;
            lstOpptyForRenewal[1].StageName = 'Written';
            insert lstOpptyForRenewal;

            //Inserting EntitySubscription Records
            List<EntitySubscription> listToFollow = new List<EntitySubscription>();
            //CreateEntitySubscription Record
            EntitySubscription followRec = new EntitySubscription(ParentId = lstOpptyForRenewal[0].Id, SubscriberId = userinfo.getUserId());
            listToFollow.add(followRec);

            EntitySubscription followRec1 = new EntitySubscription(ParentId = lstOpptyForRenewal[1].Id, SubscriberId = userinfo.getUserId());
            listToFollow.add(followRec1);

            if(!listToFollow.isEmpty()){
                database.insert(listToFollow);
            }

            /*lstOpptyForRenewal[0].StageName = 'Deferred';
lstOpptyForRenewal[1].StageName = 'Written';
update lstOpptyForRenewal;*/

            system.debug('lstOpptyForRenewal>>>'+lstOpptyForRenewal);
            //creating opportunity team member records
            List<OpportunityTeamMember> lstOpportunityTeam = TestDataFactory.createOpportunityTeamMembers(2,objUsr2.Id,lstOpptyForRenewal[0].Id);
            lstOpportunityTeam[1].OpportunityId = lstOpptyForRenewal[1].Id;
            insert lstOpportunityTeam;

            List<Product2> lstProducts = TestDataFactory.createProducts(5);
            insert lstProducts;

            //associate products to pricebook
            List<PricebookEntry> lstStandardPricebookEntries = TestDataFactory.createPricebookEntries(lstProducts, Test.getStandardPricebookId());
            insert lstStandardPricebookEntries;

            //create pricebook
            Pricebook2 priceBook = TestDataFactory.createTestPricebook('BI - National Accounts');
            insert priceBook;

            List<PricebookEntry> lstBIPricebookEntries = TestDataFactory.createPricebookEntries(lstProducts, priceBook.Id);
            insert lstBIPricebookEntries;

            lstOpptyForRenewal[0].PriceBook2Id = priceBook.Id;
            lstOpptyForRenewal[1].PriceBook2Id = priceBook.Id;
            update lstOpptyForRenewal;

            Trav_Carrier__c carr = new Trav_Carrier__c(Name = 'Carrier 1A');
            insert carr;

            OpportunityLineItem objOppLineItem = new OpportunityLineItem();
            objOppLineItem.OpportunityId = lstOpptyForRenewal[0].Id;
            objOppLineItem.Product2Id = lstProducts[0].Id;
            objOppLineItem.TRAV_Stage__c = 'Written';
            objOppLineItem.TRAV_Program_Type__c = 'Unbundled';
            objOppLineItem.TRAV_Incumbent_Insurance_Carrier_Name__c = carr.id;
            lstOpportunityLineItems.add(objOppLineItem);

            OpportunityLineItem objOppLineItem1 = new OpportunityLineItem();
            objOppLineItem1.OpportunityId = lstOpptyForRenewal[0].Id;
            objOppLineItem1.Product2Id = lstProducts[0].Id;
            objOppLineItem1.TRAV_Stage__c = 'Quote Unsuccessful';
            objOppLineItem1.TRAV_Program_Type__c = 'Bundled';
            objOppLineItem1.TRAV_Incumbent_Insurance_Carrier_Name__c = objCarrier.id;
            lstOpportunityLineItems.add(objOppLineItem1);
            List<Database.saveResult> opportunityLineItemSaveResult = database.insert(lstOpportunityLineItems,false);

        }
        Test.startTest();

        BatchCreateOpportunityOnExpirationDate obj = new BatchCreateOpportunityOnExpirationDate();
        Id batchId = DataBase.executeBatch(obj);
        String schTime = '0 0 12 * * ?';
        system.schedule('BatchCreateOpportunityOnExpirationDate2', schTime, obj);
        Test.stopTest();

        //System.assertEquals([SELECT StageName FROM Opportunity WHERE TRAV_Renewal_Opportunity__c = :lstOpptyForRenewal[0].Id].StageName,'Prospect');
    }
}
