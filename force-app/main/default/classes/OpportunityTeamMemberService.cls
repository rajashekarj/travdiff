/**
* Utility Class for OpportunityTeamMember Trigger Handler. This class will have logic for all the implementations.
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------
* Isha Shukla		      US60806             05/31/2019       Added method insertDeleteAccountTeams
* Isha Shukla             US62980             06/24/2019	   Updated method gettingAccountTeamsForDeletion and insertDeleteAccountTeams
* Isha Shukla             US62980              06/28/2019 	   Added SEMICOLON variable
* Isha Shukla             US62980			  07/03/2019       Added Account null check while creating teams in insertDeleteAccountTeams method
* Shashank Agarwal        US63739             07/30/2019       Added Method modifyEventAttendees
* Isha Shukla 			  US64798			  08/08/2019	   Updated methods insertDeleteAccountTeams and gettingAccountTeamsForDeletion
* Derek Manierre		  US68470			  03/03/2020	   Added method preventTeamMemeberChangesBSI
*/
public class OpportunityTeamMemberService {
     public static boolean addManagerCheck =false;
     public static void populateCreateFromsystemField(List<OpportunityTeamMember> lstnewOpportunityTeam, Map<Id, OpportunityTeamMember> mapOldOpportunityTeam) {
        for(OpportunityTeamMember objOpptyTeamMem : lstnewOpportunityTeam){
            system.debug('objOpptyTeamMem>>'+objOpptyTeamMem);
            if(mapOldOpportunityTeam != null &&
               objOpptyTeamMem.TeamMemberRole != mapOldOpportunityTeam.get(objOpptyTeamMem.ID).TeamMemberRole &&
              objOpptyTeamMem.TRAV_Created_From_System__c == true) {
                 system.debug('objOpptyTeamMem>TRAV_Created_From_System__c>'+objOpptyTeamMem.TRAV_Created_From_System__c);
                  objOpptyTeamMem.TRAV_Created_From_System__c = false;
            }
        }
    }
    /**
* This method will insert and delete Account Teams, when opportunity teams of related opportunities are inserted or deleted
* @param lstnewOpportunityTeam List is passed to this method that need to be manipulate having new records which are inserted
* @param mapOldOpportunityTeam Map is passed to have old values of opportunity teams which are deleted
* @return void
*/
    public static void insertDeleteAccountTeams(List<OpportunityTeamMember> lstnewOpportunityTeam, Map<Id, OpportunityTeamMember> mapOldOpportunityTeam) {
        final String METHODNAME = 'insertDeleteAccountTeams';
        List<String> lstRoles = Label.TRAV_Roles.split(Constants.SEMICOLON);//List of roles
        Set<Id> setUserId = new Set<Id>();//User ids which were part of deleing oppty teams
        Set<Id> setOpportunityTeamMemberId = new Set<Id>();
        Map<Id,List<OpportunityTeamMember>> mapOpptyToOpptyTeams = new Map<Id,List<OpportunityTeamMember>>();//Oppty to Oppty Team Map
        List<OpportunityTeamMember> lstOpptyTeamMember = new List<OpportunityTeamMember>();
        try {
            if(lstnewOpportunityTeam == null) {
                //Opportunity team member delete scenario
                lstOpptyTeamMember = mapOldOpportunityTeam.values();
            } else {
                //Opportunity team membe insert scenario
                lstOpptyTeamMember = lstnewOpportunityTeam;
            }
            for(OpportunityTeamMember objOpptyTeamMem : lstOpptyTeamMember) {
                if( (lstRoles.contains(objOpptyTeamMem.TeamMemberRole)) ) {
                    //Creating Map of Opportunities to their related opportunity teams
                    // to further utilize for inserting account teams
                    if(!mapOpptyToOpptyTeams.containsKey(objOpptyTeamMem.OpportunityId)) {
                        mapOpptyToOpptyTeams.put(objOpptyTeamMem.OpportunityId,new List<OpportunityTeamMember>{objOpptyTeamMem});
                    } else {
                        mapOpptyToOpptyTeams.get(objOpptyTeamMem.OpportunityId).add(objOpptyTeamMem);
                    }
                    //Deletion case and Updation Case and Active/Inactive case
                    if( lstnewOpportunityTeam == null ||
                       (lstnewOpportunityTeam != null &&
                        objOpptyTeamMem.TRAV_Active__c == false &&
                        objOpptyTeamMem.TRAV_Active__c != mapOldOpportunityTeam.get(objOpptyTeamMem.Id).TRAV_Active__c) ) {
                            //Populating set of user ids which are deleted
                            setUserId.add(objOpptyTeamMem.UserId);
                            // Popultating Oppotunity team ids
                            setOpportunityTeamMemberId.add(objOpptyTeamMem.Id);
                        }//End of if checking conditions for deltion and updation
                }//end of if checking roles
            }//end of for loop
            if(!mapOpptyToOpptyTeams.isEmpty()) {
                List<AccountTeamMember> listOfAccountTeamMember = new List<AccountTeamMember>();//List of Account Teams to insert
                AccountTeamMember objAccountTeam;//Account record to insert
                Set<Id> setAccountId = new Set<Id>();//Account id set of oppty teams gettting deleted
                List<Opportunity> lstOpportunity = new List<Opportunity>();

                //Querying all open opportunities or opportunities closed last 12 months
                lstOpportunity = OpportunitySelector.getOpportunities(mapOpptyToOpptyTeams.keySet());
                for(Opportunity objOpportunity : lstOpportunity) {
                    for( OpportunityTeamMember opptyTeam : mapOpptyToOpptyTeams.get(objOpportunity.Id) ) {
                        //Jul 3, 2019 - Added account null check to resolve error when opportunity will not have account associated
                        if(lstnewOpportunityTeam != null && opptyTeam.TRAV_Active__c == true ){
                            // In Opportunity Team Member insertion scenario
                            // Creating Account Team Members

                            // Account teams for standard Account
                            if(objOpportunity.AccountId != null) {
                                objAccountTeam = new AccountTeamMember();
                                objAccountTeam.UserId = opptyTeam.UserId;
                                objAccountTeam.AccountId = objOpportunity.AccountId;
                                objAccountTeam.TeamMemberRole = opptyTeam.TeamMemberRole;
                                listOfAccountTeamMember.add(objAccountTeam);
                            }
                            //Changes for US64798
                            // Account teams for Agency Brick and motor Account
                            if(objOpportunity.TRAV_Agency_Broker__c != null) {
                                objAccountTeam = new AccountTeamMember();
                                objAccountTeam.UserId = opptyTeam.UserId;
                                objAccountTeam.AccountId = objOpportunity.TRAV_Agency_Broker__c;
                                objAccountTeam.TeamMemberRole = opptyTeam.TeamMemberRole;
                                listOfAccountTeamMember.add(objAccountTeam);
                            }

                        }//End of if
                        else {
                            // In Opportunity Team Member deletion scenario
                            // Account to Opportunity Map whose opportunity teams is deleted
                            // For standard account reference
                            if(objOpportunity.AccountId != null) {
                               setAccountId.add(objOpportunity.AccountId);
                            }
                            //Changes for US64798
                            //For agency Brick and motor account
                            if(objOpportunity.TRAV_Agency_Broker__c != null) {
                               setAccountId.add(objOpportunity.TRAV_Agency_Broker__c);
                            }

                        }//end of else
                    }//end of inner for loop
                }//End of out for loop
                List<AccountTeamMember> listToDeleteAccountTeam = new List<AccountTeamMember>();//List of Account Teams which will get delete
                if(setUserId.size() > 0 && setAccountId.size() > 0) {
                    //Calling method to check whether Teams getting delete is not listed under other related opportunity teams
                    //and returns the account teams which need to be delete
                    listToDeleteAccountTeam = gettingAccountTeamsForDeletion(setUserId,setAccountId,setOpportunityTeamMemberId);
                }  //end of if
                //Account Team Deletion
                if(!listToDeleteAccountTeam.isEmpty()) {
                    Database.DeleteResult[] lstDeleteResult = Database.delete(listToDeleteAccountTeam, false);
                    ExceptionUtility.logDeletionError(Constants.OPPORTUNITYTEAMMEMBERSERVICE,METHODNAME,lstDeleteResult);
                }
                //Account Team Insertion
                if(!listOfAccountTeamMember.isEmpty()) {
                    Database.SaveResult[] lstSaveResult = Database.insert(listOfAccountTeamMember, false);
                    ExceptionUtility.logDMLError(Constants.OPPORTUNITYTEAMMEMBERSERVICE,METHODNAME,lstSaveResult);
                }
            }//end of if which is checking of map size
            if(Test.isRunningTest()){
                throw new DMLException('Error');
            }
        }//End of Try
        catch(exception objExp) {
            //Exception handling
            ExceptionUtility.logApexException(Constants.OPPORTUNITYTEAMMEMBERSERVICE,METHODNAME, objExp, null);
        }//end of catch
    }//end of method insertDeleteAccountTeams

    /**
* This method will check if any Account Team member  which is getting delete in this transaction is not among the
* other opportunity Team members listed under the Opportunity of the related Accounts of these Account Team members
* @param setUserId Set is passed to this method that need to be part of query filter
* @param setAccountId Set is passed to this method that need to be part of query filter
* @return List<AccountTeamMember> List of AccountTeamMember based on the SOQL query provided as an input parameter.
*/
    public static List<AccountTeamMember> gettingAccountTeamsForDeletion(Set<Id> setUserId, Set<Id> setAccountId, Set<Id> setOpportunityTeamMemberId) {
        Set<Id> setAccountIdOfAccountTeam = new Set<Id>();//Set of account Ids wich are part of Account Teams which might get delete
        Set<Id> setOfUserId = new Set<Id>();//Set of user Ids wich are part of Account Teams which might get delet
        List<AccountTeamMember> listToDeleteAccountTeam = new List<AccountTeamMember>();//List of Account Teams which will get delete
        Map<String,AccountTeamMember> mapAccUser = new Map<String,AccountTeamMember>(); // Map with Account_User key to account team records which might get delete
        if(setAccountId.size() > 0 && setUserId.size() > 0) {
            //querying account teams by filtering user and account which are part of removed opportunity teams
            List<AccountTeamMember> lstAcctTeam = new List<AccountTeamMember>();
            lstAcctTeam = AccountTeamMemberSelector.getAccountTeamMembers(setAccountId,setUserId);
            for(AccountTeamMember objAccTeam : lstAcctTeam) {
                //Creating a map of Account User key with Account team record values which might get delete further
                mapAccUser.put(objAccTeam.AccountId+'_'+objAccTeam.UserId,objAccTeam);
                //Populating sets wtih user and account ids to send to query methods
                setOfUserId.add(objAccTeam.UserId);
                setAccountIdOfAccountTeam.add(objAccTeam.AccountId);
            }// end of for lop of account team
        }//end of if checking set size which came as parameters
        if(setOfUserId.size() > 0 && setAccountIdOfAccountTeam.size() > 0) {
            //Querying Opportunity Teams which are related to Accounts whose one of the opportunity Teams is deleted
            List<OpportunityTeamMember> lstOpptyTeam = OpportunityTeamMemberSelector.getOpportunityTeamMembers(setAccountIdOfAccountTeam,setOfUserId);
            Map<String,Integer> mapAccUserCount = new Map<String,Integer>();
            Set<String> setAccountWithUser = new Set<String>();
            for(OpportunityTeamMember objOppTeam : lstOpptyTeam) {
                if(!setOpportunityTeamMemberId.contains(objOppTeam.Id)) {
                    //Populating the set with merged string of standard account and user relations, related to all the opportunities
                    if(objOppTeam.Opportunity.AccountId != null) {
                       setAccountWithUser.add(objOppTeam.Opportunity.AccountId+'_'+objOppTeam.UserId);
                    }
                    //Changes for US64798
                    //Populating the set with merged string of Agency brick and motor account and user relations, related to all the opportunities
                    if(objOppTeam.Opportunity.TRAV_Agency_Broker__c != null) {
                        setAccountWithUser.add(objOppTeam.Opportunity.TRAV_Agency_Broker__c+'_'+objOppTeam.UserId);
                    }
                }
            }//end of opportunity team for loop
            for(String accUser : mapAccUser.keySet()) {
                //If the member listed under all the opprtunities of an account is not in set
                //then that member can be removed from the account team member
              	if(!setAccountWithUser.contains(accUser)){
                    listToDeleteAccountTeam.add(mapAccUser.get(accUser));
                }
            }//End of for loop
        }//End of if checking set size
        return listToDeleteAccountTeam;
    }
/**
* This method will modify Event Attendees of stewardship opportunities on insert update and delete
* @param lstnewOpportunityTeam List is passed to this method that need to be manipulate having new records which are inserted
* @param mapOldOpportunityTeam Map is passed to have old values of opportunity teams which are deleted
* @return void.
*/

public static void modifyEventAttendees(List<OpportunityTeamMember> lstnewOpportunityTeam, Map<Id, OpportunityTeamMember> mapOldOpportunityTeam){
    final String METHODNAME = 'modifyEventAttendees';
    List<EventRelation> lstEventRelationToInsert = new List<EventRelation>();
    List<EventRelation> lstEventRelationToDelete = new List<EventRelation>();
    List<EventRelation> lstOldEventRelation = new List<EventRelation>();
    Map<Id,List<Event>> mapOpportunityToEvent= new Map<Id,List<Event>>();
    Map<Id,List<EventRelation>> mapEventToEventRelation = new Map<Id,List<EventRelation>>();
    Set<Id> setOpportunityIds = new Set<Id>();
    Set<Id> setEventId = new Set<Id>();
    List<Event> lstEvent = new List<Event>();
    try{
        if(lstnewOpportunityTeam != Null){
            for(OpportunityTeamMember objOpportunityTeamMember : lstnewOpportunityTeam){
                setOpportunityIds.add(objOpportunityTeamMember.OpportunityId);
            }
        }else{
            for(OpportunityTeamMember objOpportunityTeamMember : mapOldOpportunityTeam.values()){
                setOpportunityIds.add(objOpportunityTeamMember.OpportunityId);
            }
        }

        // Modification for US73728
        List<TRAV_AttendeesForStewardshipMeeting__mdt> lstAttendeesMetadata = Database.query(Constants.AttendeesMetadataForStewardshipMeetingQuery);
        // Modification for US73728
        Map<String,String> mapSubjectToAttendeesRole = new Map<String,String>();
        // Modification for US73728
        for(TRAV_AttendeesForStewardshipMeeting__mdt objAttendeesMetadata : lstAttendeesMetadata){
            mapSubjectToAttendeesRole.put(objAttendeesMetadata.MasterLabel,objAttendeesMetadata.TRAV_Attendees_Team_Role__c);
        }

        if(!setOpportunityIds.isEmpty()){
            lstEvent = EventSelector.getEventForStewardshipEventRelation(setOpportunityIds);
        }

        if(!setOpportunityIds.isEmpty()){
            for(Id objOpportunityId : setOpportunityIds){
                List<Event> lstEventOfOpportunity = new List<Event>();
                for(Event objEvent : lstEvent){
                    if(objEvent.WhatId == objOpportunityId){
                        lstEventOfOpportunity.add(objEvent);
                    }
                }
                mapOpportunityToEvent.put(objOpportunityId, lstEventOfOpportunity);
            }
        }

        if(!lstEvent.isEmpty()){
            for(Event objEvent : lstEvent){
                setEventId.add(objEvent.Id);
            }
        }
        if(!setEventId.isEmpty()){
            lstOldEventRelation = EventRelationSelector.getEventRelationForStewardshipEvent(setEventId);
            for(Id objEventId : setEventId){
                List<EventRelation> lstEventRelationOfEvent = new List<EventRelation>();
                for(EventRelation objEventRelation : lstOldEventRelation){
                    if(objEventRelation.EventId == objEventId){
                        lstEventRelationOfEvent.add(objEventRelation);
                    }
                }
                mapEventToEventRelation.put(objEventId, lstEventRelationOfEvent);
            }
        }

        //Insert Case
        if( lstnewOpportunityTeam != Null && mapOldOpportunityTeam == Null){
            for(OpportunityTeamMember objOpportunityTeamMember : lstnewOpportunityTeam){
                for(Event objEvent : mapOpportunityToEvent.get(objOpportunityTeamMember.OpportunityId)){
                    // Modification for US73728
                    system.debug('**test**'+mapSubjectToAttendeesRole+'**'+objOpportunityTeamMember.TeamMemberRole);
                    system.debug('**test**2'+mapOpportunityToEvent);
                    if(mapSubjectToAttendeesRole.containsKey(objEvent.Subject) && mapSubjectToAttendeesRole.get(objEvent.Subject).contains(objOpportunityTeamMember.TeamMemberRole)){
                        EventRelation objEventRelation = new EventRelation();
                        objEventRelation.EventId = objEvent.Id;
                        objEventRelation.RelationId = objOpportunityTeamMember.UserId;
                        objEventRelation.isInvitee = true;
                        objEventRelation.isParent = false;
                        lstEventRelationToInsert.add(objEventRelation);
                    }
                }
            }
        }
        //Update Case
        if(mapOldOpportunityTeam != Null && lstnewOpportunityTeam != Null){
            for(OpportunityTeamMember objOpportunityTeamMember : lstnewOpportunityTeam){
                if(objOpportunityTeamMember.TeamMemberRole != mapOldOpportunityTeam.get(objOpportunityTeamMember.Id).TeamMemberRole){
                    //Update Case 1: If Modified Member's New Role is a part of required attedees and old role was not a part of required attendees
                    for(Event objEvent : mapOpportunityToEvent.get(objOpportunityTeamMember.OpportunityId)){
                        // Modification for US73728
                        if(mapSubjectToAttendeesRole.containsKey(objEvent.Subject) && mapSubjectToAttendeesRole.get(objEvent.Subject).contains(objOpportunityTeamMember.TeamMemberRole) && !mapSubjectToAttendeesRole.get(objEvent.Subject).contains(mapOldOpportunityTeam.get(objOpportunityTeamMember.Id).TeamMemberRole)){
                            EventRelation objEventRelation = new EventRelation();
                            objEventRelation.EventId = objEvent.Id;
                            objEventRelation.RelationId = objOpportunityTeamMember.UserId;
                            objEventRelation.isInvitee = true;
                            objEventRelation.isParent = false;
                            lstEventRelationToInsert.add(objEventRelation);
                        }
                    }//End of case 1 check
                    //Update Case 2: If Modified Member's New Role is not a part of required attedees and old role was a part of required attendees
                    for(Event objEvent : mapOpportunityToEvent.get(objOpportunityTeamMember.OpportunityId)){
                        // Modification for US73728
                        if(mapSubjectToAttendeesRole.containsKey(objEvent.Subject) && !mapSubjectToAttendeesRole.get(objEvent.Subject).contains(objOpportunityTeamMember.TeamMemberRole) && mapSubjectToAttendeesRole.get(objEvent.Subject).contains(mapOldOpportunityTeam.get(objOpportunityTeamMember.Id).TeamMemberRole)){
                            for(EventRelation objEventRelation : mapEventToEventRelation.get(objEvent.Id)){
                                if(Constants.EventTypeStewardship.contains(objEvent.Type) && objOpportunityTeamMember.UserId == objEventRelation.RelationId){
                                    EventRelation objEventRelationToDelete = new EventRelation();
                                    objEventRelationToDelete.Id = objEventRelation.Id;
                                    lstEventRelationToDelete.add(objEventRelation);
                                }
                            }
                        }
                    } //End of case 2 check
                } //End of check for field update
            } // End of Loop on lstnewOpportunityTeam
        } // End if for Update Check

        //Delete Case
        System.debug(mapOldOpportunityTeam);
        if(mapOldOpportunityTeam != Null && lstnewOpportunityTeam == Null){
            for(Id objOpportunityTeamMemberId : mapOldOpportunityTeam.keyset()){
                for(Event objEvent : mapOpportunityToEvent.get(mapOldOpportunityTeam.get(objOpportunityTeamMemberId).OpportunityId)){
                    for(EventRelation objEventRelation : mapEventToEventRelation.get(objEvent.Id)){
                        if(objEventRelation.RelationId == mapOldOpportunityTeam.get(objOpportunityTeamMemberId).UserId){
                            EventRelation objEventRelationToDelete = new EventRelation();
                            objEventRelationToDelete.Id = objEventRelation.Id;
                            lstEventRelationToDelete.add(objEventRelationToDelete);
                        }
                    }
                }
            }
        }// End of delete check

        //Insert New Event Relation
        if(!lstEventRelationToInsert.isEmpty()) {
            Database.SaveResult[] lstSaveResult = Database.insert(lstEventRelationToInsert, false);
            ExceptionUtility.logDMLError(Constants.OPPORTUNITYTEAMMEMBERSERVICE,METHODNAME,lstSaveResult);
        }
        //Delete Event Relation
        if(!lstEventRelationToDelete.isEmpty()) {
            Database.DeleteResult[] lstDeleteResult = Database.delete(lstEventRelationToDelete, false);
            ExceptionUtility.logDeletionError(Constants.OPPORTUNITYTEAMMEMBERSERVICE, METHODNAME, lstDeleteResult);
        }
        if(Test.isRunningTest()){
            throw new DMLException('Error');
        }

    }catch(Exception objException){
        ExceptionUtility.logApexException(Constants.OPPORTUNITYTEAMMEMBERSERVICE, METHODNAME, objException, null);
    }
}
    /**
* This is a method adds opportunity owner's Manager hierarchy to it's opportunity team
* @param lstnewOpportunity - List of new values in the for the Opportunity records.
* @param mapOldOpportunityTeamMember - Map of old values in the for the Opportunity records
* @return void - returns void .
*/
    public static void addManagersInOpportunityTeamMembers(List<OpportunityTeamMember> lstNewOpportunityTeamMember,Map<Id, OpportunityTeamMember> mapOldOpportunityTeamMember) {
		final String METHODNAME = 'addManagersInOpportunityTeamMembers';
        addManagerCheck = true;
        Map<Id,Id> mapOpportunityTeamMember = new Map<Id,Id>();

        Map<Id,Set<Id>> mapOpportunityToOldOppTeamMember = new Map<Id,Set<Id>>();
        Set<ID> userId = new Set<Id>();
		try{
         //If opportunity Team member is inserted or opportunity team member is updated we will be creating map for old and new team members
        if(!System.isBatch()){
            if(lstNewOpportunityTeamMember != null) {
                for(OpportunityTeamMember objNewOpportunityTeamMem : lstNewOpportunityTeamMember) {
                    if((mapOldOpportunityTeamMember == null ||
                       objNewOpportunityTeamMem.UserId != mapOldOpportunityTeamMember.get(objNewOpportunityTeamMem.ID).UserId ||
                       objNewOpportunityTeamMem.TeamMemberRole != mapOldOpportunityTeamMember.get(objNewOpportunityTeamMem.ID).TeamMemberRole) &&
                       objNewOpportunityTeamMem.TeamMemberRole != 'Reporting Manager' &&
                      objNewOpportunityTeamMem.TeamMemberRole != Constants.MANAGINGDIRECTOR){
                        mapOpportunityTeamMember.put(objNewOpportunityTeamMem.OpportunityId,objNewOpportunityTeamMem.UserId);
                        //Populating map only if team member is updated
                          if(mapOldOpportunityTeamMember != null) {
                              mapOpportunityToOldOppTeamMember.put(objNewOpportunityTeamMem.OpportunityId,new Set<Id>{mapOldOpportunityTeamMember.get(objNewOpportunityTeamMem.Id).UserId});
                              if(mapOldOpportunityTeamMember.get(objNewOpportunityTeamMem.Id).TRAV_Manager__c != null) {
                                  String[] arrManagers = mapOldOpportunityTeamMember.get(objNewOpportunityTeamMem.Id).TRAV_Manager__c.split(',');
                                  for(String str : arrManagers) {
                                      mapOpportunityToOldOppTeamMember.get(objNewOpportunityTeamMem.OpportunityId).add(str);
                                  }
                              }
                          }
                    }

                }
            }else {
                if(mapOldOpportunityTeamMember != null ){
                    List<User> userLst = new List<User>();

                    for(OpportunityTeamMember objNewOpportunityTeamMem :mapOldOpportunityTeamMember.values()) {
                        userId.add(mapOldOpportunityTeamMember.get(objNewOpportunityTeamMem.Id).UserId);
                        mapOpportunityToOldOppTeamMember.put(mapOldOpportunityTeamMember.get(objNewOpportunityTeamMem.Id).OpportunityId,new Set<Id>{mapOldOpportunityTeamMember.get(objNewOpportunityTeamMem.Id).UserId});
                        if(mapOldOpportunityTeamMember.get(objNewOpportunityTeamMem.Id).TRAV_Manager__c != null) {
                            String[] arrManagers = mapOldOpportunityTeamMember.get(objNewOpportunityTeamMem.Id).TRAV_Manager__c.split(',');
                            for(String str : arrManagers) {
                                mapOpportunityToOldOppTeamMember.get(mapOldOpportunityTeamMember.get(objNewOpportunityTeamMem.Id).OpportunityId).add(str);
                            }
                        }

                    }
                   /* userLst = UserSelector.getUsersManagerById(userId);
                    system.debug('userLst>>'+userLst);
                    if(userLst != null) {
                        for(OpportunityTeamMember objNewOpportunityTeamMem :mapOldOpportunityTeamMember.values()) {
                            for(User u : userLst) {
                                if(mapOldOpportunityTeamMember.get(objNewOpportunityTeamMem.Id).UserId == u.Id) {
                                    mapOpportunityToOldOppTeamMember.put(objNewOpportunityTeamMem.OpportunityId,u.ManagerId);
                                    system.debug('mapOpportunityToOldOppTeamMember>>'+mapOpportunityToOldOppTeamMember);
                                }
                            }
                        }
                    }*/
                }
            }
            //Calling queueable class to process this logic asynchronously
            if(mapOpportunityTeamMember.keySet().size() > 0 || mapOpportunityToOldOppTeamMember.keySet().size() > 0) {
                OpportunityTriggerHandler.isOpportunityTriggerContext = true;
                System.enqueueJob( new QueueableAddManagerInOpportunityTeam(mapOpportunityTeamMember,mapOpportunityToOldOppTeamMember,userId,null) );
            }
        }
            if(Test.isRunningTest()){
                throw new DMLException('Error');
            }
		}
		catch(Exception e){
			System.debug(e.getLineNumber()+'  '+e.getMessage());
			ExceptionUtility.logApexException(Constants.OPPORTUNITYTEAMMEMBERSERVICE, METHODNAME, e, null);
		}
    }
    /**
* This is method to update the manager of a opportunity Team Member
* @param lstnewOpportunityTeam - List of new values in the for the Opportunity Team Member records.
* @param mapOldOpportunityTeam - Map of old values in the for the Opportunity Team Member records
* @return void - returns void .
*/
    public static void updateManagers(List<OpportunityTeamMember> lstnewOpportunityTeam,Map<Id, OpportunityTeamMember> mapOldOpportunityTeam) {
        final string METHODNAME = 'updateManagers';
        String strManagers = '';
        List<OpportunityTeamMember> lstOpportunityTeamMemberForUpsert = new List<OpportunityTeamMember>();
		Set<Id> userIdForManagers = new Set<Id>();
		List<User> userLst = new List<User>();
        try{
            Map<Id,String> mapUserToManager = new Map<Id,String>();
            if(lstnewOpportunityTeam != null) {
				for(OpportunityTeamMember objNewOpportunityTeamMem : lstNewOpportunityTeam) {
					if(mapOldOpportunityTeam == null ||
                       objNewOpportunityTeamMem.TRAV_Manager__c != mapOldOpportunityTeam.get(objNewOpportunityTeamMem.ID).TRAV_Manager__c){
						userIdForManagers.add(objNewOpportunityTeamMem.UserId);
						if(mapOldOpportunityTeam != null) {
							userIdForManagers.add(mapOldOpportunityTeam.get(objNewOpportunityTeamMem.Id).UserId);
                        }
					}
				}

				userLst = UserSelector.getUsersManagerById(userIdForManagers);
               for(User u : userLst) {
                    if(u.ManagerId!=null)
                    	strManagers = String.valueOf(u.ManagerId);
                    if(u.Manager.ManagerId != null)
                        strManagers = strManagers +','+String.valueOf(u.Manager.ManagerId);
                    if(u.Manager.Manager.ManagerId !=null)
                        strManagers = strManagers +','+String.valueOf(u.Manager.Manager.ManagerId);
                    if(u.Manager.Manager.Manager.ManagerId !=null)
                        strManagers = '';
					 mapUserToManager.put(u.Id,strManagers);
				}
				for(OpportunityTeamMember objNewOpportunityTeamMem : lstNewOpportunityTeam) {
					if(mapUserToManager.containsKey(objNewOpportunityTeamMem.userId)) {
						objNewOpportunityTeamMem.TRAV_Manager__c = mapUserToManager.get(objNewOpportunityTeamMem.userId);
						lstOpportunityTeamMemberForUpsert.add(objNewOpportunityTeamMem);
					}
				}
			}
			else {
                if(mapOldOpportunityTeam != null ){
					for(OpportunityTeamMember objNewOpportunityTeamMem :mapOldOpportunityTeam.values()) {
                       userIdForManagers.add(mapOldOpportunityTeam.get(objNewOpportunityTeamMem.Id).UserId);
                   }
				}
				userLst = UserSelector.getUsersManagerById(userIdForManagers);
				for(User u : userLst) {
                    if(U.ManagerId!=null || U.Manager.ManagerId != null || U.Manager.Manager.ManagerId !=null)
                    	strManagers = String.valueOf(U.ManagerId)+' , '+String.valueOf(U.Manager.ManagerId)+' , '+String.valueOf(U.Manager.Manager.ManagerId);
					mapUserToManager.put(u.Id,strManagers);
				}
				for(OpportunityTeamMember objNewOpportunityTeamMem : mapOldOpportunityTeam.values()) {
					if(mapUserToManager.containsKey(objNewOpportunityTeamMem.userId)) {
						objNewOpportunityTeamMem.TRAV_Manager__c = mapUserToManager.get(objNewOpportunityTeamMem.userId);
						lstOpportunityTeamMemberForUpsert.add(objNewOpportunityTeamMem);
					}
				}

			}
            /*if(!lstOpportunityTeamMemberForUpsert.isEmpty()) {
                Database.SaveResult[] lstSaveResult = Database.insert(lstOpportunityTeamMemberForUpsert, false);
                ExceptionUtility.logDMLError(Constants.OPPORTUNITYTEAMMEMBERSERVICE, METHODNAME, lstSaveResult);
            }*/
            if(Test.isRunningTest()){
                throw new DMLException('error');
            }
		}// End of try
        catch(exception objExp) {
            ExceptionUtility.logApexException(Constants.OPPORTUNITYTEAMMEMBERSERVICE, METHODNAME, objExp, null);
        }//End of catch
    }// End of Method updateManagers
    
    /**
* This method will prevent users from changing Approvers/Underwriters/URC Approvers on BSI opportunities
* @param lstnewOpportunityTeam - List of new values in the for the Opportunity Team Member records.
* @param mapOldOpportunityTeam - Map of old values in the for the Opportunity Team Member records
* @return void - returns void .
*/
    public static void preventTeamMemeberChangesBSI(List<OpportunityTeamMember> lstnewOpportunityTeam, Map<Id, OpportunityTeamMember> mapOldOpportunityTeam){
        final String METHODNAME = 'preventTeamMemeberChangesBSI';
        
        Boolean boolIsUpdate = False;
        
        List<OpportunityTeamMember> lstToProcess;
		String strTargetRoles = 'Approver; URC Approver; Underwriter';
        Set<Id> setOppIds = new Set<Id>();
        for(OpportunityTeamMember objOpptyTeam:mapOldOpportunityTeam.values()){
            setOppIds.add(objOpptyTeam.OpportunityId);
        }
        
        if (lstnewOpportunityTeam != null) {
            lstToProcess = new List<OpportunityTeamMember>(lstnewOpportunityTeam);
            boolIsUpdate = True;
        } else {
            lstToProcess = new List<OpportunityTeamMember>(mapOldOpportunityTeam.values());
        }
        
        List<Opportunity> lstOpportunity = [Select StageName, Trav_Business_Unit__c From Opportunity where Id in :setOppIds];
        Map<Id,Opportunity> mapIdOpp = new Map<Id,Opportunity>();
        for(Opportunity objOpp:lstOpportunity){
            mapIdOpp.put(objOpp.Id, objOpp);
        }
 
        try{
            //get set of opportunity id's for team members to be updated and map team members to opportunities
            for(OpportunityTeamMember objOpptyTeam:lstToProcess){
                if (!boolIsUpdate && (mapIdOpp.get(objOpptyTeam.OpportunityId).StageName != 'Prospect') &&
                    (Constants.setOpportunityBSIBU.Contains(mapIdOpp.get(objOpptyTeam.OpportunityId).TRAV_Business_Unit__c)) &&
                    (strTargetRoles.Contains(objOpptyTeam.TeamMemberRole))) {

                        objOpptyTeam.adderror('Underwriter, Approver, URC Approver roles can\'t be added, updated, or removed from the Opportunity Team. These roles are assigned from Polaris.');
                    } else if((mapIdOpp.get(objOpptyTeam.OpportunityId).StageName != 'Prospect') &&
                     (Constants.setOpportunityBSIBU.Contains(mapIdOpp.get(objOpptyTeam.OpportunityId).TRAV_Business_Unit__c)) &&
                     boolIsUpdate && strTargetRoles.Contains(mapOldOpportunityTeam.get(objOpptyTeam.Id).TeamMemberRole) &&
                     mapOldOpportunityTeam.get(objOpptyTeam.Id).TeamMemberRole != objOpptyTeam.TeamMemberRole){
                        objOpptyTeam.adderror('Underwriter, Approver, URC Approver roles can\'t be added, updated, or removed from the Opportunity Team. These roles are assigned from Polaris.');
                    }
            }
            if(Test.isRunningTest()){
                throw new DMLException('Error');
            }
        }// End of try
        catch(exception objExp) {
            ExceptionUtility.logApexException(Constants.OPPORTUNITYTEAMMEMBERSERVICE, METHODNAME, objExp, null);
        }//End of catch
    }//End of preventTeamMemeberChangesBSI

}