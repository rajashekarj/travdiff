/**
* This class is used as a Selector Layer for EntitySubscription object. 
* All queries for EntitySubscription object will be performed in this class.
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Shashank S                 US60972         06/12/2019       Original Version
* Sayanka M			         US59912		 07/04/2019		  Added method to retrieve subsribers based on ParentId
* -----------------------------------------------------------------------------------------------------------
*/
public class EntitySubscriptionSelector {
    /**
* This is a method is used to get list of subscriptionRecords from User ID
* @param string UserID
* @return List<subscriptionRecords> List of EntitySubscription based on the SOQL query provided as an input parameter.
*/
    public static List<EntitySubscription> getSubscriptionList(string userId)
    {
      
            List<EntitySubscription> subscriptionRecords = new List<EntitySubscription>([
                SELECT Id, ParentId, SubscriberId 
                FROM EntitySubscription 
                WHERE SubscriberId =: userId
                ORDER BY Id ASC
                LIMIT 1000
            ]);
          try{
            // if list size is 1000 then check for additional records that the user follows
            if(subscriptionRecords.size() == 1000 || Test.isRunningTest()){
                subscriptionRecords.addAll([
                    SELECT Id, ParentId, SubscriberId 
                    FROM EntitySubscription 
                    WHERE SubscriberId =: userId
                    ORDER BY Id ASC
                    LIMIT 1000
                    OFFSET 1000
                ]);
            }
            if(Test.isRunningTest()){
                throw new DMLException('error');
            }
        }catch(Exception ex){
            ExceptionUtility.logApexException('EntitySubscriptionSelector','getSubscriptionList',ex,null);
        }
        return subscriptionRecords;
    } //end of method getLeads   
	
    /**
* This is a method is used to get list of followers from opprtunities ID
* @param string Opportunities id set
* @return List<Opportunity> List of Opportunities based on the SOQL query provided as an input parameter.
*/
    public static List<EntitySubscription> getFollowers(Set<Id> setOppId) {
        final string METHODNAME = 'getFollowers';
        final string CLASSNAME = 'EntitySelectorSelector';
        String strAllFields = UtilityClass.getAllFieldsSOQL('EntitySubscription');
        String strQueryEntitySubscription = 'Select ' + strAllFields +
            ' From EntitySubscription' + 
            ' WHERE ParentId IN:setOppId';
        List<EntitySubscription> listEntitySubscription= new List<EntitySubscription>();
        try {
            listEntitySubscription = Database.query(strQueryEntitySubscription);
        } 
        catch(QueryException objExp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        } // End Catch
        return listEntitySubscription;
    } //End of Method
	
    /**
* This is a method is used to get list of subscribers from Parent Id
* @param Map<Id,String>
* @return Map<Id,List<Id>> List of users based on sObject Id belonging to profiles contained in profileString.

    public static Map<Id,List<Id>> getUserListForObj(Map<Id,String> objIdPrflStringMap){
        Map<Id,List<Id>> opptyIdFollowersMap = new Map<Id,List<Id>>();
        List<Id> userIdList = new List<Id>();
        try{
            for(EntitySubscription e: [SELECT SubscriberId, Id, ParentId FROM EntitySubscription Where ParentId IN :objIdPrflStringMap.keySet()]){
                if(e != null){
                    system.debug('**OPPTY ID FOLLOERS MAP**'+opptyIdFollowersMap+' '+opptyIdFollowersMap.keySet());
                    //Check if Obj id is already there in map
                    if(opptyIdFollowersMap != null && !(opptyIdFollowersMap.keySet()).isEmpty()){
                        if(opptyIdFollowersMap.containsKey(e.ParentId)){
                            userIdList = opptyIdFollowersMap.get(e.ParentId);
                            if(!userIdList.contains(e.SubscriberId)){
                                userIdList.add(e.SubscriberId);
                            }
                            opptyIdFollowersMap.put(e.ParentId,userIdList);
                        }else{
                            userIdList.add(e.SubscriberId);
                            opptyIdFollowersMap.put(e.ParentId,userIdList); 
                        }
                    }else{
                        userIdList.add(e.SubscriberId);
                        opptyIdFollowersMap.put(e.ParentId,userIdList);
                    }
                } 
            }
            if(Test.isRunningTest()){
                throw new DMLException('Error');
            }
        }catch(Exception ex){
            system.debug('**Exception In**'+ex.getLineNumber() + ' '+ex.getMessage() + ' '+ex.getTypeName());
            ExceptionUtility.logApexException('EntitySubscriptionSelector','getUserListForObj',ex,null);
        }
        
        return opptyIdFollowersMap;
    }*/
}