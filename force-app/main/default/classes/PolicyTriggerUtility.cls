/**
* Utility Class for Policy Trigger Handler. This class will have logic for all the implementations.
* 
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer     User Story/Defect   Date        Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty       US66752     09/05/2019  Original Version
* Tejal Pradhan         US66752     09/06/2019  Added method - updateAccountRecordType
* Siddharth Menon       FSC         01/17/2020  Changed TRAV_Policy__c to InsurancePolicy
*/

public without sharing class PolicyTriggerUtility {
    final static string CLASS_NAME = 'PolicyTriggerUtility';

    /**
    * This method stamps old policy owner
    * @param lstnewPolicy - List of new values of Policy records and oldPolicy
    * @return void - returns void .
    */     
    public static void stampOldPolicyOwnerId(List<InsurancePolicy> lstnewPolicy, Map<Id, InsurancePolicy> mapOldPolicy) {
        string METHOD_NAME = 'stampOldPolicyOwnerId';

        //Loop through policies
        try{
            if(!lstnewPolicy.isEmpty()){
                for(InsurancePolicy objPol : lstnewPolicy ){
                    InsurancePolicy objOldRec = mapOldPolicy.get(objPol.id);
                    if(objPol.OwnerId!= objOldRec.OwnerId){
                        objPol.TRAV_Previous_Owner__c = String.valueOf(objOldRec.OwnerId);
                    }
                }
            }

            if(Test.isRunningTest()){
                throw new DMLException('Error');
            }
        }catch(Exception objExp){
            ExceptionUtility.logApexException(CLASS_NAME, METHOD_NAME, objExp, null);
        }
    }

    /**
    * This method updates the parent account record type to Client
    * @param lstnewPolicy - List of new values of Policy records
    * @return void - returns void .
    */ 
    public static void updateAccountRecordType(List<InsurancePolicy> lstnewPolicy) {
        string METHOD_NAME = 'updateAccountRecordType';

        List<Account> listAcc = new List<Account>();
        Set<Id> setAccIds = new Set<Id>();
        List<Account> listUpdatedAcc = new List<Account>();
            
        try{
            //Create set of Account Ids from corresponding Policy records
            for(InsurancePolicy objPol : lstnewPolicy){
                setAccIds.add(objPol.NameInsuredId);
            }
            //Retrieve account list
            if(!setAccIds.isEmpty()){
                listAcc = [SELECT Id, Name, RecordType.Id, RecordType.DeveloperName FROM Account WHERE RecordType.DeveloperName = 'TRAV_Prospect' AND Id IN :setAccIds];
            }
            //Loop through account list
            if(!listAcc.isEmpty()){
                Id clientRecordTypeId = SObjectType.Account.getRecordTypeInfosByDeveloperName().get('TRAV_Client').getRecordTypeId();
                //Change record type of Account to Client
                for(Account objAcc : listAcc){
                        objAcc.RecordTypeId = clientRecordTypeId;
                        listUpdatedAcc.add(objAcc);
                    }
                }
                //if list not empty, update
                if(!listUpdatedAcc.isEmpty()){
                    database.update(listUpdatedAcc,false);
                }
            if(Test.isRunningTest()){
                throw new DMLException('Error');
            }
        }catch(Exception objExp){
            ExceptionUtility.logApexException(CLASS_NAME, METHOD_NAME, objExp, null);
        }
    }

    /**
    * This method updates the date when BOR was changed
    * @param lstnewPolicy - List of new values of Policy records and oldMap
    * @return void - returns void .
    */ 
    public static void stampBORChangeDate(List<InsurancePolicy> lstnewPolicy, Map<Id, InsurancePolicy> mapOldPolicy) {
        string METHOD_NAME = 'stampBORChangeDate';

        //Loop through policies
        try{
            if(!lstnewPolicy.isEmpty()){
                for(InsurancePolicy objPol : lstnewPolicy ){
                    InsurancePolicy objOldRec = mapOldPolicy.get(objPol.id);
                    if(objPol.TRAV_Transaction_Type_Code__c != objOldRec.TRAV_Transaction_Type_Code__c){
                        objPol.TRAV_BOR_Changed_Date__c = objPol.TRAV_Transaction_Type_Code__c == 'BA' ? Date.today() : objPol.TRAV_BOR_Changed_Date__c;
                    }
                }
            }
            if(Test.isRunningTest()){
                throw new DMLException('Error');
            }
        }catch(Exception objExp){
            ExceptionUtility.logApexException(CLASS_NAME, METHOD_NAME, objExp, null);
        }
    }
}