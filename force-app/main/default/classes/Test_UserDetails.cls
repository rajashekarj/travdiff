/**
 * Test class for User Details

*
 * @Author : Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Sayanka Mohanty        		              14/08/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
@isTest
public class Test_UserDetails {
    @testSetup static void createTestData(){
        List<User> userList = new List<User>();
        //Create a BI user
        User u = TestDataFactory.createTestUser('BI - Support','Jenny Fernandes','Fernandes');
        system.assert(u != null);
        if(u != null){
            userList.add(u);
		}
        //Create a Sys Admin user
        User u1 = TestDataFactory.createTestUser('System Administrator','Jerry Fernandes','Fernandes');
        system.assert(u1 != null);
        if(u1 != null){
            userList.add(u1);
		}
        //Create a BSI user
        User u2 = TestDataFactory.createTestUser('BSI - Support','Jacky Fernandes','Fernandes');
        system.assert(u2 != null);
        if(u2 != null){
            userList.add(u2);
		}
        system.assert(userList.size() == 3);
        insert userList;
    }
    static testmethod void testUserDetails1(){
        Test.startTest();
        User u = [SELECT Id FROM User where Profile.Name = 'BI - Support' AND isActive = true LIMIT 1];
        User u2 = [SELECT Id FROM User where Profile.Name = 'System Administrator' AND isActive = true LIMIT 1];
        User u3 = [SELECT Id FROM User where Profile.Name = 'BSI - Support' AND isActive = true LIMIT 1];
        system.runAs(u){
          UserDetails.getUserInfo(Constants.BLANKSPACE);
		}
        system.runAs(u2){
          UserDetails.getUserInfo(Constants.BLANKSPACE);
		}
        system.runAs(u3){
          UserDetails.getUserInfo(Constants.BLANKSPACE);
		}
        /*Uncomment below line once main code has been moved to QA*/
        //String str = UserDetails.getAccountId();
       Test.stopTest(); 
    }
}