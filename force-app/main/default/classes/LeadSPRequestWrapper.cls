/**
* This class is the Wrapper Class for Building Initial Requests
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect                  Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty        US73994/US73995/US73996          24/01/2020      Original Version
* -----------------------------------------------------------------------------------------------------------
*/
public class LeadSPRequestWrapper {
    @auraEnabled
    public String operation{get;set;}
    @auraEnabled
    public String cbeID{get;set;}
    @auraEnabled
    public LeadSPSearchRequestWrapper searchRequestWrapper{get;set;}
    @auraEnabled
	public LeadSPCreateRequestWrapper createRequestWrapper{get;set;}
    @auraEnabled
    public LeadSPSFFieldsWrapper getSFFieldsResponseWrapper{get;set;}
}