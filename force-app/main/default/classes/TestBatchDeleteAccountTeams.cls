/**
* Test class for BatchDeleteAccountTeams Class
*
* @Author : Deloitte Consulting
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------
* Isha Shukla            US60806              06/10/2019       Original Version
* -----------------------------------------------------------------------------------------------------------
*/
@isTest
private class TestBatchDeleteAccountTeams {


    /**
* This method will test scenarios when Account Executive is list for opportunity closed more than 12 months
* @param no parameters
* @return void
*/
    static testmethod void testBatch() {
        //create the custom setting for Opportunity trigger
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'OpportunityTeamMemberTrigger';
        insert objCustomsetting;

        //create the custom setting for general data setting
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Deferred;Written;Declined;Quote Unsuccessful';
        insert objGeneralDataSetting;

        Id currentUserId = UserInfo.getUserId();
        User objUser = [Select Id,Name from User where id = :currentUserId];
        List<User> lstUser;
        Account objProspectAccount;
        Account objAgencyAccount;
        System.runAs(objUser){
            User objUsr = TestDataFactory.createTestUser('BI - National Accounts','TestNA2','LastNameNA2');
            User objUsr3 = TestDataFactory.createTestUser('BI - National Accounts','TestNA3','LastNameNA3');
            User objUsr2 = TestDataFactory.createTestUser('BI - National Accounts','TestNA','LastNameNA');
            lstUser = new List<User>{objUsr,objUsr2,objUsr3};
                insert lstUser;


            //create account record
            objProspectAccount = TestDataFactory.createProspect();
            objAgencyAccount = TestDataFactory.createAgency();
            List<Account> lstAccount = new List<Account>{objProspectAccount,objAgencyAccount};
                insert lstAccount;

            //create a pricebook
            Pricebook2 biPricebook = TestDataFactory.createTestPricebook('BI - National Accounts Pricebook');
            List<Pricebook2> lstPricebook = new List<Pricebook2>{biPricebook};
                insert lstPricebook;


            //Creating opportunity records
            List<Opportunity> lstOpportunity = TestDataFactory.createOpportunities(5, objProspectAccount.Id, 'BI-NA');
            lstOpportunity[0].TRAV_Agency_Broker__c = objAgencyAccount.Id;
            insert lstOpportunity;

            //creating opportunity team member records
            List<OpportunityTeamMember> lstOpportunityTeam = TestDataFactory.createOpportunityTeamMembers(3,lstUser[1].Id,lstOpportunity[0].Id);
            lstOpportunityTeam[1].OpportunityId = lstOpportunity[4].Id;
            lstOpportunityTeam[1].UserId = lstUser[0].Id;
            lstOpportunityTeam[0].UserId = lstUser[2].Id;
            insert lstOpportunityTeam;
            // Closing opportunities with close date 13 months back
            for(Integer i =0 ; i < 5 ; i++) {
                lstOpportunity[i].StageName = 'Written';
                lstOpportunity[i].CloseDate = System.today().addMonths(-13);
            }
            update lstOpportunity;
        }
        //Test batch execution
        Test.startTest();
        BatchDeleteAccountTeams batchObj = new BatchDeleteAccountTeams();
        Id batchId = Database.executeBatch(batchObj);
        Test.stopTest();
        System.assertEquals([SELECT
                             Id
                             FROM
                             AccountTeamMember
                             WHERE
                             AccountId = :objProspectAccount.Id
                             AND
                             UserId = :lstUser[0].Id].size() == 0 ,true);
        System.assertEquals([SELECT
                             Id
                             FROM
                             AccountTeamMember
                             WHERE
                             AccountId = :objAgencyAccount.Id
                             AND
                             UserId = :lstUser[2].Id].size() == 0 ,true);
    }
    private static testmethod void coverScheduler(){
        BatchDeleteAccountTeams batchSchObj = new BatchDeleteAccountTeams();
        String sch = '0 0 23 * * ?';
        system.schedule('DeleteAccountTeam', sch, batchSchObj);
    }
}