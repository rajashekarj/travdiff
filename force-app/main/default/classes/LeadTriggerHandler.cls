/**
 * Handler Class for Lead Trigger. This class won't have any logic and
 * will call other Lead Service/Utilities class.
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shashank Agarwal       US59527              05/23/2019       Added code in beforeInsert and beforeUpdate
 * Bhanu Reddy			  US63671	           07/16/2019       Added new Method setExpirationDate
 * Shruti Gupta			  US62950			   07/19/2019		Added method setLastName
 * Siddharth M			  US73196			   10/02/2020		Updated After Update with 
 * 																	setAccountFieldsAfterConvert call
 * -----------------------------------------------------------------------------------------------------------
 */

public class LeadTriggerHandler implements ITriggerHandler {
    
    //Handler Method for Before Insert.
    public void beforeInsert(List<sObject> newItems) {
        LeadService.standardLeadStatusSync(newItems,null);
        LeadService.setExpirationDate(newItems,null);
		LeadService.setLastName(newItems,null);
    }
    
    //Handler Method for Before Update.
    public void beforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
        List<Lead> newList = newItems.values();
        LeadService.standardLeadStatusSync(newList,(Map<Id, Lead>)oldItems);
        LeadService.setExpirationDate(newList,(Map<Id, Lead>)oldItems);
		LeadService.setLastName(newList,(Map<Id, Lead>)oldItems);
    }
    //Handler Method for Before Delete.    
    public void beforeDelete(Map<Id, sObject> oldItems) {
        
    }
    
    //Handler method for After Insert.
    public void afterInsert(Map<Id, sObject> newItems) {
                   
    }

    //Handler method for After Update.    
    public void afterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
       	
        LeadService.convertLeads((Map<Id, Lead>)newItems, (Map<Id, Lead>)oldItems);   
        LeadService.setAccountFieldsAfterConvert(newItems.values());
    }
    //Handler method for After Delete    
    public void afterDelete(Map<Id, sObject> oldItems) {
    
    }
    
    //Handler method for After Undelete.
    public void afterUndelete(Map<Id, sObject> oldItems) {
    
    } 
}