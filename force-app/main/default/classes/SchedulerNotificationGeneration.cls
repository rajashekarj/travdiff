/**
* Schedular Class for BatchNotificationGeneration. This class will have logic to schedule the batch.
* Modification Log    :
* ------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* ------------------------------------------------------------------------------------------------------
* Shashank Agarwal       US59903              06/26/2019      Original Version
* 
*/
global class SchedulerNotificationGeneration implements Schedulable {
   Integer serialNumber = 1; 
   global void execute(SchedulableContext SC) {
      BatchNotificationGeneration objBatchNotificationGeneration = new BatchNotificationGeneration(serialNumber); 
      Database.executeBatch(objBatchNotificationGeneration, 200);
   }
}