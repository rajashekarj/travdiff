/**
 * Handler Class for OpportunityLineItem Trigger. This class won't have any logic and
 * will call other Opportunity Service/Utilities class.
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------
 * Erec Lawrie            US62419            06/28/2019       Added code to beforeInsert
 * Shashank Agarwal       US63871            07/30/2019       Added code to beforeInsert, beforeUpdate, before delete
 * Shashank Agarwal       US65788            08/05/2019       Added code to beforeInsert, beforeUpdate, before delete
 * -----------------------------------------------------------------------------------------------------------
 */
public class OpportunityLineItemTriggerHandler implements ITriggerHandler{
    //Handler Method for Before Insert.
    public void beforeInsert(List<sObject> newItems) {
        //OpportunityLineItemService.assignProductDefaults((List<OpportunityLineItem>)newItems);
        OpportunityLineItemService.updateTeamMemberOnLineItemUpdate((List<OpportunityLineItem>)newItems,null);
    }

    //Handler Method for Before Update.
    public void beforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
        List<OpportunityLineItem> lstNewOpportunityLineItem = newItems.values();
        if(!OpportunityProductPopUpUtility.opptyPopUpContext){
            OpportunityLineItemService.addErrorWhenOpportunityStageSubmission(lstNewOpportunityLineItem );
        }
        OpportunityLineItemService.updateTeamMemberOnLineItemUpdate(lstNewOpportunityLineItem,(Map<Id, OpportunityLineItem>)oldItems);
    }

    //Handler Method for Before Delete.
    public void beforeDelete(Map<Id, sObject> oldItems) {
        List<OpportunityLineItem> lstNewOpportunityLineItem = oldItems.values();
        OpportunityLineItemService.addErrorWhenOpportunityStageSubmission(lstNewOpportunityLineItem );
		OpportunityLineItemService.stampOpportunityLineItemIdOnDelete((Map<Id, OpportunityLineItem>) oldItems);
    }

    //Handler method for After Insert.
    public void afterInsert(Map<Id, sObject> newItems) {
       List<OpportunityLineItem> lstNewOpportunityLineItem = newItems.values();
        OpportunityLineItemService.addProductNamestoOpportunity(lstNewOpportunityLineItem);
        OpportunityLineItemService.populatePotentialPremium(lstNewOpportunityLineItem,Null);
        //US74195
        if(!System.isBatch()){
            System.enqueueJob(new NotificationUtility(newItems.values(), null, 'OpportunityLineItem'));
        }
    }

    //Handler method for After Update.
    public void afterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
        System.debug('Yes'+Trigger.isExecuting);
        List<OpportunityLineItem> lstNewOpportunityLineItem = newItems.values();
        OpportunityLineItemService.populatePotentialPremium(lstNewOpportunityLineItem,(Map<Id,OpportunityLineItem>)oldItems);
        //US74195
        if(!System.isBatch()){
            System.enqueueJob(new NotificationUtility(newItems.values(), oldItems, 'OpportunityLineItem'));
        }
    }

    //Handler method for After Delete
    public void afterDelete(Map<Id, SObject> oldItems) {
       OpportunityLineItemService.removeProductNameFromOpty((Map<Id, OpportunityLineItem>)oldItems);
       OpportunityLineItemService.updateTeamMemberOnLineItemUpdate(Null,(Map<Id, OpportunityLineItem>)oldItems);
       OpportunityLineItemService.populatePotentialPremium(Null,(Map<Id,OpportunityLineItem>)oldItems);

    }

    //Handler method for After Undelete.
    public void afterUndelete(Map<Id, sObject> oldItems) {

    }
}
