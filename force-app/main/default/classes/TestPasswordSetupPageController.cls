/**
 * This class is used as a test class of the controller for Password setup page
 *
 *
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect   Date            Description
 * -----------------------------------------------------------------------------------------------------------              
 * Isha Shukla            US81639             27/04/2020      Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
@isTest
private class TestPasswordSetupPageController {
    @isTest static void testPasswordSetup(){
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='BI - National Accounts']; 
        User objUser = new User(Alias = 'standt', Email='standardBINAuser@testorgBINA.com.invalid', 
            EmailEncodingKey='UTF-8', LastName='TestingBINA', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = objProfile.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardBINAuser@testorgBINA.com',TRAV_External_Id__c='n12345');
        insert objUser;
        PageReference objPage = Page.PasswordSetupPage;
        Test.setCurrentPage(objPage);
        PasswordSetupPageController controller = new PasswordSetupPageController();
        controller.username = 'standardBINAuser@testorgBINA.com';
        controller.password = 'test123@';
        controller.setupPassword();
        PasswordSetupPageController controller2 = new PasswordSetupPageController();
        controller2.username = 'test';
        controller2.password = 'test123@';
        controller2.setupPassword();
        PasswordSetupPageController controller3 = new PasswordSetupPageController();
        controller3.externalId = 'n12345';
        controller3.password = 'test123@';
        controller3.setupPassword();
        System.assertEquals(controller.message.contains('success'), true);
    }
}
