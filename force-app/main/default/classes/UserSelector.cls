/**
* This class is used as a Selector Layer for User object. 
* All queries for User object will be performed in this class.
*
*
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* BHarat Madaan            US62552         06/24/2019       Original Version
* Bharat Madaan              NA            08/20/2019       Changed the  name and param of first method getUsersByRegion
* Shashank Agarwal         US65899         08/19/2019       Added method getUsersByEmail
* -------------------------------------------------------------------------------------------------------------
*/ 
public class UserSelector {
    public static Map<Id,String> mapManagerToRole;//added for US73394 
	
    /**
* This is a method is used to get list of active users
* @param : Set of String
* @return List<User> List of user.
*/
    public static list<User> getUsersByExternalId(Set<String> externalIdSet)
    {
    
        //don't process empty list
        if(externalIdSet.size() > 0) {

            String strUsersWithRegionQuery= 'SELECT Id, Name, UserName, TRAV_External_Id__c, TRAV_BU__c  from User where TRAV_External_Id__c IN : externalIdSet';        
            List<User> listUser = Database.Query(strUsersWithRegionQuery);
            System.debug('**getUsersByExternalId ListUser Size '+listUser.size());
            return listUser;         
        } else {
            //no records to return
            return new List<User>();
        }
    }	
	
	
  /**
* This is a method is used to get list of active users
* @param : Set of String
* @return List<User> List of user.
*/
    /*public static list<User> getUsersByRegion(Set<String> regionSet)
    {
    
        //don't process empty list
        if(regionSet.size() > 0) {
            String regionSetAsString = '';
            for(String s :regionSet){
                regionSetAsString = regionSetAsString + '\'' + s + '\',';
            }
            regionSetAsString = regionSetAsString.removeEnd(',');
            
            String strUsersWithRegionQuery= 'SELECT Id, Name, UserName, TRAV_BU__c, TRAV_Region__c from User ' +
                'where TRAV_Region__c INCLUDES (' + regionSetAsString +')';
                    
            List<User> listUser = Database.Query(strUsersWithRegionQuery);
            return listUser;         
        } else {
            //no records to return
            return new List<User>();
        }
    } */
    
   /**
* This is a method is used to get a user by Id
* @param : null
* @return List<User> List of user.
*/
    public static list<User> getUsersById(Set<Id> userId) {
        List<User> listUser = [select Id, 
                               Name, 
                               FirstName,
                               lastname,
                               UserName, 
                               TRAV_BU__c, 
                               TRAV_Office__c,
                               TRAV_Region__c,
                               Profile.Name
                               from User 
                               where Id IN :userId];

        return listUser;         
    }
    
    /**
* This is a method is used to get a user by Id
* @param : null
* @return List<User> List of user.
*/
    public static list<User> getUsersManagerById(Set<Id> userId) {
        List<User> listUser = [select Id, 
                               Name, 
                               ManagerId,
                               Manager.ManagerId,
                               Manager.Manager.ManagerId,
                               Manager.Manager.Manager.ManagerId
                               from User 
                               where Id IN :userId];

        return listUser;         
    }
    
    /**
* This is a method is used to get a user profile
* @param : null
* @return List<User> List of user.
*/
    public static list<User> getUserProfileName(Set<Id> userId) {
        System.debug('No of SOQL'+Limits.getQueries());
        List<User> listUser = [select Id, 
                               Profile.Name
                               from User 
                               where Id IN :userId];
        return listUser;         
    }
      /**
* This is a method is used to get a user by Email
* @param : setEmail
* @return List<User> List of user.
*/
    public static list<User> getUsersByEmail(Set<String> setEmail) {
        final string METHODNAME = 'getUsersByEmail';
        final string CLASSNAME = 'UserSelector';
        List<User> listUser = [select Id,
                               Email
                               from User 
                               where Email IN :setEmail];
        return listUser;         
   } 
    
    /**
    * This is a method is used to get managers of user by set of user ids
    * @param : Set<Id>
    * @return List<User> List of user.
    */
    public static Map<Id,Set<Id>> getManagers(Set<Id> setUserId) {
       final string METHODNAME = 'getManagers';
       final string CLASSNAME = 'UserSelector';
       List<User> lstUsers = new List<User>();
        //Changes for US73394 starts
       mapManagerToRole = new Map<Id,String>();
       Map<Id,Set<Id>> mapUserToManagers = new Map<Id,Set<Id>>();//User to Managers map
       try {
            lstUsers = new List<User>([
                                     SELECT 
                                        Id, ManagerId, Manager.ManagerId, Manager.Manager.ManagerId,
                						Manager.Manager.Manager.ManagerId, Manager.UserRole.Name, Manager.Manager.UserRole.Name,
                					    Manager.Manager.Manager.UserRole.Name, Manager.Manager.Manager.Manager.UserRole.Name, 
                						Manager.UserRoleId, Manager.Manager.UserRoleId,
                					    Manager.Manager.Manager.UserRoleId, Manager.Manager.Manager.Manager.UserRoleId
                                     FROM 
                                        User 
                                     WHERE 
                                        ManagerId != null 
                                     AND 
                                        Id IN :setUserId 
                                     AND 
                                        IsActive = true
                              ]);
           if(lstUsers.size() > 0) {
               //Populating map of users to their managers upto level 4
                for(User objUser : lstUsers ) {
                    mapUserToManagers.put(objUser.Id,new Set<Id>{objUser.ManagerId});
                    if(objUser.Manager.UserRoleId != null){
                          mapManagerToRole.put(objUser.ManagerId,objUser.Manager.UserRole.Name);  
                    }
                    if(objUser.Manager.ManagerId != null) { 
                        mapUserToManagers.get(objUser.Id).add(objUser.Manager.ManagerId);
                        if(objUser.Manager.Manager.UserRoleId != null){
                          mapManagerToRole.put(objUser.Manager.ManagerId,objUser.Manager.Manager.UserRole.Name);  
                        }
                    }
                    if(objUser.Manager.Manager.ManagerId != null) {
                        mapUserToManagers.get(objUser.Id).add(objUser.Manager.Manager.ManagerId);
                        if(objUser.Manager.Manager.Manager.UserRoleId != null){
                          mapManagerToRole.put(objUser.Manager.Manager.ManagerId,objUser.Manager.Manager.Manager.UserRole.Name);  
                        }
                    }
                    if(objUser.Manager.Manager.Manager.ManagerId != null) {
                        mapUserToManagers.get(objUser.Id).add(objUser.Manager.Manager.Manager.ManagerId);
                        if(objUser.Manager.Manager.Manager.Manager.UserRoleId != null){
                          mapManagerToRole.put(objUser.Manager.Manager.Manager.ManagerId,objUser.Manager.Manager.Manager.Manager.UserRole.Name);  
                        }
                    }
                }
           }
           system.debug('mapManagerToRole>inuserselector>>'+mapManagerToRole);
		   
       } //End of try
       catch(QueryException exp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, exp, null);
                
       } // End Catch
        system.debug('lstUsers>'+lstUsers);
       return mapUserToManagers;
    }
    /**
    * This is a method is used to get managers of user by set of user ids
    * @param : Set<Id>
    * @return List<User> List of user.
    */
    public static Map<Id,Set<Id>> getManagersForServicePartners(Set<Id> setUserId) {
       final string METHODNAME = 'getManagersForServicePartners';
       final string CLASSNAME = 'UserSelector';
       List<User> lstUsers = new List<User>();
       mapManagerToRole = new Map<Id,String>();
       Map<Id,Set<Id>> mapUserToManagers = new Map<Id,Set<Id>>();//User to Managers map
       try {
            lstUsers = new List<User>([
                                     SELECT 
                						Id, ManagerId, Manager.ManagerId, Manager.Manager.ManagerId, 
                						Manager.UserRole.Name, Manager.Manager.UserRole.Name,
                					    Manager.Manager.Manager.UserRole.Name, 
                						Manager.UserRoleId, Manager.Manager.UserRoleId,
                					    Manager.Manager.Manager.UserRoleId
                                     FROM 
                                        User 
                                     WHERE 
                                        ManagerId != null 
                                     AND 
                                        Id IN :setUserId 
                                     AND 
                                        IsActive = true
                              ]);
           if(lstUsers.size() > 0) {
               //Populating map of users to their managers upto level 4
                for(User objUser : lstUsers ) {
                    mapUserToManagers.put(objUser.Id,new Set<Id>{objUser.ManagerId});
                    if(objUser.Manager.UserRoleId != null){
                        mapManagerToRole.put(objUser.ManagerId,objUser.Manager.UserRole.Name); 
                        if(objUser.Manager.Manager.UserRoleId != null){
                          mapManagerToRole.put(objUser.Manager.ManagerId,objUser.Manager.Manager.UserRole.Name);  
                        }
                    }
                    if(objUser.Manager.ManagerId != null) { 
                        mapUserToManagers.get(objUser.Id).add(objUser.Manager.ManagerId);
                        if(objUser.Manager.Manager.UserRoleId != null){
                          mapManagerToRole.put(objUser.Manager.ManagerId,objUser.Manager.Manager.UserRole.Name);  
                        }
                    }
                    if(objUser.Manager.Manager.ManagerId != null) {
                        mapUserToManagers.get(objUser.Id).add(objUser.Manager.Manager.ManagerId);
                        if(objUser.Manager.Manager.Manager.UserRoleId != null){
                          mapManagerToRole.put(objUser.Manager.Manager.ManagerId,objUser.Manager.Manager.Manager.UserRole.Name);  
                        }
                    }
                }
           }
           //Changes for US73394 ends
		   if(Test.isRunningTest()){
                throw new QueryException();
            }
           
       } //End of try
       catch(QueryException exp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, exp, null);
                
       } // End Catch
        system.debug('mapUserToManagers>>'+mapUserToManagers);
       return mapUserToManagers;
    }

 
}