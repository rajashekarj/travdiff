/**
 * Handler Class for Service Partner Request Trigger. This class won't have any logic and
 * will call other Opportunity Service/Utilities class.
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shashank Agarwal       US62549              07/01/2019       Added code in afterInsert
 * -----------------------------------------------------------------------------------------------------------
 */
public class ServicePartnerRequestTriggerHandler implements ITriggerHandler{
    //Handler Method for Before Insert.
    public void beforeInsert(List<sObject> newItems) {
       
    }
    
    //Handler Method for Before Update.
    public void beforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
        
    }
    //Handler Method for Before Delete.    
    public void beforeDelete(Map<Id, sObject> oldItems) {
        
    }
    
    //Handler method for After Insert.
    public void afterInsert(Map<Id, sObject> newItems) {
        List<TRAV_Service_Partner_Request__c> lstSPRequest = newItems.values();
        ServicePartnerRequestService.taskCreationOnInsertion(lstSPRequest);

    }

    //Handler method for After Update.    
    public void afterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {  
        
    }
    //Handler method for After Delete    
    public void afterDelete(Map<Id, sObject> oldItems) {
    
    }
    
    //Handler method for After Undelete.
    public void afterUndelete(Map<Id, sObject> oldItems) {
    
    } 

}