/**
* This class will create dynamic endpoints, perform callouts and retrieve response
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect   		Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty        US69347/US69863          11/10/2019      Original Version
* Sayanka Mohanty		 US81176 				  04/14/2020.     Addition of payload validation
* -----------------------------------------------------------------------------------------------------------
*/
public class RequestResponseHandler {
    public static Boolean mulesoftContext = false;
    /**
* This method will dynamically create endpoint and do the callout
* @param String strCmAPIName , Map<String,String> mapParamValues
* @return HTTPResponse responseResult
*/ 
    public static HttpResponse constructEndPointAndDoCallout(String strCmAPIName , Map<String,String> mapParamValues, String strJsonRequestBody, Boolean blnSpecialParameters){
        system.debug('**REQUEST JSON**'+strJsonRequestBody);
        final string METHODNAME = 'constructEndPointAndDoCallout';
        HttpResponse responseResult;
        String strEndPoint = '';
        Boolean blnAND = false;
        String formattedBody = '';
        try{
            
            //retrieve metadata record based on record developer name
            TRAV_Agency_MDM_Endpoint_Config__mdt objEndpointConfig = [SELECT TRAV_Path__c,
                                                                      TRAV_Request_Type__c,
                                                                      TRAV_Timeout_Threshold__c,
                                                                      TRAV_Named_Credential_Reference__c
                                                                      FROM TRAV_Agency_MDM_Endpoint_Config__mdt
                                                                      WHERE DeveloperName = :strCmAPIName LIMIT 1];
            
        //Initialise path of endpoint
        if(objEndpointConfig != null){
            strEndPoint = objEndpointConfig.TRAV_Named_Credential_Reference__c + objEndpointConfig.TRAV_Path__c;
            
            //replace any placeholders with params from map
            if(mapParamValues != null && !blnSpecialParameters){
                for(String key : mapParamValues.keySet() ){
					if(strEndPoint.contains('['+key+']') || strEndPoint.contains(key)){
                        if(strEndPoint.contains('['+key+']')){
                           strEndPoint = strEndPoint.replace('['+key+']', mapParamValues.get(key) ); 
                        }else{
                            strEndPoint = strEndPoint.replace(key, mapParamValues.get(key) ); 
                        }
						strEndPoint = strEndPoint.remove(']');
                        strEndPoint = strEndPoint.remove('[');
                        system.debug('**final End point path**'+strEndPoint);
                    }
                }
            }
            else if(mapParamValues != null && blnSpecialParameters){
                for(String key : mapParamValues.keySet()){
                    if(blnAND && blnSpecialParameters){
                        strEndPoint += '%20and%20';
                    }
                    strEndPoint += key + '=' +  '' + mapParamValues.get(key);
                    blnAND = true;
                }
                
                strEndPoint += '&depth=1&suppressLinks=true';
            }            
            system.debug('**final End point path**'+strEndPoint);
            //call function to do callout and return response
            //Changes to strip nulls and improve performance
            if(String.isNotBlank(strJsonRequestBody) && strCmAPIName.containsIgnoreCase('Create')){
                
                formattedBody = stripJsonNulls(strJsonRequestBody);
                
            }else if(String.isNotBlank(strJsonRequestBody) && strCmAPIName.containsIgnoreCase('Update')){
                system.debug('**ORIGINAL REQUEST BODY**'+strJsonRequestBody);
				formattedBody = strJsonRequestBody.containsIgnoreCase('null') ? strJsonRequestBody.replace('null', '""') : strJsonRequestBody ;
                system.debug('**CHANGED REQUEST BODY**'+formattedBody);
            }
            else{
                formattedBody = strJsonRequestBody;
            }
            responseResult = getRESTAPIResponse(formattedBody, strEndPoint, objEndpointConfig.TRAV_Request_Type__c , Integer.valueOf(objEndpointConfig.TRAV_Timeout_Threshold__c));
        }

    }catch(exception objExp) {
        system.debug('**exception caught**'+objExp.getLineNumber() + '*message*' +objExp.getMessage());
        ExceptionUtility.logApexException('RequestResponseHandler', METHODNAME, objExp, null);
    }
    return responseResult;
}

/**
* This method will do the callout and return response
* @param String strJsonRequestBodyString, String strPath, String strMethodType, Integer intTimeoutValue
* @return HTTPResponse responseResult
*/ 
public static HttpResponse getRESTAPIResponse(String strJsonRequestBodyString, String strPath, String strMethodType, Integer intTimeoutValue){
    if(strPath.contains('MulesoftProxy')||strPath.contains('MulesoftAgencyProxy')){
        mulesoftContext = true;
	}
	TRAV_Mulesoft_Auth_Settings__c objMulesoft = TRAV_Mulesoft_Auth_Settings__c.getOrgDefaults();
    system.debug('**Context**'+mulesoftContext+objMulesoft);
    final string METHODNAME = 'getRESTAPIResponse';
    HttpResponse responseResult;
    try{
        //Set REST API Parameters
        HttpRequest request = new HttpRequest();
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setEndpoint(strPath);
        request.setMethod(strMethodType);
        request.setTimeout(intTimeoutValue);
        if(mulesoftContext && objMulesoft!= null && !Test.isRunningTest()){
            system.debug('CLIENT_ID'+objMulesoft.TRAV_Client_Id__c);
            request.setHeader('client_id', objMulesoft.TRAV_Client_Id__c );
            request.setHeader('Client_Secret', objMulesoft.TRAV_Client_Secret__c);
        }
        if(String.isNotBlank(strJsonRequestBodyString)){
            //String formattedBody = stripJsonNulls(strJsonRequestBodyString);
            //String formattedBody = strJsonRequestBodyString.replace('null', '" "');
            //system.debug('**FORMATTED BODY**'+ formattedBody);
            request.setBody(strJsonRequestBodyString);
        }
        Http http = new Http();
        system.debug('request - ' + request);
        responseResult = http.send(request);
    }catch(exception objExp) {
        system.debug('**Error at **'+ objExp.getLineNumber() +'**'+objExp.getMessage());
        ExceptionUtility.logApexException('RequestResponseHandler', METHODNAME, objExp, null);
    }
    return responseResult;
}
/**
* This method will strip all null
* @param string JsonString
* @return String 
*/ 
    public static string stripJsonNulls(string JsonString)
    {
        if(JsonString != null)   	
        {
            JsonString = JsonString.replaceAll('\"[^\"]*\":null',''); //basic removeal of null values
            JsonString = JsonString.replaceAll(',{2,}', ','); //remove duplicate/multiple commas
            JsonString = JsonString.replace('{,', '{'); //prevent opening brace from having a comma after it
            JsonString = JsonString.replace(',}', '}'); //prevent closing brace from having a comma before it
            JsonString = JsonString.replace('[,', '['); //prevent opening bracket from having a comma after it
            JsonString = JsonString.replace(',]', ']'); //prevent closing bracket from having a comma before it
        }
        return JsonString;
    }
}