/**
 * Implements ITriggerHandler for the UserTrigger as part of the trigger handler framework. 
 *
 * Modification Log:
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Ben Climan             US79918              04/08/2020       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
public without sharing class UserTriggerHandler implements ITriggerHandler {
    /**
     * Reference the SailPoint_Derivation_Rule__mdt records to obtain the derivation rules
     * for User record inserts via the SailPoint integration.
     * @param {List<SObject>} newItems. Provided via Trigger.New
     */
    public void beforeInsert(List<SObject> newItems) {        
        List<SailPoint_Derivation_Rule__mdt> rules = new List<SailPoint_Derivation_Rule__mdt>();

        // Filter on fields that should be updated on INSERT
        for (SailPoint_Derivation_Rule__mdt rule : SailPointUtilities.getSailPointDerivationRules()) {
            if (rule.Set_on_Trigger__c.toLowerCase().contains('insert')) {
                rules.add(rule);
            } 
        }
        SailPointUtilities.deriveFields((List<User>)newItems, rules);
    }

    /**
     * Reference the SailPoint_Derivation_Rule__mdt records to obtain the derivation rules
     * for User record updates via the SailPoint integration.
     * @param {Map<Id, SObject>} newItems. Provided via Trigger.newMap
     * @param {Map<Id, SObject>} oldItems. Provided via Trigger.oldMap
     */
    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        // Check User record Override Settings to determine which field updates to commit to the DB
        SailPointUtilities.checkOverrideFields(newItems, oldItems);
        
        List<SailPoint_Derivation_Rule__mdt> rules = new List<SailPoint_Derivation_Rule__mdt>();

        // Filter on fields that should be updated on UPDATE
        for (SailPoint_Derivation_Rule__mdt rule : SailPointUtilities.getSailPointDerivationRules()) {
            if (rule.Set_on_Trigger__c.toLowerCase().contains('update')) {
                rules.add(rule);
            } 
        }
        
        // Handle auto-derivation fields
        SailPointUtilities.deriveFields((List<User>)newItems.values(), rules);
    }

    /**
     * Reference the SailPoint_Derivation_Rule__mdt records to obtain the derivation rules
     * for assigning Users to Permission Sets or Public Groups on AFTER INSERT tigger via the SailPoint integration.
     * @param {Map<Id, SObject>} newItems. Provided via Trigger.newMap
     */
    public void afterInsert(Map<Id, SObject> newItems) {
        List<SailPoint_Derivation_Rule__mdt> rules = new List<SailPoint_Derivation_Rule__mdt>();

        for (SailPoint_Derivation_Rule__mdt rule : SailPointUtilities.getSailPointDerivationRules()) {
            if (rule.Set_on_Trigger__c.toLowerCase().contains('insert')) {
                rules.add(rule);
            } 
        }

        SailPointUtilities.assignUsers((List<User>)newItems.values(), rules);
    }

    /**
     * Reference the SailPoint_Derivation_Rule__mdt records to obtain the derivation rules
     * for assigning Users to Permission Sets or Public Groups on AFTER UPDATE tigger via the SailPoint integration.
     * @param {Map<Id, SObject>} newItems. Provided via Trigger.newMap
     * @param {Map<Id, SObject>} oldItems. Provided via Trigger.oldMap
     */
    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        List<SailPoint_Derivation_Rule__mdt> rules = new List<SailPoint_Derivation_Rule__mdt>();

        for (SailPoint_Derivation_Rule__mdt rule : SailPointUtilities.getSailPointDerivationRules()) {
            if (rule.Set_on_Trigger__c.toLowerCase().contains('update')) {
                rules.add(rule);
            } 
        }

        SailPointUtilities.assignUsers((List<User>)newItems.values(), rules);
    }

    public void beforeDelete(Map<Id, SObject> oldItems) { }

    public void afterDelete(Map<Id, SObject> oldItems) { }

    public void afterUndelete(Map<Id, SObject> oldItems) { } 
}