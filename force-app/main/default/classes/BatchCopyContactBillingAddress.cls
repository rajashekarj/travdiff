/**
* Batch Class for Opportunity. This class will have logic for cloning of opportunity record if Record 
* Type is Renewal on opportunity 
* Modification Log    :
* ------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* Bhanu Reddy		       US69829			    10/31/2019       Original Version  		
* ------------------------------------------------------------------------------------------------------
*/ 
global class BatchCopyContactBillingAddress implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) { 
          return
           Database.getQueryLocator('Select id,accountid,recordtypeid,OtherStreet,OtherCity,OtherState,OtherPostalCode,OtherCountry from contact where recordtype.developername in(\'TRAV_Agency_Licensed_Contact\',\'TRAV_Agency_Non_Licensed_Contact\')');
    } //end of start Method
    global void execute(Database.BatchableContext BC, List<Contact> lstC0ntacts) {
        set<id> setAccountids = new set<id>();
        map<id,Account> mapAccountIdToObject = new map<id,Account>();
        try{
            for(Contact objContact :lstC0ntacts){
                setAccountids.add(objContact.AccountId);
            } //end of for contact iteration
            
             if(!setAccountids.isEmpty()){
            for(Account  objAccount : AccountSelector.getAccountBillidAddress(setAccountids)){
                mapAccountIdToObject.put(objAccount.id,objAccount);
            } //end for iteration of accounts
        } //end if -account's empty check
        if(!mapAccountIdToObject.isEmpty()){
            //iterate over the contacts and assing billing address
            for(contact objCOntact :lstC0ntacts ){
                if(mapAccountIdToObject.containsKey(objCOntact.AccountId)){
                    objCOntact.OtherStreet = ContactService.checknull(mapAccountIdToObject.get(objCOntact.AccountId).BillingStreet);
                    objCOntact.OtherCity = ContactService.checknull(mapAccountIdToObject.get(objCOntact.AccountId).BillingCity);
                    objCOntact.OtherState = ContactService.checknull(mapAccountIdToObject.get(objCOntact.AccountId).BillingState);
                    objCOntact.OtherPostalCode = ContactService.checknull(mapAccountIdToObject.get(objCOntact.AccountId).BillingPostalCode);
                    objCOntact.OtherCountry = ContactService.checknull(mapAccountIdToObject.get(objCOntact.AccountId).BillingCountry);
                   } // end of if map comtains checkbox
                
            }// end for iteration of contacts
            
            if(!lstC0ntacts.isEmpty()){
              Database.update(lstC0ntacts,false);  
            }
			if(Test.isRunningTest()){
                throw new QueryException();
            }
        }
        }catch(Exception objException){
            ExceptionUtility.logApexException('BatchCopyContactBillingAddress', 'Execute', objException, null);
             
        } //end catch
    } // end of execute Method
    global void finish(Database.BatchableContext BC) {
        
    } //end of finish Method
    
} //end of class