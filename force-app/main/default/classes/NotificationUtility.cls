/**
*    @author Sayanka Mohanty
*    @date   06/21/2019 
*    @description : This is the Utility class for processing metadata and sObject Records

Modification Log:
------------------------------------------------------------------------------------
Developer                       Date                Description
------------------------------------------------------------------------------------
Sayanka Mohanty              02/07/2019          Original Version*
*/
public class NotificationUtility implements Queueable{
    public List<sObject> listToProcess;
    public Map<Id,sObject> oldMap;
    public String sObjectType;
    public static Boolean isFieldChanged = false;
    /*
*  @author      : Shashank Shastri
*  @description : Parameterized constructor. This will accept the trigger.New, trigger.oldMap and sObjectType
*  @param       : List<sObject> listToProcess, Map<Id,sObject> oldMap, String sObjectType
*  @return      : Void
*  dummy deploy 10/16/19 ejjoseph
*/   
    public NotificationUtility(List<sObject> listToProcess, Map<Id,sObject> oldMap, String sObjectType){
        this.listToProcess = listToProcess;
        this.oldMap = oldMap;
        this.sObjectType = sObjectType;
    }
    /*
*  @author      : Shashank Shastri
*  @description : Interface method override. 
*  @param       : QueueableContext context
*  @return      : Void
*/   
    public void execute(QueueableContext context) {
        //Call the generateNotification Method to create task/event/chatter post
        generateNotification(listToProcess, oldMap, sObjectType);
    }
    /*
*  @author      : Shashank Shastri
*  @description : Main Method to be invoked from the execute method. This methodimplements the filters on the list of sObjects
*                                 
*  @param       : QueueableContext context
*  @return      : Void
*/       
    public static void generateNotification(List<sObject> listToProcess,Map<Id,sObject> oldMap,String sObjectType){ //Remove the sObjectType from the Params
        try{
            Schema.SObjectType objType = listToProcess[0].Id.getsObjectType();
            String queryStr = '';
            boolean loadManagers = false;
            Set<Id> idList = new Set<Id>();
            Boolean roleVerified = true;
            String IdListStr = '';
            Map<String, List<Id>> managerRoleMap = new Map<String, List<Id>>();
            Set<String> managerRole = new Set<String>();
            List<String> listOfRoles = new List<String>();
            List<NotificationWrapper> notWrapperList = new List<NotificationWrapper>();
            Map<String,String> fieldValueMap = new Map<String,String>();
            Set<String> fieldAPINames = new Set<String>();
            List<OpportunityTeamMember> opptyTeamList = new List<OpportunityTeamMember>();
            Map<String,String> mergeFieldAPIValMap = new Map<String,String>();
            //Get list of metadata records where object source is opportunity and is Active
            List<NotificationGenConfig__mdt> notificationRecList = new  List<NotificationGenConfig__mdt>();
            List<Id> finalUserList;
            notificationRecList = [SELECT Id,
                                   QualifiedApiName,
                                   TRAV_FieldList__c,
                                   TRAV_Role_Profile__c,
                                   TRAV_IsRealtime__c,
                                   TRAV_sObject_Target__c,
                                   TRAV_sObject_Source__c,
                                   TRAV_Subject__c,
                                   TRAV_Contains_Hyperlink__c,
                                   TRAV_AssignTo__c,
                                   TRAV_QueryFilter__c, 
                                   TRAV_FieldToBeUpdated__c, 
                                   TRAV_TriggerEvent__c,
                                   TRAV_Role__c,
                                   TRAV_WhatId__c,
                                   TRAV_Is_Manager__c,
                                   (SELECT Id, TRAV_NotificationGenConfig__c,
                                    TRAV_FieldAPI__c, 
                                    TRAV_Operator__c, 
                                    TRAV_Values__c,
                                    TRAV_Override_Field_Change__c,
                                    TRAV_Type__c 
                                    FROM Notification_Filters__r ORDER BY TRAV_Sequence__c ASC)
                                   FROM NotificationGenConfig__mdt WHERE 
                                   TRAV_Active__c = true AND TRAV_sObject_Source__c = :sObjectType AND TRAV_IsRealtime__c = true];
            //For each Configuration, check if sObject matches and create a wrapper 
            system.debug('saymohanty'+JSON.serialize(notificationRecList));
            if(!notificationRecList.isEmpty()){
                if(oldMap != NULL){
                    idList = oldMap.keySet();
                }
                else{
                    Map<Id, sObject> tempObject = new Map<Id, sObject>(listToProcess);
                    idList = tempObject.keySet();
                }
                //Convert it for usage in SOQL query
                if(!idList.isEmpty()){
                    IdListStr = convertSetString(idList);
                }
                for(NotificationGenConfig__mdt notConfig : notificationRecList){
                    List<String> tempRoleList = new List<String>();
                    //Check if query field is not empty
                    if(notConfig.TRAV_FieldList__c != null){
                        List<String> tempList = (notConfig.TRAV_FieldList__c.toLowerCase().replaceAll(' ','').trim()).split(',');
                        for(String apiName : tempList){
                            fieldAPINames.add(apiName.trim());
                        }
                    }
                    if(notConfig.TRAV_Role__c != null && String.isNotBlank(notConfig.TRAV_Role__c) && notConfig.TRAV_Is_Manager__c != null && notConfig.TRAV_Is_Manager__c == false){
                        if(!notConfig.TRAV_Role__c.contains(';')){
                            listOfRoles.add(notConfig.TRAV_Role__c);
                        }else{
                            tempRoleList = (notConfig.TRAV_Role__c).split(';');
                            listOfRoles.addAll(tempRoleList);
                        }
                    }
                    else if(notConfig.TRAV_Is_Manager__c != null && notConfig.TRAV_Is_Manager__c == true){
                        //Add this role to the manager  role map
                        loadManagers = true;
                    }
                }
                if(loadManagers){
                    managerRoleMap = getManagerRoleMap();                
                }
                //system.debug('managerRoleMap   managerRoleMap '+managerRoleMap);
                //Prepare the SOQL with the set of fields from previous step.
                queryStr = 'SELECT ' + getSeparatedString(fieldAPINames) + ' FROM ' + sObjectType + ' WHERE Id IN '+IdListStr;
                
                system.debug('query string ' + queryStr);
                Map<String, Map<String, List<Id>>> oppIdVsRoleAndMembers = new Map<String, Map<String, List<Id>>>();
                if(sObjectType == 'Opportunity' && oldMap != NULL){
                    oppIdVsRoleAndMembers = NotificationUtility.generateOpportunityTeamMembers(oldMap.keySet(), listOfRoles);
                }
                
                system.debug('*oppty is vs role**'+oppIdVsRoleAndMembers);
                List<sObject> listOfRecords = database.query(queryStr);
                // Iterate over the list of records and check if it satisfies all the filters
                for(sObject sObjRec : listOfRecords){
                    Map<String,List<Id>> rolevsUserListMap = new Map<String,List<Id>>();
                    rolevsUserListMap = oppIdVsRoleAndMembers.get(String.valueOf(sObjRec.id));
                    system.debug('**Role user list**'+rolevsUserListMap);
                    // Iterate over the Notification Config to check if it meets the filter criteria
                    for(NotificationGenConfig__mdt notif : notificationRecList){
                        finalUserList = new List<Id>();
                        if(String.isNotBlank(notif.TRAV_Role__c)){
                            if(rolevsUserListMap != null && notif.TRAV_Role__c != null){
                                if(notif.TRAV_Role__c.contains(';')){
                                    List<String> roleList = (notif.TRAV_Role__c).split(';');
                                    for(String rl : roleList){
                                        if(rolevsUserListMap.containsKey(rl)){
                                            finalUserList.addAll(rolevsUserListMap.get(rl));  
                                        }
                                    }
                                    if(String.isNotBlank(notif.TRAV_AssignTo__c )){
                                        if((notif.TRAV_AssignTo__c).contains('.')){
                                            finalUserList.add(NotificationGenerator.getfieldApiValues(sObjRec, notif.TRAV_AssignTo__c));
                                        }else{
                                            finalUserList.add((Id)sObjRec.get(notif.TRAV_AssignTo__c));
                                        }
                                    }
                                }else{
                                    if(rolevsUserListMap.containsKey(notif.TRAV_Role__c)){
                                        finalUserList.addAll(rolevsUserListMap.get(notif.TRAV_Role__c)); 
                                        if(String.isNotBlank(notif.TRAV_AssignTo__c )){
                                            if((notif.TRAV_AssignTo__c).contains('.')){
                                                finalUserList.add(NotificationGenerator.getfieldApiValues(sObjRec, notif.TRAV_AssignTo__c));
                                            }else{
                                                finalUserList.add((Id)sObjRec.get(notif.TRAV_AssignTo__c));
                                            }
                                        }
                                    }
                                }
                                
                            }
                        }else if(notif.TRAV_AssignTo__c != NULL){
                            if((notif.TRAV_AssignTo__c).contains('.')){
                                finalUserList.add(NotificationGenerator.getfieldApiValues(sObjRec, notif.TRAV_AssignTo__c));
                            }else{
                                finalUserList.add((Id)sObjRec.get(notif.TRAV_AssignTo__c));
                            }
                        }
                        if(notif.TRAV_Is_Manager__c == true && sObjRec.get('TRAV_Owner_Region__c') != NULL){
                            if (finalUserList == null || finalUserList.isEmpty())
                            	finalUserList = new List<Id>();
                            
                            //Get List Of Regions for the given Stage
                            String key = '';
                            key += notif.TRAV_Role__c;
                            if(sObjRec.get('TRAV_Owner_Region__c') != NULL){
                                key+= (string)sObjRec.get('TRAV_Owner_Region__c');
                            }
                            if(sObjRec.get('StageName') != NULL){
                                key += (string)sObjRec.get('StageName');
                            }
                            if(notif.TRAV_Role__c.contains('Actuary') && sObjRec.get('TRAV_Owner_Region__c') == 'Central'){
                                key += (string)sObjRec.get('TRAV_Travelers_Office__c');
                                
                            }
                            List<Id> tempRegionMap = managerRoleMap.get(key);
                            if(tempRegionMap != null){
                                Set<Id> tempUserIds = new Set<Id>();
                                tempUserIds.addAll(tempRegionMap);
                                 if (finalUserList == null || finalUserList.isEmpty())
                                	finalUserList = new List<Id>(tempUserIds);
                                else
                                    finalUserList.addAll(tempUserIds);
                                
                                //finalUserList = tempRegionMap.get((string)sObjRec.get('TRAV_Owner_Region__c'));
                            }
                        }
                        system.debug('after the key check'+finalUserList);
                        //List of boolean for the list of filters for this metatada
                        List<Boolean>listOfFilterResult = new List<Boolean>();
                        for(TRAV_NotificationFilter__mdt filter : notif.Notification_Filters__r){
                            
                            system.debug('value of the filer'+filter);
                            sObject oldRec;
                            if(oldMap != null){
                                oldRec = oldMap.get((Id)sObjRec.get('Id'));
                            }
                            //sObject obj = (string)oldMap.get((Id)sObjRec.get('Id')).get(filter.TRAV_FieldAPI__c)
                            //Iterate over each filter and check if record qualifies for notification Generation
                            system.debug('**EvaluationList**'+evaluateFilterCriteria(sObjRec, filter, oldRec)+' **notifId** '+notif.TRAV_Subject__c + '  '  +filter.TRAV_FieldAPI__c);
                            //ExceptionUtility.logApexException('NotificationUtility','This is the filters picked up to loop through ' + filter.TRAV_FieldAPI__c + filter.MasterLabel + filter.TRAV_Values__c ,null,null);
                            listOfFilterResult.add(evaluateFilterCriteria(sObjRec, filter, oldRec));
                            system.debug(notif.TRAV_Subject__c+'Apex CRUD Violations '+listOfFilterResult);
                        }
                        // system.debug(notif.TRAV_Subject__c+'**Saymohanty validation result '+listOfFilterResult);
                        system.debug(notif.TRAV_QueryFilter__c+'     '+notif.TRAV_Subject__c+'**Saymohanty validation result '+calculateBooleanValue(listOfFilterResult, notif.TRAV_QueryFilter__c));
                        if(notif.TRAV_Subject__c == 'Populate Claim Counts'){
                            system.debug(notif.Notification_Filters__r);
                        }
                        //Validate Role for Chatter
                        if(String.isNotBlank(notif.TRAV_Role__c) && notif.TRAV_sObject_Source__c.equalsIgnoreCase('OpportunityLineItem') && notif.TRAV_WhatId__c == 'OpportunityId' ){
                            roleVerified = checkRoleInTeam(NotificationGenerator.getfieldApiValues(sObjRec, notif.TRAV_WhatId__c), notif.TRAV_Role__c);
                        }
                        // Validate the filter Criteria
                        if(notif.TRAV_QueryFilter__c != null && calculateBooleanValue(listOfFilterResult, notif.TRAV_QueryFilter__c) && roleVerified){
                            //call ionWrapper and pass fieldValuemap as well for additional updates
                            //Prepare field API name vs value map
                            mergeFieldAPIValMap = prepareMergeFieldMap(sObjRec, fieldAPINames);
                            //SYSTEM.debug('****Saymohanty MERGE FIELD API MAP**'+json.serialize(mergeFieldAPIValMap));
                            //Added by sayanka:Field update evaluation
                            String fieldsToBeUpdated = '';
                            if(String.isNotBlank(notif.TRAV_FieldToBeUpdated__c)){
                                fieldsToBeUpdated = notif.TRAV_FieldToBeUpdated__c;
                                fieldValueMap = NotificationCustomUpdateHandler.calculateFieldUpdates(fieldsToBeUpdated,mergeFieldAPIValMap);
                                system.debug('****Saymohanty FIELD VALUE MAP**'+fieldValueMap);
                            }
                            //Added for additional profile filtering
                            String roleProfile = String.isNotBlank(notif.TRAV_Role_Profile__c) ? notif.TRAV_Role_Profile__c : '';
                            system.debug(notif.TRAV_Subject__c+'sorry sorry sorry sorry'+finalUserList);
                            NotificationWrapper nw = createNotificationWrapper(notif.TRAV_Subject__c ,(string)sObjRec.get(notif.TRAV_WhatId__c),notif.TRAV_sObject_Target__c,Userinfo.getUserId(),roleProfile,finalUserList,fieldValueMap,mergeFieldAPIValMap,notif.TRAV_Contains_Hyperlink__c);
                            notWrapperList.add(nw);
                            system.debug('Wrapper List'+notWrapperList);
                            //ExceptionUtility.logApexException('NotificationUtility','Notification wrapper list ' + json.serialize(notWrapperList),null,null);
                        }
                    }
                }
                //call utility and pass list of wrappers for Object Record Creation
                
                if(!notWrapperList.isEmpty()){
                    system.debug('**Final Wrapper List**'+json.serialize(notWrapperList));
                    NotificationGenerator.assignJob(notWrapperList,fieldValueMap);
                    
                }
            }         
        }
        catch(Exception ex){
            system.debug(ex.getMessage()+'   '+ex.getLineNumber());
            ExceptionUtility.logApexException('NotificationUtility','GenerateNotification',ex,null);
        }
    }
    /*
*  @author      : Sayanka Mohanty
*  @description : Convert a List<String> into a quoted, comma separated String literal for inclusion in a dynamic SOQL Query
*  @param       : List<String> mapKeySet
*  @return      : String
*/   
    public static NotificationWrapper createNotificationWrapper(String subject,Id objId,String objType,String owner,String roleProfile,List<Id> userList,Map<String,String> fieldValueMap,Map<String,String> mergeFieldAPIValMap,Boolean containHLink ){
        NotificationWrapper nw = new NotificationWrapper();
        nw.Subject = subject;
        nw.objectId = objId;
        nw.type = objType;
        nw.Owner = owner;
        nw.containsHyperlink = containHLink;
        nw.userIdList = userList;
        nw.fieldValueMap = fieldValueMap;
        nw.mergeFieldAPIValMap = mergeFieldAPIValMap;
        nw.roleProfile = roleProfile;
        return nw;
    }
    /*
*  @author      : Sayanka Mohanty
*  @description : Convert a List<String> into a quoted, comma separated String literal for inclusion in a dynamic SOQL Query
*  @param       : List<String> mapKeySet
*  @return      : String
*/
    public static String convertSetString(Set<Id> mapKeySet){
        //convert a Set<String> into a quoted, comma separated String literal for inclusion in a dynamic SOQL Query
        {
            String newSetStr = '' ;
            for(String str : mapKeySet)
                
                newSetStr += '\'' + str + '\',';
            
            newSetStr = newSetStr.lastIndexOf(',') > 0 ? '(' + newSetStr.substring(0,newSetStr.lastIndexOf(',')) + ')' : newSetStr ;
            //System.debug('**New Set**' + newSetStr);   
            
            return newSetStr;
            
        }
    }
    /*
*  @author      : Shashank Shastri
*  @description : Convert a Set<String> into a comma separated String literal for inclusion in a dynamic SOQL Query
*  @param       : List<String> mapKeySet
*  @return      : String
*/
    public static String getSeparatedString(Set<String> setOfString){
        //convert a Set<String> into a comma separated String literal for inclusion in a dynamic SOQL Query
        String newSetStr = '' ;
        for(String str : setOfString){
            
            newSetStr += ' ' + str + ',';
        }
        newSetStr = newSetStr.lastIndexOf(',') > 0 ? '(' + newSetStr.substring(0,newSetStr.lastIndexOf(',')) + ')' : newSetStr ;
        newSetStr = newSetStr.replace('(','');
        newSetStr = newSetStr.replace(')','');
        //System.debug('**New Set**' + newSetStr);   
        return newSetStr;
    }
    public static Boolean evaluateFilterCriteria(sObject record, TRAV_NotificationFilter__mdt filter, sObject oldRec){
        Boolean result = false;
        //system.debug('**Filter Name**'+ filter.TRAV_FieldAPI__c +' **New Value**'+record.get(filter.TRAV_FieldAPI__c) + ' **Old Value**'+oldRec.get(filter.TRAV_FieldAPI__c));
        String currentValue = NotificationGenerator.getFieldAPIValues(record, filter.TRAV_FieldAPI__c); 
        //system.debug('**Current value**'+currentValue);
        boolean bypassFieldChange = false;
        
        if(filter.TRAV_FieldAPI__c.contains('.')){
            bypassFieldChange = true;
            system.debug('5 to 10 mins'+filter.TRAV_FieldAPI__c);
        }
        if(filter.TRAV_Override_Field_Change__c == true){
            isFieldChanged = true;
        }else{
            isFieldChanged = false;
        }
        //If (isFieldChanged is false and fieldValue changed)
        if(!isFieldChanged && (bypassFieldChange || (NotificationGenerator.getFieldAPIValues(record, filter.TRAV_FieldAPI__c) != NotificationGenerator.getFieldAPIValues(oldRec, filter.TRAV_FieldAPI__c)))){
            isFieldChanged = true;
            //ExceptionUtility.logApexException('BethTest','It is setting field as changed but not calling for value',null,null);
        }
        system.debug('bypass'+bypassFieldChange+'isfied'+isFieldChanged);
        
        if(bypassFieldChange || isFieldChanged){
            if(filter.TRAV_Type__c == 'String' ){
                system.debug(filter.TRAV_FieldAPI__c+'**sshastri string eval**'+currentValue +filter.TRAV_Operator__c+filter.TRAV_Values__c );
                if(currentValue == NULL || currentValue == '' || currentValue.equalsIgnoreCase('null')){
                    currentValue = '';
                }
                if(filter.TRAV_Values__c == NULL || filter.TRAV_Values__c == '' || filter.TRAV_Values__c.equalsIgnoreCase('null')){
                    filter.TRAV_Values__c = '';
                }
                system.debug('**Current value**'+currentValue +'**Filter value**'+filter.TRAV_Values__c);
                if(filter.TRAV_Operator__c == '=' && ((String) currentValue).equalsIgnoreCase(filter.TRAV_Values__c)){
                    result = true;
                }
                else if(filter.TRAV_Operator__c == 'ISCHANGED' && (!((String) currentValue).equalsIgnoreCase((String)oldRec.get(filter.TRAV_FieldAPI__c)))){
                    result = true;
                }
                else if(filter.TRAV_Operator__c == '!=' && !((String) currentValue).equalsIgnoreCase(filter.TRAV_Values__c)){
                    system.debug('**CurrentValue**'+currentValue);
                    system.debug('**FilterValuTe**'+filter.TRAV_Values__c);
                    result = true;
                }
                else if(filter.TRAV_Operator__c == 'Contains' && ((String) currentValue).containsIgnoreCase(filter.TRAV_Values__c)){
                    result = true;
                }
                else if(filter.TRAV_Operator__c == 'IN' && (filter.TRAV_Values__c).containsIgnoreCase((String) currentValue)){
                    result = true;
                }
                else if(filter.TRAV_Operator__c == 'NOT IN' && (!((filter.TRAV_Values__c).containsIgnoreCase((String) currentValue)))){
                    result = true;
                    
                }
            }
            else if(filter.TRAV_Type__c == 'Date'){
                if(filter.TRAV_Operator__c == '=' && date.parse((String) record.get(filter.TRAV_FieldAPI__c)) == date.parse(filter.TRAV_Values__c)){
                    result = true;
                }
                else if(filter.TRAV_Operator__c == '!=' && date.parse((String) record.get(filter.TRAV_FieldAPI__c)) != date.parse(filter.TRAV_Values__c)){
                    result = true;
                }
                else if(filter.TRAV_Operator__c == '>' && date.parse((String) record.get(filter.TRAV_FieldAPI__c)) > date.parse(filter.TRAV_Values__c)){
                    result = true;
                }
                else if(filter.TRAV_Operator__c == '>=' && date.parse((String) record.get(filter.TRAV_FieldAPI__c)) >= date.parse(filter.TRAV_Values__c)){
                    result = true;
                }
                else if(filter.TRAV_Operator__c == '<' && date.parse((String) record.get(filter.TRAV_FieldAPI__c)) < date.parse(filter.TRAV_Values__c)){
                    result = true;
                }
                else if(filter.TRAV_Operator__c == '<=' && date.parse((String) record.get(filter.TRAV_FieldAPI__c)) <= date.parse(filter.TRAV_Values__c)){
                    result = true;
                }
            }
            else if(filter.TRAV_Type__c == 'Number'){
                //system.debug('**Record is**'+json.serialize(record));
                
                //system.debug(currentValue+'**Complete new expresiion**'+filter.TRAV_FieldAPI__c+' '+filter.TRAV_Operator__c+' '+filter.TRAV_Values__c+ 'Actual value'+currentValue);
                
                if(currentValue == NULL || currentValue == '' || currentValue.equalsIgnoreCase('null')){
                    currentValue = '0';
                }
                if(filter.TRAV_Values__c == NULL || filter.TRAV_Values__c == '' || filter.TRAV_Values__c.equalsIgnoreCase('null')){
                    filter.TRAV_Values__c = '0';
                }
                //system.debug('**Record is**'+json.serialize(record));
                system.debug('**Complete new expresiion**'+filter.TRAV_FieldAPI__c+' '+filter.TRAV_Operator__c+' '+filter.TRAV_Values__c);
                
                if(filter.TRAV_Operator__c == '=' && Decimal.valueOf(currentValue) == Decimal.valueOf(filter.TRAV_Values__c)){
                    result = true;
                }
                
                else if(filter.TRAV_Operator__c == '!=' && (Decimal.valueOf(currentValue)) != Decimal.valueOf(filter.TRAV_Values__c)){
                    result = true;
                }
                else if(filter.TRAV_Operator__c == '>'  && (Decimal.valueOf(currentValue)) > Decimal.valueOf(filter.TRAV_Values__c)){
                    result = true;
                }
                else if(filter.TRAV_Operator__c == '<'  && (Decimal.valueOf(currentValue)) < Decimal.valueOf(filter.TRAV_Values__c)){
                    result = true;
                }
                else if(filter.TRAV_Operator__c == '>='  && Decimal.valueOf(currentValue) >= Decimal.valueOf(filter.TRAV_Values__c)){
                    result = true;
                }
                else if(filter.TRAV_Operator__c == '<='  && Decimal.valueOf(currentValue) <= Decimal.valueOf(filter.TRAV_Values__c)){
                    result = true;
                }
            }
            else if(filter.TRAV_Type__c == 'Boolean'){
               
                if(filter.TRAV_FieldAPI__c.contains('.')){
                    system.debug('aa'+NotificationGenerator.getFieldAPIValues(record, filter.TRAV_FieldAPI__c));
                  if(filter.TRAV_Operator__c == '=' && (NotificationGenerator.getFieldAPIValues(record, filter.TRAV_FieldAPI__c)) == filter.TRAV_Values__c){
                    result = true;
                }  
                }else{
		if(filter.TRAV_Operator__c == '=' && ((Boolean) record.get(filter.TRAV_FieldAPI__c)) == Boolean.valueOf(filter.TRAV_Values__c)){
                    result = true;
                }
                else if(filter.TRAV_Operator__c == '!=' && ((Boolean) record.get(filter.TRAV_FieldAPI__c)) != Boolean.valueOf(filter.TRAV_Values__c)){
                    result = true;
                }
                }
            }
        }            
        return result;
    }
    /*
*  @author      : Sayanka Mohanty
*  @description : Convert a List<String> into a quoted, comma separated String literal for inclusion in a dynamic SOQL Query
*  @param       : List<String> mapKeySet
*  @return      : String
*/
    public static Boolean calculateBooleanValue(List<Boolean> decisionParams,String filterString){
        Boolean finalVal = false;
        integer i = 0;
        if(!decisionParams.isEmpty()){
            //Check if all are true
            if(!decisionParams.contains(False)){
                finalVal = true;
            }
            //Check if all are false
            else if(!decisionParams.contains(True)){
                finalVal = false;
                
            }else{
                for(i = 0;i< decisionParams.size();i++){
                    filterString = filterString.replace(String.valueOf(i+1), String.valueOf(decisionParams[i]));   
                }
                if(filterString.contains('AND')){
                    filterString = filterString.replace('AND', '&&');
                }
                if(filterString.contains('OR')){
                    filterString = filterString.replace('OR', '||');
                }
                //system.debug('Final String'+filterString);
                finalVal = BooleanExpression.eval(filterString.toUpperCase());
            }
            
        }
        system.debug('**Final Value**'+finalVal);
        return finalVal;
        
    }
    /*
*  @author      : Shruti Gupta
*  @description : Get list of opportunity team members for respective Opportunity Id and Role
*  @param       : List<Id> lstOpptyId, List<String> lstRoles
*  @return      : List<OpportunityTeamMember>
*/
    public static Map<String, Map<String, List<Id>>> generateOpportunityTeamMembers(Set<Id> lstOpptyId,List<String> lstRoles){
        Map<String, Map<String, List<Id>>> opportunityVsTeamRole = new Map<String, Map<String, List<Id>>>();
        List<OpportunityTeamMember> lstOpptyTeamMember = [SELECT Id,Name,OpportunityId,TeamMemberRole,Title,TRAV_Active__c,UserId FROM OpportunityTeamMember WHERE TeamMemberRole IN: lstRoles AND OpportunityId IN: lstOpptyId AND TRAV_Active__c = true];
        //system.debug(lstOpptyTeamMember);
        Map <String, List<Id>> mapOfRoleVsUserId;
        List<Id> userIds;
        for(OpportunityTeamMember oppTeamMem : lstOpptyTeamMember){
            if(opportunityVsTeamRole.get(oppTeamMem.OpportunityId) == NULL){
                //Insert a new Instance
                mapOfRoleVsUserId = new Map<String, List<Id>>();
                userIds = new List<Id>();
                userIds.add(oppTeamMem.UserId);
                mapOfRoleVsUserId.put(oppTeamMem.TeamMemberRole, userIds);
                opportunityVsTeamRole.put(oppTeamMem.OpportunityId, mapOfRoleVsUserId);
                //system.debug('when no Id'+mapOfRoleVsUserId);
            }
            else{
                // Add a new entry to the existing Map
                mapOfRoleVsUserId = opportunityVsTeamRole.get(oppTeamMem.OpportunityId);
                if(mapOfRoleVsUserId.containsKey(oppTeamMem.TeamMemberRole)){
                    List<Id> userIdForRole = mapOfRoleVsUserId.get(oppTeamMem.TeamMemberRole);
                    userIdForRole.add(oppTeamMem.UserId);
                    mapOfRoleVsUserId.put(oppTeamMem.TeamMemberRole, userIdForRole);
                    opportunityVsTeamRole.put(oppTeamMem.OpportunityId, mapOfRoleVsUserId);
                    //system.debug('when role there'+lstOpptyTeamMember);
                }
                else{
                    //Roll not already there in the map
                    List<Id> userIdForRole = new List<Id>();
                    userIdForRole.add(oppTeamMem.UserId);
                    mapOfRoleVsUserId.put(oppTeamMem.TeamMemberRole, userIdForRole);
                    opportunityVsTeamRole.put(oppTeamMem.OpportunityId, mapOfRoleVsUserId);
                    //system.debug('when no role'+lstOpptyTeamMember);
                }
            }
        }
        return opportunityVsTeamRole;
    }
    public static Map<String, String> prepareMergeFieldMap(sObject sObjRec, Set<String> fieldList){
        Map<String, String> mergeFieldMap = new Map<String, String>();
        String value = '';
        for(String apiName : fieldList){
            String trimApiName = apiName.replaceAll('(\\s)','');
            if(NotificationGenerator.getFieldAPIValues(sObjRec, trimApiName) == NULL){
                value = '';
            }
            else{
                value = NotificationGenerator.getFieldAPIValues(sObjRec, trimApiName);
            }
            mergeFieldMap.put(trimApiName, value);
        }
        return mergeFieldMap;
    }
    //Get Manager Role Map
    public static Map<String, List<Id>> getManagerRoleMap(){
        Map<String, Map<String,List<Id>>> stageVsRegionMap = new  Map<String, Map<String,List<Id>>>();
        Map<String, List<Id>> managerRoleMap = new Map<String, List<Id>>();
        Set<String> userNameSet = new Set<String>();
        List<TRAV_Service_Partner_Process__mdt> managerMDT = new List<TRAV_Service_Partner_Process__mdt>();
        managerMDT = [SELECT TRAV_UserName__c,
                      TRAV_Opprtunity_Team_Region__c,
                      TRAV_Opportunity_Team_Role__c,
                      TRAV_StageName__c ,
                      TRAV_Travelers_Office__c
                      FROM TRAV_Service_Partner_Process__mdt 
                      WHERE TRAV_IS_Manager__c = TRUE];
        for(TRAV_Service_Partner_Process__mdt mdtRec : managerMDT){
            if(mdtRec.TRAV_UserName__c != null && mdtRec.TRAV_UserName__c != ''){
                userNameSet.add(mdtRec.TRAV_UserName__c);
            }
        }
        //If set of UserNames not blank then query the users
        if(!userNameSet.isEmpty()){
            //get userList
            List<User> userList = [SELECT Id,TRAV_External_Id__c, TRAV_Region__c FROM User Where TRAV_External_Id__c IN : userNameSet AND TRAV_Region__c !=NULL];
            List<Id> userIdList = new List<Id>();
            for(TRAV_Service_Partner_Process__mdt mdtRec : managerMDT){
                for(User uRec : userList){
                    //Insert entry into the Region Map
                    String key = mdtRec.TRAV_Opportunity_Team_Role__c+mdtRec.TRAV_Opprtunity_Team_Region__c+mdtRec.TRAV_StageName__c;
                    if (mdtRec.TRAV_Travelers_Office__c != null && mdtRec.TRAV_Opprtunity_Team_Region__c == 'Central' && mdtRec.TRAV_Opportunity_Team_Role__c == 'Actuary'){
                        key += mdtRec.TRAV_Travelers_Office__c;
                    }
                    if(managerRoleMap.get(key) == NULL){
                        //add a new Instance of user Id List
                        if(uRec.TRAV_External_Id__c == mdtRec.TRAV_UserName__c){
                            //Only if the condition satisfied then insert else not
                            userIdList = new List<Id>();
                            userIdList.add(uRec.Id);
                            managerRoleMap.put(key,userIdList);
                        }
                    }
                    else{
                        userIdList = managerRoleMap.get(key);
                        if(uRec.TRAV_External_Id__c == mdtRec.TRAV_UserName__c){
                            //Only if the condition satisfied then insert else not
                            userIdList = managerRoleMap.get(key);
                            userIdList.add(uRec.Id);
                            managerRoleMap.put(key,userIdList);
                        }
                    }
                    
                }
            }
        }
        system.debug(managerRoleMap);
        return managerRoleMap;
    }
    /*
*  @author      : Sayanka Mohanty
*  @description : Get list of opportunity team members for respective Opportunity Id and Role
*  @param       : List<Id> lstOpptyId, List<String> lstRoles
*  @return      : List<OpportunityTeamMember>
*/
    public static Boolean checkRoleInTeam(String idOppty,String strRole){
        Boolean checkRolePresent = false;
        List<OpportunityTeamMember> listRole = [SELECT Id, TeamMemberRole, TRAV_Active__c,OpportunityId FROM OpportunityTeamMember where TeamMemberRole = :strRole AND OpportunityId = :idOppty AND TRAV_Active__c = true  LIMIT 1];
        if(!listRole.isEmpty()){
            checkRolePresent = true;
        }
        return checkRolePresent;
    }
}