/**
* This class is used as a Selector Layer for Lead object. 
* All queries for Lead object will be performed in this class.
*
*
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Bhanu Reddy                 US59527         05/24/2019       Original Version
* Shashank Shastri            US60972         06/10/2019       Addes methods to query list of Lead records for
*                                                              a set of Ids passed as Param
* Shashank Shastri            US60972         06/21/2019       Updated SQOL to filter out the converted Leads.
* Shruti Gupta				  US62950		  07/19/2019	   Added method getProfileName
* -----------------------------------------------------------------------------------------------------------
*/
public class LeadSelector {
    
    /**
* This is a method is used to get list leads from ID
* @param string Lead id
* @return List<Lead> List of leads based on the SOQL query provided as an input parameter.
*/
    public static list<Lead> getLeads(string strLeadId)
    {
        
        list<Lead> lstLeads = new list<lead>([
            SELECT
            Id, 
            ConvertedAccountId,
            ConvertedAccount.Name,
            OwnerId
            FROM  
            Lead 
            where 
            id = :strLeadId
        ]);
        return lstLeads;
    } //end of method getLeads 
    /**
* This is a method is used to get list leads for a given UserID
* @param string User id
* @return List<Lead> List of leads based on the SOQL query provided as an input parameter.
*/
    public static Map<Id,Lead> getLeadsIOwn(string strUserId)
    {
        Map<Id, Lead> mapOfIdVsLeads = new Map<Id, Lead>([
            SELECT Id, Name, State, Status,
            LeadSource, LastModifiedDate, isConverted,
            TRAV_BU_Interested__c, ConvertedOpportunityId,
            Industry 
            FROM Lead 
            WHERE OwnerId =: strUserId AND IsConverted = false
            ORDER BY Name ASC
        ]);
        return mapOfIdVsLeads;
    } //end of method getLeadsIOwn
    /**
* This is a method is used to get list leads for a given UserID
* @param string User id
* @return List<Lead> List of leads based on the SOQL query provided as an input parameter.
*/
    public static List<Lead> getLeadsBySetOfIds(Set<Id> setOfLeadIds)
    {
        List<Lead> listOfLeads = new List<Lead>([
            SELECT Id, Name, State, Status,
            LeadSource, LastModifiedDate,  isConverted,
            TRAV_BU_Interested__c, ConvertedOpportunityId,
            Industry 
            FROM Lead  
            WHERE Id IN: setOfLeadIds AND IsConverted = false
            ORDER BY Name ASC
        ]);
        return listOfLeads;
    } //end of method getLeadsIOwn
 /**
* This is a method is used to get Profile name for logged in user
* @param string Profile Name
* @return List<Lead> List of leads based on the SOQL query provided as an input parameter.
*/
    public static String getProfileName()
    {
        List<Profile> profileName = new List<Profile>([
            SELECT Id, Name
            FROM Profile  
            WHERE Id =: UserInfo.getProfileId()
        ]);
        return profileName[0].Name;
    } //end of method getProfileName
}