public class MDMUpdateIndividualWrapper {
    
    
    public class BMLoc {
        @AuraEnabled
        public String bmLocId;
    }
    @AuraEnabled
    public String agncyIndvId;
    @AuraEnabled
    public List<BMLoc> BMLoc;
    @AuraEnabled
    public List<RoleCdArr> roleCdArr;
    @AuraEnabled
    public List<RuArr> ruArr;
    @AuraEnabled
    public List<SpclCdArr> spclCdArr;
    @AuraEnabled
    public String fullNm;
    @AuraEnabled
    public String fstNm;
    @AuraEnabled
    public String lstNm;
    @AuraEnabled
    public String middleNm;
    @AuraEnabled
    public String prefixNm;
    @AuraEnabled
    public String suffixNm;
    /*@AuraEnabled
    public String busEmailAddr;*/
    @AuraEnabled
    public String busTelNbr;
    @AuraEnabled
    public String licInd;
    @AuraEnabled
    public String birthDt;
    @AuraEnabled
    public String taxIdNbr;
    @AuraEnabled
    public String persEstbDt;
    @AuraEnabled
    public String natlPrdrNbr;
    @AuraEnabled
    public String agcyIndvSts;
    @AuraEnabled
    public String srcId;
    @AuraEnabled
    public String srcSysCd;
    @AuraEnabled
    public String updatedBy;
    
    public class RuArr {
        @AuraEnabled
        public String ruCd;
    }
    
    public class SpclCdArr {
        public String spclCd;
    }

    
    public class RoleCdArr {
        @AuraEnabled
        public String roleCd;
    }
       @AuraEnabled
       public static MDMUpdateIndividualWrapper parse(String json) {
        return (MDMUpdateIndividualWrapper) System.JSON.deserialize(json, MDMUpdateIndividualWrapper.class);
    }
    
    public MDMUpdateIndividualWrapper(String strObjId, Contact contactRec, Map<ID, String> mapAccountExternalID){
        BMLoc bmLocRec = new BMLoc();
        String strAccountExternalID = '';
        if(!Test.isRunningTest()){
            strAccountExternalID = mapAccountExternalID.get(contactRec.AccountID);
            bmLocRec.bmLocId = (strAccountExternalID != null ? ((strAccountExternalID).startsWith('BM_') ? (strAccountExternalID).removeStart('BM_') : strAccountExternalID) : '');
        }else{
            bmLocRec.bmLocId = '1059253';
        }
        this.BMLoc = new List<BMLoc>();
        this.BMLoc.add(bmLocRec);
        
        roleCdArr roleCDRec = new roleCdArr();
        roleCDRec.roleCd = 'CSR';
        this.roleCdArr = new List<roleCdArr>();
        this.roleCdArr.add(roleCDRec);
        
        //Sending blank values
        ruArr ruObj = new ruArr();
        ruObj.ruCd = '';
        this.RuArr = new List<RuArr>();
        this.RuArr.add(ruObj);
        
        spclCdArr spObj = new spclCdArr();
        spObj.spclCd = '';
        this.SpclCdArr = new List<SpclCdArr>();
        this.SpclCdArr.add(spObj);
        
        this.agncyIndvId = contactRec.TRAV_External_Id__c;
        this.fullNm = contactRec.LastName+', '+contactRec.FirstName;
		this.fstNm = contactRec.FirstName;
        this.lstNm = contactRec.LastName;
        this.middleNm = contactRec.MiddleName;
        this.licInd = contactRec.TRAV_License_Code__c;
		this.birthDt =  contactRec.Birthdate != null ? String.valueOf(contactRec.Birthdate).remove('-'): '';
		this.srcSysCd = 'SFSC';
		this.srcId = contactRec.TRAV_GU_Id__c;
        this.updatedBy = userinfo.getFirstName();
	 }   
    
}