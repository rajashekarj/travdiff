/**
* Utility Class for OpportunityTeamMember Trigger Handler. This class will have logic for all the implementations.
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Shashank Agarwal		 US63739             07/30/2019       Added method getEventRelationForStewardshipEvent
*/
 
public class EventRelationSelector {
/**
     * This is a method is used to get list Event Relation
     * @param Set<Id> -Set Event Ids
     * @return List<EventRelation> List of EventRelation based on the SOQL query provided as an input parameter.
     */
    
    public static List<EventRelation> getEventRelationForStewardshipEvent(Set<Id> setEventId){
    
        List<EventRelation> lstEventRelation = new List<EventRelation>();
        
        try{
            lstEventRelation = [
                                SELECT 
                                    EventId,
                					RelationId
                                FROM 
                                    EventRelation
                                WHERE 
                                   EventId IN :setEventId
                        ];
			If(Test.isRunningTest())
            {
               throw new QueryException('For testing purpose');
            }
        } //End - try
        catch(QueryException exp){
            //exception handling code
            
        } //End - catch

      return lstEventRelation;
     
    } //End of the Method
}