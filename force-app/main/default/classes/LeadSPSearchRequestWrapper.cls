/**
* This class is the wrapper class for Lead Search and Pull Functionality
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect          		Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty        US73994/US73995/US73996          24/01/2020      Original Version
* -----------------------------------------------------------------------------------------------------------
*/
public class LeadSPSearchRequestWrapper {
	@auraEnabled
    public String companyName{get;set;}
    @auraEnabled
    public String state{get;set;}
    @auraEnabled
    public String street{get;set;}
	@auraEnabled
    public String city{get;set;}    
}