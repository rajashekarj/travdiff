/**
* Utility Class for Opportunity Record Clone. This class will have logic for all the implementations.
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer		      User Story/Defect	  Date		    Description
* Shashank Agarwal    US77847			  03/02/2020    Original Version
* -----------------------------------------------------------------------------------------------------------
*
*/

public class CloneBSIClosedOpportunityController {
    /**
* This method will clone the Opportunity.
* @param objOpportunityId - Id of opportunity to be cloned.
* @return Id - returns new cloned opportunity Id .
*/
    @AuraEnabled
    public static Id getCloneBSIOpportunity(Id objOpportunityId)
    {
        try{// Record being cloned
            String strOpportunityLineItemAllFields = UtilityClass.getAllFieldsSOQL('OpportunityLineItem');
            String strOpportunityTeamMemberAllFields = UtilityClass.getAllFieldsSOQL('OpportunityTeamMember');
            String strOpportunityCarrierAllFields = UtilityClass.getAllFieldsSOQL('TRAV_Opportunity_Carrier__c');
            String strOpportunityContactRoleAllFields = UtilityClass.getAllFieldsSOQL('OpportunityContactRole');
            String strOpportunityContentDocumentLinkAllFields = UtilityClass.getAllFieldsSOQL('ContentDocumentLink');
            String strOpportunityQuery = 'Select '+ Constants.strOpportunityFieldsToBeCloned + ' FROM Opportunity Where Id = : objOpportunityId';
            Opportunity objOpportunity= Database.query(strOpportunityQuery);
            Id idNewBusinessRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('New Business').getRecordTypeId();
            // Custom clone logic
            Opportunity objClonedOpportunity = new Opportunity();
            objClonedOpportunity.put('Name',objOpportunity.Name);
            objClonedOpportunity.put('RecordTypeId',idNewBusinessRecordTypeId);
            objClonedOpportunity.put('StageName',Constants.Prospect);
            objClonedOpportunity.put('TRAV_BU_Account_Name__c',objOpportunity.TRAV_BU_Account_Name__c);
            objClonedOpportunity.put('TRAV_BU_Account_Number__c',objOpportunity.TRAV_BU_Account_Number__c);
            objClonedOpportunity.put('AccountId',objOpportunity.AccountId);
            objClonedOpportunity.put('TRAV_DBA__c',objOpportunity.TRAV_DBA__c);
            objClonedOpportunity.put('TRAV_SIC_Code_SIC_Description__c',objOpportunity.TRAV_SIC_Code_SIC_Description__c);
            objClonedOpportunity.put('TRAV_File_Management_Description__c',objOpportunity.TRAV_File_Management_Description__c);
            objClonedOpportunity.put('OwnerId',objOpportunity.OwnerId);
            objClonedOpportunity.put('Type','New Business');
            objClonedOpportunity.put('TRAV_Business_Unit__c',objOpportunity.TRAV_Business_Unit__c);
            objClonedOpportunity.put('TRAV_Marketing_Conditions__c',objOpportunity.TRAV_Marketing_Conditions__c);
            objClonedOpportunity.put('TRAV_Agency_Broker__c',objOpportunity.TRAV_Agency_Broker__c);
            objClonedOpportunity.put('TRAV_NA_Producer_Code__c',objOpportunity.TRAV_NA_Producer_Code__c);
            objClonedOpportunity.put('TRAV_BAM_ID__c',objOpportunity.TRAV_BAM_ID__c);
            objClonedOpportunity.put('TRAV_Total_Employee_Count__c',objOpportunity.TRAV_Total_Employee_Count__c);
            objClonedOpportunity.put('TRAV_Total_Assets__c',objOpportunity.TRAV_Total_Assets__c);
            objClonedOpportunity.put('TRAV_Total_Revenue_Amount__c',objOpportunity.TRAV_Total_Revenue_Amount__c);
            objClonedOpportunity.put('TRAV_Rating_State_Code__c',objOpportunity.TRAV_Rating_State_Code__c);
            objClonedOpportunity.put('CloseDate',objOpportunity.CloseDate);
            objClonedOpportunity.put('TRAV_Highest_Coverage_Limit__c',objOpportunity.TRAV_Highest_Coverage_Limit__c);
            objClonedOpportunity.put('TRAV_Distribution_Channel_Description__c',objOpportunity.TRAV_Distribution_Channel_Description__c);
            objClonedOpportunity.put('TRAV_Customer_Segment_Description__c',objOpportunity.TRAV_Customer_Segment_Description__c);
            objClonedOpportunity.put('TRAV_Customer_Sub_Segment_Description__c',objOpportunity.TRAV_Customer_Sub_Segment_Description__c);
            objClonedOpportunity.put('TRAV_SAI__c',objOpportunity.TRAV_SAI__c);
            objClonedOpportunity.put('TRAV_Panel_Name__c',objOpportunity.TRAV_Panel_Name__c);
            objClonedOpportunity.put('TRAV_Product_Enterprise_Initiative__c',objOpportunity.TRAV_Product_Enterprise_Initiative__c);
            objClonedOpportunity.put('TRAV_Program_Business__c',objOpportunity.TRAV_Program_Business__c);
            objClonedOpportunity.put('TRAV_National_Programs__c',objOpportunity.TRAV_National_Programs__c);
            objClonedOpportunity.put('TRAV_Prospect_Source__c',objOpportunity.TRAV_Prospect_Source__c);
            objClonedOpportunity.put('TRAV_Market_Cap__c',objOpportunity.TRAV_Market_Cap__c);
            objClonedOpportunity.put('TRAV_Security_action_lawsuits_activity__c',objOpportunity.TRAV_Security_action_lawsuits_activity__c);
            objClonedOpportunity.put('TRAV_State_of_Incorporation__c',objOpportunity.TRAV_State_of_Incorporation__c);
            objClonedOpportunity.put('TRAV_State_of_Headquarters__c',objOpportunity.TRAV_State_of_Headquarters__c);
            objClonedOpportunity.put('TRAV_Link_to_google_or_yahoo_financials__c',objOpportunity.TRAV_Link_to_google_or_yahoo_financials__c);
            objClonedOpportunity.put('TRAV_Total_Plan_Assets__c',objOpportunity.TRAV_Total_Plan_Assets__c);
            objClonedOpportunity.put('TRAV_Date_of_most_recent_equity_offering__c',objOpportunity.TRAV_Date_of_most_recent_equity_offering__c);
            objClonedOpportunity.put('TRAV_COE_Need_By_Date__c',objOpportunity.TRAV_COE_Need_By_Date__c);
            objClonedOpportunity.put('TRAV_COE_Type__c',objOpportunity.TRAV_COE_Type__c);
            objClonedOpportunity.put('TRAV_Top_3_AOPs__c',objOpportunity.TRAV_Top_3_AOPs__c);
            objClonedOpportunity.put('TRAV_Total_Revenue_Amount__c',objOpportunity.TRAV_Total_Revenue_Amount__c);
            objClonedOpportunity.put('TRAV_Total_Employee_Count__c',objOpportunity.TRAV_Total_Employee_Count__c);
            objClonedOpportunity.put('TRAV_Number_Of_Attorneys__c',objOpportunity.TRAV_Number_Of_Attorneys__c);
            System.debug(objClonedOpportunity);

            if(objClonedOpportunity != Null) {
                insert objClonedOpportunity;
            }

            //Cloning Team Members
            String strOpportunityTeamMemberQuery = 'Select '+ strOpportunityTeamMemberAllFields + ' FROM OpportunityTeamMember Where OpportunityId = : objOpportunityId AND TRAV_Active__c = true';
            List<OpportunityTeamMember>  lstTeamMember = Database.query(strOpportunityTeamMemberQuery);
            if(lstTeamMember.size() >0){
                List<OpportunityTeamMember> lstClonedTeamMember = new List<OpportunityTeamMember>();
                for(OpportunityTeamMember objTeamMember: lstTeamMember){
                    OpportunityTeamMember objClonedTeamMember = objTeamMember.clone(false,false,false,false);
                    objClonedTeamMember.OpportunityId = objClonedOpportunity.Id;
                    lstClonedTeamMember.add(objClonedTeamMember);
                }
                if(lstClonedTeamMember.size() > 0){
                    Database.SaveResult[] lstSaveResult = Database.insert(lstClonedTeamMember, false);
                    ExceptionUtility.logDMLError('CloneBSIClosedOpportunityController', 'cloneBSIOpportunity', lstSaveResult);
                }
            }

            //Cloning Line Items
            String strOpportunityLineItemQuery = 'Select '+ strOpportunityLineItemAllFields + ' FROM OpportunityLineItem Where OpportunityId = : objOpportunityId';
            List<OpportunityLineItem>  lstLineItem = Database.query(strOpportunityLineItemQuery);
            if(lstLineItem.size() >0){
                List<OpportunityLineItem> lstClonedLineItem = new List<OpportunityLineItem>();
                for(OpportunityLineItem objLineItem: lstLineItem){
                    OpportunityLineItem objClonedLineItem = objLineItem.clone(false,false,false,false);
                    objClonedLineItem.OpportunityId = objClonedOpportunity.Id;
                    objClonedLineItem.UnitPrice = Null;
                    objClonedLineItem.TotalPrice = Null;
                    objClonedLineItem.TRAV_External_Id__c = Null;
                    lstClonedLineItem.add(objClonedLineItem);
                }
                if(lstClonedLineItem.size() > 0){
                    Database.SaveResult[] lstSaveResult = Database.insert(lstClonedLineItem, false);
                    ExceptionUtility.logDMLError('CloneBSIClosedOpportunityController', 'cloneBSIOpportunity', lstSaveResult);
                }
            }

            // Clone Opportunity Carrier
            String strOpportunityCarrierQuery = 'Select '+ strOpportunityCarrierAllFields + ' FROM TRAV_Opportunity_Carrier__c Where TRAV_Opportunity__c = : objOpportunityId';
            List<TRAV_Opportunity_Carrier__c>  lstCarrier = Database.query(strOpportunityCarrierQuery);
            if(lstCarrier.size() >0){
                List<TRAV_Opportunity_Carrier__c> lstClonedCarrier = new List<TRAV_Opportunity_Carrier__c>();
                for(TRAV_Opportunity_Carrier__c objCarrier: lstCarrier){
                    TRAV_Opportunity_Carrier__c objClonedCarrier = objCarrier.clone(false,false,false,false);
                    objClonedCarrier.TRAV_Opportunity__c = objClonedOpportunity.Id;
                    lstClonedCarrier.add(objClonedCarrier);
                }
                if(lstClonedCarrier.size() > 0){
                    Database.SaveResult[] lstSaveResult = Database.insert(lstClonedCarrier, false);
                    ExceptionUtility.logDMLError('CloneBSIClosedOpportunityController', 'cloneBSIOpportunity', lstSaveResult);
                }
            }

            // Clone Opportunity Contact Role
            String strOpportunityContactRoleQuery = 'Select '+ strOpportunityContactRoleAllFields + ' FROM OpportunityContactRole Where OpportunityId = : objOpportunityId';
            List<OpportunityContactRole>  lstContactRole = Database.query(strOpportunityContactRoleQuery);
            if(lstContactRole.size() >0){
                List<OpportunityContactRole> lstClonedCContactRole = new List<OpportunityContactRole>();
                for(OpportunityContactRole objContactRole: lstContactRole){
                    OpportunityContactRole objClonedContactRole = objContactRole.clone(false,false,false,false);
                    objClonedContactRole.OpportunityId = objClonedOpportunity.Id;
                    lstClonedCContactRole.add(objClonedContactRole);
                }
                if(lstClonedCContactRole.size() > 0){
                    Database.SaveResult[] lstSaveResult = Database.insert(lstClonedCContactRole, false);
                    ExceptionUtility.logDMLError('CloneBSIClosedOpportunityController', 'cloneBSIOpportunity', lstSaveResult);
                }
            }

            // Clone Files
            String strContentDocumentLinkQuery = 'Select '+ strOpportunityContentDocumentLinkAllFields + ' FROM ContentDocumentLink Where LinkedEntityId = :objOpportunityId';
            List<ContentDocumentLink> lstFileAttachment = Database.query(strContentDocumentLinkQuery);
            if(lstFileAttachment.size() >0){
                List<ContentDocumentLink> lstClonedFileAttachment = new List<ContentDocumentLink>();
                for(ContentDocumentLink objFileAttachment: lstFileAttachment){
                    ContentDocumentLink objClonedFileAttchment = objFileAttachment.clone(false,false,false,false);
                    objClonedFileAttchment.LinkedEntityId = objClonedOpportunity.Id;
                    lstClonedFileAttachment.add(objClonedFileAttchment);
                }
                if(lstClonedFileAttachment.size() > 0){
                    Database.SaveResult[] lstSaveResult = Database.insert(lstClonedFileAttachment, false);
                    ExceptionUtility.logDMLError('CloneBSIClosedOpportunityController', 'cloneBSIOpportunity', lstSaveResult);
                }
            }
            // Redirect to the new cloned record
            return objClonedOpportunity.Id;

        }catch(Exception objExp){
            ExceptionUtility.logApexException('CloneBSIClosedOpportunityController', 'cloneBSIOpportunity', objExp, null);
            return objOpportunityId;
        }
    }
}