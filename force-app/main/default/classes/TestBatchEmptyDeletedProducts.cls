/**
* Test class for BatchEmptyDeletedProducts Class
*
* @Author : Deloitte Consulting
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Shashank Agarwal       US59991              06/27/2019       Original Version
* -----------------------------------------------------------------------------------------------------------
*/
@isTest
private class TestBatchEmptyDeletedProducts {
    
    static testmethod void testDeleteProductCase(){
        
        //create the custom setting for Opportunity trigger
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'OpportunityLineItemTrigger';
        insert objCustomsetting;
        
        //create the custom setting for general data setting
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Deferred;Written;Declined;Quote Unsuccessful';
        insert objGeneralDataSetting;
        
        //create a pricebook
        Pricebook2 biPricebook = TestDataFactory.createTestPricebook('BI - National Accounts');
        List<Pricebook2> lstPricebook = new List<Pricebook2>{biPricebook};
        insert lstPricebook;

        //create account record
        Account objProspectAccount = TestDataFactory.createProspect(); 
        List<Account> lstAccount = new List<Account>{objProspectAccount};
        insert lstAccount;
        
        //Create List of opportunities
        List<Opportunity> lstOppty = TestDataFactory.createOpportunities(1,objProspectAccount.Id,'BI-NA');
        lstOppty[0].Pricebook2ID = lstPricebook[0].Id;
        insert lstOppty;
        
        
        
        //Get teh standard pricebook Id to create standard pricebookentry for the lsit of products
        Id pricebookId = Test.getStandardPricebookId();

        //create 5 products
        List<Product2> lstProducts = TestDataFactory.createProducts(5);
        insert lstProducts;

        //Standard PricebookEntry
        List<PricebookEntry> pricebookEntryList = TestDataFactory.createPricebookEntries(lstProducts, pricebookId);
        insert pricebookEntryList;
        
        List<PricebookEntry> lstBIPricebookEntries = TestDataFactory.createPricebookEntries(lstProducts, lstPricebook[0].Id);
        insert lstBIPricebookEntries;
        
        TRAV_Producer__c objProducer = TestDataFactory.createProducers();
        insert objProducer;
        
       TRAV_Agency_Delete_Tracking__c objAgDel = new TRAV_Agency_Delete_Tracking__c();
       objAgDel.TRAV_New_Agency_ID__c = lstAccount[0].Id;
       objAgDel.TRAV_Old_Agency_ID__c = lstAccount[0].Id;
       objAgDel.TRAV_Producer_Code__c = objProducer.Id;
        
       insert objAgDel;
        
       List<OpportunityLineItem> lstOpportunityLineItems = new List<OpportunityLineItem>();
        for(Opportunity opp : lstOppty) {
            for(Product2 product : lstProducts) {
                OpportunityLineItem lineItem = new OpportunityLineItem(
                    OpportunityId = opp.Id,
                    Product2Id = product.Id,
                    PricebookEntryId = lstBIPricebookEntries[0].Id
                );
                lstOpportunityLineItems.add(lineItem);
            }
        }
        
        insert lstOpportunityLineItems;
        List<OpportunityLineItem> lstProductToDelete = [Select Id From OpportunityLineItem];
        delete lstProductToDelete;
        
        List<TRAV_Opportunity_Product_Delete__c> lstProductToDelete2 = [Select Id From TRAV_Opportunity_Product_Delete__c];
        for(TRAV_Opportunity_Product_Delete__c objOpportunityProductDelete : lstProductToDelete2){
            objOpportunityProductDelete.TRAV_Deleted_On__c = DateTime.now().addDays(-30);
        }
        update lstProductToDelete2;
        System.debug(lstProductToDelete2);
        Test.startTest();
        BatchEmptyDeletedProducts batchObj  = new BatchEmptyDeletedProducts(); 
        Database.executebatch(batchObj);
		String sch = '0 0 23 * * ?';
        system.schedule('Test BatchEmptyDeleteProducts in SchedulableContext', sch, batchObj);
        Test.stopTest();
    }
}