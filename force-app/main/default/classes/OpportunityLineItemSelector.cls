/**
 * This class is used as a Selector Layer for Opportunity object. 
 * All queries for Opportunity object will be performed in this class.
 *
 *
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shashank Agarwal       US63781             07/30/2019        Original Version
 * Bhanu Reddy			  US66569             11/09/2019		Added new Method to fecth OpportunityLine Item	    	
 * Erec Lawrie            US77080             02/26/2020        Added TRAV_Product_Abbreviation__c to be retrieved  
 * Evan Wilcher			  US78123			  03/04/2020		Overrode getOpportunitiesForPotentialPremium and getOpportunityLineItems methods   
 * -----------------------------------------------------------------------------------------------------------
 */
public class OpportunityLineItemSelector {
    //Updated for US78123 by Evan Wilcher 03/04/2020
    //Added ExcludeOpportunityBy enum to be used to filter out CB_MDM Opportunities.
    //Can be used for future cases.
    public enum ExcludeOpportunityBy {NONE, CB_MDM}
    /**
* This method gets the OpportunityLineItem for a set of Opty Id
* @param Set<Opportunity> - Set of Opportunity records.
* @return Map<Id,AggregateResult> .
*/
    public static List<OpportunityLineItem> getOpportunitiesLineItemForTeamMemberUpdate(Set<Id> setOpportunityId,List<OpportunityLineItem> lstLineItemIds) {
      
       final string METHODNAME = 'getOpportunitiesLineItemForTeamMemberUpdate';
       final string CLASSNAME = 'OpportunitiesLineItemSelector';
       List<OpportunityLineItem> lstOpportunityLineItem = new List<OpportunityLineItem>();
        
        try{
            lstOpportunityLineItem = [
                                SELECT 
                                    Id,
                                    OpportunityId,
                                    TRAV_Coverage_Approver_Name__c,
                                    TRAV_URC_Indicator__c
                                FROM 
                                     OpportunityLineItem
                                WHERE 
                                    OpportunityId IN : setOpportunityId
                                AND
                                    TRAV_Coverage_Approver_Name__c != null
                                AND
                                      id not in:lstLineItemIds
                               
                        ];
        } //End - try
        catch(QueryException objExp){
            //exception handling code
             ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        } //End - catch
        
        return lstOpportunityLineItem;
        
    }
    
    /**
* This method gets the OpportunityLineItem for a set of Opty Id
* @param Set<Opportunity> - Set of Opportunity records.
* @return Map<Id,AggregateResult> .
* Updated for US78123 by Evan Wilcher 03/04/2020
* Now calls it's override method and passes ExcludeOpportunityBy.NONE
*/
	public static Map<Id,AggregateResult> getOpportunitiesForPotentialPremium(Set<Id> setOpportunityId) {
      return getOpportunitiesForPotentialPremium(setOpportunityID, ExcludeOpportunityBy.NONE);
    }  
    
        /**
* This method gets the OpportunityLineItem for a set of Opty Id
* Can set the filter to ExcludeOpportunityBy.NONE for no filter, 
* or ExcludeOpportunityBy.CB_MDM to filter out Certified Business Opportunities.
* @param Set<Opportunity> - Set of Opportunity records.
* @return Map<Id,AggregateResult> .
*/
    public static Map<Id,AggregateResult> getOpportunitiesForPotentialPremium(Set<Id> setOpportunityId, ExcludeOpportunityBy filterBy) {
       final string METHODNAME = 'getOpportunityLineItems';
       final string CLASSNAME = 'getOpportunitiesLineItemCountForError';
       Map<Id,AggregateResult> mapOpportunity = new Map<Id,AggregateResult>();
        
        try{
            switch on filterBy{
                when CB_MDM{
                    mapOpportunity = new Map<Id,AggregateResult>([
                                SELECT 
                                    OpportunityId Id,
                                    SUM(TRAV_Coverage_Premium__c)
                                FROM 
                                     OpportunityLineItem
                                WHERE 
                                    OpportunityId IN : setOpportunityId
                                AND
                                    Opportunity.TRAV_Business_Unit__c IN :Constants.setOpportunityBSIBU
                				AND
                					Opportunity.TRAV_Direct_Source_Sys_Code__c != 'CB MDM'
                                Group By 
                                    OpportunityId
                        ]);
                }
                when NONE{
                    mapOpportunity = new Map<Id,AggregateResult>([
                                SELECT 
                                    OpportunityId Id,
                                    SUM(TRAV_Coverage_Premium__c)
                                FROM 
                                     OpportunityLineItem
                                WHERE 
                                    OpportunityId IN : setOpportunityId
                                AND
                                    Opportunity.TRAV_Business_Unit__c IN :Constants.setOpportunityBSIBU
                                Group By 
                                    OpportunityId
                        ]);
                }
            }            
            
        } //End - try
        catch(QueryException objExp){
            //exception handling code
             ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        } //End - catch
        
        return mapOpportunity;
        
    }
    /**
* This method gets the OpportunityLineItem for a set of Opty Id
* @param Set<Opportunity> - Set of Opportunity records.
* @return List<OpportunityLineItem> .
* Updated for US78123 by Evan Wilcher 03/04/2020
* Now calls it's override method and passes ExcludeOpportunityBy.NONE
*/
     public static List<OpportunityLineItem> getOpportunityLineItems(Set<Id> setOpportunityId) {      
        return getOpportunityLineItems(setOpportunityID, ExcludeOpportunityBy.NONE);
    } //end of method

    /**
* This method gets the OpportunityLineItem for a set of Opty Id
* Can set the filter to ExcludeOpportunityBy.NONE for no filter, 
* or ExcludeOpportunityBy.CB_MDM to filter out Certified Business Opportunities.
* @param Set<Opportunity> - Set of Opportunity records.
* @return List<OpportunityLineItem> .
*/
     public static List<OpportunityLineItem> getOpportunityLineItems(Set<Id> setOpportunityId, ExcludeOpportunityBy filterBy) {
    final string METHODNAME = 'getOpportunityLineItems';
        final string CLASSNAME = 'OpportunityLineItemSelector'; 
         List<OpportunityLineItem> lstOpportunityLineItem = new List<OpportunityLineItem>();
         try{
             switch on filterBy{
                when CB_MDM{
                    lstOpportunityLineItem = [Select Id, 
                                                Name,
                                                OpportunityId, 
                                                Product2Id,//Added Product2Id field for US69846
                                                Product2.Name,
                                                Product2.TRAV_Product_Abbreviation__c, //added for US77080
                                                TRAV_Closed_Date__c,
                                                TRAV_Stage__c,
                                                TRAV_Closed_Reason__c,
                                                TRAV_Closed_Sub_Reason__c,
                                                TRAV_Product_Winning_New_Carrier__c,
                                                TRAV_Comments__c,
                                                TRAV_Program_Type__c,
                                                TRAV_Pricing_Type__c,
                                                TRAV_Maximum_Coverage_Limit_Amount__c,
                                                TRAV_Retention_Amount__c,
                                                TRAV_Coverage_Premium__c
                                                from OpportunityLineItem 
                                                Where OpportunityId IN: setOpportunityId
                                                And Opportunity.TRAV_Direct_Source_Sys_Code__c != 'CB MDM'];
                }
                when NONE{
                    lstOpportunityLineItem = [Select Id,
                                                Name,
                                                OpportunityId, 
                                                Product2Id,//Added Product2Id field for US69846
                                                Product2.Name,
                                                Product2.TRAV_Product_Abbreviation__c, //added for US77080
                                                TRAV_Closed_Date__c,
                                                TRAV_Stage__c,
                                                TRAV_Closed_Reason__c,
                                                TRAV_Closed_Sub_Reason__c,
                                                TRAV_Product_Winning_New_Carrier__c,
                                                TRAV_Comments__c,
                                                TRAV_Program_Type__c,
                                                TRAV_Pricing_Type__c,
                                                TRAV_Maximum_Coverage_Limit_Amount__c,
                                                TRAV_Retention_Amount__c,
                                                TRAV_Coverage_Premium__c
                                                from OpportunityLineItem 
                                                Where OpportunityId IN: setOpportunityId];
                }
             }
         }catch(QueryException objExp){
             //exception handling code
             ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
         }//End - catch        
        return lstOpportunityLineItem;
    } //end of method

    public static List<OpportunityLineItem> getOpportunityLineItemsForProductNameUpdate(Set<Id> setOpportunityId) {
        final string METHODNAME = 'getOpportunityLineItemsForProductNameUpdate';
        final string CLASSNAME = 'OpportunityLineItemSelector'; 
        
        List<OpportunityLineItem> lstOpportunityLineItem = new List<OpportunityLineItem>();

        try {
            lstOpportunityLineItem = [Select Id,
                                        Name,
                                        OpportunityId, 
                                        Product2Id,
                                        Product2.Name,
                                        Product2.TRAV_Product_Abbreviation__c, 
                                        Opportunity.TRAV_Direct_Source_Sys_Code__c
                                        from OpportunityLineItem 
                                        Where OpportunityId IN: setOpportunityId];
        } catch(QueryException objExp) {
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        }

        return lstOpportunityLineItem;
    }

    /**
* This is a method is used to get list opportunities from ID
* @param string Opportunities id set
* @return List<Opportunity> List of Opportunities based on the SOQL query provided as an input parameter.
*/
    public static List<OpportunityLineItem> getOppLineItem(Set<Id> setOppId) {
        final string METHODNAME = 'getOppLineItem';
        final string CLASSNAME = 'OpportunityLineItemSelector';
        String strAllFields = UtilityClass.getAllFieldsSOQL('OpportunityLineItem');
        String strQueryOpp = 'Select ' + strAllFields +
            ' ,Opportunity.TRAV_Renewal_Creation__c, Opportunity.TRAV_Deferred_From_Opportunity__c From OpportunityLineItem WHERE OpportunityId IN:setOppId' ; 
           // ' WHERE OpportunityId IN:setOppId AND TRAV_Program_Type__c ='+'\''+
          //  Constants.PricingType+'\' AND TRAV_Stage__c ='+ '\''+Constants.WRITTENSTAGE+'\'';
        List<OpportunityLineItem> listOpportunityLineItem = new List<OpportunityLineItem>();
        system.debug('strQueryOpp-->>'+strQueryOpp);
        system.debug('Opp Id'+setOppId);
        try {
            listOpportunityLineItem = Database.query(strQueryOpp);
        } 
        catch(QueryException objExp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
            
        } // End Catch
        return listOpportunityLineItem;
    } //End of Method
    
  /**
* This method get the OpportunityLineItem from Id 
* @param Id of the OpportuntiyLienItem
* @return OpportuntiyLienItem .
*/
    public static OpportunityLineItem getOpportunityLienItem( string IdOpportuntiyLineItem){
        return [select id,
                       Product2.name 
                    from 
                      OpportunityLineItem
                    where 
                       id = :IdOpportuntiyLineItem ];
    } //end of the method getOpportuntiyLineItem
} //end of the class