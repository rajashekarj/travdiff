/**
* This class is used as a Selector Layer for Account Producer Relationship object. 
* All queries for Account Producer Relationship object will be performed in this class.
*
*
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect      Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Bhanu Reddy                 US62952         06/28/2019        Original Version
* Shashank Agarwal            US69108         09/30/2019        Added method getAccountProducerRelationsForBAM
* -----------------------------------------------------------------------------------------------------------
*/

public with sharing class AccountProducerRelationshipSelector {
	
/**
* This is a method is used to get list Account Producer relationship records based on the CL Mastr Producer codes and account ids
* @param set<string> CL master Producer code
@param set<string> accountids
* @return List<TRAV_Account_Producer_Relation__c> List of producer codes
*/
 /*public static list<TRAV_Account_Producer_Relation__c> getAccountProducerRelationsbyExternalId(Set<string> setExternalIds){
 	return new List<TRAV_Account_Producer_Relation__c>(
        [select
 	 			id,
 	 			TRAV_Account__c, 
 	 			TRAV_Producer__r.TRAV_Producer_Code__c,
         		TRAV_External_ID__c
 	 		  from  TRAV_Account_Producer_Relation__c
 	 		  where
 	 		     TRAV_External_ID__c in :setExternalIds
 	 		  LIMIT :setExternalIds.size()
 	 	]);
 	
 } */  
    
    /*
     * Method to query account-producer relation records based on Salesforce Account Ids
     * @param setAccountIds - Set of Salesforce AccountIds on agency records
     * @return Map of external-id on the account-producer-relation record and the record itself
     */
    public static Map<String, TRAV_Account_Producer_Relation__c> getAccountProducerRelationsbyAccountId(Set<string> setAccountIds){
 		List<TRAV_Account_Producer_Relation__c> listAcctProdRel = new List<TRAV_Account_Producer_Relation__c>(
            [select
                    id,
                    TRAV_Account__c, 
                    TRAV_Producer__r.TRAV_Producer_Code__c,
                    TRAV_External_ID__c
                  from  TRAV_Account_Producer_Relation__c
                  where
                     TRAV_Account__c in :setAccountIds
            ]);
        
        Map<String, TRAV_Account_Producer_Relation__c> mapAcctProdRel = new Map<String, TRAV_Account_Producer_Relation__c>();
        for(TRAV_Account_Producer_Relation__c acctProdRel : listAcctProdRel) {
            mapAcctProdRel.put(acctProdRel.TRAV_External_ID__c, acctProdRel);
        }
        
        return mapAcctProdRel;
 	}

 /**
* This is a method is used to get list Account Producer relationship records based on Set of Produver Id's
* @param set<Id> Producer
* @return Set<TRAV_Account_Producer_Relation__c> set of producer codes
*/
 public static Set<TRAV_Account_Producer_Relation__c> getAccountProducerRelationsForBAM(Set<Id> setProducerId){
 	return new Set<TRAV_Account_Producer_Relation__c>(
        [
            SELECT
 	 			Id, 
 	 			TRAV_Producer__c
 	 		  FROM  
            	TRAV_Account_Producer_Relation__c
 	 		  WHERE
 	 		     TRAV_Producer__c in :setProducerId
 	 	]);
 	
 } 	
}