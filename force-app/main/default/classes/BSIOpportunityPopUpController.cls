/**
* This class is used THe Conroller for BSIOpportunityPopUp component.
* This allows the user to add the closed reason for each product associates with the Opportunity
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer             User Story/Defect         Date             Description
* -----------------------------------------------------------------------------------------------------------          
* Shashank Agarwal       US73049                   12/06/2019       Original version
* -----------------------------------------------------------------------------------------------------------              
*/
public without sharing class BSIOpportunityPopUpController {
    public class UnderReviewWrapper {
        @AuraEnabled
        public boolean loadPopup = false;
        @AuraEnabled
        public boolean isUnderReviewStage = false;
    } 
    
    /**
* This method is used to get calculate when the PopUp is required to fire.
* @param recordId The opportunity Id passed to the method to get a particular opportunity.
* @return UnderReviewWrapper is returned for the Id provided as an input parameter.
*/
    @AuraEnabled
    public static UnderReviewWrapper getLoadPopUpDetails(Id recordId){
        Opportunity objOpportunity = [Select Id,StageName,TRAV_Under_Review_Pop_Up__c,TRAV_Business_Unit__c from Opportunity Where Id =:recordId];
        UnderReviewWrapper objUnderReviewWrapper = new UnderReviewWrapper();
        if(objOpportunity.StageName == Constants.UNDERREVIEW && objOpportunity.TRAV_Under_Review_Pop_Up__c == Null && Constants.setOpportunityBSIBU.contains(objOpportunity.TRAV_Business_Unit__c)){
            objUnderReviewWrapper.loadPopup = true;
        }
        return objUnderReviewWrapper;
    }
    /**
* This method is used save the details that we are getting from PopUp.
* @param recordId The opportunity Id passed to the method to get a particular opportunity.
* @param strOutstandingItemIndicator Value of Outstanding Indicator set in the PopUp.
* @param strMaterialsRequested Value of Materials Requested set in the PopUp.
* @param strUnderReviewNotes Value of Under Review Notes set in the PopUp.
* @return void
*/
    @AuraEnabled
    public static void saveDetails(Id recordId,String strOutstandingItemIndicator,String strMaterialsRequested, String strUnderReviewNotes){
        Opportunity objOpportunity = new Opportunity();
        objOpportunity.Id = recordId;
        objOpportunity.TRAV_Outstanding_Items_Indicator__c = strOutstandingItemIndicator;
        objOpportunity.TRAV_Under_Review_Pop_Up__c = 'true';
        if(String.isNotBlank(strMaterialsRequested)){
            objOpportunity.TRAV_Requested__c = strMaterialsRequested;
        }else{
             objOpportunity.TRAV_Requested__c = Null;
        }
        if(String.isNotBlank(strUnderReviewNotes)){
            objOpportunity.TRAV_UnderReview_Notes__c = strUnderReviewNotes;
        }else{
            objOpportunity.TRAV_UnderReview_Notes__c = Null;
        }
        update objOpportunity;
    }
    
    /**
* This method will revert the stage to original stage when user clicks on cancel.
* @param recordId The opportunity Id passed to the method to get a particular opportunity.
* @return void.
*/
    @AuraEnabled
    public static void revertStageUpdate(Id recordId){
        OpportunityFieldHistory objOpportunityHistory =[SELECT Id, Field, OldValue, NewValue FROM OpportunityFieldHistory where Field = 'StageName' and OpportunityId = :recordId order by createddate desc][0];
        system.debug('objOpportunityHistory>>'+objOpportunityHistory);
        if(objOpportunityHistory.OldValue == Constants.SUBMISSION || objOpportunityHistory.OldValue == Constants.PROSPECT){
            Opportunity objOpportunity = new Opportunity();
            objOpportunity.Id = recordId;
            objOpportunity.StageName = String.valueOf(objOpportunityHistory.OldValue);
            update objOpportunity;
        }
    }
}
