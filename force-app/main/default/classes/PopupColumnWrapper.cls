/**
* @author Sayanka Mohanty
* @date   12/18/2019
* @description  
      
Modification Log:
----------------------------------------------------------------------------------------
Developer                   Date                Description
----------------------------------------------------------------------------------------
Sayanka Mohanty           12/18/2019          Original Version
*/

public class PopupColumnWrapper {
    @AuraEnabled
    public String apiName;//
    @AuraEnabled
    public String label;//
    @AuraEnabled
    public String fieldType;//
    @AuraEnabled
    public String sObjectName;//
    @AuraEnabled
    public List<String> values;//
    @AuraEnabled
    public Integer sequence;
    @AuraEnabled
    public String section;
    @AuraEnabled
    public Boolean required;//
    @AuraEnabled
    public String helptext;
    @AuraEnabled
    public String styleClass;
    @AuraEnabled
    public Boolean isReadOnly;
    @AuraEnabled
    public String defaultValue;
    @AuraEnabled
    public String opportunityValue;
    @AuraEnabled
    public String validationErrorMessage;
    @AuraEnabled
    public String sameStageError;
    
    public Integer compareTo(Object compareTo) {
        PopupColumnWrapper compareToTab = (PopupColumnWrapper)compareTo;
        if (sequence== compareToTab.sequence){
            return 0;
        }
        if (sequence> compareToTab.sequence){
            return 1;
        }
        return -1;        
    }   
}