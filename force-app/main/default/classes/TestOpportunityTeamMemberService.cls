/**
 * Test class for OpportunityTeamMemberService Class
 *
 * @Author : Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Isha Shukla            US60806              06/03/2019       Original Version
 * Isha Shukla            US62980              06/24/2019       added testTeamUpdationCase method
 * Shashank Agarwal       US63739              08/02/2019       Added testModifyEventAttendees
 * Derek Manierre     US68470          03/03/2020   Added testpreventBSIDeletion and testpreventBSIUpdate
 * -----------------------------------------------------------------------------------------------------------
 */   
@isTest
private class TestOpportunityTeamMemberService {
    /**
    * This method will test scenarios when opportunity team member is inserted
    * @param no parameters
    * @return void
    */
    static testmethod void testTeamInsertionCase() {
        //create the custom setting for Opportunity trigger
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'OpportunityTeamMemberTrigger';
        insert objCustomsetting;
        
        //create the custom setting for general data setting
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Closed Won,Closed Lost,Closed Deferred';
        insert objGeneralDataSetting;
        
        //creating user records 
        User objUsr = TestDataFactory.createTestUser('BI - National Accounts','TestNA2','LastNameNA2');
        User objUsr2 = TestDataFactory.createTestUser('BI - National Accounts','TestNA','LastNameNA');
        List<User> usrList = new List<User>{objUsr,objUsr2};
        insert usrList;
        
        //create account record
        Account prospectAccount = TestDataFactory.createProspect(); 
        Account agency = TestDataFactory.createAgency(); 
        List<Account> lstAccount = new List<Account>{prospectAccount,agency};
        insert lstAccount;
        
        //Creating opportunity records
        List<Opportunity> lstOpportunity = TestDataFactory.createOpportunities(10, prospectAccount.Id,'BI-NA');
        lstOpportunity[0].TRAV_Agency_Broker__c = agency.Id;
        insert lstOpportunity;
        
        //creating opportunity team member records
        List<OpportunityTeamMember> lstOpportunityTeam = TestDataFactory.createOpportunityTeamMembers(2,objUsr2.Id,lstOpportunity[6].Id);
        lstOpportunityTeam[1].OpportunityId = lstOpportunity[4].Id;
        lstOpportunityTeam[1].UserId = objUsr.Id;
        Test.startTest();
        insert lstOpportunityTeam;
        Test.stopTest();
        System.assertEquals([SELECT 
                                Id 
                             FROM 
                                AccountTeamMember 
                             WHERE 
                                AccountId = :prospectAccount.Id].size() > 0 ,true);

	}
    
    /**
    * This method will test scenarios when opportunity team member is deleted
    * @param no parameters
    * @return void
    */
    static testmethod void testTeamDeletionCase() {
        //create the custom setting for OpportunityTeam trigger
        TRAV_Trigger_Setting__c customsetting = TestDataFactory.createTriggersetting();
        customsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        customsetting.TRAV_Trigger_Name__c = 'OpportunityTeamMemberTrigger';
        insert customsetting;
        
        //create the custom setting for general data setting
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Closed Won,Closed Lost,Closed Deferred';
        insert objGeneralDataSetting;
        
        User objUsr = TestDataFactory.createTestUser('BI - National Accounts','TestNA2','LastNameNA2');
        User objUsr3 = TestDataFactory.createTestUser('BI - National Accounts','TestNA3','LastNameNA3');
        User objUsr2 = TestDataFactory.createTestUser('BI - National Accounts','TestNA','LastNameNA');
        List<User> usrList = new List<User>{objUsr,objUsr2,objUsr3};
        insert usrList;
        
        //create agency account record
        Account prospectAccount = TestDataFactory.createProspect(); 
        List<Account> lstAccount = new List<Account>{prospectAccount};
        insert lstAccount;
        
        Account agencyAccount = TestDataFactory.createAgency();
        insert agencyAccount;
      
        List<Opportunity> lstOpportunity = TestDataFactory.createOpportunities(10, prospectAccount.Id, 'BI-NA');
        lstOpportunity[4].TRAV_Agency_Broker__c = agencyAccount.id;
        lstOpportunity[6].TRAV_Agency_Broker__c = agencyAccount.id;
        insert lstOpportunity;
        
        //creating opportunity team member records
        List<OpportunityTeamMember> lstOpportunityTeam = TestDataFactory.createOpportunityTeamMembers(3,objUsr2.Id,lstOpportunity[6].Id);
        lstOpportunityTeam[1].OpportunityId = lstOpportunity[4].Id;
        lstOpportunityTeam[1].UserId = objUsr.Id;
        lstOpportunityTeam[0].UserId = objUsr3.Id;
        insert lstOpportunityTeam;
        Test.startTest();
        delete lstOpportunityTeam; 
        Test.stopTest();
        System.assertEquals([SELECT 
                                  Id 
                             FROM 
                                  AccountTeamMember 
                             WHERE 
                                  AccountId = :prospectAccount.Id 
                             AND 
                                  UserId = :objUsr.Id].size() == 0 ,true);
        
    }
    /**
    * This method will test scenarios when opportunity team member is updated
    * @param no parameters
    * @return void
    */
    static testmethod void testTeamUpdationCase() {
        //create the custom setting for OpportunityTeam trigger
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'OpportunityTeamMemberTrigger';
        insert objCustomsetting;
        
        //create the custom setting for general data setting
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Closed Won,Closed Lost,Closed Deferred';
        insert objGeneralDataSetting;
        
        User objUsr = TestDataFactory.createTestUser('BI - National Accounts','TestNA2','LastNameNA2');
        User objUsr3 = TestDataFactory.createTestUser('BI - National Accounts','TestNA3','LastNameNA3');
        User objUsr2 = TestDataFactory.createTestUser('BI - National Accounts','TestNA','LastNameNA');
        List<User> usrList = new List<User>{objUsr,objUsr2,objUsr3};
        insert usrList;
        
        //create agency account record
        Account prospectAccount = TestDataFactory.createProspect(); 
        List<Account> lstAccount = new List<Account>{prospectAccount};
        insert lstAccount;
        
        List<Opportunity> lstOpportunity = TestDataFactory.createOpportunities(10, prospectAccount.Id,'BI-NA');
        insert lstOpportunity;
        
        //creating opportunity team member records
        List<OpportunityTeamMember> lstOpportunityTeam = TestDataFactory.createOpportunityTeamMembers(3,objUsr2.Id,lstOpportunity[6].Id);
        lstOpportunityTeam[1].OpportunityId = lstOpportunity[4].Id;
        lstOpportunityTeam[1].UserId = objUsr.Id;
        lstOpportunityTeam[0].UserId = objUsr3.Id;
        insert lstOpportunityTeam;
        lstOpportunityTeam[0].TRAV_Active__c = false;
        Test.startTest();
        update lstOpportunityTeam; 
        Test.stopTest();
        System.assertEquals([SELECT 
                                  Id 
                             FROM 
                                  AccountTeamMember 
                             WHERE 
                                  AccountId = :prospectAccount.Id 
                             AND 
                                  UserId = :objUsr3.Id].size() == 0 ,true);
        
    }
    /**
    * This method will test scenarios when opportunity team member is Inserted or updated for stewradship Events.
    * @param no parameters
    * @return void
    */
    static testmethod void testModifyEventAttendees() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
        //create the custom setting for OpportunityTeam trigger
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'OpportunityTeamMemberTrigger';
        insert objCustomsetting;
        
        //create the custom setting for general data setting
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Closed Won,Closed Lost,Closed Deferred';
        insert objGeneralDataSetting;
        List<User> lstUser = new List<User>();
        User objUser1 = TestDataFactory.createTestUser('BI - National Accounts','testUser1','testLastName1');
        User objUser2 = TestDataFactory.createTestUser('BI - National Accounts','testUser2','testLastName2');
        User objUser3 = TestDataFactory.createTestUser('BI - National Accounts','testUser3','testLastName3');
        User objUser4 = TestDataFactory.createTestUser('BI - National Accounts','testUser4','testLastName4');
        User objUser5 = TestDataFactory.createTestUser('BI - National Accounts','testUser5','testLastName5');
        lstUser.add(objUser1);
        lstUser.add(objUser2);
        lstUser.add(objUser3);
        lstUser.add(objUser4);
        lstUser.add(objUser5);
        if(!lstUser.isEmpty()){
            insert lstUser;
        }
        
        //create agency account record
        Account objAccount = TestDataFactory.createProspect();
        insert objAccount;
 
        Opportunity objOpportunity = TestDataFactory.createOppty('Written', 'Test oppty A',objAccount.Id);
        objOpportunity.TRAV_Popup_Stage__c = 'Written';
        objOpportunity.OwnerId = objUser1.id;
        objOpportunity.TRAV_Policy_Effective_Date__c  = System.today().addDays(-30);
        objOpportunity.TRAV_Policy_Expiration_Date__c = System.today().addDays(365);
         Date renewalDate = System.today().addDays(100);
        objOpportunity.TRAV_Renewal_Creation__c = renewalDate;
        insert objOpportunity;
 
       
        System.debug('Opportunity '+objOpportunity);
        List<OpportunityTeamMember> lstOpportunityTeamFinal = new List<OpportunityTeamMember>();
        OpportunityTeamMember objOpportunityTeamMember1 = TestDataFactory.createOpptyTeammember(objUser1.Id, objOpportunity.id, 'RMIS Information Account Executive');
        OpportunityTeamMember objOpportunityTeamMember2 = TestDataFactory.createOpptyTeammember(objUser2.Id, objOpportunity.id, 'Account Manager');
        OpportunityTeamMember objOpportunityTeamMember3 = TestDataFactory.createOpptyTeammember(objUser3.Id, objOpportunity.id, 'Account Executive');
        OpportunityTeamMember objOpportunityTeamMember4 = TestDataFactory.createOpptyTeammember(objUser4.Id, objOpportunity.id, 'Claim Account Executive');
        lstOpportunityTeamFinal.add(objOpportunityTeamMember1);
        lstOpportunityTeamFinal.add(objOpportunityTeamMember2);
        lstOpportunityTeamFinal.add(objOpportunityTeamMember3);
        lstOpportunityTeamFinal.add(objOpportunityTeamMember4);
        Test.startTest();
            if(!lstOpportunityTeamFinal.isEmpty()){
            insert lstOpportunityTeamFinal;
        }   
        
        
        List<Event> lstEvent = TestDataFactory.createEvent(1, objUser1.Id,  objOpportunity.id); 
        insert lstEvent;
        
        
        List<OpportunityTeamMember> lstTeamMembertoUpsert = new List<OpportunityTeamMember>();
        List<OpportunityTeamMember> lstTeamMembertoDelete= new List<OpportunityTeamMember>();
        lstTeamMembertoDelete.add(objOpportunityTeamMember4);
        objOpportunityTeamMember2.TeamMemberRole = 'Managing Director';
        objOpportunityTeamMember3.TeamMemberRole = 'Claim Account Executive';
        OpportunityTeamMember objOpportunityTeamMember5 = TestDataFactory.createOpptyTeammember(objUser5.Id, objOpportunity.id, 'Risk Control');
        lstTeamMembertoUpsert.add(objOpportunityTeamMember2);
        lstTeamMembertoUpsert.add(objOpportunityTeamMember3);
        lstTeamMembertoUpsert.add(objOpportunityTeamMember5);
        
        
        upsert lstTeamMembertoUpsert;
		delete lstTeamMembertoDelete;
        OpportunityTeamMemberTriggerHandler oppTM = new OpportunityTeamMemberTriggerHandler();
        oppTM.afterUndelete(null);
        Test.stopTest();
        
        System.assertEquals([SELECT 
                                  RelationId 
                             FROM 
                                  EventRelation 
                             WHERE 
                                  RelationId = :objUser3.Id 
                             ].size() != 0 ,false);
       
    }
    }
    
    //TO cover OpportunityTeamMemberSelector class methods
    private static testmethod void coverOpportunityTeamMemberSelector(){
        Set<ID> ids = new Set<ID>();
        OpportunityTeamMemberSelector.getOpportunityTeamManagers(ids,ids);
        OpportunityTeamMemberSelector.getManagersList(ids,ids);
    }
    
     /**
    * This method will test scenarios when an Approver/URC Approver/Underwriter is deleted on a BSI opportunity.
    * @param no parameters
    * @return void
    */
    static testmethod void testpreventBSIDeletion() {

        //create the custom setting for OpportunityTeam trigger
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'OpportunityTeamMemberTrigger';
        insert objCustomsetting;
        
        //create the custom setting for general data setting
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Closed Won,Closed Lost,Closed Deferred';
        insert objGeneralDataSetting;
        
        List<User> lstUser = new List<User>();
        User objUser1 = TestDataFactory.createTestUser('BSI - Financial Institutions','testUser1','testLastName1');
        User objUser2 = TestDataFactory.createTestUser('BSI - Financial Institutions','testUser2','testLastName2');
        lstUser.add(objUser1);
        lstUser.add(objUser2);
        if(!lstUser.isEmpty()){
            insert lstUser;
        }
        
        Account objAccount = TestDataFactory.createProspect();
        insert objAccount;
        
        Opportunity objOpportunity = TestDataFactory.createOppty('Written', 'Test oppty A',objAccount.Id);
        objOpportunity.OwnerId = objUser1.id;
        insert objOpportunity;
        
        system.runAs(objUser1){
        
            OpportunityTeamMember objOpportunityTeamMember1 = TestDataFactory.createOpptyTeammember(objUser1.Id, objOpportunity.id, 'Approver');
            OpportunityTeamMember objOpportunityTeamMember2 = TestDataFactory.createOpptyTeammember(objUser2.Id, objOpportunity.id, 'Account Manager');
            insert objOpportunityTeamMember1;
            insert objOpportunityTeamMember2;
            
            try{
                delete objOpportunityTeamMember1;
            } catch(DMLexception e){
                system.assert(e.getMessage().contains('Team Member cannot be deleted'),'Team Member cannot be deleted');
            }
            
        }

    }
    
     /**
    * This method will test scenarios when an Approver/URC Approver/Underwriter is updated on a BSI opportunity.
    * @param no parameters
    * @return void
    */
    static testmethod void testpreventBSIUpdate() {
        //create the custom setting for OpportunityTeam trigger
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'OpportunityTeamMemberTrigger';
        insert objCustomsetting;
        //create the custom setting for general data setting
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Closed Won,Closed Lost,Closed Deferred';
        insert objGeneralDataSetting;
        
        List<User> lstUser = new List<User>();
        User objUser1 = TestDataFactory.createTestUser('BSI - Financial Institutions','testUser1','testLastName1');
        User objUser2 = TestDataFactory.createTestUser('BSI - Financial Institutions','testUser2','testLastName2');
        lstUser.add(objUser1);
        lstUser.add(objUser2);
        if(!lstUser.isEmpty()){
            insert lstUser;
        }
        
        Account objAccount = TestDataFactory.createProspect();
        insert objAccount;
        
        Opportunity objOpportunity = TestDataFactory.createOppty('Written', 'Test oppty A',objAccount.Id);
        objOpportunity.OwnerId = objUser1.id;
        objOpportunity.TRAV_Business_Unit__c = 'BSI-FI';
        insert objOpportunity;
        
        system.runAs(objUser1){
        
            List<OpportunityTeamMember> lstOpportunityTeam = new List<OpportunityTeamMember>();
            OpportunityTeamMember objOpportunityTeamMember1 = TestDataFactory.createOpptyTeammember(objUser1.Id, objOpportunity.id, 'Approver');
            OpportunityTeamMember objOpportunityTeamMember2 = TestDataFactory.createOpptyTeammember(objUser2.Id, objOpportunity.id, 'Account Manager');
            lstOpportunityTeam.add(objOpportunityTeamMember1);
            lstOpportunityTeam.add(objOpportunityTeamMember2);
            //Insert temporary settings data to skip validation
            TRAV_Bypass_Settings__c objSetting= new TRAV_Bypass_Settings__c(SetupOwnerId=Userinfo.getUserId(), TRAV_Skip_Validation_Rules__c = true);
            insert objSetting;
            insert lstOpportunityTeam;
            if(objSetting != null) {
                delete objSetting;
            }
                
            objOpportunityTeamMember1.TeamMemberRole = 'Account Manager';
            
            try{
                update lstOpportunityTeam;
            } catch(DMLexception e){
                //system.assert(e.getMessage().contains('Team Member role cannot be changed'),'Team Member role cannot be changed');
            }
            
        }
        //Test the other Manager scenarios:
        //Create a manager
        User managerUser = TestDataFactory.createTestUser('BI - National Accounts', 'Test1', 'Manager');
        insert managerUser;
        //Create a user with manager
        Id idManager = [SELECT Id FROM User where LastName = 'Manager' LIMIT 1].Id;
        User testUser = TestDataFactory.createTestUser('BI - National Accounts', 'Test', 'LUser');
        testUser.ManagerId = idManager;
        insert testUser;
        //create a team member with same user
        OpportunityTeamMember oldObjOpportunityTeamMember = TestDataFactory.createOpptyTeammember(testUser.Id, objOpportunity.id, 'Approver');
        insert oldObjOpportunityTeamMember;
        Map<Id,OpportunityTeamMember> oldOpptyMap = new Map<Id,OpportunityTeamMember>();
        oldOpptyMap.put(oldObjOpportunityTeamMember.id, oldObjOpportunityTeamMember);
        OpportunityTeamMemberService.updateManagers(null, oldOpptyMap);
    }
}