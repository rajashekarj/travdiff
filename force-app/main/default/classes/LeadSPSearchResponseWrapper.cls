public class LeadSPSearchResponseWrapper {

    public class Addresses {
        public String addressType;
        public String rawAddressLine1;
        public String rawAddressLine2;
        public String rawCity;
        public String rawState;
        public String rawZipCode;
        public String scrubbedAddressLine1;
        public String scrubbedAddressLine2;
        public String scrubbedCity;
        public String scrubbedState;
        public String scrubbedZipCode;
        public String scrubbedCounty;
        public String rawCounty;
        public String scrubbedCountry;
        public String rawCountry;
        public String rawZip4;
        public String rawZip10;
        public String scrubbedZip4;
        public String scrubbedZip10;
        public String certLocId;
    }

    public Integer recordCount{get;set;}
    public List<ResultsSet> resultsSet;

    public class AcctAliasName {
    }

    public class Address {
        public String addressType;
        public String rawAddressLine1;
        public String rawAddressLine2;
        public String rawCity;
        public String rawState;
        public String rawZipCode;
        public String scrubbedAddressLine1;
        public String scrubbedAddressLine2;
        public String scrubbedCity;
        public String scrubbedState;
        public String scrubbedZipCode;
        public String scrubbedCounty;
        public String rawCounty;
        public String scrubbedCountry;
        public String rawCountry;
        public String rawZip4;
        public String rawZip10;
        public String scrubbedZip4;
        public String scrubbedZip10;
        public String certLocId;
    }

    public class SearchHit {
        public String searchSource;
        public String addressLine1;
        public String name;
        public String city;
    }

    public class AcctAliasName_Z {
        public String acctAliasName;
    }

    public class ResultsSet {
        public CbeDetails cbeDetails;
    }

    public class Address_Y {
        public String addressType;
        public String rawAddressLine1;
        public String rawAddressLine2;
        public String rawCity;
        public String rawState;
        public String rawZipCode;
        public String scrubbedAddressLine1;
        public String scrubbedAddressLine2;
        public String scrubbedCity;
        public String scrubbedState;
        public String scrubbedZipCode;
        public String scrubbedCounty;
        public String rawCounty;
        public String scrubbedCountry;
        public String rawCountry;
        public String rawZip4;
        public String rawZip10;
        public String scrubbedZip4;
        public String scrubbedZip10;
        public String certLocId;
    }

    public class Address_Z {
        public String addressType;
        public String rawAddressLine1;
        public String rawAddressLine2;
        public String rawCity;
        public String rawState;
        public String rawZipCode;
        public String scrubbedAddressLine1;
        public String scrubbedAddressLine2;
        public String scrubbedCity;
        public String scrubbedState;
        public String scrubbedZipCode;
        public String scrubbedCounty;
        public String rawCounty;
        public String scrubbedCountry;
        public String rawCountry;
        public String rawZip4;
        public String rawZip10;
        public String scrubbedZip4;
        public String scrubbedZip10;
        public String certLocId;
    }

    public class Accounts {
        public String acctNumber;
        public String acctName;
        public String acctSICCd;
        public String acctNAICSCd;
        public String acctSrc;
        public String dunsNumber;
        public List<AcctAliasName> acctAliasName;
        public List<Address> address;
    }

    public class CbeDetails {
        public String certifiedBusinessId;
        public String dunsNumber;
        public String dunsStatus;
        public String dunsStatusReason;
        public String businessName;
        public String secondaryName;
        public List<Addresses> addresses;
        public String businessPhoneNumber;
        public String businessUrl;
        public String organizationName;
        public String sinceDate;
        public String organizationAltName;
        public String organizationAltDBAName;
        public String organizationNmScrub;
        public String organizationSICCd;
        public String organizationNAICSCd;
        public String organizationTINId;
        public String nbrOfEE;
        public String yearEstb;
        public String revenue;
        public List<Accounts> accounts;
        public List<SearchHit> searchHit;
        public String pubEntityInd;
        public String crScrPt;
        public String fincStrssScor;
        public String bkptInd;
    }

}