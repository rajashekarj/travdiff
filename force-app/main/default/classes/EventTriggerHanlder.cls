/**
 * Handler Class for Event Trigger. This class won't have any logic and
 * will call other Event Service/Utilities class.
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------
 * Isha Shukla            US62215              05/24/2019       Original Version
 * Shashank Agarwal       US63739              07/30/2019       Added insertEventRelationForStewardship to after Insert
 * Evan Wilcher			  US81058			   04/15/2020		Added EventService.updateType call to beforeInsert and beforeUpdate.
 * -----------------------------------------------------------------------------------------------------------
 */
public class EventTriggerHanlder implements ITriggerHandler{
    //Handler Method for Before Insert.
    public void beforeInsert(List<sObject> newItems) {
        EventService.fetchPrimaryContactId((List<Event>)newItems,null);
        EventService.updateTimeOfStewardshipEvent((List<Event>)newItems);
        EventService.updateType((List<Event>)newItems);
    }

    //Handler Method for Before Update.
    public void beforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
        List<Event> lstEventNew = newItems.values();
        EventService.fetchPrimaryContactId(lstEventNew,(Map<Id, Event>)oldItems);
        EventService.updateType(lstEventNew);
    }

    //Handler Method for Before Delete.
    public void beforeDelete(Map<Id, sObject> oldItems) {
    }

    //Handler method for After Insert.
    public void afterInsert(Map<Id, sObject> newItems) {
        List<Event> lstEventNew = newItems.values();
        EventService.insertStewardshipRecord(lstEventNew);
		EventService.insertEventRelationForStewardship(lstEventNew);
    }

    //Handler method for After Update.
    public void afterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
    }

    //Handler method for After Delete
    public void afterDelete(Map<Id, sObject> oldItems) {
    }

    //Handler method for After Undelete.
    public void afterUndelete(Map<Id, sObject> oldItems) {
    }
}