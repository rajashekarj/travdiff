/**
* This class is used as THe Conroller for The Pop up close screen "PopUpBaseContainer" component.
* This allows the user to add the closed reason for each product associates with the Opportunity
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect         Date             Description
* -----------------------------------------------------------------------------------------------------------          
* Isha Shukla
* Sayanka Mohanty
* Shashank Agarwal
* -----------------------------------------------------------------------------------------------------------              
*/
public class CloseScreenPopUpApexController {
    public static Boolean showProductDetailsAsPerMetadata = false;
    
    /**
* This method is used to get all the details which will determine when to load PopUp 
and what are the fields that will be displayed on the PopUp.
* @param recordId The opportunity Id passed to the method to get a particular opportunity.
* @param sObjectName Object on which Pop Up will be initiated.
* @param fields string of fields that those are needed to be queried to load Pop-Up.
* @return PopupParentWrapper
*/
    @AuraEnabled
    public static PopupParentWrapper getAllDetails(Id recordId,String sObjectName, String fields) {
        //instantiating popupparentwrapper
        PopupParentWrapper objPopupParentWrapper = new PopupParentWrapper();
        PopupColumnWrapper objcolumnWrapper;
        TRAV_Carrier__c objCarrItem = null;
        List<PopupColumnWrapper> listPopupColumnWrapper;
        List<List<PopupDataWrapper>> listDataDetails = new List<List<PopupDataWrapper>>();
        try{
            //Querying Opportunity record where user is currently landed
            String strQuery = 'SELECT '+fields+' FROM '+sObjectName+ ' WHERE Id = :recordId';
            if(sobjectName == Constants.OpportunityObject){
                Opportunity objOpportunityRecord = new Opportunity();
                objOpportunityRecord = Database.query(strQuery);
                String strOpportunityStageName = objOpportunityRecord.StageName;
                String strOpportunityBU = '%'+objOpportunityRecord.TRAV_Business_Unit__c+'%';
                //Querying parent popup metadat records by filtering objecttype, stage and BU
                List<TRAV_PopUpBaseContainerSettings__mdt> lstPopUpParentMetadata = new List<TRAV_PopUpBaseContainerSettings__mdt>();
                lstPopUpParentMetadata = [SELECT TRAV_Business_Unit__c,TRAV_Pop_Up_Header__c,TRAV_Prevent_If_Prior_Stage__c,
                                          TRAV_Section_to_be_Loaded__c,TRAV_sObject__c,TRAV_StageName__c ,TRAV_CheckNullFieldName__c,
                                          (SELECT TRAV_Column_Name__c,TRAV_Helptext__c, TRAV_Column_Sequence__c,TRAV_Is_Read_Only__c, TRAV_Required__c, TRAV_SF_Field__c, 
                                           TRAV_Column_Type__c,Style_Class__c,TRAV_NeedOneSameStageError__c,TRAV_Validation_Message__c, TRAV_ParentRecord__c,TRAV_Value__c, TRAV_Default_Value__c, TRAV_Section__c 
                                           FROM TRAV_Popup_Detail_Configs__r 
                                           ORDER BY TRAV_Column_Sequence__c ASC
                                          )
                                          FROM TRAV_PopUpBaseContainerSettings__mdt 
                                          WHERE TRAV_sObject__c = :sobjectName 
                                          AND TRAV_StageName__c = :strOpportunityStageName 
                                          AND TRAV_Business_Unit__c LIKE :strOpportunityBU ];
                
                //If opportunity stage and business unit metadat records are not present then popup will not be loaded
                if(lstPopUpParentMetadata.size() == 0) {                
                    objPopupParentWrapper.loadPopUpScreen = false;
                }else{
                    if(objOpportunityRecord.TRAV_Winning_New_Carrier__c != null){
                        objCarrItem = [SELECT Id,Name FROM TRAV_Carrier__c WHERE Id = :objOpportunityRecord.TRAV_Winning_New_Carrier__c LIMIT 1 ];
                    }
                    //Iterating over parent metadata records to check pop up initial features
                    for(TRAV_PopUpBaseContainerSettings__mdt objParentMetadata : lstPopUpParentMetadata){
                        //checking if only opportunity or both section needs to be displayed
                        if(objParentMetadata.TRAV_Section_to_be_Loaded__c.contains(sobjectName) || 
                           objParentMetadata.TRAV_Section_to_be_Loaded__c.contains(Constants.PRODUCT)) {
                               objPopupParentWrapper.loadPopUpScreen = true;
                               if(!String.isBlank(objParentMetadata.TRAV_CheckNullFieldName__c)){
                                   String CheckNullField = objParentMetadata.TRAV_CheckNullFieldName__c;
                                   try{
                                       if(!String.isBlank((String)objOpportunityRecord.get(CheckNullField)) ){
                                           objPopupParentWrapper.loadPopUpScreen = false; 
                                       }
                                   }catch(Exception objEx){
                                       if(objOpportunityRecord.get(CheckNullField) != null){
                                           objPopupParentWrapper.loadPopUpScreen = false;  
                                       } 
                                   }
                               }
                               
                               objPopupParentWrapper.headerValue = objParentMetadata.TRAV_Pop_Up_Header__c;
                               objPopupParentWrapper.opportunityRecord = objOpportunityRecord;
                               objPopupParentWrapper.opportunityRecord.TRAV_Prospect_Effective_Date__c = objOpportunityRecord.TRAV_Policy_Effective_Date__c.addYears(1);
                               listPopupColumnWrapper =  new List<PopupColumnWrapper>();
                               //Iterating over child metadata for populating column wrapper
                               for(TRAV_Popup_Detail_Config__mdt objChilaMetadata : objParentMetadata.TRAV_Popup_Detail_Configs__r){
                                   objcolumnWrapper = new PopupColumnWrapper();
                                   objcolumnWrapper.apiName = objChilaMetadata.TRAV_SF_Field__c;
                                   objcolumnWrapper.label = objChilaMetadata.TRAV_Column_Name__c;
                                   objcolumnWrapper.sObjectName = objChilaMetadata.TRAV_Section__c;
                                   objcolumnWrapper.required = objChilaMetadata.TRAV_Required__c;
                                   objcolumnWrapper.fieldType = objChilaMetadata.TRAV_Column_Type__c;
                                   objcolumnWrapper.sequence = (Integer)objChilaMetadata.TRAV_Column_Sequence__c;
                                   objcolumnWrapper.styleClass = objChilaMetadata.Style_Class__c;
                                   objcolumnWrapper.isReadOnly = objChilaMetadata.TRAV_Is_Read_Only__c;
                                   objcolumnWrapper.helptext = objChilaMetadata.TRAV_Helptext__c;
                                   try{
                                       if(objChilaMetadata.TRAV_Section__c == 'Opportunity'){
                                           objcolumnWrapper.opportunityValue = String.valueOf(objOpportunityRecord.get(objChilaMetadata.TRAV_SF_Field__c));
                                       }
                                   }catch(Exception objEx){
                                       system.debug('**Opportunity Value cannot be set**'+ ' '+ objEx.getMessage());
                                   }
                                   //Logic to poluate stage values according to Type = renewal or new business
                                   if(objChilaMetadata.TRAV_SF_Field__c == 'TRAV_Stage__c' && 
                                      objChilaMetadata.TRAV_Value__c != null && 
                                      objChilaMetadata.TRAV_Value__c.contains('/') && 
                                      objChilaMetadata.TRAV_Section__c == Constants.PRODUCT){
                                          List<String> withType = objChilaMetadata.TRAV_Value__c.split('/');
                                          String values = objChilaMetadata.TRAV_Value__c;
                                          for(String obj : withType) {
                                              if(obj.split(':')[0] == objOpportunityRecord.Type){
                                                  values = obj.split(':')[1];
                                              }
                                          }
                                          objChilaMetadata.TRAV_Value__c = values;
                                      }
                                   objcolumnWrapper.values = String.isNotBlank(objChilaMetadata.TRAV_Value__c) ?objChilaMetadata.TRAV_Value__c.contains(';')? (objChilaMetadata.TRAV_Value__c).split(';') :(objChilaMetadata.TRAV_Value__c).split(' ') :null;
                                   objcolumnWrapper.defaultValue = objChilaMetadata.TRAV_Default_Value__c;
                                   objcolumnWrapper.validationErrorMessage = objChilaMetadata.TRAV_Validation_Message__c;
                                   objcolumnWrapper.sameStageError = objChilaMetadata.TRAV_NeedOneSameStageError__c;
                                   listPopupColumnWrapper.add(objcolumnWrapper);
                               }
                               objPopupParentWrapper.columnList = listPopupColumnWrapper; 
                               //Checking if product section needs to be displayed
                               if(objParentMetadata.TRAV_Section_to_be_Loaded__c.contains(Constants.PRODUCT)) {
                                   showProductDetailsAsPerMetadata = true;
                               }   
                           }
                    } //for loop end
                    
                    //querying opportunity products
                    if(showProductDetailsAsPerMetadata){
                        List<OpportunityLineItem> lstOpportunityProducts = new List<OpportunityLineItem>();
                        lstOpportunityProducts = [SELECT Id, Name, OpportunityId, Quantity, Product2.Name, TRAV_Stage__c,
                                                  UnitPrice, TRAV_Closed_Reason__c, TRAV_Closed_Sub_Reason__c, TRAV_Comments__c, TRAV_Product_Winning_New_Carrier__c,
                                                  TRAV_Product_Winning_New_Carrier__r.Name, TRAV_Program_Type__c, TRAV_Pricing_Type__c
                                                  FROM OpportunityLineItem 
                                                  WHERE OpportunityId = :recordId ];
                        
                        if(lstOpportunityProducts.size() > 0){
                            objPopupParentWrapper.listOpportunityProducts = lstOpportunityProducts;
                            
                            /*Logic to build 2 level Data Wrapper for Product Table*/
                            if(lstOpportunityProducts.size() == 1){
                                if(lstOpportunityProducts[0].TRAV_Closed_Reason__c != null &&
                                   lstOpportunityProducts[0].TRAV_Closed_Reason__c != '' ){
                                       objPopupParentWrapper.loadPopUpScreen = false;
                                   }
                            }
                            Integer closeReasonCount = 0;
                            for(OpportunityLineItem objOpptyProd: lstOpportunityProducts){
                                // Start added for US77231
                                List<Object> nameFormatArgs = new List<Object>{objOpptyProd.Product2.Name, objOpptyProd.TRAV_Program_Type__c, objOpptyProd.TRAV_Pricing_Type__c};
                                objOpptyProd.Product2.Name = String.format('{0}<br/>{1}, {2}', nameFormatArgs);
                                // End added for US77231
                                if(objOpportunityRecord.StageName  != Constants.OUTOFAPPETITE && 
                                   objOpportunityRecord.StageName  != Constants.CLOSEDDEFERRED){
                                       if(String.isBlank(objOpptyProd.TRAV_Closed_Reason__c) ) {
                                           if(objOpptyProd.TRAV_Stage__c != 'Line Not Selected'){
                                               //	objPopupParentWrapper.loadPopUpScreen = true;
                                               closeReasonCount++;
                                           }
                                       }
                                       
                                   }
                                //Initialise a new row
                                List<PopupDataWrapper> listrowDetails = new List<PopupDataWrapper>();
                                //loop into columns and create 2-level wrapper
                                for(PopupColumnWrapper wrapColumn : listPopupColumnWrapper){
                                    if(wrapColumn.sObjectName == Constants.PRODUCT) {
                                        
                                        PopupDataWrapper wrapSingleData = new PopupDataWrapper();
                                        if(wrapColumn.isReadOnly){
                                            wrapSingleData.originalValue = NotificationGenerator.getfieldApiValues(objOpptyProd, wrapColumn.apiName);
                                            wrapSingleData.isReadOnly = true;
                                        }
                                        wrapSingleData.sObjId = objOpptyProd.Id;
                                        wrapSingleData.columnType = wrapColumn.fieldType;
                                        wrapSingleData.sfField = wrapColumn.apiName;
                                        wrapSingleData.section = wrapColumn.sObjectName;
                                        // Start added for US76860
                                        if (objOpportunityRecord.RecordType.Name == 'Renewal NA'
                                        && objOpportunityRecord.StageName == 'Bound'
                                        && objOpportunityRecord.TRAV_Marketing_Conditions__c == 'Not Being Marketed'
                                        && objOpptyProd.TRAV_Stage__c == 'Written'
                                        && wrapSingleData.sfField == 'TRAV_Closed_Reason__c') {
                                            wrapSingleData.required = false;
                                        } else {
                                            wrapSingleData.required = wrapColumn.required;
                                        }
                                        // End added for US76860
                                        wrapSingleData.validationMessage = wrapColumn.validationErrorMessage;
                                        wrapSingleData.sfOpportunityProductId = objOpptyProd.Id;
                                        wrapSingleData.sameStageError = wrapColumn.sameStageError;
                                        //Below are changes for setting defaults based on single/multiple products
                                        wrapSingleData.defaultValue = wrapColumn.defaultValue;
                                        
                                        if(String.isNotBlank(wrapColumn.defaultValue)){
                                            if(wrapColumn.defaultValue.contains(':')){
                                                List<String> strTempDefault = wrapColumn.defaultValue.split(':');
                                                
                                                if(strTempDefault[1] == 'Single' && lstOpportunityProducts.size() == 1){
                                                    wrapSingleData.values = wrapColumn.values; 
                                                    wrapSingleData.inpValue = strTempDefault[0];
                                                }else{
                                                    if(strTempDefault[1] == 'All') {
                                                        wrapSingleData.values = wrapColumn.values;
                                                        if(strTempDefault[0] == 'Quote Unsuccessful'){
                                                            wrapSingleData.inpValue = 'Quote Unsuccessful';
                                                        }
                                                    }else {
                                                        list<String> listNew = new List<String>{' '};
                                                            listNew.addAll(wrapColumn.values);  
                                                        wrapSingleData.values =  listNew;
                                                    }
                                                    
                                                    
                                                }
                                            }
                                        }else{
                                            wrapSingleData.values = wrapColumn.values;
                                            if(lstOpportunityProducts.size() == 1){
                                                wrapSingleData.inpValue = wrapColumn.defaultValue;
                                            }
                                        }
                                        if( wrapColumn.fieldType == 'Lookup'){
                                            wrapSingleData.defaultCarrier = objCarrItem;
                                            wrapSingleData.selectedLookupRecord = objCarrItem;
                                            system.debug('***Carrier**'+objCarrItem);
                                        }
                                        
                                        if(wrapSingleData.columnType == 'Multi-Select' && wrapColumn.values != null) {
                                            list<String> listNew = new List<String>();
                                            listNew.addAll(wrapColumn.values);
                                            List<PopupDataWrapper.CloseReasonWrapper> listOfOptions = new List<PopupDataWrapper.CloseReasonWrapper>();
                                            for(String reason : listNew){
                                                PopupDataWrapper.CloseReasonWrapper option = new PopupDataWrapper.CloseReasonWrapper();
                                                option.reasonValue = reason;
                                                listOfOptions.add(option);
                                            }  
                                            wrapSingleData.closeReasonList = listOfOptions;
                                        } 
                                        
                                        listrowDetails.add(wrapSingleData);
                                    }
                                }
                                listDataDetails.add(listrowDetails);
                            }
                            if(objOpportunityRecord.StageName  != Constants.OUTOFAPPETITE && 
                               objOpportunityRecord.StageName  != Constants.CLOSEDDEFERRED) {
                                   if(closeReasonCount > 0) {
                                       objPopupParentWrapper.loadPopUpScreen = true;
                                   }else {
                                       objPopupParentWrapper.loadPopUpScreen = false;
                                   } 
                               }
                            
                            objPopupParentWrapper.dataDetails = listDataDetails;
                            objPopupParentWrapper.stagesRequireReason = Label.TRAV_PopUpStageRequireReasons;
                            objPopupParentWrapper.stagesRequireSameStage = Label.TRAV_NeedSameStage;
                            objPopupParentWrapper.productWinningError = Label.TRAV_ProductWinningError;
                        }
                    }
                }
                
            }
        }catch(Exception ex){
            system.debug('ex>>'+ex.getMessage()+'line-'+ex.getStackTraceString()+ex.getLineNumber());
        }
		//Adding logic to update Opportunity Stage if popup value is false:US74909
        try{
            system.debug('**This will be executed everytime on stage change**');
            List<Opportunity> listObjOppty = [SELECT Id, StageName, TRAV_Popup_Stage__c FROM Opportunity WHERE Id = :recordId LIMIT 1];
            if(!listObjOppty.isEmpty()){
                if((listObjOppty[0].StageName != listObjOppty[0].TRAV_Popup_Stage__c) && (!objPopupParentWrapper.loadPopUpScreen) ){
                    //Update popup stage to actual stage if no popup is displayed
                    listObjOppty[0].TRAV_Popup_Stage__c = listObjOppty[0].StageName;
                    update listObjOppty;
                } 
            }
        }catch(Exception objEx){
            system.debug('**Exception caught **'+objEx.getMessage()+'**at line**'+objEx.getStackTraceString()+objEx.getLineNumber()); 
        }
        return objPopupParentWrapper;
    }
    /**
* This method is used save the details that we are getting from PopUp.
* @param recordId The opportunity Id passed to the method to get a particular opportunity.
* @param strOutstandingItemIndicator Value of Outstanding Indicator set in the PopUp.
* @param strMaterialsRequested Value of Materials Requested set in the PopUp.
* @param strUnderReviewNotes Value of Under Review Notes set in the PopUp.
* @return void
*/
    @AuraEnabled
    public static void saveProductDetails(List<List<PopupDataWrapper>> lstPopupDataWrapper, Id idOpptyRec){
        final String METHODNAME = 'saveProductDetails';
        List<sObject> listObjToUpdate = new List<sObject>();
        Map<Id,Map<String,Object>> mapIdAPIValue = new Map<Id,Map<String,Object>>();
        Boolean recordsSaved = false;
        System.debug('>>'+lstPopupDataWrapper);
        try{
            for(List<PopupDataWrapper> listDataRows : lstPopupDataWrapper ){
                for(Integer i = 0 ; i < listDataRows.size() ; i++){
                    //Consider entry as 1 row
                    if((listDataRows[i].inpValue != null && listDataRows[i].inpValue != '' && listDataRows[i].sfField != 'Product2.Name') || (listDataRows[i].selectedLookupRecord != null || listDataRows[i].closeReasonList != null) ){
                        Map<String,Object> mapApiValue = new Map<String,Object>();
                        if(mapIdAPIValue.containsKey(listDataRows[i].sObjId)){
                            mapApiValue = mapIdAPIValue.get(listDataRows[i].sObjId);
                            if(listDataRows[i].columnType == 'Lookup'){
                                mapApiValue.put(listDataRows[i].sfField,listDataRows[i].selectedLookupRecord.id);
                            }else{
                                if(listDataRows[i].columnType == 'Multi-Select'){
                                    String strPicklistSelected = '';
                                    for(PopupDataWrapper.CloseReasonWrapper wrapCR : listDataRows[i].closeReasonList){
                                        
                                        if(wrapCR.reasonChecked != null){
                                            if(wrapCR.reasonChecked){
                                                strPicklistSelected+=wrapCR.reasonValue + ';';
                                            }
                                        }
                                    }
                                    if(listDataRows[i].sfField != 'Product2.Name'){
                                        mapApiValue.put(listDataRows[i].sfField,strPicklistSelected);
                                    }
                                }else{
                                    if(listDataRows[i].sfField != 'Product2.Name'){
                                        mapApiValue.put(listDataRows[i].sfField,listDataRows[i].inpValue);
                                    }
                                }
                            }
                            //Changes for saving Close Reason values
                            
                            mapIdAPIValue.put(listDataRows[i].sObjId,mapApiValue);
                        }
                        
                        
                        
                        else{
                            
                            if(listDataRows[i].columnType == 'Lookup'){
                                mapApiValue.put(listDataRows[i].sfField,listDataRows[i].selectedLookupRecord.id);
                            }else{
                                if(listDataRows[i].columnType == 'Multi-Select'){
                                    String strPicklistSelected = '';
                                    for(PopupDataWrapper.CloseReasonWrapper wrapCR : listDataRows[i].closeReasonList){
                                        if(wrapCR.reasonChecked != null){
                                            if(wrapCR.reasonChecked){
                                                strPicklistSelected+=wrapCR.reasonValue + ';';
                                            }
                                        }
                                    }
                                    if(listDataRows[i].sfField != 'Product2.Name'){
                                        mapApiValue.put(listDataRows[i].sfField,strPicklistSelected);
                                    }
                                }else{
                                    if(listDataRows[i].sfField != 'Product2.Name'){
                                        mapApiValue.put(listDataRows[i].sfField,listDataRows[i].inpValue);
                                    }
                                }
                            }
                            
                            
                            mapIdAPIValue.put(listDataRows[i].sObjId,mapApiValue);
                        }
                    }
                }
            }
            //Printing Map Details
            //Loop through map and update records
            for(Id idObj : mapIdAPIValue.keySet()){
                sObject objNew = idObj.getSobjectType().newSObject(idObj);
                Map<String,Object> tempMap = mapIdAPIValue.get(idObj);
                if(tempMap != null){
                    for(String strApi : tempMap.keySet()){
                        objNew.put(strApi,tempMap.get(strApi));
                        
                    }
                    listObjToUpdate.add(objNew);
                }
            }
            if(!listObjToUpdate.isEmpty() && listObjToUpdate != null){
                Database.SaveResult[] dbSaveList = Database.update(listObjToUpdate, true);
                //null check on dbSaveList
                if(!dbSaveList.isEmpty() && idOpptyRec != null){
                    for(Database.SaveResult objDBRec : dbSaveList){
                        if(objDBRec.isSuccess() ){
                            recordsSaved = true;
                        }else{
                            recordsSaved = false;
                            break;
                        }
                    }
                }
                //Update Opportunity Record if records are saved
                if(recordsSaved && idOpptyRec != null){
                    List<Opportunity> listObjRec = [SELECT Id, TRAV_Popup_Stage__c,TRAV_Business_Unit__c,  StageName FROM Opportunity WHERE Id = :idOpptyRec LIMIT 1];
					//Update Prospect Creation date
                    if(listObjRec[0].TRAV_Business_Unit__c.equalsIgnoreCase('BI-NA')){
                        updateProspectCreationDate(idOpptyRec);
                    }
                    //Update Stage
					if(listObjRec[0].TRAV_Popup_Stage__c != listObjRec[0].StageName){
                        listObjRec[0].TRAV_Popup_Stage__c = listObjRec[0].StageName;
                        update listObjRec[0]; 
                    }
                }
            }
        }catch(Exception objEx){
            system.debug('**Exception at**'+objEx.getLineNumber()+'**With message**'+objEx.getMessage());
            ExceptionUtility.logIntegrationerror('CloseScreenPopUpApexController', METHODNAME,objEx.getMessage());
        }
    } 
    
    @AuraEnabled
    public static Map<String,List<PopupDataWrapper.CloseReasonWrapper>> getSubListItems(List<String> selectedReasons, String opptyCloseReason){
        List<PopupDataWrapper.closeReasonWrapper> listOfSubOptions = new List<PopupDataWrapper.closeReasonWrapper>();
        List<String> coverageList = Constants.CoverageSubReasonList;
        List<String> priceList = Constants.PriceSubReasonList;
        List<String> priceListSS = Constants.PriceSubReasonListSS;
        List<String> selectedList = new List<String>();
        Set<String> subListItems = new Set<String>();
        String isMultiSelect = 'false';
        selectedList.addAll(selectedReasons);
        Map<String,List<PopupDataWrapper.CloseReasonWrapper>> mapSubReasonList = new Map<String,List<PopupDataWrapper.CloseReasonWrapper>>();
        if(!selectedList.isEmpty()){
            for(String str : selectedList){
                if(opptyCloseReason == Constants.QUOTEUNSUCCESSFULL && str == 'Coverage'){
                    subListItems.addAll(coverageList);
                    isMultiSelect = 'true';
                }
                if(opptyCloseReason == Constants.QUOTEUNSUCCESSFULL && str =='Pricing'){
                    if(selectedList.size() == 1){
                        subListItems.addAll(priceListSS);
                    }else{
                        subListItems.addAll(priceList);
                    }
                }
            }
        }
        List<PopupDataWrapper.CloseReasonWrapper> subReasonList = new List<PopupDataWrapper.CloseReasonWrapper>();
        //Prepare a wrapper
        for(String subReason : subListItems){
            PopupDataWrapper.CloseReasonWrapper wrapObj = new PopupDataWrapper.CloseReasonWrapper();
            wrapObj.reasonValue = subReason;
            subReasonList.add(wrapObj);
        }
        mapSubReasonList.put(isMultiSelect,subReasonList);
        return mapSubReasonList;
    }
    @AuraEnabled
    public static Map<String,List<PopupDataWrapper.CloseReasonWrapper>> getStageDependentReasons(String productStage){
        List<PopupDataWrapper.CloseReasonWrapper> reasonList = new List<PopupDataWrapper.CloseReasonWrapper>();
        List<String> reasonOptions = new List<String>();
        String isMultiSelect = 'true';
        if( productStage.EqualsIgnoreCase(Constants.WRITTENSTAGE)){
            reasonOptions.addAll(Constants.GenericReasonList);
            reasonOptions.addAll(Constants.WonReasonList);
            isMultiSelect = 'false';
        } 
        else if(productStage.EqualsIgnoreCase(Constants.DECLINED)){
            reasonOptions.addAll(Constants.GenericReasonList);
            reasonOptions.addAll(Constants.DeclinedReasonList);
        } 
        else if(productStage.EqualsIgnoreCase(Constants.LINENOTSELECTED)){
            reasonOptions.addAll(Constants.GenericReasonList);
            reasonOptions.addAll(Constants.LNSList);
        }
        else if(productStage.EqualsIgnoreCase(Constants.QUOTEUNSUCCESSFULL)){
            reasonOptions.addAll(Constants.GenericReasonList);
            reasonOptions.addAll(Constants.QuoteUnSucReasonList);
        }
        else if(productStage.EqualsIgnoreCase(Constants.CLOSEDDEFERRED)){
        }
        //Added additional condition for Bound Stage
        else if(productStage.EqualsIgnoreCase(Constants.BOUND)){
            reasonOptions.addAll(Constants.GenericReasonList);
        }
        else if(productStage.EqualsIgnoreCase(Constants.STAGELOST)){
            reasonOptions.addAll(Constants.GenericReasonList);
            
        }
        if(!reasonOptions.isEmpty()){
            //Prepare a wrapper
            for(String reason : reasonOptions){
                PopupDataWrapper.CloseReasonWrapper wrapObj = new PopupDataWrapper.CloseReasonWrapper();
                wrapObj.reasonValue = reason;
                reasonList.add(wrapObj);
            }
        }
        Map<String,List<PopupDataWrapper.CloseReasonWrapper>> mapReasonList = new Map<String,List<PopupDataWrapper.CloseReasonWrapper>>();
        mapReasonList.put(isMultiSelect,reasonList);
        return mapReasonList;
    }
    
    /**
* This method will revert the stage to original stage when user clicks on cancel.
* @param recordId The opportunity Id passed to the method to get a particular opportunity.
* @return void.
*/
    @AuraEnabled
    public static void revertStageUpdate(Id recordId){
        final string METHODNAME = 'revertStageUpdate';
        try{
            OpportunityFieldHistory objOpportunityHistory =[SELECT 
                                                            Id, Field, OldValue, NewValue 
                                                            FROM 
                                                            OpportunityFieldHistory 
                                                            WHERE 
                                                            Field = :Constants.FIELDNAMEFORHISTORY
                                                            AND 
                                                            OpportunityId = :recordId order by createddate desc][0];
            String strRevertStages = Label.StageRevertOnPopUpClose;
            if(strRevertStages.contains(String.valueOf(objOpportunityHistory.NewValue))){
                Opportunity objOpportunity = new Opportunity();
                objOpportunity.Id = recordId;
                objOpportunity.StageName = String.valueOf(objOpportunityHistory.OldValue);
                //Insert temporary settings data to skip validation
				TRAV_Bypass_Settings__c objSetting= new TRAV_Bypass_Settings__c(SetupOwnerId=Userinfo.getUserId(), TRAV_Skip_Validation_Rules__c = true);
				insert objSetting;
                update objOpportunity;
                //Remove settings data
                if(objSetting != null) {
                    delete objSetting;
                }
            }
        
    }catch(Exception objEx){
        system.debug('**Exception at**'+objEx.getLineNumber()+'**With message**'+objEx.getMessage());
        ExceptionUtility.logIntegrationerror('CloseScreenPopUpApexController', METHODNAME,objEx.getMessage());
    }
}
/**
* This method will update the Prospect Creation Date to today+1.
* @param Id recordId,String stage
* @return void.
*/
    @AuraEnabled
    public static void updateProspectCreationDate(Id recordId){
        final String METHODNAME = '';
        try{
            if(recordId != null){
                //Check if stage is closed
                List<Opportunity> listOppty = [SELECT Id,StageName,TRAV_Business_Unit__c,TRAV_Defferal_Date__c FROM Opportunity WHERE Id = :recordId LIMIT 1];
                if(!listOppty.isEmpty()){
                    if(Constants.NACLOSEDSTAGES.contains(listOppty[0].StageName) && listOppty[0].TRAV_Business_Unit__c == 'BI-NA'){
                        if(listOppty[0].TRAV_Defferal_Date__c != date.today().addDays(1)){
                            listOppty[0].TRAV_Defferal_Date__c = date.today().addDays(1);
                            update listOppty;
                        }
                    }
                }
                
            }
        }catch(Exception objEx){
            ExceptionUtility.logApexException('CloseScreenPopUpApex', METHODNAME, objEx, null );
        }
    }

}