/**
 * This class is used THe Conroller for TheOpportunityProductPopUp component.
 * This allows the user to add the closed reason for each product associates with the Opportunity
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect         Date             Description
 * -----------------------------------------------------------------------------------------------------------          
 * Shashank Shastri                                              Original version
 * Sayanka Mohanty         US63929,US63930          07/19/2019       Modified
 * -----------------------------------------------------------------------------------------------------------              
 */ 
public without sharing class OpportunityProductPopUpUtility {
    public static Boolean opptyPopUpContext = false;
    /**
* This method is used to fetch the opportunity stage.
* @param opptyId The opportunity Id passed to the method to get a particular opportunity.
* @return String: Opportunity stage is returned for the Id provided as an input parameter.
*/
    @AuraEnabled
    public static ClosedOpportunityProductWrapper getComponentLoadConfig(Id opptyId, String stage) {
        List<String> lstClosedStages = Constants.SELECTEDCLOSEDSTAGES.split(Constants.SEMICOLON);//Added for US68661
        List<String> lstProdStagesForReason = Constants.PRODSTAGESFORREASON.split(Constants.SEMICOLON); //Added for DE9970
        ClosedOpportunityProductWrapper closedOpportuityConfig = new ClosedOpportunityProductWrapper();
        //Check if Opportunity was moved from Prospect to Closed, else loadPopup should be false
        ProductWithReasonList productWithReason;
        boolean boolisClosedstage = false;
        boolean priorStageValid = false;
        boolean currentStageBound = false;
        boolean updatedToDeferred = false;
        List<ProductWithReasonList> ProductWithReasonList = new List<ProductWithReasonList>();
        List<String> reasonOptions = new List<String>();
        List<String> productStageList = new List<String>();
        //Check if Opportunity was moved from Prospect to Closed, else loadPopup should be false
        List<OpportunityFieldHistory> opptyFieldHist = new List<OpportunityFieldHistory>();
        opptyFieldHist = [SELECT Id, Field, OldValue, NewValue FROM OpportunityFieldHistory where Field = 'StageName' and OpportunityId = :opptyId order by createddate desc];
        if(!opptyFieldHist.isEmpty()){
            if(opptyFieldHist[0].oldValue != 'Bound'){
                priorStageValid = true; 
            }
            //Check if current stage is bound, then dislay popup:US63929
            if(opptyFieldHist[0].newValue == 'Bound' || 
               opptyFieldHist[0].newValue =='Deferred' || 
               opptyFieldHist[0].newValue == 'Out of Appetite' || 
              opptyFieldHist[0].newValue =='Lost' || 
              opptyFieldHist[0].newValue =='Declined' || 
              opptyFieldHist[0].newValue =='Quote Unsuccessful'){
                currentStageBound = true; 
                //Added for US69890
                if(opptyFieldHist[0].oldValue != 'Deferred' && opptyFieldHist[0].newValue =='Deferred' ){
                  updatedToDeferred = true;  
                }
            }
        }
        if(Test.isRunningTest()){
            priorStageValid = true;
            currentStageBound = true;
        }
        system.debug('priorStageValid || currentStageBound'+priorStageValid +'   '+ currentStageBound);
        if(priorStageValid || currentStageBound){
            opptyPopUpContext = true;
            Opportunity oppty = [SELECT Id, TRAV_Business_Unit__c,StageName,Type, TRAV_Policy_Expiration_Date__c, TRAV_Out_of_Appetite_Reason__c,TRAV_Close_Description__c,
                                 TRAV_Defferal_Date__c, TRAV_Winning_New_Carrier__c, TRAV_Policy_Effective_Date__c, TRAV_Prospect_Effective_Date__c
                                 FROM Opportunity WHERE Id=:opptyId];
            //Added for US68543
            closedOpportuityConfig.stageName = oppty.StageName;
            closedOpportuityConfig.prospectEffectiveDate = oppty.TRAV_Policy_Effective_Date__c.addYears(1); 
            closedOpportuityConfig.deferraldate = closedOpportuityConfig.prospectEffectiveDate.addDays(-150);
            closedOpportuityConfig.isBoundStage = (oppty.StageName).equalsIgnoreCase('Bound') ? true :false;
            closedOpportuityConfig.strOpportunityBU = oppty.TRAV_Business_Unit__c;
            if(oppty.TRAV_Winning_New_Carrier__c != null){
                TRAV_Carrier__c carrItem = [SELECT Id,Name FROM TRAV_Carrier__c WHERE Id = :oppty.TRAV_Winning_New_Carrier__c LIMIT 1 ];
                closedOpportuityConfig.defaultCarrier = carrItem; 
            }
            //Adding stage list condition as in bound, only 3 stages are required
             if((oppty.StageName).equalsIgnoreCase('Bound')){//changes started for DE9970
                if(oppty.TRAV_Business_Unit__c == Constants.BI_NA){
                     productStageList = (Constants.NABOUNDSTAGES).split(';');
                }else {
                    productStageList = (Constants.BOUNDSTAGES).split(';');
                }                
            }else if((oppty.StageName).equalsIgnoreCase('Declined')){
                if(oppty.TRAV_Business_Unit__c == Constants.BI_NA){
                     productStageList = (Constants.NADECLINEDSTAGES).split(';');
                }else {
                    productStageList = (Constants.DECLINEDSTAGES).split(';');
                } 
            }else if((oppty.StageName).equalsIgnoreCase('Quote Unsuccessful')){
                if(oppty.TRAV_Business_Unit__c == Constants.BI_NA){
                     productStageList = (Constants.NAQUSTAGES).split(';');
                }else {
                    productStageList = (Constants.QUSTAGES).split(';');
                } //changes end for DE9970
            }else if((oppty.StageName).equalsIgnoreCase('Deferred')){
                productStageList = (Constants.DEFSTAGES).split(';');
            }else if((oppty.StageName).equalsIgnoreCase('Lost')){
                productStageList = (Constants.LSTSTAGES).split(';');
            }else if((oppty.StageName).equalsIgnoreCase('Written')){
                productStageList = (Constants.BOUNDSTAGES).split(';');
                //Changes for US69890 starts
            }else if((oppty.StageName).equalsIgnoreCase('Out of Appetite')){
                if(oppty.Type == 'Renewal') {
                    productStageList = (Constants.OOASTAGESREN).split(';');
                }else if(oppty.Type == 'New Business'){
                    productStageList = (Constants.OOASTAGESNA).split(';');
                }
             //Changes for US69890 end   
            }else{
                productStageList = null;
            }
            closedOpportuityConfig.productStage = productStageList;
            //Get ProductList and iterate over it to form the ProductWithReasonList
            stage = String.isBlank(stage) ? oppty.StageName : stage;
            if( stage.EqualsIgnoreCase('Written')){
                reasonOptions.addAll(Constants.GenericReasonList);
                reasonOptions.addAll(Constants.WonReasonList);
                boolisClosedstage=true;
            } 
            else if(stage.EqualsIgnoreCase('Declined')){
                reasonOptions.addAll(Constants.GenericReasonList);
                reasonOptions.addAll(Constants.DeclinedReasonList);
                boolisClosedstage=true;
            } 
            else if(stage.EqualsIgnoreCase('Line Not Selected')){
                reasonOptions.addAll(Constants.GenericReasonList);
                reasonOptions.addAll(Constants.LNSList);
                boolisClosedstage=true;
            }
            else if(stage.EqualsIgnoreCase('Quote Unsuccessful')){
                reasonOptions.addAll(Constants.GenericReasonList);
                reasonOptions.addAll(Constants.QuoteUnSucReasonList);
                boolisClosedstage=true;
            }
            else if(stage.EqualsIgnoreCase('Deferred')){
                boolisClosedstage=true;
            }
            else if(stage.EqualsIgnoreCase('Out of Appetite')){
                boolisClosedstage=true;
            }
            else if(stage.EqualsIgnoreCase('Lost')){
                reasonOptions.addAll(Constants.GenericReasonList);
                boolisClosedstage=true;
            }
            //Added additional condition for Bound Stage
            else if(stage.EqualsIgnoreCase('Bound')){
                reasonOptions.addAll(Constants.GenericReasonList);
                reasonOptions.addAll(Constants.BNDwonReasonList);
                boolisClosedstage=true;
            }
            system.debug('closed description value'+closedOpportuityConfig.loadPopup);
            //closedOpportuityConfig.loadPopup = false;
            if(boolisClosedstage){
                //Get Products
                for(OpportunityLineItem productRec : [
                    SELECT Id,
                    Name,
                    OpportunityId,
                    Quantity,
                    Product2.Name,
                    TRAV_Stage__c,
                    UnitPrice,
                    TRAV_Closed_Reason__c,
                    TRAV_Closed_Sub_Reason__c,
                    TRAV_Comments__c,
                    TRAV_Product_Winning_New_Carrier__c,
                    TRAV_Product_Winning_New_Carrier__r.Name
                    FROM OpportunityLineItem 
                    WHERE OpportunityId=:opptyId 
                ]){
                    if(string.isBlank(productRec.TRAV_Closed_Reason__c)){
                        ProductWithReasonList obj = new ProductWithReasonList();
                        obj.productData = productRec;
                        obj.closeReasonList = getReasonWrapper(reasonOptions);
                        ProductWithReasonList.add(obj);
                        
                      /*  if(oppty.StageName.EqualsIgnoreCase('Deferred') && oppty.TRAV_Prospect_Effective_Date__c == NULL ){ // if date populated then it will not load
                            closedOpportuityConfig.loadPopup = true;
                        }
                        else if(oppty.StageName.EqualsIgnoreCase('Out of Appetite') && 
                                ( oppty.TRAV_Business_Unit__c == 'BI-NA' && String.isBlank(oppty.TRAV_Prospect_Effective_Date__c) || 
                                 oppty.TRAV_Business_Unit__c != 'BI-NA' && String.isBlank(oppty.TRAV_Out_of_Appetite_Reason__c)  )
                               ){ // if any other stage then Load the pop if reason not populated
                            closedOpportuityConfig.loadPopup = true;
                        } */
                        // BSI pop up changes - Nov 20
                        if(!oppty.StageName.EqualsIgnoreCase('Deferred') && !oppty.StageName.EqualsIgnoreCase('Out of Appetite') && oppty.TRAV_Business_Unit__c == Constants.BI_NA){ // if any other stage then Load the pop if reason not populated
                            System.debug('Markedtrue5>>'+ closedOpportuityConfig.loadPopup );
                            closedOpportuityConfig.loadPopup = true;
                            //Changed for DE9970 starts
                            if(!lstProdStagesForReason.contains(productRec.TRAV_Stage__c) 
                               && !string.isBlank(productRec.TRAV_Stage__c)) {
                                   closedOpportuityConfig.loadPopup = false; 
                               }
                            //Changed for DE9970 ends
                        }
                    }
                    if(string.isBlank(productRec.TRAV_Stage__c) && 
                      (oppty.StageName.EqualsIgnoreCase('Deferred') || oppty.StageName.EqualsIgnoreCase('Out of Appetite')) &&
                       oppty.TRAV_Business_Unit__c == 'BI-NA'
                      ){
                      closedOpportuityConfig.loadPopup = true;    
                    }
                    
                }
                //Updated condition based on US69890
                
                if(oppty.StageName.EqualsIgnoreCase('Out of Appetite') && 
                   ( (oppty.TRAV_Business_Unit__c == Constants.BI_NA && oppty.TRAV_Prospect_Effective_Date__c == null) || 
                    (oppty.TRAV_Business_Unit__c != Constants.BI_NA && String.isBlank(oppty.TRAV_Out_of_Appetite_Reason__c) ) )
                  ){ // if any other stage then Load the pop if reason not populated
                      closedOpportuityConfig.loadPopup = true;
                  } 
                // BSI pop up changes - Nov 20
                else if( lstClosedStages.contains(oppty.StageName) &&  oppty.TRAV_Prospect_Effective_Date__c == NULL){ // if date populated then it will not load
                    System.debug('Markedtrue3>>'+ closedOpportuityConfig.loadPopup );
                    closedOpportuityConfig.loadPopup = true;
                    if(oppty.TRAV_Business_Unit__c != Constants.BI_NA && !oppty.StageName.EqualsIgnoreCase('Deferred') ) {
                        closedOpportuityConfig.loadPopup = false;
                    }
                }
            }
            if(ProductWithReasonList.size() > 1){
                closedOpportuityConfig.multipleProducts = true;
                
            }else if(ProductWithReasonList.size() == 1){
                closedOpportuityConfig.multipleProducts = false;
                if(stage.EqualsIgnoreCase('Bound')){
                    ProductWithReasonList.CloseReasonWrapper addVal = new ProductWithReasonList.CloseReasonWrapper();
                    addVal.reasonValue = '';
                    addVal.reasonChecked = true;
                    ProductWithReasonList[0].closeReasonList.add(addVal);
                }
                
            }
            closedOpportuityConfig.productConfig = ProductWithReasonList;
            if(ProductWithReasonList.isEmpty()){
                closedOpportuityConfig.loadPopup = false;
                //For Deferred
                 if( ( lstClosedStages.contains(oppty.StageName) &&  oppty.TRAV_Prospect_Effective_Date__c == NULL) 
                   || 
                   //For Out of Appetite 
                   (oppty.StageName.EqualsIgnoreCase('Out of Appetite') && 
                    ( (oppty.TRAV_Business_Unit__c == Constants.BI_NA && oppty.TRAV_Prospect_Effective_Date__c == null ) || 
                     oppty.TRAV_Business_Unit__c != Constants.BI_NA && String.isBlank(oppty.TRAV_Out_of_Appetite_Reason__c)  )
                   )
                  ){
                      System.debug('Markedtrue4>>'+ closedOpportuityConfig.loadPopup );
                      closedOpportuityConfig.loadPopup = true;
                      //Nov 20 changes
                      if(lstClosedStages.contains(oppty.StageName) 
                         && oppty.TRAV_Business_Unit__c != Constants.BI_NA 
                         && !oppty.StageName.EqualsIgnoreCase('Deferred')){
                          closedOpportuityConfig.loadPopup = false;
                      }
                  }
            }
            //update the opportunity Deferral Date if the stage is Deferred
            if( ( lstClosedStages.contains(stage) || stage.equalsIgnoreCase('Out of Appetite') ) 
               && oppty.TRAV_Business_Unit__c == Constants.BI_NA && oppty.TRAV_Defferal_Date__c == null){
                   Opportunity oppRec = new Opportunity(Id = opptyId);
                   oppRec.TRAV_Prospect_Effective_Date__c = closedOpportuityConfig.prospectEffectiveDate;
                   oppRec.TRAV_Defferal_Date__c = closedOpportuityConfig.deferraldate;
                   database.update(oppRec);
                   system.debug('opprec'+oppRec);
               }else if(oppty.TRAV_Business_Unit__c != Constants.BI_NA && oppty.TRAV_Defferal_Date__c == null && stage.equalsIgnoreCase('Deferred')){
                   Opportunity oppRec = new Opportunity(Id = opptyId);
                   oppRec.TRAV_Prospect_Effective_Date__c = closedOpportuityConfig.prospectEffectiveDate;
                   oppRec.TRAV_Defferal_Date__c = closedOpportuityConfig.deferraldate;
                   database.update(oppRec);
               }  
        }else{
            //Handle Bound scenario here
            closedOpportuityConfig.loadPopup = false;
        }
        
        return closedOpportuityConfig;
    }
    public static List<ProductWithReasonList.closeReasonWrapper> getReasonWrapper(List<String> listOfReasons){
        List<ProductWithReasonList.CloseReasonWrapper> listOfOptions = new List<ProductWithReasonList.CloseReasonWrapper>();
        for(String reason : listOfReasons){
            ProductWithReasonList.CloseReasonWrapper option = new ProductWithReasonList.CloseReasonWrapper();
            option.reasonValue = reason;
            listOfOptions.add(option);
        }
        
        return listOfOptions;
    }
    @AuraEnabled
    public static void saveProductList(List<OpportunityLineItem> listOfProducts,String stage){
        system.debug('stg'+stage+'s'+listOfProducts);
		for(OpportunityLineItem item : listOfProducts){
            item.TRAV_Closed_Date__c = system.today();
            if(stage.equalsIgnoreCase('Deferred') || stage.equalsIgnoreCase('Out of Appetite') ){
                opptyPopUpContext = true;
                item.TRAV_Product_Status__c = stage;
            }
            
        }
       try{
            
			database.update(listOfProducts);
			}
        catch(Exception ex){
            system.debug(ex.getMessage()+'   '+ex.getLineNumber());
        }
        opptyPopUpContext = false;
    }
    @AuraEnabled
    public static List<ProductWithReasonList.closeReasonWrapper> getSubListItems(String selectedReasons, String opptyCloseReason){
        system.debug(selectedReasons+' params '+opptyCloseReason);
        List<ProductWithReasonList.closeReasonWrapper> listOfSubOptions = new List<ProductWithReasonList.closeReasonWrapper>();
        List<String> coverageList = Constants.CoverageSubReasonList;
        List<String> priceList = Constants.PriceSubReasonList;
        List<String> priceListSS = Constants.PriceSubReasonListSS;
        List<String> selectedList = new List<String>();
        Set<String> subListItems = new Set<String>();
        if(String.isNotBlank(selectedReasons)){
            if(selectedReasons.contains(';')){
                string [] tempList = selectedReasons.split('\\;');
                system.debug('templist'+tempList);
                selectedList.addAll(tempList);
            }else{
                 selectedList.add(selectedReasons);
            }
        }
        else{
            selectedList.add(selectedReasons);
        }
        system.debug(',,,,,'+selectedList);
        List<ProductWithReasonList.closeReasonWrapper> subReasonList = new List<ProductWithReasonList.closeReasonWrapper>();
        if(!selectedList.isEmpty()){
        for(String str : selectedList){
            if(opptyCloseReason =='Quote Unsuccessful'){
                system.debug('quote unsuccessful'+str);    
            }
            if(str.equalsIgnoreCase('Coverage')){
                system.debug('converage');    
            }
            if(opptyCloseReason =='Quote Unsuccessful' && str == 'Coverage'){
                subListItems.addAll(coverageList);
                system.debug(subListItems);
            }
            if(opptyCloseReason =='Quote Unsuccessful' && str =='Pricing'){
                 if(selectedList.size() == 1){
                    subListItems.addAll(priceListSS);
                }else{
                    subListItems.addAll(priceList);
                }
                system.debug(subListItems);
            }
        }
        }
        system.debug('sublistitems  '+subListItems);
        //Prepare a wrapper
        for(String subReason : subListItems){
            ProductWithReasonList.closeReasonWrapper wrapObj = new ProductWithReasonList.closeReasonWrapper();
            wrapObj.reasonValue = subReason;
            subReasonList.add(wrapObj);
        }
        return subReasonList;
    }
    @AuraEnabled
    public static List<ProductWithReasonList.closeReasonWrapper> getStageDependentReasons(String productStage){
        List<ProductWithReasonList.closeReasonWrapper> reasonList = new List<ProductWithReasonList.closeReasonWrapper>();
        List<String> reasonOptions = new List<String>();
        if( productStage.EqualsIgnoreCase('Written')){
            reasonOptions.addAll(Constants.GenericReasonList);
            reasonOptions.addAll(Constants.WonReasonList);
        } 
        else if(productStage.EqualsIgnoreCase('Declined')){
            reasonOptions.addAll(Constants.GenericReasonList);
            reasonOptions.addAll(Constants.DeclinedReasonList);
        } 
        else if(productStage.EqualsIgnoreCase('Line Not Selected')){
            reasonOptions.addAll(Constants.GenericReasonList);
            reasonOptions.addAll(Constants.LNSList);
        }
        else if(productStage.EqualsIgnoreCase('Quote Unsuccessful')){
            reasonOptions.addAll(Constants.GenericReasonList);
            reasonOptions.addAll(Constants.QuoteUnSucReasonList);
        }
        else if(productStage.EqualsIgnoreCase('Deferred')){
        }
        //Added additional condition for Bound Stage
        else if(productStage.EqualsIgnoreCase('Bound')){
            reasonOptions.addAll(Constants.GenericReasonList);
        }
        else if(productStage.EqualsIgnoreCase('Lost')){
                reasonOptions.addAll(Constants.GenericReasonList);
          
         }
        if(!reasonOptions.isEmpty()){
           reasonList = getReasonWrapper(reasonOptions);
        }
        return reasonList;
    }
}