/**
 * Interface methods to be used in all TriggersHandler classes.
 *
 * @author Chris Aldridge, http://chrisaldridge.com/triggers/lightweight-apex-trigger-framework/
 * MIT License
 * 
 * Copyright (c) 2016 Chris Aldridge, Silver Softworks
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Pranil Thubrikar       US59671              05/09/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
public Interface ITriggerHandler{
    void beforeInsert(List<sObject> newItems);
    void beforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems);
    void beforeDelete(Map<Id, sObject> oldItems);
    void afterInsert(Map<Id, sObject> newItems);
    void afterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems);
    void afterDelete(Map<Id, sObject> oldItems);
    void afterUndelete(Map<Id, sObject> oldItems);
}