/**
 * Test class for BatchPopulateEventDetails Class
 *
 * @Author : Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * -----------------------------------------------------------------------------------------------------------
 */
@isTest
private class TestBatchPopulateEventDetails {
    static testmethod void testBatch() {
        //create the custom setting for Opportunity trigger
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'EventTrigger';
        objCustomsetting.TRAV_Is_Active__c = false;
        insert objCustomsetting;
        //create the custom setting for general data setting
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Deferred;Written;Declined;Quote Unsuccessful';
        insert objGeneralDataSetting;
        
        //create account record
        Account prospectAccount = TestDataFactory.createProspect(); 
        List<Account> lstAccount = new List<Account>{prospectAccount};
            insert lstAccount;
        
        
        //creating agency contact
        Id objRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('TRAV_Agency_Licensed_Contact').getRecordTypeId();
        List<Contact> lstContact = TestDataFactory.createTravelersContact(2);
        lstContact[0].RecordTypeId = objRecordTypeId;
        lstContact[1].RecordTypeId = objRecordTypeId;
        lstContact[0].AccountId = lstAccount[0].Id;
        lstContact[1].AccountId = lstAccount[0].Id;
        insert lstContact;
        
        //Creating event 
        List<Event> lstEvent = new List<Event>();
        Event objEvent = new Event();
        objEvent.OwnerId = UserInfo.getUserId(); //user id
        objEvent.WhoId = lstContact[0].Id; //record id
        objEvent.DurationInMinutes = 60;
        objEvent.ActivityDateTime = System.now();
        Event objEvent2 = new Event();
        objEvent2.OwnerId = UserInfo.getUserId(); //user id
        objEvent2.WhoId = lstContact[1].Id; //record id
        objEvent2.DurationInMinutes = 60;
        objEvent2.ActivityDateTime = System.now();
        lstEvent.add(objEvent);
        lstEvent.add(objEvent2);
        insert lstEvent;
        Test.startTest();
        
        BatchPopulateEventDetails obj = new BatchPopulateEventDetails();
        Database.executeBatch(obj);
        
        Test.stopTest();
        System.assertEquals([SELECT 
                             TRAV_Related_Agency_Contact__c 
                             FROM 
                             Event 
                             WHERE 
                             WhoId =: lstContact[0].Id LIMIT 1].TRAV_Related_Agency_Contact__c ,true);
    }
}