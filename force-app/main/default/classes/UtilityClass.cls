/**
 * This class will have the common reusable utility methods. 
 * All methods should be defined as static.
 *
 * @Author : Deloitte Consulting
 *
 * Modification Log    : 
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shashank Agarwal       US63866              07/31/2019       Added Method  roundOffUtility
 * Shashank Agarwal       US59903              06/12/2019       Added method getAllFieldsSOQL
 * -----------------------------------------------------------------------------------------------------------
 * Pranil Thubrikar       US59671              05/23/2019       Added triggerCheck method
 * -----------------------------------------------------------------------------------------------------------              
 * Pranil Thubrikar       N/A                  05/20/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */

public class UtilityClass{
    
    /**
     * Method to check whether Trigger is to be executed as per the Trigger Setting Custom Setting.
     * @param strTriggerName  Name of the trigger this method is being called from.
     * @return Boolean        Boolean value to check if trigger is to be executed or not.
     */
    public static Boolean triggerCheck(String strTriggerName) {
        
        //Variable to store Boolean value to run Trigger
        Boolean boolRunTrigger = false;
        
        //Check to Skip Trigger altogether or org wide
        if(TRAV_Trigger_Setting__c.getOrgDefaults().TRAV_Is_Active__c &&
           (String.isBlank(TRAV_Trigger_Setting__c.getOrgDefaults().TRAV_Skip_Trigger_Org_Wide__c) ||
           (!TRAV_Trigger_Setting__c.getOrgDefaults().TRAV_Skip_Trigger_Org_Wide__c.contains(strTriggerName)))){
            
            //Get Custom Setting record if present for any User/Profile
            TRAV_Trigger_Setting__c profileSetting = TRAV_Trigger_Setting__c.getInstance(UserInfo.getProfileId());
            TRAV_Trigger_Setting__c userSetting = TRAV_Trigger_Setting__c.getInstance(UserInfo.getUserId());
            
            //Check to skip trigger for a specific User or Profile.
            if((!profileSetting.TRAV_Trigger_Name__c.contains(strTriggerName) || 
               (profileSetting.TRAV_Trigger_Name__c.contains(strTriggerName) && profileSetting.TRAV_Is_Active__c)) &&
               (!userSetting.TRAV_Trigger_Name__c.contains(strTriggerName) ||
               (userSetting.TRAV_Trigger_Name__c.contains(strTriggerName) && userSetting.TRAV_Is_Active__c))){
                
                boolRunTrigger = true;
            } //End - if
        } //End - if
        return boolRunTrigger;
    } //End triggerCheck
    
    /**
     * Method to get Integration Endpoint URL from Custom Metadata with respect to the current org selected
     * @param strIntegrationName  Name of the Integrationthis method is being called from.
     * @return String             String value of Endpoint URL.
     */
    public static String getEndppointURL(String strIntegrationName) {
        //In Progress
        return null;
    }
	  /**
* Method to roundOff to nearest 100k
* @param Decimal  Value to be rounded off .
* @return Decimal  Rounded off value.
*/
    
    public static Decimal roundOffUtility(Decimal objEstimatedRevenue){
        objEstimatedRevenue = objEstimatedRevenue/100000.00; 
        if(objEstimatedRevenue <= 1){
            objEstimatedRevenue = 100000;
        }else{
            objEstimatedRevenue = (objEstimatedRevenue.round(System.RoundingMode.HALF_EVEN))*100000.00; 
        }
        return objEstimatedRevenue;
    }
	/**
* Method to generate string of all createable fields for any object
* @param objectName  Name of the of object for which query is to be generated.
* @return String  String for all fields.
*/
    
    public static string getAllFieldsSOQL(String objectName){
        
        String strSelectFields = '';
        
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> mapField = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        
        //List of all the fields of te defined object.
        List<String> lstSelectFields = new list<String>();
        
        //Null check on mapField 
        if (mapField != null){
            // Iterating on all field tokens
            for (Schema.SObjectField tokenField : mapField.values()){
                // Getting field describe on each field token
                Schema.DescribeFieldResult describeField = tokenField.getDescribe();
                // Check on describeField is creatable
                if (describeField.isCreateable()){ 
                    lstSelectFields.add(describeField.getName());
                } // End of if
            } // End of for Loop
        } // End of if for NUll Check
        
        // Null Check on List of fields
        if (!lstSelectFields.isEmpty()){
            // Loop for generating String of all fields for SOQL 
            for (String  objString : lstSelectFields){
                strSelectFields += objString + ',';
            }
            
            // Check on generated field.
            if (strSelectFields.endsWith(',')){
                //Trimming string
                strSelectFields = strSelectFields.substring(0, strSelectFields.lastIndexOf(','));
            }
            
        } // End of if for NUll check on List 
        
        //Returning String for all the fields for an Object
        return  strSelectFields;
        
    }
}