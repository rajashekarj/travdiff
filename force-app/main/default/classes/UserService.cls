/**
 * Service-pattern class containing static methods used to perform DML operations specifically for the User object. 
 *
 * Modification Log:
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Ben Climan             US79918              04/08/2020       Original Version
 * Ben Climan             US79918              04/13/2020       Updated methods to ignore Exception logging during tests, to avoid MIXED_DML_OPERATION errors
 * -----------------------------------------------------------------------------------------------------------
 */
public without sharing class UserService {
    /**
     * Does the necessary DML operations to assign the Users to the Permission Set.
     * @param {String} PermissionSet Name
     * @param {List<User>} The list of Users to assign the Permission Set to
     */ 
    public static void assignPermissionSet(String permissionSet, List<User> users) {
        SavePoint sp = Database.setSavePoint();
        List<Database.UpsertResult> results;
        List<PermissionSetAssignment> assignments = new List<PermissionSetAssignment>();
        Id permSetId;

        try {
            PermissionSet ps = SailPointUtilities.getPermissionSet(permissionSet);
            permSetId = ps.Id;
        } catch (QueryException qe) {
            System.debug(qe);
        }
        
        for (User u : users) {
            assignments.add(new PermissionSetAssignment(AssigneeId=u.Id, PermissionSetId=permSetId));
        }

        try {
            results = Database.upsert(assignments);
        } catch (DmlException de) {
            System.debug(de);
            if (!Test.isRunningTest()) {
                //createExceptionUtility('assignPermissionSet', de, results);
                System.enqueueJob(new SailPointUtilities.QueueableExceptionLog('UserService', 'assignPermissionSet', de));
            }
            Database.rollback(sp);
        }
    }

    /**
     * Does the necessary DML operations to assign the Users to the Public Group.
     * @param {String} PublicGroup Name
     * @param {List<User>} The list of Users to assign to the Public Group
     */
    public static void assignPublicGroup(String publicGroup, List<User> users) {
        SavePoint sp = Database.setSavePoint();
        List<Database.UpsertResult> results;
        Id groupId;
        List<GroupMember> members = new List<GroupMember>();

        try {
            Group g = SailPointUtilities.getPublicGroup(publicGroup);
            groupId = g.Id;
        } catch (QueryException qe) {
            System.debug(qe);
        }

        for (User u : users) {
            members.add(new GroupMember(UserOrGroupId=u.Id, GroupId=groupId));
        }

        try {
            results = Database.upsert(members);
        } catch (DmlException de) {
            System.debug(de);
            if (!Test.isRunningTest()) {
                System.enqueueJob(new SailPointUtilities.QueueableExceptionLog('UserService', 'assignPublicGroup', de));
            }
            Database.rollback(sp);
        }
    }
}