/**
 * This class is used as a Wrapper Class to store the Product vs the Closed Reason data.
 * This is invoked when an opportunity stage is marked as closed.
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------          
 * Shashank Shastri            US60000         06/20/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------              
 */
public class ProductWithReasonList{
   /* @AuraEnabled
    public List<string> closeReasonList = new List<string>();*/
    @AuraEnabled
    public OpportunityLineItem productData = new OpportunityLineItem();
    @AuraEnabled
    public List<CloseReasonWrapper> closeReasonList = new List<CloseReasonWrapper> ();
    public class CloseReasonWrapper{
        @AuraEnabled
        public String reasonValue;
        @AuraEnabled
        public Boolean reasonChecked = false;
    }
}