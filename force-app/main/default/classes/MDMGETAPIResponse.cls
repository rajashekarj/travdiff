public class MDMGETAPIResponse {

    public class SrcInfo {
        public Src src;
    }

    public class BMLoc {
        public String bmLocId;
        public String agcyNm;
        public String addrLn1;
        public String city;
        public String st;
    }

    public class Src {
        public String srcSysCd;
        public String srcId;
    }

    public String Status;
    public String agencyIndvId;
    public List<BMLoc> BMLoc;
    public List<RoleCdArr> roleCdArr;
    public List<RuArr> ruArr;
    public RuArr spclCdArr;
    public String fullNm;
    public String fstNm;
    public String lstNm;
    public String busEmailAddr;
    public String busTelNbr;
    public String licInd;
    public Object licCd;
    public Object natlPrdrNbr;
    public String ivldEmailInd;
    public String sysAcssInd;
    public List<SrcInfo> srcInfo;

    public class RuArr {
    }

    public class RoleCdArr {
        public String roleCd;
        public String roleShrtDesc;
        public String roleLngDesc;
        public String roleClssCd;
    }

}