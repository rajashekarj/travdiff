/**
 * This class is used as a Selector Layer for AccountTeamMember object. 
 * All queries for AccountTeamMember object will be performed in this class.
 *
 *
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Isha Shukla            US60806         	 05/31/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */ 
public class AccountTeamMemberSelector {
    /**
     * This is a method is used to get list AccountTeamMember 
     * @param  Account and user id set
     * @return List<AccountTeamMember> List of Opportunities based on the SOQL query provided as an input parameter.
     */
	 public static List<AccountTeamMember> getAccountTeamMembers(Set<Id> setOfAccountId, Set<Id> setOfUserId) {
       final string METHODNAME = 'getAccountTeamMembers';
       final string CLASSNAME = 'AccountTeamMemberSelector';
       List<AccountTeamMember> lstAccountTeam = new List<AccountTeamMember>();
       try {
	   		lstAccountTeam = new List<AccountTeamMember>([
                                     SELECT 
                                           Id,
                                           AccountId,
                						   UserId,
                						   TeamMemberRole
                                     FROM 
                                           AccountTeamMember
                                     WHERE 
                                           UserId IN : setOfUserId
                                     AND 
                						   AccountId IN : setOfAccountId
                              ]);
			If(Test.isRunningTest())
            {
               throw new QueryException('For testing purpose');
            } 
       } //End of Try
       catch(QueryException exp) {
       		//exception handling code
      		ExceptionUtility.logApexException(CLASSNAME, METHODNAME, exp, null);
                
       } // End Catch
	  	
	   return lstAccountTeam;
	 } //end of method getLeads

}