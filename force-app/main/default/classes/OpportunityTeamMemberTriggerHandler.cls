/**
 * Handler Class for OpportunityTeamMember Trigger. This class won't have any logic and
 * will call other OpportunityTeamMember Service/Utilities class.
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------
 * Isha Shukla            US60806              05/31/2019       Added code in afterInsert and before delete
 * Isha Shukla            US62980              06/24/2019       Added method call in afterUpdate
 * Derek Manierre 	  US68470	       03/03/2020	Added method call in beforeUpdate and beforeDelete. Added userProfileMatched variable
 * -----------------------------------------------------------------------------------------------------------
 */ 
public class OpportunityTeamMemberTriggerHandler implements ITriggerHandler{
    public static boolean isOpportunityTeamTriggerContext = false;
    public static boolean userProfileMatched  = OpportunityService.checkMethodAccessByProfile('Data Integration;System Administrator');
    //Handler Method for Before Insert.
    public void beforeInsert(List<sObject> newItems) {
		OpportunityTeamMemberService.updateManagers((List<OpportunityTeamMember>)newItems, null);
    }

    //Handler Method for Before Update.
    public void beforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
    List<OpportunityTeamMember> lstNewOpportunityTeamMember = newItems.values();
        if(!userProfileMatched){
            OpportunityTeamMemberService.preventTeamMemeberChangesBSI(lstNewOpportunityTeamMember,(Map<Id, OpportunityTeamMember>)oldItems);
        }
        OpportunityTeamMemberService.populateCreateFromsystemField(lstNewOpportunityTeamMember,(Map<Id, OpportunityTeamMember>)oldItems);

    }
    //Handler Method for Before Delete.
    public void beforeDelete(Map<Id, sObject> oldItems) {
        if(!userProfileMatched){
            OpportunityTeamMemberService.preventTeamMemeberChangesBSI(null,(Map<Id,OpportunityTeamMember>)oldItems);
        }
        OpportunityTeamMemberService.insertDeleteAccountTeams(null,(Map<Id,OpportunityTeamMember>)oldItems);
		OpportunityTeamMemberService.modifyEventAttendees(null,(Map<Id,OpportunityTeamMember>)oldItems);
        OpportunityTeamMemberService.addManagersInOpportunityTeamMembers(null,(Map<Id,OpportunityTeamMember>)oldItems);
    }

    //Handler method for After Insert.
    public void afterInsert(Map<Id, sObject> newItems) {
        //This method will add/remove account teams when related opportunity teams insert/delete
        List<OpportunityTeamMember> lstNewOpportunityTeam = newItems.values();
        OpportunityTeamMemberService.insertDeleteAccountTeams(lstNewOpportunityTeam,null);
        system.debug('System.isBatch()>>>>'+System.isBatch());
		OpportunityTeamMemberService.modifyEventAttendees(lstNewOpportunityTeam,null);
        system.debug('teammembers '+newItems.values());
        system.debug(System.isBatch()+'   execution logic    '+OpportunityTriggerHandler.isOpportunityTriggerContext);
       /* if(!System.isBatch() && !OpportunityTriggerHandler.isOpportunityTriggerContext){
            if(OpportunityTriggerHandler.isQueueable){
            	System.enqueueJob(new NotificationUtility(newItems.values(), null, 'OpportunityTeamMember'));
            }else{
                NotificationUtility nu = new NotificationUtility(newItems.values(), null, 'OpportunityTeamMember');
            }
        }*/
        /*Changes by SS - Don't remove
        List<OpportunityTeamMember> oppTeamList;
        if(OpportunityTriggerHandler.isOpportunityTriggerContext){
            // code  invoked from the opportunity trigger, means the stage is submission and a team member got inserted
            oppTeamList = [SELECT OpportunityId, UserId, Name,
                                                      TeamMemberRole, TRAV_Active__c,Opportunity.StageName,Opportunity.TRAV_Total_Claim_Count__c
                                                      FROM OpportunityTeamMember WHERE Opportunity.StageName != 'Submission' And Id IN:newItems.keyset()];
        }
        else{
            oppTeamList = newItems.values();
        }
        Changes by SS - Don't remove*/
		System.debug('In after Insert  '+!System.isBatch()+'    '+!System.isQueueable()+'    '+!OpportunityTriggerHandler.isOpportunityTriggerContext+'     '+!isOpportunityTeamTriggerContext+'    '+!OpportunityTeamMemberService.addManagerCheck);
        if(!System.isBatch() && !System.isQueueable() && !OpportunityTriggerHandler.isOpportunityTriggerContext && !isOpportunityTeamTriggerContext
          && !OpportunityTeamMemberService.addManagerCheck){
            System.enqueueJob(new NotificationUtility(newItems.values(), null, 'OpportunityTeamMember'));
        }
		System.debug('In after Insert  a');
        if(!System.isBatch() && !System.isQueueable())
        OpportunityTeamMemberService.addManagersInOpportunityTeamMembers(lstNewOpportunityTeam,null);
    }

    //Handler method for After Update.
    public void afterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
	try{
        //This method will add/remove account teams when related opportunity teams get activated or deactivated
        List<OpportunityTeamMember> lstNewOpportunityTeam = newItems.values();
        Map<Id,OpportunityTeamMember> mapOldOpportunityTeamMem = (Map<Id,OpportunityTeamMember>)oldItems;
        OpportunityTeamMemberService.insertDeleteAccountTeams(lstNewOpportunityTeam,mapOldOpportunityTeamMem);
		OpportunityTeamMemberService.modifyEventAttendees(lstNewOpportunityTeam,mapOldOpportunityTeamMem);
        System.debug('In after Update  '+!System.isBatch()+'    '+!System.isQueueable()+'    '+!OpportunityTriggerHandler.isOpportunityTriggerContext);
        if(!System.isBatch() && !System.isQueueable() && !OpportunityTriggerHandler.isOpportunityTriggerContext)
            OpportunityTeamMemberService.addManagersInOpportunityTeamMembers(lstNewOpportunityTeam,mapOldOpportunityTeamMem);
		}
		catch(Exception e){
			System.debug(e.getLineNumber()+'  '+e.getMessage());
		}
    }
    //Handler method for After Delete
    public void afterDelete(Map<Id, sObject> oldItems) {

    }

    //Handler method for After Undelete.
    public void afterUndelete(Map<Id, sObject> oldItems) {

    }

}