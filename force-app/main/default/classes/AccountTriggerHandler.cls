/**
 * Handler Class for Account Trigger. This class won't have any logic and
 * will call other Account Service/Utilities class.
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Pranil Thubrikar       US59671              05/09/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */

public class AccountTriggerHandler implements ITriggerHandler {
    
    //Handler Method for Before Insert.
    public void beforeInsert(List<sObject> newItems) {
        
    }
    
    //Handler Method for Before Update.
    public void beforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
       
    }
    //Handler Method for Before Delete.    
    public void beforeDelete(Map<Id, sObject> oldItems) {
        
    }
    
    //Handler method for After Insert.
    public void afterInsert(Map<Id, sObject> newItems) {
                system.enqueueJob(new AccountService((List<Account>)newItems.values(),null));  
    } 

    //Handler method for After Update.    
    public void afterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
    	if(!system.isBatch())
          system.enqueueJob(new AccountService((List<Account>)newItems.values(),(Map<id,Account>)oldItems));  
          
    }
    //Handler method for After Delete    
    public void afterDelete(Map<Id, sObject> oldItems) {
    
    }
    
    //Handler method for After Undelete.
    public void afterUndelete(Map<Id, sObject> oldItems) {
    
    } 
}