/**
* Utility Class for OpportunityContactRole Trigger Handler. This class will have logic for all the implementations.
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer     User Story/Defect   Date        Description
* -----------------------------------------------------------------------------------------------------------
* Kundan Shaw       US73671         03/04/2020  Added method onePrimaryOppContactRole
* Kundan Shaw       US73671         03/04/2020  Added method onePrimaryOppContactRoleUpdate
*/

public class OpportunityContactRoleService {
    /**
* This is a method  that make sure only one primary contact is associated with an opportunity.
* @param lstNewOppContactRole - List of new values in the for the OpportunityContactRole records.
* @param mapOldOppContactRole - Map of old values in the for the OpportunityContactRole records
* @return void - returns void .
*/
    public static void UpdatePrimaryCOntact(List<OpportunityContactRole> lstNewOppContactRole, Map<Id, OpportunityContactRole> mapOldOppContactRole) {
        final string METHODNAME = 'UpdatePrimaryCOntact';
      
       set<string> setOpportunityId = new set<string>();
       Set<String> setUniqueIdentifier = new Set<String>();
       set<string> setOpportuntiyConRolToOpportunity = new set<string>();
       List<OpportunityContactRole> lstOpportunityContactRoleToUpdate = new List<OpportunityContactRole>();
        try{
            for(OpportunityContactRole objOpportunityContRole:lstNewOppContactRole){
                String recordTypeKey=uniqueKey(objOpportunityContRole);
                setOpportunityId.add(objOpportunityContRole.OpportunityId);
                //check if primary is true
                if((objOpportunityContRole.IsPrimary && mapOldOppContactRole ==null)
                  || (mapOldOppContactRole !=null && 
                     objOpportunityContRole.IsPrimary 
                     &&objOpportunityContRole.IsPrimary !=
                     mapOldOppContactRole.get(objOpportunityContRole.id).isPrimary) ){
                      //set the unique identifiers to check for duplicate
                      if(!setUniqueIdentifier.contains(recordTypeKey))
                         setUniqueIdentifier.add(recordTypeKey);
                    else
                        objOpportunityContRole.isPrimary = false;
                } //end if the field modificaiton check
                else{
                    //check if there are no other Primary contact are present and make the first contact as priamry
                    setOpportuntiyConRolToOpportunity.add(recordTypeKey);
                } //end else
            } //end for loop iteration
        
            //qurey the existing OpportunityContact Role which are primary
            if(!setOpportunityId.isEmpty()){
                List<opportunityContactRole> lstExistingOppContRole = new List<OpportunityContactRole>();
                OpportunityContactRole objOpportunityContactRoletoUpdate ;
                if(mapOldOppContactRole ==null){
                    lstExistingOppContRole =[select OpportunityId,id,IsPrimary,ContactId,Trav_Rec_Type__c
                                             from OpportunityContactRole
                                              where OpportunityId IN:setOpportunityId AND IsPrimary=true];
                } // end if check for insert
                else{
                    lstExistingOppContRole= [select OpportunityId,id,IsPrimary,ContactId,Trav_Rec_Type__c
                    from OpportunityContactRole
                     where OpportunityId IN:setOpportunityId AND IsPrimary=true and id not in:lstNewOppContactRole];
                } //end else check for update

                if(!lstExistingOppContRole.isEmpty()){
                  for(OpportunityContactRole objOppContRole:lstExistingOppContRole){
                      String recordTypeKey=uniqueKey(objOppContRole);
                      //check for duplicates
                      if(setUniqueIdentifier.contains(recordTypeKey))
                      {
                        objOpportunityContactRoletoUpdate = new opportunityContactRole();
                        objOpportunityContactRoletoUpdate.id = objOppContRole.id;
                        objOpportunityContactRoletoUpdate.isPrimary =false;
                        lstOpportunityContactRoleToUpdate.add(objOpportunityContactRoletoUpdate);
                      }
                      //check if this is the first contact added 
                      if(setOpportuntiyConRolToOpportunity.contains(recordTypeKey)){
                        setOpportuntiyConRolToOpportunity.remove(recordTypeKey);
                      }
                  } //end for Iterate over the existing contact Role
                } //end if duplciate primary check
            } // end if lstExistingOppContRole
            for(OpportunityContactRole objOpportunityContRole:lstNewOppContactRole){
                String recordTypeKey=uniqueKey(objOpportunityContRole);
                if(mapOldOppContactRole ==null && !objOpportunityContRole.isPrimary 
                && setOpportuntiyConRolToOpportunity.contains(recordTypeKey))
                {
                    objOpportunityContRole.isPrimary =true;
                } //end if
              } //end for iteration to check if it first contact
              if(!lstOpportunityContactRoleToUpdate.isEmpty()){
                    update lstOpportunityContactRoleToUpdate;
              }
            if(Test.isRunningTest()){
                throw new QueryException();
            }
            } //end try
        catch(Exception e){
            System.debug(e.getMessage()+' '+e.getLineNumber());
            ExceptionUtility.logApexException(Constants.OPPORTUNITYCONTACTROLESERVICE, METHODNAME, e, null);
        }
        
    }
    
    
    
    /**
* This method  update the primary contact record of the associated opportunity.
* @param mapOldOppContactRole - Map of old values in the for the OpportunityContactRole records
* @return void - returns void .
*/  
    public static void updatePrimaryContactField(List<OpportunityContactRole> lstNewContactRole,Map<Id, OpportunityContactRole> mapOldOppContactRole) {
        final string METHODNAME = 'updatePrimaryContactField';
        List<OpportunityContactRole> lstOpportunityContactRoleToProcess = new List<OpportunityContactRole>();
        try{
           
            set<Id> contactIds=new set<Id>();
            set<Id> oppIds=new set<Id>();
            List<Opportunity> lstUpdatedOpp = new List<Opportunity>();
            if(lstNewContactRole!=null && !lstNewContactRole.isEmpty()){
                for(OpportunityContactRole oppContactRole:lstNewContactRole){
                    if((mapOldOppContactRole==null && oppContactRole.IsPrimary==true)
                       || (mapOldOppContactRole!=null && mapOldOppContactRole.get(oppContactRole.id).isPrimary !=oppContactRole.IsPrimary
                           )
                      )
                    {
                        //System.debug('kkr I am inside loaddata');
                        contactIds.add(oppContactRole.ContactId);
                        oppIds.add(oppContactRole.OpportunityId); 
                        lstOpportunityContactRoleToProcess.add(oppContactRole);
                    }
                }
            }
            
            Map<ID,String> mapContact = OpportunityContactRoleSelector.getContactIdName(contactIds);
            Map<ID,Opportunity> mapOpp = OpportunityContactRoleSelector.getOpportunities(oppIds);
            if(lstOpportunityContactRoleToProcess!=null && !lstOpportunityContactRoleToProcess.isEmpty()){
                for(OpportunityContactRole oppContactRole:lstOpportunityContactRoleToProcess){
                    //System.debug('Record Type value'+oppContactRole.Trav_Rec_Type__c.toLowerCase());
                    opportunity op =new opportunity();
                    if(oppContactRole.IsPrimary==true){
                        
                        //System.debug('is condition true'+oppContactRole.Trav_Rec_Type__c +' '+ Constants.LICENSEDCONTACTRECORDTYPE);
                        if(((oppContactRole.Trav_Rec_Type__c.equals(Constants.LICENSEDCONTACTRECORDTYPE)) || oppContactRole.Trav_Rec_Type__c.equals(Constants.NONLICENSEDCONTACTRECTYPE))){
                            op = mapOpp.get(oppContactRole.OpportunityId);
                            //System.debug('adding primary agency location contact');
                            op.Trav_Primary_Contact__c=oppContactRole.ContactId+'_'+mapContact.get(oppContactRole.ContactId);
                        }
                        else if(oppContactRole.Trav_Rec_Type__c.equals(Constants.CUSTOMERCONTACT)){
                            op = mapOpp.get(oppContactRole.OpportunityId);
                            //System.debug('adding primary customer contact');
                            op.Trav_Primary_Customer_Contact__c=oppContactRole.ContactId+'_'+mapContact.get(oppContactRole.ContactId);
                        }
                         }
                    else{
                        //System.debug('is condition true'+oppContactRole.Trav_Rec_Type__c +' '+ Constants.LICENSEDCONTACTRECORDTYPE);
                        if(((oppContactRole.Trav_Rec_Type__c.equals(Constants.LICENSEDCONTACTRECORDTYPE)) || oppContactRole.Trav_Rec_Type__c.equals(Constants.NONLICENSEDCONTACTRECTYPE))){
                            op = mapOpp.get(oppContactRole.OpportunityId);
                            //System.debug('adding primary agency location contact');
                            op.Trav_Primary_Contact__c='';
                        }
                        else if(oppContactRole.Trav_Rec_Type__c.equals(Constants.CUSTOMERCONTACT)){
                            op = mapOpp.get(oppContactRole.OpportunityId);
                            //System.debug('adding primary customer contact');
                            op.Trav_Primary_Customer_Contact__c='';
                        }
                    }

                        if(!(op.Trav_Primary_Customer_Contact__c==null && op.Trav_Primary_Contact__c ==null))
                          lstUpdatedOpp.add(op);
                      
                }  
            }
            //System.debug('Test Update Primary Contact '+lstUpdatedOpp.size()+'  '+lstUpdatedOpp);
            if(!lstUpdatedOpp.isEmpty())
            update lstUpdatedOpp;
            if(Test.isRunningTest()){
                throw new QueryException();
            }
        }
        catch(Exception e){
            System.debug(e.getMessage()+' '+e.getLineNumber());
            ExceptionUtility.logApexException(Constants.OPPORTUNITYCONTACTROLESERVICE, METHODNAME, e, null);
        }
        
    }
    
    /**
* This method  update the primary contact record of the associated opportunity.
* @param mapOldOppContactRole - Map of old values in the for the OpportunityContactRole records
* @param mapNewOppContactRole - Map of new values in the for the OpportunityContactRole records.
* @return void - returns void .
*/   
    /*public static void updatePrimaryContactFieldAfterUpdate(Map<Id, OpportunityContactRole> mapNewOppContactRole, Map<Id, OpportunityContactRole> mapOldOppContactRole) {
        final string METHODNAME = 'updatePrimaryContactFieldAfterUpdate';
        
        try{
            List<OpportunityContactRole> lstNewOppContactRole = mapNewOppContactRole.values();
            set<Id> contactIds=new set<Id>();
            set<Id> oppIds=new set<Id>();
            List<Opportunity> lstUpdatedOpp = new List<Opportunity>();
            if(lstNewOppContactRole!=null && !lstNewOppContactRole.isEmpty()){
                for(OpportunityContactRole oppContactRole:lstNewOppContactRole){
                    if((oppContactRole.IsPrimary==true && mapOldOppContactRole==null) || (mapOldOppContactRole!=null && (mapOldOppContactRole.get(oppContactRole.id).isPrimary==false && oppContactRole.IsPrimary==true)) ){
                        contactIds.add(oppContactRole.ContactId);
                        oppIds.add(oppContactRole.OpportunityId);                    
                    }  
                }
            }
            
            Map<ID,String> mapContact = OpportunityContactRoleSelector.getContactIdName(contactIds);
            Map<ID,Opportunity> mapOpp = OpportunityContactRoleSelector.getOpportunities(oppIds);
            if(lstNewOppContactRole!=null && !lstNewOppContactRole.isEmpty()){
                for(OpportunityContactRole oppContactRole:lstNewOppContactRole){
                    System.debug('Record Type value'+oppContactRole.Trav_Rec_Type__c.toLowerCase());
                    if(oppContactRole.IsPrimary==true){
                        opportunity op;
                        if(((oppContactRole.Trav_Rec_Type__c.equals(Constants.LICENSEDCONTACTRECORDTYPE)) || oppContactRole.Trav_Rec_Type__c.equals(Constants.LICENSEDCONTACTRECORDTYPE))){
                            op = mapOpp.get(oppContactRole.OpportunityId);
                            System.debug('adding primary agency location contact');
                            op.Trav_Primary_Contact__c=oppContactRole.ContactId+'_'+mapContact.get(oppContactRole.ContactId);
                        }
                        else if(oppContactRole.Trav_Rec_Type__c.equals(Constants.CUSTOMERCONTACT)){
                            op = mapOpp.get(oppContactRole.OpportunityId);
                            System.debug('adding primary customer contact');
                            op.Trav_Primary_Customer_Contact__c=oppContactRole.ContactId+'_'+mapContact.get(oppContactRole.ContactId);
                        }
                        lstUpdatedOpp.add(op);
                    }    
                } 
            }
            
            update lstUpdatedOpp;
        }
        catch(Exception e){
            System.debug(e.getMessage()+' '+e.getLineNumber());
            ExceptionUtility.logApexException(Constants.OPPORTUNITYCONTACTROLESERVICE, METHODNAME, e, null);
        }
        
    }*/
    
    private static String uniqueKey(OpportunityContactRole objOpportunityContRole){
        String unique;
        if(((objOpportunityContRole.Trav_Rec_Type__c.equals(Constants.LICENSEDCONTACTRECORDTYPE)) || objOpportunityContRole.Trav_Rec_Type__c.equals(Constants.NONLICENSEDCONTACTRECTYPE))){
                   unique= objOpportunityContRole.OpportunityId+'agency';
                }
                else{
                   unique= objOpportunityContRole.OpportunityId+objOpportunityContRole.Trav_Rec_Type__c;
                }
      return unique;
    }
    
    
}