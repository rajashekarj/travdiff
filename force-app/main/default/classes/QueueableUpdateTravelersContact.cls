/**
* Qeueable Class for adding opportunity owner's manager hierarchy as team members
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Shashank Agarwal       US65899              08/20/2019       Original Version
*/
public class QueueableUpdateTravelersContact implements Queueable {

    List<Contact> lstNewContact = new List<Contact>();
    Set<String> setContactEmail = new Set<String>();
    Boolean objTriggerIsExecuting;
    //constructor setting parametre values
    public QueueableUpdateTravelersContact(List<Contact> lstNewContact,Set<String> setContactEmail,Boolean objTriggerIsExecuting){
     

        this.lstNewContact = lstNewContact;
        this.setContactEmail = setContactEmail;
        this.objTriggerIsExecuting = objTriggerIsExecuting;
    }
    public void execute(QueueableContext context) { 

        
        final string METHODNAME = 'execute';
        final string CLASSNAME = 'QueueableUpdateTravelersContact';
        try{
            
            List<Contact> lstContactToUpdate = new List<Contact>();
            Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.TRAVELERSCONTACTRECORDTYPE).getRecordTypeId();
            Account objInternalAccount = new Account();
            objInternalAccount = AccountSelector.getInternalAccount();               
           
            List<User> lstUser = new List<User>();
            if(!setContactEmail.isEmpty()){
                lstUser = UserSelector.getUsersByEmail(setContactEmail);
            }
            Map<String,User> mapEmailToUserId = new  Map<String,User>();
            
            if(lstUser.size() > 0){
                for(Contact objContact : lstNewContact){
                    for(User objUser : lstUser ){
                        if( objUser.Email == objContact.Email){
                            mapEmailToUserId.put(objUser.Email,objUser);
                        }

                    }
                }
            }
            
            for(Contact objContact : lstNewContact){
                if(objContact.RecordTypeId == recordTypeId ){
                   
                    if(objTriggerIsExecuting == true){
                        objContact.AccountId =  objInternalAccount.Id;
                        if(mapEmailToUserId.containskey(objContact.Email)){
                            objContact.TRAV_User__c = mapEmailToUserId.get(objContact.Email).Id;
                        }
                    }else{
                       
                        Contact objContactToUpdate = new Contact();
                        objContactToUpdate.Id = objContact.Id;
                        objContactToUpdate.AccountId = objInternalAccount.Id;
                        if(mapEmailToUserId.containskey(objContact.Email)){
                        	objContactToUpdate.TRAV_User__c = mapEmailToUserId.get(objContact.Email).Id;
                        }

                        lstContactToUpdate.add(objContactToUpdate);
                    }
                }
            }
            
            if(!lstContactToUpdate.isEmpty() && objTriggerIsExecuting == false){

                Database.SaveResult[] lstSaveResultForUpdate = Database.update(lstContactToUpdate, false);
                ExceptionUtility.logDMLError(Constants.OPPORTUNITYLINEITEMSERVICE, METHODNAME, lstSaveResultForUpdate);
            }
            if(Test.isRunningTest()){
                throw new QueryException();
            }
        }catch(Exception objExp){
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        } //End of catch block
        
    }//End of method execute

}