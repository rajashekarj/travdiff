/**
* This class is the utility class for Contact Search and Pull Functionality
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect          Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Shashank S             US69347/US69863          11/10/2019      Original Version
* -----------------------------------------------------------------------------------------------------------
*/
public without sharing class NewContactSearchAndPullMDM{
    public static String bnmId;
    @AuraEnabled
    public static ContactSearchAndPullReturnWrapper saveContact(Contact contactRec, String recordId){
        ContactSearchAndPullReturnWrapper responseValue;
        if(recordId.substring(0, 3) == '006'){
            
            //For the given OpportunityId get the Agency Location.
            Opportunity oppRec = [SELECT Id, Name, TRAV_Agency_Broker__c, TRAV_Agency_Broker__r.TRAV_Account_External_Id__c FROM Opportunity WHERE Id=:recordId LIMIT 1];
            bnmId = oppRec.TRAV_Agency_Broker__r.TRAV_Account_External_Id__c;
            contactRec.TRAV_Agency_Broker__c = oppRec.TRAV_Agency_Broker__c;
        }
        else if(recordId.substring(0, 3) == '001'){
            Account accRec = [SELECT TRAV_Account_External_Id__c FROM Account where Id=:recordId LIMIT 1];
            bnmId = accRec.TRAV_Account_External_Id__c;
            contactRec.TRAV_Agency_Broker__c = recordId;    
        }
        //get the Agency Non Lisenced RecordType Id
        RecordType agencyNL = [SELECT Id FROM RecordType Where DeveloperName = 'TRAV_Agency_Non_Licensed_Contact' AND sObjectType = 'Contact' LIMIT 1];
        contactRec.RecordTypeId = agencyNL.Id;
        //Stamp GU Id on the contact record
        contactRec.TRAV_GU_Id__c = GUIDUtility.NewGuid();
        system.debug('contactRec'+contactRec);
        try{
            responseValue = ContactSearchPullUtility.executeContactSearchPullOperations(recordId, contactRec);
            system.debug(responseValue);
        }
        catch(Exception ex){
            system.debug(ex.getMessage());
        }        
        return responseValue;
    }
    @AuraEnabled
    public static void saveExistingContact(String wrapperString, Contact contactRec, String recordId){
        ContactSearchAndPullRetrieveWrapper contactWrapper = new ContactSearchAndPullRetrieveWrapper ();
        if(wrapperString != null && wrapperString != ''){
            //Parse the string to wrapper
            //contactWrapper = (ContactSearchAndPullRetrieveWrapper) JSON.deserialize(wrapperString, ContactSearchAndPullRetrieveWrapper.class);
        }
        if(recordId.substring(0, 3) == '006'){
            //For the given OpportunityId get the Agency Location.
            Opportunity oppRec = [SELECT Id, Name, TRAV_Agency_Broker__c FROM Opportunity WHERE Id=:recordId LIMIT 1];
            contactRec.TRAV_Agency_Broker__c = oppRec.TRAV_Agency_Broker__c;
        }
        else if(recordId.substring(0, 3) == '001'){
            contactRec.TRAV_Agency_Broker__c = recordId;    
        }
        //get the Agency Non Lisenced RecordType Id
        RecordType agencyNL = [SELECT Id FROM RecordType Where DeveloperName = 'TRAV_Agency_Non_Licensed_Contact' AND sObjectType = 'Contact' LIMIT 1];
        contactRec.RecordTypeId = agencyNL.Id;
        ContactSearchPullUtility.handleContactCreationAndAssociation(contactWrapper,'update',recordId,contactRec.TRAV_External_Id__c);
    }
    @AuraEnabled
    public static List<String> getTitleValues(){
        List<String> titleList = new List<String>(); 
        titleList.add('');
        Schema.DescribeFieldResult fieldResult = Contact.TRAV_Job_Title__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple){
			titleList.add(pickListVal.getLabel());
		}     
        return titleList;
    }
}