/**
* Test class for OpportunityService Class
*
* @Author : Deloitte Consulting
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------
* Shashank Agarwal       US69108              09/30/2019       Original Version
* -----------------------------------------------------------------------------------------------------------
*/ 

@isTest
private class TestBatchMapBAMToOpportunity {



    /**
* This method will test scenarios when opportunity is updated
* @param no parameters
* @return void
*/
    static testmethod void testUpdateBAMToOpportunity() {



        Account objProspectAccount = TestDataFactory.createProspect();
        List<Account> lstAccount = new List<Account>{objProspectAccount};
            insert lstAccount;

        Account objAgencyBM = TestDataFactory.createAgency();
        insert objAgencyBM;

        List<TRAV_BAM_Agency_Mapping__c> lstBAM = new List<TRAV_BAM_Agency_Mapping__c>();

        TRAV_BAM_Agency_Mapping__c objBAM1 = new TRAV_BAM_Agency_Mapping__c();
        objBAM1.TRAV_Business_Unit__c = 'BSI-FI';
        objBAM1.TRAV_BO_Code__c = '22';
        lstBAM.add(objBAM1);

        TRAV_BAM_Agency_Mapping__c objBAM2 = new TRAV_BAM_Agency_Mapping__c();
        objBAM2.TRAV_Business_Unit__c = 'BSI-FI';
        objBAM2.TRAV_BO_Code__c = '42';
        lstBAM.add(objBAM2);

        insert lstBAM;

        List<TRAV_Producer__c> lstProducer = new List<TRAV_Producer__c>();

        TRAV_Producer__c objProducer1 = new TRAV_Producer__c();
        objProducer1.Name = 'Producer1';
        objProducer1.TRAV_Financial_Branch_Code__c = '42';
        lstProducer.add(objProducer1);

        TRAV_Producer__c objProducer2 = new TRAV_Producer__c();
        objProducer2.Name = 'Producer2';
        objProducer2.TRAV_Financial_Branch_Code__c = '42';
        lstProducer.add(objProducer2);

        TRAV_Producer__c objProducer3 = new TRAV_Producer__c();
        objProducer3.Name = 'Producer3';
        objProducer3.TRAV_Financial_Branch_Code__c = '22';
        lstProducer.add(objProducer3);

        insert lstProducer;

        List<TRAV_Account_Producer_Relation__c> lstAccountProducerRelation = new List<TRAV_Account_Producer_Relation__c>();

        TRAV_Account_Producer_Relation__c objAccountProducerRelation1 = new TRAV_Account_Producer_Relation__c();
        objAccountProducerRelation1.TRAV_Producer__c = lstProducer[0].Id;
        objAccountProducerRelation1.TRAV_Account__c = objAgencyBM.Id;
        lstAccountProducerRelation.add(objAccountProducerRelation1);

        TRAV_Account_Producer_Relation__c objAccountProducerRelation2 = new TRAV_Account_Producer_Relation__c();
        objAccountProducerRelation2.TRAV_Producer__c = lstProducer[1].Id;
        objAccountProducerRelation2.TRAV_Account__c = objAgencyBM.Id;
        lstAccountProducerRelation.add(objAccountProducerRelation2);

        TRAV_Account_Producer_Relation__c objAccountProducerRelation3 = new TRAV_Account_Producer_Relation__c();
        objAccountProducerRelation3.TRAV_Producer__c = lstProducer[2].Id;
        objAccountProducerRelation3.TRAV_Account__c = objAgencyBM.Id;
        lstAccountProducerRelation.add(objAccountProducerRelation3);

        insert lstAccountProducerRelation;

        Opportunity objOpportunity1 = TestDataFactory.createOpptyByRecordTypeDevName('Prospect', 'Test Opp1',lstAccount[0].Id,'New Business');
        objOpportunity1.TRAV_Business_Unit__c = 'BSI-FI';
        objOpportunity1.TRAV_NA_Producer_Code__c = lstAccountProducerRelation[0].Id;
        objOpportunity1.TRAV_Agency_Broker__c = objAgencyBM.Id;
        insert objOpportunity1;

        Opportunity objOpportunity2 = TestDataFactory.createOpptyByRecordTypeDevName('Prospect', 'Test Opp2',lstAccount[0].Id,'New Business');
        objOpportunity2.TRAV_Business_Unit__c = 'BSI-FI';
        objOpportunity2.TRAV_NA_Producer_Code__c = lstAccountProducerRelation[1].Id;
        objOpportunity2.TRAV_Agency_Broker__c = objAgencyBM.Id;
        insert objOpportunity2;

        Opportunity objOpportunity3 = TestDataFactory.createOpptyByRecordTypeDevName('Prospect', 'Test Opp3',lstAccount[0].Id,'New Business');
        objOpportunity3.TRAV_Business_Unit__c = 'BSI-FI';
        objOpportunity3.TRAV_NA_Producer_Code__c = lstAccountProducerRelation[2].Id;
        objOpportunity3.TRAV_Agency_Broker__c = objAgencyBM.Id;
        insert objOpportunity3;

        List<TRAV_Producer__c> lstUpdateProducer = new List<TRAV_Producer__c>();
        TRAV_Producer__c objUpdateProducer1 = new TRAV_Producer__c();
        objUpdateProducer1.Id = lstProducer[0].Id;
        objUpdateProducer1.TRAV_Financial_Branch_Code__c = '22';
        lstUpdateProducer.add(objUpdateProducer1);

        TRAV_Producer__c objUpdateProducer2 = new TRAV_Producer__c();
        objUpdateProducer2.Id = lstProducer[1].Id;
        objUpdateProducer2.TRAV_Financial_Branch_Code__c = '';
        lstUpdateProducer.add(objUpdateProducer2);

        TRAV_Producer__c objUpdateProducer3 = new TRAV_Producer__c();
        objUpdateProducer3.Id = lstProducer[2].Id;
        objUpdateProducer3.TRAV_Financial_Branch_Code__c = '42';
        lstUpdateProducer.add(objUpdateProducer3);

        update lstUpdateProducer;

        List<TRAV_Producer__History> lstProducerHistory = new List<TRAV_Producer__History>();

        if (Test.isRunningTest()) {
            lstProducerHistory.add(new TRAV_Producer__History(Field='TRAV_Financial_Branch_Code__c',ParentId=objUpdateProducer1.Id));
            lstProducerHistory.add(new TRAV_Producer__History(Field='TRAV_Financial_Branch_Code__c',ParentId=objUpdateProducer2.Id));
            lstProducerHistory.add(new TRAV_Producer__History(Field='TRAV_Financial_Branch_Code__c',ParentId=objUpdateProducer3.Id));
            insert lstProducerHistory;
        }

		Set<Id> producerIds= new Set<Id>();
        for(TRAV_Producer__c objProd: lstProducer)
        {
            producerIds.add(objProd.Id);
        }
        AccountProducerRelationshipSelector.getAccountProducerRelationsForBAM(producerIds);

        Test.startTest();
        BatchMapBAMToOpportunity obj = new BatchMapBAMToOpportunity();
        Id batchId = DataBase.executeBatch(obj);
        Test.stopTest();


    }

    private static testMethod void testScheduler(){
        BatchMapBAMToOpportunity batchSchObj = new BatchMapBAMToOpportunity();
        String sch = '0 0 23 * * ?';
        system.schedule('ScheduleBAMBatch', sch, batchSchObj);
    }

}