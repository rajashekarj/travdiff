/**
*Test class for ContactService
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -------------------- ---------------------------------------------------------------------------------------              
* Bhanu Reddy              US62952            05/30/2019      testClass
* -----------------------------------------------------------------------------------------------------------
*/
@istest
private class TestContactService {
    static testmethod void testUpdateContactAccount(){
        //create the custom setting for opportunity trigger
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'ContactTrigger';
        insert objCustomsetting;
        
        User objUsr = TestDataFactory.createTestUser('BI - National Accounts','TestNA','LastNameNA');
        objUsr.Email = 'testemail@travelers.com';
        insert objUsr;
        Set<Id> ownerId = new Set<Id>();
        ownerId.add(objUsr.ID);
        
        ContactSelector.getContactsWithAccount(ownerId);
        //create account record
        Account objProspectAccount = TestDataFactory.createProspect(); 
        List<Account> lstAccount = new List<Account>{objProspectAccount};
        lstAccount[0].Name = Constants.InternalAccountName; 
        insert lstAccount;
        
        //create Contact record
        List<Contact> lstContact =  TestDataFactory.createTravelersContact(2);
        Map<Id,Contact> contactMap = new Map<Id,Contact>();
		contactMap = null;
        
        lstContact[0].Email ='testemail@travelers.com';
        lstContact[0].TRAV_Licensed_Individual_Indicator__c = true;
        lstContact[0].RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.TRAVELERSCONTACTRECORDTYPE).getRecordTypeId();
        insert lstContact;
        
        lstContact[0].TRAV_Licensed_Individual_Indicator__c = false;
        update lstContact;
        
        //contactMap.put(lstContact[0].Id,lstContact[0]);
        
        Test.startTest();
        ContactService.updateContactAccount(lstContact,false);
        ContactService.updateContactRecordtype(lstContact,contactMap);
        Test.stopTest();
      
        System.assertEquals([Select TRAV_User__c from Contact Where Id = :lstContact[0].Id][0].TRAV_User__c, objUsr.Id);
        System.assertEquals([Select AccountId from Contact Where Id = :lstContact[0].Id][0].AccountId, objProspectAccount.Id);
        
    }
    static testmethod void testUpdateContactAccount2(){

        User objUsr = TestDataFactory.createTestUser('BI - National Accounts','TestNA','LastNameNA');
        objUsr.Email = 'testemail@travelers.com';
        insert objUsr;
        
        Map<Id,Contact> contactMap = new Map<Id,Contact>();
        //create account record
        Account objProspectAccount = TestDataFactory.createProspect(); 
        List<Account> lstAccount = new List<Account>{objProspectAccount};
        lstAccount[0].Name = Constants.InternalAccountName; 
        insert lstAccount;
        
        //create Contact record
        List<Contact> lstContact =  TestDataFactory.createTravelersContact(1);
        lstContact[0].Email ='testemail@travelers.com';
        lstContact[0].TRAV_Licensed_Individual_Indicator__c = false;
        lstContact[0].RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.TRAVELERSCONTACTRECORDTYPE).getRecordTypeId();
        insert lstContact;
        
        Test.startTest();
        ContactService.updateContactRecordtype(lstContact,contactMap);
        ContactTriggerHandler conHandler = new ContactTriggerHandler();
        conHandler.beforeDelete(contactMap);
        conHandler.afterUndelete(contactMap);
        conHandler.afterDelete(contactMap);
        Test.stopTest();
        
        Set<String> emailSet = new Set<String>();
        emailSet.add(lstContact[0].Email);
        system.enqueueJob(new QueueableUpdateTravelersContact(lstContact, emailSet, true));
              
    }
    static testmethod void testUpdateContactAccount3(){

       Map<Id,Contact> contactMap = new Map<Id,Contact>();
        contactMap = null;
        //create account record
        Account objProspectAccount = TestDataFactory.createProspect(); 
        List<Account> lstAccount = new List<Account>{objProspectAccount};
        lstAccount[0].Name = Constants.InternalAccountName; 
        insert lstAccount;
        
        //create Contact record
        List<Contact> lstContact =  TestDataFactory.createTravelersContact(1);
        lstContact[0].Email ='testemail@travelers.com';
        lstContact[0].TRAV_Licensed_Individual_Indicator__c = true;
        lstContact[0].RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.TRAVELERSCONTACTRECORDTYPE).getRecordTypeId();
        insert lstContact;
        
        Test.startTest();
        ContactService.updateContactRecordtype(lstContact,contactMap);
        Test.stopTest();
              
    }
    
    static testMethod void testcopyBillingAdress(){
         TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'ContactTrigger';
        insert objCustomsetting;
        TestDataFactory.createGeneralTriggersetting();
        Account ObjAgency= TestDataFactory.createAgency();
        ObjAgency.BillingStreet = '12 A Banking Street';
        ObjAgency.BillingCity = 'NewYork';
        ObjAgency.BillingCountry = 'USA';
        ObjAgency.BillingPostalCode ='515291';
        insert ObjAgency;
         Contact AgencyNonLicendContact = TestDataFactory.createAgencyNonLicensed(ObjAgency.id);
        insert AgencyNonLicendContact;
        Contact objContoCheck =[select id,OtherStreet,OtherCity,OtherCountry,OtherPostalCode 
                                	from contact where id =:AgencyNonLicendContact.id];
        system.assertEquals(objContoCheck.OtherStreet,'12 A Banking Street');
        system.assertEquals(objContoCheck.OtherCity, 'NewYork');
        system.assertEquals(objContoCheck.OtherCountry, 'USA');
        system.assertEquals(objContoCheck.OtherPostalCode, '515291');
        AgencyNonLicendContact.OtherStreet = '1245 Banking street';
        update AgencyNonLicendContact;
        Contact objContoCheckafterUpdate =[select id,OtherStreet,OtherCity,OtherCountry,OtherPostalCode 
                                	from contact where id =:AgencyNonLicendContact.id];
         system.assertEquals(objContoCheckafterUpdate.OtherStreet,'12 A Banking Street');
        
    } //end  Method testcopyBillingAdress
    
    static testmethod void testCopyMobileToPhone(){
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'ContactTrigger';
        insert objCustomsetting;
        TestDataFactory.createGeneralTriggersetting();
        
        List<Contact> lstContact =  TestDataFactory.createTravelersContact(2);
        lstContact[0].FirstName = 'Contact Service Test';
        lstContact[0].Phone = NULL;
        lstContact[0].MobilePhone = '1234567890';
        lstContact[1].FirstName = 'Contact Service Test2';
        lstContact[1].Phone = '0987654321';
        lstContact[1].MobilePhone = '1234567890';
        insert lstContact;
        
        contact objContactNoPhone = [Select id, FirstName, Phone, MobilePhone from contact where FirstName = 'Contact Service Test' LIMIT 1];
        contact objContactPhone = [Select id, FirstName, Phone, MobilePhone from contact where FirstName = 'Contact Service Test2' LIMIT 1];
        
        system.assertEquals(objContactNoPhone.MobilePhone, objContactNoPhone.Phone);
        system.assertNotEquals(objContactPhone.Phone, objContactPhone.MobilePhone);
    } //end Method testCopyMobileToPhone
}