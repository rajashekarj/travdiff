@isTest(seeAllData=false)
public without sharing class TestUpdateProductNamesBatch {
    @TestSetup
    static void makeTestData(){
        Account acc = new Account();
        acc.Name = 'test account';
        insert acc;

        Product2 prod1 = new Product2();
        prod1.Name = 'ERISA Bond-ERISA';
        prod1.TRAV_Product_Abbreviation__c = 'ERISA';
        Product2 prod2 = new Product2();
        prod2.Name = 'Other Crime-OTHER CRIME';
        prod2.TRAV_Product_Abbreviation__c = 'OTHER CRIME';
        insert new List<Product2>{prod1, prod2};

        PricebookEntry entry1 = new PricebookEntry();
        entry1.UnitPrice = 500.00;
        entry1.IsActive = true;
        entry1.Pricebook2Id = Test.getStandardPricebookId();
        entry1.Product2Id = prod1.Id;
        //entry1.ProductCode = '14';
        PricebookEntry entry2 = new PricebookEntry();
        entry2.UnitPrice = 500.00;
        entry2.IsActive = true;
        entry2.Pricebook2Id = Test.getStandardPricebookId();
        entry2.Product2Id = prod2.Id;
        //entry2.ProductCode = '21';
        insert new List<PricebookEntry>{entry1, entry2};

        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.Name = 'Test Opportunity';
        opp.StageName = 'Closed Won';
        opp.CloseDate = System.today();
        opp.Pricebook2Id = Test.getStandardPricebookId();
        insert opp;

        OpportunityLineItem item1 = new OpportunityLineItem();
        item1.Product2Id = prod1.Id;
        item1.OpportunityId = opp.Id;
        OpportunityLineItem item2 = new OpportunityLineItem();
        item2.Product2Id = prod2.Id;
        item2.OpportunityId = opp.Id;
        insert new List<OpportunityLineItem>{item1, item2};
    }

    @isTest static void testBatchExecution() {
        Test.startTest();
        Database.executeBatch(new UpdateProductNamesBatch());
        Test.stopTest();

        List<Opportunity> updatedOpps = [SELECT TRAV_Product_Names_Abr__c FROM Opportunity]; 
        System.assertEquals('ERISA;OTHER CRIME', updatedOpps.get(0).TRAV_Product_Names_Abr__c);
    } 
}