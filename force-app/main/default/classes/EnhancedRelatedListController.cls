/**
 * This class is used as a The controller for the EnhancedRelatedListViewAll component
 *
 *
 * @Author Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Siddharth M             US70371             12/10/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
public without sharing class EnhancedRelatedListController {
    
    /**
   * This is a method which is used to get Related List data(column names and row data)
   * @param String objAPIName: API Name of Related List Object, String sortCriteria: sorting criteria, 
   * String filterCriteria: filter criteria, string recordId: Id of current record,
   * String parentLookup: Parent Object Lookup Field, String relatedListName: Name of the Related List
   * @return TableDataWrapper wrapper of the returned records.
   */
    @AuraEnabled
    public static TableDataWrapper getData(String objAPIName, String sortCriteria, String filterCriteria, 
                                           string recordId, String parentLookup, String relatedListName,Integer pageNumber)
    {
        recordId='\''+recordId+'\'';
        String columns='';
        String query;
        List<String> listColumns= new List<String>();
        List<String> listColumnValues= new List<String>();
        for(CMTD__EnhancedRelatedList__mdt objData:[SELECT Id,MasterLabel,CMTD__Field_API_Name__c,CMTD__Order__c 
                                                    FROM CMTD__EnhancedRelatedList__mdt
                                                    WHERE CMTD__Related_List_Name__c=:relatedListName Order by CMTD__Order__c])
        {
            if(objData.CMTD__Field_API_Name__c.contains('Id.'))
            {
                columns+= objData.CMTD__Field_API_Name__c.replace('Id.','.') +',';
                listColumns.add(objData.MasterLabel);
                listColumnValues.add(objData.CMTD__Field_API_Name__c.replace('Id.','.'));
            }
            else{
                columns+= objData.CMTD__Field_API_Name__c +',';
                listColumns.add(objData.MasterLabel);
                listColumnValues.add(objData.CMTD__Field_API_Name__c);
            }
        }
        columns= columns.substring(0, columns.length() -1);
        query= 'SELECT Id,'+ columns;
        
        query+=' FROM '+objAPIName+ 
            ' WHERE '+parentLookup+'='+recordId;
        if(filterCriteria!= null){
            query+=' AND ('+ filterCriteria + ')';
        }
        if(sortCriteria!=null){
            query+=' Order by '+ sortCriteria +' NULLS LAST';
        }
        
        query+=' Limit 50 Offset '+ String.valueOf(pageNumber*50);
        List<SObject> listResult= database.query(query);
        
        List<List<String>> values= new List<List<String>>();
        for(SObject objSObject: listResult)
        {
            List<String> fieldValues= new List<String>();
            for(String fVal:listColumnValues)
            {
                if(fVal.contains('.'))
                {
                    
                    List<String> lstfVal = fVal.split('\\.');
                    
                    SObject obj=objSObject;
                    Object obj1;
                    Integer flag=0;
                    for(String colName:lstfVal)
                    {
                       
                        if(flag==0 && obj.getSObject(colName)!=null){
                            
                            obj= obj.getSObject(colName);
                            flag=1;
                            }
                        else if( flag==1 && obj.get(colName)!=null){
                           
                            obj1= obj.get(colName);
                            }
                        else
                            obj1='';
                        
                    }
                   
                    if(String.valueOf(obj1).contains('00:00:00'))
                        fieldValues.add(String.valueOf(obj1).substringBefore('00:00:00'));
                    else
                        fieldValues.add(String.valueOf(obj1));
                    
                }else{
                    if(objSObject.get(fVal)!=null){
                        if(String.valueOf(objSObject.get(fVal)).contains('00:00:00'))
                            fieldValues.add(String.valueOf(objSObject.get(fVal)).substringBefore('00:00:00'));
                        else
                            fieldValues.add(String.valueOf(objSObject.get(fVal)));
                    }
                    else
                    {
                        fieldValues.add('');
                    }
                }
            }
            fieldValues.add(String.valueOf(objSObject.get('Id')));
            values.add(fieldValues);
        }
        TableDataWrapper objTDW= new TableDataWrapper();
        objTDW.lstColumnLabels=listColumns;
        objTDW.lstValues= values;
        objTDW.lstColumnValues=listColumnValues;
        return objTDW;
    }
    
    /**
   * This is a method which is used to get Current User's Profile Name
   * @param null
   * @return String : Profile Name String
   */
    @AuraEnabled
    public static String getUserProfile(){
        
        String ProfileName= [Select Id, Profile.Name from User where id=: userInfo.getUserId() Limit 1].Profile.Name;
        return ProfileName;
    }
    
     /**
   * This is a method which is used to get Parent Record's Name
   * @param null
   * @return String : Parent Record Name String
   */
    @AuraEnabled
    public static String getParentRecord(String recordId, String sOName){
        
        String query='Select Id, Name from '+sOName+' where Id='+'\''+ recordId+'\''+' Limit 1';
        SObject obj= database.query(query);
        return (String)obj.get('Name');
    }
    
    /**
   * This is a method which is used to delete record from the List View
   * @param null
   * @return null
   */
    @AuraEnabled
    public static void deleteRecord(String recordId)
    {
        Database.delete(recordId);
        
    }
    
    /**
   * This is a wrapper Class to return Data
   */
    public class TableDataWrapper {
        @AuraEnabled
        public List<String> lstColumnLabels { get;set; }
        @AuraEnabled
        public List<String> lstColumnValues { get;set; }
        @AuraEnabled
        public List<List<String>> lstValues { get;set; }
    } 
    
}