global class OneTimeDataLoad_Opportunity_BAM implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String strQuery = 'select id, TRAV_NA_Producer_Code__r.TRAV_Producer__c from opportunity where TRAV_Source_Sys_Code__c = \'Polaris\' ';

        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
        OpportunityService.updateBAMToOpportunity((List<Opportunity>)scope,null);
    }

    global void finish(Database.BatchableContext BC) {
       
     
    }
}