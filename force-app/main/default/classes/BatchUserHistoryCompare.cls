global class BatchUserHistoryCompare implements Database.Batchable<SObject>, Schedulable
{
    /*
    * 1- Retreive Custom Settings for the last run date 
    *  Retreive Modified Users
    * Get history record for modified users
    * Compare modified details against History 
    * 2- Get the lastest recrod for each user
    * 3- Compare between two records
    * 4- Update the latest record with changes.
    */
    private String Query; 
    private DateTime last_run;
    private String Change_description;
    global final datetime lastrun2;
    public BatchUserHistoryCompare() {
        //fields= 'id,name,IsActive,city,TRAV_BU__c,TRAV_BSI_Customer_Segment__c,Department,Division,TRAV_External_Id__c,TRAV_Isfrozen__c,Manager.name,MobilePhone,profile.name,TRAV_Region__c,UserRole.name,Title,TimeZoneSidKey,CompanyName,email';
        
        lastrun2=Datetime.now();
    }
     /**
        * This method execute this batch class when scheduled
        * @param SchedulableContext record 
        * @return void 
        */
    global void execute(SchedulableContext schCon) {
    BatchUserHistoryCompare  batchObj  = new BatchUserHistoryCompare (); 
    Database.executebatch(batchObj);
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        Renewal_Batch__c objBamBatch = Renewal_Batch__c.getOrgDefaults();
        last_run= objBamBatch.TRAV_Last_Successful_User_Batch_Run__c;
        if (last_run == null)
        {
            last_run = DateTime.newInstance(2018, 2, 29, 18, 2, 3);
        }
        
        
        Query='select id,username,name,IsActive,city,TRAV_BU__c,TRAV_BSI_Customer_Segment__c,Department,Division,TRAV_External_Id__c,TRAV_Isfrozen__c,Manager.name,MobilePhone,profile.name,TRAV_Region__c,UserRole.name,Title,TimeZoneSidKey,CompanyName,email,createddate,lastmodifiedby.name,lastmodifieddate,TRAV_Override_Automation__c,TRAV_Has_Left_Company__c,TRAV_Manager__c from user where lastmodifieddate > '+ last_run.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');   
        system.debug('Query'+Query);
        return Database.getQueryLocator(Query);
     }
  
     global void execute(Database.BatchableContext BC, List<User> scope){
        Map<String, USer> modified_users = new Map<String, User>();
        Map<String, TRAV_User_History_Snapshot__c> history_users = new Map<String, TRAV_User_History_Snapshot__c>();
        List<TRAV_User_History_Snapshot__c>   new_user_list = new list<TRAV_User_History_Snapshot__c>();
        Renewal_Batch__c objBamBatch = Renewal_Batch__c.getOrgDefaults();
        last_run= objBamBatch.TRAV_Last_Successful_User_Batch_Run__c;
        if (last_run == null)
        {
            last_run = DateTime.newInstance(2018, 2, 29, 18, 2, 3);
        }
         for(User s : scope)
         {
            modified_users.put(s.username,s);
            if(s.createddate > last_run)
            {
                //create new user
                new_user_list.add(CreateUSerHistory(s,true));
            }
         }
         List<TRAV_User_History_Snapshot__c>  usershistory = [select TRAV_Username__c,TRAV_Name__c,TRAV_User_Active__c,TRAV_City__c,TRAV_USER_BU__c,TRAV_Customer_Segment__c,TRAV_Department__c,TRAV_Division__c,TRAV_External_Id__c,TRAV_User_Frozen__c,TRAV_Manager__c,TRAV_Mobile__c,TRAV_Profile__c,TRAV_Region__c,TRAV_Role__c,TRAV_Title__c,TRAV_Timezone__c,TRAV_Company__c,TRAV_Email__c,TRAV_Override_Automation__c,TRAV_Has_Left_Company__c,TRAV_Is_Manager__c from TRAV_User_History_Snapshot__c where TRAV_Username__c IN :modified_users.keyset() ORDER BY createddate];
         for(TRAV_User_History_Snapshot__c s : usershistory)
        {
     	    //s.put(Field,Value); 
     	    history_users.put(s.TRAV_Username__c,s);
         } 
         for(TRAV_User_History_Snapshot__c s : history_users.values())
         {
            TRAV_User_History_Snapshot__c changeduser = compareUserDetails(modified_users.get(s.Trav_username__c) ,s);
            if(changeduser != null)
            {
                new_user_list.add(changeduser);
            }
         } 
         insert new_user_list;
           
      }
  
     global void finish(Database.BatchableContext BC)
     {
        DateTime dtTo = DateTime.now();
        Renewal_Batch__c objBamBatch = Renewal_Batch__c.getOrgDefaults();
        objBamBatch.TRAV_Last_Successful_User_Batch_Run__c = dtTo;
        upsert objBamBatch;
     }
     private Boolean isChanged(String Source, String Target,String fieldname,String description)
     {
         
        if(Source ==null && Target!=null) return true;
        if(Target == null) return false; 
        if(!Target.equalsIgnoreCase(Source))  
         {
            Change_description += fieldname + ' Changed from ' + Source + ' to ' + Target;
            return true;
         }
         else 
            return false;
     }
     private Boolean isChanged(boolean Source, boolean Target,String fieldname,String description)
     {
         Boolean v1 = Target;
         if(!Source==v1)
         {
            Change_description += fieldname + ' Changed from ' + Source + ' to ' + Target;
            return true;
         }
         else 
            return false;
     }
     private TRAV_User_History_Snapshot__c compareUserDetails(User modifieduser, TRAV_User_History_Snapshot__c userhistory)
     {
         Change_description = '';
         TRAV_User_History_Snapshot__c createuser= CreateUSerHistory(modifieduser,false);

         createuser.TRAV_Changed_Name__c = isChanged(userhistory.TRAV_Name__c, modifieduser.name,'Name',Change_description);
         createuser.TRAV_Changed_City__c = isChanged(userhistory.TRAV_City__c,modifieduser.City,'City',Change_description);
         createuser.TRAV_Changed_BU__c = isChanged(userhistory.TRAV_USER_BU__c,modifieduser.TRAV_BU__c,'BU',Change_description);
         createuser.TRAV_Changed_Customer_Segment__c = isChanged(userhistory.TRAV_Customer_Segment__c,modifieduser.TRAV_BSI_Customer_Segment__c,'Customer Segment',Change_description);
         createuser.TRAV_Changed_Department__c = isChanged(userhistory.TRAV_Department__c,modifieduser.Department,'Department',Change_description);
         createuser.TRAV_Changed_Division__c = isChanged(userhistory.TRAV_Division__c,modifieduser.Division,'Divsion',Change_description);
         createuser.TRAV_Changed_External_id__c = isChanged(userhistory.TRAV_External_Id__c,modifieduser.TRAV_External_Id__c,'External ID',Change_description);
         createuser.TRAV_Changed_Manager__c = isChanged(userhistory.TRAV_Manager__c,modifieduser.Manager.name,'Manager',Change_description);
         createuser.TRAV_Changed_Mobile__c = isChanged(userhistory.TRAV_Mobile__c,modifieduser.MobilePhone,'Mobile',Change_description);
         createuser.TRAV_Changed_Profile__c = isChanged(userhistory.TRAV_Profile__c,modifieduser.profile.name,'Profile',Change_description);
         createuser.TRAV_Changed_Region__c = isChanged(userhistory.TRAV_Region__c,modifieduser.TRAV_Region__c,'Region',Change_description);
         createuser.TRAV_Changed_Role__c = isChanged(userhistory.TRAV_Role__c,modifieduser.UserRole.name,'Role',Change_description);
         createuser.TRAV_Changed_Title__c = isChanged(userhistory.TRAV_Title__c,modifieduser.Title,'Title',Change_description);
         createuser.TRAV_Changed_Timezone__c = isChanged(userhistory.TRAV_Timezone__c,modifieduser.TimeZoneSidKey,'Timezone',Change_description);
         createuser.TRAV_Changed_Company__c = isChanged(userhistory.TRAV_Company__c,modifieduser.CompanyName,'Company',Change_description);
         createuser.TRAV_Changed_Email__c =  isChanged(userhistory.TRAV_Email__c,modifieduser.email,'Email',Change_description);
         boolean left_company = isChanged(userhistory.TRAV_Has_Left_Company__c,modifieduser.TRAV_Has_Left_Company__c,'Left Company',Change_description);
        boolean override_automation = isChanged(userhistory.TRAV_Override_Automation__c,modifieduser.TRAV_Override_Automation__c,'Override Automation',Change_description);
        createuser.TRAV_Change_Is_Manager__c = isChanged(userhistory.TRAV_Is_Manager__c,modifieduser.TRAV_Manager__c,'Is Manager',Change_description);
        

        
        createuser.TRAV_Active_User__c = isChanged(userhistory.TRAV_User_Active__c,modifieduser.isActive,'Active',Change_description); 
        createuser.TRAV_Frozen_User__c = isChanged(userhistory.TRAV_User_Frozen__c,modifieduser.TRAV_Isfrozen__c,'Frozen',Change_description); 
        createuser.TRAV_Change_description__c = Change_description;

        List<boolean> ischanged = new List<boolean>();
        ischanged.add(createuser.TRAV_Changed_Name__c);
        ischanged.add(createuser.TRAV_Changed_City__c);
        ischanged.add(createuser.TRAV_Changed_BU__c);
        ischanged.add(createuser.TRAV_Changed_Customer_Segment__c);
        ischanged.add(createuser.TRAV_Changed_Department__c);
        ischanged.add(createuser.TRAV_Changed_Division__c);
        ischanged.add(createuser.TRAV_Changed_External_id__c);
        ischanged.add(createuser.TRAV_Changed_Manager__c);
        ischanged.add(createuser.TRAV_Changed_Mobile__c);
        ischanged.add(createuser.TRAV_Changed_Profile__c);
        ischanged.add(createuser.TRAV_Changed_Region__c);
        ischanged.add(createuser.TRAV_Changed_Role__c);
        ischanged.add(createuser.TRAV_Changed_Title__c);
        ischanged.add(createuser.TRAV_Changed_Timezone__c);
        ischanged.add(createuser.TRAV_Changed_Company__c);
        ischanged.add(createuser.TRAV_Changed_Email__c);
        ischanged.add(createuser.TRAV_Active_User__c);
        ischanged.add(createuser.TRAV_Frozen_User__c);
        ischanged.add(createuser.TRAV_Change_Is_Manager__c);
        ischanged.add(left_company);
        ischanged.add(override_automation);
                
       if (ischanged.contains(true) )
       {
         return createuser;
       }
        return null;
     }
     private TRAV_User_History_Snapshot__c CreateUSerHistory(USer modified,boolean newuser)
     {
        TRAV_User_History_Snapshot__c createuser = new TRAV_User_History_Snapshot__c();
        
        createuser.TRAV_Username__c = modified.username;
        createuser.TRAV_Name__c = modified.name;
        createuser.TRAV_User_Active__c = modified.isActive;
        createuser.TRAV_City__c = modified.City;
        createuser.TRAV_USER_BU__c = modified.TRAV_BU__c;
        createuser.TRAV_Customer_Segment__c = modified.TRAV_BSI_Customer_Segment__c;
        createuser.TRAV_Department__c = modified.Department;
        createuser.TRAV_Division__c = modified.Division;
        createuser.TRAV_External_Id__c = modified.TRAV_External_Id__c;
        createuser.TRAV_User_Frozen__c = modified.TRAV_Isfrozen__c;
        createuser.TRAV_Manager__c = modified.Manager.name;
        createuser.TRAV_Mobile__c = modified.MobilePhone;
        createuser.TRAV_Profile__c = modified.profile.name;
        createuser.TRAV_Region__c = modified.TRAV_Region__c;
        createuser.TRAV_Role__c = modified.UserRole.name;
        createuser.TRAV_Title__c = modified.Title;
        createuser.TRAV_Timezone__c = modified.TimeZoneSidKey;
        createuser.TRAV_Company__c = modified.CompanyName;
        createuser.TRAV_Email__c = modified.email;
        createuser.TRAV_Modified_by__c = modified.lastmodifiedby.name;
        createuser.TRAV_Modified_On__c = modified.lastmodifieddate;
        createuser.TRAV_Has_Left_Company__c = modified.TRAV_Has_Left_Company__c;
        createuser.TRAV_Override_Automation__c = modified.TRAV_Override_Automation__c;
        createuser.TRAV_Is_Manager__c= modified.TRAV_Manager__c;
        createuser.TRAV_New_User__c = newuser;

         return createuser;
     }


}