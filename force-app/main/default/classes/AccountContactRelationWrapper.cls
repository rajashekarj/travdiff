/**
*    @author Bharat Madaan
*    @date   09/05/2019 
*    @description : This is the wrapper structure for AccountContactRelation object records

Modification Log:
------------------------------------------------------------------------------------
Developer                       Date                Description
------------------------------------------------------------------------------------
Bharat Madaan               09/05/2019          Original Version*
*/
public class AccountContactRelationWrapper {
    public String strUniqueId{get;set;}
    public  Id acrId{get;set;}
    public  Id accountId{get;set;}
    public  Id contactId{get;set;}
    public Boolean isInsert{get;set;}
    public Boolean isDelete{get;set;}
}