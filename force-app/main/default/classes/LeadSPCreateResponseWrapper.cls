/**
* This class is the Wrapper Class for Storing Response from Create Wrapper
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect                  Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty        US73994/US73995/US73996          24/01/2020      Original Version
* -----------------------------------------------------------------------------------------------------------
*/
public class LeadSPCreateResponseWrapper {
    @auraEnabled
    public String Party{get;set;}
    @auraEnabled
    public String RowId{get;set;}
    @auraEnabled
    public String MdmId{get;set;}
    
}