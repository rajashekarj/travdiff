@isTest
public class TestOpportunityContactRoleService {
    
    static testmethod void testOpportunityContactRole() {
        //create the custom setting for opportunity trigger
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'OpportunityContactRoleTrigger';
        insert objCustomsetting;
        
        //create the custom setting for general data setting
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Written,Declined,Quote Unsuccessful';
        insert objGeneralDataSetting;
        
        
        
        User objUser = TestDataFactory.createTestUser('BI - National Accounts','ndaigneau@deloitte.com.nauser','LastNameNA123456');
        objUser.TRAV_BU__c='BI-NA';
        objUser.TRAV_Region__c='NorthMetro';
        insert objUser;
        System.runAs ( objUser ) {
            
            Account objProspectAccount = TestDataFactory.createProspect();
            Account agency = TestDataFactory.createAgency();
            List<Account> lstAccount = new List<Account>{objProspectAccount,agency};
           
            insert lstAccount;
            
            Test.startTest();
            List<Opportunity> objOpp= TestDataFactory.createOpportunities(1, objProspectAccount.Id,'BI-NA');
            objOpp[0].TRAV_Owner_Region__c='NorthMetro';
            objOpp[0].TRAV_Agency_Broker__c=agency.id;
            System.debug(objOpp);
            insert objOpp;
            
            contact cont= TestDataFactory.createAgencyNonLicensed(objProspectAccount.Id);
            insert cont;
            
            List<OpportunityContactRole> listAgencylocationoppCntactRole = TestDataFactory.createOpportunityContactRole(2,objOpp[0].id,cont.id);
            listAgencylocationoppCntactRole[0].isPrimary=true;
            listAgencylocationoppCntactRole[1].isPrimary=true;
            
            insert listAgencylocationoppCntactRole;
            
            listAgencylocationoppCntactRole[0].isPrimary=true;
            update listAgencylocationoppCntactRole[0];
            
            OpportunityContactRole oppContactRole=[SELECT Id,isPrimary from OpportunityContactRole where id=:listAgencylocationoppCntactRole[0].id];
            System.assertEquals(oppContactRole.isPrimary, true);
            Test.stopTest();
        }
    }
    
    
}