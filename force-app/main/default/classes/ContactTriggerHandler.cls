/**
 * Handler Class for Contact Trigger. This class won't have any logic and
 * will call other Opportunity Service/Utilities class.
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shashank Agarwal       US65899             08/20/2019        Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
public class ContactTriggerHandler implements ITriggerHandler{
    //Handler Method for Before Insert.
    public void beforeInsert(List<sObject> newItems) {
          ContactService.updateContactAccount((List<Contact>)newItems,Trigger.isExecuting);
          ContactService.updateContactRecordtype((List<Contact>)newItems,null);
           ContactService.copyBillingAdress((List<Contact>)newItems,null);
          ContactService.copyMobileToPhone((List<Contact>)newItems,null);
    }
    
    //Handler Method for Before Update.
    public void beforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
       List<Contact> lstNewContact = newItems.values();
       ContactService.updateContactRecordtype(lstNewContact, (Map<Id, Contact>)oldItems);

       ContactService.updateContactINMDM(lstNewContact, (Map<Id, Contact>)oldItems);

        ContactService.copyBillingAdress(lstNewContact,(Map<Id, Contact>)oldItems);
        ContactService.copyMobileToPhone(lstNewContact,(Map<Id, Contact>)oldItems);

    }

    //Handler Method for Before Delete.    
    public void beforeDelete(Map<Id, sObject> oldItems) {
        
    }
    
    //Handler method for After Insert.
    public void afterInsert(Map<Id, sObject> newItems) {
       
    }

    //Handler method for After Update.    
    public void afterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) { 
       
    }

    //Handler method for After Delete    
    public void afterDelete(Map<Id, SObject> oldItems) {
       
    }
    
    //Handler method for After Undelete.
    public void afterUndelete(Map<Id, sObject> oldItems) {
    
    }
}