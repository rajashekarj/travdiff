/**
 * Handler Class for Opportunity Trigger. This class won't have any logic and
 * will call other Opportunity Service/Utilities class.
 * 
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------
 * Isha Shukla            US59898            05/30/2019       Added code in afterInsert and afterUpdate
 * Isha Shukla            US60806            03/06/2019       Added method calling of updateAccountTeamMembers
 * Shashank Agarwal       US59536            06/10/2019       Added code in beforeInsert and beforeUpdate
 * Shashank Agarwal       US59903            06/19/2019       Added code in afterInsert and afterUpdate
 * Bharat Madaan          US62552            06/20/2019       Added code in afterInsert and afterUpdate
 * Shashank Shastri                          06/26/2019       Added code in afterInsert and afterUpdate to create Notifications
 * Shruti Gupta           US63527            07/11/2019       Added code in bedoreInsert and beforeUpdate
 * Isha Shukla            US63800            07/15/2019       Added method call for addManagersInOpportunityTeam in after insert and update
 * Bharat Madaan          US63301            07/16/2019       Added method call for updateDeferredOpportunities in after insert and update
 * Shashank Agarwal       US63871            07/29/2019       Added Method addErrorOnPopentialPremium in before update
 * Tejal Pradhan          US65055            08/05/2019       Added method call for defaultExpirationDate in beforeInsert and beforeUpdate
 * Shashank Agarwal       US64825            08/08/2019       Added method call for addErrorIfTpaNotApproved in before update
 * Bhanu Reddy              NA               08/13/2019       Commented changeNAOpptyStageToWritten method
 * Shruti Gupta             NA               08/20/2019       Added method call for setMultiYearDealIndictor
 * Sayanka Mohanty        US66430            09/05/2019       Added method for New Business Transactions
 * Derek Manierre         US67748            09/12/2019       Added code in beforeInsert
 * Isha Shukla           US67760             09/20/2019     Added method call for updateOpportunityProductCloseDate
 * Isha Shukla           US69459             10/16/2019     Added method call for setBlankOpportunityProductDetails
 * Tejal Pradhan          US73964            02/24/2020     Added method call for stampProspectEffectiveDateForBSI
 *Ben Climan            US76573             02/13/2020       Updated beforeInsert and beforeUpdate methods - don't set timestamp for CB records
 * Evan Wilcher           US78123            03/02/2020       Updated selected methods to omit Certified Business Opportunities from OpportunityService method calls.
 * Kundan Shaw          US74801             03/06/2020          Added Method assignCreditAnalyst
     
 * -----------------------------------------------------------------------------------------------------------
 */
public class OpportunityTriggerHandler implements ITriggerHandler{
    public static boolean isOpportunityTriggerContext = false;
    public static boolean userProfileMatched  = OpportunityService.checkMethodAccessByProfile('Data Integration');
    private List<Opportunity> nonCbOpps = new List<Opportunity>();
    private Map<Id, Opportunity> oldNonCbOppsMap = new Map<Id, Opportunity>();
    
    //Updated for US78123 by Evan Wilcher 03/04/2020
    //Added List<Opportunity> nonCbOpps
    public OpportunityTriggerHandler(List<Opportunity> newItems, List<Opportunity> oldItems){
        if (newItems != null){
            for (Opportunity opp : newItems) {
                if (opp.TRAV_Direct_Source_Sys_Code__c != 'CB MDM') {
                    nonCbOpps.add(opp);
                }
            }
        }
        
        
        if (oldItems != null){
            for (Opportunity opp : oldItems){
                if (opp.TRAV_Direct_Source_Sys_Code__c != 'CB MDM'){
                    oldNonCbOppsMap.put(opp.Id, Opp);
                }
            }
        }
    }
    
    //system.debug('**User Profile Matched?**' + userProfileMatched);
    //Handler Method for Before Insert.
    public void beforeInsert(List<sObject> newItems) {
        //isOpportunityTriggerContext = true; [Commenting as opportunity team trigger is needed on insert of opportunity.]
        //OpportunityService.changeNAOpptyStageToWritten((List<Opportunity>)newItems);
        
        if(!userProfileMatched){
        system.debug('**BEFORE INSERT**');
        OpportunityService.opportunityNamingConvention(nonCbOpps, null); // Ignore CB opportunities
        //Added for US65055
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.defaultExpirationDate(nonCbOpps, NULL);
        //Added for US63527
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.defaultRenewalCreationDate(nonCbOpps, NULL);
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.stampFieldsFromUserToOpportunity(nonCbOpps, NULL);
        //Setting multi year deal indicator for first opportunity
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.setMultiYearDealIndictor(nonCbOpps, NULL);
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.clearMultiYearDeal(nonCbOpps, NULL);
        //OpportunityService.updateSubmissionPremiumOnProstectAndSubmissionStage((List<Opportunity>)newItems,NULL);
        OpportunityService.assignCOEType((List<Opportunity>)newItems);

        }//Added for US73964
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.stampProspectEffectiveDateForBSI(nonCbOpps,NULL);
        
        OpportunityService.updateStageChangeTimeStamp(nonCbOpps,NULL); // Ignore CB opportunities
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.roundOffEstimatedRevenue(nonCbOpps,NULL);
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.updateBAMToOpportunity(nonCbOpps,null);
        OpportunityService.populateSandPDataToOpportunityOnCreation((List<Opportunity>)newItems);
    }

    //Handler Method for Before Update.
    public void beforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
        List<Opportunity> lstNewOpportunity = newItems.values();
        
        // Ignore timestamping CB records
        OpportunityService.updateUnderReviewPopUpCheckbox(nonCbOpps);
        if(!userProfileMatched){
        //isOpportunityTriggerContext = true; [Commenting as opportunity team trigger is needed when Opportunity is inserted or updated]

        OpportunityService.opportunityNamingConvention(nonCbOpps, (Map<Id, Opportunity>)oldItems); // Ignore naming CB opportunities
        //Added for US65055
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.defaultExpirationDate(nonCbOpps, (Map<Id, Opportunity>)oldItems);
        //Added for US63527
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.defaultRenewalCreationDate(nonCbOpps, (Map<Id, Opportunity>)oldItems);
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.clearMultiYearDeal(nonCbOpps, (Map<Id, Opportunity>)oldItems);
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        //Setting multi year deal indicator for first opportunity
        OpportunityService.setMultiYearDealIndictor(nonCbOpps, (Map<Id, Opportunity>)oldItems);
        
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.addErrorIfTpaNotApproved(nonCbOpps, (Map<Id, Opportunity>)oldItems);
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.addErrorIfWinningTPAEmpty(nonCbOpps, (Map<Id, Opportunity>)oldItems);
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.stampFieldsFromUserToOpportunity(nonCbOpps, (Map<Id, Opportunity>)oldItems);
      //  OpportunityService.changeNAOpptyStageToWritten(lstNewOpportunity);
       // OpportunityService.updateSubmissionPremiumOnProstectAndSubmissionStage(lstNewOpportunity,(Map<Id, Opportunity>)oldItems);

        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.stageUpdateSubmissionToProspect(nonCbOpps, (Map<Id, Opportunity>)oldItems);
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.addErrorOnBSINonEditableFields(nonCbOpps, (Map<Id, Opportunity>)oldItems);
        }
        OpportunityService.updateStageChangeTimeStamp(nonCbOpps, (Map<Id, Opportunity>)oldItems); // Ignore naming CB opportunities
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.roundOffEstimatedRevenue(nonCbOpps, (Map<Id, Opportunity>)oldItems);
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.addOldTeamMemberToConstantList(nonCbOpps, (Map<Id, Opportunity>)oldItems);
        //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.updateBAMToOpportunity(nonCbOpps, (Map<Id, Opportunity>)oldItems);
        //Added for US73964
        OpportunityService.stampProspectEffectiveDateForBSI(lstNewOpportunity, (Map<Id, Opportunity>)oldItems);
        //Added for US77178
        if(userProfileMatched){
        OpportunityService.stageUpdateUnderReviewToSubmission(lstNewOpportunity, (Map<Id, Opportunity>)oldItems);
            }
    
    }
    //Handler Method for Before Delete.
    public void beforeDelete(Map<Id, sObject> oldItems) {

    }

    //Handler method for After Insert.
    public void afterInsert(Map<Id, sObject> newItems) {
        //isOpportunityTriggerContext = true;
        List<Opportunity> lstNewOpportunity = newItems.values();
         AgencyService.rollupOpportuntiyTOAgency(lstNewOpportunity,null);
        if(!userProfileMatched){

    //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        //This is method adds opportunity owner to it's opportunity team
        OpportunityService.addOwnerToOpportunityTeam(nonCbOpps, null);
       // OpportunityService.createOpportunityOnDefer(lstNewOpportunity, null);
        }
    //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.createNBOpptyAndSMList(nonCbOpps, null);
        //This is method adds opportunity owner's associated record to Traveler contacts
    //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.addOwnerAsTravelersContact(nonCbOpps, null);
        //execute the scheduled action to Generate the Notification
        if(!System.isBatch()){
            System.enqueueJob(new NotificationUtility(newItems.values(), null, 'Opportunity'));
        }
        //this method is used to add opportunity owner's manager hierarchy in opportunity team
    //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.addManagersInOpportunityTeam(nonCbOpps,null);
    }

    //Handler method for After Update.
    public void afterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
       isOpportunityTriggerContext = true;
        List<Opportunity> lstNewOpportunity = newItems.values();
        
        if(!userProfileMatched){
        //This is method adds opportunity owner to it's opportunity team

       // OpportunityService.createOpportunityOnDefer(lstNewOpportunity, (Map<Id, Opportunity>)oldItems);
       //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.addOwnerToOpportunityTeam(nonCbOpps, (Map<Id, Opportunity>)oldItems);
    //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        OpportunityService.updateDeferredOpportunities(nonCbOpps,(Map<Id, Opportunity>)oldItems);
    //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        //This method will assign opportunity close date to related opportunity products
        OpportunityService.updateOpportunityProductCloseDate(nonCbOpps,(Map<Id, Opportunity>)oldItems);
    //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
       //US69459 -This method will set blank values to opportunity product fields which displays on closure pop when stage moved back to propose
        OpportunityService.setBlankOpportunityProductDetails(nonCbOpps,(Map<Id, Opportunity>)oldItems);
        }
    //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        //This method transfers Team Members when Account is updated from opportunity
        OpportunityService.updateAccountTeamMembers(nonCbOpps, (Map<Id, Opportunity>)oldItems);
        //execute the scheduled action to Generate the Notification
        if(!System.isBatch()){
            System.enqueueJob(new NotificationUtility(newItems.values(), oldItems, 'Opportunity'));
        }
    //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        //this method is used to add opportunity owner's manager hierarchy in opportunity team
        OpportunityService.addManagersInOpportunityTeam(nonCbOpps,(Map<Id, Opportunity>)oldItems);
    //Updated for US78123 by Evan Wilcher 03/02/2020
        //Changed (List<Opportuity>)newItems to nonCbOpps
        //This is method adds opportunity owner's associated record to Traveler contacts
        OpportunityService.addOwnerAsTravelersContact(nonCbOpps, (Map<Id, Opportunity>)oldItems);
        OpportunityService.insertOldOptyTeamMembers(oldNonCbOppsMap);
        OpportunityService.assignCreditAnalyst(nonCbOpps, (Map<Id, Opportunity>)oldItems);

    }
    //Handler method for After Delete
    public void afterDelete(Map<Id, sObject> oldItems) {

    }

    //Handler method for After Undelete.
    public void afterUndelete(Map<Id, sObject> oldItems) {

    }

}