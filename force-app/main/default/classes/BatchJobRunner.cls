public without sharing class BatchJobRunner {
    private class BatchJobRunnerException extends Exception {}

    public static void runJobsToCompletion(String jobName, String jobForObject, List<String> reportToEmails, Integer scopeSize) {
        List<ApexClass> classes = (List<ApexClass>)Database.query('SELECT Name FROM ApexClass WHERE Name LIKE \'%' + jobName + '%\'');
        if (classes.size() == 0 || (classes.size() == 1 && classes.toString().toLowerCase().contains('test'))) {
            throw new BatchJobRunner.BatchJobRunnerException('The given jobName was not found');
        }

        if (scopeSize < 0) {
            scopeSize = 1;
        }
        if (scopeSize > 2000) {
            scopeSize = 2000;
        }

        List<AggregateResult> results = (List<AggregateResult>)Database.query('SELECT COUNT(Id) NumRecords FROM ' + jobForObject);
        Integer numRecords = (Integer)results.get(0).get('NumRecords');
        Integer numChunks = Math.mod(numRecords, 50000) == 0 ? numRecords / 50000 : numRecords / 50000 + 1;

        try {
            for (Integer i = 0; i < numChunks; i++) {
                Type t = Type.forName(jobName);
                Database.executeBatch((Database.Batchable<SObject>)t.newInstance(), scopeSize);
            }
        } catch (Exception e) {
            System.debug(e.getMessage());
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setToAddresses(reportToEmails); 
            email.setSubject('Error occurred while trying to run batch job ' + jobName);
            String exceptionTempl = 'Exception cause: {0}.\nException message: {1}.\nStack trace: {2}';
            String exceptionData = String.format(exceptionTempl, new List<Object>{e.getCause(), e.getMessage(), e.getStackTraceString()});
            email.setHtmlBody(exceptionData); 
            Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email});
        }
    }
}