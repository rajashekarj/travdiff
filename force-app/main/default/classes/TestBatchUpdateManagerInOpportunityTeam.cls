/**
* Test class for BatchUpdateManagerInOpportunityTeam Class
*
* @Author : Deloitte Consulting
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Isha Shukla            US63800             07/16/2019       Original Version
* -----------------------------------------------------------------------------------------------------------
*/   
@isTest
private class TestBatchUpdateManagerInOpportunityTeam {
    /**
* This method will test scenarios when when opportunity owner's manager got changed
* @param no parameters
* @return void
*/
    static testmethod void testBatch() {
        //create the custom setting for Opportunity trigger
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'OpportunityTeamMemberTrigger';
        insert objCustomsetting;
        //creating user records 
        User objUsrOwner = TestDataFactory.createTestUser('BI - National Accounts','TestNA2','LastNameNA2');
        User objManager11 = TestDataFactory.createTestUser('BI - National Accounts','TestNA2','LastNameNA2');
        User objManager1 = TestDataFactory.createTestUser('BI - National Accounts','TestNA3','LastNameNA3');
        User objManager2 = TestDataFactory.createTestUser('BI - National Accounts','TestNA','LastNameNA');
        List<User> lstUser = new List<User>{objUsrOwner,objManager1,objManager2,objManager11};
        insert lstUser;
        //Updating user hierarchy
        objManager1.ManagerId = objManager11.Id;
        objManager1.TRAV_Is_Manager_Changed__c = true;
        objUsrOwner.TRAV_Is_Manager_Changed__c = true;
        objUsrOwner.ManagerId = objManager1.Id;
        update lstUser;
        
        //create account record
        Account objProspectAccount = TestDataFactory.createProspect(); 
        List<Account> lstAccount = new List<Account>{objProspectAccount};
        insert lstAccount;
        
        //Creating opportunity records
        List<Opportunity> lstOpportunity = TestDataFactory.createOpportunities(5, objProspectAccount.Id, null);
        for(Integer i =0 ; i < 5 ; i++) {
            lstOpportunity[i].OwnerId = objUsrOwner.Id;
        }
        insert lstOpportunity; 
        
        //creating opportunity team member records
        List<OpportunityTeamMember> lstOpportunityTeam = TestDataFactory.createOpportunityTeamMembers(3,objManager1.Id,lstOpportunity[0].Id);
        lstOpportunityTeam[0].TeamMemberRole =Label.TRAV_Manager_Team_Role;
        lstOpportunityTeam[1].TeamMemberRole =Label.TRAV_Manager_Team_Role;
        lstOpportunityTeam[2].TeamMemberRole ='Underwriter';
        lstOpportunityTeam[1].OpportunityId = lstOpportunity[3].Id;
        lstOpportunityTeam[2].OpportunityId = lstOpportunity[2].Id;
        lstOpportunityTeam[1].UserId = objManager11.Id;
        lstOpportunityTeam[2].UserId = objUsrOwner.Id;
        insert lstOpportunityTeam;
        
        //Updating owner's manager
        objUsrOwner.ManagerId = objManager2.Id;
        update objUsrOwner;

        Test.startTest();
        BatchUpdateManagerInOpportunityTeam batchObj  = new BatchUpdateManagerInOpportunityTeam(); 
        Database.executebatch(batchObj);
        Test.stopTest();
        System.assertEquals([SELECT 
                             	Id 
                             FROM 
                             	OpportunityTeamMember 
                             WHERE 
                             	UserId = :objManager2.Id].size() > 0, true);
    }
}