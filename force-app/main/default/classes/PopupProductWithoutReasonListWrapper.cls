/**
* This class is used as a Wrapper Class to store the Product vs the Closed Reason data.
* This is invoked when an opportunity stage is marked as closed.
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    				Date             Description
* -----------------------------------------------------------------------------------------------------------          
* Sayanka Mohanty       US73471/US73614/US73615         01/05/2020       Original Version
* -----------------------------------------------------------------------------------------------------------              
*/
public class PopupProductWithoutReasonListWrapper {
    @AuraEnabled
    public sObject objData;
    //Add Close Reason information if required
}