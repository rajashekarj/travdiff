/**
 * Test class for ExceptionUtility Class
 *
 * @Author : Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shashank Shastri       US59672              05/15/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
 
@isTest
private class TestExceptionUtility{

    /*
    * TestMethod to test list exception.
    */
    private static testmethod void testListOutOfBoundsException(){
        
        // catch excpeption thorwn due to accessing out of bounds 
        test.startTest();
        
        //since (seeAlldata = false) no records returned
        List<Account> accList = [SELECT 
                                     Id 
                                 FROM 
                                     Account 
                                 LIMIT 1]; 
        try{
            accList[0].Name = 'Travelers'; //will throw exception
        }
        catch(Exception ex){
            //invoking exception logging framework to insert the exception in the custom object
            System.enqueueJob(new ExceptionUtility('ExceptionUtilityTest',
                                                   'testListOutOfBoundsException', ex, null, null));
        }
        
        test.stopTest();
        
        //Query the list of logs inserted to verify correctness of the logs.
        List<TRAV_Exception_Log__c> listOfLogs = [SELECT 
                                                      Error_Message__c, 
                                                      Message__c, 
                                                      Log_Type__c, 
                                                      Class_Name__c, 
                                                      Method_Name__c, 
                                                      Line__c 
                                                  FROM 
                                                      TRAV_Exception_Log__c];
        
        system.assertEquals(listOfLogs.size(), 1); //1 log entry should be created for the above transaction
        system.assertEquals(listOfLogs[0].Class_Name__c, 'ExceptionUtilityTest'); //Verifying the class name
        system.assertEquals(listOfLogs[0].Method_Name__c, 'testListOutOfBoundsException'); //Verifying the method name
        system.assert(listOfLogs[0].Message__c.contains('List index out of bounds'));
        system.assertEquals(listOfLogs[0].Log_Type__c, 'Application'); //Verifying Log Type
    }

    /**
    * TestMethod to test calloutfailure. in the absence of actual integration, tested with a blank callout
    */
    private static testmethod void testCalloutException(){
        
        //simulating an erronous httm callout
        HTTP h = new HTTP();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        HttpResponse res;
        
        test.startTest();
        
        try{
            // Send the request, and return a response
            res = h.send(req);
            res.setBody('Callout Failure');
            res.setStatusCode(404);
            if(res != null){
                System.enqueueJob(new ExceptionUtility('ExceptionUtilityTest',
                                                       'testListOutOfBoundsException', null, res, null));
            }
            else{
                String statusCode = res.getStatus();
            }
        }
        catch(Exception ex){
            //invoking exception logging framework to insert the exception in the custom object
            System.enqueueJob(new ExceptionUtility('ExceptionUtilityTest',
                                                   'testListOutOfBoundsException', ex, res, null));
        }
        
        test.stopTest();
        
        //Query the list of logs inserted to verify correctness of the logs.
        List<TRAV_Exception_Log__c> listOfLogs = [SELECT 
                                                      Error_Message__c, 
                                                      Message__c,
                                                      Log_Type__c, 
                                                      Class_Name__c, 
                                                      Method_Name__c, 
                                                      Line__c 
                                                  FROM 
                                                      TRAV_Exception_Log__c];
        
        system.assertEquals(listOfLogs.size(), 1); //1 log entry should be created for the above transaction
        system.assertEquals(listOfLogs[0].Class_Name__c, 'ExceptionUtilityTest'); //Verifying the class name
        system.assertEquals(listOfLogs[0].Method_Name__c, 'testListOutOfBoundsException'); //Verifying the method name
    }
    
    /**
    * TestMethod to test bulk DML failures. During actual runtime, validation/trigger errors will invoke this
    */
    private static testmethod void testDMLException(){
        
        //list of accounts to be inserted
        List<Account> accList = new List<Account>(); 
        List<Account> accList2 = new List<Account>(); 
        for(integer index = 0; index < 100; index++){
            //iterate and create 100 Account instances and add them in the list
            Account accRec = new Account(); //no required fields populated hence DML will throw and error
            accList2.add(accRec);
        }
        
        test.startTest();
        List<Database.saveResult> saveResult = database.insert(accList,false);
        List<Database.UpsertResult> upsertResult = database.upsert(accList,false);
        try{
            system.debug(0/0);
        }
        catch(Exception ex){
            ExceptionUtility utilObj = new ExceptionUtility('Class', 'Method', ex, null, null);    
        }
        
        ExceptionUtility.logDMLError('AddOpportunityProductController', 'saveOpportunityLineItems', saveResult );
        ExceptionUtility.logDMLError('AddOpportunityProductController', 'saveOpportunityLineItems', upsertResult );
        test.stopTest();
        
        //Query the list of logs inserted to verify correctness of the logs.
        List<TRAV_Exception_Log__c> listOfLogs = [SELECT 
                                                      Error_Message__c, 
                                                      Message__c, 
                                                      Log_Type__c, 
                                                      Class_Name__c, 
                                                      Method_Name__c, 
                                                      Line__c 
                                                  FROM 
                                                      TRAV_Exception_Log__c];
        
        //system.assertEquals(listOfLogs.size(), 100); //1 log entry should be created for each failed dml row
        //system.assertEquals(listOfLogs[0].Class_Name__c, 'ExceptionUtilityTest'); //Verifying the class name
        //system.assertEquals(listOfLogs[0].Method_Name__c, 'testListOutOfBoundsException'); //Verifying the method name
        //system.assertEquals(listOfLogs[0].Log_Type__c, 'Application'); //Verifying Log Type
    }
    /**
    * TestMethod to test bulk DML failures. During actual runtime, validation/trigger errors will invoke this
    */
    private static testmethod void testApexException(){
        
        test.startTest();
        try{
            system.debug(0/0);
        }
        catch(Exception ex){
            ExceptionUtility.logApexException('AddOpportunityProductController', 'saveOpportunityLineItems', ex, null );
        }
        test.stopTest();
        
    }

/**
    * TestMethod to test bulk DML failures. During actual runtime, validation/trigger errors will invoke this
    */
    private static testmethod void tesDeleteException(){
        List<Database.DeleteResult> lstDeleteResult;
        User biSupport = TestDataFactory.createTestUser('BI - Support', 'BI Support', 'Test User');
        insert biSupport;
        test.startTest();
            try{
            List<Account> acc = new List<Account>();
            Account ac = new Account(Name = 'Name');
            acc.add(ac);
            insert acc;
            List<sObject> sobj = [select Id, Name From User];
            lstDeleteResult = database.delete(acc, false);
            ExceptionUtility.logDeletionError('AddOpportunityProductController', 'saveOpportunityLineItems', lstDeleteResult );
            ExceptionUtility.logDMLError('AddOpportunityProductController', 'saveOpportunityLineItems', lstDeleteResult );
        }
        catch(Exception ex){
            ExceptionUtility.logDeletionError('AddOpportunityProductController', 'saveOpportunityLineItems', lstDeleteResult );   
            ExceptionUtility.logDMLError('AddOpportunityProductController', 'saveOpportunityLineItems', lstDeleteResult );
        }
        test.stopTest();
        
    }
/**
    * TestMethod to test bulk DML failures. During actual runtime, validation/trigger errors will invoke this
    */
    private static testmethod void tesDeleteAccException(){
        List<Database.DeleteResult> lstDeleteResult;
        User biSupport = TestDataFactory.createTestUser('BI - Support', 'BI Support', 'Test User');
        insert biSupport;
        test.startTest();
        List<Account> acc = new List<Account>();
            for(integer count=1; count<5; count++){
                Account ac = new Account(Name = 'Name'+count);
                acc.add(ac);
            }
            insert acc;
        system.runas(biSupport){
            try{
                lstDeleteResult = database.delete(acc, false);
                ExceptionUtility.logDeletionError('AddOpportunityProductController', 'saveOpportunityLineItems', lstDeleteResult );
                ExceptionUtility.logDMLError('AddOpportunityProductController', 'saveOpportunityLineItems', lstDeleteResult );
        }
        catch(Exception ex){
            ExceptionUtility.logDeletionError('AddOpportunityProductController', 'saveOpportunityLineItems', lstDeleteResult );   
            ExceptionUtility.logDMLError('AddOpportunityProductController', 'saveOpportunityLineItems', lstDeleteResult );
        }
        }
        test.stopTest();
    }
    /**
    * TestMethod to test bulk DML failures. During actual runtime, validation/trigger errors will invoke this
    */
    private static testmethod void tesDMLOppException(){
        List<Database.SaveResult> lstSaveResult;
        User biSupport = TestDataFactory.createTestUser('BI - Support', 'BI Support', 'Test User');
        insert biSupport;
        test.startTest();
        system.runas(biSupport){
            try{
            List<Opportunity> listToInsert = new List<Opportunity>();
            for(integer count = 0 ; count < 5; count++){
                Opportunity op = new Opportunity(Name = 'Test'+count);
                listToInsert.add(op);
            }
            UtilityClass.getEndppointURL('NoIntegration');
            UtilityClass.roundOffUtility(9.99);
            UtilityClass.roundOffUtility(9999999999.99);
            lstSaveResult = database.Insert(listToInsert, false);
            List<Database.UpsertResult> lstUpsertResult = database.Upsert(listToInsert, false);
            ExceptionUtility.logDMLError('AddOpportunityProductController', 'saveOpportunityLineItems', lstSaveResult );
            ExceptionUtility.logDMLError('AddOpportunityProductController', 'saveOpportunityLineItems', lstUpsertResult );
        }
        catch(Exception ex){
            ExceptionUtility.logDMLError('AddOpportunityProductController', 'saveOpportunityLineItems', lstSaveResult );
        }
        }
        test.stopTest();
    }
        /**
    * TestMethod to test bulk DML failures. During actual runtime, validation/trigger errors will invoke this
    */
    private static testmethod void tesLeadConvertFail(){
        List<Database.LeadConvertResult> lstLeadConvert = new List<Database.LeadConvertResult>();
        try{
            
            Account a1 = TestDataFactory.createAgency();
            insert a1;
            List<Lead> listlead = TestDataFactory.createLeads(2);
            listlead[0].TRAV_Agency_Broker__c=a1.id;
            listlead[1].TRAV_Agency_Broker__c=a1.id;
            insert listlead;
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(listlead[0].id);
            Database.LeadConvert lc2 = new database.LeadConvert();    
            lc2.setLeadId(listlead[1].id);
            List<Database.LeadConvert> listLeadConvert = new List<Database.LeadConvert>();
            listLeadConvert.add(lc);
            listLeadConvert.add(lc2);
            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);
            
            Database.LeadConvertResult lcr = Database.convertLead(lc, false);
            lstLeadConvert = Database.convertLead(listLeadConvert, false);
            System.assert(!lstLeadConvert[0].isSuccess());
        }
        catch(Exception e){
            ExceptionUtility.logDMLError('LeadConver', 'LeadConvert', lstLeadConvert);
        }
        
    }
}