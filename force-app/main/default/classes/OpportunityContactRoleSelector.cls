/**
* This class is used as a Selector Layer for OpportunityContactRole object. 
* All queries for OpportunityContactRole object will be performed in this class.
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Shruti Gupta            US68463        	 09/24/2019       Original Version
*/
public class OpportunityContactRoleSelector {
    
    static final string CLASSNAME = 'OpportunityContactRoleSelector';
    
    /**
* This is a method is used to get list of tpa contact from opprtunities ID
* @param string Opportunities id set
* @return List<Opportunity> List of Opportunities based on the SOQL query provided as an input parameter.
*/
    public static List<OpportunityContactRole> getContact(Set<Id> setOppId) {
        final string METHODNAME = 'getContact';
        
        String strAllFields = UtilityClass.getAllFieldsSOQL('OpportunityContactRole');
        String strQueryOppContact = 'Select ' + strAllFields +
            ' From OpportunityContactRole' + 
            ' WHERE OpportunityId IN:setOppId AND (Contact.RecordType.DeveloperName ='+'\''+
            Constants.TPARcordType+'\''+' OR Contact.RecordType.DeveloperName ='+'\''+
            Constants.agencyLicensedRecordType+'\''+' OR Contact.RecordType.DeveloperName ='+'\''+
            Constants.agencyNonLicensedRecordType+'\''+' OR Contact.RecordType.DeveloperName ='+'\''+
            Constants.CustomerRecordType+'\''+')';
        List<OpportunityContactRole> listContact = new List<OpportunityContactRole>();
        try {
            system.debug('strQueryOppContact-->>'+strQueryOppContact);
            listContact = Database.query(strQueryOppContact);
            system.debug('listContact->>'+listContact);
            if(Test.isRunningTest()){
                throw new QueryException();
            }
        } 
        catch(QueryException objExp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
            
        } // End Catch
        return listContact;
    } //End of Method
    
    
    public static Map<ID,String> getContactIdName(set<Id> contactIds) {
        final string METHODNAME = 'getContactIdName';
        Map<ID,String> mapContact = new Map<ID, String>();
        try{
            for(Contact c:[SELECT Id, Name FROM Contact where id IN:contactIds]){
                mapContact.put(c.id,c.name);
                
            } 
        }
        catch(Exception e){
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, e, null);
        }
        return mapContact;
        
    } 

public static Map<ID,Opportunity> getOpportunities(set<Id> oppIds) {
        final string METHODNAME = 'getOpportunities';
        Map<ID,Opportunity> mapOpp;
        try{
             mapOpp = new Map<ID, Opportunity>([SELECT Id, Name,Trav_Primary_Contact__c,Trav_Primary_Customer_Contact__c FROM Opportunity where id IN:oppIds]);
        }
        catch(Exception e){
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, e, null);
        }
        return mapOpp;
        
    }    
}