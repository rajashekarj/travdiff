/**
* This class is the wrapper class for Response Payload
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect   		Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty        US69347/US69863          11/10/2019      Original Version
* -----------------------------------------------------------------------------------------------------------
*/
public class ContactSPUpdateResponseWrapper {
    /*{"Status":"200","Operation":"CREATE","MdmId":"TRV00005400046","PtyName":"Lisa Ray"} 
	{"Status":"202","Operation":"CREATE","MdmId":"[TRV00005400056]","Message":"Agent already exists before creation"}*/
    public String Status{get;set;}
    public String Operation{get;set;}
    public List<String> MdmId{get;set;}
    public String PtyName{get;set;}
    public String srcSysCd{get;set;}
    public String srcId{get;set;}
    public String Message{get;set;}
    public List<String> agncyIndId{get;set;}
}