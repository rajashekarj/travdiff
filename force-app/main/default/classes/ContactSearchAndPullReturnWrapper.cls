/**
* This class is the wrapper class for LC
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect          Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty        US69347/US69863          11/10/2019      Original Version
* -----------------------------------------------------------------------------------------------------------
*/
public class ContactSearchAndPullReturnWrapper {
    @AuraEnabled
    public String strReturn{get;set;}
    //Return wrapper from GET API
    @AuraEnabled
    public List<ContactSearchAndPullRetrieveWrapper> lstWrapperContactDetails = new List<ContactSearchAndPullRetrieveWrapper>();
}