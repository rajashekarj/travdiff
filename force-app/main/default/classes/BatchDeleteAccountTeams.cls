/**
* Batch Class for Deletion of Account Executive when it is list for opportunity closed more than 12 months.
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Isha Shukla		      US60806             06/03/2019       Added method insertDeleteAccountTeams
* Isha Shukla            US62980              06/24/2019       Added parameter to method call of OpportunityTeamMemberService.gettingAccountTeamsForDeletion
* Isha Shukla            US62980              06/28/2019 	   Added SEMICOLON variable 
* Isha Shukla		     US64798			  08/08/2019       Updated method updateAccountTeamMembers to enable functionality for TRAV_Agency_Broker__c field
*/ 
global class BatchDeleteAccountTeams implements Database.Batchable<sObject>, Schedulable {
    /**
	* This method collect the batches of records or objects to be passed to execute
	* @param Database.BatchableContext
	* @return Database.QueryLocator record
	*/
    global Database.QueryLocator start(Database.BatchableContext batchCon) {
        List<String> lstRole = Label.TRAV_Roles.split(Constants.SEMICOLON);
        String strClosedStages = General_Data_Setting__c.getOrgDefaults().ClosedStages__c;
       	List<String> lstClosedStages = strClosedStages.split(Constants.SEMICOLON);
        Integer intNumberOfMonths = (Integer)General_Data_Setting__c.getOrgDefaults().NumberOfMonths__c;
        //Retrieving all those Opportunity Teams whose Opportunities got closed by more than last 12 months
        String strQuery = 'SELECT Id, UserId, Opportunity.AccountId, Opportunity.TRAV_Agency_Broker__c FROM OpportunityTeamMember ';
        strQuery += 'WHERE Opportunity.StageName IN :lstClosedStages AND Opportunity.CloseDate < LAST_N_DAYS:'+intNumberOfMonths;
        strQuery += 'AND TeamMemberRole IN :lstRole AND TRAV_Active__c = true';
        return Database.getQueryLocator(strQuery);
    }
    /**
	* This method process each batch of records for the implementation logic
	* @param Database.BatchableContext record
	* @param List of OpportunityTeamMember 
	* @return void 
	*/
    global void execute(Database.BatchableContext batchCon, List<OpportunityTeamMember> records){
        final string METHODNAME = 'execute';
        final string CLASSNAME = 'BatchDeleteAccountTeams';
        Set<Id> setUserId = new Set<Id>(); // Set of user ids related to opportunity teams which got closed 
        Set<Id> setAccountId = new Set<Id>();// Set of account ids related to opportunity teams which got closed 
        Set<Id> setOpportunityTeamMemberId = new Set<Id>();
        try{
            for(OpportunityTeamMember objOpptyTeam : records) {
                //Populating user ids which are part of teams of closed opportunity
                setUserId.add(objOpptyTeam.UserId); 
                //populating standard account id related to closed opportunities
                if(objOpptyTeam.Opportunity.AccountId != null) {
                   setAccountId.add(objOpptyTeam.Opportunity.AccountId); 
                }
                //populating Agency nrick and motor account id related to closed opportunities
                if(objOpptyTeam.Opportunity.TRAV_Agency_Broker__c != null) {
                  setAccountId.add(objOpptyTeam.Opportunity.TRAV_Agency_Broker__c);
                }
                //populating set of opportunity team member which already came as query result related to closed opportunities 
                setOpportunityTeamMemberId.add(objOpptyTeam.Id);
            }//End of for
            if(setUserId.size() > 0 && setAccountId.size() > 0) {
                List<AccountTeamMember> listAccountTeamToDelete = new List<AccountTeamMember>();
                //Getting those account team which are related with opportunities got closed more than last 12 months
                listAccountTeamToDelete = OpportunityTeamMemberService.gettingAccountTeamsForDeletion(setUserId, setAccountId,setOpportunityTeamMemberId);
                //Deleting account teams and handling exceptions
                if(listAccountTeamToDelete.size() > 0) {
                    Database.DeleteResult[] lstDeleteResult = Database.delete(listAccountTeamToDelete, false);
                    //exception handling for DELETE DML 
                    ExceptionUtility.logDeletionError(CLASSNAME, METHODNAME, lstDeleteResult);
                }
            }//End of if
			if(Test.isRunningTest()){
                throw new QueryException();
            }
        } //end of try
        catch(exception objExp) {
             //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        }//End of catch
        
    }//end of method execute
    
    /**
	* This method execute any post-processing operations test
	* @param Database.BatchableContext record 
	* @return void 
	*/
    global void finish(Database.BatchableContext batchCon){
       
    } 
    /**
	* This method execute this batch class when scheduled
	* @param SchedulableContext record 
	* @return void 
	*/
    global void execute(SchedulableContext schCon) {
      BatchDeleteAccountTeams batchObj  = new BatchDeleteAccountTeams(); 
      Database.executebatch(batchObj);
   }
}