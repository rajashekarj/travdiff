/**
 * This class is used as a controller for Password setup page
 *
 *
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect   Date            Description
 * -----------------------------------------------------------------------------------------------------------              
 * Isha Shukla            US81639             27/04/2020      Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
public class PasswordSetupPageController {
    public String username{get;set;}
    public String externalId{get;set;}
    public String password{get;set;}
    public String message;
    private List<User> userList;
    public PasswordSetupPageController(){
        username='';
        externalId= '';
        message = '';
        userList = new List<User>();
    }
    public void setupPassword(){
        if( (username != '' || externalId != '') && password != null) {
            if(username != '' && userList.size() == 0) {
                system.debug('with username');
                userList = [SELECT Id, Name, Username, TRAV_External_Id__c  
                            FROM User 
                            WHERE isActive = true 
                            AND Username =:username];
            }
            if(externalId != '' && userList.size() == 0) {
                system.debug('with username');
                userList = [SELECT Id, Name, Username, TRAV_External_Id__c  
                            FROM User 
                            WHERE isActive = true 
                            AND TRAV_External_Id__c =:externalId];
            }
            
            if(userList.size() > 0){
                try{
                    System.setPassword(userList[0].Id, password);
                    message = 'Password successfully changed for : '+userList[0].Name+' (UserName = '+userList[0].Username+'and NTICId = '+userList[0].TRAV_External_Id__c+')';
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,message) );
                }catch(Exception objExp){
                    message = objExp.getMessage();
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,message) );
                }
            }else {
                message = 'User not found ';
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,message) );
            }
        }
    }
}