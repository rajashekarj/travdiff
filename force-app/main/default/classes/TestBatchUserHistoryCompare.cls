@isTest
public class TestBatchUserHistoryCompare {
 @IsTest
    static void TestExcuteBatch(){
        
        /*
        * 1 - Create new user
        * 2 - Insert the user in history snapshot object with different name
        * 3 - Run the batch 
        */
        User objUsrOwner = TestDataFactory.createTestUser('BI - National Accounts','TestNA2','LastNameNA2');
        objUsrOwner.Username = 'addnewrole@travis.com';
        insert objUsrOwner;
               
        TRAV_User_History_Snapshot__c history_user =  new  TRAV_User_History_Snapshot__c();
        history_user.TRAV_Name__c = 'TestNA3';
        history_user.TRAV_Username__c = objUsrOwner.Username;
        insert history_user;
                
        
              
        Test.startTest();
              
       	BatchUserHistoryCompare bc = new BatchUserHistoryCompare();
       	ID jobID = Database.executeBatch(bc,5000);
        Test.stopTest();
        
    } {

    }
}