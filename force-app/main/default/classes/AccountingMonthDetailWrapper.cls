/**
* This class is used as a wrapper class for DaysLeftInCurrentMonth lightning web component. 
* 
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Tejal Pradhan          US63928              03/17/2020       Wrapper for LWC DaysLeftInCurrentMonth
*-----------------------------------------------------------------------------------------------------------
*/
public class AccountingMonthDetailWrapper{
    @AuraEnabled 
    public String strAccountMonthLabel {get;set;}
    @AuraEnabled 
    public Integer intDaysLeft {get;set;}
    @AuraEnabled 
    public String dtEndDateForAccountingMonth {get;set;}
    
    public AccountingMonthDetailWrapper(String strAccountMonthLabel,Integer intDaysLeft,Date dtEndDateForAccountingMonth){
        this.strAccountMonthLabel = strAccountMonthLabel;
        this.intDaysLeft = intDaysLeft;
        this.dtEndDateForAccountingMonth = dtEndDateForAccountingMonth.month() + '-' + dtEndDateForAccountingMonth.day() + '-' + dtEndDateForAccountingMonth.year();
    }
}