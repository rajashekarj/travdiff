/**
* Batch Class for NotificationGenConfig__mdt. This class will have logic to generate Notification.
* Modification Log    :
* ------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* ------------------------------------------------------------------------------------------------------
* Shashank Agarwal       US59903              06/26/2019      Original Version
* dummy deploy 10/16/19 ejjoseph
*/

global class BatchNotificationGeneration implements Database.Batchable<sObject> {
    Integer objSequenceNumber = 1;
    String soqlQuery;
    NotificationGenConfig__mdt notifObj;
    List<NotificationWrapper> notWrapperList = new List<NotificationWrapper>();
    /**
    * This is a parametrized constructor 
    * @param objSerialNumber type  Integer to capture sequence number
    */
    global BatchNotificationGeneration(Integer objSerialNumber){
        objSequenceNumber = objSerialNumber;
        //Get the Metadata Record for the given Sequence Number
        List<NotificationGenConfig__mdt> 
        lstnotifObj = [SELECT Id, TRAV_Active__c, 
                                              TRAV_AssignTo__c, TRAV_FieldList__c, 
                                              TRAV_FieldToBeUpdated__c, TRAV_IsRealtime__c, 
                                              TRAV_QueryFilter__c, TRAV_sObject_Source__c, 
                                              TRAV_sObject_Target__c, TRAV_Subject__c, 
                                              TRAV_Sequence_Number__c, TRAV_Role__c,
                                              TRAV_Contains_Hyperlink__c,
                                              TRAV_Role_Profile__c,
                                              (SELECT Id, TRAV_NotificationGenConfig__c,
                                              TRAV_FieldAPI__c, 
                                              TRAV_Operator__c, 
                                              TRAV_Values__c, 
                                              TRAV_Type__c,
                                              TRAV_Sequence__c 
                                              FROM Notification_Filters__r ORDER BY TRAV_Sequence__c ASC)
                                              FROM NotificationGenConfig__mdt WHERE TRAV_Active__c = true
                                              AND TRAV_Sequence_Number__c =: objSequenceNumber
                                              AND TRAV_IsRealtime__c = false LIMIT 1];
                                              
         if(lstnotifObj.size() > 0){
             notifObj = lstnotifObj[0];
          //Form the SOQL to be executed by the Batch
             soqlQuery = 'SELECT '+notifObj.TRAV_FieldList__c+' FROM '+notifObj.TRAV_sObject_Source__c;
             String filterString = notifObj.TRAV_QueryFilter__c;
             // to track the count of filters applied
             integer count = 1;
             system.debug('**Filter size**'+notifObj.Notification_Filters__r.size());
             //update the fileters to ensure filters dont get overridden.
             for(integer filterIndex = 1; filterIndex <= notifObj.Notification_Filters__r.size(); filterIndex++){
                 filterString = filterString.replace(''+filterIndex, '{'+filterIndex+'}');
             }
             system.debug(notifObj.Notification_Filters__r.size()+'filterTransform '+filterString);
             //Iterate over the list of filter config and form the where Condition
             for(TRAV_NotificationFilter__mdt filterRec : notifObj.Notification_Filters__r){
             system.debug(JSON.serialize(filterRec.TRAV_Sequence__c +'      '+filterRec.TRAV_FieldAPI__c));
                 String filterVal = filterRec.TRAV_Values__c;
                 if(filterRec.TRAV_Type__c == 'String'){
                     //Added condition to handle null scenarios:Sayanka
                     if(!(filterRec.TRAV_Values__c).equalsIgnoreCase('null')){
                         if(((filterRec.TRAV_Operator__c).equalsIgnoreCase('IN')) || ((filterRec.TRAV_Operator__c).equalsIgnoreCase('NOT IN')) ){
                             filterVal = filterRec.TRAV_Values__c;
                         }else{
                             filterVal = '\''+filterRec.TRAV_Values__c+'\'';
                         }
                     }else{
                         filterVal = filterRec.TRAV_Values__c;
                     }
                     
                 }
                 else if(filterRec.TRAV_Type__c == 'Date'){
                     //filterVal = 
                 }
                 system.debug('**Filter Value**'+filterVal);
                 filterString = ' '+filterString+' ';
                 //filterString = filterString.replaceAll(string.valueOf(count), filterRec.TRAV_FieldAPI__c+' '+filterRec.TRAV_Operator__c+' '+filterVal);
                 filterString = filterString.replace('{'+count+'}', ' '+filterRec.TRAV_FieldAPI__c+' '+filterRec.TRAV_Operator__c+' '+filterVal+' ');
                 count++;
                 system.debug('filter changeing '+filterString);
             }
             if(filterString != ''){
                 soqlQuery += ' WHERE '+filterString;
             }
             //system.debug(database.query(soqlQuery));
             system.debug('queryLocator'+soqlQuery);
         }
         else{
             soqlQuery = 'SELECT ID, Name FROM Opportunity WHERE Name = \'\'';
         }
    } //End of Method
    
    /**
    * This method collect the batches of records or objects to be passed to execute
    * @param Database.BatchableContext
    * @return Database.QueryLocator record
    */
    global Database.QueryLocator start(Database.BatchableContext bc) {
        try{
            system.debug('queryLocator'+soqlQuery);
            return Database.getQueryLocator(soqlQuery);

        }catch(QueryException ex){
            system.debug('**Error at**'+ex.getLineNumber() + ' **with message**'+ex.getMessage()); 
            return null;
        }
    } //End of Method
    
     /**
    * This method process each batch of records for the implementation logic
    * @param Database.BatchableContext record
    * @param List of Opportunity 
    * @return void 
    */ 
    global void execute(Database.BatchableContext bc, List<sObject> scope){
    system.debug('**scope**'+scope);
    system.debug('**size of scope**'+scope.size());
        try{
    // This code will execute for all the records that meed the filter criteria. Hence no need to further filter or segregate the data
        List<sObject> listToInsert = new List<sObject>();
        List<String> setOfTeamRoles = new List<String>();
        Map<String,String> mergeFieldAPIValMap = new Map<String,String>();
        List<Id> finalUserList;
        Map<String,String> fieldValueMap = new Map<String,String>();
        if(notifObj.TRAV_Role__c != NULL){
            if(notifObj.TRAV_Role__c.contains(';')){
                /*This will be uncommented if multiple roles will be used
                List<String> tempList = (notifObj.TRAV_Role__c).split(';');
                setOfTeamRoles.addAll(tempList);*/
            }else{
               setOfTeamRoles.add(notifObj.TRAV_Role__c); 
            }
            system.debug('set of roles'+setOfTeamRoles);
        }
        Set<Id> oppIds = new Set<Id>();
        Set<String> fieldAPINames = new Set<String>();
        oppIds = new Map<Id, sObject>(scope).keySet();
        system.debug('opp ids '+oppIds);
        //Get Map of Opportunity vs Roles vs List Of users in the role
        Map<String, Map<String, List<Id>>> oppIdVsRoleAndMembers = NotificationUtility.generateOpportunityTeamMembers(oppIds, setOfTeamRoles);
        system.debug('**oppIdVsRoleAndMembers**'+oppIdVsRoleAndMembers);
        //Check what type of notification creation
        String notificationType = notifObj.TRAV_sObject_Target__c;
        system.debug(notificationType);
        fieldAPINames.addAll(notifObj.TRAV_FieldList__c.toLowerCase().split(','));
        //Iterate over the list of records and form the Notification Wrapper
        for(sObject objRec : scope){
            finalUserList = new List<Id>();
            //Prepare field API name vs value map
            mergeFieldAPIValMap = NotificationUtility.prepareMergeFieldMap(objRec, fieldAPINames);
            if(notifObj.TRAV_Role__c == null || notifObj.TRAV_Role__c ==''){
                if(String.isNotBlank(notifObj.TRAV_AssignTo__c)){
                    if((notifObj.TRAV_AssignTo__c).contains('.')){
                        finalUserList.add(NotificationGenerator.getfieldApiValues(objRec, notifObj.TRAV_AssignTo__c));
                        system.debug('**Retrieved user list**'+finalUserList);
                    }else{
                        finalUserList.add((Id)objRec.get(notifObj.TRAV_AssignTo__c));
                    }
                }else{
                    finalUserList.add((Id)objRec.get('OwnerId'));
                }
            }
            else{
                if(!notifObj.TRAV_Role__c.contains(';') ){
                    if(oppIdVsRoleAndMembers != null){
                        if(oppIdVsRoleAndMembers.containsKey(objRec.Id)){
                            if(oppIdVsRoleAndMembers.get(objRec.Id).containskey(notifObj.TRAV_Role__c)){
                                finalUserList = oppIdVsRoleAndMembers.get(objRec.Id).get(notifObj.TRAV_Role__c);
                            }
                        }
                    }
                }else{
                   /*This will be uncommented if multiple roles will be used
                    list<String> tempList = (notifObj.TRAV_Role__c).split(';');
                    if(oppIdVsRoleAndMembers.get(objRec.Id).containskey(tempList[0])){
                        finalUserList.addAll(oppIdVsRoleAndMembers.get(objRec.Id).get(tempList[0]));
                    }
                       if(oppIdVsRoleAndMembers.get(objRec.Id).containsKey(tempList[1])){
                           finalUserList.addAll(oppIdVsRoleAndMembers.get(objRec.Id).get(tempList[1]));
                       }
                */}
                if(notifObj.TRAV_AssignTo__c != null && String.isNotBlank(notifObj.TRAV_AssignTo__c)){
                    
                    if((notifObj.TRAV_AssignTo__c).contains('.')){
                        finalUserList.add(NotificationGenerator.getfieldApiValues(objRec, notifObj.TRAV_AssignTo__c));
                        system.debug('**Retrieved user list**'+finalUserList);
                    }else{
                        finalUserList.add((Id)objRec.get(notifObj.TRAV_AssignTo__c));
                    }
                }
            }
            system.debug('**Final User List**'+finalUserList);
            system.debug(mergeFieldAPIValMap);
            String fieldsToBeUpdated = notifObj.TRAV_FieldToBeUpdated__c;
            String roleProfile = String.isNotBlank(notifObj.TRAV_Role_Profile__c) ? notifObj.TRAV_Role_Profile__c : '';
            fieldValueMap = NotificationCustomUpdateHandler.calculateFieldUpdates(fieldsToBeUpdated,mergeFieldAPIValMap);
            if(!finalUserList.isEmpty()){
                NotificationWrapper nw = NotificationUtility.createNotificationWrapper(notifObj.TRAV_Subject__c, 
                                                                                       objRec.Id, notifObj.TRAV_sObject_Target__c, 
                                                                                       Userinfo.getUserId(), roleProfile,  
                                                                                       finalUserList,fieldValueMap, 
                                                                                       mergeFieldAPIValMap,notifObj.TRAV_Contains_Hyperlink__c);
                notWrapperList.add(nw);
            }
            system.debug('**Final User List**'+finalUserList);
        }
        if(!notWrapperList.isEmpty()){
            system.debug('**Final Wrapper List**'+json.serialize(notWrapperList));
            NotificationGenerator.assignJob(notWrapperList,fieldValueMap);
        }
        }catch(Exception ex){
            system.debug('**Exception at**'+ex.getLineNumber() + '**Message**'+ex.getMessage());
        }
    } //End of Method
    
    /**
    * This method execute any post-processing operations test
    * @param Database.BatchableContext record 
    * @return void 
    */
    global void finish(Database.BatchableContext bc){
        if(!Test.isRunningTest()){
        // Getting Highest Value of Sequence number
        NotificationGenConfig__mdt objNotificationGenConfig = [SELECT TRAV_Sequence_Number__c FROM NotificationGenConfig__mdt WHERE TRAV_Active__c = true AND TRAV_Sequence_Number__c != null AND TRAV_IsRealtime__c != true  ORDER BY TRAV_Sequence_Number__c DESC LIMIT 1];
        system.debug('sequence #'+objNotificationGenConfig.TRAV_Sequence_Number__c);
        // Check if current sequence number id less than Hightest Value of sequence number
        if(objSequenceNumber < objNotificationGenConfig.TRAV_Sequence_Number__c){
            objSequenceNumber = objSequenceNumber + 1;
            Database.executeBatch(new BatchNotificationGeneration(objSequenceNumber), 200);
        } //End of if for sequence check
        }
    } //End of Method 
} //End of Class