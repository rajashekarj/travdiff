/**
 * This class is used as a Selector Layer for Sic object. 
 * All queries for Sic object will be performed in this class.
 *
 * Modification Log    :
 * * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Shruti Gupta            US74801        	  03/06/2020        Original Version
*/
public class SicSelector {
    /**
* This is a method is used to get Sic with ids as parameter
* @param : set<id>
* @return List of Sic records.
*/
    public static list<TRAV_SIC__c> getSicCodeswithIds(set<id> setSicId){
        return [SELECT Id, Trav_External_Id__c 
                                FROM TRAV_SIC__c where id in :setSicId];
        
    }

}