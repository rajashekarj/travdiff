/**
* Batch Class for Opportunity. This class will have logic for cloning of opportunity record if Record 
* Type is Renewal on opportunity
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------
* Bhanu Reddy            US78701            03/10/2020       Draft Version
 */

global  class BatchUpdateOpportuntyRolluponAgency implements Database.Batchable<sObject> {
  
    /**
* This method collect the batches of records or objects to be passed to execute
* @param Database.BatchableContext
* @return Database.QueryLocator record
*/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('Select id from Account where Recordtype.developername =\'TRAV_Agency_Brick_and_Mortar\'');
    } //end of start Method
    global void execute(Database.BatchableContext BC, List<Account> scope) {
        try{
            set<id> setAgencyids = new set<id>();
             for(Account objAcc:scope ){
                setAgencyids.add(objAcc.id);
             }
             AgencyService.rollupOpportuntiyTOAgency(null,setAgencyids);
        } //end try
        catch(Exception objExp){
            ExceptionUtility.logApexException('BatchUpdateOpportuntyRolluponAgency', 'Execute', objExp, null);
        }
    } //end of Execute method
    global void finish(Database.BatchableContext BC) { 
    } // end of finish method
} //End of class
