/**
 * Test class for BSIOpportunityPopUpController

*
 * @Author : Deloitte Consulting
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------
 * Shashank Agarwal       US73049              12/10/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */ 
@isTest
public class TestBSIOpportunityPopUpController {
     @testSetup static void createTestData(){
        //Add Account
        Account a = TestDataFactory.createProspect();
        insert a;
        system.assert(a.Name == 'Prospect Account');
        //Add a carrier
        Trav_Carrier__c carrier = new  Trav_Carrier__c(Name = 'Carrier A');
        insert carrier;
        system.assert(carrier.Name == 'Carrier A');

        //Add opportunity
        List<Opportunity> finalOpptyList = new List<Opportunity>();
        Opportunity o1 = TestDataFactory.createOppty('Submission', 'Test oppty A',a.Id);
        o1.TRAV_Business_Unit__c = 'BSI-FI';
        o1.TRAV_Winning_New_Carrier__c = carrier.Id;
        o1.TRAV_Policy_Effective_Date__c = Date.today().addYears(1);
        insert o1;
        Opportunity opp = [SELECT Id,StageName FROM Opportunity LIMIT 1][0];
        List<OpportunityFieldHistory> lstOpportunityHistory = new List<OpportunityFieldHistory>();


        Test.startTest();

     }

    static testmethod void testPopUpFireCase(){
        BSIOpportunityPopUpController.UnderReviewWrapper objUnderReviewWrapper = new BSIOpportunityPopUpController.UnderReviewWrapper();
        Test.startTest();
        Opportunity opp = [SELECT Id,StageName FROM Opportunity LIMIT 1];
        opp.StageName = 'Under Review';
        update opp;
        objUnderReviewWrapper = BSIOpportunityPopUpController.getLoadPopUpDetails(opp.Id);
        Test.stopTest();
    }

    static testmethod void testSaveDetails(){
        Test.startTest();
        Opportunity opp = [SELECT Id,StageName FROM Opportunity LIMIT 1];
        String strOutstandingItemIndicator = 'Yes';
        String strMaterialsRequested = 'Application';
        String strUnderReviewNotes = 'Test Class Notes';
        BSIOpportunityPopUpController.saveDetails(opp.Id, strOutstandingItemIndicator, strMaterialsRequested, strUnderReviewNotes);
        strOutstandingItemIndicator = 'No';
        strMaterialsRequested = '';
        strUnderReviewNotes = '';
        BSIOpportunityPopUpController.saveDetails(opp.Id, strOutstandingItemIndicator, strMaterialsRequested, strUnderReviewNotes);
        Test.stopTest();
    }

    static testmethod void testRevertStageUpdate(){
        Opportunity opp = [SELECT Id,StageName FROM Opportunity LIMIT 1][0];
        opp.StageName = 'Submission';
        Update opp;
		opp.StageName = 'Under Review';
        Update opp;


        List<OpportunityFieldHistory> lstOpportunityHistory = new List<OpportunityFieldHistory>();
         if (Test.isRunningTest()) {
            lstOpportunityHistory.add(new OpportunityFieldHistory(Field='StageName',OpportunityId=opp.Id));
            insert lstOpportunityHistory;
        }

        Test.startTest();

        BSIOpportunityPopUpController.revertStageUpdate(opp.Id);
        Test.stopTest();
    }

}