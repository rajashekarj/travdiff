/**
* Test class for OpportunityService Class
*
* @Author : Deloitte Consulting
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Shashank Agarwal       US60806              07/12/2019       Original Version
* -----------------------------------------------------------------------------------------------------------
*/
@isTest
private class TestServicePartnerRequestService {
   
    /**
* This method will test scenarios when opportunity is updated
* @param no parameters
* @return void
*/
    static testmethod void testTaskCreationOnInsertion() {
        
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'ServicePartnerRequestTrigger';
        insert objCustomsetting;
        
        User objUsr = TestDataFactory.createTestUser('BI - Support','TestBISupport','LastTestBISupport');
        insert objUsr;
        
        //create account record
        Account objProspectAccount = TestDataFactory.createProspect(); 
        List<Account> lstAccount = new List<Account>{objProspectAccount};
        insert lstAccount;
        
        List<Opportunity> lstOpportunity = TestDataFactory.createOpportunities(1, objProspectAccount.Id, 'BI-NA');
        lstOpportunity[0].StageName = Constants.SUBMISSION;
        insert lstOpportunity;
        
        List<OpportunityTeamMember> lstOpportunityTeamMember = TestDataFactory.createOpportunityTeamMembers(1,objUsr.Id, lstOpportunity[0].Id);
		lstOpportunityTeamMember[0].TeamMemberRole = Constants.COLLATERALANALYST;
        insert lstOpportunityTeamMember;
        
        TRAV_Service_Partner_Request__c lstServicePartnerRequest = new TRAV_Service_Partner_Request__c();
        lstServicePartnerRequest.Name = 'Test Service Partner';
        lstServicePartnerRequest.Opportunity__c = lstOpportunity[0].Id;
        
        Test.startTest();
        insert lstServicePartnerRequest;
        Test.stopTest();
        
        
        Task objTask = [Select Id, Subject, Service_Partner_Request__c From Task where Service_Partner_Request__c = :lstServicePartnerRequest.Id LIMIT 1];
       
        System.assertEquals(lstServicePartnerRequest.Id, objTask.Service_Partner_Request__c);
        
    }
    
    /**
    * This method is added to cover the coverage for TriggerHandler Class.
    * @param no parameters
    * @return void
    */
    static testmethod void testEmptyMethodsForTriggerHandler() {
       TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'ServicePartnerRequestTrigger';
        insert objCustomsetting;
        
        User objUsr = TestDataFactory.createTestUser('BI - Support','TestBISupport','LastTestBISupport');
        insert objUsr;
        
        //create account record
        Account objProspectAccount = TestDataFactory.createProspect(); 
        List<Account> lstAccount = new List<Account>{objProspectAccount};
        insert lstAccount;
        
        List<Opportunity> lstOpportunity = TestDataFactory.createOpportunities(1, objProspectAccount.Id, 'BI-NA');
        lstOpportunity[0].StageName = Constants.SUBMISSION;
        insert lstOpportunity;
        
        List<OpportunityTeamMember> lstOpportunityTeamMember = TestDataFactory.createOpportunityTeamMembers(1,objUsr.Id, lstOpportunity[0].Id);
		lstOpportunityTeamMember[0].TeamMemberRole = Constants.COLLATERALANALYST;
        insert lstOpportunityTeamMember;
        
        TRAV_Service_Partner_Request__c lstServicePartnerRequest = new TRAV_Service_Partner_Request__c();
        lstServicePartnerRequest.Name = 'Test Service Partner';
        lstServicePartnerRequest.Opportunity__c = lstOpportunity[0].Id;
        insert lstServicePartnerRequest;

        Test.startTest();
        update lstServicePartnerRequest;
        delete lstServicePartnerRequest;
        undelete lstServicePartnerRequest;
        Test.stopTest();
        
    }
    
}