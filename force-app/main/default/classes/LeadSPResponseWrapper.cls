/**
* This class is the Wrapper Class for Storing Response From GET API
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect                  Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty        US73994/US73995/US73996          24/01/2020      Original Version
* -----------------------------------------------------------------------------------------------------------
*/public class LeadSPResponseWrapper {
    @auraEnabled
    public String actionType{get;set;}//Describes the action type i.e Search/Get/Create
	@auraEnabled
    //public LeadSPSearchResponseWrapper searchResponseWrapper{get;set;}
    public List<LeadSPSearchReturnWrapper> listSearchResponseWrapper{get;set;}
    @auraEnabled
    public LeadSPGetResponseWrapper getResponseWrapper{get;set;}
    @auraEnabled
    public LeadSPCreateResponseWrapper createResponseWrapper{get;set;}
    @auraEnabled
    public String errorIFAny{get;set;}//If any error is encountered, show it on toast and suspend operations
    @auraEnabled
    public String successMessage{get;set;}
    @auraEnabled
    public Id idNewRecord{get;set;} 

}