/**
* This class is used as a Selector Layer for Producer object. 
* All queries for Producer object will be performed in this class.
*
*
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect      Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Bhanu Reddy                 US62952         06/27/2019        Original Version
* Shashank Agarwal            US69018         09/30/2019        Added Method getProducerForBAMBatch
* -----------------------------------------------------------------------------------------------------------
*/
public with sharing class ProducerSelector {
    /**
	* Method to get map of producer codes based on the producer codes
	* @param setProducerCodes - Set of producer codes
	* @return  Map of producer codes and producer records
	*/
    public static Map<String, TRAV_Producer__c> getProducerMapByExternalId(Set<String> setProducerCodes){  
        List<TRAV_Producer__c> listProducers = new List<TRAV_Producer__c>(
            [
                SELECT
                	Id,
                	Name,
                	TRAV_Producer_Code__c
                FROM 
                	TRAV_Producer__c 													
				WHERE 
			     	TRAV_Producer_Code__c  IN :setProducerCodes
                LIMIT
                	:setProducerCodes.size()            
        ]);
        
        Map<String, TRAV_Producer__c> mapProducers = new Map<String, TRAV_Producer__c>();
        for(TRAV_Producer__c producer : listProducers) {
            mapProducers.put(producer.TRAV_Producer_Code__c, producer);
        }
        
        return mapProducers;
   } //end of the method
   
   /**
* This is a method is used to get list opportunities from ID
* @param string Opportunities id set
* @return List<Opportunity> List of Opportunities based on the SOQL query provided as an input parameter.
*/
    public static Set<Id> getProducerForBAMBatch(DateTime dtFrom) {
       
        List<TRAV_Producer__History> listProducers = new List<TRAV_Producer__History>(
            [
                SELECT
                	ParentId
                FROM 
                	TRAV_Producer__History 													
				WHERE 
			     	Field = 'TRAV_Financial_Branch_Code__c'
                AND
                	CreatedDate > :dtFrom        
        ]);
        
        Set<Id> setProducerId = new Set<Id>(); 
        
        if(!listProducers.isEmpty()){
            for(TRAV_Producer__History objProducerHistory : listProducers){
                setProducerId.add(objProducerHistory.ParentId);
            }
        }
        return setProducerId;
    }

} //end of the class