/**
* Test class for Contact Search and Pull Classes
*
* @Author : Deloitte Consulting
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              	User Story/Defect    				Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty            US69347,US69863,              10/23/2019       Original Version
-----------------------------------------------------------------------------------------------------------
*/
@isTest
public class TestContactSearchPullUtility {
    @testSetup static void createTestData(){
        //Create Account
        Account objAcc = TestDataFactory.createProspect();
        system.assert(objAcc != null);
        objAcc.TRAV_Account_External_Id__c = 'BM_1059253';
        insert(objAcc);
        //Create Opportunity
        List<Opportunity> lstOppty = TestDataFactory.createOpportunities(1,objAcc.id , 'BSI-FI');
        system.assert(lstOppty.size() == 1);
        database.insert(lstOppty);
        //Create Contact
        Contact objCon = TestDataFactory.createAgencyNonLicensed(objAcc.id);
        objCon.Phone = '0909890987';
        objCon.MobilePhone = '9890878908';
        objCon.Fax = '8909865456';
        objCon.TRAV_Job_Title__c = 'Bookkeeper';
        objCon.Birthdate = date.today().addDays(-300);
        objCon.TRAV_Agent_Preferences__c = 'None';
        objCon.TRAV_Broker__c = false;
        
        objCon.TRAV_External_Id__c = 'TRV00005400056';
        system.assert(objCon != null);
        insert objCon;
        
        

        //Create Account Contact relationship
        AccountContactRole objACRoleRec = new AccountContactRole(ContactId = objCon.Id, AccountId = objAcc.Id);
        database.insert(objACRoleRec);
        //Create Opty Contact relationship
        OpportunityContactRole objOpRoleRec = new OpportunityContactRole(ContactId = objCon.Id, opportunityId = lstOppty[0].Id);
        database.insert(objOpRoleRec);
        
    }
    /*Test the CREATE scenario*/
    static testmethod void testExecuteSearchPullOperations(){
        Test.startTest();
        Contact objContact = [Select Id,FirstName,LastName,Email,TRAV_GU_Id__c,RecordTypeId,MobilePhone,Phone,Fax,Birthdate,TRAV_Agent_Preferences__c,TRAV_Broker__c FROM Contact LIMIT 1];
        Id idOpptyId = [SELECT Id FROM Opportunity LIMIT 1].id;
        Id idAcc = [SELECT Id FROM Account LIMIT 1].id;
        MockHttpResponseGenerator fakeCreateResp = new MockHttpResponseGenerator( 200,
                                                                                 'Complete',
                                                                                 Constants.CONTACT_CREATE_RESPONSE,
                                                                                 null);
        
        MockHttpResponseGenerator fakeGetResp = new MockHttpResponseGenerator( 200,
                                                                              'Complete',
                                                                              Constants.CONTACT_GET_RESPONSE,
                                                                              null);
        MockHttpResponseGenerator fakeUpdateResp = new MockHttpResponseGenerator( 200,
                                                                                 'Complete',
                                                                                 Constants.CONTACT_UPDATE_RESPONSE ,
                                                                                 null);
        Map<String, HttpCalloutMock> endpoint2TestResp =
            new Map<String,HttpCalloutMock>();
        endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact',fakeCreateResp);
        endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact/TRV00005400056',fakeGetResp);
        //endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact',fakeUpdateResp);
        HttpCalloutMock multiCalloutMock =
            new MultiRequestMock(endpoint2TestResp);
        
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);
        ContactSearchAndPullReturnWrapper wrapCS = NewContactSearchAndPullMDM.saveContact(objContact, idOpptyId);
        ContactSearchAndPullReturnWrapper wrapCS2 = NewContactSearchAndPullMDM.saveContact(objContact, idAcc);
        
        Test.stopTest();
    }
    /*Test the CREATE scenario from account*/
    static testmethod void testExecuteSearchPullOperationsForAccount(){
        Test.startTest();
        Contact objContact = [Select Id,FirstName,LastName,Email,TRAV_GU_Id__c,RecordTypeId,MobilePhone,Fax,Phone,Birthdate,TRAV_Agent_Preferences__c,TRAV_Broker__c FROM Contact LIMIT 1];
        Id idAcc = [SELECT Id FROM Account LIMIT 1].id;
        MockHttpResponseGenerator fakeCreateResp = new MockHttpResponseGenerator( 200,
                                                                                 'Complete',
                                                                                 Constants.CONTACT_CREATE_RESPONSE,
                                                                                 null);
        
        MockHttpResponseGenerator fakeGetResp = new MockHttpResponseGenerator( 200,
                                                                              'Complete',
                                                                              Constants.CONTACT_GET_MULTI_RESPONSE,
                                                                              null);
        MockHttpResponseGenerator fakeUpdateResp = new MockHttpResponseGenerator( 200,
                                                                                 'Complete',
                                                                                 Constants.CONTACT_UPDATE_RESPONSE ,
                                                                                 null);
        Map<String, HttpCalloutMock> endpoint2TestResp =
            new Map<String,HttpCalloutMock>();
        endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact',fakeCreateResp);
        endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact/TRV00005400056',fakeGetResp);
        //endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact',fakeUpdateResp);
        HttpCalloutMock multiCalloutMock =
            new MultiRequestMock(endpoint2TestResp);
        
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);
        ContactSearchAndPullReturnWrapper wrapCS2 = NewContactSearchAndPullMDM.saveContact(objContact, idAcc);
        
        Test.stopTest();
    }
    /*Test the 202 scenario*/
    static testmethod void testExecuteSearchPullOperationsFor202Scenario(){
        Test.startTest();
        Contact objContact = [Select Id,FirstName,LastName,Email,TRAV_GU_Id__c,TRAV_External_Id__c,RecordTypeId,MobilePhone,Phone,Fax,Birthdate,TRAV_Agent_Preferences__c,TRAV_Broker__c FROM Contact LIMIT 1];
        Id idOpptyId = [SELECT Id FROM Opportunity LIMIT 1].id;
        Id idAcc = [SELECT Id FROM Account LIMIT 1].id;
        MockHttpResponseGenerator fakeCreateResp = new MockHttpResponseGenerator(202,
                                                                                 'Complete',
                                                                                 Constants.CONTACT_CREATE_RESPONSE_202,
                                                                                 null);
        
        MockHttpResponseGenerator fakeGetResp = new MockHttpResponseGenerator(200,
                                                                              'Complete',
                                                                              Constants.CONTACT_GET_RESPONSE,
                                                                              null);
        Map<String, HttpCalloutMock> endpoint2TestResp =
            new Map<String,HttpCalloutMock>();
        endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact',fakeCreateResp);
        endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact/TRV00005400056',fakeGetResp);
        HttpCalloutMock multiCalloutMock =
            new MultiRequestMock(endpoint2TestResp);
        
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);
        ContactSearchAndPullReturnWrapper wrapCS = NewContactSearchAndPullMDM.saveContact(objContact, idOpptyId);
        NewContactSearchAndPullMDM.saveExistingContact(null, objContact,idOpptyId );
        ContactSearchAndPullReturnWrapper wrapCS2 = NewContactSearchAndPullMDM.saveContact(objContact, idAcc);
        NewContactSearchAndPullMDM.saveExistingContact('TRV00005400056', objContact,idAcc );
        Test.stopTest();
    }
        /*Test the 202 scenario*/
    static testmethod void testExecuteSearchPullOperationsFor202ACCScenario(){
        Test.startTest();
        Contact objContact = [Select Id,FirstName,LastName,Email,TRAV_GU_Id__c,TRAV_External_Id__c,RecordTypeId,MobilePhone,Phone,Fax,Birthdate,TRAV_Agent_Preferences__c,TRAV_Broker__c FROM Contact LIMIT 1];
		Id idAcc = [SELECT Id FROM Account LIMIT 1].id;
        MockHttpResponseGenerator fakeCreateResp = new MockHttpResponseGenerator(202,
                                                                                 'Complete',
                                                                                 Constants.CONTACT_CREATE_RESPONSE_202,
                                                                                 null);
        
        MockHttpResponseGenerator fakeGetResp = new MockHttpResponseGenerator(200,
                                                                              'Complete',
                                                                              Constants.CONTACT_GET_RESPONSE,
                                                                              null);
        Map<String, HttpCalloutMock> endpoint2TestResp =
            new Map<String,HttpCalloutMock>();
        endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact',fakeCreateResp);
        endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact/TRV00005400056',fakeGetResp);
        HttpCalloutMock multiCalloutMock =
            new MultiRequestMock(endpoint2TestResp);
        
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);

        ContactSearchAndPullReturnWrapper wrapCS2 = NewContactSearchAndPullMDM.saveContact(objContact, idAcc);
        NewContactSearchAndPullMDM.saveExistingContact(null, objContact,idAcc );
        Test.stopTest();
    }
    /*Test the FAILURE scenario*/
    static testmethod void testExecuteSearchPullOperationsForFAILUREScenario(){
        Test.startTest();
        Contact objContact = [Select Id,FirstName,LastName,Email,TRAV_GU_Id__c,RecordTypeId,MobilePhone,Fax,Birthdate,Phone,TRAV_Agent_Preferences__c,TRAV_Broker__c FROM Contact LIMIT 1];
        Id idOpptyId = [SELECT Id FROM Opportunity LIMIT 1].id;
        MockHttpResponseGenerator fakeFailureResp = new MockHttpResponseGenerator(201,
                                                                                  'Complete',
                                                                                  Constants.CONTACT_FAILURE_RESPONSE,
                                                                                  null);
        
        Map<String, HttpCalloutMock> endpoint2TestResp =
            new Map<String,HttpCalloutMock>();
        endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact',fakeFailureResp);
        
        HttpCalloutMock multiCalloutMock =
            new MultiRequestMock(endpoint2TestResp);
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);
        ContactSearchAndPullReturnWrapper wrapCS = NewContactSearchAndPullMDM.saveContact(objContact, idOpptyId);
        Test.stopTest();
    }
    /*Test the FAILURE scenario*/
    static testmethod void testExecuteSearchPullOperationsForFAILUREScenario2(){
        Test.startTest();
        Contact objContact = [Select Id,FirstName,LastName,Email,TRAV_GU_Id__c,RecordTypeId FROM Contact LIMIT 1];
        Id idOpptyId = [SELECT Id FROM Opportunity LIMIT 1].id;
        MockHttpResponseGenerator fakeFailureResp = new MockHttpResponseGenerator(201,
                                                                                  'Complete',
                                                                                  Constants.CONTACT_FAILURE_RESPONSE2,
                                                                                  null);
        
        Map<String, HttpCalloutMock> endpoint2TestResp =
            new Map<String,HttpCalloutMock>();
        endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact',fakeFailureResp);
        
        HttpCalloutMock multiCalloutMock =
            new MultiRequestMock(endpoint2TestResp);
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);
        ContactSearchAndPullReturnWrapper wrapCS = NewContactSearchAndPullMDM.saveContact(objContact, idOpptyId);
        Test.stopTest();
    }
    /*Test the UPDATE scenario*/
    static testmethod void testExecuteSearchPullOperationsForUPDATEScenario(){
        Test.startTest();
        Contact objContact = [Select Id,FirstName,LastName,Email,TRAV_GU_Id__c,TRAV_External_Id__c,RecordTypeId,MobilePhone,Phone,Fax,Birthdate,TRAV_Agent_Preferences__c,TRAV_Broker__c FROM Contact LIMIT 1];
        system.debug('**MDMID UPDATE**'+objContact.TRAV_External_Id__c);
        Id idOpptyId = [SELECT Id FROM Opportunity LIMIT 1].id;
        MockHttpResponseGenerator fakeUpdateResp = new MockHttpResponseGenerator(200,
                                                                                 'Complete',
                                                                                 Constants.CONTACT_UPDATE_RESPONSE,
                                                                                 null);
                
        MockHttpResponseGenerator fakeGetResp = new MockHttpResponseGenerator( 200,
                                                                              'Complete',
                                                                              Constants.CONTACT_GET_RESPONSE,
                                                                              null);
        Map<String, HttpCalloutMock> endpoint2TestResp =
            new Map<String,HttpCalloutMock>();
        endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact/TRV00005400056',fakeGetResp);
        endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact',fakeUpdateResp);
        
        HttpCalloutMock multiCalloutMock =
            new MultiRequestMock(endpoint2TestResp);
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);
        objContact.TRAV_External_Id__c = 'TRV00005400056';
        objContact.FirstName = 'Sayanka';
        update objContact;
        Test.stopTest();
    }
    
    /*Test 202 scenario where contact doesn't exist in SF*/
    static testmethod void test202ContactExistsInSF(){
        test.startTest();
        Account ibjAcc = [SELECT Id,TRAV_Account_External_Id__c FROM Account LIMIT 1];
        Id idAcc = [SELECT Id FROM Account LIMIT 1].id;
        Contact objContact = [Select Id,FirstName,LastName,Email,TRAV_GU_Id__c,TRAV_External_Id__c,RecordTypeId,MobilePhone,Phone,Fax,Birthdate,TRAV_Agent_Preferences__c,TRAV_Broker__c FROM Contact LIMIT 1];
        
        Id idOpptyId = [SELECT Id FROM Opportunity LIMIT 1].id;
        MockHttpResponseGenerator fakeGetResp = new MockHttpResponseGenerator(200,
                                                                              'Complete',
                                                                              Constants.CONTACT_GET_RESPONSE,
                                                                              null);
        MockHttpResponseGenerator fakeCreateMultiResp = new MockHttpResponseGenerator( 200,
                                                                                      'Complete',
                                                                                      Constants.CONTACT_CREATE_MULTI_RESPONSE,
                                                                                      null);
        Map<String, HttpCalloutMock> endpoint2TestResp =
            new Map<String,HttpCalloutMock>();
        endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact/TRV00005445655',fakeGetResp);
        endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact',fakeCreateMultiResp );
        HttpCalloutMock multiCalloutMock =
            new MultiRequestMock(endpoint2TestResp);
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);
        ContactSearchPullUtility.handleContactCreationAndAssociation(null, 'update',idOpptyId , 'TRV00005445655');
        ContactSearchAndPullReturnWrapper wrapCS = NewContactSearchAndPullMDM.saveContact(objContact, idAcc);
        String strTestCreate = ContactSearchPullUtility.handleContactCreationAndAssociation(null, 'create', idAcc, 'TRV00005445655');
        String strTestCreate2 = ContactSearchPullUtility.handleContactCreationAndAssociation(null, 'create', idOpptyId, 'TRV00005445655');
        test.stopTest();
    }
        /*Test the UPDATE scenario*/
    	static testmethod void testExecuteSearchPullUPOperations(){
        Test.startTest();
        Contact objContact = [Select Id,FirstName,LastName,Email,TRAV_GU_Id__c,RecordTypeId,MobilePhone,Fax,Birthdate,Phone,TRAV_Agent_Preferences__c,TRAV_Broker__c FROM Contact LIMIT 1];
        Id idOpptyId = [SELECT Id FROM Opportunity LIMIT 1].id;
        Id idAcc = [SELECT Id FROM Account LIMIT 1].id;
		MockHttpResponseGenerator fakeGetResp = new MockHttpResponseGenerator( 200,
                                                                              'Complete',
                                                                              Constants.CONTACT_GET_RESPONSE,
                                                                              null);
        MockHttpResponseGenerator fakeUpdateResp = new MockHttpResponseGenerator( 200,
                                                                                 'Complete',
                                                                                 Constants.CONTACT_UPDATE_RESPONSE ,
                                                                                 null);
        Map<String, HttpCalloutMock> endpoint2TestResp =
            new Map<String,HttpCalloutMock>();
        endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact',fakeUpdateResp);
        endpoint2TestResp.put('callout:MulesoftAgencyProxy/contact/TRV00005400056',fakeGetResp);
		HttpCalloutMock multiCalloutMock =
            new MultiRequestMock(endpoint2TestResp);
        
        Test.setMock(HttpCalloutMock.class, multiCalloutMock);
        ContactSearchAndPullReturnWrapper wrapCS = NewContactSearchAndPullMDM.saveContact(objContact, idOpptyId);
		Test.stopTest();
    }
    /*Test update scenario in MDM*/
    static testmethod void testUpdateInMDM(){
        test.startTest();
		ContactSearchPullUtility.updateContactInMDM('TRV00005445655', '');
        //check Contact Account scenario
        AccountContactRole objACRoleRec = [SELECT Id FROM AccountContactRole LIMIT 1];
        OpportunityContactRole objOpRoleRec = [SELECT Id FROM OpportunityContactRole LIMIT 1];
        Id idAcc = [SELECT Id FROM Account LIMIT 1].id;
        Id idOpptyId = [SELECT Id FROM Opportunity LIMIT 1].id;
        Contact objContact = [Select Id,FirstName,LastName,Email,TRAV_GU_Id__c,TRAV_External_Id__c,RecordTypeId,MobilePhone,Phone,Fax,Birthdate,TRAV_Agent_Preferences__c,TRAV_Broker__c FROM Contact LIMIT 1];
		Account objAcc1 = TestDataFactory.createProspect();
        system.assert(objAcc1 != null);
        objAcc1.TRAV_Account_External_Id__c = 'BM_1009253'; 
        insert(objAcc1);
        ContactSearchPullUtility.createAccountContactRole(objContact.id, objAcc1.id);
        ContactSearchPullUtility.checkContactAccountExists(objContact.id, idAcc);
        ContactSearchPullUtility.checkContactOpptyExists(objContact.id, idOpptyId);
        
        //Initialize wrapper classes
        ContactSearchAndPullRetrieveWrapper objWrap1 = new ContactSearchAndPullRetrieveWrapper();
        ContactSPUpdateResponseWrapper objWrap2 = new ContactSPUpdateResponseWrapper();
        objWrap2.Status = '200';
        objWrap2.Operation = 'CREATE';
        objWrap2.PtyName = 'Lisa ray';
        objWrap2.srcSysCd = 'xxxx';
        objWrap2.srcId = '1234';
   		objWrap2.Message = 'Agent already exists';
        objWrap2.MdmId = new List<String>{'TRv178899'};
        objWrap2.agncyIndId = new List<String>{'TRv178899'};
        ContactSearchAndPullResponseWrapper objWrap5 = new ContactSearchAndPullResponseWrapper();
        objWrap5.Status = '200';
        objWrap5.Operation = 'CREATE';
        objWrap5.PtyName = 'Lisa ray';
        objWrap5.srcSysCd = 'xxxx';
        objWrap5.srcId = '1234';
   		objWrap5.Message = 'Agent already exists';
        objWrap5.MdmId = 'TRv178899';
        objWrap5.agncyIndId = new List<String>{'TRv178899'};
        ContactSPCreateWrapper objWrap3 = new ContactSPCreateWrapper();
        ContactUpdateRequestWrapper objWrap4 = new ContactUpdateRequestWrapper();
        test.stopTest();
    }
  /*Test update scenario in MDM
    static testmethod void testUpdateScenariosInMDM(){
	
    }*/
}