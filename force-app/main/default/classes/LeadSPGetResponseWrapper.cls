/**
* This class is the Wrapper Class for Storing Response From GET API
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect                  Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty        US73994/US73995/US73996          24/01/2020      Original Version
* -----------------------------------------------------------------------------------------------------------
*/
public class LeadSPGetResponseWrapper {
public class Addresses {
		public String addressType;
		public String rawAddressLine1;
		public String rawAddressLine2;
		public String rawCity;
		public String rawState;
		public String rawZipCode;
		public String scrubbedAddressLine1;
		public String scrubbedAddressLine2;
		public String scrubbedCity;
		public String scrubbedState;
		public String scrubbedZipCode;
		public String scrubbedCounty;
		public String rawCounty;
		public String scrubbedCountry;
		public String rawCountry;
		public String rawZip4;
		public String rawZip10;
		public String scrubbedZip4;
		public String scrubbedZip10;
		public String certLocId;
	}

	public Integer recordCount;
	public List<ResultsSet> resultsSet;

    public class Contacts {
		public String contactFirstName;
        public String contactMiddleName;
        public String contactLastName;
        public String contactType;
            }

	public class ResultsSet {
		public CbeDetails cbeDetails;
	}

	public class Accounts {
		public String acctNumber;
		public String acctName;
		public String acctSICCd;
		public String acctNAICSCd;
		public String acctSrc;
		public String dunsNumber;
		public List<Contacts> acctAliasName;
		public List<Addresses> address;
		public List<Contacts> policies;
		public List<Contacts> submissions;
	}

	public class CbeDetails {
		public String certifiedBusinessId;
		public String dunsNumber;
		public String dunsStatus;
		public String dunsStatusReason;
		public String businessName;
		public String secondaryName;
		public List<Addresses> addresses;
		public String businessPhoneNumber;
		public String businessUrl;
		public String organizationName;
		public String sinceDate;
		public String organizationAltName;
		public String organizationAltDBAName;
		public String organizationNmScrub;
		public String organizationSICCd;
		public String organizationNAICSCd;
		public String organizationTINId;
		public String nbrOfEE;
		public String yearEstb;
		public String revenue;
		public String parentDunsNumber;
		public String headquarterDunsNumber;
		public String globalUltimateDunsNumber;
		public String domesticUltimateDunsNumber;
		public String creditScoreVersionNumber;
		public String creditScorePercentile;
		public String creditScorePoints;
		public String creditScoreRiskClass;
		public String financialStressScoreVersionNumber;
		public String financialStressScorePercentile;
		public String financialStressRawScore;
		public String financialStressScore;
		public String viabilityRating;
		public String bankruptcyIndicator;
		public String filingDate;
		public String bankruptcyFilingCount;
		public String orgTyCd;
		public String codeForNbrOfEmployees;
		public String yearsInBusiness;
		public String nbrOfLocations;
		public String stockTickerSymbol;
		public String sicCdDesc;
		public String sicCdExt;
		public String publicOrPrivateBusiness;
		public String subsidiary;
		public String businessStatusCode;
		public String employeeHere;
		public String importExportCode;
		public String codeForEstimation;
		public String ownerOrRentCode;
		public List<Contacts> contacts;
		public List<Contacts> orgIndusClass;
		public List<Accounts> accounts;
	}
}