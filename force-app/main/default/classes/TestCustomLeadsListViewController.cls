/**
 * This class is used as the test class for the CustomLeadsListViewController
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------                    
 * Shashank Shastri       US60972              06/15/2019       Original Version
 * -----------------------------------------------------------------------------------------------------------
 */
@isTest
public class TestCustomLeadsListViewController{
    /**
    * This is the setupData method, where all the records needed for the successful execution 
    * of the code will be created.
    * @param : None
    * @return : None
    */ 
    public static void setupData(){
        //Create List of Lead Records owned by the logged in User
        List<Lead> listOfLeadsOwned = new List<Lead>();
        Lead leadRec;
        //Creating 100 leads to test functionality in bulk.
        for(integer i = 0; i < 100 ; i++){
            leadRec = new Lead();
            leadRec = TestDataFactory.createLead();
            leadRec.LastName += i;
            leadRec.TRAV_Certified_Business_Id__c += i;
            listOfLeadsOwned.add(leadRec);
        }
        //Inserting a list of 100 leads
        database.insert(listOfLeadsOwned);
        // Following few Lead records to get the Lead Record that the logged in user is following.
        List<EntitySubscription> listToFollow = new List<EntitySubscription>();
         for(Lead leadToFollow : listOfLeadsOwned){
             //CreateEntitySubscription Record
             EntitySubscription followRec = new EntitySubscription(ParentId = leadToFollow.Id, SubscriberId = userinfo.getUserId());
             listToFollow.add(followRec);
         }
         database.insert(listToFollow);
    }
    /**
    * This Method tests the Listview Functionality. For a logged in user it will return all the records owned by the user.
    * @param : None
    * @return : None
    */    
    public static testMethod void testViewRecordsOwned(){
        //Create Test User 
        User userBINation = TestDataFactory.createTestUser('BI - National Accounts', 'BI Nation', 'User');
        insert userBINation;
        //Running the code in the BI -National Accounts user context
        system.Runas(userBINation){
        //Calling setupData method to create 100 Leads    
            setupData();
            system.assertEquals(EntitySubscriptionSelector.getSubscriptionList(UserInfo.getUserId()).size(), 100);
            //Invoking the CustomLeadsListViewController to get the list of record to be displayed on the UI
            List<Lead> leadsIFollow = CustomLeadsListViewController.fetchLeadsIFollow();
            system.assertEquals(leadsIFollow.size(), 100);
        }
    }
    /**
    * This Method tests the Listview Functionality. For a logged in user it will return all the records owned by the user.
    * @param : None
    * @return : None
    */    
    public static testMethod void testViewRecordsFollowed(){
        //Create Test User 
        User userBINation = TestDataFactory.createTestUser('BI - National Accounts', 'BI Nation', 'User');
        insert userBINation;
        User userBINationNew = TestDataFactory.createTestUser('BI - National Accounts', 'BI Nation', 'New User');
        insert userBINationNew;
        //Running the code in the BI -National Accounts user context
        system.Runas(userBINation){
        //Calling setupData method to create 100 Leads    
            setupData();
            //Update the owner for Leads
            List<Lead> listOfLeadsOwned = [SELECT Id, OwnerId from Lead WHERE OwnerId =: Userinfo.getUserId() LIMIT 100];
            for(Lead leadRec : listOfLeadsOwned){
                leadRec.ownerId = userBINationNew.Id;
            }
            database.update(listOfLeadsOwned);
            List<Lead> leadsIFollow = CustomLeadsListViewController.fetchLeadsIFollow();
            system.assertEquals(leadsIFollow.size(), 100);
        }
    }
}