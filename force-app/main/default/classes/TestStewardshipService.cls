/**
* Test class for OpportunityService Class
*
* @Author : Deloitte Consulting
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Shashank Agarwal       US73728              01/28/2020       Original Version
* -----------------------------------------------------------------------------------------------------------
*/
@isTest
private class TestStewardshipService {
    
    /**
* This method will test scenarios when opportunity is updated
* @param no parameters
* @return void
*/
    
    @TestSetup
    static void setup(){
       	User objUsr1 = TestDataFactory.createTestUser('BI - Support','TestBISupport','LastTestBISupport');
        insert objUsr1;
        User objUsr2 = TestDataFactory.createTestUser('BI - Support','TestBISupport','LastTestBISupport');
        insert objUsr2;
        User objUsr3 = TestDataFactory.createTestUser('BI - Support','TestBISupport','LastTestBISupport');
        insert objUsr3;
    }
    
    static testmethod void testStewardshipOwnerUpdate() {
        
        TRAV_Trigger_Setting__c objCustomsetting = TestDataFactory.createTriggersetting();
        objCustomsetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        objCustomsetting.TRAV_Trigger_Name__c = 'StewardshipTrigger';
        insert objCustomsetting;
        
        List<User> lstUser = [Select Id from User ];
        
        //create account record
        Account objProspectAccount = TestDataFactory.createProspect(); 
        List<Account> lstAccount = new List<Account>{objProspectAccount};
            insert lstAccount;
        
        List<Opportunity> lstOpportunity = TestDataFactory.createOpportunities(1, objProspectAccount.Id, 'BI-NA');
        lstOpportunity[0].StageName = Constants.SUBMISSION;
        lstOpportunity[0].OwnerId = lstUser[0].Id;
        insert lstOpportunity;
        
        TRAV_Stewardship__c objStewardship = new TRAV_Stewardship__c();
        
        objStewardship.TRAV_Related_Opportunity__c = lstOpportunity[0].Id;
        objStewardship.TRAV_Related_Account__c = lstAccount[0].Id;
        objStewardship.OwnerId = lstUser[0].Id;
        insert objStewardship;
        
        Test.startTest();
        objStewardship.TRAV_CAE_Name__c = lstUser[1].Id;
        update objStewardship;
        objStewardship.TRAV_CAE_Name__c = lstUser[2].Id;
        update objStewardship;
        objStewardship.TRAV_CAE_Name__c = Null;
        update objStewardship;
        Test.stopTest();
        
        
        
        
    }
    
   
    
}