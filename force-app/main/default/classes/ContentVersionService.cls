/**
* Utility Class for Opportunity Trigger Handler. This class will have logic for all the implementations.
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer		      User Story/Defect	  Date		    Description
* Shashank Agarwal    US62549			  07/01/2019    Added Method taskCreationOnInsertion
* -----------------------------------------------------------------------------------------------------------              
* 
*/ 
public class ContentVersionService {
    /**
* This method will add error on notes when event type is 'Lync Event' When a record is Updated. 
* @param lstNewNote - List of new values in the for the records.
* @return void - returns void .
*/
    public static void addErrorLyncOnNoteModified(List<ContentVersion> lstNewContentVersion,Map<Id,ContentVersion> mapOldlstNewContentVersion){
        
        final string CLASSNAME = 'NoteService';
        final string METHODNAME = 'addErrorLyncOnNoteModified';
        Set<Id> setEventId = new Set<Id>();
        Set<Id> setIdWithTypeLyncEvent = new Set<Id>();
        Set<Id> setContentDocumentId = new Set<Id>();
        List<ContentDocumentLink> lstContentDocumentLink = new List<ContentDocumentLink>();
        Set<Id> setConventDocumentIdToAddError = new Set<Id>();
        try{
            Set<Id> setUserId = new Set<Id>();
            setUserId.add(UserInfo.getUserId());
            String userProfile = UserSelector.getUserProfileName(setUserId)[0].Profile.Name;
            if(Constants.BIBSIPROFILES.contains(userProfile)){
                for(ContentVersion objContentVersion : lstNewContentVersion){
                    if(objContentVersion.FileType == 'SNOTE'){
                        setContentDocumentId.add(objContentVersion.ContentDocumentId);
                    }
                }
                
                if(!setContentDocumentId.isEmpty()){
                    lstContentDocumentLink = ContentDocumentLinkSelector.getContentDocumentLinkForError(setContentDocumentId);
                }  
                
                for(ContentDocumentLink objContentDocumentLink : lstContentDocumentLink ){
                    if(String.valueOf(objContentDocumentLink.LinkedEntityId.getsobjecttype()) == 'Event'){
                        setEventId.add(objContentDocumentLink.LinkedEntityId);
                    }
                }
                
                List<Event> lstEvent = new List<Event>();
                
                if(!setEventId.isEmpty()){
                    lstEvent = EventSelector.getEventAddingErrorOnNonEditableNotes(setEventId);
                }
                
                for(Event objEvent : lstEvent){
                    setIdWithTypeLyncEvent.add(objEvent.Id);
                }
                
                for(ContentDocumentLink objContentDocumentLink : lstContentDocumentLink){
                    if(setIdWithTypeLyncEvent.contains(objContentDocumentLink.LinkedEntityId)){
                        setConventDocumentIdToAddError.add(objContentDocumentLink.ContentDocumentId);
                    }
                }
                
                if(lstNewContentVersion != Null && mapOldlstNewContentVersion == Null){
                    for(ContentVersion objContentVersion: lstNewContentVersion){
                        //Check if Content Document Id is present in the set. 
                        if(setConventDocumentIdToAddError.contains(objContentVersion.ContentDocumentId)){
                            //Adding Error on Content Version
                            objContentVersion.addError('Notes are not updateable for Event type Lync Event');
                        }
                    }// End For Loop
                }
                if(lstNewContentVersion != Null && mapOldlstNewContentVersion != Null){
                    for(ContentVersion objContentVersion: lstNewContentVersion){
                        //Check if Content Document Id is present in the set. 
                        if(setConventDocumentIdToAddError.contains(objContentVersion.ContentDocumentId)){
                            //Adding Error on Content Version
                            objContentVersion.addError('Notes are not updateable for Event type Lync Event');
                        }
                    }// End For Loop
                }
            }
			If(Test.isRunningTest() && lstNewContentVersion.isEmpty())
            {
               throw new QueryException('For testing purpose');
            } 
        }catch(Exception objExp){
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        } // End of catch
        
    } //End Of Method   
    
}