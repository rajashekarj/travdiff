/**
* Utility Class for Event Trigger Handler. This class will have logic for all the implementations.
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------
* Isha Shukla		      US62215             05/24/2019       Original Version
* Bhanu	Reddy			  PO Feedback		  07/29/2019	   Added a new coniditon to check type of the event
* Shashank Agarwal        US63739             07/30/2019       Added Method insertEventRelationForStewardship
* Shashank Agarwal        US73728             01/24/2020       Updated Method insertStewardshipRecord
* Shashank Agarwal        US73728             01/27/2020       Updated Method insertEventRelationForStewardship
* Shashank Agarwal        US73728             01/27/2020       Added Method updateTimeOfStewardshipEvent
* Evan Wilcher			  US81058			  04/15/2020	   Added Method updateType
*/
public class EventService {


    /**
* This method will insert stewardship record when event is created and stores it's id in stewarship id field on event
* @param lstNewEvent List is passed to this method that need to be manipulate having new records which are inserted
* @return void
*/
    public static void insertStewardshipRecord(List<Event> lstNewEvent) {
        final string METHODNAME = 'insertStewardshipRecord';
        final string CLASSNAME = 'EventService';
        //Retrieving stewardship record type of event
        Id objEventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('Stewardship').getRecordTypeId();
        //Map used to relate event and the stewardship record
        Map<Id,TRAV_Stewardship__c> mapIdToEventDetails = new Map<Id,TRAV_Stewardship__c>();
        //Map of event to relted opportnity id to tag stewardhsip on Opportunity.
        Map<Id,Id> mapEventIdToOpportunity = new Map<Id,Id>();
        //Map of event to related opportunity to stamp Oppty field values on Stewardship
		Map<Id,Id> mapEventIdToOpptySTWDPlanning = new Map<Id,Id>();
        //Map of oppty Id and Opportunity
		Map<Id,Opportunity> mapEventIdToOppty = new Map<Id,Opportunity>();
        //Stewarship List which will be inserted
        List<TRAV_Stewardship__c> lstStewardship = new List<TRAV_Stewardship__c>();
        TRAV_Stewardship__c objStewardship;
        try {

            Set<Id> setOpportunityId = new Set<Id>();
            Map<Id,Opportunity> mapIdToOpportunity = new Map<Id,Opportunity>();


            for(Event objEvent : lstNewEvent) {
                system.debug('>>whoid'+objEvent.whoid);
                //If event is of stewardship record type then
                //populating map of event id to stewardship record to create it
                //US73728 -- Moved to constants value of objEvent.Subject for comparision
                if((objEvent.RecordTypeId == objEventRecordTypeId || objEvent.Type.EqualsIgnorecase(Constants.STEWARDSHIP)) && (objEvent.Subject == Constants.StewardshipEventSubject )) {
                    objStewardship = new TRAV_Stewardship__c();
                    objStewardship.TRAV_Event_Id__c = objEvent.Id;
                    objStewardship.TRAV_Event_Subject__c = objEvent.Subject;

                    String strOpportunityId = objEvent.WhatId;
                    //Checking if opportunity is related to this event and tagging to stewardhsip
                    if(strOpportunityId != null && strOpportunityId.startsWith(Label.TRAV_OpportunityIdPrefix)) {
                        objStewardship.TRAV_Related_Opportunity__c = objEvent.WhatId;
                        mapEventIdToOpportunity.put(objEvent.Id,objEvent.WhatId);
                        //if(objEvent.Subject == 'Stewardship Planning Meeting'){
                            mapEventIdToOpptySTWDPlanning.put(objEvent.Id,objEvent.WhatId);
                        //}

                    }
                    mapIdToEventDetails.put(objEvent.Id,objStewardship);
                }
            }
			/*Prepare map for opportunity*/
            //check if event oppty map is not null
            if(mapEventIdToOpptySTWDPlanning != null && !mapEventIdToOpptySTWDPlanning.keySet().isEmpty()){
                //loop through Opportunity
                for(Opportunity o : [SELECT Id,OwnerId, TRAV_Industry_Specialization_Group__c, AccountId FROM Opportunity WHERE ID IN :mapEventIdToOpptySTWDPlanning.values()]){
                    //Null check
                    if(o != null){
                        mapEventIdToOppty.put(o.Id,o);
                    }
                }
            }
            /*Manipulate map for events related to opportunity*/
            //Check if final map is not null
            if(!mapIdToEventDetails.keySet().isEmpty()){
                //Loop through all events
                for(Event evt : [SELECT Id from Event where Id IN :mapIdToEventDetails.keySet()]){
                    //check if the event is present in stewardship planning map
                    if(mapEventIdToOpptySTWDPlanning != null){
                        if(mapEventIdToOpptySTWDPlanning.containsKey(evt.id)){
                            TRAV_Stewardship__c ste = new TRAV_Stewardship__c();
                            if(mapIdToEventDetails.containsKey(evt.Id)){
                                //ste = mapIdToEventDetails.get(evt.Id);
                                Id opptyId = mapEventIdToOpptySTWDPlanning.get(evt.Id);
                                Opportunity ot;
                                if(opptyId != null){
                                     ot = mapEventIdToOppty.get(opptyId);
                                }
                                if(ot != null){
                                    mapIdToEventDetails.get(evt.Id).OwnerId = ot.OwnerId;
                                    mapIdToEventDetails.get(evt.Id).TRAV_Industry__c = ot.TRAV_Industry_Specialization_Group__c != null ? ot.TRAV_Industry_Specialization_Group__c : null;
                                    mapIdToEventDetails.get(evt.Id).TRAV_Related_Account__c = ot.AccountId != null ? ot.AccountId : null ;

                                }
                            }
                        }
                    }

                }
            }

            //Inserting stewardship records
            if(mapIdToEventDetails.values().size() > 0) {
                Database.SaveResult[] lstSaveResult = Database.insert(mapIdToEventDetails.values(), false);
                ExceptionUtility.logDMLError(CLASSNAME, METHODNAME, lstSaveResult);
            }
            List<Event> lstEventToUpdate = new List<Event>();
            Event objEventRecord;
            //Map which will be used to relate opportunity and stewardship
            Map<Id,Id> mapOpportunityToStewardship = new Map<Id,Id>();
            for(Id objEventId : mapIdToEventDetails.keySet()) {
                //Populating stewardship id field on event to store stewardship record id
                objEventRecord = new Event();
                objEventRecord.TRAV_Stewardship_Id__c = mapIdToEventDetails.get(objEventId).Id;
                objEventRecord.Id = objEventId;
                lstEventToUpdate.add(objEventRecord);

                //Populating relating map of Opportunity with stewardship record id
                if( mapEventIdToOpportunity.keySet().size() > 0 &&
                   mapEventIdToOpportunity.containsKey(objEventId) &&
                   !mapOpportunityToStewardship.containsKey(mapEventIdToOpportunity.get(objEventId)) ) {
                       mapOpportunityToStewardship.put(mapEventIdToOpportunity.get(objEventId),mapIdToEventDetails.get(objEventId).Id);
                   }
            }
            //Relating Opportunity with Stewardship newly created
            List<Opportunity> lstOpportunityToUpdate = new List<Opportunity>();
            if(mapOpportunityToStewardship.keySet().size() > 0 ) {
                Opportunity objOpportunityRecord;
                for(Id objOpportunityId : mapOpportunityToStewardship.keySet()) {
                    objOpportunityRecord = new Opportunity();
                    objOpportunityRecord.TRAV_Stewardship__c = mapOpportunityToStewardship.get(objOpportunityId);
                    objOpportunityRecord.Id = objOpportunityId;
                    lstOpportunityToUpdate.add(objOpportunityRecord);
                }
            }
            //Updating event records
            if(lstEventToUpdate.size() > 0) {
                Database.SaveResult[] lstSaveResult = Database.update(lstEventToUpdate, false);
                ExceptionUtility.logDMLError(CLASSNAME, METHODNAME, lstSaveResult);
            }
            //Updating Opportunity records
            if(lstOpportunityToUpdate.size() > 0) {
                Database.SaveResult[] lstSaveResult = Database.update(lstOpportunityToUpdate, false);
                ExceptionUtility.logDMLError(CLASSNAME, METHODNAME, lstSaveResult);
            }

        }//End of try
        catch(exception objExp) {
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        }//End of catch

    }
    /**
* This method will add event Realation for stewardship
* @param lstNewEvent List is passed to this method that need to be manipulate having new records which are inserted
* @return void
*/

    public static void insertEventRelationForStewardship(List<Event> lstNewEvent){
        final string METHODNAME = 'insertEventRelationForStewardship';
        final string CLASSNAME = 'EventService';
        Set<ID> setOpportunityID = new Set<Id>();
        Map<Id,List<OpportunityTeamMember>> mapOpptyToOpptyTeamMember = new Map<Id,List<OpportunityTeamMember>>();
        List<OpportunityTeamMember> lstOpportunityTeam = new List<OpportunityTeamMember>();
        try{
            for(Event objEvent : lstNewEvent){
                if(objEvent.WhatId.getSObjectType().getDescribe().getName() == Constants.OpportunityObject && Constants.EventTypeStewardship.contains(objEvent.Type)){
                    setOpportunityID.add(objEvent.WhatId);
                }
            }

            lstOpportunityTeam = OpportunityTeamMemberSelector.getOpportunityTeamMembersForStweardshipEvents(setOpportunityID);

            // Modification for US73728
            List<TRAV_AttendeesForStewardshipMeeting__mdt> lstAttendeesMetadata = Database.query(Constants.AttendeesMetadataForStewardshipMeetingQuery);
            // Modification for US73728
            Map<String,String> mapSubjectToAttendeesRole = new Map<String,String>();
            // Modification for US73728
            for(TRAV_AttendeesForStewardshipMeeting__mdt objAttendeesMetadata : lstAttendeesMetadata){
                mapSubjectToAttendeesRole.put(objAttendeesMetadata.MasterLabel,objAttendeesMetadata.TRAV_Attendees_Team_Role__c);
            }

            for(Id objOppId : setOpportunityId){
                List<OpportunityTeamMember> lstOpportunityTeamMember = new List<OpportunityTeamMember>();
                for(OpportunityTeamMember objOpptyTeamMember : lstOpportunityTeam ){
                    if(objOpptyTeamMember.OpportunityId == objOppId){
                        lstOpportunityTeamMember.add(objOpptyTeamMember);
                    }
                }
                mapOpptyToOpptyTeamMember.put(objOppId, lstOpportunityTeamMember);
            }

            List<EventRelation> lstEventRelation = new List<EventRelation>();
            for(Event objEvent : lstNewEvent){
                if(objEvent.WhatId.getSObjectType().getDescribe().getName() == Constants.OpportunityObject && Constants.EventTypeStewardship.contains(objEvent.Type)){
                    List<OpportunityTeamMember> lstTeamMember = mapOpptyToOpptyTeamMember.get(objEvent.WhatId);
                    for(OpportunityTeamMember objTeamMember : lstTeamMember){
                        // Modification for US73728
                        if(mapSubjectToAttendeesRole.containsKey(objEvent.Subject) && mapSubjectToAttendeesRole.get(objEvent.Subject).contains(objTeamMember.TeamMemberRole)){
                            EventRelation objEventRelation = new EventRelation();
                            objEventRelation.EventId = objEvent.Id;
                            objEventRelation.RelationId = objTeamMember.UserId;
                            objEventRelation.isInvitee = true;
                            objEventRelation.isParent = false;
                            lstEventRelation.add(objEventRelation);
                        }
                    }
                }
            }
            if( !lstEventRelation.isEmpty() ) {
                Database.SaveResult[] lstSaveResult = Database.insert(lstEventRelation, false);
                ExceptionUtility.logDMLError(CLASSNAME, METHODNAME, lstSaveResult);
            }//End of if
        }//End of try
        catch(exception objExp) {
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        }//End of catch
    }

     /**
* This method will capture if related contact is agency contact
* @param lstNewEvent List is passed to this method that need to be manipulate having new records which are inserted
* @return void
*/
    public static void fetchPrimaryContactId(List<Event> lstNewEvent,Map<Id,Event> mapOldEvents){
        final string METHODNAME = 'fetchPrimaryContactId';
        final string CLASSNAME = 'EventService';
        Map<Id,Id> mapContactIds = new Map<Id,Id>();
        try{
            for(Event objEvent : lstNewEvent){
                String contactId = objEvent.WhoId;
                //checking if whoid is related to contact when inserted and updated
                if(objEvent.WhoId != null && contactId.startsWith('003') &&
                   ( mapOldEvents == null || mapOldEvents.get(objEvent.Id).WhoId != objEvent.WhoId) ){
                  mapContactIds.put(objEvent.Id,objEvent.WhoId);
                }
            }
            if( mapContactIds.values().size() > 0 ) {
                List<Contact> lstContact = new List<Contact>();
                Set<Id> setContIds = new Set<Id>();
                setContIds.addAll(mapContactIds.values());
                lstContact = ContactSelector.fetchAgencyContactsById(setContIds);
                Map<Id,String> mapAgencyContacts = new  Map<Id,String>();
                for(Contact objContact : lstContact) {
                    mapAgencyContacts.put(objContact.Id,objContact.Name);
                }
                if(mapAgencyContacts.keySet().size() > 0) {
                    for(Event objEvent : lstNewEvent){
                        if(mapContactIds.containsKey(objEvent.Id) ){
                            //if whoid is related to agency contact then marking the flag true
                             if( mapAgencyContacts.containsKey(mapContactIds.get(objEvent.Id)) ){
                               objEvent.TRAV_Related_Agency_Contact__c = true;
                               objEvent.TRAV_Primary_Contact_Name__c = mapAgencyContacts.get(mapContactIds.get(objEvent.Id));
                                 system.debug('objEvent.TRAV_Primary_Contact_Name__c '+objEvent.TRAV_Primary_Contact_Name__c );
                             }else {
                                 //if whoid is not related to agency contact then making this flag false
                                 objEvent.TRAV_Related_Agency_Contact__c = false;
                             }
                        }
                    }
                }
            }//End of if
        }//End of try
        catch(exception objExp) {
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        }//End of catch
    }

    /**
* This method will update the ACtivity date time is the event is of Stewardship Or Pre-Stewardship Type to the Opportuity Owner's Time Zone
* @param lstNewEvent List is passed to this method that need to be manipulate having new records which are inserted
* @return void
*/
    public static void updateTimeOfStewardshipEvent(List<Event> lstNewEvent){
        final string METHODNAME = 'updateTimeOfStewardshipEvent';
        final string CLASSNAME = 'EventService';
        Set<ID> setOpportunityID = new Set<Id>();
        Map<Id,Opportunity> mapIdVsOpportunityOwnerDetails = new Map<Id,Opportunity>();

        try{
            for(Event objEvent : lstNewEvent){
                if(objEvent.WhatId.getSObjectType().getDescribe().getName() == Constants.OpportunityObject && Constants.EventTypeStewardship.contains(objEvent.Type)){
                    setOpportunityID.add(objEvent.WhatId);
                }
            }

            List<Event> lstEvent = new List<Event>();

            if(!setOpportunityID.isEmpty()){
                mapIdVsOpportunityOwnerDetails = OpportunitySelector.getOpportunitiesOwner(setOpportunityID);
            }

            for(Event objEvent : lstNewEvent){
                if(objEvent.WhatId.getSObjectType().getDescribe().getName() == Constants.OpportunityObject && Constants.EventTypeStewardship.contains(objEvent.Type)){
                    Date dtEventDate = objEvent.ActivityDateTime.date();
                    Time dtStartTime = Time.newInstance(8, 0, 0, 0);
                    TimeZone targetTimezone = TimeZone.getTimeZone(mapIdVsOpportunityOwnerDetails.get(objEvent.WhatId).Owner.TimeZoneSidKey);
                    System.debug('>>'+targetTimezone);
                    // The dtEventDate is used here to decide whether
                    // Daylight Savings should apply.
                    Integer offsetSeconds = targetTimezone.getOffset(dtEventDate) /1000;
                    System.debug('>>'+offsetSeconds);
                    dtStartTime = dtStartTime.addSeconds(-offsetSeconds);
                    System.debug('>>'+dtStartTime);
                    objEvent.ActivityDateTime = Datetime.newInstanceGmt(dtEventDate, dtStartTime);
                    System.debug('>>'+objEvent.ActivityDateTime);
                }
            }
            /*if( !lstEvent.isEmpty() ) {
                Database.SaveResult[] lstSaveResult = Database.update(lstEvent, false);
                ExceptionUtility.logDMLError(CLASSNAME, METHODNAME, lstSaveResult);
            }//End of if */
        }//End of try
        catch(exception objExp) {
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objExp, null);
        }//End of catch
    }

    /**
* This method will update the Event type picklist 
* @param lstNewEvent List is passed to this method that need to be manipulate having new records which are inserted
* @return void
*/
    public static void updateType(List<Event> listNewEvent){
        final string METHODNAME = 'updateType';
        final string CLASSNAME = 'EventService';
        
        //updating all event.type fields passed in.
        for (Event newEvent : listNewEvent){
            newEvent.Type = newEvent.TRAV_EventType__c;
        }
    }

}