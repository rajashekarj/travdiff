/**
* This apex class has different methods to create data for test classes
*

* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect      Date             Description
* -----------------------------------------------------------------------------------------------------------
* Bhanu Reddy               N/A              05/10/2019        Original Version
* Isha Shukla               US59898          06/03/2019        Added methods to create user,opportunity,opportunity team
* Bhanu Reddy               US62952          07/01/2019        Added a new method for producer record creations
* Erec Lawrie               US62419          07/02/2019        Added new methods for Opportunity Carrier
* Bhanu Reddy               US62952          07/03/2019        Added new Bulk method for Account and producer creation
* Siddharth Menon           TestClass        08/23/2019       Added new method to create an AccountProducerRelation record
* Sayanka Mohanty           US666430/US66750 09/09/2019       Added a new method fot creating Policies(createPolicies)
                            /US66752
* Siddharth Menon			US68464			 10/07/2019		  Added method createTestAdminUser
*/
@isTest
public class TestDataFactory {
    /**
    * This method is used to test the bulk producer records fo test classes
    *   @return void - returns void .
    */
    public static list<Trav_Producer__c> createProducersBluk(){
        list<TRAV_Producer__c> listProducers = new list<TRAV_Producer__c>();
        TRAV_Producer__c objProducer;

        //create 50 Producers
        for(integer i=0;i<50;i++){
            objProducer = new TRAV_Producer__c();
            objProducer.name = 'The Producer'+string.valueof(i);
            objProducer.TRAV_Producer_Code__c = 'Code'+string.valueof(i);
            listProducers.add(objProducer);
        }
        return listProducers;
    }
    //create batch custom setting
    public static void createRenewalBatchsetting(){
        Renewal_Batch__c objRenewalBatch = new  Renewal_Batch__c();
        objRenewalBatch.SetupOwnerId=UserInfo.getOrganizationId();
        objRenewalBatch.Last_Sucessful_Btach_Run__c = system.today();
        upsert  objRenewalBatch ;

      //  insert new Renewal_Batch__c(SetupOwnerId=UserInfo.getOrganizationId(), Last_Sucessful_Btach_Run__c = system.today());


    }

    /**
    * This method is used to test the bulk Account records fo test classes
    *   @return void - returns void .
    */
    public static list<Account> createAgencyBrickMortorBatch(){
        list<Account> listBrickandMortarAccounts = new List<Account>();
        Id AgencyRecordtype =
          Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency Location').getRecordTypeId();
        for(integer i;i<25;i++){
            listBrickandMortarAccounts.add(new Account(name = 'Agency Account'+string.valueof(i),recordTypeID = AgencyRecordtype));
        }
        return listBrickandMortarAccounts;
    }

    /**
    * This method is used to create an account producer relation record.
    *   @params producerId,accountId.
    *   @return void - returns void .
    */
    public static TRAV_Account_Producer_Relation__c createAccountProducerRelation(Id producerId, Id accountId)
    {
        TRAV_Account_Producer_Relation__c objAccountProducerRelation= new TRAV_Account_Producer_Relation__c(Name='The Account Producer Relation', TRAV_Producer__c=producerId,TRAV_Account__c=accountId);
        return objAccountProducerRelation;
    }

     /**
    * This method is used to create an account contact relation record.
    *   @params producerId,accountId.
    *   @return void - returns void .
    */
    public static AccountContactRelation createAccountContactRelation(Id accountId, Id contactId)
    {
        AccountContactRelation objAccountProducerRelation= new AccountContactRelation(AccountId=accountId,ContactId=contactId);
        return objAccountProducerRelation;
    }
    /**
    * This method is used to test the bulk Account records fo test classes
    *   @return void - returns void .
    */
    public static list<Account> createAgencyHeadquarterBatch(){
        list<Account> listAgencyHeaddquarterAccounts = new List<Account>();
        Id AgencyRecordtype =
          Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency Headquarter').getRecordTypeId();
        for(integer i;i<25;i++){
            listAgencyHeaddquarterAccounts.add(new Account(name = 'Agency Account'+string.valueof(i),recordTypeID = AgencyRecordtype));
        }
        return listAgencyHeaddquarterAccounts;
    }

    /**
    * This method is used to test the bulk Account records fo test classes
    *   @return void - returns void .
    */
    public static list<Account> createAgencyHeadQuatersBatch(){
        list<Account> listAgencyHQAccounts = new List<Account>();

        Id AgencyRecordtype =
          Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency Organization').getRecordTypeId();

        for(integer i;i<25;i++){
           listAgencyHQAccounts.add( new Account(name = 'Agency Account'+string.valueof(i),recordTypeID = AgencyRecordtype));
        }

        return listAgencyHQAccounts;
    }

    /**
    * This method is used to test the producer records fo test classes
    * @return void - returns void .
    */
    public static TRAV_Producer__c createProducers(){
        TRAV_Producer__c objProducer = new TRAV_Producer__c();
        objProducer.name='The Producer';
        return objProducer;
    }

    //create an agency account
    public static Account createAgency()
    {
        Id AgencyRecordtype =
          Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency Location').getRecordTypeId();

        Account agency= new Account(name = 'Agency Account',recordTypeID = AgencyRecordtype);
        return agency;
    }

    //create a prospect account
    public static Account createProspect()
    {
        Id ProspectRecordtype =
          Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();

        Account agency= new Account(name = 'Prospect Account',recordTypeID = ProspectRecordtype);
        return agency;
    }
    //create a TPA account
    public static Account createTPAAccount()
    {
        Id tpaRecordtype =
          Schema.SObjectType.Account.getRecordTypeInfosByName().get('TPA Location').getRecordTypeId();

        Account agency= new Account(name = 'TPA Account',recordTypeID = tpaRecordtype);
        return agency;
    }
   //create a TPA account
    public static Account createTPALocationAccount(string accname)
    {
        Id ProspectRecordtype =
          Schema.SObjectType.Account.getRecordTypeInfosByName().get('TPA Location').getRecordTypeId();

        Account ObjTPAAccount= new Account(name = accname,recordTypeID = ProspectRecordtype);
        return ObjTPAAccount;
    }

    //lead with certified business id
    public static Lead createLead()
    {
        Lead leadin = new Lead();
        leadin.lastname = 'Jhon';
        leadin.TRAV_Certified_Business_Id__c = 'A101';
        leadin.status ='new';
        leadin.company = 'Travelers';
        leadin.Street = 'NY Street';
        leadin.City = 'NY';
        leadin.PostalCode = '12345';
        leadin.Country = 'USA';
        leadin.TRAV_Effective_Date__c = system.today();
        return leadin;
    }
    
    public static List<Lead> createLeads(Integer n)
    {
        List<Lead> listLead=new List<Lead>();
        for(Integer i=0;i<n;i++){
            Lead leadin = new Lead();
            leadin.lastname = 'Jhon'+i;
            leadin.TRAV_Certified_Business_Id__c = 'A10'+i;
            leadin.status ='new';
            leadin.company = 'Travelers'+i;
            leadin.Street = 'NY Street';
            leadin.City = 'NY';
            leadin.PostalCode = '12345';
            leadin.Country = 'USA';
            leadin.TRAV_Effective_Date__c = system.today()+i;
            listLead.add(leadin);
        }
        
        return listLead;
    }

    //insert custom setting
    public static TRAV_Trigger_Setting__c createTriggersetting()
    {
        TRAV_Trigger_Setting__c triggersetting = new TRAV_Trigger_Setting__c();
        triggersetting.TRAV_Skip_Trigger_Org_Wide__c ='AccountTrigger';
        triggersetting.TRAV_Is_Active__c = true;
        triggersetting.TRAV_Trigger_Name__c = 'AccountTrigger';
        return triggersetting;
    }
    //insert custom setting
    public static TRAV_Trigger_Setting__c createGeneralTriggersetting()
    {
        TRAV_Trigger_Setting__c triggersetting = new TRAV_Trigger_Setting__c();
        triggersetting.TRAV_Skip_Trigger_Org_Wide__c ='';
        triggersetting.TRAV_Is_Active__c = true;
        triggersetting.TRAV_Trigger_Name__c = '';
        return triggersetting;
    }
    public static General_Data_Setting__c createGeneralDataSetting()
    {
        General_Data_Setting__c objGeneralDataSetting = new General_Data_Setting__c();
        objGeneralDataSetting.NumberOfMonths__c = 365;
        objGeneralDataSetting.ClosedStages__c = 'Deferred;Written;Declined;Quote Unsuccessful';
        return objGeneralDataSetting;
    }

    //method to create list of opportunities
    public static List<Opportunity> createOpportunities(Integer count, Id accountId, String businessUnit) {
        List<Opportunity> opptyList = new List<Opportunity>();
        for(Integer i =0 ; i < count; i++) {
            Opportunity oppty = new Opportunity();
            oppty.Name = 'TestOppty'+i;
            oppty.AccountId = accountId;
            oppty.StageName = 'Prospect';
            oppty.TRAV_BU_Account_Name__c = 'TestBUAccountName'+i;
            oppty.CloseDate = System.today() + 30;
            oppty.TRAV_Strength_Position__c='Good';
            oppty.TRAV_Marketing_Conditions__c='Broad';
            oppty.TRAV_Travelers_Office__c='Alpharetta-Atlanta';
            if(businessUnit != null){
                if(businessUnit=='BI-NA')
                oppty.RecordTypeId=Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('New Business NA').getRecordTypeId();
                oppty.TRAV_Business_Unit__c = businessUnit;
            }
            opptyList.add(oppty);
        }
        return opptyList;
    }
    
    //method to create list of opportunities Contact Role
    public static List<OpportunityContactRole> createOpportunityContactRole(Integer count, Id oppId, Id contactId) {
        List<OpportunityContactRole> opptyList = new List<OpportunityContactRole>();
        for(Integer i =0 ; i < count; i++) {
            OpportunityContactRole oppty = new OpportunityContactRole();
            oppty.ContactId=contactId;
			oppty.OpportunityId=oppId;
            opptyList.add(oppty);
        }
        return opptyList;
    }
    
        //method to create list of opportunities
    public static Opportunity createOppty(String stage, String name, Id accId) {
        Opportunity oppty = new Opportunity();
        oppty.Name = name;
        oppty.AccountId = accId;
        oppty.StageName = stage;
        oppty.CloseDate = System.today() + 30;
        oppty.TRAV_Policy_Expiration_Date__c = System.today() + 60;
        return oppty;
    }
    //Create Opportunities based on record type
        public static Opportunity createOpptyByRecordTypeDevName(String stage, String name, Id accId,String rcdTypeDevName) {
        Id recId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('New Business').getRecordTypeId();
        Opportunity oppty = new Opportunity();
        oppty.Name = name;
        oppty.RecordTypeId = recId;
        oppty.AccountId = accId;
        oppty.StageName = stage;
        oppty.CloseDate = System.today() + 30;
        oppty.TRAV_Policy_Expiration_Date__c = System.today() + 60;
        return oppty;
    }
    //Create Opportunities based on record type
        public static Opportunity createOpptyByRecordType(String stage, String name, Id accId,String rcdTypeDevName) {
        Id recId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(rcdTypeDevName).getRecordTypeId();
        Opportunity oppty = new Opportunity();
        oppty.Name = name;
        oppty.RecordTypeId = recId;
        oppty.AccountId = accId;
        oppty.StageName = stage;
        oppty.CloseDate = System.today() + 30;
        oppty.TRAV_Policy_Expiration_Date__c = System.today() + 60;
        return oppty;
    }
    //method to create BI-NA opportunities
    public static Opportunity createBINAOppty(String stage, String name, Id accBnMId, Id userId) {

        Opportunity oppty = new Opportunity();
        oppty.Name = name;
        oppty.TRAV_Agency_Broker__c = accBnMId;
        oppty.AccountId = accBnMId;
        oppty.TRAV_Business_Unit__c = 'BI-NA';
        oppty.StageName = stage;
        oppty.OwnerId = userId;
        oppty.CloseDate = System.today() + 365;
        return oppty;
    }

    //method to create list of Opportunity Team Member
    public static List<OpportunityTeamMember> createOpportunityTeamMembers(Integer count, Id userId, Id OpptyId) {
        List<OpportunityTeamMember> opptyTeamList = new List<OpportunityTeamMember>();
        for(Integer i =0 ; i < count; i++) {
            OpportunityTeamMember opptyTeam = new OpportunityTeamMember();
            opptyTeam.TRAV_Active__c = true;
            opptyTeam.OpportunityId = OpptyId;
            opptyTeam.UserId = userId;
            opptyTeam.TeamMemberRole = Constants.ACCOUNTEXECUTIVE;
            opptyTeamList.add(opptyTeam);
        }
        return opptyTeamList;
    }
        public static OpportunityTeamMember createOpptyTeammember(Id uId, Id opptyId, String role) {

            OpportunityTeamMember opptyTeam = new OpportunityTeamMember();
            opptyTeam.TRAV_Active__c = true;
            opptyTeam.OpportunityId = opptyId;
            opptyTeam.UserId = uId;
            opptyTeam.TeamMemberRole = role;
            return opptyTeam;
    }

    //method to create list of Opportunity Team Member
    public static List<AccountTeamMember> createAccountTeamMembers(Integer count, Id userId, Id accountId) {
        List<AccountTeamMember> accountTeamList = new List<AccountTeamMember>();
        for(Integer i =0 ; i < count; i++) {
            AccountTeamMember accTeam = new AccountTeamMember();
            accTeam.TeamMemberRole = Constants.ACCOUNTEXECUTIVE;
            accTeam.AccountId = accountId;
            accTeam.UserId = userId;
            accountTeamList.add(accTeam);
        }
        return accountTeamList;
    }

    //Method to create test users
    public static User createTestUser(String profileName, String fName, String lName) {
        String orgId = userInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        Id profId = [SELECT
                        Id
                    FROM
                        Profile
                    WHERE
                        Name = :profileName].Id;
        User testUser = new User(  Firstname = fName,
                                 LastName = lName,
                                 Email = uniqueName + '@test' + orgId + '.org',
                                 Username = uniqueName + '@test' + orgId + '.org',
                                 EmailEncodingKey = 'ISO-8859-1',
                                 Alias = uniqueName.substring(18, 23),
                                 TimeZoneSidKey = 'America/Los_Angeles',
                                 LocaleSidKey = 'en_US',
                                 LanguageLocaleKey = 'en_US',
                                 ProfileId = profId );
        return testUser;
    }

    //Method to create test price book
    public static Pricebook2 createTestPricebook(String priceBookName){
        Pricebook2 pricebook = new Pricebook2(
            Name = priceBookName
        );
        return pricebook;
    }

    //Method to create Event record
     public static List<Event> createEvent(Integer count, Id userId, Id opportunityId) {
        List<Event> lstEvent = new List<Event>();
        for(Integer i =0 ; i < count; i++) {
            Event objEvent = new Event();
            objEvent.TRAV_Stewardship_Id__c='123456';
            objEvent.Type = 'Stewardship';
            objEvent.Subject='Post-Renewal Debrief';
            objEvent.Description = 'Description - '+i; //string
            objEvent.OwnerId = userId; //user id
            objEvent.WhatId = opportunityId; //record id
            objEvent.DurationInMinutes = 60;
            objEvent.ActivityDateTime = System.now();
            lstEvent.add(objEvent);
        }
        return lstEvent;
    }

    public static List<Product2> createProducts(Integer count) {
        List<Product2> lstProducts = new List<Product2>();
        for(Integer i=0; i < count; i++) {
            Product2 objProduct = new Product2(
                isActive = true,
                ProductCode = 'PRODUCT-'+i,
                Name = 'Fake Product - '+i
            );

            lstProducts.add(objProduct);
        }
        return lstProducts;
    }

    //Method to create PricebookEntry
    public static List<PricebookEntry> createPricebookEntries(List<Product2> products, Id priceBookId) {
        List<PricebookEntry> lstPricebookEntries = new List<PricebookEntry>();
        for(Product2 product : products) {
            PricebookEntry objPricebookEntry = new PricebookEntry(
                IsActive = true,
                Pricebook2Id = priceBookId,
                Product2Id = product.Id,
                UnitPrice = 1.00,
                UseStandardPrice = false
            );

            lstPricebookEntries.add(objPricebookEntry);
        }
        return lstPricebookEntries;
    }

    //Method to create Carriers
    public static List<TRAV_Carrier__c> createCarriers(List<String> names) {
        List<TRAV_Carrier__c> carriers = new List<TRAV_Carrier__c>();
        for(String name: names){
            TRAV_Carrier__c carrier = new TRAV_Carrier__c(
                Name = name
            );
            carriers.add(carrier);
        }
        return carriers;
    }

    //Method to create Opportunity Carriers
    public static List<TRAV_Opportunity_Carrier__c> createOpportunityCarriers(List<Opportunity> opportunities, List<TRAV_Carrier__c> carriers, String carrierType) {
        List<TRAV_Opportunity_Carrier__c> lstOppCarriers = new List<TRAV_Opportunity_Carrier__c>();

        for(Opportunity opp : opportunities) {
            for(TRAV_Carrier__c carrier : carriers) {
                TRAV_Opportunity_Carrier__c objOppCarrier = new TRAV_Opportunity_Carrier__c(
                    TRAV_Carrier__c = carrier.Id,
                    TRAV_Carrier_Type__c = carrierType,
                    TRAV_Opportunity__c = opp.Id
                );

                lstOppCarriers.add(objOppCarrier);
            }
        }
        return lstOppCarriers;
    }

    //Method to create Travelers Contact
    public static List<Contact> createTravelersContact(Integer objInteger) {
        List<Contact> lstContact = new List<Contact>();

        for(Integer i =0 ; i < objInteger; i++) {
            Contact objContact = new Contact();
            objContact.LastName = 'LastName';
            lstContact.add(objContact);
        }
        return lstContact;
    }
    //Method to create Agency Non Licensed contacts
    public static contact createAgencyNonLicensed(id strAccountid){
        Id recordTypeIdforNonLicensed = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.NONLICENSEDCONTACTRECTYPE).getRecordTypeId();
        Contact objContact = new Contact();
            objContact.LastName = 'LastName';
        objContact.AccountId = strAccountid;
            objContact.RecordTypeId = recordTypeIdforNonLicensed;
            return objContact;
    }
    
    public static contact createContact(){
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.TRAVELERSCONTACTRECORDTYPE).getRecordTypeId();
        Contact objContact = new Contact();
            objContact.LastName = 'LastName';
        //objContact.AccountId = strAccountid;
            objContact.RecordTypeId = recordTypeId;
            return objContact;
    }
    //create Opportunity Line Items
    public static List<OpportunityLineItem> createOpportunityLineItem(Id OpptyId, Integer no) {
        List<OpportunityLineItem> opptyProdList = new List<OpportunityLineItem>();
        //PriceBook
        Id pricebookId = Test.getStandardPricebookId();

        //create Products
        List<Product2> prodList = createProducts(no);
        insert prodList;
        //Create Price Book Entry
        List<PricebookEntry> pbe = createPricebookEntries(prodList, pricebookId);
        insert pbe;
        //Create Incumbent Carrier
        Trav_Carrier__c carr = new Trav_Carrier__c(Name = 'Carrier 1A');
        insert carr;
        for(integer i = 0;i < no; i++){
            OpportunityLineItem o = new OpportunityLineItem();
            //o.Name = 'Line Item' + i;
            o.OpportunityId = OpptyId;
            o.Product2Id = prodList[i].Id;
            o.TRAV_Closed_Reason__c = '';
            o.TRAV_Incumbent_Insurance_Carrier_Name__c = carr.id;
            o.PricebookEntryId = pbe[0].Id;
            opptyProdList.add(o);
        }
        return opptyProdList;

    }
    public static List<InsurancePolicy> createPolicies(Id OpptyId, String trType,Id accId,Integer i ) {
        List<InsurancePolicy> poList = new List<InsurancePolicy>();
        for(Integer j = 0 ;j < i ; j++){
        InsurancePolicy po = new InsurancePolicy();
        po.TRAV_Opportunity__c = OpptyId;
        po.TRAV_Transaction_Type_Code__c = trType;
		po.NameInsuredId = accId;
        po.TRAV_Reason_Code__c  = 'product shortage'+j;
        po.Name = 'BopH789'+j;
        poList.add(po);
        }
        return poList;
    }

    public static TRAV_SIC__c createSIC(String code, String coeType){
        TRAV_SIC__c sic = new TRAV_SIC__c();
        sic.TRAV_SIC_Code__c = code;
        sic.TRAV_COE_Type__c = coeType;
        return sic;
    }

    //Creates System Admin User
    public static User createTestAdminUser(String fName, String lName) {
        String orgId = userInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        Id profId = [SELECT
                        Id
                    FROM
                        Profile
                    WHERE
                        Name = :Constants.SystemAdministrator].Id;
        User testUser = new User(  Firstname = fName,
                                 LastName = lName,
                                 Email = uniqueName + '@test' + orgId + '.org',
                                 Username = uniqueName + '@test' + orgId + '.org',
                                 EmailEncodingKey = 'ISO-8859-1',
                                 Alias = uniqueName.substring(18, 23),
                                 TimeZoneSidKey = 'America/Los_Angeles',
                                 LocaleSidKey = 'en_US',
                                 LanguageLocaleKey = 'en_US',
                                 ProfileId = profId );
        return testUser;
    }
}