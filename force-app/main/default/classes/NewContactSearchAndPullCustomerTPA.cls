/**
* This class is the utility class for Contact Search and Pull Functionality for Customer and TPA Contacts
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect          Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Tejal P             	US66730         		 	11/5/2019      Original Version
* -----------------------------------------------------------------------------------------------------------
*/
public without sharing class NewContactSearchAndPullCustomerTPA{
    @AuraEnabled
    public static SelectedRecordWrapper saveContact(Contact contactRec, String recordId, String accId, String recordType){
        SelectedRecordWrapper wrapperSelectedRecord = new SelectedRecordWrapper();
        system.debug('-contactRec--' +contactRec );
        if(recordType == 'TRAV_TPA'){
            contactRec.AccountId = accId;
        }
        else{
            Opportunity oppRec = [SELECT Id, Name, AccountId FROM Opportunity WHERE Id=:recordId LIMIT 1];
            contactRec.AccountId = oppRec.AccountId;
        }
        Id idRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(recordType).getRecordTypeId();
        contactRec.RecordTypeId = idRecordType;  
        Database.insert(contactRec);
        wrapperSelectedRecord.val = contactRec.Id;
        wrapperSelectedRecord.text = contactRec.FirstName + ' ' + contactRec.LastName;
        system.debug('---' +wrapperSelectedRecord );
        return wrapperSelectedRecord;
    }
    
    /**
* Wrapper class declaration
*/  
    public class SelectedRecordWrapper{
         @AuraEnabled public String text{get;set;}
         @AuraEnabled public String val{get;set;}
    }
}