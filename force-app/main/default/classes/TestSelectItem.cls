/**
* This class is the Test Class for the SelectItem wrapper class
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------         
* Tejal Pradhan           NA                  08/02/2019       Created this class with methods - testSelectItemWithSelection,testSelectItemWithoutSelection                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   and checkForPricingAsReason methods
* -----------------------------------------------------------------------------------------------------------             
*/

@isTest

public class TestSelectItem{
    
/* This is a test method for the SelectItem wrapper class with Selection Parameter
*/   
    @isTest
    Static void testSelectItemWithSelection(){
        SelectItem selectItemRecWithSelection = new SelectItem('testValue','testLabel',true);
        /*************Assert************/
        System.assertEquals(selectItemRecWithSelection.selected,true);
    }
    
/* This is a test method for the SelectItem wrapper class without Selection Parameter
*/   
    @isTest
    Static void testSelectItemWithoutSelection(){
        SelectItem selectItemRecWithoutSelection = new SelectItem('testValue','testLabel');
        /*************Assert************/
        System.assertEquals(selectItemRecWithoutSelection.value,'testValue');
    }
    
}