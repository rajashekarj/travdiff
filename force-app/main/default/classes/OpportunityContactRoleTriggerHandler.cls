public class OpportunityContactRoleTriggerHandler implements ITriggerHandler{
    
       public void beforeInsert(List<sObject> newItems) {     
        OpportunityContactRoleService.UpdatePrimaryCOntact((List<OpportunityContactRole>)newItems, null);
        
    }

    //Handler Method for Before Update.
    public void beforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
      
       OpportunityContactRoleService.UpdatePrimaryCOntact((List<OpportunityContactRole>)newItems.values(), (Map<Id, OpportunityContactRole>)oldItems); 
    
    }
    //Handler Method for Before Delete.
    public void beforeDelete(Map<Id, sObject> oldItems) {

    }

    //Handler method for After Insert.
    public void afterInsert(Map<Id, sObject> newItems) {
        OpportunityContactRoleService.updatePrimaryContactField((List<OpportunityContactRole>)newItems.values(), null);
        
    }

    //Handler method for After Update.
    public void afterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
      OpportunityContactRoleService.updatePrimaryContactField((List<OpportunityContactRole>)newItems.values(), (Map<Id, OpportunityContactRole>)oldItems); 

    }
    //Handler method for After Delete
    public void afterDelete(Map<Id, sObject> oldItems) {

    }

    //Handler method for After Undelete.
    public void afterUndelete(Map<Id, sObject> oldItems) {

    }

}