/**
* This class is used as a Selector Layer for Profile  . 
* All queries for Profile will be performed in this class.
*
* 
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -----------------------------------------------------------------------------------------------------------              
* Siddharth Menon		   US68464		   10/07/2019       Original Version 		
* -----------------------------------------------------------------------------------------------------------
*/
public class ProfileSelector {
	private final static string CLASSNAME = 'ProfileSelector';
    
        /**
* This is a method is used to get Id of System Administrator
* @param :null
* @return Id of System Admin.
*/
    public static Id getSystemAdminProfileId() {
        final string METHODNAME = 'getSystemAdminProfileId';
        String sysAdminProfileName=Constants.SystemAdministrator;
        Id idSysAdmin;
        try {
            idSysAdmin = [
                SELECT id 
                FROM Profile 
                WHERE Name=:sysAdminProfileName
            	Limit 1].Id;
            if(Test.isRunningTest()){
                throw new QueryException();
            }
        } //End of try
        catch(QueryException exp) {
            //exception handling code
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, exp, null);
            
        } // End Catch
        
        return idSysAdmin;
    } //end of method getOpportunityChatterFeed
}