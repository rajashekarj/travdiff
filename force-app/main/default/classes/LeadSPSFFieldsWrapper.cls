/**
* This class is the Wrapper Class for Storing Response From GET API
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect                  Date            Description
* -----------------------------------------------------------------------------------------------------------              
* Sayanka Mohanty        US73994/US73995/US73996          24/01/2020      Original Version
* -----------------------------------------------------------------------------------------------------------
*/
public class LeadSPSFFieldsWrapper {
    @auraEnabled
    public String leadSource{get;set;}
    @auraEnabled
    public String industry{get;set;}
    @auraEnabled
    public String leadStatus{get;set;}
    @auraEnabled
    public String bUInterested{get;set;}
	@auraEnabled
    public String totalAssets{get;set;}
    @auraEnabled
    public Date effectiveDate{get;set;}
    @auraEnabled
    public Date expirationDate{get;set;}
    @auraEnabled
    public Id agencyLocation{get;set;}
    @auraEnabled
    public String annualRevenue{get;set;}
    @auraEnabled
    public String numberOfEmployees{get;set;}
    @auraEnabled
    public String guid{get;set;}
}
