/**
 * Handler Class for Policy Trigger. This class won't have any logic and
 * will call other Policy Service/Utilities class.
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer                User Story/Defect   Date            Description
 * -----------------------------------------------------------------------------------------------------------
 * Sayanka Mohanty          NA                  08/29/2019      Original Version
 * Tejal PradhanNA          US67699             09/08/2019      Added method in after insert and after update
 * Siddharth Menon          FSC                 01/17/2020      Changed TRAV_Policy__c to InsurancePolicy
 -------------------------------------------------------------------------------------------------------------
 */
public class PolicyTriggerHandler implements ITriggerHandler{
    //Handler Method for Before Insert.
    public void beforeInsert(List<sObject> newItems) {

    }
    
    //Handler Method for Before Update.
    public void beforeUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {
        system.debug('Invoked beforeUpdate');
        
        List<InsurancePolicy> lstNewPolicies = newItems.values();

        //Stamp Old Policy owner
        PolicyTriggerUtility.stampOldPolicyOwnerId(lstNewPolicies, (Map<Id, InsurancePolicy>)oldItems);

        //Stamp Date change on BOR Update
        PolicyTriggerUtility.stampBORChangeDate(lstNewPolicies, (Map<Id, InsurancePolicy>)oldItems);
    }

    //Handler Method for Before Delete.    
    public void beforeDelete(Map<Id, sObject> oldItems) {
    }
    
    //Handler method for After Insert.
    public void afterInsert(Map<Id, sObject> newItems) {
        system.debug('Invoked afterInsert');
        List<InsurancePolicy> lstNewPolicies = newItems.values();

        //Added for US67699
        PolicyTriggerUtility.updateAccountRecordType(lstNewPolicies);

        //execute the scheduled action to Generate the Notification
        if(!System.isBatch()){
            System.enqueueJob(new NotificationUtility(newItems.values(), null, 'InsurancePolicy'));
        }
    }

    //Handler method for After Update.    
    public void afterUpdate(Map<Id, sObject> newItems, Map<Id, sObject> oldItems) {  
        system.debug('Invoked afterUpdate');
        List<InsurancePolicy> lstNewPolicies = newItems.values();

        //Added for US67699
        PolicyTriggerUtility.updateAccountRecordType(lstNewPolicies);

        if(!System.isBatch()){
            System.enqueueJob(new NotificationUtility(newItems.values(), oldItems, 'InsurancePolicy'));
        }
    }

    //Handler method for After Delete
    public void afterDelete(Map<Id, sObject> oldItems) {
    }
    
    //Handler method for After Undelete.
    public void afterUndelete(Map<Id, sObject> oldItems) {
    } 
}