/**
*Test class for AccountService
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* -------------------- ---------------------------------------------------------------------------------------              
* Bhanu Reddy              US62952            05/30/2019      testClass

* -----------------------------------------------------------------------------------------------------------
*/
@istest
private class TestAccountService {
    /**
* This method is used to test the processAgencyProducerCode method
*of the AccountService test class
* @return void - returns void .
*/ 
    static testmethod void testprocessAgencyProducerCode(){
        
        list<TRAV_Producer__c> lstProducers = new list<TRAV_Producer__c>();
        //create producer record;
        TRAV_Producer__c objProducer = TestDataFactory.createProducers();
        objProducer.TRAV_Producer_Code__c = 'OA160';
        lstProducers.add(objProducer);
        
        TRAV_Producer__c objProducersecond = TestDataFactory.createProducers();
        objProducersecond.TRAV_Producer_Code__c = 'F160';
        lstProducers.add(objProducersecond);
        
        insert lstProducers;
        
        //create custom setting
        
        TRAV_Trigger_Setting__c objTriggerSetting  = TestDataFactory.createTriggersetting();
        objTriggerSetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        insert objTriggerSetting;
        test.startTest();
        //create Agency Account
        Account objAgencyAccount = TestDataFactory.createAgency();
        objAgencyAccount.TRAV_CL_Prime_Producer_Code__c = 'OA160';
        insert objAgencyAccount;
        Contact objContact =TestDataFactory.createAgencyNonLicensed(objAgencyAccount.id);
        insert objContact;
        List<Account> lstAgencyAccounts = new List<Account> ();
        lstAgencyAccounts.add(objAgencyAccount);
        system.enqueueJob(new AccountService(lstAgencyAccounts,null));
        TRAV_Account_Producer_Relation__c objAccProducer = new TRAV_Account_Producer_Relation__c();
        objAccProducer.TRAV_Account__c = objAgencyAccount.id;
        objAccProducer.TRAV_Producer__c = objProducer.id;
        insert objAccProducer;
        
        list<TRAV_Account_Producer_Relation__c> lstAccountProducerRelation = new list<TRAV_Account_Producer_Relation__c>();
        lstAccountProducerRelation.add(objAccProducer);
        
        lstAccountProducerRelation 
            = [select id,
               TRAV_Producer__c 
               from 
               TRAV_Account_Producer_Relation__c 
               where 
               TRAV_Account__c =:objAgencyAccount.id
               LIMIT 1
              ];
        system.assertEquals(lstAccountProducerRelation.size(),1);
        system.assertEquals(lstAccountProducerRelation[0].TRAV_Producer__c, objProducer.id);  
        
        //update the code to add another producer code
        objAgencyAccount.TRAV_CL_Prime_Producer_Code__c = 'OA160,F160';
        objAgencyAccount.BillingStreet ='12 A Banking Street';
        objAgencyAccount.BillingCity ='Santiago';
        objAgencyAccount.BillingCountry ='Chile';
        objAgencyAccount.BillingPostalCode ='515201';
        update objAgencyAccount;
        
        Map<id,account> mapAgencyAccountforupdate = new map<id,account>(); 
        mapAgencyAccountforupdate.put(objAgencyAccount.id,objAgencyAccount);
        system.enqueueJob(new AccountService(lstAgencyAccounts,mapAgencyAccountforupdate));
        Contact objContactToCheck =[select id,OtherStreet,OtherCity,OtherCountry,otherPostalCode 
                                    	from contact where id =:objContact.id];
        TRAV_Account_Producer_Relation__c objSecondAccProducer = new TRAV_Account_Producer_Relation__c();
        objSecondAccProducer.TRAV_Account__c = objAgencyAccount.id;
        objSecondAccProducer.TRAV_Producer__c = objProducersecond.id;
        insert objSecondAccProducer;
        lstAccountProducerRelation.add(objSecondAccProducer);
        
        lstAccountProducerRelation 
            = [select id,
               TRAV_Producer__c 
               from 
               TRAV_Account_Producer_Relation__c 
               where 
               TRAV_Account__c =:objAgencyAccount.id
              ];
        
        system.assertEquals(lstAccountProducerRelation.size(),2); 
        
        //update account to check if the new code the removal of account contact relationship
        objAgencyAccount.TRAV_CL_Prime_Producer_Code__c = 'OA160';
        update objAgencyAccount ; 
        delete lstAccountProducerRelation[1];
        
        lstAccountProducerRelation 
            = [select id,
               TRAV_Producer__c 
               from 
               TRAV_Account_Producer_Relation__c 
               where 
               TRAV_Account__c =:objAgencyAccount.id
              ];
        
        system.assertEquals(lstAccountProducerRelation.size(),1); 
        List<Account> lstAcc = new List<Account>();
        lstAcc.add(objAgencyAccount);
        //Added for covering Delete operation in trigger handler
        Database.DeleteResult[] delRelsult = Database.delete(lstAcc);
        system.assertEquals(delRelsult[0].isSuccess(), true);
        //Added for covering undelete operation in trigger handler
        Database.UndeleteResult[] undelRelsult = Database.undelete(lstAcc);
        system.assertEquals(undelRelsult[0].isSuccess(), true);
        
        test.stopTest();
    } //end of method testprocessAgencyProducerCode
    /**
* This method is used to test the processAgencyProducerCode method for batch of records
*of the AccountService test class
* @return void - returns void .
*/ 
    static testmethod void testBatchprocessAgencyProducerCode(){
        list<TRAV_Account_Producer_Relation__c> lstAccountProducerRelation = new list<TRAV_Account_Producer_Relation__c>();
        TRAV_Trigger_Setting__c objTriggerSetting  = TestDataFactory.createTriggersetting();
        objTriggerSetting.TRAV_Skip_Trigger_Org_Wide__c = '';
        insert objTriggerSetting;
        List<Trav_Producer__c> listProducers = TestDataFactory.createProducersBluk();
        insert listProducers;
        Id AgencyRecordtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency Headquarter').getRecordTypeId();
        list<Account> lstAgencyBrickandMortar = new List<Account>(); 
        Account objacc = TestDataFactory.createAgency();
        lstAgencyBrickandMortar.add(objacc);
        string strCode = 'Code';
        String strCode1 = 'Test';
        integer intcounter = 1;
        for(Account objAccount :  lstAgencyBrickandMortar){
            objAccount.TRAV_Account_External_Id__c = 'EX'+intcounter;
            objAccount.TRAV_CL_Master_Producer_Code__c = strCode;
            objAccount.RecordTypeId = AgencyRecordtype;
            intcounter++;
            strCode = ','+strCode + string.valueof(intcounter);
            system.debug('Account valeus are '+objAccount);
        } //end for iteration over objects
        Database.insert(lstAgencyBrickandMortar);
		Test.startTest();
        lstAgencyBrickandMortar[0].TRAV_CL_Master_Producer_Code__c = strCode1;
        Database.update(lstAgencyBrickandMortar);
        Test.stopTest();
        
    } //end of method testBatchprocessAgencyProducerCode
/**
* This method is used to test the AccountFeed method
*of the AccountService test class
* @return void - returns void .
*/     
    public static testmethod void coverAccountFeed(){
        //AccountSelector.getAccountFeed();
        AccountTriggerHandler obj = new AccountTriggerHandler();
        obj.beforeInsert(New List<sObject>());
        obj.beforeUpdate(New Map<Id, sObject>(),New Map<Id, sObject>());
        obj.afterDelete(New Map<Id, sObject>());
        obj.beforeDelete(New Map<Id, sObject>());
        obj.afterUndelete(New Map<Id, sObject>());
    }
    
} //end of class