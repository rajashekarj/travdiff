/**
* Batch Class for Opportunity. This class will have logic for cloning of opportunity record if Record 
* Type is Renewal on opportunity 
* Modification Log    :
* ------------------------------------------------------------------------------------------------------
* Developer              User Story/Defect    Date             Description
* Shashank Agarwal		 US					  09/30/2019       Original Version  		
* ------------------------------------------------------------------------------------------------------
*/
global class BatchMapBAMToOpportunity implements Database.Batchable<sObject>, Schedulable{
    String strQuery;
    String CLASSNAME ='BatchMapBAMToOpportunity';
    Renewal_Batch__c objBAMBatch = Renewal_Batch__c.getOrgDefaults();
    DateTime dtFrom = objBAMBatch.TRAV_Last_Successful_BAM_Batch_Run__c;
    
    
    /**
* This method collect the batches of records or objects to be passed to execute
* @param Database.BatchableContext
* @return Database.QueryLocator record
*/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug(dtFrom);
        String METHODNAME = 'Start';
        Set<Id> setProducerId = new Set<Id>();
        Set<TRAV_Account_Producer_Relation__c> setAccountProducerRelation = new Set<TRAV_Account_Producer_Relation__c>();
        try{
            setProducerId = ProducerSelector.getProducerForBAMBatch(dtFrom);
            setAccountProducerRelation = AccountProducerRelationshipSelector.getAccountProducerRelationsForBAM(setProducerId);
            strQuery = 'Select Id, TRAV_NA_Producer_Code__c, TRAV_BAM_ID__c, TRAV_Business_Unit__c, TRAV_Branch_Master_Code__c From Opportunity Where TRAV_NA_Producer_Code__c IN :setAccountProducerRelation ';
            return Database.getQueryLocator(strQuery);
        }catch(Exception objException){
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objException, null);
            return null;
        }
    } //End  Method
    
    /**
* This method process each batch of records for the implementation logic
* @param Database.BatchableContext record
* @param List of Opportunity 
* @return void 
*/ 
    global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
        String METHODNAME = 'execute';
        Map<String,Id> mapBAMStringToId = new Map<String,Id>();
        Set<String> setBAMUniqueIdentifier = new Set<String>();
        List<TRAV_BAM_Agency_Mapping__c> lstBAM = new List<TRAV_BAM_Agency_Mapping__c>();
        try{
            //Creating set of Unique Identifiers
            for(Opportunity objOpportunity: scope){
                // Check for Bond Master Code and Financial Branch Code not NULL
                if(String.isNotBlank(objOpportunity.TRAV_Business_Unit__c) && String.isNotBlank(objOpportunity.TRAV_Branch_Master_Code__c)){
                    setBAMUniqueIdentifier.add(objOpportunity.TRAV_Business_Unit__c + objOpportunity.TRAV_Branch_Master_Code__c);
                }
            } // End For Loop
            
            //SOQL for getting revelent BAM Records
            if(!setBAMUniqueIdentifier.isEmpty()){
                lstBAM = BAMSelector.getBAMtoTagForProducers(setBAMUniqueIdentifier);
            } // End If
            
            // Populating Map mapBAMStringToId
            if(!lstBAM.isEmpty()){
                for(TRAV_BAM_Agency_Mapping__c objBAM : lstBAM){
                    mapBAMStringToId.put(objBAM.TRAV_Unique_Identifier__c,objBAM.Id);
                } // End for
            } // End if
            
            List<Opportunity> lstOpportunity = new List<Opportunity>();
            for(Opportunity objOpportunity : scope){
                //Case 1: If TRAV_Bond_Master_Code__c and TRAV_Financial_Branch_Code__c are not blank
                if(String.isNotBlank(objOpportunity.TRAV_Business_Unit__c) && String.isNotBlank(objOpportunity.TRAV_Branch_Master_Code__c)){
                    String strKey = objOpportunity.TRAV_Business_Unit__c + objOpportunity.TRAV_Branch_Master_Code__c;
                    if(mapBAMStringToId.containsKey(strKey)){
                        Opportunity objOpportunityToUpdate = new Opportunity();
                        objOpportunityToUpdate.Id = objOpportunity.Id;
                        objOpportunityToUpdate.TRAV_BAM_ID__c = mapBAMStringToId.get(strKey);
                        lstOpportunity.add(objOpportunityToUpdate);
                    } 
                } // End Case 1 Check
                
                //Case 2: If TRAV_Bond_Master_Code__c or TRAV_Financial_Branch_Code__c are blank
                if(String.isBlank(objOpportunity.TRAV_Business_Unit__c) || String.isBlank(objOpportunity.TRAV_Branch_Master_Code__c)){
                    Opportunity objOpportunityToUpdate = new Opportunity();
                        objOpportunityToUpdate.Id = objOpportunity.Id;
                        objOpportunityToUpdate.TRAV_BAM_ID__c = Null;
                        lstOpportunity.add(objOpportunityToUpdate);
                } // End Case 2 Check
            } // End for Loop
            
            if(!scope.isEmpty()){
                Database.SaveResult[] lstSaveResult = Database.update(lstOpportunity, false);
                ExceptionUtility.logDMLError(Constants.OPPORTUNITYSERVICE, METHODNAME, lstSaveResult);
            }
        }catch(Exception objException){
            ExceptionUtility.logApexException(CLASSNAME, METHODNAME, objException, null);
        }
    } // End  Method
    
    /**
* This method execute any post-processing operations test
* @param Database.BatchableContext record 
* @return void 
*/ 
    global void finish(Database.BatchableContext BC) {
        DateTime dtTo = DateTime.now();
        Renewal_Batch__c objBamBatch = Renewal_Batch__c.getOrgDefaults();
        objBamBatch.TRAV_Last_Successful_BAM_Batch_Run__c = dtTo;
        upsert objBamBatch;
     
    }
    
    /**
* This method execute this batch class when scheduled
* @param SchedulableContext record 
* @return void 
*/
    global void execute(SchedulableContext schCon) {
        BatchMapBAMToOpportunity batchObj  = new BatchMapBAMToOpportunity(); 
        Database.executebatch(batchObj);
    }//End Method  
}