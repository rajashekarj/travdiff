/**
* Utility Class for Stewardship Trigger Handler. This class will have logic for all the implementations.
*
* Modification Log    :
* -----------------------------------------------------------------------------------------------------------
* Developer		      User Story/Defect	  Date		    Description
* Shashank Agarwal    US73728			  01/27/2020    Added Method updateOwnerToStewardshipLead
* -----------------------------------------------------------------------------------------------------------              
* 
*/
public class StewardshipService {
    /*** This method will update owner of the Stewardship record to Stewardship Lead if present else to Opportunity Owner
* @param lstNewServicePartnerRequest - List of new values in the for the records.
* @return void - returns void .
*/
    public static void updateOwnerToStewardshipLead(List<TRAV_Stewardship__c> lstNewStewardship,Map<Id, TRAV_Stewardship__c> mapOldStewardship){
        
        final string METHODNAME = 'updateOwnerToStewardshipLead';
        // Set of  account ids related to opportinities.
        Set<Id> setOpportunityIds = new Set<Id>();
        // Map for getting account names related to opportunites. 
        Map<Id,Opportunity> mapOpportunity = new Map<Id,Opportunity>();
        for(TRAV_Stewardship__c objStewardship : lstNewStewardship){
            setOpportunityIds.add(objStewardship.TRAV_Related_Opportunity__c);
        }
        
        try{
            
            //Null check on set of opportunity Id
            if(!setOpportunityIds.isEmpty()){
                mapOpportunity = OpportunitySelector.getOpportunitiesOwner(setOpportunityIds);
            } // End of If condition
            
            for(TRAV_Stewardship__c objStewardship : lstNewStewardship){
                if(objStewardship.TRAV_CAE_Name__c == Null && mapOldStewardship.get(objStewardship.Id).TRAV_CAE_Name__c !=Null){
                    objStewardship.OwnerId = mapOpportunity.get(objStewardship.TRAV_Related_Opportunity__c).OwnerId;
                }else if(objStewardship.TRAV_CAE_Name__c != Null && (mapOldStewardship.get(objStewardship.Id).TRAV_CAE_Name__c ==Null || mapOldStewardship.get(objStewardship.Id).TRAV_CAE_Name__c != objStewardship.TRAV_CAE_Name__c)){
                    objStewardship.OwnerId = objStewardship.TRAV_CAE_Name__c;
                }
            } //End of for loop
            
            if(Test.isRunningTest()){
                throw new QueryException();
            }
        }catch(Exception objExp){
             ExceptionUtility.logApexException('StewardshipService', METHODNAME, objExp, null);
        } //End of Catch
    } //End of Method
    
} //End of class