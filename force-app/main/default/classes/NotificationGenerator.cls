/**
*    @author Sayanka Mohanty
*    @date   06/21/2019 
*    @description : This is the main Controlling class for generating notifications and other Object Records

Modification Log:
------------------------------------------------------------------------------------
Developer                       Date                Description
------------------------------------------------------------------------------------
Sayanka Mohanty               06/23/2019          Original Version*
Shashank Shastri              06/25/2019          Updated the class to filter records based on PriorValue from Trigger.OldMap
*/
public class NotificationGenerator {
    /*
*    @author Sayanka Mohanty
*    @date   06/21/2019
*    @description  This method will accept list of wrappers and assign them to methods for generating records
*/
    public static void assignJob(List<NotificationWrapper> notList,Map<String,String> fieldValueMap){
        system.debug('Generator class invoked');
        List<NotificationWrapper> tskList = new List<NotificationWrapper>();
        List<NotificationWrapper> eventList = new List<NotificationWrapper>();
        List<NotificationWrapper> feedItemList = new List<NotificationWrapper>();
        List<Task> taskListFinal = new List<Task>();
        List<Event> eventListFinal = new List<Event>();
        List<ConnectApi.FeedItemInput> feedItemListFinal = new List<ConnectApi.FeedItemInput>();
        //List<FeedItem> feedItemListFinal = new List<FeedItem>();
        if(!notList.isEmpty()){
            for(NotificationWrapper nw:notList){
                if(nw.type.equals('Task')){
                    tskList.add(nw);
                }
                if(nw.type.equals('Event')){
                    eventList.add(nw);
                }
                if(nw.type.equals('Chatter')){
                    feedItemList.add(nw);
                }
                
            }
        }
        if(!tskList.isEmpty()){ 
            taskListFinal = generateTasks(tskList);
        }
        if(!eventList.isEmpty()){
            eventListFinal = generateEvents(eventList);
        }
        if(!feedItemList.isEmpty()){
            //Create Chatter Post
            feedItemListFinal = generateChatterPosts(feedItemList);
        }
        //Check each List for null check and insert
        if(!taskListFinal.isEmpty()){
            Database.SaveResult[] saveResult = Database.insert(taskListFinal, false);

        }
        if(!eventListFinal.isEmpty()){
            Database.SaveResult[] lstSaveResult = Database.insert(eventListFinal,false);
        }
        if(!feedItemListFinal.isEmpty()){
            //Insert Chatter Post
        }
    }
    /*
*    @author Sayanka Mohanty
*    @date   06/21/2019
*    @description  This method will accept sObj and create Task and return List of Tasks to be created
*/
    //This list of sObject will be changed to list of wrapper
    public static List<Task> generateTasks(List<NotificationWrapper> nWList){
        
        List<Task> taskListCreation = new List<Task>();
        List<Id> allUserList = new List<Id>();
        Map<Id,String> userIDProfileMap = new Map<Id,String>();
        if(nWList != null){
            //Create a list of all users
            for(NotificationWrapper nw : nWList){
                system.debug('**User Id List**'+nW.userIdList );
                if(String.isNotBlank(nw.roleProfile) && nw.userIdList != null){
                    allUserList.addAll(nw.userIdList); 
                }
            }
            system.debug('**All user List**'+allUserList);
            //Create a map of userId and profileName
            if(!allUserList.isEmpty()){
                for(User us: [SELECT Id,Profile.Name FROM User WHERE Id IN :allUserList]){
                    userIDProfileMap.put(us.Id, us.Profile.Name);
                }
            }
            system.debug('**User Id profile Map**'+userIDProfileMap);
            for(NotificationWrapper nw : nWList){
                //check if multiple users are there,if yes,iterate and assign
                if( nw.userIdList != null){
                    if(!(nw.userIdList).isEmpty()){
                        for(Id usId : nw.userIdList){
                            Task tk = new Task();
                            String subject = replaceWithMergeFields(nw.Subject, nw.mergeFieldAPIValMap);
                           /* if(subject.contains('Due Date = today')) {
                                String todayDate = String.valueOf(Date.today());
                                subject = subject.replace('today', todayDate);
                            }*/
                            //Put a check for profile
                            if(String.isNotBlank(nw.roleProfile)){
                                //Check if user belongs to that role and create task else dont create task
                                if(userIDProfileMap != null){
                                    if(userIDProfileMap.containsKey(usId)){
                                        if(userIDProfileMap.get(usId) == nw.roleProfile ){
                                            tk = createTask(subject, usId, nw.objectId, nw.fieldValueMap);
                                            taskListCreation.add(tk) ;
                                        }
                                    }
                                    
                                }
                                
                            }else{
                                tk = createTask(subject, usId, nw.objectId, nw.fieldValueMap);
                                taskListCreation.add(tk);
                            }
                            
                            
                        }
                    }
                }else{
                    Task tk = new Task();
                    String subject = replaceWithMergeFields(nw.Subject, nw.mergeFieldAPIValMap);
                    system.debug('**Updated Subject**'+subject);
                    tk = createTask(subject, nw.Owner, nw.objectId, nw.fieldValueMap);
                    taskListCreation.add(tk);
                }
                
            }
            system.debug('**Final Task List**'+taskListCreation);
        }
        return taskListCreation;
    }
    /*
*    @author Sayanka Mohanty
*    @date   06/21/2019
*    @description  This method will accept sObj and create Events and return List of Events to be created
*/
    //This list of sObject will be changed to list of wrapper
    public static List<Event> generateEvents(List<NotificationWrapper> nWList){
        List<Event> eventListCreation = new List<Event>();
        try{
            if(nWList != null){
                
                for(NotificationWrapper nw : nWList){
                    //check if multiple users are there,if yes,iterate and assign
                    if(!(nw.userIdList).isEmpty()){
                        for(Id usId:nw.userIdList){
                            Event ev = new Event();
                            ev = createEvent(nw.Subject,usId,nw.objectId, nw.fieldValueMap);
                            eventListCreation.add(ev);
                        }
                    }else{
                        Event ev = new Event();
                        ev = createEvent(nw.Subject,nw.Owner,nw.objectId, nw.fieldValueMap);
                        eventListCreation.add(ev);
                    }
                    
                }
                system.debug('**Final Task List**'+eventListCreation);
            }
            if(Test.isRunningTest()){
                 throw new DMLException('Error');
            }
        }catch(Exception ex){
            system.debug('**Exception Caught**'+ex.getLineNumber() + ' ' + ex.getMessage()  + ' NotificationGenerator');
        }
        return eventListCreation;
    }

/*
*    @author Shruti Gupta
*    @date   06/21/2019
*    @description  This method will accept sObj and create Chatter Post and return List of FeedItems to be created
*    @updated 7/16/19 by Beth Joseph
*    @description refactored chatter post code to support multiple users in same role
*/

//This list of sObject will be changed to list of wrapper
public static List<ConnectApi.FeedItemInput> generateChatterPosts(List<NotificationWrapper> nWList){
        system.debug('inside chatter-->>'+JSON.serialize(nWList));
    	Set<Id> setProductId = new Set<Id>(); 
    	List<OpportunityLineItem> listOpptyProds = new List<OpportunityLineItem>();
        List<NotificationWrapper> lstFeedContainsHyperLink = new List<NotificationWrapper>();
        List<ConnectApi.FeedItemInput> feedItemListCreation = new List<ConnectApi.FeedItemInput>();
        if(nWList != null){
            for(NotificationWrapper nw : nWList){
                System.debug('Notifificaton Wrapper'+JSON.serialize(nw));
                String subject = nw.Subject;
                subject = replaceWithMergeFields(subject, nw.mergeFieldAPIValMap);
                system.debug('check the subject '+ subject);
                ConnectApi.FeedItemInput feedItemRec = new ConnectApi.FeedItemInput();
                    if(!(nw.userIdList).isEmpty()){
                    for(Id usId:nw.userIdList){
                        system.debug('usId-->>'+usId);
                        String replacedSubject = subject;
                        //no hyperlink and mentioned used, leverage feed approach looping through user list
                        if(nw.containsHyperlink == false && replacedSubject.contains('@mentionId') && usId != null ) {
                            replacedSubject = replacedSubject.replace('@mentionId', usId);
                            feedItemRec = ChatterUtility.generateFeedInputWithMentions(usId,nw.objectId,replacedSubject);
                            //do the actual post and catch exceptions
                       
                        }
                        //hyperlink, leverage hyperlink approach looping through user list
                        else if(nw.containsHyperlink) {
                            nw.subject = replaceWithMergeFields(replacedSubject, nw.mergeFieldAPIValMap);
                            lstFeedContainsHyperLink.add(nw);
                        }
                         try{
                            if(feedItemRec!=null) {
                                ConnectApi.ChatterFeeds.postFeedElement(NULL , feedItemRec);
                                feedItemListCreation.add(feedItemRec);
                            }
                            
                        }
                        catch(Exception ex){
                            ExceptionUtility.logApexException('Notification Generator','generateChatter',ex,null);
                            system.debug('error creating chatter post in user loop '+ex.getMessage()+'Line no'+ex.getLineNumber()+'**'+replacedSubject);
                        }
                    }
                    }else{
                        //This is for hyperlink without userId
                        if(nw.containsHyperlink) {
                            nw.subject = replaceWithMergeFields(subject, nw.mergeFieldAPIValMap);
                            lstFeedContainsHyperLink.add(nw);
                        }
                    }
                if (nw.containsHyperlink == false && (!subject.contains('@mentionId'))) {
                    feedItemRec = ChatterUtility.generateFeedInputWithMentions(nw.Owner,nw.objectId,subject);
                    try{
                            if(feedItemRec!=null) {
                                ConnectApi.ChatterFeeds.postFeedElement(NULL , feedItemRec);
                                feedItemListCreation.add(feedItemRec);
                                //Update Approval Indicator:US74195
                                if(nw.fieldValueMap != null){
                                    if(nw.mergeFieldAPIValMap != null){
                                        if(nw.mergeFieldAPIValMap.containsKey('id')){
                                            setProductId.add(nw.mergeFieldAPIValMap.get('id'));
                                        }
                                    }
                                }
                            }
                            
                        }
                        catch(Exception ex){
                             ExceptionUtility.logApexException('Notification Generator','Issue executing chatter for owner ' + subject + ex.getMessage()+'Line no'+ex.getLineNumber(),null,null);
                             system.debug('error creating chatter post in user loop '+ex.getMessage()+'Line no'+ex.getLineNumber());
                        }
                }        
            if(lstFeedContainsHyperLink!=null) {
                system.debug('lstFeedContainsHyperLink-->>'+lstFeedContainsHyperLink);
                ChatterUtility.postToChatter(lstFeedContainsHyperLink);
            }
        }
            //Update approval indicator
            try{
                if(!setProductId.isEmpty()){
                    List<OpportunityLineItem> listFinal = new List<OpportunityLineItem>();
                    listOpptyProds = [SELECT Id,TRAV_Approval_Completed__c FROM OpportunityLineItem WHERE Id in :setProductId LIMIT 1000 ];
                    if(!listOpptyProds.isEmpty()){
                        for(OpportunityLineItem objProd : listOpptyProds){
                            if(!objProd.TRAV_Approval_Completed__c){
                                objProd.TRAV_Approval_Completed__c = true;
                                listFinal.add(objProd);
                            }
                        }
                    }
                    if(!listFinal.isEmpty()){
                        update listFinal;
                    }  
                }
            }catch(Exception objex){
                system.debug('**Exception**'+objex.getLineNumber()+'**Message**'+objex.getMessage());
            }
        }
        return feedItemListCreation;
    }
    
    /*
*    @author Shruti Gupta
*    @date   06/21/2019
*    @description  This method will accept sObj and create Chatter Post and return List of FeedItems to be created
*/
    //This list of sObject will be changed to list of wrapper
    public static String getfieldApiValues(sObject rec, String fieldApiName){
        String fieldValue;
        try{
            if(fieldApiName.contains('.') && rec != null){
                system.debug('test-->>'+fieldApiName);
                String[] arrTest = fieldApiName.split('\\.');
                system.debug('**Size of field api array**'+arrTest.size());
                //Check if 2 level
                if(arrTest.size() == 2){
                    sObject tempSObject = rec.getSobject(arrTest[0]);
                    if(tempSObject != NULL){
                        fieldValue = String.valueOf(rec.getSobject(arrTest[0]).get(arrTest[1]));
                    }
                    //Check if 3 level
                }else if(arrTest.size() == 3){
                    sObject tempSObject1 = rec.getSobject(arrTest[0]);
                    if(tempSObject1 != NULL){
                        sObject tempSOBject2 = rec.getSobject(arrTest[0]).getSobject(arrTest[1]);
                        if(tempSOBject2 != NULL){
                            fieldValue = String.valueOf(rec.getSobject(arrTest[0]).getSobject(arrTest[1]).get(arrTest[2]));
                        }
                    }
                    
                }else if(arrTest.size() == 4){
                    sObject tempSObject1 = rec.getSobject(arrTest[0]);
                    if(tempSObject1 != NULL){
                        sObject tempSOBject2 = rec.getSobject(arrTest[0]).getSobject(arrTest[1]);
                        if(tempSOBject2 != NULL){
                            sObject tempSOBject3 = rec.getSobject(arrTest[0]).getSobject(arrTest[1]).getSobject(arrTest[2]);
                            if(tempSOBject3 != NULL){
                                fieldValue = String.valueOf(rec.getSobject(arrTest[0]).getSobject(arrTest[1]).getSobject(arrTest[2]).get(arrTest[3]));
                            }
                        }
                    }
                    
                }else{
                    fieldValue = null;
                }
            }else{
                if(fieldApiName == null || rec == null){
                    fieldValue = '';
                }else{
                    fieldApiName = fieldApiName.trim();
                    fieldValue = String.valueOf(rec.get(fieldApiName));
                }
            }
            system.debug('**Field Value**'+fieldValue);
        }catch(Exception ex){
            system.debug('**Exception caught**'+ ex.getLineNumber()+'**'+ex.getMessage()+'**record is'+json.serialize(rec));
        }
        return fieldValue;
    }
    /*Common method for task creation*/
    public static Task createTask(String Subject,Id Owner,Id ObjId,Map<String,String> fieldValueMap){
        system.debug('ownerownerowner'+owner);
        SObjectType r = ((SObject)(Type.forName('Schema.Task').newInstance())).getSObjectType();
        DescribeSObjectResult d = r.getDescribe();
        Task taskRec = new Task();
        taskRec.Subject = Subject;
        taskRec.OwnerId = Owner;
        taskRec.ActivityDate = system.today();
        taskRec.TRAV_Task_Unique_Identifier__c = ObjId + Subject + 'Open';
        system.debug('**FieldValue Map**' + fieldValueMap);
        if(fieldValueMap != null){
            for(String apiName : fieldValueMap.keySet()){
                system.debug('apiName-->>'+apiName);
                Schema.DisplayType fieldType = d.fields.getMap().get(apiName).getDescribe().getType();
                System.debug(fieldType);
                if(fieldType == Schema.DisplayType.TextArea || fieldType == Schema.DisplayType.String){
                    taskRec.put(apiName, fieldValueMap.get(apiName));
                }
                else if(fieldType == Schema.DisplayType.Date){
                    if(fieldValueMap.get(apiName) != null && fieldValueMap.get(apiName) != ''){
                        taskRec.put(apiName, date.valueOf(fieldValueMap.get(apiName)));
                    }
   
                }
                else if(fieldType == Schema.DisplayType.DateTime){
                    //First convert to date and then datetime
                    Date activityDt = date.valueOf(fieldValueMap.get(apiName));
                    DateTime DT = DateTime.newInstance(activityDt.year(), activityDt.month(), activityDt.day(), 11, 59, 59);
                    taskRec.put(apiName, DT);
                }
                else if(fieldType == Schema.DisplayType.Integer){
                    taskRec.put(apiName, decimal.valueOf(fieldValueMap.get(apiName)));
                }
                else if(fieldType == Schema.DisplayType.Boolean){
                    taskRec.put(apiName, boolean.valueOf(fieldValueMap.get(apiName)));
                }
            }
        }
        
        /* if(fieldValueMap != null){
for(String apiName : fieldValueMap.keySet()){
taskRec.put(apiName, fieldValueMap.get(apiName));
}
}*/
        taskRec.WhatId = ObjId;
        return taskRec;
    }
    /*Common method for event creation*/
    public static Event createEvent(String Subject,Id Owner,Id ObjId,Map<String,String> fieldValueMap){
        SObjectType r = ((SObject)(Type.forName('Schema.Event').newInstance())).getSObjectType();
        DescribeSObjectResult d = r.getDescribe();
        Event eveRec = new Event();
        eveRec.Subject = Subject;
        eveRec.ActivityDateTime = system.now();
        eveRec.OwnerId = Owner;
        eveRec.DurationInMinutes = 60;
        eveRec.WhatId = ObjId;
        eveRec.TRAV_Task_Unique_Identifier__c = ObjId + Subject + 'Open';
        if(fieldValueMap != null){
            for(String apiName : fieldValueMap.keySet()){
                Schema.DisplayType fieldType = d.fields.getMap().get(apiName).getDescribe().getType();
                System.debug(fieldType);
                if(fieldType == Schema.DisplayType.TextArea || fieldType == Schema.DisplayType.String){
                    eveRec.put(apiName, fieldValueMap.get(apiName));
                }
                else if(fieldType == Schema.DisplayType.Date){
                    eveRec.put(apiName, date.valueOf(fieldValueMap.get(apiName)));
                }
                else if(fieldType == Schema.DisplayType.DateTime){
                    //First convert to date and then datetime
                    system.debug('Date in map'+fieldValueMap.get('TRAV_Policy_Effective_Date__c'));
                    system.debug('Date in map'+fieldValueMap.get('trav_policy_effective_date__c'));
                    /*Date activityDt = date.valueOf(fieldValueMap.get(apiName));
DateTime DT = DateTime.newInstance(activityDt.year(), activityDt.month(), activityDt.day(), 11, 59, 59);*/
                    DateTime DT = datetime.valueOf(fieldValueMap.get(apiName));
                    system.debug('**Date in Map**'+DT);
                    eveRec.put(apiName, DT);
                }
                else if(fieldType == Schema.DisplayType.Integer){
                    eveRec.put(apiName, decimal.valueOf(fieldValueMap.get(apiName)));
                }
                else if(fieldType == Schema.DisplayType.Boolean){
                    eveRec.put(apiName, boolean.valueOf(fieldValueMap.get(apiName)));
                }
                else if(fieldType == Schema.DisplayType.PICKLIST){
                    eveRec.put(apiName, String.valueOf(fieldValueMap.get(apiName)));
                }
            }
        }
        /* if(fieldValueMap != null){
for(String apiName : fieldValueMap.keySet()){
eveRec.put(apiName, fieldValueMap.get(apiName));
}
}*/
        return eveRec;
    }
    /* to replace the merge fields */
    public static String replaceWithMergeFields(String subject, Map<String, String> fieldValueMap){
        for(String apiName : fieldValueMap.keySet()){
            String key = '{'+apiName+'}';
            system.debug('keykey'+key + subject);
            subject = subject.replace(key, fieldValueMap.get(apiName));
            system.debug('subjsubj'+subject);
        }
        return subject;
    }
}
