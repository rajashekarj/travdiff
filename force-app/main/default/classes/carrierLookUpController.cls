/**
 * Class to get carriers for lookUpResult component
 *
 * @Author : Derek Manierre
 *
 * Modification Log    :
 * -----------------------------------------------------------------------------------------------------------
 * Developer              User Story/Defect    Date             Description
 * -----------------------------------------------------------------------------------------------------------              
 * Derek Manierre         US62418              07/02/2019       Class to get carriers for lookUpResult component
 * -----------------------------------------------------------------------------------------------------------
 */

public without sharing class carrierLookUpController {
   @AuraEnabled
 public static List <TRAV_Carrier__c> fetchCarrier(String searchKeyWord) {
  String searchKey = searchKeyWord + '%';
  List <TRAV_Carrier__c> returnList = new List <TRAV_Carrier__c> ();
  List <TRAV_Carrier__c> lstOfCarrier = [select Name from TRAV_Carrier__c where Name LIKE: searchKey];
 
  for (TRAV_Carrier__c carrier: lstOfCarrier) {
     returnList.add(carrier);
     }
  return returnList;
 }
}